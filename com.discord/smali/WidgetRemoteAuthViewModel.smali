.class public final LWidgetRemoteAuthViewModel;
.super Lf/a/b/l0;
.source "WidgetRemoteAuthViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        LWidgetRemoteAuthViewModel$ViewState;,
        LWidgetRemoteAuthViewModel$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "LWidgetRemoteAuthViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/discord/utilities/rest/RestAPI;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 10

    const-string v0, "fingerprint"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, LWidgetRemoteAuthViewModel$ViewState$c;->a:LWidgetRemoteAuthViewModel$ViewState$c;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, LWidgetRemoteAuthViewModel;->e:Lcom/discord/utilities/rest/RestAPI;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, LWidgetRemoteAuthViewModel;->d:Lrx/subjects/BehaviorSubject;

    invoke-static {p1}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, LWidgetRemoteAuthViewModel$ViewState$a;->a:LWidgetRemoteAuthViewModel$ViewState$a;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;

    invoke-direct {v0, p1}, Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/discord/utilities/rest/RestAPI;->postRemoteAuthInitialize(Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    const/4 v0, 0x0

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, LWidgetRemoteAuthViewModel;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v7, LWidgetRemoteAuthViewModel$a;

    invoke-direct {v7, p0}, LWidgetRemoteAuthViewModel$a;-><init>(LWidgetRemoteAuthViewModel;)V

    const/4 v6, 0x0

    new-instance v5, LWidgetRemoteAuthViewModel$b;

    invoke-direct {v5, p0}, LWidgetRemoteAuthViewModel$b;-><init>(LWidgetRemoteAuthViewModel;)V

    const/16 v8, 0x16

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :goto_0
    return-void
.end method
