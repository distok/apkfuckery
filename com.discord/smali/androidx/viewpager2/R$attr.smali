.class public final Landroidx/viewpager2/R$attr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/viewpager2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alpha:I = 0x7f04003b

.field public static final fastScrollEnabled:I = 0x7f040266

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f040267

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f040268

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f040269

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f04026a

.field public static final font:I = 0x7f04028e

.field public static final fontProviderAuthority:I = 0x7f040290

.field public static final fontProviderCerts:I = 0x7f040291

.field public static final fontProviderFetchStrategy:I = 0x7f040292

.field public static final fontProviderFetchTimeout:I = 0x7f040293

.field public static final fontProviderPackage:I = 0x7f040294

.field public static final fontProviderQuery:I = 0x7f040295

.field public static final fontStyle:I = 0x7f040296

.field public static final fontVariationSettings:I = 0x7f040297

.field public static final fontWeight:I = 0x7f040298

.field public static final layoutManager:I = 0x7f040383

.field public static final recyclerViewStyle:I = 0x7f0404c3

.field public static final reverseLayout:I = 0x7f0404cc

.field public static final spanCount:I = 0x7f040521

.field public static final stackFromEnd:I = 0x7f040528

.field public static final ttcIndex:I = 0x7f040601


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
