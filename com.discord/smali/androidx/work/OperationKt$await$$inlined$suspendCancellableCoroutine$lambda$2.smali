.class public final Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;
.super Ljava/lang/Object;
.source "ListenableFuture.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/ListenableFutureKt;->await(Lf/h/b/a/a/a;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $cancellableContinuation:Ly/a/f;

.field public final synthetic $this_await$inlined:Lf/h/b/a/a/a;


# direct methods
.method public constructor <init>(Ly/a/f;Lf/h/b/a/a/a;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;->$cancellableContinuation:Ly/a/f;

    iput-object p2, p0, Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;->$this_await$inlined:Lf/h/b/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;->$cancellableContinuation:Ly/a/f;

    iget-object v1, p0, Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;->$this_await$inlined:Lf/h/b/a/a/a;

    invoke-interface {v1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    instance-of v0, v0, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;->$cancellableContinuation:Ly/a/f;

    invoke-interface {v0, v1}, Ly/a/f;->c(Ljava/lang/Throwable;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Landroidx/work/OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$2;->$cancellableContinuation:Ly/a/f;

    invoke-static {v1}, Lf/h/a/f/f/n/g;->createFailure(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method
