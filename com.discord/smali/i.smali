.class public final Li;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Li;->d:I

    iput-object p2, p0, Li;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget p1, p0, Li;->d:I

    if-eqz p1, :cond_7

    const/4 v0, 0x1

    if-eq p1, v0, :cond_6

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Li;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onExpandButtonClicked()V

    :cond_0
    return-void

    :cond_1
    const/4 p1, 0x0

    throw p1

    :cond_2
    iget-object p1, p0, Li;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onGalleryButtonClicked()V

    :cond_3
    return-void

    :cond_4
    iget-object p1, p0, Li;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_5

    invoke-interface {p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onCameraButtonClicked()V

    :cond_5
    return-void

    :cond_6
    iget-object p1, p0, Li;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-virtual {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->n()V

    return-void

    :cond_7
    iget-object p1, p0, Li;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_8

    invoke-interface {p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onExpressionTrayButtonClicked()V

    :cond_8
    return-void
.end method
