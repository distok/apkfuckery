.class public final Lcom/android/billingclient/api/zzh;
.super Landroid/os/ResultReceiver;
.source "com.android.billingclient:billing@@3.0.0"


# instance fields
.field public final synthetic d:Lf/e/a/a/a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/billingclient/api/zzh;->d:Lf/e/a/a/a;

    invoke-direct {p0, p2}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(ILandroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lcom/android/billingclient/api/zzh;->d:Lf/e/a/a/a;

    iget-object v0, v0, Lf/e/a/a/a;->d:Lf/e/a/a/t;

    iget-object v0, v0, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    iget-object v0, v0, Lf/e/a/a/u;->a:Lf/e/a/a/e;

    const-string v1, "BillingClient"

    if-nez v0, :cond_0

    const-string p1, "PurchasesUpdatedListener is null - no way to return the response."

    invoke-static {v1, p1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p2}, Lf/h/a/f/i/m/b;->b(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v3

    iput p1, v3, Lcom/android/billingclient/api/BillingResult$a;->a:I

    invoke-static {p2, v1}, Lf/h/a/f/i/m/b;->e(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object p1

    invoke-interface {v0, p1, v2}, Lf/e/a/a/e;->onPurchasesUpdated(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-void
.end method
