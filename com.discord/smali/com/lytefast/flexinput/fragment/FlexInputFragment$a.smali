.class public final Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/FlexInputFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;->d:I

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;->d:I

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->x:Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;->a:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->clear()V

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_0

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-interface {p1, v0}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onAttachmentsUpdated(Ljava/util/List;)V

    :cond_0
    return-void

    :cond_1
    const-string p1, "attachmentPreviewAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    throw v1

    :cond_3
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_4

    invoke-interface {p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onInputTextClicked()Z

    :cond_4
    return-void
.end method
