.class public final Lcom/lytefast/flexinput/fragment/CameraFragment$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/CameraFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->d:I

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 14

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    iget v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->d:I

    const-string v2, "cameraView"

    const/4 v3, 0x0

    if-eqz v1, :cond_19

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eq v1, v6, :cond_16

    const/4 p1, 0x0

    if-eq v1, v5, :cond_b

    if-ne v1, v4, :cond_a

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->e:Ljava/lang/Object;

    check-cast v1, Lcom/lytefast/flexinput/fragment/CameraFragment;

    iget-object v4, v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/otaliastudios/cameraview/CameraView;->getCameraOptions()Lf/l/a/c;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lf/l/a/c;->a()Ljava/util/Collection;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v4}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-gt v4, v6, :cond_1

    goto :goto_2

    :cond_1
    iget-object v4, v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lcom/otaliastudios/cameraview/CameraView;->getFacing()Lf/l/a/l/e;

    move-result-object v4

    const-string v5, "cameraView.facing"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lx/h/f;->getIndices(Ljava/util/Collection;)Lkotlin/ranges/IntRange;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/ranges/IntProgression;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    move-object v7, v5

    check-cast v7, Lx/p/b;

    iget-boolean v7, v7, Lx/p/b;->e:Z

    if-eqz v7, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/l/a/l/e;

    if-ne v4, v8, :cond_3

    const/4 v8, 0x1

    goto :goto_0

    :cond_3
    const/4 v8, 0x0

    :goto_0
    if-eqz v8, :cond_2

    goto :goto_1

    :cond_4
    move-object v7, v3

    :goto_1
    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :cond_5
    add-int/2addr p1, v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    rem-int/2addr p1, v4

    invoke-static {v0, p1}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/l/a/l/e;

    if-eqz p1, :cond_7

    invoke-virtual {v1}, Lcom/lytefast/flexinput/fragment/CameraFragment;->f()V

    iget-object v0, v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setFacing(Lf/l/a/l/e;)V

    goto :goto_2

    :cond_6
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_7
    :goto_2
    return-void

    :cond_8
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_9
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_a
    throw v3

    :cond_b
    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->e:Ljava/lang/Object;

    move-object v7, v1

    check-cast v7, Lcom/lytefast/flexinput/fragment/CameraFragment;

    iget-object v1, v7, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/otaliastudios/cameraview/CameraView;->getCameraOptions()Lf/l/a/c;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lf/l/a/c;->b()Ljava/util/Collection;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-static {v1}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    :cond_c
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v6, :cond_d

    goto/16 :goto_5

    :cond_d
    iget-object v1, v7, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/otaliastudios/cameraview/CameraView;->getFlash()Lf/l/a/l/f;

    move-result-object v1

    const-string v4, "cameraView.flash"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lx/h/f;->getIndices(Ljava/util/Collection;)Lkotlin/ranges/IntRange;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/ranges/IntProgression;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_e
    move-object v5, v4

    check-cast v5, Lx/p/b;

    iget-boolean v5, v5, Lx/p/b;->e:Z

    if-eqz v5, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v8, v5

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/l/a/l/f;

    if-ne v1, v8, :cond_f

    const/4 v8, 0x1

    goto :goto_3

    :cond_f
    const/4 v8, 0x0

    :goto_3
    if-eqz v8, :cond_e

    goto :goto_4

    :cond_10
    move-object v5, v3

    :goto_4
    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_11

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :cond_11
    add-int/2addr p1, v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr p1, v1

    invoke-static {v0, p1}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/l/a/l/f;

    if-eqz p1, :cond_13

    :try_start_0
    iget-object v0, v7, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v0, :cond_12

    invoke-virtual {v0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setFlash(Lf/l/a/l/f;)V

    invoke-virtual {v7}, Lcom/lytefast/flexinput/fragment/CameraFragment;->g()V

    goto :goto_5

    :cond_12
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    throw v3

    :catch_0
    move-exception p1

    move-object v9, p1

    sget p1, Lcom/lytefast/flexinput/R$g;->camera_unknown_error:I

    invoke-virtual {v7, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string p1, "getString(R.string.camera_unknown_error)"

    invoke-static {v8, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/16 v12, 0x8

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/lytefast/flexinput/fragment/CameraFragment;->i(Lcom/lytefast/flexinput/fragment/CameraFragment;Ljava/lang/String;Ljava/lang/Exception;ZLjava/lang/String;ILjava/lang/Object;)V

    :cond_13
    :goto_5
    return-void

    :cond_14
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_15
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_16
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/lytefast/flexinput/fragment/CameraFragment;

    const-string v1, "it"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "it.context"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->l:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/CameraFragment;->h()Lf/b/a/b;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-interface {v1}, Lf/b/a/b;->getFileManager()Lcom/lytefast/flexinput/managers/FileManager;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-interface {v1}, Lcom/lytefast/flexinput/managers/FileManager;->b()Ljava/io/File;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/lytefast/flexinput/managers/FileManager;->a(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "output"

    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v5, "Intent(MediaStore.ACTION\u2026ANT_WRITE_URI_PERMISSION)"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    if-eqz v5, :cond_18

    iput-object v2, v0, Lcom/lytefast/flexinput/fragment/CameraFragment;->k:Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v5, 0x10000

    invoke-virtual {v2, v3, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    const-string v5, "packageManager.queryInte\u2026nager.MATCH_DEFAULT_ONLY)"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_17

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v5, v1, v4}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    goto :goto_6

    :cond_17
    const/16 p1, 0x11d7

    invoke-virtual {v0, v3, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_18
    return-void

    :cond_19
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/lytefast/flexinput/fragment/CameraFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz p1, :cond_1a

    new-instance v0, Lf/l/a/k$a;

    invoke-direct {v0}, Lf/l/a/k$a;-><init>()V

    iget-object p1, p1, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {p1, v0}, Lf/l/a/m/j;->O0(Lf/l/a/k$a;)V

    return-void

    :cond_1a
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method
