.class public final Lcom/airbnb/lottie/R$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/airbnb/lottie/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final actionBarDivider:I = 0x7f040000

.field public static final actionBarItemBackground:I = 0x7f040001

.field public static final actionBarPopupTheme:I = 0x7f040002

.field public static final actionBarSize:I = 0x7f040003

.field public static final actionBarSplitStyle:I = 0x7f040004

.field public static final actionBarStyle:I = 0x7f040005

.field public static final actionBarTabBarStyle:I = 0x7f040006

.field public static final actionBarTabStyle:I = 0x7f040007

.field public static final actionBarTabTextStyle:I = 0x7f040008

.field public static final actionBarTheme:I = 0x7f040009

.field public static final actionBarWidgetTheme:I = 0x7f04000a

.field public static final actionButtonStyle:I = 0x7f04000b

.field public static final actionDropDownStyle:I = 0x7f04000c

.field public static final actionLayout:I = 0x7f04000d

.field public static final actionMenuTextAppearance:I = 0x7f04000e

.field public static final actionMenuTextColor:I = 0x7f04000f

.field public static final actionModeBackground:I = 0x7f040010

.field public static final actionModeCloseButtonStyle:I = 0x7f040011

.field public static final actionModeCloseDrawable:I = 0x7f040012

.field public static final actionModeCopyDrawable:I = 0x7f040013

.field public static final actionModeCutDrawable:I = 0x7f040014

.field public static final actionModeFindDrawable:I = 0x7f040015

.field public static final actionModePasteDrawable:I = 0x7f040016

.field public static final actionModePopupWindowStyle:I = 0x7f040017

.field public static final actionModeSelectAllDrawable:I = 0x7f040018

.field public static final actionModeShareDrawable:I = 0x7f040019

.field public static final actionModeSplitBackground:I = 0x7f04001a

.field public static final actionModeStyle:I = 0x7f04001b

.field public static final actionModeWebSearchDrawable:I = 0x7f04001c

.field public static final actionOverflowButtonStyle:I = 0x7f04001d

.field public static final actionOverflowMenuStyle:I = 0x7f04001e

.field public static final actionProviderClass:I = 0x7f04001f

.field public static final actionViewClass:I = 0x7f040021

.field public static final activityChooserViewStyle:I = 0x7f040024

.field public static final alertDialogButtonGroupStyle:I = 0x7f04002c

.field public static final alertDialogCenterButtons:I = 0x7f04002d

.field public static final alertDialogStyle:I = 0x7f04002e

.field public static final alertDialogTheme:I = 0x7f04002f

.field public static final allowStacking:I = 0x7f04003a

.field public static final alpha:I = 0x7f04003b

.field public static final alphabeticModifiers:I = 0x7f04003c

.field public static final arrowHeadLength:I = 0x7f040044

.field public static final arrowShaftLength:I = 0x7f040045

.field public static final autoCompleteTextViewStyle:I = 0x7f04004f

.field public static final autoSizeMaxTextSize:I = 0x7f040050

.field public static final autoSizeMinTextSize:I = 0x7f040051

.field public static final autoSizePresetSizes:I = 0x7f040052

.field public static final autoSizeStepGranularity:I = 0x7f040053

.field public static final autoSizeTextType:I = 0x7f040054

.field public static final background:I = 0x7f040059

.field public static final backgroundSplit:I = 0x7f040061

.field public static final backgroundStacked:I = 0x7f040062

.field public static final backgroundTint:I = 0x7f040063

.field public static final backgroundTintMode:I = 0x7f040064

.field public static final barLength:I = 0x7f040069

.field public static final borderlessButtonStyle:I = 0x7f04008d

.field public static final buttonBarButtonStyle:I = 0x7f0400a2

.field public static final buttonBarNegativeButtonStyle:I = 0x7f0400a3

.field public static final buttonBarNeutralButtonStyle:I = 0x7f0400a4

.field public static final buttonBarPositiveButtonStyle:I = 0x7f0400a5

.field public static final buttonBarStyle:I = 0x7f0400a6

.field public static final buttonGravity:I = 0x7f0400a8

.field public static final buttonIconDimen:I = 0x7f0400a9

.field public static final buttonPanelSideLayout:I = 0x7f0400aa

.field public static final buttonStyle:I = 0x7f0400ac

.field public static final buttonStyleSmall:I = 0x7f0400ad

.field public static final buttonTint:I = 0x7f0400ae

.field public static final buttonTintMode:I = 0x7f0400af

.field public static final checkboxStyle:I = 0x7f0400fa

.field public static final checkedTextViewStyle:I = 0x7f040101

.field public static final closeIcon:I = 0x7f04011d

.field public static final closeItemLayout:I = 0x7f040124

.field public static final collapseContentDescription:I = 0x7f040125

.field public static final collapseIcon:I = 0x7f040126

.field public static final color:I = 0x7f040129

.field public static final colorAccent:I = 0x7f04012a

.field public static final colorBackgroundFloating:I = 0x7f04012e

.field public static final colorButtonNormal:I = 0x7f04013e

.field public static final colorControlActivated:I = 0x7f040144

.field public static final colorControlHighlight:I = 0x7f040145

.field public static final colorControlNormal:I = 0x7f040146

.field public static final colorError:I = 0x7f040151

.field public static final colorPrimary:I = 0x7f040160

.field public static final colorPrimaryDark:I = 0x7f040163

.field public static final colorSwitchThumbNormal:I = 0x7f040173

.field public static final commitIcon:I = 0x7f04017c

.field public static final contentDescription:I = 0x7f040184

.field public static final contentInsetEnd:I = 0x7f040185

.field public static final contentInsetEndWithActions:I = 0x7f040186

.field public static final contentInsetLeft:I = 0x7f040187

.field public static final contentInsetRight:I = 0x7f040188

.field public static final contentInsetStart:I = 0x7f040189

.field public static final contentInsetStartWithNavigation:I = 0x7f04018a

.field public static final controlBackground:I = 0x7f040193

.field public static final coordinatorLayoutStyle:I = 0x7f040195

.field public static final customNavigationLayout:I = 0x7f0401d7

.field public static final defaultQueryHint:I = 0x7f0401f8

.field public static final dialogCornerRadius:I = 0x7f040200

.field public static final dialogPreferredPadding:I = 0x7f040205

.field public static final dialogTheme:I = 0x7f040206

.field public static final displayOptions:I = 0x7f040209

.field public static final divider:I = 0x7f04020a

.field public static final dividerHorizontal:I = 0x7f04020e

.field public static final dividerPadding:I = 0x7f04020f

.field public static final dividerVertical:I = 0x7f040210

.field public static final drawableSize:I = 0x7f04021a

.field public static final drawerArrowStyle:I = 0x7f04022f

.field public static final dropDownListViewStyle:I = 0x7f040230

.field public static final dropdownListPreferredItemHeight:I = 0x7f040231

.field public static final editTextBackground:I = 0x7f040234

.field public static final editTextColor:I = 0x7f040235

.field public static final editTextStyle:I = 0x7f040237

.field public static final elevation:I = 0x7f040239

.field public static final expandActivityOverflowButtonDrawable:I = 0x7f040251

.field public static final firstBaselineToTopHeight:I = 0x7f04026f

.field public static final font:I = 0x7f04028e

.field public static final fontFamily:I = 0x7f04028f

.field public static final fontProviderAuthority:I = 0x7f040290

.field public static final fontProviderCerts:I = 0x7f040291

.field public static final fontProviderFetchStrategy:I = 0x7f040292

.field public static final fontProviderFetchTimeout:I = 0x7f040293

.field public static final fontProviderPackage:I = 0x7f040294

.field public static final fontProviderQuery:I = 0x7f040295

.field public static final fontStyle:I = 0x7f040296

.field public static final fontVariationSettings:I = 0x7f040297

.field public static final fontWeight:I = 0x7f040298

.field public static final gapBetweenBars:I = 0x7f04029c

.field public static final goIcon:I = 0x7f0402a3

.field public static final height:I = 0x7f0402ad

.field public static final hideOnContentScroll:I = 0x7f0402b3

.field public static final homeAsUpIndicator:I = 0x7f0402bb

.field public static final homeLayout:I = 0x7f0402bc

.field public static final icon:I = 0x7f040322

.field public static final iconTint:I = 0x7f040329

.field public static final iconTintMode:I = 0x7f04032a

.field public static final iconifiedByDefault:I = 0x7f04032b

.field public static final imageButtonStyle:I = 0x7f04032e

.field public static final indeterminateProgressStyle:I = 0x7f04034e

.field public static final initialActivityCount:I = 0x7f040355

.field public static final isLightTheme:I = 0x7f04035a

.field public static final itemPadding:I = 0x7f040365

.field public static final keylines:I = 0x7f040379

.field public static final lastBaselineToBottomHeight:I = 0x7f04037f

.field public static final layout:I = 0x7f040380

.field public static final layout_anchor:I = 0x7f040385

.field public static final layout_anchorGravity:I = 0x7f040386

.field public static final layout_behavior:I = 0x7f040387

.field public static final layout_dodgeInsetEdges:I = 0x7f0403b4

.field public static final layout_insetEdge:I = 0x7f0403c3

.field public static final layout_keyline:I = 0x7f0403c4

.field public static final lineHeight:I = 0x7f0403db

.field public static final listChoiceBackgroundIndicator:I = 0x7f0403de

.field public static final listDividerAlertDialog:I = 0x7f0403e1

.field public static final listItemLayout:I = 0x7f0403e2

.field public static final listLayout:I = 0x7f0403e3

.field public static final listMenuViewStyle:I = 0x7f0403e4

.field public static final listPopupWindowStyle:I = 0x7f0403e5

.field public static final listPreferredItemHeight:I = 0x7f0403e6

.field public static final listPreferredItemHeightLarge:I = 0x7f0403e7

.field public static final listPreferredItemHeightSmall:I = 0x7f0403e8

.field public static final listPreferredItemPaddingLeft:I = 0x7f0403ea

.field public static final listPreferredItemPaddingRight:I = 0x7f0403eb

.field public static final logo:I = 0x7f0403ed

.field public static final logoDescription:I = 0x7f0403ee

.field public static final lottieAnimationViewStyle:I = 0x7f0403ef

.field public static final lottie_autoPlay:I = 0x7f0403f0

.field public static final lottie_cacheComposition:I = 0x7f0403f1

.field public static final lottie_colorFilter:I = 0x7f0403f2

.field public static final lottie_enableMergePathsForKitKatAndAbove:I = 0x7f0403f3

.field public static final lottie_fallbackRes:I = 0x7f0403f4

.field public static final lottie_fileName:I = 0x7f0403f5

.field public static final lottie_imageAssetsFolder:I = 0x7f0403f6

.field public static final lottie_loop:I = 0x7f0403f7

.field public static final lottie_progress:I = 0x7f0403f8

.field public static final lottie_rawRes:I = 0x7f0403f9

.field public static final lottie_renderMode:I = 0x7f0403fa

.field public static final lottie_repeatCount:I = 0x7f0403fb

.field public static final lottie_repeatMode:I = 0x7f0403fc

.field public static final lottie_scale:I = 0x7f0403fd

.field public static final lottie_speed:I = 0x7f0403fe

.field public static final lottie_url:I = 0x7f0403ff

.field public static final maxButtonHeight:I = 0x7f04041a

.field public static final measureWithLargestChild:I = 0x7f040425

.field public static final multiChoiceItemLayout:I = 0x7f04043c

.field public static final navigationContentDescription:I = 0x7f04043d

.field public static final navigationIcon:I = 0x7f04043e

.field public static final navigationMode:I = 0x7f04043f

.field public static final numericModifiers:I = 0x7f040447

.field public static final overlapAnchor:I = 0x7f040450

.field public static final paddingBottomNoButtons:I = 0x7f040455

.field public static final paddingEnd:I = 0x7f040457

.field public static final paddingStart:I = 0x7f04045a

.field public static final paddingTopNoTitle:I = 0x7f04045b

.field public static final panelBackground:I = 0x7f04045c

.field public static final panelMenuListTheme:I = 0x7f04045d

.field public static final panelMenuListWidth:I = 0x7f04045e

.field public static final popupMenuStyle:I = 0x7f04047a

.field public static final popupTheme:I = 0x7f04047b

.field public static final popupWindowStyle:I = 0x7f04047c

.field public static final preserveIconSpacing:I = 0x7f04048a

.field public static final progressBarPadding:I = 0x7f0404b5

.field public static final progressBarStyle:I = 0x7f0404b6

.field public static final queryBackground:I = 0x7f0404ba

.field public static final queryHint:I = 0x7f0404bb

.field public static final radioButtonStyle:I = 0x7f0404bc

.field public static final ratingBarStyle:I = 0x7f0404c0

.field public static final ratingBarStyleIndicator:I = 0x7f0404c1

.field public static final ratingBarStyleSmall:I = 0x7f0404c2

.field public static final searchHintIcon:I = 0x7f0404ed

.field public static final searchIcon:I = 0x7f0404ee

.field public static final searchViewStyle:I = 0x7f0404f0

.field public static final seekBarStyle:I = 0x7f0404f8

.field public static final selectableItemBackground:I = 0x7f0404fa

.field public static final selectableItemBackgroundBorderless:I = 0x7f0404fb

.field public static final showAsAction:I = 0x7f040508

.field public static final showDividers:I = 0x7f04050c

.field public static final showText:I = 0x7f040510

.field public static final showTitle:I = 0x7f040511

.field public static final singleChoiceItemLayout:I = 0x7f040517

.field public static final spinBars:I = 0x7f040522

.field public static final spinnerDropDownItemStyle:I = 0x7f040523

.field public static final spinnerStyle:I = 0x7f040524

.field public static final splitTrack:I = 0x7f040525

.field public static final srcCompat:I = 0x7f040527

.field public static final state_above_anchor:I = 0x7f040530

.field public static final statusBarBackground:I = 0x7f040536

.field public static final subMenuArrow:I = 0x7f04053f

.field public static final submitBackground:I = 0x7f040540

.field public static final subtitle:I = 0x7f040542

.field public static final subtitleTextAppearance:I = 0x7f040543

.field public static final subtitleTextColor:I = 0x7f040544

.field public static final subtitleTextStyle:I = 0x7f040545

.field public static final suggestionRowLayout:I = 0x7f040549

.field public static final switchMinWidth:I = 0x7f040553

.field public static final switchPadding:I = 0x7f040554

.field public static final switchStyle:I = 0x7f040557

.field public static final switchTextAppearance:I = 0x7f040558

.field public static final textAllCaps:I = 0x7f04057d

.field public static final textAppearanceLargePopupMenu:I = 0x7f040588

.field public static final textAppearanceListItem:I = 0x7f04058a

.field public static final textAppearanceListItemSecondary:I = 0x7f04058b

.field public static final textAppearanceListItemSmall:I = 0x7f04058c

.field public static final textAppearancePopupMenuHeader:I = 0x7f04058e

.field public static final textAppearanceSearchResultSubtitle:I = 0x7f04058f

.field public static final textAppearanceSearchResultTitle:I = 0x7f040590

.field public static final textAppearanceSmallPopupMenu:I = 0x7f040591

.field public static final textColorAlertDialogListItem:I = 0x7f040594

.field public static final textColorSearchUrl:I = 0x7f040595

.field public static final theme:I = 0x7f04059c

.field public static final thickness:I = 0x7f0405c7

.field public static final thumbTextPadding:I = 0x7f0405cb

.field public static final thumbTint:I = 0x7f0405cc

.field public static final thumbTintMode:I = 0x7f0405cd

.field public static final tickMark:I = 0x7f0405d1

.field public static final tickMarkTint:I = 0x7f0405d2

.field public static final tickMarkTintMode:I = 0x7f0405d3

.field public static final tint:I = 0x7f0405d5

.field public static final tintMode:I = 0x7f0405d6

.field public static final title:I = 0x7f0405d7

.field public static final titleMargin:I = 0x7f0405d9

.field public static final titleMarginBottom:I = 0x7f0405da

.field public static final titleMarginEnd:I = 0x7f0405db

.field public static final titleMarginStart:I = 0x7f0405dc

.field public static final titleMarginTop:I = 0x7f0405dd

.field public static final titleMargins:I = 0x7f0405de

.field public static final titleTextAppearance:I = 0x7f0405df

.field public static final titleTextColor:I = 0x7f0405e0

.field public static final titleTextStyle:I = 0x7f0405e1

.field public static final toolbarNavigationButtonStyle:I = 0x7f0405e5

.field public static final toolbarStyle:I = 0x7f0405e6

.field public static final tooltipForegroundColor:I = 0x7f0405e7

.field public static final tooltipFrameBackground:I = 0x7f0405e8

.field public static final tooltipText:I = 0x7f0405ea

.field public static final track:I = 0x7f0405f1

.field public static final trackTint:I = 0x7f0405f6

.field public static final trackTintMode:I = 0x7f0405f7

.field public static final ttcIndex:I = 0x7f040601

.field public static final viewInflaterClass:I = 0x7f040629

.field public static final voiceIcon:I = 0x7f04062b

.field public static final windowActionBar:I = 0x7f040638

.field public static final windowActionBarOverlay:I = 0x7f040639

.field public static final windowActionModeOverlay:I = 0x7f04063a

.field public static final windowFixedHeightMajor:I = 0x7f04063b

.field public static final windowFixedHeightMinor:I = 0x7f04063c

.field public static final windowFixedWidthMajor:I = 0x7f04063d

.field public static final windowFixedWidthMinor:I = 0x7f04063e

.field public static final windowMinWidthMajor:I = 0x7f04063f

.field public static final windowMinWidthMinor:I = 0x7f040640

.field public static final windowNoTitle:I = 0x7f040641


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
