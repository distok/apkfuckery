.class public final Lcom/discord/rtcconnection/KrispOveruseDetector;
.super Ljava/lang/Object;
.source "KrispOveruseDetector.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/rtcconnection/KrispOveruseDetector$Status;
    }
.end annotation


# instance fields
.field public a:Lco/discord/media_engine/OutboundRtpAudio;

.field public b:Lco/discord/media_engine/OutboundRtpAudio;

.field public c:I

.field public final d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/rtcconnection/KrispOveruseDetector;->d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    return-void
.end method


# virtual methods
.method public final a(Lco/discord/media_engine/OutboundRtpAudio;Lco/discord/media_engine/OutboundRtpAudio;D)Lkotlin/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/discord/media_engine/OutboundRtpAudio;",
            "Lco/discord/media_engine/OutboundRtpAudio;",
            "D)",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-wide/16 v1, 0x0

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p2}, Lco/discord/media_engine/OutboundRtpAudio;->getNoiseCancellerProcessTime()J

    move-result-wide v3

    invoke-virtual {p1}, Lco/discord/media_engine/OutboundRtpAudio;->getNoiseCancellerProcessTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {p2}, Lco/discord/media_engine/OutboundRtpAudio;->getFramesCaptured()J

    move-result-wide v5

    invoke-virtual {p1}, Lco/discord/media_engine/OutboundRtpAudio;->getFramesCaptured()J

    move-result-wide p1

    sub-long/2addr v5, p1

    cmp-long p1, v5, v1

    if-nez p1, :cond_1

    new-instance p1, Lkotlin/Pair;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-direct {p1, v0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/Pair;

    long-to-double v0, v3

    long-to-double v5, v5

    div-double/2addr v0, v5

    cmpl-double p2, v0, p3

    if-lez p2, :cond_2

    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_3
    :goto_1
    new-instance p1, Lkotlin/Pair;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-direct {p1, v0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method
