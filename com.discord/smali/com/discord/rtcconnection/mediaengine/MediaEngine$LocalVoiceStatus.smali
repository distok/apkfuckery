.class public final Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;
.super Ljava/lang/Object;
.source "MediaEngine.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/rtcconnection/mediaengine/MediaEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocalVoiceStatus"
.end annotation


# instance fields
.field public final a:F

.field public final b:Z


# direct methods
.method public constructor <init>(FZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->a:F

    iput-boolean p2, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->b:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->a:F

    iget v1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->a:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->b:Z

    iget-boolean p1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->b:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->b:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "LocalVoiceStatus(amplitude="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", isSpeaking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;->b:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
