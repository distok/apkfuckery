.class public interface abstract Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;
.super Ljava/lang/Object;
.source "MediaEngineConnection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;,
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;,
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;,
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;,
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;,
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;,
        Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;
    }
.end annotation


# virtual methods
.method public abstract a(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(Z)V
.end method

.method public abstract d(JF)V
.end method

.method public abstract e(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
.end method

.method public abstract f()V
.end method

.method public abstract g(Z)V
.end method

.method public abstract getType()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;
.end method

.method public abstract h(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;)V
.end method

.method public abstract i()V
.end method

.method public abstract j(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V
.end method

.method public abstract k(Lkotlin/jvm/functions/Function1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lco/discord/media_engine/Stats;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract l(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V
.end method

.method public abstract m(JILjava/lang/Integer;Z)V
.end method

.method public abstract n(Z)V
.end method

.method public abstract o(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract p(Ljava/lang/String;[I)V
.end method

.method public abstract q(Z)V
.end method

.method public abstract r(JZ)V
.end method
