.class public final Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;
.super Ljava/lang/Object;
.source "MediaEngineConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:Z

.field public final f:I


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3f

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;-><init>(IIIZZII)V

    return-void
.end method

.method public constructor <init>(IIIZZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    iput p2, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    iput p3, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    iput-boolean p4, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    iput-boolean p5, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    iput p6, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->f:I

    return-void
.end method

.method public constructor <init>(IIIZZII)V
    .locals 2

    and-int/lit8 v0, p7, 0x1

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_1

    const/16 p2, 0xa

    :cond_1
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_2

    const/16 p3, 0x28

    :cond_2
    and-int/lit8 v0, p7, 0x8

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    const/4 p4, 0x1

    :cond_3
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_4

    const/4 p5, 0x1

    :cond_4
    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_5

    const/4 p6, 0x5

    :cond_5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    iput p2, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    iput p3, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    iput-boolean p4, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    iput-boolean p5, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    iput p6, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->f:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    iget v1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    iget v1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    iget v1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    iget-boolean v1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    iget-boolean v1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->f:I

    iget p1, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->f:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "InputModeOptions(vadThreshold="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", vadLeadingFrames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", vadTrailingFrames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", vadAutoThreshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", vadUseKrisp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", pttReleaseDelayMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->f:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
