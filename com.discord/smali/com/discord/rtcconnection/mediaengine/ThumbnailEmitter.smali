.class public final Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;
.super Ljava/lang/Object;
.source "ThumbnailEmitter.kt"


# instance fields
.field public a:J

.field public final b:Ljava/nio/ByteBuffer;

.field public final c:Landroid/graphics/Matrix;

.field public final d:Lorg/webrtc/GlRectDrawer;

.field public final e:Lorg/webrtc/VideoFrameDrawer;

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:J

.field public final j:J

.field public final k:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/graphics/Bitmap;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/discord/utilities/time/Clock;


# direct methods
.method public constructor <init>(IIJJLkotlin/jvm/functions/Function1;Lcom/discord/utilities/time/Clock;I)V
    .locals 0

    and-int/lit8 p8, p9, 0x8

    if-eqz p8, :cond_0

    const-wide/16 p5, 0x0

    :cond_0
    and-int/lit8 p8, p9, 0x20

    if-eqz p8, :cond_1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p8

    goto :goto_0

    :cond_1
    const/4 p8, 0x0

    :goto_0
    const-string p9, "onNextThumbnail"

    invoke-static {p7, p9}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p9, "clock"

    invoke-static {p8, p9}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    iput p2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    iput-wide p3, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->i:J

    iput-wide p5, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->j:J

    iput-object p7, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->k:Lkotlin/jvm/functions/Function1;

    iput-object p8, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->l:Lcom/discord/utilities/time/Clock;

    const/16 p5, 0x3e8

    int-to-long p5, p5

    mul-long p3, p3, p5

    mul-long p3, p3, p5

    neg-long p3, p3

    iput-wide p3, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->a:J

    mul-int p1, p1, p2

    mul-int/lit8 p1, p1, 0x4

    invoke-static {p1}, Lorg/webrtc/JniCommon;->nativeAllocateByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->b:Ljava/nio/ByteBuffer;

    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->c:Landroid/graphics/Matrix;

    new-instance p2, Lorg/webrtc/GlRectDrawer;

    invoke-direct {p2}, Lorg/webrtc/GlRectDrawer;-><init>()V

    iput-object p2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->d:Lorg/webrtc/GlRectDrawer;

    new-instance p2, Lorg/webrtc/VideoFrameDrawer;

    invoke-direct {p2}, Lorg/webrtc/VideoFrameDrawer;-><init>()V

    iput-object p2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->e:Lorg/webrtc/VideoFrameDrawer;

    invoke-interface {p8}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->f:J

    const/high16 p2, 0x3f000000    # 0.5f

    invoke-virtual {p1, p2, p2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    const/high16 p2, 0x3f800000    # 1.0f

    const/high16 p3, -0x40800000    # -1.0f

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->preScale(FF)Z

    const/high16 p2, -0x41000000    # -0.5f

    invoke-virtual {p1, p2, p2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    return-void
.end method


# virtual methods
.method public final a(Lorg/webrtc/VideoFrame;)Landroid/graphics/Bitmap;
    .locals 11

    new-instance v0, Lorg/webrtc/GlTextureFrameBuffer;

    const/16 v1, 0x1908

    invoke-direct {v0, v1}, Lorg/webrtc/GlTextureFrameBuffer;-><init>(I)V

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    iget v2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/GlTextureFrameBuffer;->setSize(II)V

    invoke-virtual {v0}, Lorg/webrtc/GlTextureFrameBuffer;->getFrameBufferId()I

    move-result v1

    const v2, 0x8d40

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    const-string v1, "glBindFramebuffer"

    invoke-static {v1}, Lorg/webrtc/GlUtil;->checkNoGLES2Error(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v1, v1, v2}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getRotatedWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getRotatedHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    int-to-float v2, v2

    iget v3, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getRotatedWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    int-to-float v2, v2

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getRotatedHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    mul-float v2, v2, v1

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    int-to-float v1, v1

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    iget-object v3, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->e:Lorg/webrtc/VideoFrameDrawer;

    iget-object v5, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->d:Lorg/webrtc/GlRectDrawer;

    iget-object v6, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->c:Landroid/graphics/Matrix;

    invoke-static {v1}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result v7

    const/4 v8, 0x0

    invoke-static {v2}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result v9

    iget v10, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    move-object v4, p1

    invoke-virtual/range {v3 .. v10}, Lorg/webrtc/VideoFrameDrawer;->drawFrame(Lorg/webrtc/VideoFrame;Lorg/webrtc/RendererCommon$GlDrawer;Landroid/graphics/Matrix;IIII)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getRotatedHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    int-to-float v2, v2

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getRotatedWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    mul-float v2, v2, v1

    iget v1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    int-to-float v1, v1

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    iget-object v3, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->e:Lorg/webrtc/VideoFrameDrawer;

    iget-object v5, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->d:Lorg/webrtc/GlRectDrawer;

    iget-object v6, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->c:Landroid/graphics/Matrix;

    const/4 v7, 0x0

    invoke-static {v1}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result v8

    iget v9, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    invoke-static {v2}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result v10

    move-object v4, p1

    invoke-virtual/range {v3 .. v10}, Lorg/webrtc/VideoFrameDrawer;->drawFrame(Lorg/webrtc/VideoFrame;Lorg/webrtc/RendererCommon$GlDrawer;Landroid/graphics/Matrix;IIII)V

    :goto_0
    iget-object p1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0}, Lorg/webrtc/GlTextureFrameBuffer;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Lorg/webrtc/GlTextureFrameBuffer;->getHeight()I

    move-result v4

    const/16 v5, 0x1908

    const/16 v6, 0x1401

    iget-object v7, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->b:Ljava/nio/ByteBuffer;

    invoke-static/range {v1 .. v7}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    const-string p1, "ThumbnailEmitter.createThumbnail"

    invoke-static {p1}, Lorg/webrtc/GlUtil;->checkNoGLES2Error(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/webrtc/GlTextureFrameBuffer;->release()V

    iget-object p1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget p1, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->g:I

    iget v0, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->h:I

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
