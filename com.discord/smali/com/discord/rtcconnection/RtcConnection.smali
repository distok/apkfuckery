.class public final Lcom/discord/rtcconnection/RtcConnection;
.super Ljava/lang/Object;
.source "RtcConnection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/rtcconnection/RtcConnection$c;,
        Lcom/discord/rtcconnection/RtcConnection$State;,
        Lcom/discord/rtcconnection/RtcConnection$Quality;,
        Lcom/discord/rtcconnection/RtcConnection$b;,
        Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;,
        Lcom/discord/rtcconnection/RtcConnection$a;
    }
.end annotation


# static fields
.field public static I:I

.field public static final J:Lcom/discord/rtcconnection/RtcConnection$a;


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:J

.field public final C:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

.field public final D:Lcom/discord/utilities/logging/Logger;

.field public final E:Lcom/discord/utilities/time/Clock;

.field public final F:Lcom/discord/rtcconnection/RtcConnection$c;

.field public final G:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final H:Ljava/lang/String;

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/rtcconnection/RtcConnection$b;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lf/a/h/v/h;

.field public final e:Lcom/discord/utilities/networking/Backoff;

.field public f:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;

.field public g:Lcom/discord/rtcconnection/RtcConnection$State;

.field public h:Z

.field public i:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lf/a/h/u/a;

.field public k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/Integer;

.field public n:I

.field public o:Ljava/lang/Long;

.field public p:Ljava/lang/Long;

.field public q:I

.field public r:Lrx/Subscription;

.field public s:Z

.field public t:Ljava/lang/String;

.field public final u:Lf/a/h/r;

.field public final v:Lf/a/h/l;

.field public final w:Ljava/lang/Long;

.field public final x:J

.field public y:Ljava/lang/String;

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/rtcconnection/RtcConnection$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/rtcconnection/RtcConnection$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/rtcconnection/RtcConnection;->J:Lcom/discord/rtcconnection/RtcConnection$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;JLjava/lang/String;ZLjava/lang/String;JLcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/time/Clock;Lcom/discord/rtcconnection/RtcConnection$c;Lcom/discord/utilities/networking/NetworkMonitor;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move/from16 v9, p16

    and-int/lit16 v10, v9, 0x200

    if-eqz v10, :cond_0

    sget-object v10, Lcom/discord/rtcconnection/RtcConnection$c$a;->a:Lcom/discord/rtcconnection/RtcConnection$c$a;

    goto :goto_0

    :cond_0
    move-object/from16 v10, p12

    :goto_0
    and-int/lit16 v11, v9, 0x800

    if-eqz v11, :cond_1

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    goto :goto_1

    :cond_1
    move-object/from16 v11, p14

    :goto_1
    and-int/lit16 v9, v9, 0x1000

    if-eqz v9, :cond_2

    const/4 v9, 0x0

    goto :goto_2

    :cond_2
    move-object/from16 v9, p15

    :goto_2
    const-string v12, "sessionId"

    invoke-static {v4, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "rtcServerId"

    invoke-static {v5, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "mediaEngine"

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "logger"

    invoke-static {v7, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "clock"

    invoke-static {v8, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "rtcConnectionType"

    invoke-static {v10, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "networkMonitor"

    move-object/from16 v13, p13

    invoke-static {v13, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "mutedUsers"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    iput-wide v2, v0, Lcom/discord/rtcconnection/RtcConnection;->x:J

    iput-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->y:Ljava/lang/String;

    move/from16 v4, p5

    iput-boolean v4, v0, Lcom/discord/rtcconnection/RtcConnection;->z:Z

    iput-object v5, v0, Lcom/discord/rtcconnection/RtcConnection;->A:Ljava/lang/String;

    move-wide/from16 v4, p7

    iput-wide v4, v0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    iput-object v6, v0, Lcom/discord/rtcconnection/RtcConnection;->C:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    iput-object v7, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iput-object v8, v0, Lcom/discord/rtcconnection/RtcConnection;->E:Lcom/discord/utilities/time/Clock;

    iput-object v10, v0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    iput-object v11, v0, Lcom/discord/rtcconnection/RtcConnection;->G:Ljava/util/Map;

    iput-object v9, v0, Lcom/discord/rtcconnection/RtcConnection;->H:Ljava/lang/String;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UUID.randomUUID().toString()"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-class v5, Lcom/discord/rtcconnection/RtcConnection;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x3a

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v5, Lcom/discord/rtcconnection/RtcConnection;->I:I

    const/4 v6, 0x1

    add-int/2addr v5, v6

    sput v5, Lcom/discord/rtcconnection/RtcConnection;->I:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v0, Lcom/discord/rtcconnection/RtcConnection;->c:Ljava/util/List;

    new-instance v5, Lcom/discord/utilities/networking/Backoff;

    const-wide/16 v15, 0x3e8

    const-wide/16 v17, 0x2710

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x1c

    const/16 v23, 0x0

    move-object v14, v5

    invoke-direct/range {v14 .. v23}, Lcom/discord/utilities/networking/Backoff;-><init>(JJIZLcom/discord/utilities/networking/Backoff$Scheduler;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v5, v0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    new-instance v5, Lcom/discord/rtcconnection/RtcConnection$State$d;

    const/4 v8, 0x0

    invoke-direct {v5, v8}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    iput-object v5, v0, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, v0, Lcom/discord/rtcconnection/RtcConnection;->i:Ljava/util/LinkedList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Created RtcConnection. GuildID: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " ChannelID: "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1, v4}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p13 .. p13}, Lcom/discord/utilities/networking/NetworkMonitor;->observeIsConnected()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1, v6}, Lrx/Observable;->N(I)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/h/b;

    invoke-direct {v2, v0}, Lf/a/h/b;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    new-instance v3, Lf/a/h/c;

    invoke-direct {v3, v0}, Lf/a/h/c;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    invoke-virtual {v1, v2, v3}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    new-instance v1, Lf/a/h/r;

    invoke-direct {v1, v0}, Lf/a/h/r;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    iput-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->u:Lf/a/h/r;

    new-instance v1, Lf/a/h/l;

    invoke-direct {v1, v0}, Lf/a/h/l;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    iput-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->v:Lf/a/h/l;

    return-void
.end method

.method public static d(Lcom/discord/rtcconnection/RtcConnection;ZLjava/lang/String;Ljava/lang/Throwable;ZI)V
    .locals 9

    and-int/lit8 v0, p5, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 p3, p5, 0x8

    if-eqz p3, :cond_1

    const/4 p4, 0x1

    :cond_1
    if-eqz p4, :cond_2

    iget-object v2, p0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v4, p2

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object p3, p0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object p4, p0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {p3, p4, p2, v5}, Lcom/discord/utilities/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    iget-object p3, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lf/a/h/v/h;->a()V

    :cond_3
    iput-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    iget-object p3, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz p3, :cond_4

    invoke-interface {p3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->i()V

    :cond_4
    iput-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    iget-object p3, p0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {p3}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    new-instance p3, Lcom/discord/rtcconnection/RtcConnection$State$d;

    invoke-direct {p3, p1}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    invoke-virtual {p0, p3}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lcom/discord/rtcconnection/RtcConnection;->k()V

    goto :goto_2

    :cond_5
    sget-object p1, Lf/a/h/j;->d:Lf/a/h/j;

    invoke-virtual {p0, p1}, Lcom/discord/rtcconnection/RtcConnection;->j(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p0, p2}, Lcom/discord/rtcconnection/RtcConnection;->c(Ljava/lang/String;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "hostname"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "port"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object p1
.end method

.method public final b(Lcom/discord/rtcconnection/RtcConnection$b;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lf/a/h/u/a;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {v0}, Lf/a/h/u/a;->d()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    instance-of v1, v1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    const/4 v2, 0x0

    if-nez v1, :cond_3

    invoke-virtual {p0, v2, p1}, Lcom/discord/rtcconnection/RtcConnection;->h(ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    instance-of v3, v1, Lcom/discord/rtcconnection/RtcConnection$c$b;

    if-eqz v3, :cond_3

    check-cast v1, Lcom/discord/rtcconnection/RtcConnection$c$b;

    iget-wide v3, v1, Lcom/discord/rtcconnection/RtcConnection$c$b;->a:J

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lf/a/h/v/h;->g:Lf/a/h/v/j;

    if-eqz v1, :cond_1

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lf/a/h/v/j;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    check-cast v3, Lcom/discord/rtcconnection/RtcConnection$c$b;

    iget-wide v3, v3, Lcom/discord/rtcconnection/RtcConnection$c$b;->a:J

    invoke-virtual {p0, v3, v4, v1}, Lcom/discord/rtcconnection/RtcConnection;->f(JLjava/util/Map;)V

    :cond_1
    iget-wide v3, p0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz v1, :cond_2

    iget-object v1, v1, Lf/a/h/v/h;->g:Lf/a/h/v/j;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lf/a/h/v/j;->c()Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    invoke-virtual {p0, v3, v4, v1}, Lcom/discord/rtcconnection/RtcConnection;->g(JLjava/util/Map;)V

    :cond_3
    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->r:Lrx/Subscription;

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lrx/Subscription;->unsubscribe()V

    :cond_4
    iput-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->r:Lrx/Subscription;

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->i()V

    :cond_5
    iput-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    new-instance v0, Lcom/discord/rtcconnection/RtcConnection$State$d;

    invoke-direct {v0, v2}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    const-string v1, "Destroy internal RTC connection: "

    invoke-static {v1, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/rtcconnection/RtcConnection;->h:Z

    return-void
.end method

.method public final e(JLjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    const-string v2, "guild_id"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-wide v1, p0, Lcom/discord/rtcconnection/RtcConnection;->x:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel_id"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "sender_user_id"

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;->VIDEO_STREAM_ENDED:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    invoke-virtual {p0, p1, v0}, Lcom/discord/rtcconnection/RtcConnection;->i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    return-void
.end method

.method public final f(JLjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p3, :cond_0

    new-instance v0, Lkotlin/Pair;

    const-string v1, "participant_type"

    const-string v2, "receiver"

    invoke-direct {v0, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p3, v0}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p3

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/rtcconnection/RtcConnection;->e(JLjava/util/Map;)V

    :cond_0
    return-void
.end method

.method public final g(JLjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    instance-of v0, v0, Lcom/discord/rtcconnection/RtcConnection$c$b;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "streamer"

    goto :goto_0

    :cond_0
    const-string v0, "sender"

    :goto_0
    new-instance v1, Lkotlin/Pair;

    const-string v2, "participant_type"

    invoke-direct {v1, v2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p3, v0}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p3

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/rtcconnection/RtcConnection;->e(JLjava/util/Map;)V

    :cond_1
    return-void
.end method

.method public final h(ZLjava/lang/String;)V
    .locals 9

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/Pair;

    iget v1, p0, Lcom/discord/rtcconnection/RtcConnection;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    const-string v3, "ping_bad_count"

    invoke-direct {v2, v3, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    aput-object v2, v0, v1

    iget v2, p0, Lcom/discord/rtcconnection/RtcConnection;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string v4, "connect_count"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x1

    aput-object v3, v0, v2

    invoke-static {v0}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/rtcconnection/RtcConnection;->a(Ljava/util/Map;)Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v3, "reconnect"

    invoke-interface {v0, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    const-string p1, "reason"

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->i:Ljava/util/LinkedList;

    const-string p2, "$this$average"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const-wide/16 v3, 0x0

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->longValue()J

    move-result-wide v7

    long-to-double v7, v7

    add-double/2addr v3, v7

    add-int/lit8 p2, p2, 0x1

    if-ltz p2, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/h/f;->throwCountOverflow()V

    throw v6

    :cond_2
    if-nez p2, :cond_3

    const-wide/high16 p1, 0x7ff8000000000000L    # Double.NaN

    goto :goto_1

    :cond_3
    int-to-double p1, p2

    div-double p1, v3, p1

    :goto_1
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_6

    const v3, 0x7fffffff

    int-to-double v4, v3

    const/high16 v7, -0x80000000

    cmpl-double v8, p1, v4

    if-lez v8, :cond_4

    goto :goto_2

    :cond_4
    int-to-double v3, v7

    cmpg-double v5, p1, v3

    if-gez v5, :cond_5

    const/high16 v3, -0x80000000

    goto :goto_2

    :cond_5
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide p1

    long-to-int v3, p1

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "ping_average"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot round NaN value."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_3
    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->t:Ljava/lang/String;

    if-eqz p1, :cond_8

    const-string p2, "media_session_id"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz p1, :cond_b

    iget-object p1, p1, Lf/a/h/v/h;->a:Lco/discord/media_engine/Stats;

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object p2

    if-eqz p2, :cond_9

    invoke-virtual {p2}, Lco/discord/media_engine/OutboundRtpAudio;->getPacketsSent()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "packets_sent"

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lco/discord/media_engine/OutboundRtpAudio;->getPacketsLost()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string v3, "packets_sent_lost"

    invoke-interface {v0, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    invoke-virtual {p1}, Lco/discord/media_engine/Stats;->getInboundRtpAudio()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/discord/media_engine/InboundRtpAudio;

    invoke-virtual {v3}, Lco/discord/media_engine/InboundRtpAudio;->getPacketsLost()I

    move-result v4

    add-int/2addr p2, v4

    invoke-virtual {v3}, Lco/discord/media_engine/InboundRtpAudio;->getPacketsReceived()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_4

    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "packets_received"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "packets_received_lost"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz p1, :cond_b

    iget-object p1, p1, Lf/a/h/v/h;->f:Lco/discord/media_engine/VoiceQuality;

    if-eqz p1, :cond_b

    invoke-virtual {p1, v0}, Lco/discord/media_engine/VoiceQuality;->getDurationStats(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lco/discord/media_engine/VoiceQuality;->getMosStats(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lco/discord/media_engine/VoiceQuality;->getPacketStats(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lco/discord/media_engine/VoiceQuality;->getBufferStats(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lco/discord/media_engine/VoiceQuality;->getFrameOpStats(Ljava/util/Map;)V

    :cond_b
    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->p:Ljava/lang/Long;

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->E:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, p1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_5

    :cond_c
    move-object p1, v6

    :goto_5
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "duration"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->f:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;

    if-eqz p1, :cond_e

    iget-object v6, p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;->c:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo$Protocol;

    :cond_e
    if-eqz v6, :cond_11

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_10

    if-ne p1, v2, :cond_f

    const-string/jumbo p1, "tcp"

    goto :goto_6

    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_10
    const-string/jumbo p1, "udp"

    :goto_6
    const-string p2, "protocol"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;->VOICE_DISCONNECT:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    invoke-virtual {p0, p1, v0}, Lcom/discord/rtcconnection/RtcConnection;->i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    instance-of p1, p1, Lcom/discord/rtcconnection/RtcConnection$c$a;

    if-eqz p1, :cond_14

    iget-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz p1, :cond_14

    iget-object p1, p1, Lf/a/h/v/h;->g:Lf/a/h/v/j;

    if-eqz p1, :cond_14

    iget-object p2, p1, Lf/a/h/v/j;->f:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_12
    :goto_7
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lx/s/l;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0}, Lf/a/h/v/j;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/discord/rtcconnection/RtcConnection;->f(JLjava/util/Map;)V

    goto :goto_7

    :cond_13
    iget-wide v0, p0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    invoke-virtual {p1}, Lf/a/h/v/j;->c()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, v0, v1, p1}, Lcom/discord/rtcconnection/RtcConnection;->g(JLjava/util/Map;)V

    :cond_14
    return-void
.end method

.method public final i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->a:Ljava/lang/String;

    const-string v1, "rtc_connection_id"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$c$a;->a:Lcom/discord/rtcconnection/RtcConnection$c$a;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "default"

    goto :goto_0

    :cond_0
    instance-of v0, v0, Lcom/discord/rtcconnection/RtcConnection$c$b;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "stream"

    :goto_0
    const-string v1, "context"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->t:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v1, "media_session_id"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->H:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v1, "parent_media_session_id"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    new-instance v0, Lcom/discord/rtcconnection/RtcConnection$d;

    invoke-direct {v0, p1, p2}, Lcom/discord/rtcconnection/RtcConnection$d;-><init>(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lcom/discord/rtcconnection/RtcConnection;->j(Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final j(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/rtcconnection/RtcConnection$b;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final k()V
    .locals 3

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    const-string v2, "reconnect"

    invoke-virtual {v0, v2, v1}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/discord/rtcconnection/RtcConnection;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->E:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->o:Ljava/lang/Long;

    :cond_0
    iget v0, p0, Lcom/discord/rtcconnection/RtcConnection;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/discord/rtcconnection/RtcConnection;->q:I

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/h/u/a;->d()V

    invoke-virtual {v0}, Lf/a/h/u/a;->e()Z

    :cond_1
    return-void
.end method

.method public final l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->C:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    invoke-interface {v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz p1, :cond_0

    new-instance v1, Lf/a/h/s;

    invoke-direct {v1, p1}, Lf/a/h/s;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p1, v1

    :cond_0
    check-cast p1, Ljava/lang/Runnable;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public final m(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    instance-of v0, v0, Lcom/discord/rtcconnection/RtcConnection$c$b;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting screenshare "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {v0, p1, p2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->a(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->f()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final n(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    new-instance v0, Lcom/discord/rtcconnection/RtcConnection$e;

    invoke-direct {v0, p1}, Lcom/discord/rtcconnection/RtcConnection$e;-><init>(Lcom/discord/rtcconnection/RtcConnection$State;)V

    invoke-virtual {p0, v0}, Lcom/discord/rtcconnection/RtcConnection;->j(Lkotlin/jvm/functions/Function1;)V

    :cond_0
    return-void
.end method

.method public final o(JF)V
    .locals 1

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->d(JF)V

    :cond_0
    return-void
.end method
