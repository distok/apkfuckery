.class public final Lcom/discord/rtcconnection/socket/io/Payloads$Hello;
.super Ljava/lang/Object;
.source "Payloads.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/rtcconnection/socket/io/Payloads;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Hello"
.end annotation


# instance fields
.field private final heartbeatIntervalMs:J
    .annotation runtime Lf/h/d/v/b;
        value = "heartbeat_interval"
    .end annotation
.end field


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/rtcconnection/socket/io/Payloads$Hello;JILjava/lang/Object;)Lcom/discord/rtcconnection/socket/io/Payloads$Hello;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    iget-wide p1, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->copy(J)Lcom/discord/rtcconnection/socket/io/Payloads$Hello;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    return-wide v0
.end method

.method public final copy(J)Lcom/discord/rtcconnection/socket/io/Payloads$Hello;
    .locals 1

    new-instance v0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;

    invoke-direct {v0, p1, p2}, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;-><init>(J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;

    iget-wide v0, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    iget-wide v2, p1, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHeartbeatIntervalMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Hello(heartbeatIntervalMs="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->heartbeatIntervalMs:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
