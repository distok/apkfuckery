.class public final Lcom/discord/rtcconnection/RtcConnection$d;
.super Lx/m/c/k;
.source "RtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/rtcconnection/RtcConnection;->i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/RtcConnection$b;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $event:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

.field public final synthetic $properties:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$event:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    iput-object p2, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$properties:Ljava/util/Map;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$b;

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$event:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    iget-object v1, p0, Lcom/discord/rtcconnection/RtcConnection$d;->$properties:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Lcom/discord/rtcconnection/RtcConnection$b;->onAnalyticsEvent(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
