.class public abstract Lcom/discord/overlay/OverlayService;
.super Landroid/app/Service;
.source "OverlayService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/overlay/OverlayService$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/overlay/OverlayService$Companion;

.field public static final NOTIFICATION_ID:I = 0x16e2


# instance fields
.field public overlayManager:Lcom/discord/overlay/OverlayManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/overlay/OverlayService$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/overlay/OverlayService$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/overlay/OverlayService;->Companion:Lcom/discord/overlay/OverlayService$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private final attachBubbleToWindow(Landroid/content/Intent;)Z
    .locals 4

    invoke-virtual {p0, p1}, Lcom/discord/overlay/OverlayService;->createOverlayBubble(Landroid/content/Intent;)Lcom/discord/overlay/views/OverlayBubbleWrap;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    const/4 v1, 0x0

    const-string v2, "overlayManager"

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/discord/overlay/OverlayManager;->a(Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    new-instance v0, Lcom/discord/overlay/OverlayService$a;

    invoke-direct {v0, p0, p1}, Lcom/discord/overlay/OverlayService$a;-><init>(Lcom/discord/overlay/OverlayService;Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    if-nez p1, :cond_1

    new-instance p1, Lf/a/e/f/a;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "applicationContext"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lf/a/e/f/a;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "trashWrap"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, v0, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    iget-object v0, v0, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    invoke-virtual {p1}, Lf/a/e/f/a;->getWindowLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public abstract createNotification(Landroid/content/Intent;)Landroid/app/Notification;
.end method

.method public abstract createOverlayBubble(Landroid/content/Intent;)Lcom/discord/overlay/views/OverlayBubbleWrap;
.end method

.method public final getOverlayManager()Lcom/discord/overlay/OverlayManager;
    .locals 1

    iget-object v0, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "overlayManager"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.view.WindowManager"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Landroid/view/WindowManager;

    new-instance v1, Lcom/discord/overlay/OverlayManager;

    invoke-direct {v1, v0}, Lcom/discord/overlay/OverlayManager;-><init>(Landroid/view/WindowManager;)V

    iput-object v1, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    new-instance v0, Lcom/discord/overlay/OverlayService$b;

    invoke-direct {v0, p0}, Lcom/discord/overlay/OverlayService$b;-><init>(Lcom/discord/overlay/OverlayService;)V

    iput-object v0, v1, Lcom/discord/overlay/OverlayManager;->j:Lf/a/e/e;

    new-instance v0, Lcom/discord/overlay/OverlayService$c;

    invoke-direct {v0, p0}, Lcom/discord/overlay/OverlayService$c;-><init>(Lcom/discord/overlay/OverlayService;)V

    const-string v2, "<set-?>"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v1, Lcom/discord/overlay/OverlayManager;->h:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/overlay/OverlayManager;->close()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void

    :cond_0
    const-string v0, "overlayManager"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 0

    if-eqz p1, :cond_0

    move-object p2, p1

    goto :goto_0

    :cond_0
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    :goto_0
    invoke-direct {p0, p2}, Lcom/discord/overlay/OverlayService;->attachBubbleToWindow(Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    :goto_1
    invoke-virtual {p0, p1}, Lcom/discord/overlay/OverlayService;->createNotification(Landroid/content/Intent;)Landroid/app/Notification;

    move-result-object p1

    const/16 p2, 0x16e2

    invoke-virtual {p0, p2, p1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    :cond_2
    const/4 p1, 0x3

    return p1
.end method

.method public final setOverlayManager(Lcom/discord/overlay/OverlayManager;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/overlay/OverlayService;->overlayManager:Lcom/discord/overlay/OverlayManager;

    return-void
.end method
