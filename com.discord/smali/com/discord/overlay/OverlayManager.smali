.class public Lcom/discord/overlay/OverlayManager;
.super Ljava/lang/Object;
.source "OverlayManager.kt"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final d:Landroid/graphics/Rect;

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/discord/overlay/views/OverlayBubbleWrap;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/overlay/views/OverlayBubbleWrap;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lf/a/e/f/a;

.field public j:Lf/a/e/e;

.field public k:Landroid/view/View$OnTouchListener;

.field public l:Lcom/discord/overlay/views/OverlayBubbleWrap;

.field public final m:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 1

    const-string/jumbo v0, "windowManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/discord/overlay/OverlayManager;->d:Landroid/graphics/Rect;

    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->e:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->f:Ljava/util/List;

    sget-object p1, Lcom/discord/overlay/OverlayManager$b;->d:Lcom/discord/overlay/OverlayManager$b;

    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->g:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/overlay/OverlayManager$c;->d:Lcom/discord/overlay/OverlayManager$c;

    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->h:Lkotlin/jvm/functions/Function1;

    new-instance p1, Lcom/discord/overlay/OverlayManager$a;

    invoke-direct {p1, p0}, Lcom/discord/overlay/OverlayManager$a;-><init>(Lcom/discord/overlay/OverlayManager;)V

    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->k:Landroid/view/View$OnTouchListener;

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/overlay/views/OverlayBubbleWrap;)V
    .locals 2

    const-string v0, "bubble"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    invoke-virtual {p1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getWindowLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->e:Ljava/util/ArrayList;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->g:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Lcom/discord/overlay/views/OverlayBubbleWrap;)V
    .locals 14

    const-string v0, "bubble"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/overlay/OverlayManager;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getCenterX()I

    move-result v0

    iget-object v1, p0, Lcom/discord/overlay/OverlayManager;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    if-le v0, v1, :cond_0

    const v3, 0x7fffffff

    invoke-virtual {p1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getY()F

    move-result v0

    float-to-int v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lcom/discord/overlay/views/OverlayBubbleWrap;->d(Lcom/discord/overlay/views/OverlayBubbleWrap;IILandroid/graphics/Rect;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/high16 v9, -0x80000000

    invoke-virtual {p1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getY()F

    move-result v0

    float-to-int v10, v0

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v8, p1

    invoke-static/range {v8 .. v13}, Lcom/discord/overlay/views/OverlayBubbleWrap;->d(Lcom/discord/overlay/views/OverlayBubbleWrap;IILandroid/graphics/Rect;ILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final c(Lcom/discord/overlay/views/OverlayBubbleWrap;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->l:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/discord/overlay/OverlayManager;->l:Lcom/discord/overlay/views/OverlayBubbleWrap;

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lf/a/e/f/a;->a(Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    :cond_1
    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->j:Lf/a/e/e;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lf/a/e/e;->a(Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    :cond_2
    return-void
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->e:Ljava/util/ArrayList;

    new-instance v1, Lf/a/e/a;

    invoke-direct {v1, p0}, Lf/a/e/a;-><init>(Lcom/discord/overlay/OverlayManager;)V

    invoke-static {v0, v1}, Lx/h/f;->removeAll(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Z

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    invoke-interface {v1, v0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    return-void
.end method

.method public final d(Lcom/discord/overlay/views/OverlayBubbleWrap;)V
    .locals 1

    const-string v0, "bubble"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    invoke-interface {v0, p1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    iget-object v0, p0, Lcom/discord/overlay/OverlayManager;->h:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
