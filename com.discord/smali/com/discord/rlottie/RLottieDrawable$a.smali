.class public final Lcom/discord/rlottie/RLottieDrawable$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/rlottie/RLottieDrawable;-><init>(Landroid/content/Context;ILjava/lang/String;IIFZ[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lcom/discord/rlottie/RLottieDrawable$a;->d:I

    iput-object p2, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->d:I

    const-wide/16 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    packed-switch v0, :pswitch_data_0

    throw v4

    :pswitch_0
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iput-object v4, v0, Lcom/discord/rlottie/RLottieDrawable;->p:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/discord/rlottie/RLottieDrawable;->a(Lcom/discord/rlottie/RLottieDrawable;)V

    return-void

    :pswitch_1
    throw v4

    :pswitch_2
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v1, v0, Lcom/discord/rlottie/RLottieDrawable;->o:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    iget-wide v2, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget v4, v0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    iget v0, v0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    invoke-static {v1, v2, v3, v4, v0}, Lcom/discord/rlottie/RLottieDrawable$Companion;->b(Lcom/discord/rlottie/RLottieDrawable$Companion;JII)V

    sget-object v0, Lcom/discord/rlottie/RLottieDrawable;->M:Landroid/os/Handler;

    iget-object v1, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v1, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v1, v1, Lcom/discord/rlottie/RLottieDrawable;->H:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void

    :pswitch_3
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget-wide v3, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/rlottie/RLottieDrawable;->Q:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v1, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v2, v1, Lcom/discord/rlottie/RLottieDrawable;->J:Ljava/lang/Runnable;

    iput-object v2, v1, Lcom/discord/rlottie/RLottieDrawable;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    invoke-static {v0}, Lcom/discord/rlottie/RLottieDrawable;->a(Lcom/discord/rlottie/RLottieDrawable;)V

    return-void

    :pswitch_4
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iput-object v4, v0, Lcom/discord/rlottie/RLottieDrawable;->o:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/discord/rlottie/RLottieDrawable;->a(Lcom/discord/rlottie/RLottieDrawable;)V

    return-void

    :pswitch_5
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iput-boolean v3, v0, Lcom/discord/rlottie/RLottieDrawable;->u:Z

    invoke-virtual {v0}, Lcom/discord/rlottie/RLottieDrawable;->c()V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    invoke-static {v0}, Lcom/discord/rlottie/RLottieDrawable;->a(Lcom/discord/rlottie/RLottieDrawable;)V

    return-void

    :pswitch_6
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget-wide v5, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    cmp-long v0, v5, v1

    if-nez v0, :cond_2

    sget-object v0, Lcom/discord/rlottie/RLottieDrawable;->M:Landroid/os/Handler;

    iget-object v1, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v1, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v1, v1, Lcom/discord/rlottie/RLottieDrawable;->G:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_6

    :cond_2
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    const-string v1, "RLottieDrawable"

    if-nez v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_1
    iget v2, v0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    iget v5, v0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v2, "Error Loading Frame in Runnable"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_f

    :try_start_2
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_3
    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->j:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_4
    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->j:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    sget-object v6, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    iget-object v7, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    check-cast v7, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_5
    iget-wide v7, v7, Lcom/discord/rlottie/RLottieDrawable;->E:J

    invoke-static {v6, v7, v8, v5, v2}, Lcom/discord/rlottie/RLottieDrawable$Companion;->f(Lcom/discord/rlottie/RLottieDrawable$Companion;JLjava/lang/String;I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_6
    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_2

    :catch_0
    nop

    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v2, v0, Lcom/discord/rlottie/RLottieDrawable;->h:[Ljava/lang/Integer;

    const/4 v5, 0x0

    if-eqz v2, :cond_7

    sget-object v6, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    iget-wide v7, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    const-string v0, "$this$toIntArray"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, v2

    new-array v9, v0, [I

    const/4 v10, 0x0

    :goto_3
    if-ge v10, v0, :cond_6

    aget-object v11, v2, v10

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    aput v11, v9, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_6
    invoke-static {v6, v7, v8, v9}, Lcom/discord/rlottie/RLottieDrawable$Companion;->e(Lcom/discord/rlottie/RLottieDrawable$Companion;J[I)V

    :cond_7
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    iput-object v4, v0, Lcom/discord/rlottie/RLottieDrawable;->h:[Ljava/lang/Integer;

    :try_start_7
    sget-object v6, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    iget-wide v7, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_8
    iget v9, v0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    iget-object v10, v0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    invoke-static {v10}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_9
    iget v11, v0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    iget v12, v0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v13

    invoke-static/range {v6 .. v13}, Lcom/discord/rlottie/RLottieDrawable$Companion;->d(Lcom/discord/rlottie/RLottieDrawable$Companion;JILandroid/graphics/Bitmap;III)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_8

    sget-object v0, Lcom/discord/rlottie/RLottieDrawable;->M:Landroid/os/Handler;

    iget-object v2, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    check-cast v2, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_a
    iget-object v2, v2, Lcom/discord/rlottie/RLottieDrawable;->G:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_6

    :cond_8
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_b
    iget-object v2, v0, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    const/4 v4, 0x2

    aget v2, v2, v4

    if-eqz v2, :cond_9

    sget-object v2, Lcom/discord/rlottie/RLottieDrawable;->M:Landroid/os/Handler;

    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->K:Ljava/lang/Runnable;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_c
    iget-object v0, v0, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    aput v5, v0, v4

    :cond_9
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_d
    iget-object v2, v0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    iput-object v2, v0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_e
    iget-boolean v2, v0, Lcom/discord/rlottie/RLottieDrawable;->x:Z

    if-eqz v2, :cond_a

    goto :goto_4

    :cond_a
    const/4 v4, 0x1

    :goto_4
    iget v2, v0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    add-int/2addr v2, v4

    iget-object v4, v0, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    aget v4, v4, v5

    if-ge v2, v4, :cond_c

    iget-object v4, v0, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    sget-object v6, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->FREEZE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    if-ne v4, v6, :cond_b

    iput-boolean v3, v0, Lcom/discord/rlottie/RLottieDrawable;->n:Z

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_f
    iget v2, v0, Lcom/discord/rlottie/RLottieDrawable;->l:I

    add-int/2addr v2, v3

    iput v2, v0, Lcom/discord/rlottie/RLottieDrawable;->l:I

    goto :goto_5

    :cond_b
    iput v2, v0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    iput-boolean v5, v0, Lcom/discord/rlottie/RLottieDrawable;->n:Z

    goto :goto_5

    :cond_c
    iget-object v2, v0, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    sget-object v4, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->LOOP:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    if-ne v2, v4, :cond_d

    iput v5, v0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    iput-boolean v5, v0, Lcom/discord/rlottie/RLottieDrawable;->n:Z

    goto :goto_5

    :cond_d
    sget-object v4, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->ONCE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    if-ne v2, v4, :cond_e

    iput v5, v0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    iput-boolean v3, v0, Lcom/discord/rlottie/RLottieDrawable;->n:Z

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    check-cast v0, Lcom/discord/rlottie/RLottieDrawable;

    :try_start_10
    iget v2, v0, Lcom/discord/rlottie/RLottieDrawable;->l:I

    add-int/2addr v2, v3

    iput v2, v0, Lcom/discord/rlottie/RLottieDrawable;->l:I

    goto :goto_5

    :cond_e
    iput-boolean v3, v0, Lcom/discord/rlottie/RLottieDrawable;->n:Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    const-string v2, "Error loading frame"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_f
    :goto_5
    sget-object v0, Lcom/discord/rlottie/RLottieDrawable;->M:Landroid/os/Handler;

    iget-object v1, p0, Lcom/discord/rlottie/RLottieDrawable$a;->e:Ljava/lang/Object;

    check-cast v1, Lcom/discord/rlottie/RLottieDrawable;

    iget-object v1, v1, Lcom/discord/rlottie/RLottieDrawable;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
