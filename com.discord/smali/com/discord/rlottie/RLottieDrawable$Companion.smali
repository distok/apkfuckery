.class public final Lcom/discord/rlottie/RLottieDrawable$Companion;
.super Ljava/lang/Object;
.source "RLottieDrawable.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/rlottie/RLottieDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lcom/discord/rlottie/RLottieDrawable$Companion;Ljava/lang/String;II[IZ[IZ)J
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/discord/rlottie/RLottieDrawable$Companion;->create(Ljava/lang/String;II[IZ[IZ)J

    move-result-wide p0

    return-wide p0
.end method

.method public static final synthetic b(Lcom/discord/rlottie/RLottieDrawable$Companion;JII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/rlottie/RLottieDrawable$Companion;->createCache(JII)V

    return-void
.end method

.method public static final synthetic c(Lcom/discord/rlottie/RLottieDrawable$Companion;Ljava/lang/String;Ljava/lang/String;[I[I)J
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/rlottie/RLottieDrawable$Companion;->createWithJson(Ljava/lang/String;Ljava/lang/String;[I[I)J

    move-result-wide p0

    return-wide p0
.end method

.method private final native create(Ljava/lang/String;II[IZ[IZ)J
.end method

.method private final native createCache(JII)V
.end method

.method private final native createWithJson(Ljava/lang/String;Ljava/lang/String;[I[I)J
.end method

.method public static final synthetic d(Lcom/discord/rlottie/RLottieDrawable$Companion;JILandroid/graphics/Bitmap;III)I
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/discord/rlottie/RLottieDrawable$Companion;->getFrame(JILandroid/graphics/Bitmap;III)I

    move-result p0

    return p0
.end method

.method private final native destroy(J)V
.end method

.method public static final synthetic e(Lcom/discord/rlottie/RLottieDrawable$Companion;J[I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/rlottie/RLottieDrawable$Companion;->replaceColors(J[I)V

    return-void
.end method

.method public static final synthetic f(Lcom/discord/rlottie/RLottieDrawable$Companion;JLjava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/rlottie/RLottieDrawable$Companion;->setLayerColor(JLjava/lang/String;I)V

    return-void
.end method

.method private final native getFrame(JILandroid/graphics/Bitmap;III)I
.end method

.method private final native replaceColors(J[I)V
.end method

.method private final native setLayerColor(JLjava/lang/String;I)V
.end method
