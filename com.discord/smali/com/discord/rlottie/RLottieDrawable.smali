.class public final Lcom/discord/rlottie/RLottieDrawable;
.super Landroid/graphics/drawable/BitmapDrawable;
.source "RLottieDrawable.kt"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;,
        Lcom/discord/rlottie/RLottieDrawable$Companion;
    }
.end annotation


# static fields
.field public static final M:Landroid/os/Handler;

.field public static N:[B

.field public static final O:[B

.field public static final P:Ljava/util/concurrent/ExecutorService;

.field public static Q:Ljava/util/concurrent/ThreadPoolExecutor;

.field public static final R:Lcom/discord/rlottie/RLottieDrawable$Companion;


# instance fields
.field public A:F

.field public B:Z

.field public final C:Landroid/graphics/Rect;

.field public volatile D:Z

.field public volatile E:J

.field public final F:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field public final G:Ljava/lang/Runnable;

.field public final H:Ljava/lang/Runnable;

.field public final I:Ljava/lang/Runnable;

.field public final J:Ljava/lang/Runnable;

.field public final K:Ljava/lang/Runnable;

.field public final L:Ljava/lang/Runnable;

.field public d:I

.field public e:I

.field public final f:[I

.field public g:I

.field public h:[Ljava/lang/Integer;

.field public final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public volatile j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

.field public l:I

.field public m:J

.field public volatile n:Z

.field public o:Ljava/lang/Runnable;

.field public p:Ljava/lang/Runnable;

.field public volatile q:Landroid/graphics/Bitmap;

.field public volatile r:Landroid/graphics/Bitmap;

.field public volatile s:Landroid/graphics/Bitmap;

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:I

.field public x:Z

.field public y:F

.field public z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/rlottie/RLottieDrawable$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/rlottie/RLottieDrawable$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/discord/rlottie/RLottieDrawable;->M:Landroid/os/Handler;

    const/high16 v0, 0x10000

    new-array v0, v0, [B

    sput-object v0, Lcom/discord/rlottie/RLottieDrawable;->N:[B

    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, Lcom/discord/rlottie/RLottieDrawable;->O:[B

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/discord/rlottie/RLottieDrawable;->P:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;IIFZ[I)V
    .locals 12
    .param p2    # I
        .annotation build Landroidx/annotation/RawRes;
        .end annotation
    .end param

    move-object v1, p0

    move-object v0, p3

    const-string v2, "context"

    move-object v3, p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "name"

    invoke-static {p3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    const/4 v2, 0x3

    new-array v4, v2, [I

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->i:Ljava/util/HashMap;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->j:Ljava/util/HashMap;

    sget-object v4, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->LOOP:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    const/high16 v4, 0x42700000    # 60.0f

    iput v4, v1, Lcom/discord/rlottie/RLottieDrawable;->y:F

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v1, Lcom/discord/rlottie/RLottieDrawable;->z:F

    iput v4, v1, Lcom/discord/rlottie/RLottieDrawable;->A:F

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->C:Landroid/graphics/Rect;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    new-instance v4, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v5, 0x6

    invoke-direct {v4, v5, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->G:Ljava/lang/Runnable;

    new-instance v4, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v5, 0x2

    invoke-direct {v4, v5, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->H:Ljava/lang/Runnable;

    new-instance v4, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v6, 0x1

    invoke-direct {v4, v6, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->I:Ljava/lang/Runnable;

    new-instance v4, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v7, 0x4

    invoke-direct {v4, v7, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->J:Ljava/lang/Runnable;

    new-instance v4, Lcom/discord/rlottie/RLottieDrawable$a;

    invoke-direct {v4, v2, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v4, v1, Lcom/discord/rlottie/RLottieDrawable;->K:Ljava/lang/Runnable;

    new-instance v2, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v4, 0x0

    invoke-direct {v2, v4, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v2, v1, Lcom/discord/rlottie/RLottieDrawable;->L:Ljava/lang/Runnable;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move v3, p2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    const-string v3, "context.resources.openRawResource(rawRes)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_0
    sget-object v7, Lcom/discord/rlottie/RLottieDrawable;->O:[B

    array-length v8, v7

    invoke-virtual {v2, v7, v4, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    if-lez v8, :cond_1

    sget-object v9, Lcom/discord/rlottie/RLottieDrawable;->N:[B

    array-length v10, v9

    add-int v11, v3, v8

    if-ge v10, v11, :cond_0

    array-length v10, v9

    mul-int/lit8 v10, v10, 0x2

    new-array v10, v10, [B

    invoke-static {v9, v4, v10, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sput-object v10, Lcom/discord/rlottie/RLottieDrawable;->N:[B

    :cond_0
    sget-object v9, Lcom/discord/rlottie/RLottieDrawable;->N:[B

    invoke-static {v7, v4, v9, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v3, v11

    goto :goto_0

    :cond_1
    sget-object v7, Lcom/discord/rlottie/RLottieDrawable;->N:[B

    new-instance v8, Ljava/lang/String;

    sget-object v9, Lx/s/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v8, v7, v4, v3, v9}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    move/from16 v2, p4

    iput v2, v1, Lcom/discord/rlottie/RLottieDrawable;->d:I

    move/from16 v2, p5

    iput v2, v1, Lcom/discord/rlottie/RLottieDrawable;->e:I

    move/from16 v2, p6

    iput v2, v1, Lcom/discord/rlottie/RLottieDrawable;->y:F

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    const-string v3, "paint"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFlags(I)V

    sget-object v2, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    iget-object v3, v1, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    move-object/from16 v4, p8

    invoke-static {v2, v8, p3, v3, v4}, Lcom/discord/rlottie/RLottieDrawable$Companion;->c(Lcom/discord/rlottie/RLottieDrawable$Companion;Ljava/lang/String;Ljava/lang/String;[I[I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/discord/rlottie/RLottieDrawable;->E:J

    const/16 v0, 0x10

    const/high16 v2, 0x447a0000    # 1000.0f

    iget-object v3, v1, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    aget v3, v3, v6

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Lcom/discord/rlottie/RLottieDrawable;->g:I

    sget-object v0, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->LOOP:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    iput-object v0, v1, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    if-eqz p7, :cond_2

    invoke-virtual {p0, v6}, Lcom/discord/rlottie/RLottieDrawable;->e(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    const-string v2, "RLottieDrawable"

    const-string v3, "Error Constructing"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    return-void
.end method

.method public constructor <init>(Ljava/io/File;IIZZF[II)V
    .locals 14

    move-object v0, p0

    const-string v1, "file"

    move-object v2, p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    const/4 v1, 0x3

    new-array v11, v1, [I

    iput-object v11, v0, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->i:Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->j:Ljava/util/HashMap;

    sget-object v3, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->LOOP:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    const/high16 v3, 0x42700000    # 60.0f

    iput v3, v0, Lcom/discord/rlottie/RLottieDrawable;->y:F

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, v0, Lcom/discord/rlottie/RLottieDrawable;->z:F

    iput v3, v0, Lcom/discord/rlottie/RLottieDrawable;->A:F

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->C:Landroid/graphics/Rect;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    new-instance v3, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v4, 0x6

    invoke-direct {v3, v4, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->G:Ljava/lang/Runnable;

    new-instance v3, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v4, 0x2

    invoke-direct {v3, v4, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->H:Ljava/lang/Runnable;

    new-instance v3, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v12, 0x1

    invoke-direct {v3, v12, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->I:Ljava/lang/Runnable;

    new-instance v3, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v5, 0x4

    invoke-direct {v3, v5, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->J:Ljava/lang/Runnable;

    new-instance v3, Lcom/discord/rlottie/RLottieDrawable$a;

    invoke-direct {v3, v1, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v3, v0, Lcom/discord/rlottie/RLottieDrawable;->K:Ljava/lang/Runnable;

    new-instance v1, Lcom/discord/rlottie/RLottieDrawable$a;

    const/4 v13, 0x0

    invoke-direct {v1, v13, p0}, Lcom/discord/rlottie/RLottieDrawable$a;-><init>(ILjava/lang/Object;)V

    iput-object v1, v0, Lcom/discord/rlottie/RLottieDrawable;->L:Ljava/lang/Runnable;

    move/from16 v1, p2

    iput v1, v0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    move/from16 v6, p3

    iput v6, v0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    move/from16 v3, p5

    iput-boolean v3, v0, Lcom/discord/rlottie/RLottieDrawable;->x:Z

    move/from16 v3, p6

    iput v3, v0, Lcom/discord/rlottie/RLottieDrawable;->y:F

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    const-string v5, "paint"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFlags(I)V

    sget-object v3, Lcom/discord/rlottie/RLottieDrawable;->R:Lcom/discord/rlottie/RLottieDrawable$Companion;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file.absolutePath"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v10, v0, Lcom/discord/rlottie/RLottieDrawable;->x:Z

    const/4 v9, 0x0

    move/from16 v5, p2

    move-object v7, v11

    move/from16 v8, p4

    invoke-static/range {v3 .. v10}, Lcom/discord/rlottie/RLottieDrawable$Companion;->a(Lcom/discord/rlottie/RLottieDrawable$Companion;Ljava/lang/String;II[IZ[IZ)J

    move-result-wide v3

    iput-wide v3, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    if-eqz p4, :cond_0

    sget-object v1, Lcom/discord/rlottie/RLottieDrawable;->Q:Ljava/util/concurrent/ThreadPoolExecutor;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-wide/16 v5, 0x0

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object/from16 p2, v1

    move/from16 p3, v3

    move/from16 p4, v4

    move-wide/from16 p5, v5

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    invoke-direct/range {p2 .. p8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    sput-object v1, Lcom/discord/rlottie/RLottieDrawable;->Q:Ljava/util/concurrent/ThreadPoolExecutor;

    :cond_0
    iget-wide v3, v0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_1
    iget-boolean v1, v0, Lcom/discord/rlottie/RLottieDrawable;->x:Z

    if-eqz v1, :cond_2

    aget v1, v11, v12

    const/16 v2, 0x3c

    if-ge v1, v2, :cond_2

    iput-boolean v13, v0, Lcom/discord/rlottie/RLottieDrawable;->x:Z

    :cond_2
    iget-boolean v1, v0, Lcom/discord/rlottie/RLottieDrawable;->x:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x21

    goto :goto_0

    :cond_3
    const/16 v1, 0x10

    :goto_0
    const/high16 v2, 0x447a0000    # 1000.0f

    aget v3, v11, v12

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/discord/rlottie/RLottieDrawable;->g:I

    return-void
.end method

.method public static final a(Lcom/discord/rlottie/RLottieDrawable;)V
    .locals 5

    iget-wide v0, p0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    :cond_2
    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 4

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    const/4 v2, 0x0

    if-lez v0, :cond_2

    iget-object v3, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_1

    return v1

    :cond_1
    iget-object v3, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->invalidateSelf()V

    :cond_2
    return-void
.end method

.method public final d()Z
    .locals 5

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->p:Ljava/lang/Runnable;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->t:Z

    if-eqz v0, :cond_2

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->u:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->i:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->j:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/discord/rlottie/RLottieDrawable;->i:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_1
    sget-object v0, Lcom/discord/rlottie/RLottieDrawable;->P:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/discord/rlottie/RLottieDrawable;->L:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/discord/rlottie/RLottieDrawable;->p:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return v1

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 14

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/discord/rlottie/RLottieDrawable;->E:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_10

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/discord/rlottie/RLottieDrawable;->m:J

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    iget v4, p0, Lcom/discord/rlottie/RLottieDrawable;->y:F

    const/16 v5, 0x3c

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/discord/rlottie/RLottieDrawable;->g:I

    add-int/lit8 v4, v4, -0x6

    goto :goto_0

    :cond_0
    iget v4, p0, Lcom/discord/rlottie/RLottieDrawable;->g:I

    :goto_0
    iget-boolean v6, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    const-wide/16 v7, 0x10

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    if-nez v6, :cond_1

    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    goto/16 :goto_6

    :cond_1
    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_2

    int-to-long v12, v4

    cmp-long v6, v2, v12

    if-ltz v6, :cond_d

    :cond_2
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v6

    if-eqz v6, :cond_3

    goto :goto_2

    :cond_3
    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    :goto_1
    if-lez v6, :cond_6

    iget-object v12, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/ref/WeakReference;

    invoke-virtual {v12}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v12

    if-nez v12, :cond_4

    iget-object v12, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    :cond_4
    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->F:Ljava/util/ArrayList;

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    if-nez v6, :cond_5

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v6, 0x1

    :goto_3
    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    iput-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    iput-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    iget-boolean v6, p0, Lcom/discord/rlottie/RLottieDrawable;->n:Z

    if-eqz v6, :cond_7

    iput-boolean v11, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    :cond_7
    iput-object v10, p0, Lcom/discord/rlottie/RLottieDrawable;->p:Ljava/lang/Runnable;

    iput-boolean v9, p0, Lcom/discord/rlottie/RLottieDrawable;->u:Z

    iput-object v10, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    iget v6, p0, Lcom/discord/rlottie/RLottieDrawable;->y:F

    cmpg-float v5, v6, v5

    if-gtz v5, :cond_8

    goto :goto_4

    :cond_8
    int-to-long v4, v4

    sub-long/2addr v2, v4

    invoke-static {v7, v8, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    sub-long/2addr v0, v2

    :goto_4
    iput-wide v0, p0, Lcom/discord/rlottie/RLottieDrawable;->m:J

    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    goto :goto_6

    :cond_9
    iget-boolean v6, p0, Lcom/discord/rlottie/RLottieDrawable;->v:Z

    if-nez v6, :cond_a

    iget-boolean v6, p0, Lcom/discord/rlottie/RLottieDrawable;->t:Z

    if-eqz v6, :cond_d

    int-to-long v12, v4

    cmp-long v6, v2, v12

    if-ltz v6, :cond_d

    :cond_a
    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    iput-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->s:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    iput-object v6, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    iput-object v10, p0, Lcom/discord/rlottie/RLottieDrawable;->p:Ljava/lang/Runnable;

    iput-boolean v9, p0, Lcom/discord/rlottie/RLottieDrawable;->u:Z

    iput-object v10, p0, Lcom/discord/rlottie/RLottieDrawable;->r:Landroid/graphics/Bitmap;

    iget v6, p0, Lcom/discord/rlottie/RLottieDrawable;->y:F

    cmpg-float v5, v6, v5

    if-gtz v5, :cond_b

    goto :goto_5

    :cond_b
    int-to-long v4, v4

    sub-long/2addr v2, v4

    invoke-static {v7, v8, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    sub-long/2addr v0, v2

    :goto_5
    iput-wide v0, p0, Lcom/discord/rlottie/RLottieDrawable;->m:J

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->v:Z

    if-eqz v0, :cond_c

    iput-boolean v11, p0, Lcom/discord/rlottie/RLottieDrawable;->u:Z

    iput-boolean v11, p0, Lcom/discord/rlottie/RLottieDrawable;->v:Z

    :cond_c
    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    :cond_d
    :goto_6
    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->B:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/discord/rlottie/RLottieDrawable;->z:F

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/discord/rlottie/RLottieDrawable;->A:F

    iput-boolean v11, p0, Lcom/discord/rlottie/RLottieDrawable;->B:Z

    :cond_e
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->C:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->z:F

    iget v1, p0, Lcom/discord/rlottie/RLottieDrawable;->A:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->q:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->c()V

    :cond_f
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_10
    return-void
.end method

.method public final e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/rlottie/RLottieDrawable;->t:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    :cond_0
    return-void
.end method

.method public final f(Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    sget-object v1, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->ONCE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->FREEZE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    return v0
.end method

.method public getMinimumHeight()I
    .locals 1

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->e:I

    return v0
.end method

.method public getMinimumWidth()I
    .locals 1

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->d:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x2

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    return v0
.end method

.method public onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "bounds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/graphics/drawable/BitmapDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/rlottie/RLottieDrawable;->B:Z

    return-void
.end method

.method public start()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/rlottie/RLottieDrawable;->k:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    sget-object v1, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->ONCE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    invoke-virtual {v0, v1}, Ljava/lang/Enum;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/discord/rlottie/RLottieDrawable;->l:I

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    invoke-virtual {p0}, Lcom/discord/rlottie/RLottieDrawable;->c()V

    :cond_1
    :goto_0
    return-void
.end method

.method public stop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    return-void
.end method
