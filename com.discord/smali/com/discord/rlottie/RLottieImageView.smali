.class public final Lcom/discord/rlottie/RLottieImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "RLottieImageView.kt"


# instance fields
.field public d:Lcom/discord/rlottie/RLottieDrawable;

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object p1, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->FREEZE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/discord/rlottie/RLottieImageView;->f:Z

    iget-boolean v1, p0, Lcom/discord/rlottie/RLottieImageView;->e:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/rlottie/RLottieDrawable;->start()V

    :cond_1
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/rlottie/RLottieImageView;->e:Z

    iget-boolean v0, p0, Lcom/discord/rlottie/RLottieImageView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/rlottie/RLottieDrawable;->start()V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/rlottie/RLottieImageView;->e:Z

    iget-object v1, p0, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-eqz v1, :cond_0

    iput-boolean v0, v1, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    :cond_0
    return-void
.end method

.method public final setPlaybackMode(Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;)V
    .locals 1

    const-string v0, "playbackMode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/rlottie/RLottieDrawable;->f(Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;)V

    :cond_0
    return-void
.end method

.method public final setProgress(F)V
    .locals 4

    iget-object v0, p0, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v3, p1, v1

    if-gez v3, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    cmpl-float v1, p1, v2

    if-lez v1, :cond_1

    const/high16 p1, 0x3f800000    # 1.0f

    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/discord/rlottie/RLottieDrawable;->f:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    int-to-float v1, v1

    mul-float v1, v1, p1

    float-to-int p1, v1

    iput p1, v0, Lcom/discord/rlottie/RLottieDrawable;->w:I

    iput-boolean v2, v0, Lcom/discord/rlottie/RLottieDrawable;->n:Z

    iput-boolean v2, v0, Lcom/discord/rlottie/RLottieDrawable;->u:Z

    invoke-virtual {v0}, Lcom/discord/rlottie/RLottieDrawable;->d()Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    iput-boolean p1, v0, Lcom/discord/rlottie/RLottieDrawable;->v:Z

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->invalidateSelf()V

    :cond_3
    return-void
.end method
