.class public final Lcom/discord/views/OAuthPermissionViews;
.super Ljava/lang/Object;
.source "OAuthPermissionViews.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/OAuthPermissionViews$InvalidScopeException;,
        Lcom/discord/views/OAuthPermissionViews$a;
    }
.end annotation


# direct methods
.method public static final a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$setScopePermissionText"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string v0, "applications.builds.read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215b1

    goto/16 :goto_0

    :sswitch_1
    const-string v0, "applications.builds.upload"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215b3

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "connections"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215be

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "applications.store.update"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215b9

    goto/16 :goto_0

    :sswitch_4
    const-string v0, "gdm.join"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215c2

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "email"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215c0

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "rpc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215cd

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "bot"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215bb

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "identify"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215c8

    goto :goto_0

    :sswitch_9
    const-string v0, "activities.read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215ad

    goto :goto_0

    :sswitch_a
    const-string v0, "messages.read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215c9

    goto :goto_0

    :sswitch_b
    const-string v0, "rpc.notifications.read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215cf

    goto :goto_0

    :sswitch_c
    const-string v0, "applications.entitlements"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215b7

    goto :goto_0

    :sswitch_d
    const-string v0, "guilds"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215c4

    goto :goto_0

    :sswitch_e
    const-string v0, "guilds.join"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215c6

    goto :goto_0

    :sswitch_f
    const-string v0, "activities.write"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215af

    goto :goto_0

    :sswitch_10
    const-string v0, "relationships.read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, 0x7f1215cb

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_0
    :goto_1
    new-instance p0, Lcom/discord/views/OAuthPermissionViews$InvalidScopeException;

    invoke-direct {p0, p1}, Lcom/discord/views/OAuthPermissionViews$InvalidScopeException;-><init>(Ljava/lang/String;)V

    throw p0

    :sswitch_data_0
    .sparse-switch
        -0x71f43197 -> :sswitch_10
        -0x6816e562 -> :sswitch_f
        -0x5e746088 -> :sswitch_e
        -0x499abd20 -> :sswitch_d
        -0x4824902f -> :sswitch_c
        -0x478c367b -> :sswitch_b
        -0x2a0e7f08 -> :sswitch_a
        -0x1c243d09 -> :sswitch_9
        -0x81790f4 -> :sswitch_8
        0x17dc7 -> :sswitch_7
        0x1b9e5 -> :sswitch_6
        0x5c24b9c -> :sswitch_5
        0x32428528 -> :sswitch_4
        0x476a8d61 -> :sswitch_3
        0x66cb5d55 -> :sswitch_2
        0x6898fabf -> :sswitch_1
        0x77fa5dd4 -> :sswitch_0
    .end sparse-switch
.end method
