.class public final Lcom/discord/views/typing/TypingDot;
.super Landroid/view/View;
.source "TypingDot.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/typing/TypingDot$a;
    }
.end annotation


# instance fields
.field public final d:Landroid/view/animation/Animation;

.field public final e:Landroid/view/animation/Animation;

.field public f:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "context"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f010034

    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object p2

    const-string v0, "AnimationUtils.loadAnima\u2026nim_typing_dots_scale_up)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/views/typing/TypingDot;->d:Landroid/view/animation/Animation;

    const p2, 0x7f010033

    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object p1

    const-string p2, "AnimationUtils.loadAnima\u2026m_typing_dots_scale_down)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/typing/TypingDot;->e:Landroid/view/animation/Animation;

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/typing/TypingDot;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, p1, p2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object p1, p0, Lcom/discord/views/typing/TypingDot;->d:Landroid/view/animation/Animation;

    new-instance p2, Lcom/discord/views/typing/TypingDot$a;

    new-instance v0, Lcom/discord/views/typing/TypingDot$b;

    invoke-direct {v0, p0}, Lcom/discord/views/typing/TypingDot$b;-><init>(Lcom/discord/views/typing/TypingDot;)V

    invoke-direct {p2, v0}, Lcom/discord/views/typing/TypingDot$a;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object p1, p0, Lcom/discord/views/typing/TypingDot;->e:Landroid/view/animation/Animation;

    new-instance p2, Lcom/discord/views/typing/TypingDot$a;

    new-instance v0, Lcom/discord/views/typing/TypingDot$c;

    invoke-direct {v0, p0}, Lcom/discord/views/typing/TypingDot$c;-><init>(Lcom/discord/views/typing/TypingDot;)V

    invoke-direct {p2, v0}, Lcom/discord/views/typing/TypingDot$a;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object p1, p0, Lcom/discord/views/typing/TypingDot;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final getOnScaleDownCompleteListener()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/typing/TypingDot;->f:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/discord/views/typing/TypingDot;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    iget-object v0, p0, Lcom/discord/views/typing/TypingDot;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    return-void
.end method

.method public final setOnScaleDownCompleteListener(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/views/typing/TypingDot;->f:Lkotlin/jvm/functions/Function0;

    return-void
.end method
