.class public final Lcom/discord/views/typing/TypingDots;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "TypingDots.kt"


# static fields
.field public static final synthetic h:I


# instance fields
.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/discord/views/typing/TypingDot;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:J

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const p2, 0x7f0d0104

    const/4 v1, 0x1

    invoke-virtual {p1, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b0002

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/discord/views/typing/TypingDots;->e:I

    int-to-double p1, p1

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    div-double/2addr p1, v2

    double-to-long p1, p1

    iput-wide p1, p0, Lcom/discord/views/typing/TypingDots;->f:J

    const/4 p1, 0x3

    new-array p1, p1, [Lcom/discord/views/typing/TypingDot;

    const p2, 0x7f0a0b74

    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v2, "findViewById(R.id.view_typing_dots_1)"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/views/typing/TypingDot;

    aput-object p2, p1, v0

    const p2, 0x7f0a0b75

    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "findViewById(R.id.view_typing_dots_2)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/views/typing/TypingDot;

    aput-object p2, p1, v1

    const p2, 0x7f0a0b76

    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "findViewById(R.id.view_typing_dots_3)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/views/typing/TypingDot;

    const/4 v0, 0x2

    aput-object p2, p1, v0

    invoke-static {p1}, Lx/h/f;->arrayListOf([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/typing/TypingDots;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Lx/h/f;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/typing/TypingDot;

    new-instance p2, Lf/a/n/k0/a;

    invoke-direct {p2, p0}, Lf/a/n/k0/a;-><init>(Lcom/discord/views/typing/TypingDots;)V

    invoke-virtual {p1, p2}, Lcom/discord/views/typing/TypingDot;->setOnScaleDownCompleteListener(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    iget-boolean v0, p0, Lcom/discord/views/typing/TypingDots;->g:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-wide v3, p0, Lcom/discord/views/typing/TypingDots;->f:J

    goto :goto_1

    :cond_2
    const-wide/16 v3, 0x0

    :goto_1
    iget-object p1, p0, Lcom/discord/views/typing/TypingDots;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/typing/TypingDot;

    invoke-virtual {p1, v3, v4}, Lcom/discord/views/typing/TypingDot;->a(J)V

    iget-object p1, p0, Lcom/discord/views/typing/TypingDots;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/typing/TypingDot;

    iget-wide v0, p0, Lcom/discord/views/typing/TypingDots;->f:J

    add-long/2addr v0, v3

    invoke-virtual {p1, v0, v1}, Lcom/discord/views/typing/TypingDot;->a(J)V

    iget-object p1, p0, Lcom/discord/views/typing/TypingDots;->d:Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/typing/TypingDot;

    iget-wide v0, p0, Lcom/discord/views/typing/TypingDots;->f:J

    add-long/2addr v3, v0

    add-long/2addr v3, v0

    invoke-virtual {p1, v3, v4}, Lcom/discord/views/typing/TypingDot;->a(J)V

    iput-boolean v2, p0, Lcom/discord/views/typing/TypingDots;->g:Z

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/discord/views/typing/TypingDots;->d:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/views/typing/TypingDot;

    iget-object v2, v1, Lcom/discord/views/typing/TypingDot;->d:Landroid/view/animation/Animation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->cancel()V

    iget-object v1, v1, Lcom/discord/views/typing/TypingDot;->e:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/views/typing/TypingDots;->g:Z

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/discord/views/typing/TypingDots;->b()V

    return-void
.end method
