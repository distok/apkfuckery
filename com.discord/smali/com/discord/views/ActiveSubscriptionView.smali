.class public final Lcom/discord/views/ActiveSubscriptionView;
.super Landroid/widget/FrameLayout;
.source "ActiveSubscriptionView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;
    }
.end annotation


# static fields
.field public static final synthetic l:I


# instance fields
.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/ImageView;

.field public final f:Landroid/widget/ImageView;

.field public final g:Landroid/widget/ImageView;

.field public final h:Landroid/widget/Button;

.field public final i:Landroid/widget/Button;

.field public final j:Landroid/view/View;

.field public final k:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "context"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f0d0115

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f0a0059

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active_subscription_header_text)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->d:Landroid/widget/TextView;

    const p1, 0x7f0a0056

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active\u2026iption_header_background)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->e:Landroid/widget/ImageView;

    const p1, 0x7f0a0057

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active_subscription_header_icon)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->f:Landroid/widget/ImageView;

    const p1, 0x7f0a0058

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active_subscription_header_logo)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->g:Landroid/widget/ImageView;

    const p1, 0x7f0a005c

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active_subscription_top_button)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->h:Landroid/widget/Button;

    const p1, 0x7f0a005a

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active\u2026age_premium_guild_button)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->i:Landroid/widget/Button;

    const p1, 0x7f0a0055

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active\u2026bscription_cancel_button)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->j:Landroid/view/View;

    const p1, 0x7f0a005b

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.active_subscription_progress)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/discord/views/ActiveSubscriptionView;->k:Landroid/widget/ProgressBar;

    return-void
.end method

.method public static final b(Lcom/discord/models/domain/ModelSubscription;)Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;
    .locals 4

    const-string/jumbo v0, "subscription"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    invoke-virtual {v2, v0}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->fromSkuName(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/discord/utilities/billing/GooglePlaySkuKt;->isBundledSku(Lcom/discord/utilities/billing/GooglePlaySku;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-object p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_AND_PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    return-object p0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v1

    :cond_2
    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-ne v1, v0, :cond_3

    sget-object p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    return-object p0

    :cond_3
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_4

    const/4 v0, 0x3

    if-eq p0, v0, :cond_4

    packed-switch p0, :pswitch_data_0

    sget-object p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_CLASSIC:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    goto :goto_1

    :pswitch_0
    sget-object p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    goto :goto_1

    :cond_4
    :pswitch_1
    sget-object p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    :goto_1
    return-object p0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;Lcom/discord/models/domain/ModelSubscription$Status;ZLjava/lang/CharSequence;ZILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Z)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;",
            "Lcom/discord/models/domain/ModelSubscription$Status;",
            "Z",
            "Ljava/lang/CharSequence;",
            "ZI",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;Z)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    const-string v6, "activeSubscriptionType"

    move-object/from16 v7, p1

    invoke-static {v7, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v6, "status"

    invoke-static {v1, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "priceText"

    invoke-static {v2, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v0, Lcom/discord/views/ActiveSubscriptionView;->e:Landroid/widget/ImageView;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    const/4 v9, 0x4

    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x3

    if-eq v8, v10, :cond_1

    if-eq v8, v12, :cond_0

    if-eq v8, v9, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderBackground()I

    move-result v8

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderBackgroundResub()I

    move-result v8

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderBackgroundError()I

    move-result v8

    :goto_0
    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, v0, Lcom/discord/views/ActiveSubscriptionView;->f:Landroid/widget/ImageView;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    if-eq v8, v10, :cond_3

    if-eq v8, v12, :cond_2

    if-eq v8, v9, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderImage()I

    move-result v8

    goto :goto_1

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderImageResub()I

    move-result v8

    goto :goto_1

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderImageError()I

    move-result v8

    :goto_1
    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, v0, Lcom/discord/views/ActiveSubscriptionView;->g:Landroid/widget/ImageView;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->getHeaderLogo()I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, v0, Lcom/discord/views/ActiveSubscriptionView;->g:Landroid/widget/ImageView;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    const/4 v13, 0x1

    if-eqz v8, :cond_7

    if-eq v8, v13, :cond_6

    if-eq v8, v10, :cond_5

    if-ne v8, v12, :cond_4

    goto :goto_2

    :cond_4
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_5
    const v8, 0x7f1213a3

    goto :goto_3

    :cond_6
    :goto_2
    const v8, 0x7f121438

    goto :goto_3

    :cond_7
    const v8, 0x7f121436

    :goto_3
    invoke-static {v0, v8}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v6, v0, Lcom/discord/views/ActiveSubscriptionView;->d:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v8, "context"

    invoke-static {v14, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_11

    const v15, 0x7f121419

    if-eq v7, v13, :cond_e

    if-eq v7, v10, :cond_b

    if-ne v7, v12, :cond_a

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    if-eq v7, v12, :cond_9

    if-eq v7, v9, :cond_8

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v8

    aput-object v2, v7, v13

    const v2, 0x7f121418

    invoke-static {v0, v2, v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_8
    new-array v7, v10, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v8

    aput-object v2, v7, v13

    invoke-static {v0, v15, v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_9
    new-array v7, v10, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v8

    aput-object v2, v7, v13

    const v2, 0x7f12141c

    invoke-static {v0, v2, v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_a
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v11, 0x7f1000f0

    new-array v15, v13, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v15, v8

    move/from16 v10, p6

    invoke-virtual {v7, v11, v10, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "resources.getQuantityStr\u2026bscriptionCount\n        )"

    invoke-static {v7, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v11

    if-eq v11, v12, :cond_d

    if-eq v11, v9, :cond_c

    const v11, 0x7f1213ab

    new-array v15, v12, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v15, v8

    aput-object v7, v15, v13

    const/4 v7, 0x2

    aput-object v2, v15, v7

    invoke-static {v0, v11, v15}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_c
    const/4 v11, 0x2

    const v15, 0x7f1213a9

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v8

    aput-object v7, v9, v13

    aput-object v2, v9, v11

    invoke-static {v0, v15, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_d
    const/4 v11, 0x2

    const v9, 0x7f1213ae

    new-array v15, v12, [Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v15, v8

    aput-object v7, v15, v13

    aput-object v2, v15, v11

    invoke-static {v0, v9, v15}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_e
    const/4 v7, 0x2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v9

    if-eq v9, v12, :cond_10

    const/4 v10, 0x4

    if-eq v9, v10, :cond_f

    new-array v9, v7, [Ljava/lang/Object;

    aput-object v11, v9, v8

    aput-object v2, v9, v13

    const v2, 0x7f121418

    invoke-static {v0, v2, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_f
    new-array v9, v7, [Ljava/lang/Object;

    aput-object v11, v9, v8

    aput-object v2, v9, v13

    invoke-static {v0, v15, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_10
    new-array v9, v7, [Ljava/lang/Object;

    aput-object v11, v9, v8

    aput-object v2, v9, v13

    const v2, 0x7f12141c

    invoke-static {v0, v2, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_11
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    if-eq v7, v12, :cond_13

    const/4 v9, 0x4

    if-eq v7, v9, :cond_12

    const v7, 0x7f121412

    new-array v9, v13, [Ljava/lang/Object;

    aput-object v2, v9, v8

    invoke-static {v0, v7, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_12
    const v7, 0x7f121413

    new-array v9, v13, [Ljava/lang/Object;

    aput-object v2, v9, v8

    invoke-static {v0, v7, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_13
    const v7, 0x7f121416

    new-array v9, v13, [Ljava/lang/Object;

    aput-object v2, v9, v8

    invoke-static {v0, v7, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    move-object v15, v2

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3c

    const/16 v21, 0x0

    invoke-static/range {v14 .. v21}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/discord/views/ActiveSubscriptionView;->h:Landroid/widget/Button;

    xor-int/lit8 v6, p5, 0x1

    const/16 v7, 0x8

    if-eqz v6, :cond_14

    const/4 v6, 0x0

    goto :goto_5

    :cond_14
    const/16 v6, 0x8

    :goto_5
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/discord/views/ActiveSubscriptionView;->j:Landroid/view/View;

    if-eqz v5, :cond_15

    if-nez p5, :cond_15

    sget-object v6, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-eq v1, v6, :cond_15

    if-nez p10, :cond_15

    const/4 v6, 0x1

    goto :goto_6

    :cond_15
    const/4 v6, 0x0

    :goto_6
    if-eqz v6, :cond_16

    const/4 v6, 0x0

    goto :goto_7

    :cond_16
    const/16 v6, 0x8

    :goto_7
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/discord/views/ActiveSubscriptionView;->h:Landroid/widget/Button;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eq v1, v12, :cond_18

    const/4 v6, 0x4

    if-eq v1, v6, :cond_17

    const v1, 0x7f121431

    goto :goto_8

    :cond_17
    const v1, 0x7f1202fe

    goto :goto_8

    :cond_18
    const v1, 0x7f121541

    :goto_8
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->h:Landroid/widget/Button;

    new-instance v2, Lcom/discord/views/ActiveSubscriptionView$a;

    invoke-direct {v2, v8, v3}, Lcom/discord/views/ActiveSubscriptionView$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->h:Landroid/widget/Button;

    if-eqz v3, :cond_19

    const/4 v2, 0x1

    goto :goto_9

    :cond_19
    const/4 v2, 0x0

    :goto_9
    if-eqz v2, :cond_1a

    const/4 v2, 0x0

    goto :goto_a

    :cond_1a
    const/16 v2, 0x8

    :goto_a
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->h:Landroid/widget/Button;

    xor-int/lit8 v2, p3, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->i:Landroid/widget/Button;

    if-eqz v4, :cond_1b

    const/4 v2, 0x1

    goto :goto_b

    :cond_1b
    const/4 v2, 0x0

    :goto_b
    if-eqz v2, :cond_1c

    const/4 v2, 0x0

    goto :goto_c

    :cond_1c
    const/16 v2, 0x8

    :goto_c
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->i:Landroid/widget/Button;

    new-instance v2, Lcom/discord/views/ActiveSubscriptionView$a;

    invoke-direct {v2, v13, v4}, Lcom/discord/views/ActiveSubscriptionView$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->j:Landroid/view/View;

    new-instance v2, Lcom/discord/views/ActiveSubscriptionView$a;

    const/4 v3, 0x2

    invoke-direct {v2, v3, v5}, Lcom/discord/views/ActiveSubscriptionView$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/discord/views/ActiveSubscriptionView;->k:Landroid/widget/ProgressBar;

    if-eqz p5, :cond_1d

    goto :goto_d

    :cond_1d
    const/16 v8, 0x8

    :goto_d
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
