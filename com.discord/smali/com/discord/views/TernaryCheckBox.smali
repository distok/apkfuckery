.class public final Lcom/discord/views/TernaryCheckBox;
.super Landroid/widget/RelativeLayout;
.source "TernaryCheckBox.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/TernaryCheckBox$b;,
        Lcom/discord/views/TernaryCheckBox$a;
    }
.end annotation


# static fields
.field public static final o:Lcom/discord/views/TernaryCheckBox$a;


# instance fields
.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Lcom/discord/views/CheckableImageView;

.field public g:Lcom/discord/views/CheckableImageView;

.field public h:Lcom/discord/views/CheckableImageView;

.field public i:Landroid/view/View;

.field public j:Landroid/view/View;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/CharSequence;

.field public m:Lcom/discord/views/TernaryCheckBox$b;

.field public n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/views/TernaryCheckBox$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/views/TernaryCheckBox$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/views/TernaryCheckBox;->o:Lcom/discord/views/TernaryCheckBox$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 13

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/discord/views/TernaryCheckBox;->n:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    sget-object v4, Lcom/discord/R$a;->TernaryCheckBox:[I

    invoke-virtual {p1, p2, v4, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const-string v4, "context.obtainStyledAttr\u2026le.TernaryCheckBox, 0, 0)"

    invoke-static {p2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/discord/views/TernaryCheckBox;->k:Ljava/lang/String;

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3c

    const/4 v12, 0x0

    invoke-static/range {v5 .. v12}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    :goto_0
    iput-object v0, p0, Lcom/discord/views/TernaryCheckBox;->l:Ljava/lang/CharSequence;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    :goto_1
    const p2, 0x7f0d0164

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f0a093e

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.setting_label)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/views/TernaryCheckBox;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->k:Ljava/lang/String;

    const/16 v4, 0x8

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/16 v0, 0x8

    :goto_2
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/views/TernaryCheckBox;->d:Landroid/widget/TextView;

    if-eqz p2, :cond_8

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->k:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0a093f

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.setting_subtext)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/views/TernaryCheckBox;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    const/4 v4, 0x0

    :cond_3
    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/views/TernaryCheckBox;->e:Landroid/widget/TextView;

    if-eqz p2, :cond_7

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->l:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0a093d

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.setting_disabled_overlay)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/views/TernaryCheckBox;->i:Landroid/view/View;

    const p2, 0x7f0a0735

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.off_disabled_overlay)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/views/TernaryCheckBox;->j:Landroid/view/View;

    const p2, 0x7f0a0a98

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.ternary_check_on)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/views/CheckableImageView;

    iput-object p2, p0, Lcom/discord/views/TernaryCheckBox;->f:Lcom/discord/views/CheckableImageView;

    const p2, 0x7f0a0a97

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.ternary_check_off)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/views/CheckableImageView;

    iput-object p2, p0, Lcom/discord/views/TernaryCheckBox;->g:Lcom/discord/views/CheckableImageView;

    const p2, 0x7f0a0a96

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.ternary_check_neutral)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/CheckableImageView;

    iput-object p1, p0, Lcom/discord/views/TernaryCheckBox;->h:Lcom/discord/views/CheckableImageView;

    iget-object p1, p0, Lcom/discord/views/TernaryCheckBox;->f:Lcom/discord/views/CheckableImageView;

    if-eqz p1, :cond_6

    new-instance p2, Le;

    invoke-direct {p2, v2, p0}, Le;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/views/TernaryCheckBox;->g:Lcom/discord/views/CheckableImageView;

    if-eqz p1, :cond_5

    new-instance p2, Le;

    invoke-direct {p2, v1, p0}, Le;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/views/TernaryCheckBox;->h:Lcom/discord/views/CheckableImageView;

    if-eqz p1, :cond_4

    new-instance p2, Le;

    const/4 v0, 0x2

    invoke-direct {p2, v0, p0}, Le;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_4
    const-string p1, "checkNeutral"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_5
    const-string p1, "checkOff"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_6
    const-string p1, "checkOn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_7
    const-string/jumbo p1, "subtext"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_8
    const-string p1, "label"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public static final synthetic a(Lcom/discord/views/TernaryCheckBox;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/views/TernaryCheckBox;->setSwitchStatus(I)V

    return-void
.end method

.method private final setDisabled(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->g:Lcom/discord/views/CheckableImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->j:Landroid/view/View;

    if-eqz v0, :cond_2

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->i:Landroid/view/View;

    const-string v2, "allDisabledOverlay"

    if-eqz v0, :cond_1

    new-instance v3, Lcom/discord/views/TernaryCheckBox$c;

    invoke-direct {v3, p0, p1}, Lcom/discord/views/TernaryCheckBox$c;-><init>(Lcom/discord/views/TernaryCheckBox;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/views/TernaryCheckBox;->i:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    const-string p1, "offDisabledOverlay"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string p1, "checkOff"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setOffDisabled(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->g:Lcom/discord/views/CheckableImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->i:Landroid/view/View;

    if-eqz v0, :cond_2

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->j:Landroid/view/View;

    const-string v3, "offDisabledOverlay"

    if-eqz v0, :cond_1

    new-instance v4, Lcom/discord/views/TernaryCheckBox$d;

    invoke-direct {v4, p0, p1}, Lcom/discord/views/TernaryCheckBox$d;-><init>(Lcom/discord/views/TernaryCheckBox;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/views/TernaryCheckBox;->j:Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    const-string p1, "allDisabledOverlay"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string p1, "checkOff"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setSwitchStatus(I)V
    .locals 5

    iput p1, p0, Lcom/discord/views/TernaryCheckBox;->n:I

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->f:Lcom/discord/views/CheckableImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v0, v4}, Lcom/discord/views/CheckableImageView;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->g:Lcom/discord/views/CheckableImageView;

    if-eqz v0, :cond_5

    const/4 v4, -0x1

    if-ne p1, v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v0, v4}, Lcom/discord/views/CheckableImageView;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->h:Lcom/discord/views/CheckableImageView;

    if-eqz v0, :cond_4

    if-nez p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {v0, v2}, Lcom/discord/views/CheckableImageView;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->m:Lcom/discord/views/TernaryCheckBox$b;

    if-eqz v0, :cond_3

    check-cast v0, Lf/a/o/a/c0;

    iget-object v1, v0, Lf/a/o/a/c0;->a:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;

    iget v0, v0, Lf/a/o/a/c0;->b:I

    invoke-virtual {v1, v0, p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->i(II)V

    :cond_3
    return-void

    :cond_4
    const-string p1, "checkNeutral"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_5
    const-string p1, "checkOff"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_6
    const-string p1, "checkOn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final b()Z
    .locals 2

    iget v0, p0, Lcom/discord/views/TernaryCheckBox;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->g:Lcom/discord/views/CheckableImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->j:Landroid/view/View;

    if-eqz v0, :cond_1

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const-string v0, "allDisabledOverlay"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    const-string v0, "offDisabledOverlay"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    const-string v0, "checkOff"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/views/TernaryCheckBox;->setSwitchStatus(I)V

    return-void
.end method

.method public final e()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/discord/views/TernaryCheckBox;->setSwitchStatus(I)V

    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/views/TernaryCheckBox;->setSwitchStatus(I)V

    return-void
.end method

.method public final getOnSwitchStatusChangedListener()Lcom/discord/views/TernaryCheckBox$b;
    .locals 1

    iget-object v0, p0, Lcom/discord/views/TernaryCheckBox;->m:Lcom/discord/views/TernaryCheckBox$b;

    return-object v0
.end method

.method public final getSwitchStatus()I
    .locals 1

    iget v0, p0, Lcom/discord/views/TernaryCheckBox;->n:I

    return v0
.end method

.method public final setDisabled(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(messageRes)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/views/TernaryCheckBox;->setDisabled(Ljava/lang/String;)V

    return-void
.end method

.method public final setOffDisabled(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(messageRes)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/views/TernaryCheckBox;->setOffDisabled(Ljava/lang/String;)V

    return-void
.end method

.method public final setOnSwitchStatusChangedListener(Lcom/discord/views/TernaryCheckBox$b;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/views/TernaryCheckBox;->m:Lcom/discord/views/TernaryCheckBox$b;

    return-void
.end method
