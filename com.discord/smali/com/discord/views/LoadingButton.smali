.class public final Lcom/discord/views/LoadingButton;
.super Landroid/widget/FrameLayout;
.source "LoadingButton.kt"


# static fields
.field public static final synthetic i:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public f:F

.field public g:Ljava/lang/CharSequence;

.field public h:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/LoadingButton;

    const-string v3, "progress"

    const-string v4, "getProgress()Landroid/widget/ProgressBar;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/LoadingButton;

    const-string v6, "button"

    const-string v7, "getButton()Lcom/google/android/material/button/MaterialButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/LoadingButton;->i:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v0, 0x4

    const-string v1, "context"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v2, 0x7f0a064a

    invoke-static {p0, v2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v2

    iput-object v2, p0, Lcom/discord/views/LoadingButton;->d:Lkotlin/properties/ReadOnlyProperty;

    const v2, 0x7f0a0649

    invoke-static {p0, v2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v2

    iput-object v2, p0, Lcom/discord/views/LoadingButton;->e:Lkotlin/properties/ReadOnlyProperty;

    const v2, 0x3e4ccccd    # 0.2f

    iput v2, p0, Lcom/discord/views/LoadingButton;->f:F

    const v2, 0x7f0d013b

    invoke-static {p1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Landroid/widget/FrameLayout;->setClickable(Z)V

    invoke-virtual {p0, v2}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    if-eqz p2, :cond_8

    sget-object v3, Lcom/discord/R$a;->LoadingButton:[I

    invoke-virtual {p1, p2, v3, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026able.LoadingButton, 0, 0)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x7

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    iput-object p2, p0, Lcom/discord/views/LoadingButton;->g:Ljava/lang/CharSequence;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/material/button/MaterialButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    :cond_1
    const/16 p2, 0x9

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    int-to-float v3, v1

    cmpl-float v3, p2, v3

    if-lez v3, :cond_2

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v3

    invoke-virtual {v3, v1, p2}, Landroidx/appcompat/widget/AppCompatButton;->setTextSize(IF)V

    :cond_2
    const/4 p2, 0x5

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->getPaddingTop()I

    move-result v3

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v3

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Button;->getPaddingLeft()I

    move-result v4

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Button;->getPaddingRight()I

    move-result v5

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Button;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v4, p2, v5, v6}, Landroid/widget/Button;->setPadding(IIII)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/Button;->getPaddingBottom()I

    move-result p2

    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v3

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Button;->getPaddingLeft()I

    move-result v4

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Button;->getPaddingTop()I

    move-result v5

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Button;->getPaddingRight()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6, p2}, Landroid/widget/Button;->setPadding(IIII)V

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/discord/views/LoadingButton;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/material/button/MaterialButton;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v5

    if-nez v4, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string/jumbo p2, "start"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string p2, "end"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_2
    const-string/jumbo v0, "textStart"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo p2, "textEnd"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    goto :goto_1

    :cond_5
    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v5, v0}, Lcom/google/android/material/button/MaterialButton;->setIconGravity(I)V

    const/4 p2, 0x6

    const v0, 0x7fffffff

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    if-eq p2, v0, :cond_6

    invoke-direct {p0, p2}, Lcom/discord/views/LoadingButton;->setProgressBarColor(I)V

    :cond_6
    iget p2, p0, Lcom/discord/views/LoadingButton;->f:F

    invoke-virtual {p1, v2, p2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/discord/views/LoadingButton;->f:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->isEnabled()Z

    move-result p2

    if-eqz p2, :cond_7

    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_7
    iget p2, p0, Lcom/discord/views/LoadingButton;->f:F

    :goto_2
    invoke-virtual {p0, p2}, Landroid/widget/FrameLayout;->setAlpha(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_3

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    :cond_8
    :goto_3
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5482df92 -> :sswitch_3
        -0x3e813ccb -> :sswitch_2
        0x188db -> :sswitch_1
        0x68ac462 -> :sswitch_0
    .end sparse-switch
.end method

.method private final getButton()Lcom/google/android/material/button/MaterialButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/LoadingButton;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/LoadingButton;->i:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    return-object v0
.end method

.method private final getProgress()Landroid/widget/ProgressBar;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/LoadingButton;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/LoadingButton;->i:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private final setProgressBarColor(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getProgress()Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    return-void
.end method


# virtual methods
.method public setBackgroundColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/material/button/MaterialButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/discord/views/LoadingButton;->f:F

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    return-void
.end method

.method public final setIconVisibility(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/views/LoadingButton;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/google/android/material/button/MaterialButton;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/material/button/MaterialButton;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public final setIsLoading(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/views/LoadingButton;->g:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/material/button/MaterialButton;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getProgress()Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/views/LoadingButton;->g:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/views/LoadingButton;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/google/android/material/button/MaterialButton;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getProgress()Landroid/widget/ProgressBar;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;)V
    .locals 1

    iput-object p1, p0, Lcom/discord/views/LoadingButton;->g:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/discord/views/LoadingButton;->getButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
