.class public final Lcom/discord/views/user/UserAvatarPresenceView;
.super Landroid/widget/RelativeLayout;
.source "UserAvatarPresenceView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/user/UserAvatarPresenceView$a;
    }
.end annotation


# static fields
.field public static final synthetic g:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/user/UserAvatarPresenceView;

    const-string v3, "avatarSimpleDraweeView"

    const-string v4, "getAvatarSimpleDraweeView()Lcom/facebook/drawee/view/SimpleDraweeView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/user/UserAvatarPresenceView;

    const-string/jumbo v6, "statusView"

    const-string v7, "getStatusView()Lcom/discord/views/StatusView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/user/UserAvatarPresenceView;->g:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f0a0aec

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView;->d:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0aee

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView;->e:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    invoke-direct {v0}, Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;-><init>()V

    iput-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView;->f:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    const v0, 0x7f0d010d

    invoke-static {p1, v0, p0}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    sget-object v0, Lcom/discord/R$a;->UserAvatarPresenceView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026e.UserAvatarPresenceView)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const p2, 0x7f0404a8

    invoke-static {p0, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p2

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, p2}, Lcom/discord/views/user/UserAvatarPresenceView;->setAvatarBackgroundColor(I)V

    return-void
.end method

.method private final getAvatarSimpleDraweeView()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/user/UserAvatarPresenceView;->g:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getStatusView()Lcom/discord/views/StatusView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/user/UserAvatarPresenceView;->g:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/StatusView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/discord/views/user/UserAvatarPresenceView$a;)V
    .locals 8

    const-string/jumbo v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getAvatarSimpleDraweeView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    iget-object v0, p1, Lcom/discord/views/user/UserAvatarPresenceView$a;->a:Lcom/discord/models/domain/ModelUser;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v0, v2, v3, v4, v3}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/discord/views/user/UserAvatarPresenceView;->f:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object v0

    iget-object p1, p1, Lcom/discord/views/user/UserAvatarPresenceView$a;->b:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0, p1}, Lcom/discord/views/StatusView;->setPresence(Lcom/discord/models/domain/ModelPresence;)V

    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getAvatarSimpleDraweeView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getAvatarSimpleDraweeView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    int-to-float p1, p1

    const p2, 0x3eaccccd    # 0.3375f

    mul-float p2, p2, p1

    const v0, 0x3d19999a    # 0.0375f

    mul-float v0, v0, p1

    const v1, 0x3d4ccccd    # 0.05f

    mul-float p1, p1, v1

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/16 v2, 0xc

    int-to-float v2, v2

    mul-float v2, v2, v1

    cmpg-float v3, p2, v2

    if-gez v3, :cond_0

    div-float/2addr v2, p2

    mul-float p2, p2, v2

    mul-float v0, v0, v2

    mul-float p1, p1, v2

    :cond_0
    move v2, v0

    const/4 v3, 0x2

    int-to-float v3, v3

    mul-float v1, v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    float-to-int p2, p2

    iput p2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setTranslationX(F)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object p2

    float-to-int v0, v0

    invoke-virtual {p2, v0}, Lcom/discord/views/StatusView;->setBorderWidth(I)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/discord/views/StatusView;->setCornerRadius(F)V

    return-void
.end method

.method public final setAvatarBackgroundColor(I)V
    .locals 4
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getAvatarSimpleDraweeView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/discord/utilities/images/MGImages;->setCornerRadius(Landroid/widget/ImageView;FZLjava/lang/Integer;)V

    invoke-direct {p0}, Lcom/discord/views/user/UserAvatarPresenceView;->getStatusView()Lcom/discord/views/StatusView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/views/StatusView;->setBackgroundColor(I)V

    return-void
.end method
