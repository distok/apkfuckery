.class public final Lcom/discord/views/user/UserAvatarPresenceViewController;
.super Lcom/discord/utilities/viewcontroller/RxViewController;
.source "UserAvatarPresenceViewController.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/viewcontroller/RxViewController<",
        "Lcom/discord/views/user/UserAvatarPresenceView;",
        "Lcom/discord/views/user/UserAvatarPresenceView$a;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J

.field public final b:Lcom/discord/stores/StoreUser;

.field public final c:Lcom/discord/stores/StoreUserPresence;

.field public final d:Lcom/discord/utilities/streams/StreamContextService;


# direct methods
.method public constructor <init>(Lcom/discord/views/user/UserAvatarPresenceView;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/utilities/streams/StreamContextService;I)V
    .locals 16

    move-object/from16 v0, p0

    and-int/lit8 v1, p5, 0x2

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    and-int/lit8 v3, p5, 0x4

    if-eqz v3, :cond_1

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_1
    and-int/lit8 v4, p5, 0x8

    if-eqz v4, :cond_2

    new-instance v2, Lcom/discord/utilities/streams/StreamContextService;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xff

    const/4 v15, 0x0

    move-object v5, v2

    invoke-direct/range {v5 .. v15}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_2
    const-string/jumbo v4, "view"

    move-object/from16 v5, p1

    invoke-static {v5, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "storeUser"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "storeUserPresence"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "streamContextService"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p1}, Lcom/discord/utilities/viewcontroller/RxViewController;-><init>(Landroid/view/View;)V

    iput-object v1, v0, Lcom/discord/views/user/UserAvatarPresenceViewController;->b:Lcom/discord/stores/StoreUser;

    iput-object v3, v0, Lcom/discord/views/user/UserAvatarPresenceViewController;->c:Lcom/discord/stores/StoreUserPresence;

    iput-object v2, v0, Lcom/discord/views/user/UserAvatarPresenceViewController;->d:Lcom/discord/utilities/streams/StreamContextService;

    return-void
.end method


# virtual methods
.method public configureView(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/discord/views/user/UserAvatarPresenceView$a;

    const-string/jumbo v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/viewcontroller/RxViewController;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/discord/views/user/UserAvatarPresenceView;

    invoke-virtual {v0, p1}, Lcom/discord/views/user/UserAvatarPresenceView;->a(Lcom/discord/views/user/UserAvatarPresenceView$a;)V

    return-void
.end method

.method public observeState()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/views/user/UserAvatarPresenceView$a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceViewController;->b:Lcom/discord/stores/StoreUser;

    iget-wide v1, p0, Lcom/discord/views/user/UserAvatarPresenceViewController;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreUser;->observeUser(J)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "filter { it != null }.map { it!! }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/widgets/user/presence/ModelRichPresence;->Companion:Lcom/discord/widgets/user/presence/ModelRichPresence$Companion;

    iget-wide v2, p0, Lcom/discord/views/user/UserAvatarPresenceViewController;->a:J

    iget-object v4, p0, Lcom/discord/views/user/UserAvatarPresenceViewController;->c:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v1, v2, v3, v4}, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion;->get(JLcom/discord/stores/StoreUserPresence;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/views/user/UserAvatarPresenceViewController;->d:Lcom/discord/utilities/streams/StreamContextService;

    iget-wide v3, p0, Lcom/discord/views/user/UserAvatarPresenceViewController;->a:J

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/discord/utilities/streams/StreamContextService;->getForUser(JZ)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/views/user/UserAvatarPresenceViewController$a;->a:Lcom/discord/views/user/UserAvatarPresenceViewController$a;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026reamContext\n      )\n    }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
