.class public final Lcom/discord/views/user/UserAvatarPresenceView$a;
.super Ljava/lang/Object;
.source "UserAvatarPresenceView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/views/user/UserAvatarPresenceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/discord/models/domain/ModelUser;

.field public final b:Lcom/discord/models/domain/ModelPresence;

.field public final c:Lcom/discord/utilities/streams/StreamContext;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 1

    const-string/jumbo v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->a:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->b:Lcom/discord/models/domain/ModelPresence;

    iput-object p3, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->c:Lcom/discord/utilities/streams/StreamContext;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/views/user/UserAvatarPresenceView$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/views/user/UserAvatarPresenceView$a;

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->a:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/views/user/UserAvatarPresenceView$a;->a:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->b:Lcom/discord/models/domain/ModelPresence;

    iget-object v1, p1, Lcom/discord/views/user/UserAvatarPresenceView$a;->b:Lcom/discord/models/domain/ModelPresence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->c:Lcom/discord/utilities/streams/StreamContext;

    iget-object p1, p1, Lcom/discord/views/user/UserAvatarPresenceView$a;->c:Lcom/discord/utilities/streams/StreamContext;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->a:Lcom/discord/models/domain/ModelUser;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->b:Lcom/discord/models/domain/ModelPresence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->c:Lcom/discord/utilities/streams/StreamContext;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/utilities/streams/StreamContext;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ViewState(user="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->a:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", presence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->b:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", streamContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/views/user/UserAvatarPresenceView$a;->c:Lcom/discord/utilities/streams/StreamContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
