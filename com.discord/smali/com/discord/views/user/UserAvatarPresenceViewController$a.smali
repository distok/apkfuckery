.class public final Lcom/discord/views/user/UserAvatarPresenceViewController$a;
.super Ljava/lang/Object;
.source "UserAvatarPresenceViewController.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/views/user/UserAvatarPresenceViewController;->observeState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/widgets/user/presence/ModelRichPresence;",
        "Lcom/discord/utilities/streams/StreamContext;",
        "Lcom/discord/views/user/UserAvatarPresenceView$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/discord/views/user/UserAvatarPresenceViewController$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/views/user/UserAvatarPresenceViewController$a;

    invoke-direct {v0}, Lcom/discord/views/user/UserAvatarPresenceViewController$a;-><init>()V

    sput-object v0, Lcom/discord/views/user/UserAvatarPresenceViewController$a;->a:Lcom/discord/views/user/UserAvatarPresenceViewController$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Lcom/discord/widgets/user/presence/ModelRichPresence;

    check-cast p3, Lcom/discord/utilities/streams/StreamContext;

    new-instance v0, Lcom/discord/views/user/UserAvatarPresenceView$a;

    const-string/jumbo v1, "user"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-direct {v0, p1, p2, p3}, Lcom/discord/views/user/UserAvatarPresenceView$a;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;)V

    return-object v0
.end method
