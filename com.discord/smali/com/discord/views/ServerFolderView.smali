.class public final Lcom/discord/views/ServerFolderView;
.super Landroid/widget/LinearLayout;
.source "ServerFolderView.kt"


# static fields
.field public static final synthetic i:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public g:Ljava/lang/Long;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/ServerFolderView;

    const-string v3, "folderImg"

    const-string v4, "getFolderImg()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/ServerFolderView;

    const-string v6, "guildViewsContainer"

    const-string v7, "getGuildViewsContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/ServerFolderView;

    const-string v6, "guildViews"

    const-string v7, "getGuildViews()Ljava/util/List;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/ServerFolderView;->i:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v0, 0x4

    const/4 v1, 0x0

    const-string v2, "context"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f0a0464

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/views/ServerFolderView;->d:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a053e

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/views/ServerFolderView;->e:Lkotlin/properties/ReadOnlyProperty;

    new-array p2, v0, [I

    fill-array-data p2, :array_0

    const-string v0, "$this$bindViews"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ids"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lz/d;->d:Lz/d;

    invoke-static {p2, v0}, Ly/a/g0;->B([ILkotlin/jvm/functions/Function2;)Lz/h;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/views/ServerFolderView;->f:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0d0150

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-void

    :array_0
    .array-data 4
        0x7f0a053a
        0x7f0a053b
        0x7f0a053c
        0x7f0a053d
    .end array-data
.end method

.method private final getFolderImg()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/ServerFolderView;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/ServerFolderView;->i:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGuildViews()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/views/GuildView;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/ServerFolderView;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/ServerFolderView;->i:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getGuildViewsContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/ServerFolderView;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/ServerFolderView;->i:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(JZLjava/util/List;Ljava/lang/Integer;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    const-string v0, "guilds"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/views/ServerFolderView;->g:Ljava/lang/Long;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, p1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/discord/views/ServerFolderView;->h:Z

    if-eq v0, p3, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/ServerFolderView;->g:Ljava/lang/Long;

    if-eqz p3, :cond_4

    iput-boolean v1, p0, Lcom/discord/views/ServerFolderView;->h:Z

    if-eqz v0, :cond_2

    const p1, 0x7f01002d

    invoke-virtual {p0, p1}, Lcom/discord/views/ServerFolderView;->c(I)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/views/ServerFolderView;->b()V

    :goto_2
    if-eqz p5, :cond_3

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f060057

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    :goto_3
    int-to-long p1, p1

    const-wide p3, 0xff000000L

    add-long/2addr p1, p3

    long-to-int p2, p1

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getFolderImg()Landroid/widget/ImageView;

    move-result-object p1

    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-static {p1, p2}, Landroidx/core/widget/ImageViewCompat;->setImageTintList(Landroid/widget/ImageView;Landroid/content/res/ColorStateList;)V

    goto :goto_7

    :cond_4
    iput-boolean v2, p0, Lcom/discord/views/ServerFolderView;->h:Z

    if-eqz v0, :cond_5

    const p1, 0x7f01002e

    invoke-virtual {p0, p1}, Lcom/discord/views/ServerFolderView;->c(I)V

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/discord/views/ServerFolderView;->b()V

    :goto_4
    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getGuildViews()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    add-int/lit8 p5, p2, 0x1

    const/4 v0, 0x0

    if-ltz p2, :cond_8

    check-cast p3, Lcom/discord/views/GuildView;

    invoke-static {p4}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v1

    if-lt v1, p2, :cond_7

    invoke-interface {p4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->hasIcon()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    const-string v4, "asset://asset/images/default_icon.jpg"

    move-object v3, p2

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Lcom/discord/models/domain/ModelGuild;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-virtual {p3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object p2

    const-string v1, "guild.shortName"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3, p2, v0}, Lcom/discord/views/GuildView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    const/16 p2, 0x8

    invoke-virtual {p3, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_6
    move p2, p5

    goto :goto_5

    :cond_8
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    throw v0

    :cond_9
    :goto_7
    return-void
.end method

.method public final b()V
    .locals 3

    iget-boolean v0, p0, Lcom/discord/views/ServerFolderView;->h:Z

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getFolderImg()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getGuildViewsContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getFolderImg()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getGuildViewsContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final c(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/AnimRes;
        .end annotation
    .end param

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object p1

    new-instance v0, Lcom/discord/views/ServerFolderView$a;

    invoke-direct {v0, p0}, Lcom/discord/views/ServerFolderView$a;-><init>(Lcom/discord/views/ServerFolderView;)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getFolderImg()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getGuildViewsContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/ServerFolderView;->getFolderImg()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
