.class public final Lcom/discord/views/calls/VideoCallParticipantView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "VideoCallParticipantView.kt"

# interfaces
.implements Lcom/discord/utilities/view/grid/FrameGridLayout$DataView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;
    }
.end annotation


# static fields
.field public static final synthetic x:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public final g:Lkotlin/properties/ReadOnlyProperty;

.field public final h:Lkotlin/properties/ReadOnlyProperty;

.field public final i:Lkotlin/properties/ReadOnlyProperty;

.field public final j:Lkotlin/properties/ReadOnlyProperty;

.field public final k:Lkotlin/properties/ReadOnlyProperty;

.field public final l:Lkotlin/properties/ReadOnlyProperty;

.field public final m:Lkotlin/properties/ReadOnlyProperty;

.field public final n:Lkotlin/properties/ReadOnlyProperty;

.field public final o:Lkotlin/properties/ReadOnlyProperty;

.field public final p:Lkotlin/properties/ReadOnlyProperty;

.field public final q:Z

.field public r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

.field public s:Ljava/lang/String;

.field public t:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lrx/Subscription;

.field public v:Z

.field public w:Lrx/Subscription;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xd

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string/jumbo v3, "voiceUserView"

    const-string v4, "getVoiceUserView()Lcom/discord/views/VoiceUserView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "backgroundAvatar"

    const-string v7, "getBackgroundAvatar()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "loadingIndicator"

    const-string v7, "getLoadingIndicator()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "letterbox"

    const-string v7, "getLetterbox()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string/jumbo v6, "videoStreamRenderer"

    const-string v7, "getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "applicationStreamPreviewImage"

    const-string v7, "getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "applicationStreamPreviewText"

    const-string v7, "getApplicationStreamPreviewText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "muteStatusIndicator"

    const-string v7, "getMuteStatusIndicator()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "deafenStatusIndicator"

    const-string v7, "getDeafenStatusIndicator()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "liveIndicator"

    const-string v7, "getLiveIndicator()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "applicationStreamPaused"

    const-string v7, "getApplicationStreamPaused()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string v6, "applicationStreamEnded"

    const-string v7, "getApplicationStreamEnded()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/calls/VideoCallParticipantView;

    const-string/jumbo v6, "videoLabel"

    const-string v7, "getVideoLabel()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/discord/views/calls/VideoCallParticipantView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    const-string p4, "context"

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p3, 0x7f0a075f

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->d:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0750

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->e:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a075c

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->f:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0751

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->g:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a075d

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->h:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0759

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->i:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a075a

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->j:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0754

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->k:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0752

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->l:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0753

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->m:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0758

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->n:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a0755

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->o:Lkotlin/properties/ReadOnlyProperty;

    const p3, 0x7f0a075b

    invoke-static {p0, p3}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->p:Lkotlin/properties/ReadOnlyProperty;

    sget-object p3, Lf/a/n/f0/g;->d:Lf/a/n/f0/g;

    iput-object p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->t:Lkotlin/jvm/functions/Function1;

    const p3, 0x7f0d016b

    invoke-static {p1, p3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    if-eqz p2, :cond_2

    sget-object p3, Lcom/discord/R$a;->VideoCallParticipantView:[I

    invoke-virtual {p1, p2, p3, v0, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026            0\n          )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    const/4 p3, 0x1

    invoke-virtual {p1, p3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/discord/views/calls/VideoCallParticipantView;->q:Z

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    move v0, p2

    goto :goto_0

    :cond_2
    iput-boolean v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->q:Z

    :goto_0
    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/discord/views/calls/AppVideoStreamRenderer;->setIsOverlay(Z)V

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object p1

    iget-boolean p2, p0, Lcom/discord/views/calls/VideoCallParticipantView;->q:Z

    invoke-virtual {p1, p2}, Lcom/discord/views/calls/AppVideoStreamRenderer;->setMatchVideoOrientation(Z)V

    return-void
.end method

.method public static final synthetic a(Lcom/discord/views/calls/VideoCallParticipantView;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getBackgroundAvatar()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Lcom/discord/views/calls/VideoCallParticipantView;Landroid/graphics/Point;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLetterbox()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    const/4 v2, 0x1

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eq p1, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    iget-boolean p1, p0, Lcom/discord/views/calls/VideoCallParticipantView;->q:Z

    if-eqz p1, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    iput p1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_3
    return-void
.end method

.method private final getApplicationStreamEnded()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->o:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getApplicationStreamPaused()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->n:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->i:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getApplicationStreamPreviewText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->j:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBackgroundAvatar()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDeafenStatusIndicator()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->l:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getLetterbox()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->g:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getLiveIndicator()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->m:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getLoadingIndicator()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMuteStatusIndicator()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->k:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getVideoLabel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->p:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->h:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/calls/AppVideoStreamRenderer;

    return-object v0
.end method

.method private final getVoiceUserView()Lcom/discord/views/VoiceUserView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView;->x:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/VoiceUserView;

    return-object v0
.end method


# virtual methods
.method public final getData()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;
    .locals 1

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    return-object v0
.end method

.method public getDataId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public onBind(Lcom/discord/utilities/view/grid/FrameGridLayout$Data;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    invoke-virtual {p0, p1}, Lcom/discord/views/calls/VideoCallParticipantView;->set(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->u:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/views/calls/VideoCallParticipantView;->v:Z

    invoke-direct {p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2, v2, v0}, Lcom/discord/views/calls/AppVideoStreamRenderer;->c(Ljava/lang/Integer;Lorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Z)V

    return-void
.end method

.method public onRemove()V
    .locals 0

    invoke-static {p0}, Lcom/discord/utilities/view/grid/FrameGridLayout$DataView$DefaultImpls;->onRemove(Lcom/discord/utilities/view/grid/FrameGridLayout$DataView;)V

    return-void
.end method

.method public final set(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V
    .locals 21
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    if-eqz v3, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVoiceUserView()Lcom/discord/views/VoiceUserView;

    move-result-object v4

    new-instance v5, Lf/a/n/f0/h;

    invoke-direct {v5, v3}, Lf/a/n/f0/h;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V

    invoke-virtual {v4, v5}, Lcom/discord/views/VoiceUserView;->setOnBitmapLoadedListener(Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVoiceUserView()Lcom/discord/views/VoiceUserView;

    move-result-object v4

    const v5, 0x7f070066

    invoke-virtual {v4, v3, v5}, Lcom/discord/views/VoiceUserView;->a(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V

    iget-object v4, v0, Lcom/discord/views/calls/VideoCallParticipantView;->u:Lrx/Subscription;

    if-eqz v4, :cond_1

    invoke-interface {v4}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    sget-object v4, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->observeRepresentativeColorForUser(J)Lrx/Observable;

    move-result-object v4

    sget-object v5, Lf/a/n/f0/i;->d:Lf/a/n/f0/i;

    invoke-virtual {v4, v5}, Lrx/Observable;->W(Lg0/k/b;)Lrx/Observable;

    move-result-object v6

    const-string v4, "UserRepresentativeColors\u2026.takeUntil { it != null }"

    invoke-static {v6, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v7, Lcom/discord/views/calls/VideoCallParticipantView;

    const/4 v8, 0x0

    new-instance v12, Lf/a/n/f0/j;

    invoke-direct {v12, v0}, Lf/a/n/f0/j;-><init>(Lcom/discord/views/calls/VideoCallParticipantView;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v9, Lf/a/n/f0/k;

    invoke-direct {v9, v0}, Lf/a/n/f0/k;-><init>(Lcom/discord/views/calls/VideoCallParticipantView;)V

    const/16 v13, 0x1a

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    goto :goto_1

    :cond_3
    move-object v4, v2

    :goto_1
    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static {v4, v6, v2, v5, v2}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/discord/views/calls/VideoCallParticipantView;->s:Ljava/lang/String;

    invoke-static {v4, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    const/4 v7, 0x1

    xor-int/2addr v5, v7

    if-eqz v5, :cond_4

    iput-object v4, v0, Lcom/discord/views/calls/VideoCallParticipantView;->s:Ljava/lang/String;

    :cond_4
    iget-object v4, v0, Lcom/discord/views/calls/VideoCallParticipantView;->r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-eqz v4, :cond_5

    iget-object v5, v4, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    if-eqz v5, :cond_5

    goto :goto_2

    :cond_5
    sget-object v5, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->DEFAULT:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    :goto_2
    if-eqz v4, :cond_6

    iget-boolean v8, v4, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->i:Z

    if-ne v8, v7, :cond_6

    const/4 v8, 0x1

    goto :goto_3

    :cond_6
    const/4 v8, 0x0

    :goto_3
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b()Ljava/lang/Integer;

    move-result-object v4

    goto :goto_4

    :cond_7
    move-object v4, v2

    :goto_4
    iput-object v1, v0, Lcom/discord/views/calls/VideoCallParticipantView;->r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-eqz v1, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b()Ljava/lang/Integer;

    move-result-object v9

    goto :goto_5

    :cond_8
    move-object v9, v2

    :goto_5
    if-eqz v1, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b()Ljava/lang/Integer;

    move-result-object v10

    goto :goto_6

    :cond_9
    move-object v10, v2

    :goto_6
    invoke-static {v10, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v7

    const/16 v11, 0x8

    if-eqz v4, :cond_d

    if-eqz v10, :cond_c

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLetterbox()Landroid/view/View;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v10

    if-nez v10, :cond_a

    const/4 v10, 0x1

    goto :goto_7

    :cond_a
    const/4 v10, 0x0

    :goto_7
    xor-int/2addr v10, v7

    if-eqz v10, :cond_b

    const/4 v10, 0x0

    goto :goto_8

    :cond_b
    const/16 v10, 0x8

    :goto_8
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLetterbox()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_d
    :goto_9
    if-eqz v1, :cond_e

    iget-object v4, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    goto :goto_a

    :cond_e
    move-object v4, v2

    :goto_a
    sget-object v10, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->APPLICATION_STREAMING:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    if-ne v4, v10, :cond_1a

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getBackgroundAvatar()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVoiceUserView()Lcom/discord/views/VoiceUserView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getMuteStatusIndicator()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getDeafenStatusIndicator()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->f:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    if-nez v4, :cond_f

    goto/16 :goto_14

    :cond_f
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    if-eqz v4, :cond_18

    if-eq v4, v7, :cond_16

    const/4 v12, 0x2

    if-eq v4, v12, :cond_13

    const/4 v12, 0x3

    if-eq v4, v12, :cond_11

    const/4 v12, 0x4

    if-eq v4, v12, :cond_10

    goto/16 :goto_14

    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPaused()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamEnded()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLiveIndicator()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_14

    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPaused()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamEnded()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLiveIndicator()Landroid/widget/TextView;

    move-result-object v4

    iget-boolean v12, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->i:Z

    xor-int/2addr v12, v7

    if-eqz v12, :cond_12

    const/4 v12, 0x0

    goto :goto_b

    :cond_12
    const/16 v12, 0x8

    :goto_b
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_14

    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPaused()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamEnded()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLiveIndicator()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    new-instance v12, Lf/a/n/f0/l;

    invoke-direct {v12, v0, v1}, Lf/a/n/f0/l;-><init>(Lcom/discord/views/calls/VideoCallParticipantView;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v4

    if-eqz v4, :cond_14

    invoke-virtual {v4}, Lcom/discord/utilities/streams/StreamContext;->getPreview()Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    move-result-object v4

    goto :goto_c

    :cond_14
    move-object v4, v2

    :goto_c
    instance-of v12, v4, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;

    if-eqz v12, :cond_15

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v12

    check-cast v4, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Ljava/lang/String;)V

    goto/16 :goto_14

    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Ljava/lang/String;)V

    goto/16 :goto_14

    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPaused()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamEnded()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLiveIndicator()Landroid/widget/TextView;

    move-result-object v4

    iget-boolean v12, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->i:Z

    xor-int/2addr v12, v7

    if-eqz v12, :cond_17

    const/4 v12, 0x0

    goto :goto_d

    :cond_17
    const/16 v12, 0x8

    :goto_d
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_14

    :cond_18
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPaused()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamEnded()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLiveIndicator()Landroid/widget/TextView;

    move-result-object v4

    iget-boolean v12, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->i:Z

    xor-int/2addr v12, v7

    if-eqz v12, :cond_19

    const/4 v12, 0x0

    goto :goto_e

    :cond_19
    const/16 v12, 0x8

    :goto_e
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_14

    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPreviewText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamPaused()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getApplicationStreamEnded()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getMuteStatusIndicator()Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v3, :cond_1b

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMuted()Z

    move-result v12

    goto :goto_f

    :cond_1b
    const/4 v12, 0x0

    :goto_f
    if-eqz v12, :cond_1c

    const/4 v12, 0x0

    goto :goto_10

    :cond_1c
    const/16 v12, 0x8

    :goto_10
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getDeafenStatusIndicator()Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v3, :cond_1d

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isDeafened()Z

    move-result v12

    goto :goto_11

    :cond_1d
    const/4 v12, 0x0

    :goto_11
    if-eqz v12, :cond_1e

    const/4 v12, 0x0

    goto :goto_12

    :cond_1e
    const/16 v12, 0x8

    :goto_12
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    if-eqz v9, :cond_1f

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getBackgroundAvatar()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVoiceUserView()Lcom/discord/views/VoiceUserView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_13

    :cond_1f
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getBackgroundAvatar()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVoiceUserView()Lcom/discord/views/VoiceUserView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_13
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getLiveIndicator()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_14
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    if-eqz v1, :cond_20

    iget-object v12, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->d:Lorg/webrtc/RendererCommon$ScalingType;

    goto :goto_15

    :cond_20
    move-object v12, v2

    :goto_15
    if-eqz v1, :cond_21

    iget-object v13, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->e:Lorg/webrtc/RendererCommon$ScalingType;

    goto :goto_16

    :cond_21
    move-object v13, v2

    :goto_16
    if-eqz v1, :cond_22

    iget-boolean v14, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->c:Z

    goto :goto_17

    :cond_22
    const/4 v14, 0x0

    :goto_17
    invoke-virtual {v4, v9, v12, v13, v14}, Lcom/discord/views/calls/AppVideoStreamRenderer;->c(Ljava/lang/Integer;Lorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Z)V

    iget-object v4, v0, Lcom/discord/views/calls/VideoCallParticipantView;->w:Lrx/Subscription;

    if-eqz v4, :cond_23

    invoke-interface {v4}, Lrx/Subscription;->unsubscribe()V

    :cond_23
    if-eqz v9, :cond_24

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    iget-object v4, v4, Lcom/discord/views/calls/AppVideoStreamRenderer;->h:Lrx/subjects/BehaviorSubject;

    const-string v9, "currentFrameResolutionSubject"

    invoke-static {v4, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v9, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {v4, v9}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    sget-object v9, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {v4, v9}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v12

    const-string v4, "filter { it != null }.map { it!! }"

    invoke-static {v12, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v13, Lcom/discord/views/calls/VideoCallParticipantView;

    const/4 v14, 0x0

    new-instance v4, Lf/a/n/f0/m;

    invoke-direct {v4, v0}, Lf/a/n/f0/m;-><init>(Lcom/discord/views/calls/VideoCallParticipantView;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v15, Lf/a/n/f0/n;

    invoke-direct {v15, v0}, Lf/a/n/f0/n;-><init>(Lcom/discord/views/calls/VideoCallParticipantView;)V

    const/16 v19, 0x1a

    const/16 v20, 0x0

    move-object/from16 v18, v4

    invoke-static/range {v12 .. v20}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_24
    iget-object v4, v0, Lcom/discord/views/calls/VideoCallParticipantView;->r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-eqz v4, :cond_25

    iget-object v2, v4, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    :cond_25
    if-eqz v1, :cond_26

    iget-boolean v4, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->i:Z

    if-ne v4, v7, :cond_26

    const/4 v4, 0x1

    goto :goto_18

    :cond_26
    const/4 v4, 0x0

    :goto_18
    if-eqz v2, :cond_29

    if-ne v2, v5, :cond_27

    if-eq v4, v8, :cond_29

    :cond_27
    sget-object v5, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->DEFAULT:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    if-ne v2, v5, :cond_28

    if-nez v4, :cond_28

    new-instance v2, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v2}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    invoke-virtual {v2, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    new-instance v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v6}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v6}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v6}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainDefaultHeight(II)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v6}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainDefaultWidth(II)V

    invoke-virtual {v2, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    goto :goto_19

    :cond_28
    new-instance v2, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v2}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    invoke-virtual {v2, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    new-instance v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v5, v5}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v7}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainDefaultHeight(II)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoStreamRenderer()Lcom/discord/views/calls/AppVideoStreamRenderer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getId()I

    move-result v4

    invoke-virtual {v2, v4, v7}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainDefaultWidth(II)V

    invoke-virtual {v2, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    :cond_29
    :goto_19
    if-eqz v3, :cond_2d

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoLabel()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    if-ne v2, v10, :cond_2a

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f120988

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "context.getString(R.string.go_live_tile_screen)"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v6

    invoke-static {v4, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "java.lang.String.format(format, *args)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoLabel()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f080410

    invoke-virtual {v2, v3, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1a

    :cond_2a
    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoLabel()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_1a
    iget-boolean v2, v0, Lcom/discord/views/calls/VideoCallParticipantView;->v:Z

    if-eqz v2, :cond_2b

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoLabel()Landroid/widget/TextView;

    move-result-object v2

    iget-boolean v1, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->h:Z

    const-wide/16 v3, 0xc8

    invoke-static {v2, v1, v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeBy(Landroid/view/View;ZJ)V

    goto :goto_1c

    :cond_2b
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/calls/VideoCallParticipantView;->getVideoLabel()Landroid/widget/TextView;

    move-result-object v2

    iget-boolean v1, v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->h:Z

    if-eqz v1, :cond_2c

    goto :goto_1b

    :cond_2c
    const/16 v6, 0x8

    :goto_1b
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v7, v0, Lcom/discord/views/calls/VideoCallParticipantView;->v:Z

    :cond_2d
    :goto_1c
    return-void
.end method

.method public final setData(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/views/calls/VideoCallParticipantView;->r:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    return-void
.end method

.method public final setOnWatchStreamClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onWatchStreamClicked"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/calls/VideoCallParticipantView;->t:Lkotlin/jvm/functions/Function1;

    return-void
.end method
