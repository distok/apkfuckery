.class public final Lcom/discord/views/calls/AppVideoStreamRenderer;
.super Lco/discord/media_engine/VideoStreamRenderer;
.source "AppVideoStreamRenderer.kt"


# instance fields
.field public d:Z

.field public final e:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lrx/Subscription;

.field public g:Ljava/lang/Integer;

.field public h:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/graphics/Point;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lorg/webrtc/RendererCommon$ScalingType;

.field public k:Lorg/webrtc/RendererCommon$ScalingType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lco/discord/media_engine/VideoStreamRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->e:Lrx/subjects/BehaviorSubject;

    const/4 p1, 0x0

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->h:Lrx/subjects/BehaviorSubject;

    sget-object p1, Lorg/webrtc/RendererCommon$ScalingType;->SCALE_ASPECT_BALANCED:Lorg/webrtc/RendererCommon$ScalingType;

    iput-object p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->j:Lorg/webrtc/RendererCommon$ScalingType;

    iput-object p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->k:Lorg/webrtc/RendererCommon$ScalingType;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 7
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->f:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    sget-object v0, Lf/a/n/f0/d;->a:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->g:Ljava/lang/Integer;

    const-string v2, "null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>"

    invoke-static {v0, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {v0}, Lx/m/c/w;->asMutableMap(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->h:Lrx/subjects/BehaviorSubject;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaEngine;->getVoiceEngineNative()Lcom/hammerandchisel/libdiscord/Discord;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lco/discord/media_engine/VideoStreamRenderer;->attachToStream$default(Lco/discord/media_engine/VideoStreamRenderer;Lcom/hammerandchisel/libdiscord/Discord;Ljava/lang/String;Lorg/webrtc/RendererCommon$RendererEvents;ILjava/lang/Object;)V

    return-void
.end method

.method public final c(Ljava/lang/Integer;Lorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Z)V
    .locals 15
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    if-eqz v1, :cond_8

    iget-object v2, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->g:Ljava/lang/Integer;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/discord/views/calls/AppVideoStreamRenderer;->b()V

    :cond_0
    if-eqz v2, :cond_4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lf/a/n/f0/d;->a:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lco/discord/media_engine/VideoStreamRenderer;

    if-eqz v5, :cond_2

    if-ne v5, v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreMediaEngine;->getVoiceEngineNative()Lcom/hammerandchisel/libdiscord/Discord;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lco/discord/media_engine/VideoStreamRenderer;->attachToStream$default(Lco/discord/media_engine/VideoStreamRenderer;Lcom/hammerandchisel/libdiscord/Discord;Ljava/lang/String;Lorg/webrtc/RendererCommon$RendererEvents;ILjava/lang/Object;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    :cond_2
    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lf/a/n/f0/e;

    invoke-direct {v3}, Lf/a/n/f0/e;-><init>()V

    iget-object v4, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->f:Lrx/Subscription;

    if-eqz v4, :cond_3

    invoke-interface {v4}, Lrx/Subscription;->unsubscribe()V

    :cond_3
    iget-object v4, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->e:Lrx/subjects/BehaviorSubject;

    new-instance v5, Lf/a/n/f0/a;

    invoke-direct {v5, v3}, Lf/a/n/f0/a;-><init>(Lf/a/n/f0/e;)V

    invoke-virtual {v4, v5}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    const-string v5, "onSizeChangedSubject\n   \u2026rameResolutionSampled() }"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {v4, v5}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    sget-object v5, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {v4, v5}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    const-string v5, "filter { it != null }.map { it!! }"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v10, Lf/a/n/f0/b;

    invoke-direct {v10, p0}, Lf/a/n/f0/b;-><init>(Lcom/discord/views/calls/AppVideoStreamRenderer;)V

    const-class v4, Lcom/discord/views/calls/AppVideoStreamRenderer;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    const-string v4, "javaClass.simpleName"

    invoke-static {v8, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v9, Lf/a/n/f0/c;

    invoke-direct {v9, p0}, Lf/a/n/f0/c;-><init>(Lcom/discord/views/calls/AppVideoStreamRenderer;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x31

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    const-string v4, "binding native renderer "

    invoke-static {v4}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Landroid/view/SurfaceView;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " to stream id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AppVideoStreamRenderer"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreMediaEngine;->getVoiceEngineNative()Lcom/hammerandchisel/libdiscord/Discord;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v4, v2, v3}, Lco/discord/media_engine/VideoStreamRenderer;->attachToStream(Lcom/hammerandchisel/libdiscord/Discord;Ljava/lang/String;Lorg/webrtc/RendererCommon$RendererEvents;)V

    iput-object v1, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->g:Ljava/lang/Integer;

    :cond_4
    if-eqz p2, :cond_5

    move-object/from16 v1, p2

    goto :goto_1

    :cond_5
    iget-object v1, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->j:Lorg/webrtc/RendererCommon$ScalingType;

    :goto_1
    if-eqz p3, :cond_6

    move-object/from16 v2, p3

    goto :goto_2

    :cond_6
    iget-object v2, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->k:Lorg/webrtc/RendererCommon$ScalingType;

    :goto_2
    move/from16 v3, p4

    invoke-virtual {p0, v3}, Lorg/webrtc/SurfaceViewRenderer;->setMirror(Z)V

    iget-boolean v3, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->d:Z

    invoke-virtual {p0, v3}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    iget-object v3, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->j:Lorg/webrtc/RendererCommon$ScalingType;

    if-ne v1, v3, :cond_7

    iget-object v3, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->k:Lorg/webrtc/RendererCommon$ScalingType;

    if-eq v2, v3, :cond_9

    :cond_7
    invoke-virtual {p0, v1, v2}, Lorg/webrtc/SurfaceViewRenderer;->setScalingType(Lorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;)V

    iput-object v1, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->j:Lorg/webrtc/RendererCommon$ScalingType;

    iput-object v2, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->k:Lorg/webrtc/RendererCommon$ScalingType;

    goto :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/discord/views/calls/AppVideoStreamRenderer;->b()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/discord/views/calls/AppVideoStreamRenderer;->g:Ljava/lang/Integer;

    :cond_9
    :goto_3
    return-void
.end method

.method public final getOnFrameRenderedListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/graphics/Point;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->i:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/SurfaceView;->onSizeChanged(IIII)V

    iget-object p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->e:Lrx/subjects/BehaviorSubject;

    sget-object p2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final setIsOverlay(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->d:Z

    return-void
.end method

.method public final setMatchVideoOrientation(Z)V
    .locals 0

    return-void
.end method

.method public final setOnFrameRenderedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/graphics/Point;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/views/calls/AppVideoStreamRenderer;->i:Lkotlin/jvm/functions/Function1;

    return-void
.end method
