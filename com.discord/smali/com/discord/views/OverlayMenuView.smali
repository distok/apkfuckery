.class public final Lcom/discord/views/OverlayMenuView;
.super Landroid/widget/LinearLayout;
.source "OverlayMenuView.kt"

# interfaces
.implements Lcom/discord/app/AppComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/OverlayMenuView$a;
    }
.end annotation


# static fields
.field public static final synthetic o:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public final g:Lkotlin/properties/ReadOnlyProperty;

.field public final h:Lkotlin/properties/ReadOnlyProperty;

.field public final i:Lkotlin/properties/ReadOnlyProperty;

.field public final j:Lkotlin/properties/ReadOnlyProperty;

.field public final k:Lkotlin/properties/ReadOnlyProperty;

.field public final l:Lkotlin/properties/ReadOnlyProperty;

.field public final m:Lkotlin/properties/ReadOnlyProperty;

.field public n:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/OverlayMenuView;

    const-string v3, "networkQualityIv"

    const-string v4, "getNetworkQualityIv()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string v6, "guildNameTv"

    const-string v7, "getGuildNameTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string v6, "channelNameTv"

    const-string v7, "getChannelNameTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string v6, "inviteLinkBtn"

    const-string v7, "getInviteLinkBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string/jumbo v6, "switchChannelBtn"

    const-string v7, "getSwitchChannelBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string v6, "openAppBtn"

    const-string v7, "getOpenAppBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string/jumbo v6, "srcToggle"

    const-string v7, "getSrcToggle()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string v6, "muteToggle"

    const-string v7, "getMuteToggle()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const-string v6, "disconnectBtn"

    const-string v7, "getDisconnectBtn()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d013f

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    const-string p2, "PublishSubject.create()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->d:Lrx/subjects/Subject;

    const p1, 0x7f0a0746

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->e:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a073f

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->f:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a073d

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->g:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0741

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->h:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0748

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->i:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0747

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->j:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a1e

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->k:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a06dd

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->l:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0365

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->m:Lkotlin/properties/ReadOnlyProperty;

    sget-object p1, Lf/a/n/t;->d:Lf/a/n/t;

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->n:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final a(Lcom/discord/views/OverlayMenuView;Lcom/discord/views/OverlayMenuView$a;)V
    .locals 5

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/discord/views/OverlayMenuView$a;->c:Lcom/discord/widgets/voice/model/CallModel;

    if-nez v0, :cond_0

    goto/16 :goto_4

    :cond_0
    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getInviteLinkBtn()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lg;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0, p1}, Lg;-><init>(ILjava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getInviteLinkBtn()Landroid/view/View;

    move-result-object v0

    const-wide/16 v3, 0x1

    iget-object v1, p1, Lcom/discord/views/OverlayMenuView$a;->a:Ljava/lang/Long;

    invoke-static {v3, v4, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getSwitchChannelBtn()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lj;

    invoke-direct {v1, v2, p0}, Lj;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getOpenAppBtn()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lg;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0, p1}, Lg;-><init>(ILjava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getSrcToggle()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p1, Lcom/discord/views/OverlayMenuView$a;->c:Lcom/discord/widgets/voice/model/CallModel;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v1

    sget-object v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    if-ne v1, v3, :cond_2

    const/4 v1, -0x1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0601a8

    invoke-static {v1, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    :goto_1
    const-string v3, "if (selectedOutputDevice\u2026rimary_dark_400))\n      }"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getSrcToggle()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lj;

    invoke-direct {v1, v2, p1}, Lj;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getMuteToggle()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p1, Lcom/discord/views/OverlayMenuView$a;->c:Lcom/discord/widgets/voice/model/CallModel;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isMeMutedByAnySource()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setActivated(Z)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getMuteToggle()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lj;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lj;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getDisconnectBtn()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lj;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p0}, Lj;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getNetworkQualityIv()Landroid/widget/ImageView;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/voice/VoiceViewUtils;->INSTANCE:Lcom/discord/utilities/voice/VoiceViewUtils;

    iget-object v2, p1, Lcom/discord/views/OverlayMenuView$a;->d:Lcom/discord/rtcconnection/RtcConnection$Quality;

    invoke-virtual {v1, v2}, Lcom/discord/utilities/voice/VoiceViewUtils;->getQualityIndicator(Lcom/discord/rtcconnection/RtcConnection$Quality;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getGuildNameTv()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p1, Lcom/discord/views/OverlayMenuView$a;->b:Lcom/discord/models/domain/ModelGuild;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    const-string v2, ""

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    move-object v1, v2

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/views/OverlayMenuView;->getChannelNameTv()Landroid/widget/TextView;

    move-result-object p0

    iget-object p1, p1, Lcom/discord/views/OverlayMenuView$a;->c:Lcom/discord/widgets/voice/model/CallModel;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    move-object v2, p1

    :cond_5
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    return-void
.end method

.method private final getChannelNameTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->g:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDisconnectBtn()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->m:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGuildNameTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getInviteLinkBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->h:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMuteToggle()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->l:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getNetworkQualityIv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getOpenAppBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->j:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSrcToggle()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->k:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getSwitchChannelBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->i:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/OverlayMenuView;->o:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final getOnDismissRequested$app_productionDiscordExternalRelease()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->n:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public getUnsubscribeSignal()Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/OverlayMenuView;->d:Lrx/subjects/Subject;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 12

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/n/q;->d:Lf/a/n/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n            \u2026        }\n              }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n            \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/views/OverlayMenuView;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/views/OverlayMenuView$b;

    invoke-direct {v9, p0}, Lcom/discord/views/OverlayMenuView$b;-><init>(Lcom/discord/views/OverlayMenuView;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/views/OverlayMenuView;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public final setOnDismissRequested$app_productionDiscordExternalRelease(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/OverlayMenuView;->n:Lkotlin/jvm/functions/Function0;

    return-void
.end method
