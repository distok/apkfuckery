.class public final Lcom/discord/views/CodeVerificationView;
.super Landroid/widget/LinearLayout;
.source "CodeVerificationView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/CodeVerificationView$a;
    }
.end annotation


# static fields
.field public static final j:Lcom/discord/views/CodeVerificationView$a;


# instance fields
.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/graphics/drawable/Drawable;

.field public final f:Landroid/graphics/drawable/Drawable;

.field public g:Lcom/discord/views/CodeVerificationView$a;

.field public h:Ljava/lang/String;

.field public i:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/discord/views/CodeVerificationView$a;->d:Lcom/discord/views/CodeVerificationView$a;

    sput-object v0, Lcom/discord/views/CodeVerificationView;->j:Lcom/discord/views/CodeVerificationView$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v0, 0x4

    const-string v1, "context"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v2, ""

    iput-object v2, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    sget-object v2, Lf/a/n/e;->d:Lf/a/n/e;

    iput-object v2, p0, Lcom/discord/views/CodeVerificationView;->i:Lkotlin/jvm/functions/Function1;

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/discord/R$a;->CodeVerificationView:[I

    invoke-virtual {v2, p2, v3, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const-string v2, "context.obtainStyledAttr\u2026deVerificationView, 0, 0)"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    sget-object v2, Lcom/discord/views/CodeVerificationView;->j:Lcom/discord/views/CodeVerificationView$a;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    sget-object v3, Lcom/discord/views/CodeVerificationView$a;->g:Lcom/discord/views/CodeVerificationView$a$a;

    sget-object v3, Lcom/discord/views/CodeVerificationView$a;->f:[Lcom/discord/views/CodeVerificationView$a;

    aget-object v2, v3, v2

    iput-object v2, p0, Lcom/discord/views/CodeVerificationView;->g:Lcom/discord/views/CodeVerificationView$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1

    :cond_0
    :goto_0
    const p2, 0x7f0d0128

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    new-instance v2, Lf/a/n/b;

    invoke-direct {v2, p0}, Lf/a/n/b;-><init>(Lcom/discord/views/CodeVerificationView;)V

    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v2, Lf/a/n/c;

    invoke-direct {v2, p0}, Lf/a/n/c;-><init>(Lcom/discord/views/CodeVerificationView;)V

    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lf/a/n/d;

    invoke-direct {v2, p0}, Lf/a/n/d;-><init>(Lcom/discord/views/CodeVerificationView;)V

    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v2, 0x6

    new-array v2, v2, [Landroid/widget/TextView;

    const v3, 0x7f0a0b56

    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "findViewById(R.id.verify_char_1)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v2, v1

    const v3, 0x7f0a0b57

    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "findViewById(R.id.verify_char_2)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v2, p2

    const p2, 0x7f0a0b58

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v3, "findViewById(R.id.verify_char_3)"

    invoke-static {p2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 p2, 0x3

    const v3, 0x7f0a0b59

    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "findViewById(R.id.verify_char_4)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v2, p2

    const p2, 0x7f0a0b5a

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v3, "findViewById(R.id.verify_char_5)"

    invoke-static {p2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    aput-object p2, v2, v0

    const/4 p2, 0x5

    const v0, 0x7f0a0b5b

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.verify_char_6)"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, p2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/views/CodeVerificationView;->d:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/discord/views/CodeVerificationView;->a(Lcom/discord/views/CodeVerificationView;)V

    goto :goto_1

    :cond_1
    new-instance v0, Lf/a/n/a;

    invoke-direct {v0, p0}, Lf/a/n/a;-><init>(Lcom/discord/views/CodeVerificationView;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :goto_1
    const p2, 0x7f0801d9

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-static {p2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/views/CodeVerificationView;->e:Landroid/graphics/drawable/Drawable;

    const p2, 0x7f0801da

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/views/CodeVerificationView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/discord/views/CodeVerificationView;->e()V

    return-void
.end method

.method public static final a(Lcom/discord/views/CodeVerificationView;)V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/discord/views/CodeVerificationView;->d()V

    :cond_1
    return-void
.end method

.method public final c(C)V
    .locals 2

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/discord/views/CodeVerificationView;->d()V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 5

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-ltz v1, :cond_1

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-static {v4}, Lx/s/r;->getLastIndex(Ljava/lang/CharSequence;)I

    move-result v4

    if-gt v1, v4, :cond_0

    iget-object v4, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_0
    const-string v1, ""

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    move v1, v3

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 v0, 0x0

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->i:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-virtual {p0}, Lcom/discord/views/CodeVerificationView;->e()V

    return-void
.end method

.method public final e()V
    .locals 5

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/discord/views/CodeVerificationView;->d:Ljava/util/List;

    invoke-static {v1}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/discord/views/CodeVerificationView;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-ltz v2, :cond_1

    check-cast v3, Landroid/widget/TextView;

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/discord/views/CodeVerificationView;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/discord/views/CodeVerificationView;->e:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move v2, v4

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 v0, 0x0

    throw v0

    :cond_2
    return-void
.end method

.method public final getCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final getOnCodeEntered()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->i:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    const-string v0, "outAttrs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->g:Lcom/discord/views/CodeVerificationView$a;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/discord/views/CodeVerificationView$a;->d:Lcom/discord/views/CodeVerificationView$a;

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/view/inputmethod/BaseInputConnection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    const/16 v1, 0x12

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const-string p1, "inputType"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public final setCode(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "code"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/views/CodeVerificationView;->d()V

    :cond_0
    return-void
.end method

.method public final setOnCodeEntered(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/CodeVerificationView;->i:Lkotlin/jvm/functions/Function1;

    return-void
.end method
