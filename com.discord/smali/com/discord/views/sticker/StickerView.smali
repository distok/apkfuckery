.class public final Lcom/discord/views/sticker/StickerView;
.super Landroid/widget/FrameLayout;
.source "StickerView.kt"


# static fields
.field public static final synthetic j:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public g:Lcom/discord/models/sticker/dto/ModelSticker;

.field public h:Lrx/Subscription;

.field public i:Lkotlinx/coroutines/Job;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/sticker/StickerView;

    const-string v3, "pngImageView"

    const-string v4, "getPngImageView()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/sticker/StickerView;

    const-string v6, "lottieView"

    const-string v7, "getLottieView()Lcom/discord/rlottie/RLottieImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/sticker/StickerView;

    const-string v6, "placeholder"

    const-string v7, "getPlaceholder()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/sticker/StickerView;->j:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p1, 0x7f0a0a60

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/sticker/StickerView;->d:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a61

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/sticker/StickerView;->e:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a62

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/views/sticker/StickerView;->f:Lkotlin/properties/ReadOnlyProperty;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d00f2

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public static final synthetic a(Lcom/discord/views/sticker/StickerView;)Lcom/discord/rlottie/RLottieImageView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/discord/views/sticker/StickerView;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getPlaceholder()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/discord/views/sticker/StickerView;)Landroid/widget/ImageView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final d(Lcom/discord/views/sticker/StickerView;Ljava/io/File;Z)Lkotlinx/coroutines/Job;
    .locals 6

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/discord/utilities/apng/ApngUtils;->INSTANCE:Lcom/discord/utilities/apng/ApngUtils;

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v2

    sget-object p0, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {p0}, Lcom/discord/utilities/dsti/StickerUtils;->getDEFAULT_STICKER_SIZE_PX()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/discord/utilities/dsti/StickerUtils;->getDEFAULT_STICKER_SIZE_PX()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/apng/ApngUtils;->renderApngFromFile(Ljava/io/File;Landroid/widget/ImageView;Ljava/lang/Integer;Ljava/lang/Integer;Z)Lkotlinx/coroutines/Job;

    move-result-object p0

    return-object p0
.end method

.method private final getLottieView()Lcom/discord/rlottie/RLottieImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/sticker/StickerView;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/rlottie/RLottieImageView;

    return-object v0
.end method

.method private final getPlaceholder()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/sticker/StickerView;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPngImageView()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/sticker/StickerView;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->i:Lkotlinx/coroutines/Job;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, v2}, Ly/a/g0;->l(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    return-void
.end method

.method public final f(Lcom/discord/models/sticker/dto/ModelSticker;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const p1, 0x7f1216f6

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(\n     \u2026ker.description}\"\n      )"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final g()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getPlaceholder()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    return-void
.end method

.method public final getSubscription()Lrx/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->h:Lrx/Subscription;

    return-object v0
.end method

.method public final h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v1, p2

    const-string/jumbo v3, "sticker"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/discord/views/sticker/StickerView;->g:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/discord/views/sticker/StickerView;->h:Lrx/Subscription;

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    return-void

    :cond_1
    iget-object v3, v0, Lcom/discord/views/sticker/StickerView;->g:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 v6, 0x0

    if-eqz v3, :cond_4

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v9

    cmp-long v3, v7, v9

    if-eqz v3, :cond_4

    :cond_2
    iget-object v3, v0, Lcom/discord/views/sticker/StickerView;->h:Lrx/Subscription;

    if-eqz v3, :cond_3

    invoke-interface {v3}, Lrx/Subscription;->unsubscribe()V

    :cond_3
    iput-object v6, v0, Lcom/discord/views/sticker/StickerView;->h:Lrx/Subscription;

    :cond_4
    iput-object v2, v0, Lcom/discord/views/sticker/StickerView;->g:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    const/16 v7, 0x8

    if-eq v3, v5, :cond_7

    const/4 v8, 0x2

    const-string v9, "Observable.combineLatest\u2026lobalAnimationSettings) }"

    const-string v10, "context"

    if-eq v3, v8, :cond_6

    const/4 v8, 0x3

    if-eq v3, v8, :cond_5

    sget-object v11, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid Sticker Format passed to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v3, Lcom/discord/views/sticker/StickerView;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPlaceholder()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->clearAnimation()V

    sget-object v3, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v7, v2}, Lcom/discord/utilities/dsti/StickerUtils;->fetchSticker(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelSticker;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3, v4, v5, v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreUserSettings;->observeStickerAnimationSettings()Lrx/Observable;

    move-result-object v4

    sget-object v5, Lf/a/n/j0/e;->d:Lf/a/n/j0/e;

    invoke-static {v3, v4, v5}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v10

    const-class v11, Lcom/discord/views/sticker/StickerView;

    new-instance v13, Lf/a/n/j0/f;

    invoke-direct {v13, v0}, Lf/a/n/j0/f;-><init>(Lcom/discord/views/sticker/StickerView;)V

    new-instance v3, Lf/a/n/j0/h;

    invoke-direct {v3, v0, v1, v2}, Lf/a/n/j0/h;-><init>(Lcom/discord/views/sticker/StickerView;Ljava/lang/Integer;Lcom/discord/models/sticker/dto/ModelSticker;)V

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x1a

    const/16 v18, 0x0

    move-object/from16 v16, v3

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPlaceholder()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v3, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v7, v2}, Lcom/discord/utilities/dsti/StickerUtils;->fetchSticker(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelSticker;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3, v4, v5, v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreUserSettings;->observeStickerAnimationSettings()Lrx/Observable;

    move-result-object v4

    sget-object v5, Lf/a/n/j0/a;->d:Lf/a/n/j0/a;

    invoke-static {v3, v4, v5}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v10

    const-class v11, Lcom/discord/views/sticker/StickerView;

    new-instance v13, Lf/a/n/j0/b;

    invoke-direct {v13, v0}, Lf/a/n/j0/b;-><init>(Lcom/discord/views/sticker/StickerView;)V

    new-instance v3, Lf/a/n/j0/d;

    invoke-direct {v3, v0, v1, v2}, Lf/a/n/j0/d;-><init>(Lcom/discord/views/sticker/StickerView;Ljava/lang/Integer;Lcom/discord/models/sticker/dto/ModelSticker;)V

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x1a

    const/16 v18, 0x0

    move-object/from16 v16, v3

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_1

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPlaceholder()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual/range {p0 .. p1}, Lcom/discord/views/sticker/StickerView;->f(Lcom/discord/models/sticker/dto/ModelSticker;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v7

    sget-object v1, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v5, 0x6

    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/dsti/StickerUtils;->getCDNAssetUrl$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x7c

    const/4 v12, 0x0

    move-object v4, v7

    move v7, v1

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->h:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/views/sticker/StickerView;->h:Lrx/Subscription;

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/views/sticker/StickerView;->g:Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getLottieView()Lcom/discord/rlottie/RLottieImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/discord/views/sticker/StickerView;->getPngImageView()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void
.end method
