.class public final enum Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;
.super Ljava/lang/Enum;
.source "ActiveSubscriptionView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/views/ActiveSubscriptionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActiveSubscriptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

.field public static final enum PREMIUM:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

.field public static final enum PREMIUM_AND_PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

.field public static final enum PREMIUM_CLASSIC:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

.field public static final enum PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;


# instance fields
.field private final headerBackground:I

.field private final headerBackgroundError:I

.field private final headerBackgroundResub:I

.field private final headerImage:I

.field private final headerImageError:I

.field private final headerImageResub:I

.field private final headerLogo:I


# direct methods
.method public static constructor <clinit>()V
    .locals 22

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    new-instance v11, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const-string v2, "PREMIUM_CLASSIC"

    const/4 v3, 0x0

    const v4, 0x7f08052e

    const v5, 0x7f08052c

    const v6, 0x7f08052f

    const v7, 0x7f08052d

    const v8, 0x7f0800cb

    const v9, 0x7f0800d1

    const v10, 0x7f0800d0

    move-object v1, v11

    invoke-direct/range {v1 .. v10}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;-><init>(Ljava/lang/String;IIIIIIII)V

    sput-object v11, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_CLASSIC:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const/4 v1, 0x0

    aput-object v11, v0, v1

    new-instance v1, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const-string v13, "PREMIUM"

    const/4 v14, 0x1

    const v15, 0x7f080539

    const v16, 0x7f080537

    const v17, 0x7f08053a

    const v18, 0x7f080538

    const v19, 0x7f0800cf

    const v20, 0x7f0800d1

    const v21, 0x7f0800d0

    move-object v12, v1

    invoke-direct/range {v12 .. v21}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;-><init>(Ljava/lang/String;IIIIIIII)V

    sput-object v1, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const-string v4, "PREMIUM_GUILD"

    const/4 v5, 0x2

    const v6, 0x7f080532

    const v7, 0x7f080530

    const v8, 0x7f080533

    const v9, 0x7f080531

    const v10, 0x7f0800cc

    const v11, 0x7f0800ce

    const v12, 0x7f0800cd

    move-object v3, v1

    invoke-direct/range {v3 .. v12}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;-><init>(Ljava/lang/String;IIIIIIII)V

    sput-object v1, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const-string v4, "PREMIUM_AND_PREMIUM_GUILD"

    const/4 v5, 0x3

    const v6, 0x7f080507

    const v7, 0x7f08052b

    const v8, 0x7f08052b

    const v9, 0x7f08052b

    const v10, 0x7f0800ca

    const v11, 0x7f0800d1

    const v12, 0x7f0800d0

    move-object v3, v1

    invoke-direct/range {v3 .. v12}, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;-><init>(Ljava/lang/String;IIIIIIII)V

    sput-object v1, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_AND_PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->$VALUES:[Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIIII)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIIII)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerLogo:I

    iput p4, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerImage:I

    iput p5, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerImageResub:I

    iput p6, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerImageError:I

    iput p7, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerBackground:I

    iput p8, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerBackgroundResub:I

    iput p9, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerBackgroundError:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;
    .locals 1

    const-class v0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    return-object p0
.end method

.method public static values()[Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;
    .locals 1

    sget-object v0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->$VALUES:[Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    invoke-virtual {v0}, [Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    return-object v0
.end method


# virtual methods
.method public final getHeaderBackground()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerBackground:I

    return v0
.end method

.method public final getHeaderBackgroundError()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerBackgroundError:I

    return v0
.end method

.method public final getHeaderBackgroundResub()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerBackgroundResub:I

    return v0
.end method

.method public final getHeaderImage()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerImage:I

    return v0
.end method

.method public final getHeaderImageError()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerImageError:I

    return v0
.end method

.method public final getHeaderImageResub()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerImageResub:I

    return v0
.end method

.method public final getHeaderLogo()I
    .locals 1

    iget v0, p0, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->headerLogo:I

    return v0
.end method
