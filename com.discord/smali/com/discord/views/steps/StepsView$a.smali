.class public final Lcom/discord/views/steps/StepsView$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/views/steps/StepsView;->c(ILkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:I

.field public final synthetic f:Ljava/lang/Object;

.field public final synthetic g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(IILjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    iput p1, p0, Lcom/discord/views/steps/StepsView$a;->d:I

    iput p2, p0, Lcom/discord/views/steps/StepsView$a;->e:I

    iput-object p3, p0, Lcom/discord/views/steps/StepsView$a;->f:Ljava/lang/Object;

    iput-object p4, p0, Lcom/discord/views/steps/StepsView$a;->g:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget v0, p0, Lcom/discord/views/steps/StepsView$a;->d:I

    const-string v1, "it"

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView$a;->f:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget v2, p0, Lcom/discord/views/steps/StepsView$a;->e:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/steps/StepsView$b;

    iget-object v0, v0, Lcom/discord/views/steps/StepsView$b;->f:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_0

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/views/steps/StepsView$a;->g:Ljava/lang/Object;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_0
    return-void

    :cond_1
    const/4 p1, 0x0

    throw p1

    :cond_2
    iget-object v0, p0, Lcom/discord/views/steps/StepsView$a;->g:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget v3, p0, Lcom/discord/views/steps/StepsView$a;->e:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/steps/StepsView$b;

    iget-object v0, v0, Lcom/discord/views/steps/StepsView$b;->d:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_3

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/discord/views/steps/StepsView$a;->f:Ljava/lang/Object;

    check-cast p1, Lcom/discord/views/steps/StepsView;

    iget-object p1, p1, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result p1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView$a;->f:Ljava/lang/Object;

    check-cast v0, Lcom/discord/views/steps/StepsView;

    iget-object v0, v0, Lcom/discord/views/steps/StepsView;->j:Lcom/discord/views/steps/StepsView$d;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/views/steps/StepsView$d;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq p1, v0, :cond_5

    :cond_4
    iget-object p1, p0, Lcom/discord/views/steps/StepsView$a;->f:Ljava/lang/Object;

    check-cast p1, Lcom/discord/views/steps/StepsView;

    iget-object p1, p1, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    :cond_5
    :goto_1
    return-void
.end method
