.class public final Lcom/discord/views/steps/StepsView;
.super Landroid/widget/RelativeLayout;
.source "StepsView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/steps/StepsView$b;,
        Lcom/discord/views/steps/StepsView$d;,
        Lcom/discord/views/steps/StepsView$c;
    }
.end annotation


# static fields
.field public static final synthetic k:I


# instance fields
.field public d:Lcom/discord/views/LoadingButton;

.field public e:Landroid/view/View;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/TextView;

.field public h:Lcom/discord/utilities/simple_pager/SimplePager;

.field public i:Landroid/widget/LinearLayout;

.field public j:Lcom/discord/views/steps/StepsView$d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p2, "layout_inflater"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type android.view.LayoutInflater"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Landroid/view/LayoutInflater;

    const p2, 0x7f0d0162

    const/4 v1, 0x1

    invoke-virtual {p1, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p1, 0x7f0a0a2e

    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.steps_done)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/LoadingButton;

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    const p1, 0x7f0a0a2d

    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.steps_close)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->e:Landroid/view/View;

    const p1, 0x7f0a0a2f

    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.steps_next)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->f:Landroid/widget/Button;

    const p1, 0x7f0a0a2c

    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.steps_cancel)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->g:Landroid/widget/TextView;

    const p1, 0x7f0a0a30

    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.steps_viewpager)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/utilities/simple_pager/SimplePager;

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    const p1, 0x7f0a0a2b

    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.steps_button_container)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/discord/views/steps/StepsView;->setIsLoading(Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/views/steps/StepsView$d;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/views/steps/StepsView$d;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClose"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPageSelected"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/steps/StepsView;->j:Lcom/discord/views/steps/StepsView$d;

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/simple_pager/SimplePager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    iget-object p1, p0, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->clearOnPageChangeListeners()V

    iget-object p1, p0, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    new-instance v0, Lcom/discord/views/steps/StepsView$e;

    invoke-direct {v0, p0, p3, p2}, Lcom/discord/views/steps/StepsView$e;-><init>(Lcom/discord/views/steps/StepsView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/discord/views/steps/StepsView;->c(ILkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->j:Lcom/discord/views/steps/StepsView$d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/views/steps/StepsView$d;->getCount()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    :cond_1
    return-void
.end method

.method public final c(ILkotlin/jvm/functions/Function0;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->e:Landroid/view/View;

    new-instance v1, Lcom/discord/views/steps/StepsView$f;

    invoke-direct {v1, p2}, Lcom/discord/views/steps/StepsView$f;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->j:Lcom/discord/views/steps/StepsView$d;

    if-eqz v0, :cond_c

    iget-object v0, v0, Lcom/discord/views/steps/StepsView$d;->a:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->h:Lcom/discord/utilities/simple_pager/SimplePager;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/views/steps/StepsView$b;

    iget-boolean v2, v2, Lcom/discord/views/steps/StepsView$b;->i:Z

    invoke-virtual {v1, v2}, Lcom/discord/utilities/simple_pager/SimplePager;->setScrollingEnabled(Z)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->f:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/views/steps/StepsView$b;

    iget v3, v3, Lcom/discord/views/steps/StepsView$b;->a:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/views/steps/StepsView$b;

    iget v3, v3, Lcom/discord/views/steps/StepsView$b;->b:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/views/steps/StepsView$b;

    iget v3, v3, Lcom/discord/views/steps/StepsView$b;->c:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->f:Landroid/widget/Button;

    new-instance v2, Lcom/discord/views/steps/StepsView$a;

    const/4 v3, 0x0

    invoke-direct {v2, v3, p1, p0, v0}, Lcom/discord/views/steps/StepsView$a;-><init>(IILjava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->g:Landroid/widget/TextView;

    new-instance v2, Lcom/discord/views/steps/StepsView$g;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/discord/views/steps/StepsView$g;-><init>(Lcom/discord/views/steps/StepsView;Ljava/util/List;ILkotlin/jvm/functions/Function0;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    new-instance v2, Lcom/discord/views/steps/StepsView$a;

    const/4 v4, 0x1

    invoke-direct {v2, v4, p1, v0, p2}, Lcom/discord/views/steps/StepsView$a;-><init>(IILjava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/discord/views/steps/StepsView;->j:Lcom/discord/views/steps/StepsView$d;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/views/steps/StepsView$d;->getCount()I

    move-result p2

    sub-int/2addr p2, v4

    if-ne p1, p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->f:Landroid/widget/Button;

    if-nez p2, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/views/steps/StepsView$b;

    iget-boolean v2, v2, Lcom/discord/views/steps/StepsView$b;->g:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    const/16 v5, 0x8

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    const/16 v2, 0x8

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/views/steps/StepsView;->g:Landroid/widget/TextView;

    if-nez p2, :cond_3

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/steps/StepsView$b;

    iget-boolean p1, p1, Lcom/discord/views/steps/StepsView$b;->h:Z

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_4

    const/4 p1, 0x0

    goto :goto_4

    :cond_4
    const/16 p1, 0x8

    :goto_4
    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    if-eqz p2, :cond_5

    const/4 p2, 0x0

    goto :goto_5

    :cond_5
    const/16 p2, 0x8

    :goto_5
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/views/steps/StepsView;->i:Landroid/widget/LinearLayout;

    iget-object p2, p0, Lcom/discord/views/steps/StepsView;->f:Landroid/widget/Button;

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-nez p2, :cond_6

    const/4 p2, 0x1

    goto :goto_6

    :cond_6
    const/4 p2, 0x0

    :goto_6
    if-nez p2, :cond_a

    iget-object p2, p0, Lcom/discord/views/steps/StepsView;->g:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-nez p2, :cond_7

    const/4 p2, 0x1

    goto :goto_7

    :cond_7
    const/4 p2, 0x0

    :goto_7
    if-nez p2, :cond_a

    iget-object p2, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-nez p2, :cond_8

    const/4 p2, 0x1

    goto :goto_8

    :cond_8
    const/4 p2, 0x0

    :goto_8
    if-eqz p2, :cond_9

    goto :goto_9

    :cond_9
    const/4 v4, 0x0

    :cond_a
    :goto_9
    if-eqz v4, :cond_b

    goto :goto_a

    :cond_b
    const/16 v3, 0x8

    :goto_a
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_c
    return-void
.end method

.method public final setIsDoneButtonEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    return-void
.end method

.method public final setIsLoading(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->d:Lcom/discord/views/LoadingButton;

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    return-void
.end method

.method public final setIsNextButtonEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/steps/StepsView;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
