.class public final enum Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;
.super Ljava/lang/Enum;
.source "PremiumGuildConfirmationView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

.field public static final enum e:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

.field public static final enum f:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

.field public static final synthetic g:[Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    new-instance v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    const-string v2, "UPGRADE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->d:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    const-string v2, "DOWNGRADE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->e:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    const-string v2, "NONE"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->f:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->g:[Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;
    .locals 1

    const-class v0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    return-object p0
.end method

.method public static values()[Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;
    .locals 1

    sget-object v0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->g:[Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    invoke-virtual {v0}, [Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    return-object v0
.end method
