.class public final Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;
.super Landroidx/cardview/widget/CardView;
.source "PremiumGuildConfirmationView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;
    }
.end annotation


# instance fields
.field public final d:Landroid/widget/ImageView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/ImageView;

.field public final g:Lcom/google/android/material/chip/Chip;

.field public final h:Lcom/google/android/material/chip/Chip;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d0142

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f0a07a0

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.premiu\u2026uild_confirmation_avatar)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->d:Landroid/widget/ImageView;

    const p1, 0x7f0a07a1

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.premiu\u2026_confirmation_guild_name)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->e:Landroid/widget/TextView;

    const p1, 0x7f0a07a4

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.premiu\u2026nfirmation_upgrade_arrow)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->f:Landroid/widget/ImageView;

    const p1, 0x7f0a07a2

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.premiu\u2026tion_level_info_previous)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/google/android/material/chip/Chip;

    iput-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->g:Lcom/google/android/material/chip/Chip;

    const p1, 0x7f0a07a3

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.premiu\u2026ation_level_info_upgrade)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/google/android/material/chip/Chip;

    iput-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->h:Lcom/google/android/material/chip/Chip;

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/models/domain/ModelGuild;I)V
    .locals 11

    sget-object v0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->e:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    sget-object v1, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->f:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    const-string v2, "guild"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, p2

    sget-object p2, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    invoke-virtual {p2, v2}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->getPremiumGuildTier(I)I

    move-result p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    if-le p2, v2, :cond_1

    sget-object v2, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;->d:Lcom/discord/views/premiumguild/PremiumGuildConfirmationView$a;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    if-ge p2, v2, :cond_2

    move-object v2, v0

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    iget-object v4, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->d:Landroid/widget/ImageView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xe

    const/4 v10, 0x0

    move-object v5, p1

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Lcom/discord/models/domain/ModelGuild;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1c

    invoke-static/range {v4 .. v10}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object v4, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->g:Lcom/google/android/material/chip/Chip;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->b(I)I

    move-result v6

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/material/chip/Chip;->setChipIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->g:Lcom/google/android/material/chip/Chip;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->c(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->f:Landroid/widget/ImageView;

    const/4 v4, 0x1

    if-eq v2, v1, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    const/16 v6, 0x8

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    goto :goto_3

    :cond_4
    const/16 v5, 0x8

    :goto_3
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->h:Lcom/google/android/material/chip/Chip;

    if-eq v2, v1, :cond_5

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_6

    goto :goto_5

    :cond_6
    const/16 v3, 0x8

    :goto_5
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    if-ne v2, v1, :cond_7

    return-void

    :cond_7
    if-ne v2, v0, :cond_8

    const p1, 0x7f060238

    goto :goto_6

    :cond_8
    const p1, 0x7f060200

    :goto_6
    iget-object v0, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->f:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColorResource(Landroid/widget/ImageView;I)V

    iget-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->h:Lcom/google/android/material/chip/Chip;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->b(I)I

    move-result v1

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/chip/Chip;->setChipIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->h:Lcom/google/android/material/chip/Chip;

    invoke-virtual {p0, p2}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->c(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final b(I)I
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "context"

    const/4 v4, 0x2

    if-eq p1, v1, :cond_2

    if-eq p1, v4, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0402a8

    invoke-static {p1, v1, v0, v4, v2}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0402a7

    invoke-static {p1, v1, v0, v4, v2}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0402a6

    invoke-static {p1, v1, v0, v4, v2}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_3
    const v0, 0x7f08033c

    :goto_0
    return v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1213b3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1213b2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1213b1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f121322

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    const-string/jumbo v0, "when (premiumTier) {\n   \u2026       else -> \"\"\n      }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
