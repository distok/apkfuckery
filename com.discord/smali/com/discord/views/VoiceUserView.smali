.class public final Lcom/discord/views/VoiceUserView;
.super Landroid/widget/FrameLayout;
.source "VoiceUserView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/views/VoiceUserView$a;
    }
.end annotation


# static fields
.field public static final synthetic m:[Lkotlin/reflect/KProperty;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public f:Lcom/discord/views/VoiceUserView$a;

.field public final g:Lcom/discord/utilities/anim/RingAnimator;

.field public h:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

.field public i:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/graphics/Bitmap;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/views/VoiceUserView;

    const-string v3, "avatar"

    const-string v4, "getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/views/VoiceUserView;

    const-string/jumbo v6, "username"

    const-string v7, "getUsername()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/views/VoiceUserView;->m:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v1, 0x7f0a0b98

    invoke-static {p0, v1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/views/VoiceUserView;->d:Lkotlin/properties/ReadOnlyProperty;

    const v1, 0x7f0a0ba5

    invoke-static {p0, v1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/views/VoiceUserView;->e:Lkotlin/properties/ReadOnlyProperty;

    new-instance v1, Lcom/discord/utilities/anim/RingAnimator;

    new-instance v2, Lf/a/n/d0;

    invoke-direct {v2, p0}, Lf/a/n/d0;-><init>(Lcom/discord/views/VoiceUserView;)V

    invoke-direct {v1, p0, v2}, Lcom/discord/utilities/anim/RingAnimator;-><init>(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    iput-object v1, p0, Lcom/discord/views/VoiceUserView;->g:Lcom/discord/utilities/anim/RingAnimator;

    sget-object v1, Lf/a/n/c0;->d:Lf/a/n/c0;

    iput-object v1, p0, Lcom/discord/views/VoiceUserView;->i:Lkotlin/jvm/functions/Function1;

    const v1, 0x7f0d016c

    invoke-static {p1, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    if-eqz p2, :cond_1

    sget-object v1, Lcom/discord/R$a;->VoiceUserView:[I

    invoke-virtual {p1, p2, v1, v0, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026able.VoiceUserView, 0, 0)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    float-to-int p2, p2

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p1, v0, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    if-lez p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/discord/views/VoiceUserView;->setAvatarSize(I)V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/discord/views/VoiceUserView;->setFadeWhenDisconnected(Z)V

    invoke-virtual {p0, v0}, Lcom/discord/views/VoiceUserView;->setAnimateAvatarWhenRinging(Z)V

    :cond_1
    return-void
.end method

.method private final getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/VoiceUserView;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/VoiceUserView;->m:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getUsername()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/views/VoiceUserView;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/views/VoiceUserView;->m:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final setVoiceState(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V
    .locals 2

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSelfMute()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isMute()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSuppress()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isSpeaking()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object p1, Lcom/discord/views/VoiceUserView$a;->e:Lcom/discord/views/VoiceUserView$a;

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object p1, Lcom/discord/views/VoiceUserView$a;->g:Lcom/discord/views/VoiceUserView$a;

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result p1

    if-nez p1, :cond_4

    sget-object p1, Lcom/discord/views/VoiceUserView$a;->h:Lcom/discord/views/VoiceUserView$a;

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    sget-object p1, Lcom/discord/views/VoiceUserView$a;->f:Lcom/discord/views/VoiceUserView$a;

    goto :goto_1

    :cond_5
    sget-object p1, Lcom/discord/views/VoiceUserView$a;->d:Lcom/discord/views/VoiceUserView$a;

    :goto_1
    invoke-direct {p0, p1}, Lcom/discord/views/VoiceUserView;->setVoiceState(Lcom/discord/views/VoiceUserView$a;)V

    return-void
.end method

.method private final setVoiceState(Lcom/discord/views/VoiceUserView$a;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/views/VoiceUserView;->f:Lcom/discord/views/VoiceUserView$a;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/discord/views/VoiceUserView;->f:Lcom/discord/views/VoiceUserView$a;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const v0, 0x106000d

    goto :goto_0

    :cond_1
    const v0, 0x7f080200

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    sget-object v0, Lcom/discord/views/VoiceUserView$a;->h:Lcom/discord/views/VoiceUserView$a;

    if-ne p1, v0, :cond_2

    iget-boolean p1, p0, Lcom/discord/views/VoiceUserView;->k:Z

    if-eqz p1, :cond_2

    const p1, 0x3e99999a    # 0.3f

    goto :goto_1

    :cond_2
    const/high16 p1, 0x3f800000    # 1.0f

    :goto_1
    invoke-direct {p0}, Lcom/discord/views/VoiceUserView;->getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    iget-object p1, p0, Lcom/discord/views/VoiceUserView;->g:Lcom/discord/utilities/anim/RingAnimator;

    invoke-virtual {p1}, Lcom/discord/utilities/anim/RingAnimator;->onUpdate()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V
    .locals 8
    .param p2    # I
        .annotation build Landroidx/annotation/DimenRes;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string/jumbo v0, "voiceUser"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/views/VoiceUserView;->h:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/discord/views/VoiceUserView;->h:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-lez v0, :cond_2

    invoke-static {v0}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v0

    iget-object v3, p0, Lcom/discord/views/VoiceUserView;->h:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v4, 0x2

    invoke-static {v3, v1, v0, v4, v2}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/discord/views/VoiceUserView;->h:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    :goto_1
    const/4 v3, 0x6

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v2, v0

    iget-object v0, p0, Lcom/discord/views/VoiceUserView;->j:Ljava/lang/String;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    iput-object v2, p0, Lcom/discord/views/VoiceUserView;->j:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/views/VoiceUserView;->getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    new-instance v4, Lcom/discord/views/VoiceUserView$c;

    invoke-direct {v4, p0}, Lcom/discord/views/VoiceUserView$c;-><init>(Lcom/discord/views/VoiceUserView;)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move v3, p2

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :cond_4
    invoke-direct {p0}, Lcom/discord/views/VoiceUserView;->getUsername()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/discord/views/VoiceUserView;->setVoiceState(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V

    return-void
.end method

.method public final setAnimateAvatarWhenRinging(Z)V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iput-boolean p1, p0, Lcom/discord/views/VoiceUserView;->l:Z

    return-void
.end method

.method public final setAvatarSize(I)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    new-instance v0, Lcom/discord/views/VoiceUserView$b;

    invoke-direct {v0, p1}, Lcom/discord/views/VoiceUserView$b;-><init>(I)V

    invoke-direct {p0}, Lcom/discord/views/VoiceUserView;->getAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/views/VoiceUserView$b;->invoke(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/views/VoiceUserView;->getUsername()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/views/VoiceUserView$b;->invoke(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    return-void
.end method

.method public final setFadeWhenDisconnected(Z)V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iput-boolean p1, p0, Lcom/discord/views/VoiceUserView;->k:Z

    return-void
.end method

.method public final setOnBitmapLoadedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/graphics/Bitmap;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onBitmapLoadedListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/views/VoiceUserView;->i:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public setSelected(Z)V
    .locals 7

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->isSelected()Z

    move-result v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setSelected(Z)V

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/views/VoiceUserView;->getUsername()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->isSelected()Z

    move-result v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeBy$default(Landroid/view/View;ZJILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final setVoiceUser(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const v0, 0x7f07006c

    invoke-virtual {p0, p1, v0}, Lcom/discord/views/VoiceUserView;->a(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V

    return-void
.end method
