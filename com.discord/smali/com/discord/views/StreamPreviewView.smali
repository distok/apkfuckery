.class public final Lcom/discord/views/StreamPreviewView;
.super Landroid/widget/FrameLayout;
.source "StreamPreviewView.kt"


# instance fields
.field public final d:Lcom/facebook/drawee/view/SimpleDraweeView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d0163

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f0a0a69

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.stream_preview_image)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/views/StreamPreviewView;->d:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a0a6a

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.stream_preview_overlay_text)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/views/StreamPreviewView;->e:Landroid/widget/TextView;

    const p1, 0x7f0a0a6b

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.stream\u2026view_placeholder_caption)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/views/StreamPreviewView;->f:Landroid/widget/TextView;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private final setCaptionText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/StreamPreviewView;->f:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setImage(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/StreamPreviewView;->d:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Ljava/lang/String;)V

    return-void
.end method

.method private final setOverlayCaptionText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/views/StreamPreviewView;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Z)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "preview"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "joinability"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Fetching;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f121770

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setCaptionText(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;

    invoke-virtual {p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;->getUrl()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setImage(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/discord/views/StreamPreviewView;->setCaptionText(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/discord/views/StreamPreviewView;->setImage(Ljava/lang/String;)V

    sget-object p1, Lcom/discord/utilities/streams/StreamContext$Joinability;->CAN_CONNECT:Lcom/discord/utilities/streams/StreamContext$Joinability;

    if-ne p2, p1, :cond_2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f121769

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setCaptionText(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lcom/discord/views/StreamPreviewView;->setCaptionText(Ljava/lang/String;)V

    :goto_0
    if-eqz p3, :cond_3

    const p1, 0x7f121a6f

    invoke-static {p0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setOverlayCaptionText(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_6

    const/4 p2, 0x1

    if-eq p1, p2, :cond_5

    const/4 p2, 0x2

    if-ne p1, p2, :cond_4

    const p1, 0x7f120443

    invoke-static {p0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setOverlayCaptionText(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_5
    const p1, 0x7f1218b4

    invoke-static {p0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setOverlayCaptionText(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const p1, 0x7f120ea4

    invoke-static {p0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/views/StreamPreviewView;->setOverlayCaptionText(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
