.class public final Lcom/discord/views/JoinVoiceChannelButton;
.super Lcom/google/android/material/button/MaterialButton;
.source "JoinVoiceChannelButton.kt"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/material/button/MaterialButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p1, 0x7f1204fc

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/material/button/MaterialButton;I)V
    .locals 1
    .param p2    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setAlpha(F)V

    new-instance v0, Lcom/discord/views/JoinVoiceChannelButton$a;

    invoke-direct {v0, p1, p2}, Lcom/discord/views/JoinVoiceChannelButton$a;-><init>(Lcom/google/android/material/button/MaterialButton;I)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
