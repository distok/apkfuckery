.class public final Lcom/discord/floating_view_manager/FloatingViewManager$f;
.super Ljava/lang/Object;
.source "FloatingViewManager.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/floating_view_manager/FloatingViewManager;->c(Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;IILrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/floating_view_manager/FloatingViewManager;


# direct methods
.method public constructor <init>(Lcom/discord/floating_view_manager/FloatingViewManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$f;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 6

    move-object v2, p1

    check-cast v2, Ljava/lang/Throwable;

    iget-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$f;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    iget-object v0, p1, Lcom/discord/floating_view_manager/FloatingViewManager;->c:Lcom/discord/utilities/logging/Logger;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "error while subscribing to componentPausedObservable for hiding floating view"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return-void
.end method
