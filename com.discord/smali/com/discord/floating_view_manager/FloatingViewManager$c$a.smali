.class public final Lcom/discord/floating_view_manager/FloatingViewManager$c$a;
.super Ljava/lang/Object;
.source "View.kt"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/floating_view_manager/FloatingViewManager$c;->onLayoutChange(Landroid/view/View;IIIIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/floating_view_manager/FloatingViewManager$c;


# direct methods
.method public constructor <init>(Lcom/discord/floating_view_manager/FloatingViewManager$c;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c$a;->d:Lcom/discord/floating_view_manager/FloatingViewManager$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c$a;->d:Lcom/discord/floating_view_manager/FloatingViewManager$c;

    iget-object p2, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    iget-object p3, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->f:Landroid/view/View;

    iget-object p4, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->e:Landroid/view/View;

    iget-object p5, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->g:Lcom/discord/floating_view_manager/FloatingViewGravity;

    iget p6, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->h:I

    iget p7, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->i:I

    invoke-static/range {p2 .. p7}, Lcom/discord/floating_view_manager/FloatingViewManager;->a(Lcom/discord/floating_view_manager/FloatingViewManager;Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;II)V

    iget-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c$a;->d:Lcom/discord/floating_view_manager/FloatingViewManager$c;

    iget-object p1, p1, Lcom/discord/floating_view_manager/FloatingViewManager$c;->f:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
