.class public final Lcom/discord/floating_view_manager/FloatingViewManager$g;
.super Ljava/lang/Object;
.source "FloatingViewManager.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/floating_view_manager/FloatingViewManager;->c(Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;IILrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/floating_view_manager/FloatingViewManager;

.field public final synthetic e:Landroid/view/View;

.field public final synthetic f:Landroid/view/View;

.field public final synthetic g:Lcom/discord/floating_view_manager/FloatingViewGravity;

.field public final synthetic h:I

.field public final synthetic i:I


# direct methods
.method public constructor <init>(Lcom/discord/floating_view_manager/FloatingViewManager;Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;II)V
    .locals 0

    iput-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    iput-object p2, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->e:Landroid/view/View;

    iput-object p3, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->f:Landroid/view/View;

    iput-object p4, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->g:Lcom/discord/floating_view_manager/FloatingViewGravity;

    iput p5, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->h:I

    iput p6, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->i:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 6

    iget-object v0, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    iget-object v1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->e:Landroid/view/View;

    iget-object v2, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->f:Landroid/view/View;

    iget-object v3, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->g:Lcom/discord/floating_view_manager/FloatingViewGravity;

    iget v4, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->h:I

    iget v5, p0, Lcom/discord/floating_view_manager/FloatingViewManager$g;->i:I

    invoke-static/range {v0 .. v5}, Lcom/discord/floating_view_manager/FloatingViewManager;->a(Lcom/discord/floating_view_manager/FloatingViewManager;Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;II)V

    const/4 v0, 0x1

    return v0
.end method
