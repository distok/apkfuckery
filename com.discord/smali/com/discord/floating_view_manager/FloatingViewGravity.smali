.class public final enum Lcom/discord/floating_view_manager/FloatingViewGravity;
.super Ljava/lang/Enum;
.source "FloatingViewGravity.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/floating_view_manager/FloatingViewGravity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/floating_view_manager/FloatingViewGravity;

.field public static final enum BOTTOM:Lcom/discord/floating_view_manager/FloatingViewGravity;

.field public static final enum CENTER:Lcom/discord/floating_view_manager/FloatingViewGravity;

.field public static final enum TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/floating_view_manager/FloatingViewGravity;

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewGravity;

    const-string v2, "TOP"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/floating_view_manager/FloatingViewGravity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/floating_view_manager/FloatingViewGravity;->TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewGravity;

    const-string v2, "CENTER"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/floating_view_manager/FloatingViewGravity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/floating_view_manager/FloatingViewGravity;->CENTER:Lcom/discord/floating_view_manager/FloatingViewGravity;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewGravity;

    const-string v2, "BOTTOM"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/floating_view_manager/FloatingViewGravity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/floating_view_manager/FloatingViewGravity;->BOTTOM:Lcom/discord/floating_view_manager/FloatingViewGravity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewGravity;->$VALUES:[Lcom/discord/floating_view_manager/FloatingViewGravity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/floating_view_manager/FloatingViewGravity;
    .locals 1

    const-class v0, Lcom/discord/floating_view_manager/FloatingViewGravity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/floating_view_manager/FloatingViewGravity;

    return-object p0
.end method

.method public static values()[Lcom/discord/floating_view_manager/FloatingViewGravity;
    .locals 1

    sget-object v0, Lcom/discord/floating_view_manager/FloatingViewGravity;->$VALUES:[Lcom/discord/floating_view_manager/FloatingViewGravity;

    invoke-virtual {v0}, [Lcom/discord/floating_view_manager/FloatingViewGravity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/floating_view_manager/FloatingViewGravity;

    return-object v0
.end method
