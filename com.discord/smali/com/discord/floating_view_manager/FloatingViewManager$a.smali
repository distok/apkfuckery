.class public final Lcom/discord/floating_view_manager/FloatingViewManager$a;
.super Ljava/lang/Object;
.source "FloatingViewManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/floating_view_manager/FloatingViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/ViewGroup;

.field public final c:Landroid/view/ViewTreeObserver$OnPreDrawListener;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
    .locals 1

    const-string v0, "floatingView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ancestorViewGroup"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ancestorPreDrawListener"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$a;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/discord/floating_view_manager/FloatingViewManager$a;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/discord/floating_view_manager/FloatingViewManager$a;->c:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    return-void
.end method
