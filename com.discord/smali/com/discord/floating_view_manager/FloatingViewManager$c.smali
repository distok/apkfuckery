.class public final Lcom/discord/floating_view_manager/FloatingViewManager$c;
.super Ljava/lang/Object;
.source "View.kt"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/floating_view_manager/FloatingViewManager;->c(Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;IILrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/floating_view_manager/FloatingViewManager;

.field public final synthetic e:Landroid/view/View;

.field public final synthetic f:Landroid/view/View;

.field public final synthetic g:Lcom/discord/floating_view_manager/FloatingViewGravity;

.field public final synthetic h:I

.field public final synthetic i:I


# direct methods
.method public constructor <init>(Lcom/discord/floating_view_manager/FloatingViewManager;Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;II)V
    .locals 0

    iput-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    iput-object p2, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->e:Landroid/view/View;

    iput-object p3, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->f:Landroid/view/View;

    iput-object p4, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->g:Lcom/discord/floating_view_manager/FloatingViewGravity;

    iput p5, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->h:I

    iput p6, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->i:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->e:Landroid/view/View;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p3, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->d:Lcom/discord/floating_view_manager/FloatingViewManager;

    iget-object p4, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->f:Landroid/view/View;

    iget-object p5, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->e:Landroid/view/View;

    iget-object p6, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->g:Lcom/discord/floating_view_manager/FloatingViewGravity;

    iget p7, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->h:I

    iget p8, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->i:I

    invoke-static/range {p3 .. p8}, Lcom/discord/floating_view_manager/FloatingViewManager;->a(Lcom/discord/floating_view_manager/FloatingViewManager;Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;II)V

    iget-object p1, p0, Lcom/discord/floating_view_manager/FloatingViewManager$c;->f:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/discord/floating_view_manager/FloatingViewManager$c$a;

    invoke-direct {p2, p0}, Lcom/discord/floating_view_manager/FloatingViewManager$c$a;-><init>(Lcom/discord/floating_view_manager/FloatingViewManager$c;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :goto_0
    return-void
.end method
