.class public final Lcom/discord/gateway/GatewaySocket$Timer;
.super Ljava/lang/Object;
.source "GatewaySocket.kt"

# interfaces
.implements Lcom/discord/utilities/networking/Backoff$Scheduler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/GatewaySocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Timer"
.end annotation


# instance fields
.field private final scheduler:Lrx/Scheduler;

.field private subscription:Lrx/Subscription;


# direct methods
.method public constructor <init>(Lrx/Scheduler;)V
    .locals 1

    const-string v0, "scheduler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$Timer;->scheduler:Lrx/Scheduler;

    return-void
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/gateway/GatewaySocket$Timer;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/gateway/GatewaySocket$Timer;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    return-void
.end method

.method public static synthetic cancel$default(Lcom/discord/gateway/GatewaySocket$Timer;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final cancel(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method public final getPending()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final postInterval(Lkotlin/jvm/functions/Function0;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;J)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel()V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v6

    move-wide v1, p2

    move-wide v3, p2

    invoke-static/range {v1 .. v6}, Lrx/Observable;->B(JJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p2

    iget-object p3, p0, Lcom/discord/gateway/GatewaySocket$Timer;->scheduler:Lrx/Scheduler;

    invoke-virtual {p2, p3}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p2

    new-instance p3, Lcom/discord/gateway/GatewaySocket$Timer$postInterval$1;

    invoke-direct {p3, p1}, Lcom/discord/gateway/GatewaySocket$Timer$postInterval$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    sget-object p1, Lcom/discord/gateway/GatewaySocket$Timer$postInterval$2;->INSTANCE:Lcom/discord/gateway/GatewaySocket$Timer$postInterval$2;

    invoke-virtual {p2, p3, p1}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    return-void
.end method

.method public schedule(Lkotlin/jvm/functions/Function0;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;J)V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel()V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p2, p3, v0}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p2

    iget-object p3, p0, Lcom/discord/gateway/GatewaySocket$Timer;->scheduler:Lrx/Scheduler;

    invoke-virtual {p2, p3}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p2

    new-instance p3, Lcom/discord/gateway/GatewaySocket$Timer$schedule$1;

    invoke-direct {p3, p0, p1}, Lcom/discord/gateway/GatewaySocket$Timer$schedule$1;-><init>(Lcom/discord/gateway/GatewaySocket$Timer;Lkotlin/jvm/functions/Function0;)V

    sget-object p1, Lcom/discord/gateway/GatewaySocket$Timer$schedule$2;->INSTANCE:Lcom/discord/gateway/GatewaySocket$Timer$schedule$2;

    invoke-virtual {p2, p3, p1}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$Timer;->subscription:Lrx/Subscription;

    return-void
.end method
