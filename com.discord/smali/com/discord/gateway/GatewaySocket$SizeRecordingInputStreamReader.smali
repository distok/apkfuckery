.class public final Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;
.super Ljava/io/Reader;
.source "GatewaySocket.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/GatewaySocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SizeRecordingInputStreamReader"
.end annotation


# instance fields
.field private size:I

.field private final source:Ljava/io/InputStreamReader;


# direct methods
.method public constructor <init>(Ljava/io/InputStreamReader;I)V
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->source:Ljava/io/InputStreamReader;

    iput p2, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->size:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/io/InputStreamReader;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;-><init>(Ljava/io/InputStreamReader;I)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->source:Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/io/InputStreamReader;->close()V

    return-void
.end method

.method public final getSize()I
    .locals 1

    iget v0, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->size:I

    return v0
.end method

.method public final getSource()Ljava/io/InputStreamReader;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->source:Ljava/io/InputStreamReader;

    return-object v0
.end method

.method public read([CII)I
    .locals 1

    const-string v0, "buffer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->source:Ljava/io/InputStreamReader;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStreamReader;->read([CII)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    iget p2, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->size:I

    add-int/2addr p2, p1

    iput p2, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->size:I

    :cond_0
    return p1
.end method

.method public final setSize(I)V
    .locals 0

    iput p1, p0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->size:I

    return-void
.end method
