.class public final Lcom/discord/gateway/GatewaySocket;
.super Ljava/lang/Object;
.source "GatewaySocket.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/gateway/GatewaySocket$Timer;,
        Lcom/discord/gateway/GatewaySocket$IdentifyData;,
        Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;,
        Lcom/discord/gateway/GatewaySocket$Companion;
    }
.end annotation


# static fields
.field private static final CLIENT_CAPABILITIES:J = 0x1fL

.field private static final CLOSE_CODE_CLEAN:I = 0x3e8

.field private static final CLOSE_CODE_DIRTY:I = 0xfa0

.field private static final CLOSE_CODE_UNAUTHORIZED:I = 0xfa4

.field private static final COMPRESS_DATA:Z = true

.field private static final CONNECTED:I = 0x5

.field private static final CONNECTING:I = 0x2

.field public static final Companion:Lcom/discord/gateway/GatewaySocket$Companion;

.field private static final DEDUPE_USER_OBJECTS:J = 0x10L

.field private static final DISCONNECTED:I = 0x0

.field private static final DISCOVERING:I = 0x1

.field private static final EXPECTED_NULL_DATA_EVENTS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final GATEWAY_COMPRESSION:Ljava/lang/String; = "zlib-stream"

.field private static final GATEWAY_ENCODING:Ljava/lang/String; = "json"

.field private static final GATEWAY_URL_RESET_THRESHOLD:I = 0x4

.field private static final GATEWAY_VERSION:I = 0x8

.field private static final HEARTBEAT_MAX_RESUME_THRESHOLD:I = 0x2bf20

.field private static final HELLO_TIMEOUT:J = 0x4e20L

.field private static final IDENTIFYING:I = 0x3

.field private static final LARGE_GUILD_THRESHOLD:I = 0x64

.field private static final LAZY_USER_NOTES:J = 0x1L

.field private static final NO_AFFINE_USER_IDS:J = 0x2L

.field private static final RESUMING:I = 0x4

.field private static final VERSIONED_READ_STATES:J = 0x4L

.field private static final VERSIONED_USER_GUILD_SETTINGS:J = 0x8L

.field private static final clock:Lcom/discord/utilities/time/Clock;

.field private static final gsonIncludeNulls:Lcom/google/gson/Gson;

.field private static final gsonOmitNulls:Lcom/google/gson/Gson;


# instance fields
.field private connected:Z

.field private connectionReady:Z

.field private connectionStartTime:J

.field private connectionState:I

.field private final eventHandler:Lcom/discord/gateway/GatewayEventHandler;

.field private final gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

.field private final gatewayDiscovery:Lcom/discord/gateway/GatewayDiscovery;

.field private final gatewaySocketLogger:Lcom/discord/gateway/GatewaySocketLogger;

.field private final gatewayUrlTransform:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasConnectedOnce:Z

.field private heartbeatAck:Z

.field private heartbeatAckTimeMostRecent:J

.field private heartbeatExpeditedTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

.field private heartbeatInterval:J

.field private heartbeater:Lcom/discord/gateway/GatewaySocket$Timer;

.field private helloTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

.field private final identifyDataProvider:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/discord/gateway/GatewaySocket$IdentifyData;",
            ">;"
        }
    .end annotation
.end field

.field private final identifyProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private identifyStartTime:J

.field private final logger:Lcom/discord/utilities/logging/Logger;

.field private nextReconnectIsImmediate:Z

.field private replayedEvents:I

.field private final scheduler:Lrx/Scheduler;

.field private seq:I

.field private sessionId:Ljava/lang/String;

.field private final sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private token:Ljava/lang/String;

.field private final trackReadyPayload:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private webSocket:Lcom/discord/utilities/websocket/WebSocket;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/gateway/GatewaySocket$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/gateway/GatewaySocket$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    new-instance v0, Lf/h/d/k;

    invoke-direct {v0}, Lf/h/d/k;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/d/k;->g:Z

    invoke-virtual {v0}, Lf/h/d/k;->a()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/discord/gateway/GatewaySocket;->gsonIncludeNulls:Lcom/google/gson/Gson;

    new-instance v0, Lf/h/d/k;

    invoke-direct {v0}, Lf/h/d/k;-><init>()V

    invoke-virtual {v0}, Lf/h/d/k;->a()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/discord/gateway/GatewaySocket;->gsonOmitNulls:Lcom/google/gson/Gson;

    const-string v0, "USER_SUBSCRIPTIONS_UPDATE"

    const-string v1, "USER_PAYMENT_SOURCES_UPDATE"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/gateway/GatewaySocket;->EXPECTED_NULL_DATA_EVENTS:Ljava/util/Set;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    sput-object v0, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/gateway/GatewayEventHandler;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/networking/NetworkMonitor;Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Ljavax/net/ssl/SSLSocketFactory;Ljava/util/Map;Lcom/discord/gateway/GatewaySocketLogger;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/discord/gateway/GatewaySocket$IdentifyData;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/gateway/GatewayEventHandler;",
            "Lrx/Scheduler;",
            "Lcom/discord/utilities/logging/Logger;",
            "Lcom/discord/utilities/networking/NetworkMonitor;",
            "Lcom/discord/gateway/rest/RestConfig;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/net/ssl/SSLSocketFactory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/discord/gateway/GatewaySocketLogger;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v7, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    const-string v10, "identifyDataProvider"

    invoke-static {v1, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v10, "trackReadyPayload"

    invoke-static {v2, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "eventHandler"

    invoke-static {v3, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "scheduler"

    invoke-static {v7, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "logger"

    invoke-static {v4, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "networkMonitor"

    move-object/from16 v11, p6

    invoke-static {v11, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "restConfig"

    invoke-static {v5, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "context"

    invoke-static {v6, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "identifyProperties"

    invoke-static {v8, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "gatewaySocketLogger"

    invoke-static {v9, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/discord/gateway/GatewaySocket;->identifyDataProvider:Lkotlin/jvm/functions/Function0;

    iput-object v2, v0, Lcom/discord/gateway/GatewaySocket;->trackReadyPayload:Lkotlin/jvm/functions/Function1;

    iput-object v3, v0, Lcom/discord/gateway/GatewaySocket;->eventHandler:Lcom/discord/gateway/GatewayEventHandler;

    iput-object v7, v0, Lcom/discord/gateway/GatewaySocket;->scheduler:Lrx/Scheduler;

    iput-object v4, v0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/discord/gateway/GatewaySocket;->gatewayUrlTransform:Lkotlin/jvm/functions/Function1;

    move-object/from16 v1, p10

    iput-object v1, v0, Lcom/discord/gateway/GatewaySocket;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v8, v0, Lcom/discord/gateway/GatewaySocket;->identifyProperties:Ljava/util/Map;

    iput-object v9, v0, Lcom/discord/gateway/GatewaySocket;->gatewaySocketLogger:Lcom/discord/gateway/GatewaySocketLogger;

    new-instance v4, Lcom/discord/utilities/networking/Backoff;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-direct {v1, v7}, Lcom/discord/gateway/GatewaySocket$Timer;-><init>(Lrx/Scheduler;)V

    const-wide/16 v13, 0x3e8

    const-wide/16 v15, 0x2710

    const/16 v17, 0x4

    const/16 v18, 0x1

    move-object v12, v4

    move-object/from16 v19, v1

    invoke-direct/range {v12 .. v19}, Lcom/discord/utilities/networking/Backoff;-><init>(JJIZLcom/discord/utilities/networking/Backoff$Scheduler;)V

    iput-object v4, v0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-direct {v1, v7}, Lcom/discord/gateway/GatewaySocket$Timer;-><init>(Lrx/Scheduler;)V

    iput-object v1, v0, Lcom/discord/gateway/GatewaySocket;->heartbeatExpeditedTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    const-wide v1, 0x7fffffffffffffffL

    iput-wide v1, v0, Lcom/discord/gateway/GatewaySocket;->heartbeatInterval:J

    new-instance v1, Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-direct {v1, v7}, Lcom/discord/gateway/GatewaySocket$Timer;-><init>(Lrx/Scheduler;)V

    iput-object v1, v0, Lcom/discord/gateway/GatewaySocket;->heartbeater:Lcom/discord/gateway/GatewaySocket$Timer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/discord/gateway/GatewaySocket;->heartbeatAck:Z

    new-instance v1, Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-direct {v1, v7}, Lcom/discord/gateway/GatewaySocket$Timer;-><init>(Lrx/Scheduler;)V

    iput-object v1, v0, Lcom/discord/gateway/GatewaySocket;->helloTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    sget-object v1, Lcom/discord/gateway/rest/RestClient;->INSTANCE:Lcom/discord/gateway/rest/RestClient;

    invoke-virtual {v1, v5, v6}, Lcom/discord/gateway/rest/RestClient;->init(Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;)V

    new-instance v8, Lcom/discord/gateway/GatewayDiscovery;

    new-instance v5, Lcom/discord/gateway/GatewaySocket$1;

    invoke-direct {v5, v0}, Lcom/discord/gateway/GatewaySocket$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v1}, Lcom/discord/gateway/rest/RestClient;->getGateway()Lrx/Observable;

    move-result-object v9

    move-object v1, v8

    move-object/from16 v2, p8

    move-object/from16 v3, p4

    move-object v6, v9

    invoke-direct/range {v1 .. v6}, Lcom/discord/gateway/GatewayDiscovery;-><init>(Landroid/content/Context;Lrx/Scheduler;Lcom/discord/utilities/networking/Backoff;Lkotlin/jvm/functions/Function1;Lrx/Observable;)V

    iput-object v8, v0, Lcom/discord/gateway/GatewaySocket;->gatewayDiscovery:Lcom/discord/gateway/GatewayDiscovery;

    invoke-virtual/range {p6 .. p6}, Lcom/discord/utilities/networking/NetworkMonitor;->observeIsConnected()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1, v7}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/gateway/GatewaySocket$2;->INSTANCE:Lcom/discord/gateway/GatewaySocket$2;

    new-instance v3, Lg0/l/a/z1;

    new-instance v4, Lg0/l/a/y1;

    invoke-direct {v4, v2}, Lg0/l/a/y1;-><init>(Lg0/k/b;)V

    invoke-direct {v3, v4}, Lg0/l/a/z1;-><init>(Lrx/functions/Func2;)V

    new-instance v2, Lg0/l/a/u;

    iget-object v1, v1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v1, v3}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/gateway/GatewaySocket$3;

    invoke-direct {v2, v0}, Lcom/discord/gateway/GatewaySocket$3;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    new-instance v3, Lcom/discord/gateway/GatewaySocket$4;

    invoke-direct {v3, v0}, Lcom/discord/gateway/GatewaySocket$4;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v1, v2, v3}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/gateway/GatewayEventHandler;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/networking/NetworkMonitor;Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Ljavax/net/ssl/SSLSocketFactory;Ljava/util/Map;Lcom/discord/gateway/GatewaySocketLogger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 16

    move/from16 v0, p13

    and-int/lit16 v1, v0, 0x100

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v12, v2

    goto :goto_0

    :cond_0
    move-object/from16 v12, p9

    :goto_0
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_1

    move-object v13, v2

    goto :goto_1

    :cond_1
    move-object/from16 v13, p10

    :goto_1
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_2

    sget-object v0, Lx/h/m;->d:Lx/h/m;

    move-object v14, v0

    goto :goto_2

    :cond_2
    move-object/from16 v14, p11

    :goto_2
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v15, p12

    invoke-direct/range {v3 .. v15}, Lcom/discord/gateway/GatewaySocket;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/gateway/GatewayEventHandler;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/networking/NetworkMonitor;Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Ljavax/net/ssl/SSLSocketFactory;Ljava/util/Map;Lcom/discord/gateway/GatewaySocketLogger;)V

    return-void
.end method

.method public static final synthetic access$cleanup(Lcom/discord/gateway/GatewaySocket;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->cleanup(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$connect(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->connect(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$discover(Lcom/discord/gateway/GatewaySocket;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->discover()V

    return-void
.end method

.method public static final synthetic access$discoveryFailed(Lcom/discord/gateway/GatewaySocket;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->discoveryFailed()V

    return-void
.end method

.method public static final synthetic access$getConnectionStartTime$p(Lcom/discord/gateway/GatewaySocket;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    return-wide v0
.end method

.method public static final synthetic access$getConnectionState$p(Lcom/discord/gateway/GatewaySocket;)I
    .locals 0

    iget p0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    return p0
.end method

.method public static final synthetic access$getGatewayBackoff$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/utilities/networking/Backoff;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    return-object p0
.end method

.method public static final synthetic access$getGatewaySocketLogger$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/gateway/GatewaySocketLogger;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket;->gatewaySocketLogger:Lcom/discord/gateway/GatewaySocketLogger;

    return-object p0
.end method

.method public static final synthetic access$getGatewayUrlTransform$p(Lcom/discord/gateway/GatewaySocket;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayUrlTransform:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getGsonOmitNulls$cp()Lcom/google/gson/Gson;
    .locals 1

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->gsonOmitNulls:Lcom/google/gson/Gson;

    return-object v0
.end method

.method public static final synthetic access$getHeartbeatAck$p(Lcom/discord/gateway/GatewaySocket;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAck:Z

    return p0
.end method

.method public static final synthetic access$getHeartbeatExpeditedTimeout$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/gateway/GatewaySocket$Timer;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatExpeditedTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    return-object p0
.end method

.method public static final synthetic access$getLogger$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/utilities/logging/Logger;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    return-object p0
.end method

.method public static final synthetic access$getNextReconnectIsImmediate$p(Lcom/discord/gateway/GatewaySocket;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/gateway/GatewaySocket;->nextReconnectIsImmediate:Z

    return p0
.end method

.method public static final synthetic access$getSeq$p(Lcom/discord/gateway/GatewaySocket;)I
    .locals 0

    iget p0, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    return p0
.end method

.method public static final synthetic access$getWebSocket$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/utilities/websocket/WebSocket;
    .locals 0

    iget-object p0, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    return-object p0
.end method

.method public static final synthetic access$handleClose(Lcom/discord/gateway/GatewaySocket;ZILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/gateway/GatewaySocket;->handleClose(ZILjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleDeviceConnectivityChange(Lcom/discord/gateway/GatewaySocket;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->handleDeviceConnectivityChange(Z)V

    return-void
.end method

.method public static final synthetic access$handleHeartbeatTimeout(Lcom/discord/gateway/GatewaySocket;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->handleHeartbeatTimeout()V

    return-void
.end method

.method public static final synthetic access$handleReconnect(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->handleReconnect(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleWebSocketClose(Lcom/discord/gateway/GatewaySocket;Lcom/discord/utilities/websocket/WebSocket$Closed;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->handleWebSocketClose(Lcom/discord/utilities/websocket/WebSocket$Closed;)V

    return-void
.end method

.method public static final synthetic access$handleWebSocketError(Lcom/discord/gateway/GatewaySocket;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->handleWebSocketError()V

    return-void
.end method

.method public static final synthetic access$handleWebSocketMessage(Lcom/discord/gateway/GatewaySocket;Ljava/io/InputStreamReader;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/gateway/GatewaySocket;->handleWebSocketMessage(Ljava/io/InputStreamReader;I)V

    return-void
.end method

.method public static final synthetic access$handleWebSocketOpened(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->handleWebSocketOpened(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$heartbeat(Lcom/discord/gateway/GatewaySocket;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->heartbeat(I)V

    return-void
.end method

.method public static final synthetic access$logError(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;Ljava/lang/Exception;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/gateway/GatewaySocket;->logError(Ljava/lang/String;Ljava/lang/Exception;Ljava/util/Map;)V

    return-void
.end method

.method public static final synthetic access$reset(Lcom/discord/gateway/GatewaySocket;ZILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/gateway/GatewaySocket;->reset(ZILjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setConnectionStartTime$p(Lcom/discord/gateway/GatewaySocket;J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    return-void
.end method

.method public static final synthetic access$setConnectionState$p(Lcom/discord/gateway/GatewaySocket;I)V
    .locals 0

    iput p1, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    return-void
.end method

.method public static final synthetic access$setHeartbeatAck$p(Lcom/discord/gateway/GatewaySocket;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAck:Z

    return-void
.end method

.method public static final synthetic access$setHeartbeatExpeditedTimeout$p(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/GatewaySocket$Timer;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatExpeditedTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    return-void
.end method

.method public static final synthetic access$setNextReconnectIsImmediate$p(Lcom/discord/gateway/GatewaySocket;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/gateway/GatewaySocket;->nextReconnectIsImmediate:Z

    return-void
.end method

.method public static final synthetic access$setSeq$p(Lcom/discord/gateway/GatewaySocket;I)V
    .locals 0

    iput p1, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    return-void
.end method

.method public static final synthetic access$setWebSocket$p(Lcom/discord/gateway/GatewaySocket;Lcom/discord/utilities/websocket/WebSocket;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    return-void
.end method

.method private final cleanup(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/websocket/WebSocket;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->stopHeartbeater()V

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->clearHelloTimeout()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    iget-object p1, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/utilities/websocket/WebSocket;->resetListeners()V

    :cond_1
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    iget-object p1, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {p1}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    return-void
.end method

.method private final clearHelloTimeout()V
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->helloTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel()V

    return-void
.end method

.method public static synthetic close$default(Lcom/discord/gateway/GatewaySocket;ZILjava/lang/Object;)V
    .locals 0

    const/4 p3, 0x1

    and-int/2addr p2, p3

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/gateway/GatewaySocket;->close(Z)V

    return-void
.end method

.method private final connect(Ljava/lang/String;)V
    .locals 8

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    sget-object v2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v0, "Connect to: "

    const-string v4, ", encoding: json, version 8."

    invoke-static {v0, p1, v4}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    sget-object v0, Lcom/discord/gateway/GatewaySocket$connect$2;->INSTANCE:Lcom/discord/gateway/GatewaySocket$connect$2;

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->cleanup(Lkotlin/jvm/functions/Function1;)V

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->helloTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    new-instance v2, Lcom/discord/gateway/GatewaySocket$connect$3;

    invoke-direct {v2, p0}, Lcom/discord/gateway/GatewaySocket$connect$3;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    const-wide/16 v3, 0x4e20

    invoke-virtual {v0, v2, v3, v4}, Lcom/discord/gateway/GatewaySocket$Timer;->schedule(Lkotlin/jvm/functions/Function0;J)V

    new-instance v0, Lcom/discord/utilities/websocket/WebSocket;

    iget-object v2, p0, Lcom/discord/gateway/GatewaySocket;->scheduler:Lrx/Scheduler;

    new-instance v3, Lcom/discord/gateway/GatewaySocket$connect$4;

    invoke-direct {v3, p0}, Lcom/discord/gateway/GatewaySocket$connect$4;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    iget-object v4, p0, Lcom/discord/gateway/GatewaySocket;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    invoke-direct {v0, v2, v3, v4}, Lcom/discord/utilities/websocket/WebSocket;-><init>(Lrx/Scheduler;Lkotlin/jvm/functions/Function3;Ljavax/net/ssl/SSLSocketFactory;)V

    new-instance v2, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$1;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/websocket/WebSocket;->setOnOpened(Lkotlin/jvm/functions/Function1;)V

    new-instance v2, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p1}, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$2;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/websocket/WebSocket;->setOnClosed(Lkotlin/jvm/functions/Function1;)V

    new-instance v2, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$3;

    invoke-direct {v2, p0, p1}, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$3;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/websocket/WebSocket;->setOnMessage(Lkotlin/jvm/functions/Function2;)V

    iget-object v2, p0, Lcom/discord/gateway/GatewaySocket;->gatewaySocketLogger:Lcom/discord/gateway/GatewaySocketLogger;

    invoke-interface {v2}, Lcom/discord/gateway/GatewaySocketLogger;->getLogLevel()Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    if-eqz v2, :cond_2

    if-ne v2, v1, :cond_1

    new-instance v1, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;

    invoke-direct {v1, p0, p1}, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/discord/utilities/websocket/WebSocket;->setRawMessageHandler(Lcom/discord/utilities/websocket/RawMessageHandler;)V

    new-instance v1, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$5;

    invoke-direct {v1, p0, p1}, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$5;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/websocket/WebSocket;->setOnError(Lkotlin/jvm/functions/Function1;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/?encoding=json&v=8&compress=zlib-stream"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/websocket/WebSocket;->connect(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    return-void
.end method

.method private final discover()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/gateway/GatewaySocket;->nextReconnectIsImmediate:Z

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayDiscovery:Lcom/discord/gateway/GatewayDiscovery;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$discover$1;

    invoke-direct {v1, p0}, Lcom/discord/gateway/GatewaySocket$discover$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    new-instance v2, Lcom/discord/gateway/GatewaySocket$discover$2;

    invoke-direct {v2, p0}, Lcom/discord/gateway/GatewaySocket$discover$2;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/gateway/GatewayDiscovery;->discoverGatewayUrl(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final discoveryFailed()V
    .locals 8

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$discoveryFailed$delay$1;

    invoke-direct {v1, p0}, Lcom/discord/gateway/GatewaySocket$discoveryFailed$delay$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/networking/Backoff;->fail(Lkotlin/jvm/functions/Function0;)J

    move-result-wide v0

    sget-object v2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v4, "Discovery failed, retrying in "

    const-string v5, "ms."

    invoke-static {v4, v0, v1, v5}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->hasReachedFailureThreshold()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const-string v1, "Gateway discovery failed."

    invoke-direct {p0, v0, v0, v1}, Lcom/discord/gateway/GatewaySocket;->reset(ZILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private final doIdentify()V
    .locals 14

    const/4 v0, 0x0

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->identifyDataProvider:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/gateway/GatewaySocket$IdentifyData;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/16 v1, 0xfa4

    const-string v2, "No connection info provided."

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/gateway/GatewaySocket;->handleClose(ZILjava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x3

    iput v1, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    sget-object v1, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/discord/gateway/GatewaySocket;->identifyStartTime:J

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$IdentifyData;->getToken()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/gateway/GatewaySocket;->token:Ljava/lang/String;

    sget-object v6, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v7, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/4 v11, 0x0

    const-string v8, "Sending identify."

    invoke-static/range {v6 .. v11}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    new-instance v1, Lcom/discord/gateway/io/Outgoing;

    sget-object v2, Lcom/discord/gateway/opcodes/Opcode;->IDENTIFY:Lcom/discord/gateway/opcodes/Opcode;

    new-instance v3, Lcom/discord/gateway/io/OutgoingPayload$Identify;

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$IdentifyData;->getToken()Ljava/lang/String;

    move-result-object v7

    const-wide/16 v10, 0x1f

    iget-object v12, p0, Lcom/discord/gateway/GatewaySocket;->identifyProperties:Ljava/util/Map;

    const/16 v8, 0x64

    const/4 v9, 0x1

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$IdentifyData;->getClientState()Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

    move-result-object v13

    move-object v6, v3

    invoke-direct/range {v6 .. v13}, Lcom/discord/gateway/io/OutgoingPayload$Identify;-><init>(Ljava/lang/String;IZJLjava/util/Map;Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;)V

    invoke-direct {v1, v2, v3}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method

.method private final doResume()V
    .locals 13

    const/4 v0, 0x4

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->replayedEvents:I

    sget-object v1, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v2, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v0, "Resuming session "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " at sequence: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    new-instance v8, Lcom/discord/gateway/io/Outgoing;

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->RESUME:Lcom/discord/gateway/opcodes/Opcode;

    new-instance v1, Lcom/discord/gateway/io/OutgoingPayload$Resume;

    iget-object v2, p0, Lcom/discord/gateway/GatewaySocket;->token:Ljava/lang/String;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    iget v4, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/gateway/io/OutgoingPayload$Resume;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v8, v0, v1}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    move-object v7, p0

    invoke-static/range {v7 .. v12}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method

.method private final doResumeOrIdentify()V
    .locals 9

    sget-object v6, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-wide v1, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAckTimeMostRecent:J

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v6

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAckTimeMostRecent:J

    const-wide/16 v4, 0x0

    cmp-long v7, v2, v4

    if-nez v7, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    long-to-float v2, v0

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    :goto_0
    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    const v3, 0x2bf20

    int-to-long v7, v3

    cmp-long v3, v0, v7

    if-gtz v3, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v0, "Attempting to resume after elapsed duration of "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    const-string v3, "%.2f"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "java.lang.String.format(this, *args)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " minutes."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v6

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->doResume()V

    goto :goto_2

    :cond_2
    invoke-direct {p0, v5}, Lcom/discord/gateway/GatewaySocket;->handleInvalidSession(Z)V

    :goto_2
    sget-object v0, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAckTimeMostRecent:J

    return-void
.end method

.method public static synthetic expeditedHeartbeat$default(Lcom/discord/gateway/GatewaySocket;JLjava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    const/4 p4, 0x1

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/gateway/GatewaySocket;->expeditedHeartbeat(JLjava/lang/String;Z)V

    return-void
.end method

.method private final flattenTraces(Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/ArrayList<",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "micros"

    if-eqz p1, :cond_a

    :try_start_0
    instance-of v1, p1, Ljava/util/List;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    instance-of v1, p1, Lx/m/c/x/a;

    if-eqz v1, :cond_0

    instance-of v1, p1, Lx/m/c/x/c;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_a

    const/4 v1, 0x0

    :cond_2
    :goto_1
    add-int/lit8 v4, v1, 0x1

    move-object v5, p1

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_a

    move-object v5, p1

    check-cast v5, Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v6, v5, Ljava/lang/String;

    const/4 v7, 0x0

    if-nez v6, :cond_3

    move-object v5, v7

    :cond_3
    check-cast v5, Ljava/lang/String;

    move-object v6, p1

    check-cast v6, Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v6, v4, Ljava/util/Map;

    if-eqz v6, :cond_5

    instance-of v6, v4, Lx/m/c/x/a;

    if-eqz v6, :cond_4

    instance-of v6, v4, Lx/m/c/x/d;

    if-eqz v6, :cond_5

    :cond_4
    const/4 v6, 0x1

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    :goto_2
    if-nez v6, :cond_6

    move-object v4, v7

    :cond_6
    check-cast v4, Ljava/util/Map;

    add-int/lit8 v1, v1, 0x2

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    instance-of v8, v6, Ljava/lang/Number;

    if-nez v8, :cond_7

    goto :goto_3

    :cond_7
    move-object v7, v6

    :goto_3
    check-cast v7, Ljava/lang/Number;

    :cond_8
    if-eqz v7, :cond_9

    new-instance v6, Lkotlin/Pair;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    div-int/lit16 v7, v7, 0x3e8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    const-string v5, "calls"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {p0, v4, p2}, Lcom/discord/gateway/GatewaySocket;->flattenTraces(Ljava/lang/Object;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    move-object v2, p1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "Unable to parse ready payload traces"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :cond_a
    return-void
.end method

.method private final getConnectionPath(Lcom/discord/models/domain/ModelPayload$Hello;)Ljava/lang/String;
    .locals 8

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload$Hello;->getTrace()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const-string v1, " -> "

    invoke-static/range {v0 .. v7}, Lx/h/f;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, "???"

    :goto_0
    return-object p1
.end method

.method private final handleClose(ZILjava/lang/String;)V
    .locals 9

    sget-object v0, Lcom/discord/gateway/GatewaySocket$handleClose$1;->INSTANCE:Lcom/discord/gateway/GatewaySocket$handleClose$1;

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->cleanup(Lkotlin/jvm/functions/Function1;)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/discord/gateway/GatewaySocket;->handleConnected$default(Lcom/discord/gateway/GatewaySocket;Ljava/lang/Boolean;Ljava/lang/Boolean;ILjava/lang/Object;)V

    const/16 v0, 0xfa4

    if-ne p2, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/gateway/GatewaySocket;->reset(ZILjava/lang/String;)V

    return-void

    :cond_0
    iput v2, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Closed cleanly: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", with code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", for reason: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'."

    invoke-static {v0, p3, v1}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/gateway/GatewaySocket;->nextReconnectIsImmediate:Z

    if-eqz v1, :cond_1

    sget-object v2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string p1, " Retrying immediately."

    invoke-static {v0, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->discover()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    new-instance v2, Lcom/discord/gateway/GatewaySocket$handleClose$delay$1;

    invoke-direct {v2, p0}, Lcom/discord/gateway/GatewaySocket$handleClose$delay$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/networking/Backoff;->fail(Lkotlin/jvm/functions/Function0;)J

    move-result-wide v1

    sget-object v3, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v4, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " Retrying in: "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "ms."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->hasReachedFailureThreshold()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/gateway/GatewaySocket;->reset(ZILjava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final handleConnected(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/gateway/GatewaySocket;->connected:Z

    iget-boolean v0, p0, Lcom/discord/gateway/GatewaySocket;->hasConnectedOnce:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/discord/gateway/GatewaySocket;->hasConnectedOnce:Z

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->eventHandler:Lcom/discord/gateway/GatewayEventHandler;

    invoke-interface {v0, p1}, Lcom/discord/gateway/GatewayEventHandler;->handleConnected(Z)V

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/gateway/GatewaySocket;->connectionReady:Z

    iget-object p2, p0, Lcom/discord/gateway/GatewaySocket;->eventHandler:Lcom/discord/gateway/GatewayEventHandler;

    invoke-interface {p2, p1}, Lcom/discord/gateway/GatewayEventHandler;->handleConnectionReady(Z)V

    :cond_3
    return-void
.end method

.method public static synthetic handleConnected$default(Lcom/discord/gateway/GatewaySocket;Ljava/lang/Boolean;Ljava/lang/Boolean;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    move-object p2, v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/gateway/GatewaySocket;->handleConnected(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-void
.end method

.method private final handleDeviceConnectivityChange(Z)V
    .locals 14

    if-eqz p1, :cond_0

    const-wide/16 v1, 0x1194

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, "network detected online"

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/discord/gateway/GatewaySocket;->expeditedHeartbeat$default(Lcom/discord/gateway/GatewaySocket;JLjava/lang/String;ZILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    const-wide/16 v8, 0x2328

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const-string v10, "network detected offline"

    move-object v7, p0

    invoke-static/range {v7 .. v13}, Lcom/discord/gateway/GatewaySocket;->expeditedHeartbeat$default(Lcom/discord/gateway/GatewaySocket;JLjava/lang/String;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final handleDispatch(Ljava/lang/Object;Ljava/lang/String;IIJ)V
    .locals 9

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->replayedEvents:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->replayedEvents:I

    :cond_0
    const-string v0, "READY"

    invoke-static {p2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "RESUMED"

    if-nez v1, :cond_1

    invoke-static {p2, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_1
    if-nez p2, :cond_2

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v3, 0x4a3e183

    if-eq v1, v3, :cond_4

    const p3, 0x6c36c9b7

    if-eq v1, p3, :cond_3

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_6

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Resumed session, took "

    invoke-static {p4}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p4

    sget-object p5, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-wide v1, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    const/4 v3, 0x0

    const/4 p6, 0x2

    const/4 v6, 0x0

    move-object v0, p5

    move v4, p6

    move-object v5, v6

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v0

    invoke-virtual {p4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "ms, "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "replayed "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->replayedEvents:I

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " events, new seq: "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string p3, "StringBuilder()\n        \u2026              .toString()"

    invoke-static {v2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 v3, 0x0

    move-object v0, p5

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    const/4 p3, 0x0

    iput p3, p0, Lcom/discord/gateway/GatewaySocket;->replayedEvents:I

    goto :goto_0

    :cond_4
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v2, p1

    check-cast v2, Lcom/discord/models/domain/ModelPayload;

    if-nez v2, :cond_5

    const-string p1, "Reconnect due to invalid ready payload received."

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->handleReconnect(Ljava/lang/String;)V

    return-void

    :cond_5
    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPayload;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    move-object v1, p0

    move v3, p3

    move v4, p4

    move-wide v5, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/gateway/GatewaySocket;->trackReadyPayload(Lcom/discord/models/domain/ModelPayload;IIJ)V

    sget-object p3, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object p4, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string p5, "Ready with session id: "

    invoke-static {p5}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p5

    iget-object p6, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p6, ", took "

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    const/4 v6, 0x0

    const/4 p6, 0x2

    const/4 v0, 0x0

    move-object v3, p3

    move v7, p6

    move-object v8, v0

    invoke-static/range {v3 .. v8}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v1

    const-string v3, "ms"

    invoke-static {p5, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v3 .. v8}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    :cond_6
    :goto_0
    iget-object p3, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {p3}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    const/4 p3, 0x5

    iput p3, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    sget-object p3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-direct {p0, p3, p3}, Lcom/discord/gateway/GatewaySocket;->handleConnected(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    :cond_7
    if-eqz p1, :cond_8

    iget-object p3, p0, Lcom/discord/gateway/GatewaySocket;->eventHandler:Lcom/discord/gateway/GatewayEventHandler;

    invoke-interface {p3, p2, p1}, Lcom/discord/gateway/GatewayEventHandler;->handleDispatch(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_8
    sget-object p1, Lcom/discord/gateway/GatewaySocket;->EXPECTED_NULL_DATA_EVENTS:Ljava/util/Set;

    invoke-static {p1, p2}, Lx/h/f;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/discord/gateway/GatewaySocket;->eventHandler:Lcom/discord/gateway/GatewayEventHandler;

    sget-object p3, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-interface {p1, p2, p3}, Lcom/discord/gateway/GatewayEventHandler;->handleDispatch(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_9
    :goto_1
    return-void
.end method

.method private final handleHeartbeat()V
    .locals 1

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->heartbeat(I)V

    return-void
.end method

.method private final handleHeartbeatAck()V
    .locals 4

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v2, "Received heartbeat ACK."

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/gateway/GatewaySocket$Companion;->access$log(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Z)V

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAckTimeMostRecent:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAck:Z

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatExpeditedTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$handleHeartbeatAck$1;

    invoke-direct {v1, p0}, Lcom/discord/gateway/GatewaySocket$handleHeartbeatAck$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v0, v1}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleHeartbeatTimeout()V
    .locals 8

    sget-object v0, Lcom/discord/gateway/GatewaySocket$handleHeartbeatTimeout$1;->INSTANCE:Lcom/discord/gateway/GatewaySocket$handleHeartbeatTimeout$1;

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->cleanup(Lkotlin/jvm/functions/Function1;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->gatewayBackoff:Lcom/discord/utilities/networking/Backoff;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$handleHeartbeatTimeout$delay$1;

    invoke-direct {v1, p0}, Lcom/discord/gateway/GatewaySocket$handleHeartbeatTimeout$delay$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/networking/Backoff;->fail(Lkotlin/jvm/functions/Function0;)J

    move-result-wide v0

    sget-object v2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v4, "Ack timeout, reconnecting om "

    const-string v5, "ms."

    invoke-static {v4, v0, v1, v5}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    return-void
.end method

.method private final handleHello(Lcom/discord/models/domain/ModelPayload$Hello;)V
    .locals 10

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->clearHelloTimeout()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload$Hello;->getHeartbeatInterval()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatInterval:J

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v2, "Hello via "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->getConnectionPath(Lcom/discord/models/domain/ModelPayload$Hello;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", at interval "

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatInterval:J

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " took "

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    const/4 v5, 0x0

    const/4 p1, 0x2

    const/4 v9, 0x0

    move-object v2, v0

    move v6, p1

    move-object v7, v9

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v2

    const-string v4, "ms."

    invoke-static {v8, v2, v3, v4}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, v0

    move-object v3, v1

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->startHeartbeater()V

    return-void
.end method

.method private final handleInvalidSession(Z)V
    .locals 6

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v2, "Invalid session, is "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_0

    const-string v3, ""

    goto :goto_0

    :cond_0
    const-string v3, "not"

    :goto_0
    const-string v4, " resumable."

    invoke-static {v2, v3, v4}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->doResumeOrIdentify()V

    goto :goto_1

    :cond_1
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-direct {p0, p1, p1}, Lcom/discord/gateway/GatewaySocket;->handleConnected(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->doIdentify()V

    :goto_1
    return-void
.end method

.method private final handleReconnect(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/discord/gateway/GatewaySocket$handleReconnect$1;

    invoke-direct {v0, p1}, Lcom/discord/gateway/GatewaySocket$handleReconnect$1;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->cleanup(Lkotlin/jvm/functions/Function1;)V

    const/4 v0, 0x1

    const/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/gateway/GatewaySocket;->reset(ZILjava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/gateway/GatewaySocket;->handleClose(ZILjava/lang/String;)V

    return-void
.end method

.method public static synthetic handleReconnect$default(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, "Reconnect to gateway requested."

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/gateway/GatewaySocket;->handleReconnect(Ljava/lang/String;)V

    return-void
.end method

.method private final handleWebSocketClose(Lcom/discord/utilities/websocket/WebSocket$Closed;)V
    .locals 3

    invoke-virtual {p1}, Lcom/discord/utilities/websocket/WebSocket$Closed;->getReason()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "unspecified reason"

    :goto_1
    invoke-virtual {p1}, Lcom/discord/utilities/websocket/WebSocket$Closed;->getCode()I

    move-result p1

    invoke-direct {p0, v1, p1, v0}, Lcom/discord/gateway/GatewaySocket;->handleClose(ZILjava/lang/String;)V

    return-void
.end method

.method private final handleWebSocketError()V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "An error with the web socket occurred."

    invoke-direct {p0, v0, v0, v1}, Lcom/discord/gateway/GatewaySocket;->handleClose(ZILjava/lang/String;)V

    return-void
.end method

.method private final handleWebSocketMessage(Ljava/io/InputStreamReader;I)V
    .locals 18

    move-object/from16 v7, p0

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    new-instance v0, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;

    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-direct {v0, v4, v1, v8, v9}, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;-><init>(Ljava/io/InputStreamReader;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    new-instance v1, Lcom/discord/gateway/io/IncomingParser;

    new-instance v4, Lcom/discord/gateway/GatewaySocket$handleWebSocketMessage$incomingParser$1;

    invoke-direct {v4, v7}, Lcom/discord/gateway/GatewaySocket$handleWebSocketMessage$incomingParser$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-direct {v1, v4}, Lcom/discord/gateway/io/IncomingParser;-><init>(Lkotlin/jvm/functions/Function1;)V

    new-instance v4, Lcom/discord/models/domain/Model$JsonReader;

    new-instance v5, Lcom/google/gson/stream/JsonReader;

    invoke-direct {v5, v0}, Lcom/google/gson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-direct {v4, v5}, Lcom/discord/models/domain/Model$JsonReader;-><init>(Lcom/google/gson/stream/JsonReader;)V

    invoke-virtual {v4, v1}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object v1

    check-cast v1, Lcom/discord/gateway/io/IncomingParser;

    invoke-virtual {v1}, Lcom/discord/gateway/io/IncomingParser;->build()Lcom/discord/gateway/io/Incoming;

    move-result-object v10

    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getSeq()Ljava/lang/Integer;

    move-result-object v11

    sget-object v12, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v12

    invoke-static/range {v1 .. v6}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v5

    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v7, Lcom/discord/gateway/GatewaySocket;->seq:I

    :cond_0
    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getOpcode()Lcom/discord/gateway/opcodes/Opcode;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    if-eq v1, v8, :cond_2

    const/16 v0, 0x8

    if-eq v1, v0, :cond_1

    packed-switch v1, :pswitch_data_0

    iget-object v13, v7, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v0, "Unhandled op code "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getOpcode()Lcom/discord/gateway/opcodes/Opcode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x2

    const/16 v17, 0x0

    invoke-static/range {v12 .. v17}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/gateway/GatewaySocket;->handleHeartbeatAck()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getData()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.models.domain.ModelPayload.Hello"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/models/domain/ModelPayload$Hello;

    invoke-direct {v7, v0}, Lcom/discord/gateway/GatewaySocket;->handleHello(Lcom/discord/models/domain/ModelPayload$Hello;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getData()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type kotlin.Boolean"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v7, v0}, Lcom/discord/gateway/GatewaySocket;->handleInvalidSession(Z)V

    goto :goto_0

    :cond_1
    invoke-static {v7, v9, v2, v9}, Lcom/discord/gateway/GatewaySocket;->handleReconnect$default(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/gateway/GatewaySocket;->handleHeartbeat()V

    goto :goto_0

    :cond_3
    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getData()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v10}, Lcom/discord/gateway/io/Incoming;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;->getSize()I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/discord/gateway/GatewaySocket;->handleDispatch(Ljava/lang/Object;Ljava/lang/String;IIJ)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final handleWebSocketOpened(Ljava/lang/String;)V
    .locals 10

    sget-object v6, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v7, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v0, "Connected to "

    const-string v1, " in "

    invoke-static {v0, p1, v1}, Lf/e/c/a/a;->M(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-wide v1, p0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v0, v6

    move v4, v8

    move-object v5, v9

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v0

    const-string v2, "ms."

    invoke-static {p1, v0, v1, v2}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object v0, v6

    move-object v1, v7

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/gateway/GatewaySocket;->doResumeOrIdentify()V

    return-void
.end method

.method private final heartbeat(I)V
    .locals 10

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending heartbeat at sequence: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/gateway/GatewaySocket$Companion;->access$log(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Z)V

    new-instance v5, Lcom/discord/gateway/io/Outgoing;

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->HEARTBEAT:Lcom/discord/gateway/opcodes/Opcode;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v5, v0, p1}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method

.method private final logError(Ljava/lang/String;Ljava/lang/Exception;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Exception;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    sget-object p3, Lx/h/m;->d:Lx/h/m;

    :goto_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/utilities/logging/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    return-void
.end method

.method public static synthetic presenceUpdate$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;ILjava/lang/Object;)V
    .locals 2

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    sget-object p2, Lcom/discord/gateway/GatewaySocket;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {p2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    sget-object p4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/gateway/GatewaySocket;->presenceUpdate(Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static synthetic requestGuildMembers$default(Lcom/discord/gateway/GatewaySocket;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    move-object p4, v0

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/gateway/GatewaySocket;->requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V

    return-void
.end method

.method private final reset(ZILjava/lang/String;)V
    .locals 8

    const/16 v0, 0xfa0

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/discord/gateway/GatewaySocket;->sessionId:Ljava/lang/String;

    iput v1, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    :cond_0
    if-eqz p1, :cond_1

    const-string p1, "cleanly"

    goto :goto_0

    :cond_1
    const-string p1, "dirty"

    :goto_0
    sget-object v2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Reset "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", with code "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", at sequence "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/discord/gateway/GatewaySocket;->seq:I

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ". Reason: \'"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'."

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    const/4 p1, 0x1

    if-ne p2, v0, :cond_2

    iget-boolean p3, p0, Lcom/discord/gateway/GatewaySocket;->connected:Z

    if-eqz p3, :cond_2

    const/4 p3, 0x1

    goto :goto_1

    :cond_2
    const/4 p3, 0x0

    :goto_1
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-direct {p0, p3, v0}, Lcom/discord/gateway/GatewaySocket;->handleConnected(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iget-object p3, p0, Lcom/discord/gateway/GatewaySocket;->eventHandler:Lcom/discord/gateway/GatewayEventHandler;

    const/16 v0, 0xfa4

    if-ne p2, v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-interface {p3, v1}, Lcom/discord/gateway/GatewayEventHandler;->handleDisconnect(Z)V

    return-void
.end method

.method private final schedule(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/gateway/GatewaySocket;->scheduler:Lrx/Scheduler;

    invoke-virtual {v0, p1}, Lrx/Observable;->S(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/gateway/GatewaySocket$schedule$1;->INSTANCE:Lcom/discord/gateway/GatewaySocket$schedule$1;

    sget-object v1, Lcom/discord/gateway/GatewaySocket$schedule$2;->INSTANCE:Lcom/discord/gateway/GatewaySocket$schedule$2;

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method private final send(Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;)V
    .locals 7

    const/16 v0, 0x2e

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/discord/gateway/GatewaySocket;->isSessionEstablished()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v2, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string p2, "Attempted to send while not being in a connected state, opcode: "

    invoke-static {p2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p1}, Lcom/discord/gateway/io/Outgoing;->getOp()I

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    if-eqz p2, :cond_2

    invoke-virtual {p3, p1}, Lcom/google/gson/Gson;->k(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/gateway/GatewaySocket;->gatewaySocketLogger:Lcom/discord/gateway/GatewaySocketLogger;

    const-string p3, "json"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/discord/gateway/GatewaySocketLogger;->logOutboundMessage(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/discord/gateway/GatewaySocket;->webSocket:Lcom/discord/utilities/websocket/WebSocket;

    if-eqz p2, :cond_3

    invoke-virtual {p2, p1}, Lcom/discord/utilities/websocket/WebSocket;->message(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sget-object p2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    const-string p3, "Attempted to send without a web socket that exists, opcode: "

    invoke-static {p3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p1}, Lcom/discord/gateway/io/Outgoing;->getOp()I

    move-result p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public static synthetic send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    sget-object p3, Lcom/discord/gateway/GatewaySocket;->gsonIncludeNulls:Lcom/google/gson/Gson;

    const-string p4, "gsonIncludeNulls"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/gateway/GatewaySocket;->send(Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;)V

    return-void
.end method

.method private final startHeartbeater()V
    .locals 4

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeater:Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatAck:Z

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeater:Lcom/discord/gateway/GatewaySocket$Timer;

    new-instance v1, Lcom/discord/gateway/GatewaySocket$startHeartbeater$1;

    invoke-direct {v1, p0}, Lcom/discord/gateway/GatewaySocket$startHeartbeater$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    iget-wide v2, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatInterval:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/gateway/GatewaySocket$Timer;->postInterval(Lkotlin/jvm/functions/Function0;J)V

    return-void
.end method

.method private final stopHeartbeater()V
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeater:Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel()V

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket;->heartbeatExpeditedTimeout:Lcom/discord/gateway/GatewaySocket$Timer;

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket$Timer;->cancel()V

    return-void
.end method

.method private final trackReadyPayload(Lcom/discord/models/domain/ModelPayload;IIJ)V
    .locals 18

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelGuild;

    const-string v7, "guild"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v4, v7

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/models/domain/ModelChannel;

    const-string v8, "channel"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelChannel;->isCategory()Z

    move-result v7

    if-eqz v7, :cond_1

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelPayload;->getTrace()Ljava/lang/Object;

    move-result-object v6

    invoke-direct {v0, v6, v2}, Lcom/discord/gateway/GatewaySocket;->flattenTraces(Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v7, 0x2

    if-lez v6, :cond_3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lkotlin/Pair;

    invoke-virtual {v6}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v6}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    const-string v9, "gateway-"

    invoke-static {v8, v9, v3, v7}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v8

    if-eqz v8, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v8, 0x0

    const/4 v9, 0x0

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lkotlin/Pair;

    invoke-virtual {v10}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v10}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    move-result v10

    const-string/jumbo v12, "start_session"

    invoke-static {v11, v12}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    move v8, v10

    goto :goto_2

    :cond_5
    const-string v12, "guilds_connect"

    invoke-static {v11, v12}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    move v9, v10

    goto :goto_2

    :cond_6
    iget-object v2, v0, Lcom/discord/gateway/GatewaySocket;->trackReadyPayload:Lkotlin/jvm/functions/Function1;

    const/16 v10, 0xf

    new-array v10, v10, [Lkotlin/Pair;

    sget-object v17, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-wide v12, v0, Lcom/discord/gateway/GatewaySocket;->connectionStartTime:J

    const/4 v14, 0x0

    const/4 v15, 0x2

    const/16 v16, 0x0

    move-object/from16 v11, v17

    invoke-static/range {v11 .. v16}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v12, Lkotlin/Pair;

    const-string v13, "duration_ms_since_connection_start"

    invoke-direct {v12, v13, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v12, v10, v3

    iget-wide v12, v0, Lcom/discord/gateway/GatewaySocket;->identifyStartTime:J

    const/4 v14, 0x0

    const/4 v15, 0x2

    const/16 v16, 0x0

    move-object/from16 v11, v17

    invoke-static/range {v11 .. v16}, Lcom/discord/gateway/GatewaySocket$Companion;->getDelay$default(Lcom/discord/gateway/GatewaySocket$Companion;JLjava/lang/Long;ILjava/lang/Object;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v11, Lkotlin/Pair;

    const-string v12, "duration_ms_since_identify_start"

    invoke-direct {v11, v12, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v3, 0x1

    aput-object v11, v10, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v6, Lkotlin/Pair;

    const-string v11, "identify_total_server_duration_ms"

    invoke-direct {v6, v11, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v10, v7

    const/4 v3, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lkotlin/Pair;

    const-string v8, "identify_api_duration_ms"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/4 v3, 0x4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lkotlin/Pair;

    const-string v8, "identify_guilds_duration_ms"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/4 v3, 0x5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lkotlin/Pair;

    const-string v8, "compressed_byte_size"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/4 v3, 0x6

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lkotlin/Pair;

    const-string/jumbo v8, "uncompressed_byte_size"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/4 v3, 0x7

    new-instance v6, Lkotlin/Pair;

    const-string v7, "compression_algorithm"

    const-string/jumbo v8, "zlib"

    invoke-direct {v6, v7, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v10, v3

    const/16 v3, 0x8

    new-instance v6, Lkotlin/Pair;

    const-string v7, "packing_algorithm"

    const-string v8, "json"

    invoke-direct {v6, v7, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v10, v3

    const/16 v3, 0x9

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v7, Lkotlin/Pair;

    const-string/jumbo v8, "unpack_duration_ms"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/16 v3, 0xa

    iget-boolean v6, v0, Lcom/discord/gateway/GatewaySocket;->hasConnectedOnce:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    new-instance v7, Lkotlin/Pair;

    const-string v8, "is_reconnect"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/16 v3, 0xb

    sget-object v6, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    new-instance v7, Lkotlin/Pair;

    const-string v8, "is_fast_connect"

    invoke-direct {v7, v8, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v10, v3

    const/16 v3, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v6, Lkotlin/Pair;

    const-string v7, "num_guilds"

    invoke-direct {v6, v7, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v10, v3

    const/16 v1, 0xd

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lkotlin/Pair;

    const-string v6, "num_guild_channels"

    invoke-direct {v4, v6, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v10, v1

    const/16 v1, 0xe

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lkotlin/Pair;

    const-string v5, "num_guild_category_channels"

    invoke-direct {v4, v5, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v10, v1

    invoke-static {v10}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v2, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final callConnect(J)V
    .locals 1

    new-instance v0, Lcom/discord/gateway/GatewaySocket$callConnect$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/discord/gateway/GatewaySocket$callConnect$1;-><init>(Lcom/discord/gateway/GatewaySocket;J)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final close(Z)V
    .locals 1

    new-instance v0, Lcom/discord/gateway/GatewaySocket$close$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/gateway/GatewaySocket$close$1;-><init>(Lcom/discord/gateway/GatewaySocket;Z)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final connect()V
    .locals 1

    new-instance v0, Lcom/discord/gateway/GatewaySocket$connect$1;

    invoke-direct {v0, p0}, Lcom/discord/gateway/GatewaySocket$connect$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final expeditedHeartbeat(JLjava/lang/String;Z)V
    .locals 7

    new-instance v6, Lcom/discord/gateway/GatewaySocket$expeditedHeartbeat$1;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p3

    move-wide v3, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$expeditedHeartbeat$1;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;JZ)V

    invoke-direct {p0, v6}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final isSessionEstablished()Z
    .locals 2

    iget v0, p0, Lcom/discord/gateway/GatewaySocket;->connectionState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final presenceUpdate(Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelPresence$Status;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    new-instance v6, Lcom/discord/gateway/GatewaySocket$presenceUpdate$1;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$presenceUpdate$1;-><init>(Lcom/discord/gateway/GatewaySocket;Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;)V

    invoke-direct {p0, v6}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final requestApplicationCommands(Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;)V
    .locals 7

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/gateway/io/Outgoing;

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->REQUEST_GUILD_APPLICATION_COMMANDS:Lcom/discord/gateway/opcodes/Opcode;

    invoke-direct {v2, v0, p1}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    sget-object v4, Lcom/discord/gateway/GatewaySocket;->gsonOmitNulls:Lcom/google/gson/Gson;

    const-string p1, "gsonOmitNulls"

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method

.method public final requestGuildMembers(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/discord/gateway/GatewaySocket;->requestGuildMembers$default(Lcom/discord/gateway/GatewaySocket;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method public final requestGuildMembers(Ljava/util/List;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/discord/gateway/GatewaySocket;->requestGuildMembers$default(Lcom/discord/gateway/GatewaySocket;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method public final requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/discord/gateway/GatewaySocket;->requestGuildMembers$default(Lcom/discord/gateway/GatewaySocket;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method public final requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    const-string v0, "guildIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/gateway/GatewaySocket$requestGuildMembers$1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/discord/gateway/GatewaySocket$requestGuildMembers$1;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final resetOnError()V
    .locals 1

    new-instance v0, Lcom/discord/gateway/GatewaySocket$resetOnError$1;

    invoke-direct {v0, p0}, Lcom/discord/gateway/GatewaySocket$resetOnError$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final streamCreate(Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;)V
    .locals 13

    const-string/jumbo v0, "streamType"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    move-object v0, p0

    iget-object v4, v0, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sending STREAM_CREATE: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide v9, p2

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v5, 0x20

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v11, p4

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    new-instance v7, Lcom/discord/gateway/io/Outgoing;

    sget-object v8, Lcom/discord/gateway/opcodes/Opcode;->STREAM_CREATE:Lcom/discord/gateway/opcodes/Opcode;

    new-instance v12, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;

    move-object v1, v12

    move-wide v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;-><init>(Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;)V

    invoke-direct {v7, v8, v12}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    move-object v4, p0

    move-object v5, v7

    move-object v7, v1

    invoke-static/range {v4 .. v9}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method

.method public final streamDelete(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/gateway/GatewaySocket$streamDelete$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/gateway/GatewaySocket$streamDelete$1;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final streamWatch(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/gateway/GatewaySocket$streamWatch$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/gateway/GatewaySocket$streamWatch$1;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final updateGuildSubscriptions(JLcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;)V
    .locals 13

    move-object/from16 v0, p3

    const-string v1, "guildSubscriptions"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    move-object v1, p0

    iget-object v3, v1, Lcom/discord/gateway/GatewaySocket;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sending guild subscriptions: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide v8, p1

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, " -- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    new-instance v4, Lcom/discord/gateway/io/Outgoing;

    sget-object v2, Lcom/discord/gateway/opcodes/Opcode;->GUILD_SUBSCRIPTIONS:Lcom/discord/gateway/opcodes/Opcode;

    new-instance v3, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptionsUpdate;

    invoke-virtual/range {p3 .. p3}, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;->getTyping()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;->getActivities()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;->getMembers()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {v6}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v6

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    move-object v11, v6

    invoke-virtual/range {p3 .. p3}, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;->getChannels()Ljava/util/Map;

    move-result-object v12

    move-object v6, v3

    move-wide v7, p1

    move-object v9, v5

    invoke-direct/range {v6 .. v12}, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptionsUpdate;-><init>(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/Map;)V

    invoke-direct {v4, v2, v3}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    const/4 v5, 0x0

    sget-object v6, Lcom/discord/gateway/GatewaySocket;->gsonOmitNulls:Lcom/google/gson/Gson;

    const-string v0, "gsonOmitNulls"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method

.method public final voiceServerPing()V
    .locals 1

    new-instance v0, Lcom/discord/gateway/GatewaySocket$voiceServerPing$1;

    invoke-direct {v0, p0}, Lcom/discord/gateway/GatewaySocket$voiceServerPing$1;-><init>(Lcom/discord/gateway/GatewaySocket;)V

    invoke-direct {p0, v0}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V
    .locals 9

    new-instance v8, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p6

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;-><init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/Long;Ljava/lang/Long;ZZLjava/lang/String;Z)V

    invoke-direct {p0, v8}, Lcom/discord/gateway/GatewaySocket;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
