.class public final Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;
.super Lx/m/c/k;
.source "GatewayDiscovery.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/gateway/GatewayDiscovery;->discoverGatewayUrl(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $handleFailure$1:Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$1;

.field public final synthetic $onSuccess:Lkotlin/jvm/functions/Function1;

.field public final synthetic this$0:Lcom/discord/gateway/GatewayDiscovery;


# direct methods
.method public constructor <init>(Lcom/discord/gateway/GatewayDiscovery;Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$1;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->this$0:Lcom/discord/gateway/GatewayDiscovery;

    iput-object p2, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->$handleFailure$1:Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$1;

    iput-object p3, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->$onSuccess:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->$handleFailure$1:Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$1;

    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Malformed gateway url."

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$1;->invoke(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->this$0:Lcom/discord/gateway/GatewayDiscovery;

    invoke-static {v0, p1}, Lcom/discord/gateway/GatewayDiscovery;->access$setGatewayUrl$p(Lcom/discord/gateway/GatewayDiscovery;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/gateway/GatewayDiscovery$Cache;->INSTANCE:Lcom/discord/gateway/GatewayDiscovery$Cache;

    invoke-virtual {v0, p1}, Lcom/discord/gateway/GatewayDiscovery$Cache;->setGatewayUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->this$0:Lcom/discord/gateway/GatewayDiscovery;

    invoke-static {v0}, Lcom/discord/gateway/GatewayDiscovery;->access$getLog$p(Lcom/discord/gateway/GatewayDiscovery;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Discovered gateway url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/gateway/GatewayDiscovery$discoverGatewayUrl$2;->$onSuccess:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method
