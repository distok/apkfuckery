.class public final synthetic Lcom/discord/gateway/GatewaySocket$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->values()[Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    const/4 v0, 0x2

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/gateway/GatewaySocket$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->NONE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v2, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->VERBOSE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    aput v0, v1, v3

    invoke-static {}, Lcom/discord/gateway/opcodes/Opcode;->values()[Lcom/discord/gateway/opcodes/Opcode;

    const/16 v1, 0x13

    new-array v1, v1, [I

    sput-object v1, Lcom/discord/gateway/GatewaySocket$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v2, Lcom/discord/gateway/opcodes/Opcode;->HELLO:Lcom/discord/gateway/opcodes/Opcode;

    const/16 v2, 0xb

    aput v3, v1, v2

    sget-object v2, Lcom/discord/gateway/opcodes/Opcode;->RECONNECT:Lcom/discord/gateway/opcodes/Opcode;

    const/16 v2, 0x8

    aput v0, v1, v2

    sget-object v2, Lcom/discord/gateway/opcodes/Opcode;->INVALID_SESSION:Lcom/discord/gateway/opcodes/Opcode;

    const/16 v2, 0xa

    const/4 v4, 0x3

    aput v4, v1, v2

    sget-object v2, Lcom/discord/gateway/opcodes/Opcode;->HEARTBEAT:Lcom/discord/gateway/opcodes/Opcode;

    const/4 v2, 0x4

    aput v2, v1, v0

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->HEARTBEAT_ACK:Lcom/discord/gateway/opcodes/Opcode;

    const/16 v0, 0xc

    const/4 v2, 0x5

    aput v2, v1, v0

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->DISPATCH:Lcom/discord/gateway/opcodes/Opcode;

    const/4 v0, 0x6

    aput v0, v1, v3

    return-void
.end method
