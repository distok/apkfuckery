.class public final Lcom/discord/gateway/GatewaySocket$IdentifyData;
.super Ljava/lang/Object;
.source "GatewaySocket.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/GatewaySocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IdentifyData"
.end annotation


# instance fields
.field private final clientState:Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

.field private final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;)V
    .locals 1

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$IdentifyData;->token:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/gateway/GatewaySocket$IdentifyData;->clientState:Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

    return-void
.end method


# virtual methods
.method public final getClientState()Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$IdentifyData;->clientState:Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$IdentifyData;->token:Ljava/lang/String;

    return-object v0
.end method
