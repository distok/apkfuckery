.class public final Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;
.super Ljava/lang/Object;
.source "GatewaySocket.kt"

# interfaces
.implements Lcom/discord/utilities/websocket/RawMessageHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/gateway/GatewaySocket;->connect(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $gatewayUrl$inlined:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/gateway/GatewaySocket;


# direct methods
.method public constructor <init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;->this$0:Lcom/discord/gateway/GatewaySocket;

    iput-object p2, p0, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;->$gatewayUrl$inlined:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRawMessage(Ljava/lang/String;)V
    .locals 1

    const-string v0, "rawMessage"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;->this$0:Lcom/discord/gateway/GatewaySocket;

    invoke-static {v0}, Lcom/discord/gateway/GatewaySocket;->access$getGatewaySocketLogger$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/gateway/GatewaySocketLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/discord/gateway/GatewaySocketLogger;->logInboundMessage(Ljava/lang/String;)V

    return-void
.end method

.method public onRawMessageInflateFailed(Ljava/lang/Throwable;)V
    .locals 1

    const-string/jumbo v0, "throwable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/gateway/GatewaySocket$connect$$inlined$apply$lambda$4;->this$0:Lcom/discord/gateway/GatewaySocket;

    invoke-static {v0}, Lcom/discord/gateway/GatewaySocket;->access$getGatewaySocketLogger$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/gateway/GatewaySocketLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/discord/gateway/GatewaySocketLogger;->logMessageInflateFailed(Ljava/lang/Throwable;)V

    return-void
.end method
