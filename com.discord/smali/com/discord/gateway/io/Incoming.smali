.class public final Lcom/discord/gateway/io/Incoming;
.super Ljava/lang/Object;
.source "Incoming.kt"


# instance fields
.field private final data:Ljava/lang/Object;

.field private final opcode:Lcom/discord/gateway/opcodes/Opcode;

.field private final seq:Ljava/lang/Integer;

.field private final type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "opcode"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/io/Incoming;->type:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/gateway/io/Incoming;->seq:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/discord/gateway/io/Incoming;->opcode:Lcom/discord/gateway/opcodes/Opcode;

    iput-object p4, p0, Lcom/discord/gateway/io/Incoming;->data:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getData()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/Incoming;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public final getOpcode()Lcom/discord/gateway/opcodes/Opcode;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/Incoming;->opcode:Lcom/discord/gateway/opcodes/Opcode;

    return-object v0
.end method

.method public final getSeq()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/Incoming;->seq:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/Incoming;->type:Ljava/lang/String;

    return-object v0
.end method
