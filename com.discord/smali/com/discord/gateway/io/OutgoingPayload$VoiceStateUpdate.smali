.class public final Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;
.super Lcom/discord/gateway/io/OutgoingPayload;
.source "Outgoing.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/io/OutgoingPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceStateUpdate"
.end annotation


# instance fields
.field private final channel_id:Ljava/lang/Long;

.field private final guild_id:Ljava/lang/Long;

.field private final preferred_region:Ljava/lang/String;

.field private final self_deaf:Z

.field private final self_mute:Z

.field private final self_video:Z


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/gateway/io/OutgoingPayload;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    iput-object p2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    iput-boolean p3, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    iput-boolean p4, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    iput-boolean p5, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    iput-object p6, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;-><init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->copy(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final component2()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    return v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;
    .locals 8

    new-instance v7, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;-><init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    iget-boolean v1, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    iget-boolean v1, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    iget-boolean v1, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel_id()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final getGuild_id()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final getPreferred_region()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelf_deaf()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    return v0
.end method

.method public final getSelf_mute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    return v0
.end method

.method public final getSelf_video()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    if-eqz v2, :cond_4

    goto :goto_2

    :cond_4
    move v3, v2

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "VoiceStateUpdate(guild_id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->guild_id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channel_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->channel_id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", self_mute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_mute:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", self_deaf="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_deaf:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", self_video="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->self_video:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", preferred_region="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->preferred_region:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
