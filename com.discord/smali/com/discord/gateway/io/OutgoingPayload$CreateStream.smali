.class public final Lcom/discord/gateway/io/OutgoingPayload$CreateStream;
.super Ljava/lang/Object;
.source "Outgoing.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/io/OutgoingPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateStream"
.end annotation


# instance fields
.field private final channel_id:J

.field private final guild_id:Ljava/lang/Long;

.field private final preferred_region:Ljava/lang/String;

.field private final type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->type:Ljava/lang/String;

    iput-wide p2, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->channel_id:J

    iput-object p4, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->guild_id:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->preferred_region:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getChannel_id()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->channel_id:J

    return-wide v0
.end method

.method public final getGuild_id()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->guild_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final getPreferred_region()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->preferred_region:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$CreateStream;->type:Ljava/lang/String;

    return-object v0
.end method
