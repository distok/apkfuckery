.class public final Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;
.super Ljava/lang/Object;
.source "Outgoing.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/io/OutgoingPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IdentifyClientState"
.end annotation


# instance fields
.field private final guild_hashes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final highest_last_message_id:J

.field private final read_state_version:I

.field private final user_guild_settings_version:I


# direct methods
.method public constructor <init>(Ljava/util/Map;JII)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "[",
            "Ljava/lang/String;",
            ">;JII)V"
        }
    .end annotation

    const-string v0, "guild_hashes"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->guild_hashes:Ljava/util/Map;

    iput-wide p2, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->highest_last_message_id:J

    iput p4, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->read_state_version:I

    iput p5, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->user_guild_settings_version:I

    return-void
.end method


# virtual methods
.method public final getGuild_hashes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->guild_hashes:Ljava/util/Map;

    return-object v0
.end method

.method public final getHighest_last_message_id()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->highest_last_message_id:J

    return-wide v0
.end method

.method public final getRead_state_version()I
    .locals 1

    iget v0, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->read_state_version:I

    return v0
.end method

.method public final getUser_guild_settings_version()I
    .locals 1

    iget v0, p0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;->user_guild_settings_version:I

    return v0
.end method
