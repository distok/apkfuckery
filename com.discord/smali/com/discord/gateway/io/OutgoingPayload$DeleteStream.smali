.class public final Lcom/discord/gateway/io/OutgoingPayload$DeleteStream;
.super Lcom/discord/gateway/io/OutgoingPayload;
.source "Outgoing.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/io/OutgoingPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteStream"
.end annotation


# instance fields
.field private final stream_key:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "stream_key"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/gateway/io/OutgoingPayload;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/gateway/io/OutgoingPayload$DeleteStream;->stream_key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getStream_key()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$DeleteStream;->stream_key:Ljava/lang/String;

    return-object v0
.end method
