.class public final Lcom/discord/gateway/io/OutgoingPayload$Identify;
.super Lcom/discord/gateway/io/OutgoingPayload;
.source "Outgoing.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/io/OutgoingPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Identify"
.end annotation


# instance fields
.field private final capabilities:J

.field private final client_state:Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

.field private final compress:Z

.field private final large_threshold:I

.field private final properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IZJLjava/util/Map;Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZJ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "properties"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/gateway/io/OutgoingPayload;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/gateway/io/OutgoingPayload$Identify;->token:Ljava/lang/String;

    iput p2, p0, Lcom/discord/gateway/io/OutgoingPayload$Identify;->large_threshold:I

    iput-boolean p3, p0, Lcom/discord/gateway/io/OutgoingPayload$Identify;->compress:Z

    iput-wide p4, p0, Lcom/discord/gateway/io/OutgoingPayload$Identify;->capabilities:J

    iput-object p6, p0, Lcom/discord/gateway/io/OutgoingPayload$Identify;->properties:Ljava/util/Map;

    iput-object p7, p0, Lcom/discord/gateway/io/OutgoingPayload$Identify;->client_state:Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

    return-void
.end method
