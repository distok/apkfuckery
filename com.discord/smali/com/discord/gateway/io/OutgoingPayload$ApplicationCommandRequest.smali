.class public final Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;
.super Lcom/discord/gateway/io/OutgoingPayload;
.source "Outgoing.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/io/OutgoingPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApplicationCommandRequest"
.end annotation


# instance fields
.field private final applications:Z

.field private final guild_id:J

.field private final limit:Ljava/lang/Integer;

.field private final nonce:Ljava/lang/String;

.field private final offset:Ljava/lang/Integer;

.field private final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "nonce"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/gateway/io/OutgoingPayload;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->guild_id:J

    iput-object p3, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->query:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->offset:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->limit:Ljava/lang/Integer;

    iput-boolean p6, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->applications:Z

    iput-object p7, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->nonce:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_1

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object v6, p4

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_2

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object v7, p5

    :goto_2
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    move/from16 v8, p6

    :goto_3
    move-object v2, p0

    move-wide v3, p1

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;-><init>(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getApplications()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->applications:Z

    return v0
.end method

.method public final getGuild_id()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->guild_id:J

    return-wide v0
.end method

.method public final getLimit()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->limit:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getNonce()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public final getOffset()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->offset:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;->query:Ljava/lang/String;

    return-object v0
.end method
