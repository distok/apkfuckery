.class public final Lcom/discord/gateway/io/IncomingParser;
.super Ljava/lang/Object;
.source "Incoming.kt"

# interfaces
.implements Lcom/discord/models/domain/Model;


# instance fields
.field private data:Ljava/lang/Object;

.field private final log:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private opcode:Lcom/discord/gateway/opcodes/Opcode;

.field private seq:Ljava/lang/Integer;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "log"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->log:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public assignField(Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "reader"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_d

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_4

    const/16 v2, 0xde1

    if-eq v1, v2, :cond_3

    const/16 v2, 0x73

    if-eq v1, v2, :cond_2

    const/16 v2, 0x74

    if-eq v1, v2, :cond_1

    goto/16 :goto_d

    :cond_1
    const-string/jumbo v1, "t"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/discord/gateway/io/IncomingParser;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->type:Ljava/lang/String;

    goto/16 :goto_e

    :cond_2
    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextIntOrNull()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->seq:Ljava/lang/Integer;

    goto/16 :goto_e

    :cond_3
    const-string v1, "op"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->Companion:Lcom/discord/gateway/opcodes/Opcode$Companion;

    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/gateway/opcodes/Opcode$Companion;->fromApiInt(I)Lcom/discord/gateway/opcodes/Opcode;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->opcode:Lcom/discord/gateway/opcodes/Opcode;

    goto/16 :goto_e

    :cond_4
    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/discord/gateway/io/IncomingParser;->opcode:Lcom/discord/gateway/opcodes/Opcode;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    const/16 v1, 0xa

    if-eq v0, v1, :cond_6

    const/16 v1, 0xb

    if-eq v0, v1, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    goto/16 :goto_e

    :cond_5
    new-instance v0, Lcom/discord/models/domain/ModelPayload$Hello;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelPayload$Hello;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextBoolean(Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :cond_7
    iget-object v0, p0, Lcom/discord/gateway/io/IncomingParser;->type:Ljava/lang/String;

    if-nez v0, :cond_8

    goto/16 :goto_c

    :cond_8
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_c

    :sswitch_0
    const-string v1, "STREAM_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;->INSTANCE:Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamCreateOrUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_1
    const-string v1, "GUILD_ROLE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_0

    :sswitch_2
    const-string v1, "VOICE_SERVER_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelVoice$Server;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelVoice$Server;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_3
    const-string v1, "VOICE_STATE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelVoice$State;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelVoice$State;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_4
    const-string v1, "PRESENCES_REPLACE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/gateway/io/IncomingParser$assignField$1;

    invoke-direct {v0, p1}, Lcom/discord/gateway/io/IncomingParser$assignField$1;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_5
    const-string v1, "STREAM_DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/StreamDelete$Parser;->INSTANCE:Lcom/discord/models/domain/StreamDelete$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/StreamDelete$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamDelete;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_6
    const-string v1, "GUILD_BAN_REMOVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_8

    :sswitch_7
    const-string v1, "STREAM_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;->INSTANCE:Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamCreateOrUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_8
    const-string v1, "MESSAGE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_1

    :sswitch_9
    const-string v1, "GUILD_ROLE_DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_0

    :sswitch_a
    const-string v1, "USER_CONNECTIONS_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_a

    :sswitch_b
    const-string v1, "GUILD_ROLE_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_0
    new-instance v0, Lcom/discord/models/domain/ModelGuildRole$Payload;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuildRole$Payload;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_c
    const-string v1, "USER_GUILD_SETTINGS_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelNotificationSettings;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelNotificationSettings;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_d
    const-string v1, "MESSAGE_ACK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/ModelReadState$Parser;->INSTANCE:Lcom/discord/models/domain/ModelReadState$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelReadState$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelReadState;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_e
    const-string v1, "GUILD_EMOJIS_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;

    invoke-direct {v0}, Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_f
    const-string v1, "MESSAGE_REACTION_REMOVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_b

    :sswitch_10
    const-string v1, "CHANNEL_RECIPIENT_ADD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_5

    :sswitch_11
    const-string v1, "MESSAGE_DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_2

    :sswitch_12
    const-string v1, "MESSAGE_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_1
    new-instance v0, Lcom/discord/models/domain/ModelMessage;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessage;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_13
    const-string v1, "USER_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p1}, Lf/e/c/a/a;->a0(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_14
    const-string v1, "CALL_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_3

    :sswitch_15
    const-string v1, "GUILD_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_4

    :sswitch_16
    const-string v1, "STREAM_SERVER_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/StreamServerUpdate$Parser;->INSTANCE:Lcom/discord/models/domain/StreamServerUpdate$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/StreamServerUpdate$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamServerUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_17
    const-string v1, "READY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/discord/gateway/io/IncomingParser;->log:Lkotlin/jvm/functions/Function1;

    const-string v1, "Ready payload data received."

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Lcom/discord/models/domain/ModelPayload;

    invoke-direct {v2}, Lcom/discord/models/domain/ModelPayload;-><init>()V

    invoke-virtual {p1, v2}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->log:Lkotlin/jvm/functions/Function1;

    const-string v2, "Ready payload deserialized in "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v3

    invoke-interface {v3}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " ms."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_18
    const-string v1, "GUILD_MEMBER_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_7

    :sswitch_19
    const-string v1, "MESSAGE_DELETE_BULK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_2
    new-instance v0, Lcom/discord/models/domain/ModelMessageDelete;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessageDelete;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_1a
    const-string v1, "GUILD_MEMBER_REMOVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelGuildMember;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuildMember;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_1b
    const-string v1, "CHANNEL_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_6

    :sswitch_1c
    const-string v1, "CHANNEL_UNREAD_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/ModelChannelUnreadUpdate$Parser;->INSTANCE:Lcom/discord/models/domain/ModelChannelUnreadUpdate$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelChannelUnreadUpdate$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelChannelUnreadUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_1d
    const-string v1, "CALL_DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_3

    :sswitch_1e
    const-string v1, "CALL_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_3
    new-instance v0, Lcom/discord/models/domain/ModelCall;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelCall;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_1f
    const-string v1, "GUILD_DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_4

    :sswitch_20
    const-string v1, "GUILD_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_4
    new-instance v0, Lcom/discord/models/domain/ModelGuild;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuild;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_21
    const-string v1, "TYPING_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelUser$Typing;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelUser$Typing;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_22
    const-string v1, "CHANNEL_RECIPIENT_REMOVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_5
    new-instance v0, Lcom/discord/models/domain/ModelChannel$Recipient;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelChannel$Recipient;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_23
    const-string v1, "CHANNEL_DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_6

    :sswitch_24
    const-string v1, "CHANNEL_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_6
    new-instance v0, Lcom/discord/models/domain/ModelChannel;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelChannel;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_25
    const-string v1, "RELATIONSHIP_REMOVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_9

    :sswitch_26
    const-string v1, "GUILD_APPLICATION_COMMANDS_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser;->INSTANCE:Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_27
    const-string v1, "USER_SETTINGS_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelUserSettings;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelUserSettings;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_28
    const-string v1, "GUILD_INTEGRATIONS_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelGuildIntegration$Update;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuildIntegration$Update;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_29
    const-string v1, "PRESENCE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/ModelPresence$Parser;->INSTANCE:Lcom/discord/models/domain/ModelPresence$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelPresence$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelPresence;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_2a
    const-string v1, "USER_NOTE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/ModelUserNote$Update$Parser;->INSTANCE:Lcom/discord/models/domain/ModelUserNote$Update$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelUserNote$Update$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelUserNote$Update;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_2b
    const-string v1, "GUILD_MEMBER_LIST_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Parser;->INSTANCE:Lcom/discord/models/domain/ModelGuildMemberListUpdate$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildMemberListUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_2c
    const-string v1, "MESSAGE_REACTION_ADD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_b

    :sswitch_2d
    const-string v1, "GUILD_MEMBER_ADD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_7
    new-instance v0, Lcom/discord/models/domain/ModelGuildMember;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuildMember;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_2e
    const-string v1, "GUILD_BAN_ADD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_8
    new-instance v0, Lcom/discord/models/domain/ModelBan;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelBan;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_2f
    const-string v1, "RELATIONSHIP_ADD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_9
    new-instance v0, Lcom/discord/models/domain/ModelUserRelationship;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelUserRelationship;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto/16 :goto_e

    :sswitch_30
    const-string v1, "USER_REQUIRED_ACTION_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto :goto_e

    :sswitch_31
    const-string v1, "MESSAGE_REACTION_REMOVE_ALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_b

    :sswitch_32
    const-string v1, "GUILD_MEMBERS_CHUNK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/models/domain/ModelGuildMember$Chunk;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuildMember$Chunk;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto :goto_e

    :sswitch_33
    const-string v1, "USER_CONNECTIONS_INTEGRATION_JOINING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_a
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto :goto_e

    :sswitch_34
    const-string v1, "SESSIONS_REPLACE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/gateway/io/IncomingParser$assignField$2;

    invoke-direct {v0, p1}, Lcom/discord/gateway/io/IncomingParser$assignField$2;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto :goto_e

    :sswitch_35
    const-string v1, "MESSAGE_REACTION_REMOVE_EMOJI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_b
    new-instance v0, Lcom/discord/models/domain/ModelMessageReaction$Update;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessageReaction$Update;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    goto :goto_e

    :cond_9
    :goto_c
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    goto :goto_e

    :cond_a
    const-string p1, "opcode"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_b
    :goto_d
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    :goto_e
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f659b57 -> :sswitch_35
        -0x78ca470e -> :sswitch_34
        -0x70da87b1 -> :sswitch_33
        -0x6cd113e5 -> :sswitch_32
        -0x5c91e13c -> :sswitch_31
        -0x57767eba -> :sswitch_30
        -0x4f1a5206 -> :sswitch_2f
        -0x4b2dfc3b -> :sswitch_2e
        -0x4a71b2b8 -> :sswitch_2d
        -0x49d2993d -> :sswitch_2c
        -0x47a432af -> :sswitch_2b
        -0x394108be -> :sswitch_2a
        -0x35d8e373 -> :sswitch_29
        -0x32448a83 -> :sswitch_28
        -0x2e5f9c0f -> :sswitch_27
        -0x2dbcd51b -> :sswitch_26
        -0x2ad96395 -> :sswitch_25
        -0x283cac28 -> :sswitch_24
        -0x273bc779 -> :sswitch_23
        -0x20ab369a -> :sswitch_22
        -0x1e714c86 -> :sswitch_21
        -0x10511518 -> :sswitch_20
        -0xf503069 -> :sswitch_1f
        -0xdeca603 -> :sswitch_1e
        -0xcebc154 -> :sswitch_1d
        -0xc585f03 -> :sswitch_1c
        -0x9a2145b -> :sswitch_1b
        -0x816eda3 -> :sswitch_1a
        -0x669dd32 -> :sswitch_19
        -0x261ad5e -> :sswitch_18
        0x4a3e183 -> :sswitch_17
        0x5195ac6 -> :sswitch_16
        0xe4982b5 -> :sswitch_15
        0x10adf1ca -> :sswitch_14
        0x17547bbd -> :sswitch_13
        0x3b7f2454 -> :sswitch_12
        0x3c800903 -> :sswitch_11
        0x46f81adf -> :sswitch_10
        0x4a7f4302 -> :sswitch_f
        0x4c1b20ef -> :sswitch_e
        0x4c5c6c11 -> :sswitch_d
        0x52d81e85 -> :sswitch_c
        0x58044679 -> :sswitch_b
        0x581fe407 -> :sswitch_a
        0x59052b28 -> :sswitch_9
        0x5a19bc21 -> :sswitch_8
        0x60555e9b -> :sswitch_7
        0x60ba6ec0 -> :sswitch_6
        0x6156434a -> :sswitch_5
        0x64b9c00d -> :sswitch_4
        0x654afa64 -> :sswitch_3
        0x702fd8d8 -> :sswitch_2
        0x769ede46 -> :sswitch_1
        0x7eeff668 -> :sswitch_0
    .end sparse-switch
.end method

.method public final build()Lcom/discord/gateway/io/Incoming;
    .locals 5

    new-instance v0, Lcom/discord/gateway/io/Incoming;

    iget-object v1, p0, Lcom/discord/gateway/io/IncomingParser;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/discord/gateway/io/IncomingParser;->seq:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/discord/gateway/io/IncomingParser;->opcode:Lcom/discord/gateway/opcodes/Opcode;

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/discord/gateway/io/IncomingParser;->data:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/gateway/io/Incoming;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    return-object v0

    :cond_0
    const-string v0, "opcode"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
