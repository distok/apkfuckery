.class public final enum Lcom/discord/gateway/opcodes/Opcode;
.super Ljava/lang/Enum;
.source "Opcode.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/gateway/opcodes/Opcode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/gateway/opcodes/Opcode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum CALL_CONNECT:Lcom/discord/gateway/opcodes/Opcode;

.field public static final Companion:Lcom/discord/gateway/opcodes/Opcode$Companion;

.field public static final enum DISPATCH:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum GUILD_SUBSCRIPTIONS:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum HEARTBEAT:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum HEARTBEAT_ACK:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum HELLO:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum IDENTIFY:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum INVALID_SESSION:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum PRESENCE_UPDATE:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum RECONNECT:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum REQUEST_GUILD_APPLICATION_COMMANDS:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum REQUEST_GUILD_MEMBERS:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum RESUME:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum STREAM_CREATE:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum STREAM_DELETE:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum STREAM_WATCH:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum UNHANDLED:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum VOICE_SERVER_PING:Lcom/discord/gateway/opcodes/Opcode;

.field public static final enum VOICE_STATE_UPDATE:Lcom/discord/gateway/opcodes/Opcode;

.field private static final intLookup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/discord/gateway/opcodes/Opcode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final apiInt:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x13

    new-array v1, v0, [Lcom/discord/gateway/opcodes/Opcode;

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "UNHANDLED"

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->UNHANDLED:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v4

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "DISPATCH"

    const/4 v5, 0x1

    invoke-direct {v2, v3, v5, v4}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->DISPATCH:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "HEARTBEAT"

    const/4 v6, 0x2

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->HEARTBEAT:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "IDENTIFY"

    const/4 v5, 0x3

    invoke-direct {v2, v3, v5, v6}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->IDENTIFY:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "PRESENCE_UPDATE"

    const/4 v6, 0x4

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->PRESENCE_UPDATE:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "VOICE_STATE_UPDATE"

    const/4 v5, 0x5

    invoke-direct {v2, v3, v5, v6}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->VOICE_STATE_UPDATE:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "VOICE_SERVER_PING"

    const/4 v6, 0x6

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->VOICE_SERVER_PING:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "RESUME"

    const/4 v5, 0x7

    invoke-direct {v2, v3, v5, v6}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->RESUME:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "RECONNECT"

    const/16 v6, 0x8

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->RECONNECT:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "REQUEST_GUILD_MEMBERS"

    const/16 v5, 0x9

    invoke-direct {v2, v3, v5, v6}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->REQUEST_GUILD_MEMBERS:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "INVALID_SESSION"

    const/16 v6, 0xa

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->INVALID_SESSION:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "HELLO"

    const/16 v5, 0xb

    invoke-direct {v2, v3, v5, v6}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->HELLO:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "HEARTBEAT_ACK"

    const/16 v6, 0xc

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->HEARTBEAT_ACK:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "CALL_CONNECT"

    const/16 v5, 0xd

    invoke-direct {v2, v3, v5, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->CALL_CONNECT:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "GUILD_SUBSCRIPTIONS"

    const/16 v5, 0xe

    invoke-direct {v2, v3, v5, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->GUILD_SUBSCRIPTIONS:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "STREAM_CREATE"

    const/16 v5, 0xf

    const/16 v6, 0x12

    invoke-direct {v2, v3, v5, v6}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->STREAM_CREATE:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "STREAM_DELETE"

    const/16 v5, 0x10

    invoke-direct {v2, v3, v5, v0}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->STREAM_DELETE:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "STREAM_WATCH"

    const/16 v5, 0x11

    const/16 v7, 0x14

    invoke-direct {v2, v3, v5, v7}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->STREAM_WATCH:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v5

    new-instance v2, Lcom/discord/gateway/opcodes/Opcode;

    const-string v3, "REQUEST_GUILD_APPLICATION_COMMANDS"

    const/16 v5, 0x18

    invoke-direct {v2, v3, v6, v5}, Lcom/discord/gateway/opcodes/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/discord/gateway/opcodes/Opcode;->REQUEST_GUILD_APPLICATION_COMMANDS:Lcom/discord/gateway/opcodes/Opcode;

    aput-object v2, v1, v6

    sput-object v1, Lcom/discord/gateway/opcodes/Opcode;->$VALUES:[Lcom/discord/gateway/opcodes/Opcode;

    new-instance v1, Lcom/discord/gateway/opcodes/Opcode$Companion;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/discord/gateway/opcodes/Opcode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v1, Lcom/discord/gateway/opcodes/Opcode;->Companion:Lcom/discord/gateway/opcodes/Opcode$Companion;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcom/discord/gateway/opcodes/Opcode;->values()[Lcom/discord/gateway/opcodes/Opcode;

    move-result-object v2

    :goto_0
    if-ge v4, v0, :cond_0

    aget-object v3, v2, v4

    iget v5, v3, Lcom/discord/gateway/opcodes/Opcode;->apiInt:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/discord/gateway/opcodes/Opcode;->intLookup:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/gateway/opcodes/Opcode;->apiInt:I

    return-void
.end method

.method public static final synthetic access$getIntLookup$cp()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->intLookup:Ljava/util/HashMap;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/gateway/opcodes/Opcode;
    .locals 1

    const-class v0, Lcom/discord/gateway/opcodes/Opcode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/gateway/opcodes/Opcode;

    return-object p0
.end method

.method public static values()[Lcom/discord/gateway/opcodes/Opcode;
    .locals 1

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->$VALUES:[Lcom/discord/gateway/opcodes/Opcode;

    invoke-virtual {v0}, [Lcom/discord/gateway/opcodes/Opcode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/gateway/opcodes/Opcode;

    return-object v0
.end method


# virtual methods
.method public final getApiInt()I
    .locals 1

    iget v0, p0, Lcom/discord/gateway/opcodes/Opcode;->apiInt:I

    return v0
.end method
