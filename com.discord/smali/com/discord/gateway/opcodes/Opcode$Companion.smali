.class public final Lcom/discord/gateway/opcodes/Opcode$Companion;
.super Ljava/lang/Object;
.source "Opcode.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/opcodes/Opcode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/gateway/opcodes/Opcode$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromApiInt(I)Lcom/discord/gateway/opcodes/Opcode;
    .locals 1

    invoke-static {}, Lcom/discord/gateway/opcodes/Opcode;->access$getIntLookup$cp()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/gateway/opcodes/Opcode;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/gateway/opcodes/Opcode;->UNHANDLED:Lcom/discord/gateway/opcodes/Opcode;

    :goto_0
    return-object p1
.end method
