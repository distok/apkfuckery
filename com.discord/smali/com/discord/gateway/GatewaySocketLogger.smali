.class public interface abstract Lcom/discord/gateway/GatewaySocketLogger;
.super Ljava/lang/Object;
.source "GatewaySocketLogger.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/gateway/GatewaySocketLogger$LogLevel;
    }
.end annotation


# virtual methods
.method public abstract getLogLevel()Lcom/discord/gateway/GatewaySocketLogger$LogLevel;
.end method

.method public abstract logInboundMessage(Ljava/lang/String;)V
.end method

.method public abstract logMessageInflateFailed(Ljava/lang/Throwable;)V
.end method

.method public abstract logOutboundMessage(Ljava/lang/String;)V
.end method
