.class public final Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;
.super Lx/m/c/k;
.source "GatewaySocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/gateway/GatewaySocket;->voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:Ljava/lang/Long;

.field public final synthetic $guildId:Ljava/lang/Long;

.field public final synthetic $preferredRegion:Ljava/lang/String;

.field public final synthetic $selfDeaf:Z

.field public final synthetic $selfMute:Z

.field public final synthetic $selfVideo:Z

.field public final synthetic this$0:Lcom/discord/gateway/GatewaySocket;


# direct methods
.method public constructor <init>(Lcom/discord/gateway/GatewaySocket;Ljava/lang/Long;Ljava/lang/Long;ZZLjava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->this$0:Lcom/discord/gateway/GatewaySocket;

    iput-object p2, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$guildId:Ljava/lang/Long;

    iput-object p3, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$channelId:Ljava/lang/Long;

    iput-boolean p4, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfMute:Z

    iput-boolean p5, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfDeaf:Z

    iput-object p6, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$preferredRegion:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfVideo:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 15

    sget-object v0, Lcom/discord/gateway/GatewaySocket;->Companion:Lcom/discord/gateway/GatewaySocket$Companion;

    iget-object v1, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->this$0:Lcom/discord/gateway/GatewaySocket;

    invoke-static {v1}, Lcom/discord/gateway/GatewaySocket;->access$getLogger$p(Lcom/discord/gateway/GatewaySocket;)Lcom/discord/utilities/logging/Logger;

    move-result-object v1

    const-string v2, "Sending voice state update for guild ["

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$guildId:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, "] and channel ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$channelId:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, "]. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "Muted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfMute:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, ", deafened: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfDeaf:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, ", preferredRegion: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$preferredRegion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/gateway/GatewaySocket$Companion;->log$default(Lcom/discord/gateway/GatewaySocket$Companion;Lcom/discord/utilities/logging/Logger;Ljava/lang/String;ZILjava/lang/Object;)V

    iget-object v6, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->this$0:Lcom/discord/gateway/GatewaySocket;

    new-instance v7, Lcom/discord/gateway/io/Outgoing;

    sget-object v0, Lcom/discord/gateway/opcodes/Opcode;->VOICE_STATE_UPDATE:Lcom/discord/gateway/opcodes/Opcode;

    new-instance v1, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    iget-object v9, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$guildId:Ljava/lang/Long;

    iget-object v10, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$channelId:Ljava/lang/Long;

    iget-boolean v11, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfMute:Z

    iget-boolean v12, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfDeaf:Z

    iget-boolean v13, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$selfVideo:Z

    iget-object v14, p0, Lcom/discord/gateway/GatewaySocket$voiceStateUpdate$1;->$preferredRegion:Ljava/lang/String;

    move-object v8, v1

    invoke-direct/range {v8 .. v14}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;-><init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V

    invoke-direct {v7, v0, v1}, Lcom/discord/gateway/io/Outgoing;-><init>(Lcom/discord/gateway/opcodes/Opcode;Ljava/lang/Object;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x6

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lcom/discord/gateway/GatewaySocket;->send$default(Lcom/discord/gateway/GatewaySocket;Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;ILjava/lang/Object;)V

    return-void
.end method
