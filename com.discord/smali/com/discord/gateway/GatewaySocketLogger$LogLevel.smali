.class public final enum Lcom/discord/gateway/GatewaySocketLogger$LogLevel;
.super Ljava/lang/Enum;
.source "GatewaySocketLogger.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/gateway/GatewaySocketLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LogLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/gateway/GatewaySocketLogger$LogLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

.field public static final enum NONE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

.field public static final enum VERBOSE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    new-instance v1, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->NONE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    const-string v2, "VERBOSE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->VERBOSE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->$VALUES:[Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/gateway/GatewaySocketLogger$LogLevel;
    .locals 1

    const-class v0, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    return-object p0
.end method

.method public static values()[Lcom/discord/gateway/GatewaySocketLogger$LogLevel;
    .locals 1

    sget-object v0, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->$VALUES:[Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    invoke-virtual {v0}, [Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    return-object v0
.end method
