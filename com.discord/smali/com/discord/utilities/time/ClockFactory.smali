.class public final Lcom/discord/utilities/time/ClockFactory;
.super Ljava/lang/Object;
.source "ClockFactory.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/time/ClockFactory;

.field private static ntpClock:Lcom/discord/utilities/time/NtpClock;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/time/ClockFactory;

    invoke-direct {v0}, Lcom/discord/utilities/time/ClockFactory;-><init>()V

    sput-object v0, Lcom/discord/utilities/time/ClockFactory;->INSTANCE:Lcom/discord/utilities/time/ClockFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final get()Lcom/discord/utilities/time/Clock;
    .locals 1

    sget-object v0, Lcom/discord/utilities/time/ClockFactory;->ntpClock:Lcom/discord/utilities/time/NtpClock;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "ntpClock"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method


# virtual methods
.method public final init(Landroid/app/Application;)V
    .locals 11

    const-string v0, "application"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/16 v10, 0x3e

    move-object v1, p1

    invoke-static/range {v1 .. v10}, Lf/j/a/a;->a(Landroid/content/Context;Lf/j/a/e;Ljava/util/List;JJJI)Lcom/lyft/kronos/KronosClock;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lf/j/a/g/b;

    iget-object v0, v0, Lf/j/a/g/b;->a:Lf/j/a/g/d/h;

    invoke-interface {v0}, Lf/j/a/g/d/h;->b()V

    new-instance v0, Lcom/discord/utilities/time/NtpClock;

    invoke-direct {v0, p1}, Lcom/discord/utilities/time/NtpClock;-><init>(Lcom/lyft/kronos/KronosClock;)V

    sput-object v0, Lcom/discord/utilities/time/ClockFactory;->ntpClock:Lcom/discord/utilities/time/NtpClock;

    return-void
.end method
