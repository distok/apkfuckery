.class public final Lcom/discord/utilities/time/NtpClock;
.super Ljava/lang/Object;
.source "NtpClock.kt"

# interfaces
.implements Lcom/discord/utilities/time/Clock;


# instance fields
.field private final kronosClock:Lcom/lyft/kronos/KronosClock;


# direct methods
.method public constructor <init>(Lcom/lyft/kronos/KronosClock;)V
    .locals 1

    const-string v0, "kronosClock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/time/NtpClock;->kronosClock:Lcom/lyft/kronos/KronosClock;

    return-void
.end method


# virtual methods
.method public currentTimeMillis()J
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/time/NtpClock;->kronosClock:Lcom/lyft/kronos/KronosClock;

    invoke-interface {v0}, Lcom/lyft/kronos/KronosClock;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getKronosClock()Lcom/lyft/kronos/KronosClock;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/time/NtpClock;->kronosClock:Lcom/lyft/kronos/KronosClock;

    return-object v0
.end method
