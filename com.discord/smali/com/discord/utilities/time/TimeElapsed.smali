.class public final Lcom/discord/utilities/time/TimeElapsed;
.super Ljava/lang/Object;
.source "TimeElapsed.kt"


# instance fields
.field private final milliseconds$delegate:Lkotlin/Lazy;

.field private final seconds$delegate:Lkotlin/Lazy;

.field private final startTime:J


# direct methods
.method public constructor <init>(Lcom/discord/utilities/time/Clock;J)V
    .locals 1

    const-string v0, "clock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/discord/utilities/time/TimeElapsed;->startTime:J

    new-instance p2, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;

    invoke-direct {p2, p0, p1}, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;-><init>(Lcom/discord/utilities/time/TimeElapsed;Lcom/discord/utilities/time/Clock;)V

    invoke-static {p2}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/time/TimeElapsed;->milliseconds$delegate:Lkotlin/Lazy;

    new-instance p1, Lcom/discord/utilities/time/TimeElapsed$seconds$2;

    invoke-direct {p1, p0}, Lcom/discord/utilities/time/TimeElapsed$seconds$2;-><init>(Lcom/discord/utilities/time/TimeElapsed;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/time/TimeElapsed;->seconds$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/time/Clock;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide p2

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/time/TimeElapsed;-><init>(Lcom/discord/utilities/time/Clock;J)V

    return-void
.end method

.method public static final synthetic access$getStartTime$p(Lcom/discord/utilities/time/TimeElapsed;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/time/TimeElapsed;->startTime:J

    return-wide v0
.end method


# virtual methods
.method public final getMilliseconds()J
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/time/TimeElapsed;->milliseconds$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getSeconds()F
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/time/TimeElapsed;->seconds$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method
