.class public final Lcom/discord/utilities/time/TimeUtils$UTCFormat;
.super Ljava/lang/Object;
.source "TimeUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/time/TimeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UTCFormat"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/time/TimeUtils$UTCFormat;

.field public static final LONG:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss"

.field public static final SHORT:Ljava/lang/String; = "yyyy-MM-dd"


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/time/TimeUtils$UTCFormat;

    invoke-direct {v0}, Lcom/discord/utilities/time/TimeUtils$UTCFormat;-><init>()V

    sput-object v0, Lcom/discord/utilities/time/TimeUtils$UTCFormat;->INSTANCE:Lcom/discord/utilities/time/TimeUtils$UTCFormat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
