.class public final Lcom/discord/utilities/time/TimeElapsed$seconds$2;
.super Lx/m/c/k;
.source "TimeElapsed.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/time/TimeElapsed;-><init>(Lcom/discord/utilities/time/Clock;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/time/TimeElapsed;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/time/TimeElapsed;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/time/TimeElapsed$seconds$2;->this$0:Lcom/discord/utilities/time/TimeElapsed;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()F
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/time/TimeElapsed$seconds$2;->this$0:Lcom/discord/utilities/time/TimeElapsed;

    invoke-virtual {v0}, Lcom/discord/utilities/time/TimeElapsed;->getMilliseconds()J

    move-result-wide v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/time/TimeElapsed$seconds$2;->invoke()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
