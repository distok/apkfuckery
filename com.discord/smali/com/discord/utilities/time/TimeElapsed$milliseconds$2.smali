.class public final Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;
.super Lx/m/c/k;
.source "TimeElapsed.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/time/TimeElapsed;-><init>(Lcom/discord/utilities/time/Clock;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $clock:Lcom/discord/utilities/time/Clock;

.field public final synthetic this$0:Lcom/discord/utilities/time/TimeElapsed;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/time/TimeElapsed;Lcom/discord/utilities/time/Clock;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;->this$0:Lcom/discord/utilities/time/TimeElapsed;

    iput-object p2, p0, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;->$clock:Lcom/discord/utilities/time/Clock;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()J
    .locals 4

    iget-object v0, p0, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;->$clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;->this$0:Lcom/discord/utilities/time/TimeElapsed;

    invoke-static {v2}, Lcom/discord/utilities/time/TimeElapsed;->access$getStartTime$p(Lcom/discord/utilities/time/TimeElapsed;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/utilities/time/TimeElapsed$milliseconds$2;->invoke()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
