.class public final Lcom/discord/utilities/surveys/SurveyUtils;
.super Ljava/lang/Object;
.source "SurveyUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/surveys/SurveyUtils$Survey;
    }
.end annotation


# static fields
.field private static final GENERIC_SURVEY_BUCKET_B_AND_A:I = 0xe

.field private static final GENERIC_SURVEY_BUCKET_CONSOLE_RESEARCH:I = 0x5

.field private static final GENERIC_SURVEY_BUCKET_COVID_NEW_USER:I = 0x6

.field private static final GENERIC_SURVEY_BUCKET_NPS:I = 0xa

.field private static final GENERIC_SURVEY_BUCKET_PRODUCT_FEEDBACK:I = 0xb

.field public static final INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils;

.field private static final MIN_APP_INSTALL_TIME:J = 0x240c8400L

.field private static applicationContext:Landroid/app/Application;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/surveys/SurveyUtils;

    invoke-direct {v0}, Lcom/discord/utilities/surveys/SurveyUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/surveys/SurveyUtils;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getGenericSurvey(Lcom/discord/utilities/surveys/SurveyUtils;Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/surveys/SurveyUtils;->getGenericSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getUserSurvey(Lcom/discord/utilities/surveys/SurveyUtils;Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/surveys/SurveyUtils;->getUserSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final getGenericSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/surveys/SurveyUtils$Survey;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v1, "2019-04_mobile-survey"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$1;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;

    invoke-direct {v1, p1}, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "StoreStream\n        .get\u2026one\n          }\n        }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getUserSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/surveys/SurveyUtils$Survey;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->isHypeSquad()Z

    move-result v0

    const-string v1, "StoreStream\n          .g\u2026            }\n          }"

    const/4 v2, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->isInHypesquadHouse()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v3, "2019-07_user-survey-android-non-hs"

    invoke-virtual {v0, v3, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$3;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$3;

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$4;

    invoke-direct {v2, p1}, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$4;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v3, "2019-07_user-survey-android-hs"

    invoke-virtual {v0, v3, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$1;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$1;

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$2;

    invoke-direct {v2, p1}, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$2;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method private final isInstallOldEnough()Z
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/discord/utilities/surveys/SurveyUtils;->applicationContext:Landroid/app/Application;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    const-string v3, "applicationContext"

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v4, Lcom/discord/utilities/surveys/SurveyUtils;->applicationContext:Landroid/app/Application;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x240c8400

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    throw v2

    :cond_1
    :try_start_2
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    throw v2

    :catch_0
    :cond_2
    :goto_0
    return v0
.end method


# virtual methods
.method public final getSurveyToShow()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/surveys/SurveyUtils$Survey;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/discord/utilities/surveys/SurveyUtils;->isInstallOldEnough()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string v0, "Observable.just(Survey.None)"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/surveys/SurveyUtils$getSurveyToShow$1;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$getSurveyToShow$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/surveys/SurveyUtils$getSurveyToShow$2;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$getSurveyToShow$2;

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n        .get\u2026              }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final init(Landroid/app/Application;)V
    .locals 1

    const-string v0, "applicationContext"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object p1, Lcom/discord/utilities/surveys/SurveyUtils;->applicationContext:Landroid/app/Application;

    return-void
.end method
