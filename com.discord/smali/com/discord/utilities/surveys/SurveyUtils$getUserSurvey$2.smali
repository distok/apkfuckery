.class public final Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$2;
.super Ljava/lang/Object;
.source "SurveyUtils.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/surveys/SurveyUtils;->getUserSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Boolean;",
        "Lcom/discord/utilities/surveys/SurveyUtils$Survey;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $meUser:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;)Lcom/discord/utilities/surveys/SurveyUtils$Survey;
    .locals 1

    const-string v0, "isInExperiment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$HypesquadSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p1, v0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$HypesquadSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/surveys/SurveyUtils$getUserSurvey$2;->call(Ljava/lang/Boolean;)Lcom/discord/utilities/surveys/SurveyUtils$Survey;

    move-result-object p1

    return-object p1
.end method
