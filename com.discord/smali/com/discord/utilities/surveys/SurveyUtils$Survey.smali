.class public abstract Lcom/discord/utilities/surveys/SurveyUtils$Survey;
.super Ljava/lang/Object;
.source "SurveyUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/surveys/SurveyUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Survey"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$HypesquadSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$EngagedUserSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$NpsSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$ConsoleResearchSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$CovidNewUserSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$BrandSurvey;,
        Lcom/discord/utilities/surveys/SurveyUtils$Survey$NoticeKeys;
    }
.end annotation


# instance fields
.field private final noticeKey:Ljava/lang/String;

.field private final surveyBody:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final surveyId:Ljava/lang/String;

.field private final surveyURL:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->surveyId:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->surveyURL:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->noticeKey:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->surveyBody:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/surveys/SurveyUtils$Survey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/surveys/SurveyUtils$Survey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public getNoticeKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->noticeKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getSurveyBody(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->getSurveyBody()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const v0, 0x7f12116a

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string p1, "activity.getString(R.string.notice_survey_body)"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public getSurveyBody()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->surveyBody:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getSurveyId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->surveyId:Ljava/lang/String;

    return-object v0
.end method

.method public getSurveyURL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->surveyURL:Ljava/lang/String;

    return-object v0
.end method
