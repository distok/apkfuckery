.class public final Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;
.super Lcom/discord/utilities/surveys/SurveyUtils$Survey;
.source "SurveyUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/surveys/SurveyUtils$Survey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductFeedbackSurvey"
.end annotation


# instance fields
.field private final meUser:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;)V
    .locals 9

    const-string v0, "meUser"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://www.surveymonkey.com/r/6KKSDCK?user_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey$1;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey$1;

    const-string v4, "6KKSDCK"

    const-string v6, "NOTICE_KEY_PRODUCT_FEEDBACK_SURVEY"

    const/4 v8, 0x0

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/discord/utilities/surveys/SurveyUtils$Survey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;Lcom/discord/models/domain/ModelUser;ILjava/lang/Object;)Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->copy(Lcom/discord/models/domain/ModelUser;)Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelUser;)Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;
    .locals 1

    const-string v0, "meUser"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;

    invoke-direct {v0, p1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    iget-object p1, p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMeUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ProductFeedbackSurvey(meUser="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
