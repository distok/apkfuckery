.class public final Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;
.super Ljava/lang/Object;
.source "SurveyUtils.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/surveys/SurveyUtils;->getGenericSurvey(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Integer;",
        "Lcom/discord/utilities/surveys/SurveyUtils$Survey;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $meUser:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Integer;)Lcom/discord/utilities/surveys/SurveyUtils$Survey;
    .locals 2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    new-instance p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$NpsSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p1, v0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$NpsSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    goto :goto_5

    :cond_1
    :goto_0
    const/4 v0, 0x5

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_3

    new-instance p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ConsoleResearchSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p1, v0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ConsoleResearchSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    goto :goto_5

    :cond_3
    :goto_1
    const/4 v0, 0x6

    if-nez p1, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_5

    new-instance p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$CovidNewUserSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p1, v0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$CovidNewUserSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    goto :goto_5

    :cond_5
    :goto_2
    const/16 v0, 0xb

    if-nez p1, :cond_6

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_7

    new-instance p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p1, v0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$ProductFeedbackSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    goto :goto_5

    :cond_7
    :goto_3
    const/16 v0, 0xe

    if-nez p1, :cond_8

    goto :goto_4

    :cond_8
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v0, :cond_9

    new-instance p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$BrandSurvey;

    iget-object v0, p0, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p1, v0}, Lcom/discord/utilities/surveys/SurveyUtils$Survey$BrandSurvey;-><init>(Lcom/discord/models/domain/ModelUser;)V

    goto :goto_5

    :cond_9
    :goto_4
    sget-object p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;

    :goto_5
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/surveys/SurveyUtils$getGenericSurvey$2;->call(Ljava/lang/Integer;)Lcom/discord/utilities/surveys/SurveyUtils$Survey;

    move-result-object p1

    return-object p1
.end method
