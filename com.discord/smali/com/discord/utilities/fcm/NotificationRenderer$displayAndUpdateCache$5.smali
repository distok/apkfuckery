.class public final Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;
.super Lx/m/c/k;
.source "NotificationRenderer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/fcm/NotificationRenderer;->displayAndUpdateCache(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData;Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $displayPayload:Lcom/discord/utilities/fcm/NotificationData$DisplayPayload;

.field public final synthetic $notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData$DisplayPayload;Landroidx/core/app/NotificationCompat$Builder;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->$displayPayload:Lcom/discord/utilities/fcm/NotificationData$DisplayPayload;

    iput-object p3, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->invoke(Ljava/lang/Long;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Long;)V
    .locals 4

    sget-object p1, Lcom/discord/utilities/fcm/NotificationRenderer;->INSTANCE:Lcom/discord/utilities/fcm/NotificationRenderer;

    iget-object v0, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->$displayPayload:Lcom/discord/utilities/fcm/NotificationData$DisplayPayload;

    invoke-virtual {v1}, Lcom/discord/utilities/fcm/NotificationData$DisplayPayload;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayAndUpdateCache$5;->$notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    const-string v3, "notificationBuilder"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/fcm/NotificationRenderer;->access$displayNotification(Lcom/discord/utilities/fcm/NotificationRenderer;Landroid/content/Context;ILandroidx/core/app/NotificationCompat$Builder;)V

    return-void
.end method
