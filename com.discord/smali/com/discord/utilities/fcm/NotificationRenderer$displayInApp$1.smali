.class public final Lcom/discord/utilities/fcm/NotificationRenderer$displayInApp$1;
.super Lx/m/c/k;
.source "NotificationRenderer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/fcm/NotificationRenderer;->displayInApp(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $notificationData:Lcom/discord/utilities/fcm/NotificationData;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/fcm/NotificationData;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayInApp$1;->$notificationData:Lcom/discord/utilities/fcm/NotificationData;

    iput-object p2, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayInApp$1;->$context:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/fcm/NotificationRenderer$displayInApp$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayInApp$1;->$notificationData:Lcom/discord/utilities/fcm/NotificationData;

    iget-object v0, p0, Lcom/discord/utilities/fcm/NotificationRenderer$displayInApp$1;->$context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/fcm/NotificationData;->getContentIntentInApp(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/PendingIntent;->send()V

    return-void
.end method
