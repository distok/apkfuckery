.class public final Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;
.super Ljava/lang/Object;
.source "PremiumGuildSubscriptionUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;
    }
.end annotation


# static fields
.field public static final DEFAULT_GUILD_SUBSCRIPTION_GUILD_COUNT:I = 0x1

.field public static final DEFAULT_GUILD_SUBSCRIPTION_SLOT_COUNT:I = 0x1

.field public static final INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

.field public static final PREMIUM_SUBSCRIPTION_COOLDOWN_DAYS:I = 0x7


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    invoke-direct {v0}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getCurrentTierSubs(I)I
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v0, :cond_2

    if-eq p1, v1, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x1e

    goto :goto_0

    :cond_1
    const/16 v1, 0xf

    :cond_2
    :goto_0
    return v1
.end method

.method private final getNextTierSubs(I)I
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v0, :cond_2

    if-eq p1, v1, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/16 v1, 0x1e

    goto :goto_0

    :cond_2
    const/16 v1, 0xf

    :goto_0
    return v1
.end method

.method private final modifySubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;ZLcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/rest/RestAPI;",
            "J",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Z",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
            ">;"
        }
    .end annotation

    if-eqz p5, :cond_0

    invoke-virtual {p1, p2, p3}, Lcom/discord/utilities/rest/RestAPI;->cancelSubscriptionSlot(J)Lrx/Observable;

    move-result-object p2

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p2, p3}, Lcom/discord/utilities/rest/RestAPI;->uncancelSubscriptionSlot(J)Lrx/Observable;

    move-result-object p2

    :goto_0
    invoke-static {}, Lg0/p/a;->c()Lrx/Scheduler;

    move-result-object p3

    invoke-virtual {p2, p3}, Lrx/Observable;->S(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p2

    new-instance p3, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$1;

    invoke-direct {p3, p6}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$1;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription;)V

    invoke-virtual {p2, p3}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p2

    sget-object p3, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$2;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$2;

    invoke-virtual {p2, p3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    sget-object p3, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$3;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$3;

    invoke-virtual {p2, p3}, Lrx/Observable;->I(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    new-instance p3, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;

    invoke-direct {p3, p1, p4, p5}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;-><init>(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/models/domain/ModelSubscription;Z)V

    invoke-virtual {p2, p3}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "apiObs\n        .subscrib\u2026N }\n          }\n        }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final calculateAdditionalPlansWithPremiumGuildAdjustment(Lcom/discord/models/domain/ModelSubscription;I)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "I)",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "subscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getAdditionalPlans()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getAdditionalPlans()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    :goto_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;->getPlanId()J

    move-result-wide v7

    sget-object v9, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v9

    cmp-long v11, v7, v9

    if-eqz v11, :cond_4

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;->getPlanId()J

    move-result-wide v6

    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v8

    cmp-long v10, v6, v8

    if-nez v10, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_2

    move-object v2, v5

    :cond_5
    check-cast v2, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;

    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;->getQuantity()I

    move-result v4

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    :goto_3
    add-int/2addr v4, p2

    if-ltz v4, :cond_d

    if-nez v2, :cond_8

    goto :goto_6

    :cond_8
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_9
    :goto_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;->getPlanId()J

    move-result-wide v6

    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v8

    cmp-long v10, v6, v8

    if-eqz v10, :cond_a

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;->getPlanId()J

    move-result-wide v5

    sget-object v7, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v7

    cmp-long v9, v5, v7

    if-eqz v9, :cond_a

    const/4 v5, 0x1

    goto :goto_5

    :cond_a
    const/4 v5, 0x0

    :goto_5
    if-eqz v5, :cond_9

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_b
    if-nez v4, :cond_c

    return-object p1

    :cond_c
    new-instance p2, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;->getPlanId()J

    move-result-wide v0

    invoke-direct {p2, v0, v1, v4}, Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;-><init>(JI)V

    invoke-static {p2}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-static {p1, p2}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_d
    :goto_6
    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error calculating additional_plans adjustment, "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "new sub count:"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getAdditionalPlans()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final calculatePercentToNextTier(II)I
    .locals 2

    const/16 v0, 0x64

    const/4 v1, 0x3

    if-lt p1, v1, :cond_0

    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->getNextTierSubs(I)I

    move-result v1

    invoke-direct {p0, p1}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->getCurrentTierSubs(I)I

    move-result p1

    sub-int/2addr p2, p1

    int-to-float p1, p2

    int-to-float p2, v1

    div-float/2addr p1, p2

    int-to-float p2, v0

    mul-float p1, p1, p2

    invoke-static {p1}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result p1

    return p1
.end method

.method public final calculateTotalProgress(II)I
    .locals 2

    const/4 v0, 0x3

    if-lt p1, v0, :cond_0

    const/16 p1, 0x64

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->getNextTierSubs(I)I

    move-result v0

    invoke-direct {p0, p1}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->getCurrentTierSubs(I)I

    move-result v1

    sub-int/2addr p2, v1

    int-to-float p2, p2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr p2, v0

    int-to-float p1, p1

    const v0, 0x42053333    # 33.3f

    mul-float p1, p1, v0

    mul-float p2, p2, v0

    add-float/2addr p2, p1

    invoke-static {p2}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result p1

    return p1
.end method

.method public final cancelSubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;Lcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/rest/RestAPI;",
            "J",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
            ">;"
        }
    .end annotation

    const-string v0, "api"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "subscription"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storePremiumGuild"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->modifySubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;ZLcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getPremiumGuildTier(I)I
    .locals 2

    const/4 v0, 0x2

    const/16 v1, 0x1e

    if-lt p1, v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/16 v1, 0xf

    if-lt p1, v1, :cond_1

    goto :goto_0

    :cond_1
    if-lt p1, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final uncancelSubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;Lcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/rest/RestAPI;",
            "J",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
            ">;"
        }
    .end annotation

    const-string v0, "api"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "subscription"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storePremiumGuild"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->modifySubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;ZLcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
