.class public final Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$1;
.super Ljava/lang/Object;
.source "PremiumGuildSubscriptionUtils.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->modifySubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;ZLcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $storePremiumGuild:Lcom/discord/stores/StorePremiumGuildSubscription;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePremiumGuildSubscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$1;->$storePremiumGuild:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$1;->$storePremiumGuild:Lcom/discord/stores/StorePremiumGuildSubscription;

    const-string v1, "it"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription;->updateUserPremiumGuildSubscriptionSlot(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$1;->call(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V

    return-void
.end method
