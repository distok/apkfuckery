.class public final Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;
.super Ljava/lang/Object;
.source "PremiumGuildSubscriptionUtils.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->modifySubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;ZLcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Boolean;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $api:Lcom/discord/utilities/rest/RestAPI;

.field public final synthetic $cancel:Z

.field public final synthetic $subscription:Lcom/discord/models/domain/ModelSubscription;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/models/domain/ModelSubscription;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$api:Lcom/discord/utilities/rest/RestAPI;

    iput-object p2, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$subscription:Lcom/discord/models/domain/ModelSubscription;

    iput-boolean p3, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$cancel:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->call(Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Boolean;)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;->FAILURE_MODIFYING_SLOT:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$api:Lcom/discord/utilities/rest/RestAPI;

    iget-object v0, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$subscription:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v0

    new-instance v8, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v1, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    iget-object v5, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$subscription:Lcom/discord/models/domain/ModelSubscription;

    iget-boolean v6, p0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4;->$cancel:Z

    if-eqz v6, :cond_1

    const/4 v6, -0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v1, v5, v6}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->calculateAdditionalPlansWithPremiumGuildAdjustment(Lcom/discord/models/domain/ModelSubscription;I)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/discord/restapi/RestAPIParams$UpdateSubscription;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p1, v0, v8}, Lcom/discord/utilities/rest/RestAPI;->updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4$1;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4$2;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$modifySubscriptionSlot$4$2;

    invoke-virtual {p1, v0}, Lrx/Observable;->I(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    :goto_1
    return-object v0
.end method
