.class public final Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "RoundedRelativeLayout.kt"


# instance fields
.field private final roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance p1, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    invoke-direct {p1}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    invoke-direct {p1}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    invoke-direct {p0, p2}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    invoke-direct {p1}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    invoke-direct {p0, p2}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$draw$s1843631363(Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private final initialize(Landroid/util/AttributeSet;)V
    .locals 8

    iget-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    sget-object v3, Lcom/discord/utils/R$e;->RoundedRelativeLayout:[I

    const-string v1, "R.styleable.RoundedRelativeLayout"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget v4, Lcom/discord/utils/R$e;->RoundedRelativeLayout_topLeftRadius:I

    sget v5, Lcom/discord/utils/R$e;->RoundedRelativeLayout_topRightRadius:I

    sget v6, Lcom/discord/utils/R$e;->RoundedRelativeLayout_bottomLeftRadius:I

    sget v7, Lcom/discord/utils/R$e;->RoundedRelativeLayout_bottomRightRadius:I

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->initialize(Landroid/view/View;Landroid/util/AttributeSet;[IIIII)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    new-instance v1, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout$draw$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout$draw$1;-><init>(Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;)V

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->draw(Landroid/graphics/Canvas;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    iget-object p3, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    invoke-virtual {p3, p1, p2}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->onSizeChanged(II)V

    return-void
.end method

.method public final updateTopLeftRadius(F)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    sget-object v1, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;->TOP_LEFT:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;

    invoke-virtual {v0, p0, p1, v1}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->updateRadius(Landroid/view/View;FLcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;)V

    return-void
.end method

.method public final updateTopRightRadius(F)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->roundedViewCoordinator:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;

    sget-object v1, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;->TOP_RIGHT:Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;

    invoke-virtual {v0, p0, p1, v1}, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->updateRadius(Landroid/view/View;FLcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;)V

    return-void
.end method
