.class public final Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;
.super Ljava/lang/Object;
.source "RoundedCornerViewCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;
    }
.end annotation


# instance fields
.field private bottomLeftRadius:F

.field private bottomRightRadius:F

.field private path:Landroid/graphics/Path;

.field private final radii:[F

.field private topLeftRadius:F

.field private topRightRadius:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->path:Landroid/graphics/Path;

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->radii:[F

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/graphics/Canvas;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDraw"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->path:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public final initialize(Landroid/view/View;Landroid/util/AttributeSet;[IIIII)V
    .locals 1
    .param p3    # [I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrsIndexArray"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string/jumbo p2, "view.context.obtainStyle\u2026s(attrs, attrsIndexArray)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    :try_start_0
    invoke-virtual {p1, p4, p2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p3

    iput p3, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->topLeftRadius:F

    invoke-virtual {p1, p5, p2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p3

    iput p3, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->topRightRadius:F

    invoke-virtual {p1, p6, p2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p3

    iput p3, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->bottomLeftRadius:F

    invoke-virtual {p1, p7, p2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->bottomRightRadius:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public final onSizeChanged(II)V
    .locals 4

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->radii:[F

    iget v2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->topLeftRadius:F

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v3, 0x1

    aput v2, v1, v3

    iget v2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->topRightRadius:F

    const/4 v3, 0x2

    aput v2, v1, v3

    const/4 v3, 0x3

    aput v2, v1, v3

    iget v2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->bottomRightRadius:F

    const/4 v3, 0x4

    aput v2, v1, v3

    const/4 v3, 0x5

    aput v2, v1, v3

    iget v2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->bottomLeftRadius:F

    const/4 v3, 0x6

    aput v2, v1, v3

    const/4 v3, 0x7

    aput v2, v1, v3

    new-instance v1, Landroid/graphics/RectF;

    int-to-float p1, p1

    int-to-float p2, p2

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object p1, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->radii:[F

    sget-object p2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, p1, p2}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    iget-object p1, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->path:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    return-void
.end method

.method public final updateRadius(Landroid/view/View;FLcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator$Corner;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "corner"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Enum;->ordinal()I

    move-result p3

    if-eqz p3, :cond_3

    const/4 v0, 0x1

    if-eq p3, v0, :cond_2

    const/4 v0, 0x2

    if-eq p3, v0, :cond_1

    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    goto :goto_0

    :cond_0
    iput p2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->bottomRightRadius:F

    goto :goto_0

    :cond_1
    iput p2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->bottomLeftRadius:F

    goto :goto_0

    :cond_2
    iput p2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->topRightRadius:F

    goto :goto_0

    :cond_3
    iput p2, p0, Lcom/discord/utilities/view/rounded/RoundedCornerViewCoordinator;->topLeftRadius:F

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method
