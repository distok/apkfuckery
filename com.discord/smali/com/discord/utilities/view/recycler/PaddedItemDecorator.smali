.class public final Lcom/discord/utilities/view/recycler/PaddedItemDecorator;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "PaddedItemDecorator.kt"


# instance fields
.field private final orientation:I

.field private final spaceBetweenItemsOnly:Z

.field private final spaceHorz:I

.field private final spaceVert:I


# direct methods
.method public constructor <init>(IIIZ)V
    .locals 0

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    iput p1, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->orientation:I

    iput p2, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceHorz:I

    iput p3, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceVert:I

    iput-boolean p4, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceBetweenItemsOnly:Z

    return-void
.end method

.method public synthetic constructor <init>(IIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;-><init>(IIIZ)V

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 2

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "state"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3, p4}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result p2

    const/4 p3, 0x1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p4}, Landroidx/recyclerview/widget/RecyclerView$State;->getItemCount()I

    move-result p4

    sub-int/2addr p4, p3

    if-ne p2, p4, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    iget p4, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->orientation:I

    if-eqz p4, :cond_6

    if-ne p4, p3, :cond_5

    if-eqz v1, :cond_2

    iget-boolean p3, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceBetweenItemsOnly:Z

    if-nez p3, :cond_2

    iget p3, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceVert:I

    goto :goto_2

    :cond_2
    const/4 p3, 0x0

    :goto_2
    iput p3, p1, Landroid/graphics/Rect;->top:I

    if-eqz p2, :cond_3

    iget-boolean p2, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceBetweenItemsOnly:Z

    if-nez p2, :cond_4

    :cond_3
    iget v0, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceVert:I

    :cond_4
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    iget p2, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceHorz:I

    iput p2, p1, Landroid/graphics/Rect;->left:I

    iput p2, p1, Landroid/graphics/Rect;->right:I

    goto :goto_4

    :cond_5
    const-string p1, "Other("

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget p2, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->orientation:I

    const-string p3, ") orientation padding for items not supported yet."

    invoke-static {p1, p2, p3}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Lx/c;

    const-string p3, "An operation is not implemented: "

    invoke-static {p3, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lx/c;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    iget p3, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceVert:I

    iput p3, p1, Landroid/graphics/Rect;->top:I

    iput p3, p1, Landroid/graphics/Rect;->bottom:I

    if-eqz v1, :cond_7

    iget-boolean p3, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceBetweenItemsOnly:Z

    if-nez p3, :cond_7

    iget p3, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceHorz:I

    goto :goto_3

    :cond_7
    const/4 p3, 0x0

    :goto_3
    iput p3, p1, Landroid/graphics/Rect;->left:I

    if-eqz p2, :cond_8

    iget-boolean p2, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceBetweenItemsOnly:Z

    if-nez p2, :cond_9

    :cond_8
    iget v0, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceHorz:I

    :cond_9
    iput v0, p1, Landroid/graphics/Rect;->right:I

    :goto_4
    return-void
.end method

.method public final getOrientation()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->orientation:I

    return v0
.end method

.method public final getSpaceBetweenItemsOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceBetweenItemsOnly:Z

    return v0
.end method

.method public final getSpaceHorz()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceHorz:I

    return v0
.end method

.method public final getSpaceVert()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;->spaceVert:I

    return v0
.end method
