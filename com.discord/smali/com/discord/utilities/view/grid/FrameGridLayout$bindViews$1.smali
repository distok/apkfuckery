.class public final Lcom/discord/utilities/view/grid/FrameGridLayout$bindViews$1;
.super Lx/m/c/k;
.source "FrameGridLayout.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/view/grid/FrameGridLayout;->bindViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/discord/utilities/view/grid/FrameGridLayout$Data;",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/view/grid/FrameGridLayout;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/view/grid/FrameGridLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/view/grid/FrameGridLayout$bindViews$1;->this$0:Lcom/discord/utilities/view/grid/FrameGridLayout;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/view/grid/FrameGridLayout$Data;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/view/grid/FrameGridLayout$bindViews$1;->invoke(Lcom/discord/utilities/view/grid/FrameGridLayout$Data;Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/view/grid/FrameGridLayout$Data;Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$bindView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p2, Lcom/discord/utilities/view/grid/FrameGridLayout$DataView;

    if-nez v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    check-cast p2, Lcom/discord/utilities/view/grid/FrameGridLayout$DataView;

    if-eqz p2, :cond_1

    invoke-interface {p2, p1}, Lcom/discord/utilities/view/grid/FrameGridLayout$DataView;->onBind(Lcom/discord/utilities/view/grid/FrameGridLayout$Data;)V

    iget-object v0, p0, Lcom/discord/utilities/view/grid/FrameGridLayout$bindViews$1;->this$0:Lcom/discord/utilities/view/grid/FrameGridLayout;

    invoke-static {v0}, Lcom/discord/utilities/view/grid/FrameGridLayout;->access$getOnBindView$p(Lcom/discord/utilities/view/grid/FrameGridLayout;)Lkotlin/jvm/functions/Function2;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast p2, Landroid/view/View;

    invoke-interface {v0, p2, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method
