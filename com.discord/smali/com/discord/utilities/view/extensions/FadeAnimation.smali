.class public final Lcom/discord/utilities/view/extensions/FadeAnimation;
.super Ljava/lang/Object;
.source "ViewExtensions.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/view/extensions/FadeAnimation$Type;
    }
.end annotation


# instance fields
.field private final type:Lcom/discord/utilities/view/extensions/FadeAnimation$Type;

.field private final viewPropertyAnimator:Landroid/view/ViewPropertyAnimator;


# direct methods
.method public constructor <init>(Landroid/view/ViewPropertyAnimator;Lcom/discord/utilities/view/extensions/FadeAnimation$Type;)V
    .locals 1

    const-string/jumbo v0, "viewPropertyAnimator"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/view/extensions/FadeAnimation;->viewPropertyAnimator:Landroid/view/ViewPropertyAnimator;

    iput-object p2, p0, Lcom/discord/utilities/view/extensions/FadeAnimation;->type:Lcom/discord/utilities/view/extensions/FadeAnimation$Type;

    return-void
.end method


# virtual methods
.method public final getType()Lcom/discord/utilities/view/extensions/FadeAnimation$Type;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/view/extensions/FadeAnimation;->type:Lcom/discord/utilities/view/extensions/FadeAnimation$Type;

    return-object v0
.end method

.method public final getViewPropertyAnimator()Landroid/view/ViewPropertyAnimator;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/view/extensions/FadeAnimation;->viewPropertyAnimator:Landroid/view/ViewPropertyAnimator;

    return-object v0
.end method
