.class public final Lcom/discord/utilities/view/extensions/ViewExtensions$interceptScrollWhenInsideScrollable$1;
.super Ljava/lang/Object;
.source "ViewExtensions.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/view/extensions/ViewExtensions;->interceptScrollWhenInsideScrollable(Lcom/google/android/material/textfield/TextInputLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/view/extensions/ViewExtensions$interceptScrollWhenInsideScrollable$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/view/extensions/ViewExtensions$interceptScrollWhenInsideScrollable$1;

    invoke-direct {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions$interceptScrollWhenInsideScrollable$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/view/extensions/ViewExtensions$interceptScrollWhenInsideScrollable$1;->INSTANCE:Lcom/discord/utilities/view/extensions/ViewExtensions$interceptScrollWhenInsideScrollable$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->isFocused()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
