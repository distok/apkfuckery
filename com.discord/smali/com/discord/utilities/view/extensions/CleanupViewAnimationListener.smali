.class public final Lcom/discord/utilities/view/extensions/CleanupViewAnimationListener;
.super Ljava/lang/Object;
.source "ViewExtensions.kt"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/view/extensions/CleanupViewAnimationListener;->view:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/view/extensions/CleanupViewAnimationListener;->view:Landroid/view/View;

    return-object v0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    invoke-static {}, Lcom/discord/utilities/view/extensions/ViewExtensions;->access$getFadeAnimations$p()Ljava/util/HashMap;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/view/extensions/CleanupViewAnimationListener;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    invoke-static {}, Lcom/discord/utilities/view/extensions/ViewExtensions;->access$getFadeAnimations$p()Ljava/util/HashMap;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/view/extensions/CleanupViewAnimationListener;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method
