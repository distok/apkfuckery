.class public Lcom/discord/utilities/view/chips/Chip$ChipParams;
.super Ljava/lang/Object;
.source "Chip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/view/chips/Chip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChipParams"
.end annotation


# instance fields
.field public final chipHeight:I

.field public final chipLayout:I

.field public final chipsBgColor:I

.field public final chipsBgColorClicked:I

.field public final chipsBgRes:I

.field public final chipsColor:I

.field public final chipsColorClicked:I

.field public final chipsDeleteResId:I

.field public final chipsPlaceholderResId:I

.field public final chipsTextColor:I

.field public final chipsTextColorClicked:I

.field public final density:F


# direct methods
.method public constructor <init>(IFIIIIIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsBgColorClicked:I

    iput p2, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->density:F

    iput p3, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsBgRes:I

    iput p4, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsBgColor:I

    iput p5, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsTextColor:I

    iput p6, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsPlaceholderResId:I

    iput p7, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsDeleteResId:I

    iput p8, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsTextColorClicked:I

    iput p9, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsColorClicked:I

    iput p10, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipsColor:I

    iput p11, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipHeight:I

    iput p12, p0, Lcom/discord/utilities/view/chips/Chip$ChipParams;->chipLayout:I

    return-void
.end method
