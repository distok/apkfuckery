.class public Lcom/discord/utilities/view/chips/ChipsImageUtil;
.super Ljava/lang/Object;
.source "ChipsImageUtil.java"


# static fields
.field private static final SMALL_IMAGE_MAX_SIZE:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getImageRequest(Ljava/lang/String;II)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;
    .locals 2

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v0

    sget-object v1, Lcom/facebook/imagepipeline/request/ImageRequest$c;->d:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iput-object v1, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    const-string v1, "gif"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/16 p0, 0xc8

    if-gt p1, p0, :cond_0

    if-gt p2, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    sget-object p0, Lcom/facebook/imagepipeline/request/ImageRequest$b;->d:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    goto :goto_1

    :cond_1
    sget-object p0, Lcom/facebook/imagepipeline/request/ImageRequest$b;->e:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    :goto_1
    iput-object p0, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->f:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    if-lez p1, :cond_2

    if-lez p2, :cond_2

    new-instance p0, Lf/g/j/d/e;

    invoke-direct {p0, p1, p2}, Lf/g/j/d/e;-><init>(II)V

    iput-object p0, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->c:Lf/g/j/d/e;

    :cond_2
    return-object v0
.end method

.method public static setImage(Landroid/widget/ImageView;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {p0, p1, p2, p2}, Lcom/discord/utilities/view/chips/ChipsImageUtil;->setImage(Landroid/widget/ImageView;Ljava/lang/String;II)V

    return-void
.end method

.method public static setImage(Landroid/widget/ImageView;Ljava/lang/String;II)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    check-cast p0, Lcom/facebook/drawee/view/DraweeView;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {}, Lf/g/g/a/a/b;->a()Lf/g/g/a/a/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()Lcom/facebook/drawee/interfaces/DraweeController;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->k:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-virtual {v1, v0}, Lf/g/g/a/a/d;->f(Landroid/net/Uri;)Lf/g/g/a/a/d;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->j:Z

    invoke-static {p1, p2, p3}, Lcom/discord/utilities/view/chips/ChipsImageUtil;->getImageRequest(Ljava/lang/String;II)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object p1

    iput-object p1, v0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->a()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    return-void
.end method
