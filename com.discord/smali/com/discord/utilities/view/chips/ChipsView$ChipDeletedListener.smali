.class public interface abstract Lcom/discord/utilities/view/chips/ChipsView$ChipDeletedListener;
.super Ljava/lang/Object;
.source "ChipsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/view/chips/ChipsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ChipDeletedListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/discord/utilities/view/chips/ChipsView$DataContract;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onChipDeleted(Lcom/discord/utilities/view/chips/ChipsView$DataContract;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method
