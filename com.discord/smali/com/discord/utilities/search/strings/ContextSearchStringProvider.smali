.class public final Lcom/discord/utilities/search/strings/ContextSearchStringProvider;
.super Ljava/lang/Object;
.source "ContextSearchStringProvider.kt"

# interfaces
.implements Lcom/discord/utilities/search/strings/SearchStringProvider;


# instance fields
.field private final embedAnswerString:Ljava/lang/String;

.field private final fileAnswerString:Ljava/lang/String;

.field private final fromFilterString:Ljava/lang/String;

.field private final hasFilterString:Ljava/lang/String;

.field private final imageAnswerString:Ljava/lang/String;

.field private final inFilterString:Ljava/lang/String;

.field private final linkAnswerString:Ljava/lang/String;

.field private final mentionsFilterString:Ljava/lang/String;

.field private final soundAnswerString:Ljava/lang/String;

.field private final videoAnswerString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f12160a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.search_filter_from)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->fromFilterString:Ljava/lang/String;

    const v0, 0x7f12160c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.search_filter_in)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->inFilterString:Ljava/lang/String;

    const v0, 0x7f12160e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.search_filter_mentions)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->mentionsFilterString:Ljava/lang/String;

    const v0, 0x7f12160b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.search_filter_has)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->hasFilterString:Ljava/lang/String;

    const v0, 0x7f1215f5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.string.search_answer_has_link)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->linkAnswerString:Ljava/lang/String;

    const v0, 0x7f1215f3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026.search_answer_has_embed)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->embedAnswerString:Ljava/lang/String;

    const v0, 0x7f1215f2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026ch_answer_has_attachment)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->fileAnswerString:Ljava/lang/String;

    const v0, 0x7f1215f7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026.search_answer_has_video)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->videoAnswerString:Ljava/lang/String;

    const v0, 0x7f1215f4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026.search_answer_has_image)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->imageAnswerString:Ljava/lang/String;

    const v0, 0x7f1215f6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.stri\u2026.search_answer_has_sound)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->soundAnswerString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getEmbedAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->embedAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getFileAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->fileAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getFromFilterString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->fromFilterString:Ljava/lang/String;

    return-object v0
.end method

.method public getHasFilterString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->hasFilterString:Ljava/lang/String;

    return-object v0
.end method

.method public getImageAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->imageAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getInFilterString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->inFilterString:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->linkAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getMentionsFilterString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->mentionsFilterString:Ljava/lang/String;

    return-object v0
.end method

.method public getSoundAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->soundAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;->videoAnswerString:Ljava/lang/String;

    return-object v0
.end method
