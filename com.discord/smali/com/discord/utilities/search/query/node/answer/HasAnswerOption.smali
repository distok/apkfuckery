.class public final enum Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;
.super Ljava/lang/Enum;
.source "HasNode.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/search/query/node/answer/HasAnswerOption$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

.field public static final Companion:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption$Companion;

.field public static final enum EMBED:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

.field public static final enum FILE:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

.field public static final enum IMAGE:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

.field public static final enum LINK:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

.field public static final enum SOUND:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

.field public static final enum VIDEO:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;


# instance fields
.field private final restParamValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    new-instance v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    const-string v2, "LINK"

    const/4 v3, 0x0

    const-string v4, "link"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->LINK:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    const-string v2, "EMBED"

    const/4 v3, 0x1

    const-string v4, "embed"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->EMBED:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    const-string v2, "FILE"

    const/4 v3, 0x2

    const-string v4, "file"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->FILE:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    const-string v2, "VIDEO"

    const/4 v3, 0x3

    const-string/jumbo v4, "video"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->VIDEO:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    const-string v2, "IMAGE"

    const/4 v3, 0x4

    const-string v4, "image"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->IMAGE:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    const-string v2, "SOUND"

    const/4 v3, 0x5

    const-string v4, "sound"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->SOUND:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->$VALUES:[Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    new-instance v0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->Companion:Lcom/discord/utilities/search/query/node/answer/HasAnswerOption$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->restParamValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;
    .locals 1

    const-class v0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;
    .locals 1

    sget-object v0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->$VALUES:[Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    invoke-virtual {v0}, [Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    return-object v0
.end method


# virtual methods
.method public final getLocalizedInputText(Lcom/discord/utilities/search/strings/SearchStringProvider;)Ljava/lang/String;
    .locals 2

    const-string v0, "searchStringProvider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Lcom/discord/utilities/search/strings/SearchStringProvider;->getSoundAnswerString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    invoke-interface {p1}, Lcom/discord/utilities/search/strings/SearchStringProvider;->getImageAnswerString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/discord/utilities/search/strings/SearchStringProvider;->getVideoAnswerString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Lcom/discord/utilities/search/strings/SearchStringProvider;->getFileAnswerString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Lcom/discord/utilities/search/strings/SearchStringProvider;->getEmbedAnswerString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_5
    invoke-interface {p1}, Lcom/discord/utilities/search/strings/SearchStringProvider;->getLinkAnswerString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getRestParamValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;->restParamValue:Ljava/lang/String;

    return-object v0
.end method
