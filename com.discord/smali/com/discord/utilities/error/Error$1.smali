.class public synthetic Lcom/discord/utilities/error/Error$1;
.super Ljava/lang/Object;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/error/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1009
    name = null
.end annotation


# static fields
.field public static final synthetic $SwitchMap$com$discord$utilities$error$Error$Type:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/discord/utilities/error/Error$Type;->values()[Lcom/discord/utilities/error/Error$Type;

    const/16 v0, 0x11

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    const/4 v1, 0x1

    const/16 v2, 0x9

    :try_start_0
    sget-object v3, Lcom/discord/utilities/error/Error$Type;->DISCORD_REQUEST_ERROR_UNKNOWN:Lcom/discord/utilities/error/Error$Type;

    aput v1, v0, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x2

    :try_start_1
    sget-object v3, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v4, Lcom/discord/utilities/error/Error$Type;->OTHER:Lcom/discord/utilities/error/Error$Type;

    const/16 v4, 0x10

    aput v0, v3, v4
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v3, 0x3

    :try_start_2
    sget-object v4, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v5, Lcom/discord/utilities/error/Error$Type;->INTERMITTENT_CLOUD_FLARE:Lcom/discord/utilities/error/Error$Type;

    aput v3, v4, v0
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v4, Lcom/discord/utilities/error/Error$Type;->FORBIDDEN_CLOUD_FLARE:Lcom/discord/utilities/error/Error$Type;

    const/4 v4, 0x0

    const/4 v5, 0x4

    aput v5, v0, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    const/4 v0, 0x5

    const/16 v4, 0xa

    :try_start_4
    sget-object v5, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v6, Lcom/discord/utilities/error/Error$Type;->RATE_LIMITED:Lcom/discord/utilities/error/Error$Type;

    aput v0, v5, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    const/4 v5, 0x6

    :try_start_5
    sget-object v6, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v7, Lcom/discord/utilities/error/Error$Type;->FORBIDDEN_DISCORD:Lcom/discord/utilities/error/Error$Type;

    aput v5, v6, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    const/4 v1, 0x7

    :try_start_6
    sget-object v6, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v7, Lcom/discord/utilities/error/Error$Type;->DISCORD_REQUEST_ERROR:Lcom/discord/utilities/error/Error$Type;

    aput v1, v6, v3
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    const/16 v3, 0x8

    :try_start_7
    sget-object v6, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v7, Lcom/discord/utilities/error/Error$Type;->DISCORD_BAD_REQUEST:Lcom/discord/utilities/error/Error$Type;

    aput v3, v6, v0
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    const/16 v0, 0xb

    :try_start_8
    sget-object v6, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v7, Lcom/discord/utilities/error/Error$Type;->NETWORK:Lcom/discord/utilities/error/Error$Type;

    aput v2, v6, v0
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    const/16 v2, 0xc

    :try_start_9
    sget-object v6, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v7, Lcom/discord/utilities/error/Error$Type;->SSL:Lcom/discord/utilities/error/Error$Type;

    aput v4, v6, v2
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    const/16 v4, 0xd

    :try_start_a
    sget-object v6, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v7, Lcom/discord/utilities/error/Error$Type;->TIMEOUT:Lcom/discord/utilities/error/Error$Type;

    aput v0, v6, v4
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v6, Lcom/discord/utilities/error/Error$Type;->REQUEST_TOO_LARGE:Lcom/discord/utilities/error/Error$Type;

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v1, Lcom/discord/utilities/error/Error$Type;->UNAUTHORIZED:Lcom/discord/utilities/error/Error$Type;

    aput v4, v0, v3
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/discord/utilities/error/Error$1;->$SwitchMap$com$discord$utilities$error$Error$Type:[I

    sget-object v1, Lcom/discord/utilities/error/Error$Type;->INTERNAL_SERVER_ERROR:Lcom/discord/utilities/error/Error$Type;

    const/16 v1, 0xe

    aput v1, v0, v5
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    return-void
.end method
