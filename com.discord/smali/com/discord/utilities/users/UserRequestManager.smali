.class public final Lcom/discord/utilities/users/UserRequestManager;
.super Ljava/lang/Object;
.source "UserRequestManager.kt"


# instance fields
.field private final onFlush:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final userRequests:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lrx/Subscription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onFlush"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/users/UserRequestManager;->onFlush:Lkotlin/jvm/functions/Function1;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/users/UserRequestManager;->userRequests:Ljava/util/HashMap;

    return-void
.end method

.method public static final synthetic access$getOnFlush$p(Lcom/discord/utilities/users/UserRequestManager;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/users/UserRequestManager;->onFlush:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$onRequestEnded(Lcom/discord/utilities/users/UserRequestManager;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/users/UserRequestManager;->onRequestEnded(J)V

    return-void
.end method

.method public static final synthetic access$onRequestStarted(Lcom/discord/utilities/users/UserRequestManager;JLrx/Subscription;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/users/UserRequestManager;->onRequestStarted(JLrx/Subscription;)V

    return-void
.end method

.method private final declared-synchronized onRequestEnded(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/users/UserRequestManager;->userRequests:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Subscription;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lrx/Subscription;->unsubscribe()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized onRequestStarted(JLrx/Subscription;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/users/UserRequestManager;->userRequests:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final declared-synchronized requestUser(J)V
    .locals 17

    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    monitor-enter p0

    :try_start_0
    iget-object v0, v1, Lcom/discord/utilities/users/UserRequestManager;->userRequests:Ljava/util/HashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/discord/utilities/rest/RestAPI;->userGet(J)Lrx/Observable;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v4

    invoke-virtual {v0, v4}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v4, Lcom/discord/utilities/users/UserRequestManager$requestUser$1;

    invoke-direct {v4, v1, v2, v3}, Lcom/discord/utilities/users/UserRequestManager$requestUser$1;-><init>(Lcom/discord/utilities/users/UserRequestManager;J)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lg0/k/a;->a:Lg0/k/a$b;

    new-instance v6, Lg0/k/a$a;

    invoke-direct {v6, v4}, Lg0/k/a$a;-><init>(Lrx/functions/Action0;)V

    new-instance v7, Lg0/l/e/a;

    invoke-direct {v7, v5, v6, v4}, Lg0/l/e/a;-><init>(Lrx/functions/Action1;Lrx/functions/Action1;Lrx/functions/Action0;)V

    new-instance v4, Lg0/l/a/n;

    invoke-direct {v4, v0, v7}, Lg0/l/a/n;-><init>(Lrx/Observable;Lg0/g;)V

    invoke-static {v4}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v8

    const-string v0, "RestAPI\n        .api\n   \u2026 onRequestEnded(userId) }"

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const/4 v10, 0x0

    new-instance v14, Lcom/discord/utilities/users/UserRequestManager$requestUser$2;

    invoke-direct {v14, v1}, Lcom/discord/utilities/users/UserRequestManager$requestUser$2;-><init>(Lcom/discord/utilities/users/UserRequestManager;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v11, Lcom/discord/utilities/users/UserRequestManager$requestUser$3;

    invoke-direct {v11, v1, v2, v3}, Lcom/discord/utilities/users/UserRequestManager$requestUser$3;-><init>(Lcom/discord/utilities/users/UserRequestManager;J)V

    const/16 v15, 0x1a

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized requestUsers(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "userIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/utilities/users/UserRequestManager;->requestUser(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
