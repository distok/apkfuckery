.class public final synthetic Lcom/discord/utilities/billing/GooglePlayInAppSkus$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->values()[Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v0, 0x3

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/utilities/billing/GooglePlayInAppSkus$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v2, 0x2

    aput v2, v1, v3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    aput v0, v1, v2

    invoke-static {}, Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;->values()[Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;

    const/4 v0, 0x4

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;->GOOGLE:Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;

    aput v3, v0, v2

    return-void
.end method
