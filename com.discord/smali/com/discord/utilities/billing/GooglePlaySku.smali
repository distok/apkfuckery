.class public final enum Lcom/discord/utilities/billing/GooglePlaySku;
.super Ljava/lang/Enum;
.source "GooglePlaySku.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/billing/GooglePlaySku$Section;,
        Lcom/discord/utilities/billing/GooglePlaySku$Type;,
        Lcom/discord/utilities/billing/GooglePlaySku$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/billing/GooglePlaySku;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/billing/GooglePlaySku;

.field private static final ALL_SKU_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

.field public static final enum PREMIUM_GUILD_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_GUILD_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_1_PREMIUM_GUILD_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_1_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_10_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_13_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_28_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_3_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_5_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field public static final enum PREMIUM_TIER_2_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

.field private static final skusBySkuName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/billing/GooglePlaySku;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final iconDrawableResId:I

.field private final interval:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

.field private final premiumSubscriptionCount:I

.field private final skuName:Ljava/lang/String;

.field private final type:Lcom/discord/utilities/billing/GooglePlaySku$Type;

.field private final upgrade:Lcom/discord/utilities/billing/GooglePlaySku;


# direct methods
.method public static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x13

    new-array v1, v0, [Lcom/discord/utilities/billing/GooglePlaySku;

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    sget-object v14, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    sget-object v15, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->YEARLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    const-string v3, "PREMIUM_TIER_2_YEARLY"

    const/4 v4, 0x0

    const-string v5, "premium_tier_2_yearly"

    const v6, 0x7f0803e0

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/16 v11, 0x8

    const/4 v12, 0x0

    move-object v2, v13

    move-object v7, v14

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v16, 0x0

    aput-object v13, v1, v16

    new-instance v17, Lcom/discord/utilities/billing/GooglePlaySku;

    sget-object v18, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->MONTHLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    const-string v3, "PREMIUM_TIER_2_MONTHLY"

    const/4 v4, 0x1

    const-string v5, "premium_tier_2_monthly"

    move-object/from16 v2, v17

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v17, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x1

    aput-object v17, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    sget-object v14, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const-string v3, "PREMIUM_TIER_1_YEARLY"

    const/4 v4, 0x2

    const-string v5, "premium_tier_1_yearly"

    const v6, 0x7f0803de

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v13

    move-object v7, v14

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_1_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x2

    aput-object v13, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_1_MONTHLY"

    const/4 v4, 0x3

    const-string v5, "premium_tier_1_monthly"

    move-object v2, v11

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x3

    aput-object v11, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    sget-object v14, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY"

    const/4 v4, 0x4

    const-string v5, "premium_tier_2_premium_guild_1_yearly"

    const v6, 0x7f0803d5

    const/4 v8, 0x0

    const/4 v9, 0x3

    const/16 v11, 0x8

    move-object v2, v13

    move-object v7, v14

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x4

    aput-object v13, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_1_MONTHLY"

    const/4 v4, 0x5

    const-string v5, "premium_tier_2_premium_guild_1_monthly"

    move-object v2, v11

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x5

    aput-object v11, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY"

    const/4 v4, 0x6

    const-string v5, "premium_tier_2_premium_guild_2_yearly"

    const v6, 0x7f0803d8

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/16 v11, 0x8

    move-object v2, v13

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x6

    aput-object v13, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_2_MONTHLY"

    const/4 v4, 0x7

    const-string v5, "premium_tier_2_premium_guild_2_monthly"

    move-object v2, v11

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v2, 0x7

    aput-object v11, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY"

    const/16 v4, 0x8

    const-string v5, "premium_tier_2_premium_guild_3_yearly"

    const v6, 0x7f0803da

    const/4 v8, 0x0

    const/4 v9, 0x5

    const/16 v11, 0x8

    move-object v2, v13

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0x8

    aput-object v13, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_3_MONTHLY"

    const/16 v4, 0x9

    const-string v5, "premium_tier_2_premium_guild_3_monthly"

    move-object v2, v11

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_3_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0x9

    aput-object v11, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY"

    const/16 v4, 0xa

    const-string v5, "premium_tier_2_premium_guild_5_yearly"

    const v6, 0x7f0803db

    const/4 v8, 0x0

    const/4 v9, 0x7

    const/16 v11, 0x8

    move-object v2, v13

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0xa

    aput-object v13, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_5_MONTHLY"

    const/16 v4, 0xb

    const-string v5, "premium_tier_2_premium_guild_5_monthly"

    move-object v2, v11

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_5_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0xb

    aput-object v11, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_10_MONTHLY"

    const/16 v4, 0xc

    const-string v5, "premium_tier_2_premium_guild_10_monthly"

    const v6, 0x7f0803d6

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/16 v11, 0x8

    move-object v2, v13

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_10_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0xc

    aput-object v13, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_13_MONTHLY"

    const/16 v4, 0xd

    const-string v5, "premium_tier_2_premium_guild_13_monthly"

    const v6, 0x7f0803d7

    const/16 v9, 0xf

    move-object v2, v13

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_13_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0xd

    aput-object v13, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_2_PREMIUM_GUILD_28_MONTHLY"

    const/16 v4, 0xe

    const-string v5, "premium_tier_2_premium_guild_28_monthly"

    const v6, 0x7f0803d9

    const/16 v9, 0x1e

    move-object v2, v13

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_PREMIUM_GUILD_28_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0xe

    aput-object v13, v1, v2

    new-instance v13, Lcom/discord/utilities/billing/GooglePlaySku;

    sget-object v14, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const-string v3, "PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY"

    const/16 v4, 0xf

    const-string v5, "premium_tier_1_premium_guild_1_yearly"

    const v6, 0x7f0803df

    const/4 v9, 0x1

    move-object v2, v13

    move-object v7, v14

    move-object v10, v15

    invoke-direct/range {v2 .. v12}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v13, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0xf

    aput-object v13, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_TIER_1_PREMIUM_GUILD_1_MONTHLY"

    const/16 v4, 0x10

    const-string v5, "premium_tier_1_premium_guild_1_monthly"

    move-object v2, v11

    move-object v8, v13

    move-object/from16 v10, v18

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_1_PREMIUM_GUILD_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v12, 0x10

    aput-object v11, v1, v12

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    sget-object v13, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const-string v3, "PREMIUM_GUILD_1_MONTHLY"

    const/16 v4, 0x11

    const-string v5, "premium_guild_1_monthly"

    const v6, 0x7f0803dc

    move-object v2, v11

    move-object v7, v13

    move-object/from16 v8, v17

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_GUILD_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0x11

    aput-object v11, v1, v2

    new-instance v11, Lcom/discord/utilities/billing/GooglePlaySku;

    const-string v3, "PREMIUM_GUILD_2_MONTHLY"

    const/16 v4, 0x12

    const-string v5, "premium_guild_2_monthly"

    const v6, 0x7f0803dd

    const/4 v9, 0x2

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    sput-object v11, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_GUILD_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    const/16 v2, 0x12

    aput-object v11, v1, v2

    sput-object v1, Lcom/discord/utilities/billing/GooglePlaySku;->$VALUES:[Lcom/discord/utilities/billing/GooglePlaySku;

    new-instance v1, Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v1, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v1, v4

    iget-object v5, v5, Lcom/discord/utilities/billing/GooglePlaySku;->skuName:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    sput-object v2, Lcom/discord/utilities/billing/GooglePlaySku;->ALL_SKU_NAMES:Ljava/util/List;

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v1

    invoke-static {v0}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v0

    if-ge v0, v12, :cond_1

    goto :goto_1

    :cond_1
    move v12, v0

    :goto_1
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v12}, Ljava/util/LinkedHashMap;-><init>(I)V

    array-length v2, v1

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    iget-object v5, v4, Lcom/discord/utilities/billing/GooglePlaySku;->skuName:Ljava/lang/String;

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    sput-object v0, Lcom/discord/utilities/billing/GooglePlaySku;->skusBySkuName:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/discord/utilities/billing/GooglePlaySku$Type;",
            "Lcom/discord/utilities/billing/GooglePlaySku;",
            "I",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/utilities/billing/GooglePlaySku;->skuName:Ljava/lang/String;

    iput p4, p0, Lcom/discord/utilities/billing/GooglePlaySku;->iconDrawableResId:I

    iput-object p5, p0, Lcom/discord/utilities/billing/GooglePlaySku;->type:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    iput-object p6, p0, Lcom/discord/utilities/billing/GooglePlaySku;->upgrade:Lcom/discord/utilities/billing/GooglePlaySku;

    iput p7, p0, Lcom/discord/utilities/billing/GooglePlaySku;->premiumSubscriptionCount:I

    iput-object p8, p0, Lcom/discord/utilities/billing/GooglePlaySku;->interval:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p9, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object/from16 v7, p6

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/discord/utilities/billing/GooglePlaySku;-><init>(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)V

    return-void
.end method

.method public static final synthetic access$getALL_SKU_NAMES$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku;->ALL_SKU_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getSkusBySkuName$cp()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku;->skusBySkuName:Ljava/util/Map;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    const-class v0, Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/billing/GooglePlaySku;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku;->$VALUES:[Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v0}, [Lcom/discord/utilities/billing/GooglePlaySku;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/billing/GooglePlaySku;

    return-object v0
.end method


# virtual methods
.method public final getIconDrawableResId()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/billing/GooglePlaySku;->iconDrawableResId:I

    return v0
.end method

.method public final getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlaySku;->interval:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    return-object v0
.end method

.method public final getPremiumSubscriptionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/billing/GooglePlaySku;->premiumSubscriptionCount:I

    return v0
.end method

.method public final getSkuName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlaySku;->skuName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlaySku;->type:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    return-object v0
.end method

.method public final getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlaySku;->upgrade:Lcom/discord/utilities/billing/GooglePlaySku;

    return-object v0
.end method
