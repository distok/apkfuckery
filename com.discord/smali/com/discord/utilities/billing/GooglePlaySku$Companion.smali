.class public final Lcom/discord/utilities/billing/GooglePlaySku$Companion;
.super Ljava/lang/Object;
.source "GooglePlaySku.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/billing/GooglePlaySku;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSkuName(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    const-string v0, "skuName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->access$getSkusBySkuName$cp()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/billing/GooglePlaySku;

    return-object p1
.end method

.method public final getALL_SKU_NAMES()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->access$getALL_SKU_NAMES$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getBorderResource(Lcom/discord/utilities/billing/GooglePlaySku$Type;)I
    .locals 1
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    const-string v0, "skuType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const p1, 0x7f080127

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const p1, 0x7f080129

    goto :goto_0

    :cond_2
    const p1, 0x7f08012a

    goto :goto_0

    :cond_3
    const p1, 0x7f080121

    goto :goto_0

    :cond_4
    const p1, 0x7f080123

    :goto_0
    return p1
.end method

.method public final getDowngrade(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 5

    const-string v0, "skuName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x0

    const/16 v3, 0x13

    if-ge v1, v3, :cond_2

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-static {v2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-object v2
.end method
