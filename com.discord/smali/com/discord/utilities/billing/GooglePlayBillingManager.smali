.class public final Lcom/discord/utilities/billing/GooglePlayBillingManager;
.super Ljava/lang/Object;
.source "GooglePlayBillingManager.kt"

# interfaces
.implements Lf/e/a/a/e;
.implements Lf/e/a/a/b;
.implements Lf/e/a/a/g;
.implements Lf/e/a/a/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;,
        Lcom/discord/utilities/billing/GooglePlayBillingManager$GooglePlayBillingManagerLifecycleListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_BACKOFF_TIME_MS:J = 0x3e8L

.field public static final INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

.field public static final PLAY_STORE_SUBSCRIPTION_DEEPLINK_URL:Ljava/lang/String; = "https://play.google.com/store/account/subscriptions?sku=%s&package=%s"

.field public static final PLAY_STORE_SUBSCRIPTION_URL:Ljava/lang/String; = "https://play.google.com/store/account/subscriptions"

.field private static backoffTimeMs:Ljava/util/concurrent/atomic/AtomicLong;

.field private static billingClient:Lcom/android/billingclient/api/BillingClient;

.field private static inAppSkusToConsume:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;",
            ">;"
        }
    .end annotation
.end field

.field private static isAuthenticated:Z

.field private static isReconnecting:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-direct {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;-><init>()V

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x3e8

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->backoffTimeMs:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isReconnecting:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->inAppSkusToConsume:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getBackoffTimeMs$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 0

    sget-object p0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->backoffTimeMs:Ljava/util/concurrent/atomic/AtomicLong;

    return-object p0
.end method

.method public static final synthetic access$getBillingClient$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Lcom/android/billingclient/api/BillingClient;
    .locals 0

    sget-object p0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "billingClient"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$isAuthenticated$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Z
    .locals 0

    sget-boolean p0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isAuthenticated:Z

    return p0
.end method

.method public static final synthetic access$isReconnecting$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    sget-object p0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isReconnecting:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method public static final synthetic access$setAuthenticated$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;Z)V
    .locals 0

    sput-boolean p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isAuthenticated:Z

    return-void
.end method

.method public static final synthetic access$setBackoffTimeMs$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;Ljava/util/concurrent/atomic/AtomicLong;)V
    .locals 0

    sput-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->backoffTimeMs:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public static final synthetic access$setBillingClient$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;Lcom/android/billingclient/api/BillingClient;)V
    .locals 0

    sput-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    return-void
.end method

.method public static final synthetic access$setReconnecting$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    sput-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isReconnecting:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private final handleConsumeEnd(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->inAppSkusToConsume:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getType()Lcom/discord/utilities/billing/InAppSkuType;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreStickers;->fetchOwnedStickerPacks()V

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getSkuId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/discord/stores/StoreStickers;->completePurchasingStickerPack(J)V

    :cond_3
    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->inAppSkusToConsume:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void
.end method

.method private final handleConsumeFailure(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->inAppSkusToConsume:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getType()Lcom/discord/utilities/billing/InAppSkuType;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->trackPaymentFlowFailed(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private final handleConsumeStart(Lcom/android/billingclient/api/Purchase;Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;)V
    .locals 2

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->inAppSkusToConsume:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->a()Ljava/lang/String;

    move-result-object p1

    const-string v1, "purchase.purchaseToken"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getType()Lcom/discord/utilities/billing/InAppSkuType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getSkuId()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreStickers;->startPurchasingStickerPack(J)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleConsumeSuccess(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->inAppSkusToConsume:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getType()Lcom/discord/utilities/billing/InAppSkuType;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreGooglePlayPurchases;->trackPaymentFlowCompleted(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;->getSkuId()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object p1

    invoke-virtual {p1, v1, v2}, Lcom/discord/stores/StoreStickers;->showStickerPackActivatedDialog(J)V

    :cond_3
    :goto_1
    return-void
.end method

.method private final queryInAppSkuDetails()V
    .locals 5

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const/4 v1, 0x0

    const-string v2, "billingClient"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->getSkus()Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    invoke-virtual {v4}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v3, Lf/e/a/a/f;

    invoke-direct {v3}, Lf/e/a/a/f;-><init>()V

    const-string v4, "inapp"

    iput-object v4, v3, Lf/e/a/a/f;->a:Ljava/lang/String;

    iput-object v0, v3, Lf/e/a/a/f;->b:Ljava/util/List;

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3, p0}, Lcom/android/billingclient/api/BillingClient;->f(Lf/e/a/a/f;Lf/e/a/a/g;)V

    return-void

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final reconnect()V
    .locals 12

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isReconnecting:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->backoffTimeMs:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v3

    const-string v2, "Observable\n          .ti\u2026s, TimeUnit.MILLISECONDS)"

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v4, Lcom/discord/utilities/billing/GooglePlayBillingManager;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/utilities/billing/GooglePlayBillingManager$reconnect$1;

    invoke-direct {v9, v0, v1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$reconnect$1;-><init>(J)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final consumePurchase(Lcom/android/billingclient/api/Purchase;Lcom/discord/utilities/billing/InAppSkuType;Ljava/lang/Long;)V
    .locals 4

    const-string v0, "purchase"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inAppSkuType"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const-string v1, "billingClient"

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v3, Lf/e/a/a/c;

    invoke-direct {v3, v2}, Lf/e/a/a/c;-><init>(Lf/e/a/a/r;)V

    iput-object v0, v3, Lf/e/a/a/c;->a:Ljava/lang/String;

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3, p0}, Lcom/android/billingclient/api/BillingClient;->a(Lf/e/a/a/c;Lf/e/a/a/d;)V

    new-instance v0, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;

    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "purchase.sku"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2, p3, v1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;-><init>(Lcom/discord/utilities/billing/InAppSkuType;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->handleConsumeStart(Lcom/android/billingclient/api/Purchase;Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;)V

    return-void

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Purchase token must be set"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public final init(Landroid/app/Application;)V
    .locals 9

    const-string v0, "application"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    new-instance v2, Lf/e/a/a/a;

    invoke-direct {v2, v1, v0, p0}, Lf/e/a/a/a;-><init>(ZLandroid/content/Context;Lf/e/a/a/e;)V

    const-string v0, "BillingClient.newBuilder\u2026chases()\n        .build()"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v2, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    new-instance v0, Lcom/discord/utilities/billing/GooglePlayBillingManager$GooglePlayBillingManagerLifecycleListener;

    invoke-direct {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager$GooglePlayBillingManagerLifecycleListener;-><init>()V

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreAuthentication;->getIsAuthed$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/utilities/billing/GooglePlayBillingManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/discord/utilities/billing/GooglePlayBillingManager$init$1;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager$init$1;

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Please provide a valid Context."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final launchBillingFlow(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)I
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "params"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const/4 v1, 0x0

    const-string v2, "billingClient"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Lcom/android/billingclient/api/BillingClient;->d(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)Lcom/android/billingclient/api/BillingResult;

    move-result-object p1

    const-string p2, "billingClient.launchBillingFlow(activity, params)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget p1, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    return p1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final onActivityCreated()V
    .locals 3

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const/4 v1, 0x0

    const-string v2, "billingClient"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/billingclient/api/BillingClient;->g(Lf/e/a/a/b;)V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final onActivityDestroyed()V
    .locals 3

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const/4 v1, 0x0

    const-string v2, "billingClient"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->b()V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onBillingServiceDisconnected()V
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->isReconnecting:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->reconnect()V

    :cond_0
    return-void
.end method

.method public onBillingSetupFinished(Lcom/android/billingclient/api/BillingResult;)V
    .locals 2

    const-string v0, "billingResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget p1, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->querySkuDetails()V

    invoke-direct {p0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryInAppSkuDetails()V

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    sget-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->backoffTimeMs:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v0, 0x3e8

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    :cond_0
    return-void
.end method

.method public onConsumeResponse(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V
    .locals 7

    const-string v0, "billingResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchaseToken"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->handleConsumeSuccess(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->handleConsumeFailure(Ljava/lang/String;)V

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v0, "Failed to consume purchase. "

    const-string v2, "Billing Response Code: "

    invoke-static {v0, v2}, Lf/e/c/a/a;->L(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget p1, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "Purchase Token: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_0
    invoke-direct {p0, p2}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->handleConsumeEnd(Ljava/lang/String;)V

    return-void
.end method

.method public onPurchasesUpdated(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/billingclient/api/BillingResult;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)V"
        }
    .end annotation

    const-string v0, "billingResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreGooglePlayPurchases;->updatePendingDowngrade(Lcom/discord/stores/PendingDowngrade;)V

    :cond_0
    iget p1, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    const/4 v0, 0x1

    if-eqz p1, :cond_4

    if-eq p1, v0, :cond_3

    const/4 p2, 0x5

    if-eq p1, p2, :cond_2

    const/4 p2, 0x7

    if-eq p1, p2, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, "onPurchasesUpdated: The user already owns this item"

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    const-string v1, "onPurchasesUpdated: Google Play doesn\'t recognize this app config. Verify the SKU product ID and the signed APK you are using."

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_1

    :cond_3
    const-string p1, "onPurchasesUpdated: User canceled the purchase"

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    if-eqz p2, :cond_6

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :cond_6
    :goto_0
    if-eqz v0, :cond_7

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->downgradePurchase()V

    :cond_7
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreGooglePlayPurchases;->processPurchases(Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public onSkuDetailsResponse(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/billingclient/api/BillingResult;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    const-string v0, "billingResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p1, Lcom/android/billingclient/api/BillingResult;->a:I

    iget-object p1, p1, Lcom/android/billingclient/api/BillingResult;->b:Ljava/lang/String;

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlaySkuDetails()Lcom/discord/stores/StoreGooglePlaySkuDetails;

    move-result-object p1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Lx/h/l;->d:Lx/h/l;

    :goto_0
    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreGooglePlaySkuDetails;->updateSkuDetails(Ljava/util/List;)V

    goto :goto_1

    :pswitch_1
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlaySkuDetails()Lcom/discord/stores/StoreGooglePlaySkuDetails;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlaySkuDetails;->handleError()V

    goto :goto_1

    :pswitch_2
    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlaySkuDetails()Lcom/discord/stores/StoreGooglePlaySkuDetails;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreGooglePlaySkuDetails;->handleError()V

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSkuDetailsResponse: "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final queryPurchases()V
    .locals 5

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const/4 v1, 0x0

    const-string v2, "billingClient"

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sget-object v3, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v3, :cond_4

    const-string/jumbo v4, "subs"

    invoke-virtual {v3, v4}, Lcom/android/billingclient/api/BillingClient;->e(Ljava/lang/String;)Lcom/android/billingclient/api/Purchase$a;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, v3, Lcom/android/billingclient/api/Purchase$a;->a:Ljava/util/List;

    if-eqz v3, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_1
    sget-object v3, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v3, :cond_3

    const-string v1, "inapp"

    invoke-virtual {v3, v1}, Lcom/android/billingclient/api/BillingClient;->e(Ljava/lang/String;)Lcom/android/billingclient/api/Purchase$a;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/android/billingclient/api/Purchase$a;->a:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v1

    invoke-static {v0}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreGooglePlayPurchases;->processPurchases(Ljava/util/List;)V

    return-void

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final querySkuDetails()V
    .locals 5

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    const/4 v1, 0x0

    const-string v2, "billingClient"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->getALL_SKU_NAMES()Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v0, Lf/e/a/a/f;

    invoke-direct {v0}, Lf/e/a/a/f;-><init>()V

    const-string/jumbo v4, "subs"

    iput-object v4, v0, Lf/e/a/a/f;->a:Ljava/lang/String;

    iput-object v3, v0, Lf/e/a/a/f;->b:Ljava/util/List;

    sget-object v3, Lcom/discord/utilities/billing/GooglePlayBillingManager;->billingClient:Lcom/android/billingclient/api/BillingClient;

    if-eqz v3, :cond_1

    invoke-virtual {v3, v0, p0}, Lcom/android/billingclient/api/BillingClient;->f(Lf/e/a/a/f;Lf/e/a/a/g;)V

    return-void

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
