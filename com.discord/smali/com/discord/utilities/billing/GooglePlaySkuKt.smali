.class public final Lcom/discord/utilities/billing/GooglePlaySkuKt;
.super Ljava/lang/Object;
.source "GooglePlaySku.kt"


# direct methods
.method public static final getSection(Lcom/discord/utilities/billing/GooglePlaySku;)Lcom/discord/utilities/billing/GooglePlaySku$Section;
    .locals 1

    const-string v0, "$this$getSection"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    sget-object p0, Lcom/discord/utilities/billing/GooglePlaySku$Section;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Section;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_1
    sget-object p0, Lcom/discord/utilities/billing/GooglePlaySku$Section;->PREMIUM_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Section;

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/discord/utilities/billing/GooglePlaySku$Section;->PREMIUM:Lcom/discord/utilities/billing/GooglePlaySku$Section;

    :goto_0
    return-object p0
.end method

.method public static final isBundledSku(Lcom/discord/utilities/billing/GooglePlaySku;)Z
    .locals 2

    const-string v0, "$this$isBundledSku"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object p0

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isTier1(Lcom/discord/utilities/billing/GooglePlaySku;)Z
    .locals 2

    const-string v0, "$this$isTier1"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object p0

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isTier2(Lcom/discord/utilities/billing/GooglePlaySku;)Z
    .locals 2

    const-string v0, "$this$isTier2"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object p0

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
