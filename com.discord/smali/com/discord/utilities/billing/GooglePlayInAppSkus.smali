.class public final Lcom/discord/utilities/billing/GooglePlayInAppSkus;
.super Ljava/lang/Object;
.source "GooglePlayInAppSku.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

.field private static final skus:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/billing/GooglePlayInAppSku;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    invoke-direct {v0}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;-><init>()V

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/utilities/billing/GooglePlayInAppSku;

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPack()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPackDiscounted()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->skus:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getSkusById()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/billing/GooglePlayInAppSku;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->skus:Ljava/util/List;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    const/16 v1, 0x10

    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    invoke-virtual {v3}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v2
.end method


# virtual methods
.method public final getInAppSku(Lcom/discord/models/domain/ModelSku;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/utilities/billing/GooglePlayInAppSku;
    .locals 8

    const-string v0, "sku"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "premiumTier"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku;->isStickerPack()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku;->getExternalSkuStrategies()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/discord/models/domain/ModelSubscription$PaymentGateway;->GOOGLE:Lcom/discord/models/domain/ModelSubscription$PaymentGateway;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription$PaymentGateway;->getIntRepresentation()Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->getType()Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    :goto_1
    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    const-string v3, "Tried to get In-App SKU for sticker pack not configured for IAP."

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return-object v1

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 p2, 0x1

    if-eq p1, p2, :cond_4

    if-ne p1, v0, :cond_3

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPackDiscounted()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object p1

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPack()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_5
    return-object v1
.end method

.method public final getInAppSku(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlayInAppSku;
    .locals 1

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->getSkusById()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    return-object p1
.end method

.method public final getSkus()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/billing/GooglePlayInAppSku;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->skus:Ljava/util/List;

    return-object v0
.end method

.method public final isInAppSku(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->getSkusById()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final populateSkuDetails(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    const-string v0, "skuDetails"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    sget-object v1, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    invoke-direct {v1}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->getSkusById()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v1, v0}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->setSkuDetails(Lcom/android/billingclient/api/SkuDetails;)V

    goto :goto_0

    :cond_1
    return-void
.end method
