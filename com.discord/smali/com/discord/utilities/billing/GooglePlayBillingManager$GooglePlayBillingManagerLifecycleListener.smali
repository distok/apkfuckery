.class public final Lcom/discord/utilities/billing/GooglePlayBillingManager$GooglePlayBillingManagerLifecycleListener;
.super Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;
.source "GooglePlayBillingManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/billing/GooglePlayBillingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GooglePlayBillingManagerLifecycleListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Lcom/discord/app/AppActivity;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;->onActivityCreated(Lcom/discord/app/AppActivity;Landroid/os/Bundle;)V

    sget-object p2, Lf/a/b/m;->g:Lf/a/b/m;

    sget-object p2, Lf/a/b/m;->f:Ljava/util/List;

    invoke-virtual {p1, p2}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-static {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->access$isAuthenticated$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->onActivityCreated()V

    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Lcom/discord/app/AppActivity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;->onActivityDestroyed(Lcom/discord/app/AppActivity;)V

    sget-object v0, Lf/a/b/m;->g:Lf/a/b/m;

    sget-object v0, Lf/a/b/m;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->onActivityDestroyed()V

    :cond_0
    return-void
.end method

.method public onActivityResumed(Lcom/discord/app/AppActivity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;->onActivityResumed(Lcom/discord/app/AppActivity;)V

    sget-object v0, Lf/a/b/m;->g:Lf/a/b/m;

    sget-object v0, Lf/a/b/m;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-static {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->access$isAuthenticated$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    :cond_0
    return-void
.end method
