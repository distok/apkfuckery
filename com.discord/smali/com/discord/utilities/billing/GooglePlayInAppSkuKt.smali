.class public final Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;
.super Ljava/lang/Object;
.source "GooglePlayInAppSku.kt"


# static fields
.field private static final inAppStickerPack:Lcom/discord/utilities/billing/GooglePlayInAppSku;

.field private static final inAppStickerPackDiscounted:Lcom/discord/utilities/billing/GooglePlayInAppSku;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    new-instance v6, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    sget-object v7, Lcom/discord/utilities/billing/InAppSkuType;->STICKER_PACK:Lcom/discord/utilities/billing/InAppSkuType;

    const-string/jumbo v1, "sticker_pack_299"

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, v7

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/billing/GooglePlayInAppSku;-><init>(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v6, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->inAppStickerPack:Lcom/discord/utilities/billing/GooglePlayInAppSku;

    new-instance v6, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    const-string/jumbo v1, "sticker_pack_199"

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/billing/GooglePlayInAppSku;-><init>(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v6, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->inAppStickerPackDiscounted:Lcom/discord/utilities/billing/GooglePlayInAppSku;

    return-void
.end method

.method public static final getInAppStickerPack()Lcom/discord/utilities/billing/GooglePlayInAppSku;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->inAppStickerPack:Lcom/discord/utilities/billing/GooglePlayInAppSku;

    return-object v0
.end method

.method public static final getInAppStickerPackDiscounted()Lcom/discord/utilities/billing/GooglePlayInAppSku;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->inAppStickerPackDiscounted:Lcom/discord/utilities/billing/GooglePlayInAppSku;

    return-object v0
.end method
