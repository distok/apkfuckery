.class public final synthetic Lcom/discord/utilities/billing/GooglePlaySku$Companion$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku$Type;->values()[Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const/4 v0, 0x5

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const/4 v2, 0x2

    aput v2, v1, v3

    sget-object v3, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    const/4 v3, 0x4

    const/4 v4, 0x3

    aput v4, v1, v3

    sget-object v5, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_2_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    aput v3, v1, v2

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    aput v0, v1, v4

    return-void
.end method
