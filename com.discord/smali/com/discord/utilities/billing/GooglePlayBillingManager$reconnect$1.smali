.class public final Lcom/discord/utilities/billing/GooglePlayBillingManager$reconnect$1;
.super Lx/m/c/k;
.source "GooglePlayBillingManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/billing/GooglePlayBillingManager;->reconnect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $currentBackoffTimeMs:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/utilities/billing/GooglePlayBillingManager$reconnect$1;->$currentBackoffTimeMs:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager$reconnect$1;->invoke(Ljava/lang/Long;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Long;)V
    .locals 5

    sget-object p1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-static {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->access$getBillingClient$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Lcom/android/billingclient/api/BillingClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingClient;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->access$getBillingClient$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Lcom/android/billingclient/api/BillingClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/billingclient/api/BillingClient;->g(Lf/e/a/a/b;)V

    invoke-static {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->access$getBackoffTimeMs$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/utilities/billing/GooglePlayBillingManager$reconnect$1;->$currentBackoffTimeMs:J

    const/4 v3, 0x2

    int-to-long v3, v3

    mul-long v1, v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    :cond_0
    invoke-static {p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->access$isReconnecting$p(Lcom/discord/utilities/billing/GooglePlayBillingManager;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method
