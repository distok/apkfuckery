.class public final Lcom/discord/utilities/billing/BillingUtils;
.super Ljava/lang/Object;
.source "BillingUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/billing/BillingUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/billing/BillingUtils;

    invoke-direct {v0}, Lcom/discord/utilities/billing/BillingUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/billing/BillingUtils;->INSTANCE:Lcom/discord/utilities/billing/BillingUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final verifyPurchase(Lcom/android/billingclient/api/Purchase;)V
    .locals 20

    move-object/from16 v0, p1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    sget-object v2, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    invoke-virtual/range {p1 .. p1}, Lcom/android/billingclient/api/Purchase;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "purchase.sku"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->isInAppSku(Ljava/lang/String;)Z

    move-result v2

    const/4 v10, 0x0

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/android/billingclient/api/Purchase;->b()Ljava/lang/String;

    move-result-object v2

    move-object v9, v2

    move-object v8, v10

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/billingclient/api/Purchase;->b()Ljava/lang/String;

    move-result-object v2

    move-object v8, v2

    move-object v9, v10

    :goto_0
    new-instance v2, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;

    invoke-virtual/range {p1 .. p1}, Lcom/android/billingclient/api/Purchase;->a()Ljava/lang/String;

    move-result-object v4

    const-string v3, "purchase.purchaseToken"

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/android/billingclient/api/Purchase;->c:Lorg/json/JSONObject;

    const-string v7, "packageName"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v3, "purchase.packageName"

    invoke-static {v7, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v2

    invoke-direct/range {v3 .. v9}, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreGooglePlayPurchases;->onVerificationStart()V

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/discord/utilities/rest/RestAPI;->verifyPurchaseToken(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v11

    const-class v12, Lcom/discord/utilities/billing/BillingUtils;

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v1, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;

    invoke-direct {v1, v0}, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;-><init>(Lcom/android/billingclient/api/Purchase;)V

    const/16 v16, 0x0

    new-instance v15, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$2;

    invoke-direct {v15, v0}, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$2;-><init>(Lcom/android/billingclient/api/Purchase;)V

    const/16 v18, 0x16

    const/16 v19, 0x0

    move-object/from16 v17, v1

    invoke-static/range {v11 .. v19}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final createPendingPurchaseMetadata(Ljava/lang/String;JLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSuccess"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onFailure"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;

    invoke-direct {v0, p2, p3, p1}, Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;-><init>(JLjava/lang/String;)V

    sget-object p1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/discord/utilities/rest/RestAPI;->createPurchaseMetadata(Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x1

    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/utilities/billing/BillingUtils;

    new-instance v7, Lcom/discord/utilities/billing/BillingUtils$createPendingPurchaseMetadata$1;

    invoke-direct {v7, p4}, Lcom/discord/utilities/billing/BillingUtils$createPendingPurchaseMetadata$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    new-instance v5, Lcom/discord/utilities/billing/BillingUtils$createPendingPurchaseMetadata$2;

    invoke-direct {v5, p5}, Lcom/discord/utilities/billing/BillingUtils$createPendingPurchaseMetadata$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x16

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final microToMinor(J)J
    .locals 2

    const-wide/16 v0, 0x2710

    div-long/2addr p1, v0

    return-wide p1
.end method

.method public final verifyPurchases(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/billingclient/api/Purchase;

    iget-object v1, v0, Lcom/android/billingclient/api/Purchase;->c:Lorg/json/JSONObject;

    const-string v2, "purchaseState"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x2

    :goto_1
    if-ne v1, v3, :cond_0

    invoke-virtual {v0}, Lcom/android/billingclient/api/Purchase;->c()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/discord/utilities/billing/BillingUtils;->INSTANCE:Lcom/discord/utilities/billing/BillingUtils;

    invoke-direct {v1, v0}, Lcom/discord/utilities/billing/BillingUtils;->verifyPurchase(Lcom/android/billingclient/api/Purchase;)V

    goto :goto_0

    :cond_2
    return-void
.end method
