.class public final Lcom/discord/utilities/billing/GooglePlayInAppSku;
.super Ljava/lang/Object;
.source "GooglePlayInAppSku.kt"


# instance fields
.field private final paymentGatewaySkuId:Ljava/lang/String;

.field private skuDetails:Lcom/android/billingclient/api/SkuDetails;

.field private type:Lcom/discord/utilities/billing/InAppSkuType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;)V
    .locals 1

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    iput-object p3, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/billing/GooglePlayInAppSku;-><init>(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/billing/GooglePlayInAppSku;Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;ILjava/lang/Object;)Lcom/discord/utilities/billing/GooglePlayInAppSku;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->copy(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;)Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/discord/utilities/billing/InAppSkuType;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    return-object v0
.end method

.method public final component3()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;)Lcom/discord/utilities/billing/GooglePlayInAppSku;
    .locals 1

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/utilities/billing/GooglePlayInAppSku;-><init>(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/billing/GooglePlayInAppSku;

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    iget-object v1, p1, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    iget-object p1, p1, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPaymentGatewaySkuId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSkuDetails()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final getType()Lcom/discord/utilities/billing/InAppSkuType;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final setSkuDetails(Lcom/android/billingclient/api/SkuDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-void
.end method

.method public final setType(Lcom/discord/utilities/billing/InAppSkuType;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "GooglePlayInAppSku(paymentGatewaySkuId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->paymentGatewaySkuId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->type:Lcom/discord/utilities/billing/InAppSkuType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skuDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/billing/GooglePlayInAppSku;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
