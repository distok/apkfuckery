.class public final enum Lcom/discord/utilities/billing/InAppSkuType;
.super Ljava/lang/Enum;
.source "GooglePlayInAppSku.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/billing/InAppSkuType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/billing/InAppSkuType;

.field public static final enum STICKER_PACK:Lcom/discord/utilities/billing/InAppSkuType;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/discord/utilities/billing/InAppSkuType;

    new-instance v1, Lcom/discord/utilities/billing/InAppSkuType;

    const-string v2, "STICKER_PACK"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/billing/InAppSkuType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/billing/InAppSkuType;->STICKER_PACK:Lcom/discord/utilities/billing/InAppSkuType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/utilities/billing/InAppSkuType;->$VALUES:[Lcom/discord/utilities/billing/InAppSkuType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/billing/InAppSkuType;
    .locals 1

    const-class v0, Lcom/discord/utilities/billing/InAppSkuType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/billing/InAppSkuType;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/billing/InAppSkuType;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/InAppSkuType;->$VALUES:[Lcom/discord/utilities/billing/InAppSkuType;

    invoke-virtual {v0}, [Lcom/discord/utilities/billing/InAppSkuType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/billing/InAppSkuType;

    return-object v0
.end method
