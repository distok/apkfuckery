.class public final Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;
.super Lx/m/c/k;
.source "BillingUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/billing/BillingUtils;->verifyPurchase(Lcom/android/billingclient/api/Purchase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $purchase:Lcom/android/billingclient/api/Purchase;


# direct methods
.method public constructor <init>(Lcom/android/billingclient/api/Purchase;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;->$purchase:Lcom/android/billingclient/api/Purchase;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;->invoke(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;)V
    .locals 5

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;->$purchase:Lcom/android/billingclient/api/Purchase;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreGooglePlayPurchases;->onVerificationSuccess(Lcom/android/billingclient/api/Purchase;)V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    iget-object v1, p0, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;->$purchase:Lcom/android/billingclient/api/Purchase;

    invoke-virtual {v1}, Lcom/android/billingclient/api/Purchase;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "purchase.sku"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->getInAppSku(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    iget-object v2, p0, Lcom/discord/utilities/billing/BillingUtils$verifyPurchase$1;->$purchase:Lcom/android/billingclient/api/Purchase;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getType()Lcom/discord/utilities/billing/InAppSkuType;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;->getVerifiedSkuId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0, p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->consumePurchase(Lcom/android/billingclient/api/Purchase;Lcom/discord/utilities/billing/InAppSkuType;Ljava/lang/Long;)V

    :cond_1
    return-void
.end method
