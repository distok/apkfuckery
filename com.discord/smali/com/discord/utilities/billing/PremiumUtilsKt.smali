.class public final Lcom/discord/utilities/billing/PremiumUtilsKt;
.super Ljava/lang/Object;
.source "PremiumUtils.kt"


# static fields
.field private static final ALL_PAID_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field private static final GRANDFATHERED_MONTHLY_END_DATE:Ljava/util/Date;

.field private static final GRANDFATHERED_YEARLY_END_DATE:Ljava/util/Date;

.field public static final MAX_ACCOUNT_HOLD_DAYS:I = 0x1e

.field public static final MAX_INVOICE_PAYMENT_DAYS:I = 0x3

.field private static final NONE_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field private static final TIER_1_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field private static final TIER_2_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field private static final UPGRADE_ELIGIBILITY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0x7e4

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    const-string v1, "Calendar.getInstance().apply { set(2020, 2, 1) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    const-string v1, "Calendar.getInstance().a\u2026 { set(2020, 2, 1) }.time"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->GRANDFATHERED_MONTHLY_END_DATE:Ljava/util/Date;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0x7e5

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/util/Calendar;->set(III)V

    const-string v1, "Calendar.getInstance().apply { set(2021, 0, 1) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    const-string v1, "Calendar.getInstance().a\u2026 { set(2021, 0, 1) }.time"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->GRANDFATHERED_YEARLY_END_DATE:Ljava/util/Date;

    new-array v0, v2, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->NONE_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v1, v0, v4

    sget-object v5, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->NONE_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v5, v0, v3

    invoke-static {v0}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->NONE_PLANS:Ljava/util/Set;

    new-array v6, v2, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v7, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v7, v6, v4

    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v8, v6, v3

    invoke-static {v6}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v6

    sput-object v6, Lcom/discord/utilities/billing/PremiumUtilsKt;->TIER_1_PLANS:Ljava/util/Set;

    new-array v9, v2, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v10, v9, v4

    sget-object v11, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v11, v9, v3

    invoke-static {v9}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v9

    sput-object v9, Lcom/discord/utilities/billing/PremiumUtilsKt;->TIER_2_PLANS:Ljava/util/Set;

    invoke-static {v6, v9}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v6

    sput-object v6, Lcom/discord/utilities/billing/PremiumUtilsKt;->ALL_PAID_PLANS:Ljava/util/Set;

    const/16 v12, 0x9

    new-array v12, v12, [Lkotlin/Pair;

    invoke-static {v0, v6}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    new-instance v13, Lkotlin/Pair;

    const/4 v14, 0x0

    invoke-direct {v13, v14, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v13, v12, v4

    invoke-static {v5}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, v6}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v12, v3

    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, v5, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v0, v12, v2

    const/4 v0, 0x3

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-static {v8}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-static {v2, v9}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v12, v0

    const/4 v0, 0x4

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-static {v11}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v12, v0

    const/4 v0, 0x5

    invoke-static {v8}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v1, v9}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v7, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v12, v0

    const/4 v0, 0x6

    invoke-static {v11}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v8, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v12, v0

    const/4 v0, 0x7

    invoke-static {v11}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v10, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v12, v0

    const/16 v0, 0x8

    sget-object v1, Lx/h/n;->d:Lx/h/n;

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v11, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v12, v0

    invoke-static {v12}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->UPGRADE_ELIGIBILITY:Ljava/util/Map;

    return-void
.end method

.method public static final getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p0, p0

    const/16 v0, 0x64

    int-to-float v0, v0

    div-float/2addr p0, v0

    new-instance v0, Lcom/discord/utilities/locale/LocaleManager;

    invoke-direct {v0}, Lcom/discord/utilities/locale/LocaleManager;-><init>()V

    invoke-virtual {v0, p1}, Lcom/discord/utilities/locale/LocaleManager;->getPrimaryLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object p1

    invoke-static {p1}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p1

    const-string v0, "USD"

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "numberFormat.format(priceUsdDollars)"

    invoke-static {p0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getGRANDFATHERED_MONTHLY_END_DATE()Ljava/util/Date;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->GRANDFATHERED_MONTHLY_END_DATE:Ljava/util/Date;

    return-object v0
.end method

.method public static final getGRANDFATHERED_YEARLY_END_DATE()Ljava/util/Date;
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->GRANDFATHERED_YEARLY_END_DATE:Ljava/util/Date;

    return-object v0
.end method

.method public static final getPaymentSourceIcon(Lcom/discord/models/domain/ModelPaymentSource;)I
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    const-string v0, "paymentSource"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p0, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourcePaypal;

    const v1, 0x7f0802c9

    if-eqz v0, :cond_0

    const v1, 0x7f0802cb

    goto :goto_2

    :cond_0
    instance-of v0, p0, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourceCard;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourceCard;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourceCard;->getBrand()Ljava/lang/String;

    move-result-object p0

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "(this as java.lang.String).toLowerCase()"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_2

    :sswitch_0
    const-string v0, "master-card"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_1

    :sswitch_1
    const-string v0, "discover"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const v1, 0x7f0802c8

    goto :goto_2

    :sswitch_2
    const-string v0, "american-express"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "visa"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const v1, 0x7f0802cc

    goto :goto_2

    :sswitch_4
    const-string v0, "amex"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    :goto_0
    const v1, 0x7f0802c7

    goto :goto_2

    :sswitch_5
    const-string v0, "mastercard"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    :goto_1
    const v1, 0x7f0802ca

    :cond_1
    :goto_2
    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79845b8e -> :sswitch_5
        0x2dbddf -> :sswitch_4
        0x373c41 -> :sswitch_3
        0x3a3b6c3 -> :sswitch_2
        0x104877e9 -> :sswitch_1
        0x46009f9b -> :sswitch_0
    .end sparse-switch
.end method

.method public static final getUPGRADE_ELIGIBILITY()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;>;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/billing/PremiumUtilsKt;->UPGRADE_ELIGIBILITY:Ljava/util/Map;

    return-object v0
.end method
