.class public final Lcom/discord/utilities/games/GameDetectionService;
.super Landroid/app/Service;
.source "GameDetectionService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/games/GameDetectionService$Companion;
    }
.end annotation


# static fields
.field private static final BACKGROUNDED_DELAY:J = 0x5L

.field public static final Companion:Lcom/discord/utilities/games/GameDetectionService$Companion;

.field private static final FOREGROUND_ACTION:Ljava/lang/String; = "com.discord.utilities.games.GameDetectionService.action.startforeground"

.field private static final SECONDS_PER_CHECK:J = 0xfL

.field private static final SERVICE_ID:I = 0xf1206


# instance fields
.field private detectionSub:Lrx/Subscription;

.field private runningGameSub:Lrx/Subscription;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/games/GameDetectionService$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/games/GameDetectionService$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/games/GameDetectionService;->Companion:Lcom/discord/utilities/games/GameDetectionService$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static final synthetic access$getDetectionSub$p(Lcom/discord/utilities/games/GameDetectionService;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/games/GameDetectionService;->detectionSub:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getRunningGameSub$p(Lcom/discord/utilities/games/GameDetectionService;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/games/GameDetectionService;->runningGameSub:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setDetectionSub$p(Lcom/discord/utilities/games/GameDetectionService;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/games/GameDetectionService;->detectionSub:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setRunningGameSub$p(Lcom/discord/utilities/games/GameDetectionService;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/games/GameDetectionService;->runningGameSub:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$updateNotification(Lcom/discord/utilities/games/GameDetectionService;Lcom/discord/stores/StoreRunningGame$RunningGame;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/games/GameDetectionService;->updateNotification(Lcom/discord/stores/StoreRunningGame$RunningGame;)V

    return-void
.end method

.method public static final declared-synchronized startIfEnabled(Landroid/content/Context;)V
    .locals 2

    const-class v0, Lcom/discord/utilities/games/GameDetectionService;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/discord/utilities/games/GameDetectionService;->Companion:Lcom/discord/utilities/games/GameDetectionService$Companion;

    invoke-virtual {v1, p0}, Lcom/discord/utilities/games/GameDetectionService$Companion;->startIfEnabled(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private final updateNotification(Lcom/discord/stores/StoreRunningGame$RunningGame;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sget-object v1, Lcom/discord/utilities/games/GameDetectionService;->Companion:Lcom/discord/utilities/games/GameDetectionService$Companion;

    invoke-static {v1, p0, p1}, Lcom/discord/utilities/games/GameDetectionService$Companion;->access$getForegroundNotification(Lcom/discord/utilities/games/GameDetectionService$Companion;Landroid/content/Context;Lcom/discord/stores/StoreRunningGame$RunningGame;)Landroid/app/Notification;

    move-result-object p1

    const v1, 0xf1206

    invoke-virtual {v0, v1, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 14

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    sget-object v0, Lf/a/b/o;->c:Lf/a/b/o;

    invoke-virtual {v0, p0}, Lf/a/b/o;->a(Ljava/lang/Object;)V

    sget-object v0, Lcom/discord/utilities/games/GameDetectionHelper;->INSTANCE:Lcom/discord/utilities/games/GameDetectionHelper;

    invoke-virtual {v0, p0}, Lcom/discord/utilities/games/GameDetectionHelper;->setup(Landroid/content/Context;)V

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->get()Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3, v1}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xf

    invoke-static {v2, v3, v4, v5, v1}, Lrx/Observable;->A(JJLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getRunningGame()Lcom/discord/stores/StoreRunningGame;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreRunningGame;->getForceGameDetection()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/utilities/games/GameDetectionService$onCreate$1;->INSTANCE:Lcom/discord/utilities/games/GameDetectionService$onCreate$1;

    invoke-static {v0, v1, v3, v4}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v5

    const-string v0, "Observable\n        .comb\u2026\n        ) { _, _, _ -> }"

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v6, Lcom/discord/utilities/games/GameDetectionService;

    new-instance v11, Lcom/discord/utilities/games/GameDetectionService$onCreate$2;

    invoke-direct {v11, p0}, Lcom/discord/utilities/games/GameDetectionService$onCreate$2;-><init>(Lcom/discord/utilities/games/GameDetectionService;)V

    new-instance v8, Lcom/discord/utilities/games/GameDetectionService$onCreate$3;

    invoke-direct {v8, p0}, Lcom/discord/utilities/games/GameDetectionService$onCreate$3;-><init>(Lcom/discord/utilities/games/GameDetectionService;)V

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v12, 0x1a

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getRunningGame()Lcom/discord/stores/StoreRunningGame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreRunningGame;->getRunningGame()Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/utilities/games/GameDetectionService;

    new-instance v7, Lcom/discord/utilities/games/GameDetectionService$onCreate$4;

    invoke-direct {v7, p0}, Lcom/discord/utilities/games/GameDetectionService$onCreate$4;-><init>(Lcom/discord/utilities/games/GameDetectionService;)V

    new-instance v4, Lcom/discord/utilities/games/GameDetectionService$onCreate$5;

    invoke-direct {v4, p0}, Lcom/discord/utilities/games/GameDetectionService$onCreate$5;-><init>(Lcom/discord/utilities/games/GameDetectionService;)V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1a

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/games/GameDetectionService;->detectionSub:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/games/GameDetectionService;->runningGameSub:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    sget-object v0, Lf/a/b/o;->c:Lf/a/b/o;

    invoke-virtual {v0, p0}, Lf/a/b/o;->b(Ljava/lang/Object;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public declared-synchronized onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    monitor-enter p0

    const v0, 0xf1206

    :try_start_0
    sget-object v1, Lcom/discord/utilities/games/GameDetectionService;->Companion:Lcom/discord/utilities/games/GameDetectionService$Companion;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, p0, v3, v2, v3}, Lcom/discord/utilities/games/GameDetectionService$Companion;->getForegroundNotification$default(Lcom/discord/utilities/games/GameDetectionService$Companion;Landroid/content/Context;Lcom/discord/stores/StoreRunningGame$RunningGame;ILjava/lang/Object;)Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
