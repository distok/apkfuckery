.class public final Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkMonitor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/networking/NetworkMonitor;->registerBroadcastConnectivityNetworkMonitor(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/networking/NetworkMonitor;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/networking/NetworkMonitor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1;->this$0:Lcom/discord/utilities/networking/NetworkMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intent"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1;->this$0:Lcom/discord/utilities/networking/NetworkMonitor;

    invoke-static {v0}, Lcom/discord/utilities/networking/NetworkMonitor;->access$getLogger$p(Lcom/discord/utilities/networking/NetworkMonitor;)Lcom/discord/utilities/logging/Logger;

    move-result-object v1

    const-string v2, "[NetworkMonitor]"

    const-string v3, "Got connectivity action broadcast intent."

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1;->this$0:Lcom/discord/utilities/networking/NetworkMonitor;

    invoke-static {v0, p1, p2}, Lcom/discord/utilities/networking/NetworkMonitor;->access$updateNetworkState(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method
