.class public final Lcom/discord/utilities/networking/NetworkMonitor;
.super Ljava/lang/Object;
.source "NetworkMonitor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/networking/NetworkMonitor$State;
    }
.end annotation


# instance fields
.field private logger:Lcom/discord/utilities/logging/Logger;

.field private final networkStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/utilities/networking/NetworkMonitor$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/utilities/logging/Logger;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/discord/utilities/networking/NetworkMonitor;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 p2, 0x0

    const/4 v0, 0x2

    invoke-static {p0, p1, p2, v0, p2}, Lcom/discord/utilities/networking/NetworkMonitor;->getState$default(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/content/Context;Landroid/content/Intent;ILjava/lang/Object;)Lcom/discord/utilities/networking/NetworkMonitor$State;

    move-result-object p2

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/utilities/networking/NetworkMonitor;->networkStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-direct {p0, p1}, Lcom/discord/utilities/networking/NetworkMonitor;->registerConnectivityNetworkMonitor(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$getLogger$p(Lcom/discord/utilities/networking/NetworkMonitor;)Lcom/discord/utilities/logging/Logger;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/networking/NetworkMonitor;->logger:Lcom/discord/utilities/logging/Logger;

    return-object p0
.end method

.method public static final synthetic access$setLogger$p(Lcom/discord/utilities/networking/NetworkMonitor;Lcom/discord/utilities/logging/Logger;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/networking/NetworkMonitor;->logger:Lcom/discord/utilities/logging/Logger;

    return-void
.end method

.method public static final synthetic access$updateNetworkState(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/networking/NetworkMonitor;->updateNetworkState(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private final getState(Landroid/content/Context;Landroid/content/Intent;)Lcom/discord/utilities/networking/NetworkMonitor$State;
    .locals 6

    sget-object v0, Lcom/discord/utilities/io/NetworkUtils;->INSTANCE:Lcom/discord/utilities/io/NetworkUtils;

    iget-object v1, p0, Lcom/discord/utilities/networking/NetworkMonitor;->logger:Lcom/discord/utilities/logging/Logger;

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/utilities/io/NetworkUtils;->isDeviceConnected(Landroid/content/Context;Landroid/content/Intent;Lcom/discord/utilities/logging/Logger;)Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p1, Lcom/discord/utilities/networking/NetworkMonitor$State;->ONLINE:Lcom/discord/utilities/networking/NetworkMonitor$State;

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/discord/utilities/io/NetworkUtils;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/utilities/networking/NetworkMonitor$State;->OFFLINE_AIRPLANE_MODE:Lcom/discord/utilities/networking/NetworkMonitor$State;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/discord/utilities/networking/NetworkMonitor$State;->OFFLINE:Lcom/discord/utilities/networking/NetworkMonitor$State;

    :goto_0
    iget-object v0, p0, Lcom/discord/utilities/networking/NetworkMonitor;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Updating network state to "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", API: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "[NetworkMonitor]"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-object p1
.end method

.method public static synthetic getState$default(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/content/Context;Landroid/content/Intent;ILjava/lang/Object;)Lcom/discord/utilities/networking/NetworkMonitor$State;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/networking/NetworkMonitor;->getState(Landroid/content/Context;Landroid/content/Intent;)Lcom/discord/utilities/networking/NetworkMonitor$State;

    move-result-object p0

    return-object p0
.end method

.method private final registerBroadcastAirplaneMode(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastAirplaneMode$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastAirplaneMode$1;-><init>(Lcom/discord/utilities/networking/NetworkMonitor;)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private final registerBroadcastConnectivityNetworkMonitor(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/networking/NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1;-><init>(Lcom/discord/utilities/networking/NetworkMonitor;)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private final registerConnectivityNetworkMonitor(Landroid/content/Context;)V
    .locals 5

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/net/ConnectivityManager;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_2

    new-instance v1, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;-><init>(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/content/Context;)V

    new-instance v2, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;-><init>(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/net/ConnectivityManager;Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v3, v4, :cond_1

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-virtual {v2}, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->invoke()V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->invoke()V

    :goto_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/networking/NetworkMonitor;->registerBroadcastConnectivityNetworkMonitor(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/networking/NetworkMonitor;->registerBroadcastAirplaneMode(Landroid/content/Context;)V

    :cond_2
    return-void
.end method

.method private final updateNetworkState(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/networking/NetworkMonitor;->networkStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/networking/NetworkMonitor;->getState(Landroid/content/Context;Landroid/content/Intent;)Lcom/discord/utilities/networking/NetworkMonitor$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic updateNetworkState$default(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/content/Context;Landroid/content/Intent;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/networking/NetworkMonitor;->updateNetworkState(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final observeIsConnected()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/utilities/networking/NetworkMonitor;->observeState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/networking/NetworkMonitor$observeIsConnected$1;->INSTANCE:Lcom/discord/utilities/networking/NetworkMonitor$observeIsConnected$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observeState().map { sta\u2026 }.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/networking/NetworkMonitor$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/networking/NetworkMonitor;->networkStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->G()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "networkStateSubject.onBa\u2026().distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
