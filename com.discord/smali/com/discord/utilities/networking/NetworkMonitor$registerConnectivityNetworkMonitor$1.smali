.class public final Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;
.super Lx/m/c/k;
.source "NetworkMonitor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/networking/NetworkMonitor;->registerConnectivityNetworkMonitor(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $connectivityManager:Landroid/net/ConnectivityManager;

.field public final synthetic $networkCallback:Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;

.field public final synthetic this$0:Lcom/discord/utilities/networking/NetworkMonitor;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/networking/NetworkMonitor;Landroid/net/ConnectivityManager;Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->this$0:Lcom/discord/utilities/networking/NetworkMonitor;

    iput-object p2, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->$connectivityManager:Landroid/net/ConnectivityManager;

    iput-object p3, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->$networkCallback:Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    :try_start_0
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->$connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->$networkCallback:Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;

    invoke-virtual {v1, v0, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/discord/utilities/networking/NetworkMonitor$registerConnectivityNetworkMonitor$1;->this$0:Lcom/discord/utilities/networking/NetworkMonitor;

    invoke-static {v1}, Lcom/discord/utilities/networking/NetworkMonitor;->access$getLogger$p(Lcom/discord/utilities/networking/NetworkMonitor;)Lcom/discord/utilities/logging/Logger;

    move-result-object v1

    const-string v2, "[NetworkMonitor]"

    const-string v3, "Unable to register network callback."

    invoke-virtual {v1, v2, v3, v0}, Lcom/discord/utilities/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
