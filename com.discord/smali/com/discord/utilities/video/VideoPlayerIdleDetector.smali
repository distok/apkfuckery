.class public final Lcom/discord/utilities/video/VideoPlayerIdleDetector;
.super Ljava/lang/Object;
.source "VideoPlayerIdleDetector.kt"


# instance fields
.field private final backgroundThreadScheduler:Lrx/Scheduler;

.field private final idleDetectionMs:J

.field private final idleDetectionScheduler:Lrx/Scheduler;

.field private idleDetectionSubscription:Lrx/Subscription;

.field private isIdle:Z

.field private final onIdleStateChanged:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLrx/Scheduler;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lrx/Scheduler;",
            "Lrx/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "idleDetectionScheduler"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backgroundThreadScheduler"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onIdleStateChanged"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionMs:J

    iput-object p3, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionScheduler:Lrx/Scheduler;

    iput-object p4, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->backgroundThreadScheduler:Lrx/Scheduler;

    iput-object p5, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onIdleStateChanged:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(JLrx/Scheduler;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    const-wide/16 p1, 0xbb8

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    invoke-static {}, Lg0/j/b/a;->a()Lrx/Scheduler;

    move-result-object p3

    const-string p1, "AndroidSchedulers.mainThread()"

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    move-object v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object p4

    const-string p1, "Schedulers.computation()"

    invoke-static {p4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    move-object v4, p4

    move-object v0, p0

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;-><init>(JLrx/Scheduler;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$getIdleDetectionSubscription$p(Lcom/discord/utilities/video/VideoPlayerIdleDetector;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setIdle(Lcom/discord/utilities/video/VideoPlayerIdleDetector;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    return-void
.end method

.method public static final synthetic access$setIdleDetectionSubscription$p(Lcom/discord/utilities/video/VideoPlayerIdleDetector;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionSubscription:Lrx/Subscription;

    return-void
.end method

.method private final beginIdleDetectionTimer()V
    .locals 11

    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->cancelIdleDetectionTimer()V

    iget-wide v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionMs:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->backgroundThreadScheduler:Lrx/Scheduler;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->Z(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v1}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v2

    const-string v0, "Observable\n        .time\u2026n(idleDetectionScheduler)"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    new-instance v8, Lcom/discord/utilities/video/VideoPlayerIdleDetector$beginIdleDetectionTimer$1;

    invoke-direct {v8, p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector$beginIdleDetectionTimer$1;-><init>(Lcom/discord/utilities/video/VideoPlayerIdleDetector;)V

    new-instance v5, Lcom/discord/utilities/video/VideoPlayerIdleDetector$beginIdleDetectionTimer$2;

    invoke-direct {v5, p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector$beginIdleDetectionTimer$2;-><init>(Lcom/discord/utilities/video/VideoPlayerIdleDetector;)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1a

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final cancelIdleDetectionTimer()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionSubscription:Lrx/Subscription;

    return-void
.end method

.method private final setIdle(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->isIdle:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->isIdle:Z

    iget-object v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onIdleStateChanged:Lkotlin/jvm/functions/Function1;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method public final beginIdleDetection()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionSubscription:Lrx/Subscription;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->beginIdleDetectionTimer()V

    :cond_0
    return-void
.end method

.method public final dispose()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->cancelIdleDetectionTimer()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    return-void
.end method

.method public final endIdleDetection()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->cancelIdleDetectionTimer()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    return-void
.end method

.method public final onInteraction()V
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->isIdle:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->beginIdleDetectionTimer()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->cancelIdleDetectionTimer()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    :goto_0
    return-void
.end method

.method public final onPreventIdle()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->setIdle(Z)V

    iget-object v0, p0, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->idleDetectionSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->beginIdleDetectionTimer()V

    :cond_0
    return-void
.end method
