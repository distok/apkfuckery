.class public final Lcom/discord/utilities/ChannelShortcutInfo;
.super Ljava/lang/Object;
.source "ShareUtils.kt"


# instance fields
.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final isPinnedOnly:Z

.field private final rank:I


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;IZ)V
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    iput p2, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    iput-boolean p3, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/ChannelShortcutInfo;Lcom/discord/models/domain/ModelChannel;IZILjava/lang/Object;)Lcom/discord/utilities/ChannelShortcutInfo;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/ChannelShortcutInfo;->copy(Lcom/discord/models/domain/ModelChannel;IZ)Lcom/discord/utilities/ChannelShortcutInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;IZ)Lcom/discord/utilities/ChannelShortcutInfo;
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/ChannelShortcutInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/utilities/ChannelShortcutInfo;-><init>(Lcom/discord/models/domain/ModelChannel;IZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/ChannelShortcutInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/ChannelShortcutInfo;

    iget-object v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    iget v1, p1, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    iget-boolean p1, p1, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getRank()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isPinnedOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ChannelShortcutInfo(channel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->rank:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isPinnedOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/ChannelShortcutInfo;->isPinnedOnly:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
