.class public final Lcom/discord/utilities/app/ApplicationProvider;
.super Ljava/lang/Object;
.source "ApplicationProvider.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

.field private static application:Landroid/app/Application;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/app/ApplicationProvider;

    invoke-direct {v0}, Lcom/discord/utilities/app/ApplicationProvider;-><init>()V

    sput-object v0, Lcom/discord/utilities/app/ApplicationProvider;->INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Landroid/app/Application;
    .locals 1

    sget-object v0, Lcom/discord/utilities/app/ApplicationProvider;->application:Landroid/app/Application;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "application"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final init(Landroid/app/Application;)V
    .locals 1

    const-string v0, "application"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object p1, Lcom/discord/utilities/app/ApplicationProvider;->application:Landroid/app/Application;

    return-void
.end method
