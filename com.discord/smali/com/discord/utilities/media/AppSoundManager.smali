.class public final Lcom/discord/utilities/media/AppSoundManager;
.super Ljava/lang/Object;
.source "AppSoundManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;,
        Lcom/discord/utilities/media/AppSoundManager$Provider;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private soundPlayers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1

    const-string v0, "application"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/media/AppSoundManager;->context:Landroid/content/Context;

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final isPlaying(Lcom/discord/utilities/media/AppSound;)Z
    .locals 1

    const-string v0, "sound"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSound;->getResId()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final play(Lcom/discord/utilities/media/AppSound;)V
    .locals 5

    const-string v0, "sound"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSound;->getResId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;->release()Lkotlin/Unit;

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSound;->getResId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;

    iget-object v3, p0, Lcom/discord/utilities/media/AppSoundManager;->context:Landroid/content/Context;

    new-instance v4, Lcom/discord/utilities/media/AppSoundManager$play$1;

    invoke-direct {v4, p0, p1}, Lcom/discord/utilities/media/AppSoundManager$play$1;-><init>(Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/media/AppSound;)V

    invoke-direct {v2, v3, p1, v4}, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;-><init>(Landroid/content/Context;Lcom/discord/utilities/media/AppSound;Lkotlin/jvm/functions/Function0;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSound;->getResId()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;->start()Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method public final stop(Lcom/discord/utilities/media/AppSound;)V
    .locals 2

    const-string v0, "sound"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSound;->getResId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;->release()Lkotlin/Unit;

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager;->soundPlayers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AppSound;->getResId()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
