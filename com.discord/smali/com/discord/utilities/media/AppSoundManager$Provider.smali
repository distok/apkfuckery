.class public final Lcom/discord/utilities/media/AppSoundManager$Provider;
.super Ljava/lang/Object;
.source "AppSoundManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/media/AppSoundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Provider"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/media/AppSoundManager$Provider;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/media/AppSoundManager$Provider;

    invoke-direct {v0}, Lcom/discord/utilities/media/AppSoundManager$Provider;-><init>()V

    sput-object v0, Lcom/discord/utilities/media/AppSoundManager$Provider;->INSTANCE:Lcom/discord/utilities/media/AppSoundManager$Provider;

    sget-object v0, Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/media/AppSoundManager$Provider;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getINSTANCE()Lcom/discord/utilities/media/AppSoundManager;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSoundManager$Provider;->INSTANCE$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/media/AppSoundManager;

    return-object v0
.end method


# virtual methods
.method public final get()Lcom/discord/utilities/media/AppSoundManager;
    .locals 1

    invoke-direct {p0}, Lcom/discord/utilities/media/AppSoundManager$Provider;->getINSTANCE()Lcom/discord/utilities/media/AppSoundManager;

    move-result-object v0

    return-object v0
.end method
