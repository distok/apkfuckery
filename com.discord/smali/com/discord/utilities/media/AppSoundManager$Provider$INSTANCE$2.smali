.class public final Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;
.super Lx/m/c/k;
.source "AppSoundManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/media/AppSoundManager$Provider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/utilities/media/AppSoundManager;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;

    invoke-direct {v0}, Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;-><init>()V

    sput-object v0, Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/utilities/media/AppSoundManager;
    .locals 2

    new-instance v0, Lcom/discord/utilities/media/AppSoundManager;

    sget-object v1, Lcom/discord/utilities/app/ApplicationProvider;->INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

    invoke-virtual {v1}, Lcom/discord/utilities/app/ApplicationProvider;->get()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/utilities/media/AppSoundManager;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/media/AppSoundManager$Provider$INSTANCE$2;->invoke()Lcom/discord/utilities/media/AppSoundManager;

    move-result-object v0

    return-object v0
.end method
