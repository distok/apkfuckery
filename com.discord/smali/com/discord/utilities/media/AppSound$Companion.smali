.class public final Lcom/discord/utilities/media/AppSound$Companion;
.super Ljava/lang/Object;
.source "AppSound.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/media/AppSound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/media/AppSound$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getSOUND_CALL_CALLING()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_CALL_CALLING$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_CALL_RINGING()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_CALL_RINGING$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_DEAFEN()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_DEAFEN$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_MUTE()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_MUTE$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_STREAM_ENDED()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_STREAM_ENDED$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_STREAM_STARTED()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_STREAM_STARTED$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_STREAM_USER_JOINED()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_STREAM_USER_JOINED$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_STREAM_USER_LEFT()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_STREAM_USER_LEFT$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_UNDEAFEN()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_UNDEAFEN$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method

.method public final getSOUND_UNMUTE()Lcom/discord/utilities/media/AppSound;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/media/AppSound;->access$getSOUND_UNMUTE$cp()Lcom/discord/utilities/media/AppSound;

    move-result-object v0

    return-object v0
.end method
