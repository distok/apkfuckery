.class public final Lcom/discord/utilities/media/AudioOutputMonitor;
.super Landroid/content/BroadcastReceiver;
.source "AudioOutputMonitor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/media/AudioOutputMonitor$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/media/AudioOutputMonitor$Companion;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;


# instance fields
.field private final intentFilter:Landroid/content/IntentFilter;

.field private final intentHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/KFunction<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private outputState:Lcom/discord/utilities/media/AudioOutputState;

.field private final outputStateSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Lcom/discord/utilities/media/AudioOutputState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/media/AudioOutputMonitor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/media/AudioOutputMonitor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AudioOutputMonitor;->Companion:Lcom/discord/utilities/media/AudioOutputMonitor$Companion;

    sget-object v0, Lcom/discord/utilities/media/AudioOutputMonitor$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/media/AudioOutputMonitor$Companion$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/media/AudioOutputMonitor;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v8, Lcom/discord/utilities/media/AudioOutputState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/utilities/media/AudioOutputState;-><init>(ZZZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v8, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputState:Lcom/discord/utilities/media/AudioOutputState;

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {v8}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputStateSubject:Lrx/subjects/SerializedSubject;

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/Pair;

    new-instance v1, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$1;-><init>(Lcom/discord/utilities/media/AudioOutputMonitor;)V

    new-instance v2, Lkotlin/Pair;

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, v3, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$2;

    invoke-direct {v2, p0}, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$2;-><init>(Lcom/discord/utilities/media/AudioOutputMonitor;)V

    new-instance v3, Lkotlin/Pair;

    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$3;

    invoke-direct {v2, p0}, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$3;-><init>(Lcom/discord/utilities/media/AudioOutputMonitor;)V

    new-instance v3, Lkotlin/Pair;

    const-string v4, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$4;

    invoke-direct {v2, p0}, Lcom/discord/utilities/media/AudioOutputMonitor$intentHandlers$4;-><init>(Lcom/discord/utilities/media/AudioOutputMonitor;)V

    new-instance v3, Lkotlin/Pair;

    const-string v4, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->intentHandlers:Ljava/util/Map;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->intentFilter:Landroid/content/IntentFilter;

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AudioOutputMonitor;->INSTANCE$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$handleBluetoothAdapterUpdate(Lcom/discord/utilities/media/AudioOutputMonitor;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleBluetoothAdapterUpdate(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic access$handleBluetoothConnectionUpdate(Lcom/discord/utilities/media/AudioOutputMonitor;ILandroid/bluetooth/BluetoothDevice;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleBluetoothConnectionUpdate(ILandroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method public static final synthetic access$handleBluetoothConnectionUpdate(Lcom/discord/utilities/media/AudioOutputMonitor;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleBluetoothConnectionUpdate(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic access$handleHeadsetPlugIntent(Lcom/discord/utilities/media/AudioOutputMonitor;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleHeadsetPlugIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic access$handleScoUpdate(Lcom/discord/utilities/media/AudioOutputMonitor;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleScoUpdate(Landroid/content/Intent;)V

    return-void
.end method

.method private final declared-synchronized handleBluetoothAdapterUpdate(I)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputState:Lcom/discord/utilities/media/AudioOutputState;

    const/16 v1, 0xa

    if-eq p1, v1, :cond_1

    const/16 v1, 0xc

    if-eq p1, v1, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled()Z

    move-result p1

    move v1, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    const/4 v1, 0x1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/media/AudioOutputState;->copy$default(Lcom/discord/utilities/media/AudioOutputState;ZZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->setOutputState(Lcom/discord/utilities/media/AudioOutputState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleBluetoothAdapterUpdate(Landroid/content/Intent;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "android.bluetooth.adapter.extra.STATE"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleBluetoothAdapterUpdate(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleBluetoothConnectionUpdate(ILandroid/bluetooth/BluetoothDevice;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputState:Lcom/discord/utilities/media/AudioOutputState;

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    const/4 p1, 0x1

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    move-object v5, p1

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/media/AudioOutputState;->copy$default(Lcom/discord/utilities/media/AudioOutputState;ZZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->setOutputState(Lcom/discord/utilities/media/AudioOutputState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleBluetoothConnectionUpdate(Landroid/content/Intent;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "android.bluetooth.profile.extra.STATE"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleBluetoothConnectionUpdate(ILandroid/bluetooth/BluetoothDevice;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleHeadsetPlugIntent(Landroid/content/Intent;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "state"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputState:Lcom/discord/utilities/media/AudioOutputState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq p1, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/media/AudioOutputState;->copy$default(Lcom/discord/utilities/media/AudioOutputState;ZZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->setOutputState(Lcom/discord/utilities/media/AudioOutputState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleInitialState(Landroid/content/Context;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "bluetooth"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/bluetooth/BluetoothManager;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/bluetooth/BluetoothManager;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v1, Lcom/discord/utilities/media/AudioOutputMonitor$handleInitialState$1;

    invoke-direct {v1, p0, v0}, Lcom/discord/utilities/media/AudioOutputMonitor$handleInitialState$1;-><init>(Lcom/discord/utilities/media/AudioOutputMonitor;Landroid/bluetooth/BluetoothAdapter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 p1, 0xc

    goto :goto_0

    :cond_1
    const/16 p1, 0xa

    :goto_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleBluetoothAdapterUpdate(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleScoUpdate(Landroid/content/Intent;)V
    .locals 8

    monitor-enter p0

    :try_start_0
    const-string v0, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputState:Lcom/discord/utilities/media/AudioOutputState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1b

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/media/AudioOutputState;->copy$default(Lcom/discord/utilities/media/AudioOutputState;ZZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->setOutputState(Lcom/discord/utilities/media/AudioOutputState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final setOutputState(Lcom/discord/utilities/media/AudioOutputState;)V
    .locals 1

    iput-object p1, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputState:Lcom/discord/utilities/media/AudioOutputState;

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputStateSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final bindContext(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->handleInitialState(Landroid/content/Context;)V

    return-void
.end method

.method public final getOutputState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/media/AudioOutputState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->outputStateSubject:Lrx/subjects/SerializedSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "outputStateSubject\n        .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "intent?.action ?: return"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputMonitor;->intentHandlers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/reflect/KFunction;

    if-eqz p1, :cond_0

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
