.class public final Lcom/discord/utilities/media/AudioOutputMonitor$Companion;
.super Ljava/lang/Object;
.source "AudioOutputMonitor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/media/AudioOutputMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/media/AudioOutputMonitor$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getINSTANCE()Lcom/discord/utilities/media/AudioOutputMonitor;
    .locals 2

    invoke-static {}, Lcom/discord/utilities/media/AudioOutputMonitor;->access$getINSTANCE$cp()Lkotlin/Lazy;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/media/AudioOutputMonitor;->Companion:Lcom/discord/utilities/media/AudioOutputMonitor$Companion;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/media/AudioOutputMonitor;

    return-object v0
.end method

.method public final initialize(Landroid/app/Application;)V
    .locals 1

    const-string v0, "application"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/media/AudioOutputMonitor$Companion;->getINSTANCE()Lcom/discord/utilities/media/AudioOutputMonitor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/media/AudioOutputMonitor;->bindContext(Landroid/content/Context;)V

    return-void
.end method
