.class public final Lcom/discord/utilities/media/AppSoundManager$play$1;
.super Lx/m/c/k;
.source "AppSoundManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/media/AppSoundManager;->play(Lcom/discord/utilities/media/AppSound;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $sound:Lcom/discord/utilities/media/AppSound;

.field public final synthetic this$0:Lcom/discord/utilities/media/AppSoundManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/media/AppSound;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/media/AppSoundManager$play$1;->this$0:Lcom/discord/utilities/media/AppSoundManager;

    iput-object p2, p0, Lcom/discord/utilities/media/AppSoundManager$play$1;->$sound:Lcom/discord/utilities/media/AppSound;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/media/AppSoundManager$play$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager$play$1;->$sound:Lcom/discord/utilities/media/AppSound;

    invoke-virtual {v0}, Lcom/discord/utilities/media/AppSound;->getShouldLoop()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/media/AppSoundManager$play$1;->this$0:Lcom/discord/utilities/media/AppSoundManager;

    iget-object v1, p0, Lcom/discord/utilities/media/AppSoundManager$play$1;->$sound:Lcom/discord/utilities/media/AppSound;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/media/AppSoundManager;->stop(Lcom/discord/utilities/media/AppSound;)V

    :cond_0
    return-void
.end method
