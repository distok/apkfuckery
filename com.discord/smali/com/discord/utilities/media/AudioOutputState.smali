.class public final Lcom/discord/utilities/media/AudioOutputState;
.super Ljava/lang/Object;
.source "AudioOutputMonitor.kt"


# instance fields
.field private final bluetoothDeviceName:Ljava/lang/String;

.field private final canBluetoothScoStart:Z

.field private final isBluetoothAdapterDisabled:Z

.field private final isBluetoothHeadsetDisconnected:Z

.field private final isBluetoothOutputConnected:Z

.field private final isBluetoothScoDisconnected:Z

.field private final isExternalAudioOutputConnected:Z

.field private final isHeadsetUnplugged:Z


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/discord/utilities/media/AudioOutputState;-><init>(ZZZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZZZLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    iput-boolean p2, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    iput-boolean p3, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    iput-boolean p4, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    iput-object p5, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    const/4 p5, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected:Z

    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    iput-boolean p2, p0, Lcom/discord/utilities/media/AudioOutputState;->canBluetoothScoStart:Z

    if-nez p1, :cond_3

    if-nez p4, :cond_2

    goto :goto_2

    :cond_2
    const/4 p5, 0x0

    :cond_3
    :goto_2
    iput-boolean p5, p0, Lcom/discord/utilities/media/AudioOutputState;->isExternalAudioOutputConnected:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x1

    if-eqz p7, :cond_0

    const/4 p7, 0x1

    goto :goto_0

    :cond_0
    move p7, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    move v2, p3

    :goto_2
    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move v0, p4

    :goto_3
    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_4

    const/4 p5, 0x0

    :cond_4
    move-object p6, p5

    move-object p1, p0

    move p2, p7

    move p3, v1

    move p4, v2

    move p5, v0

    invoke-direct/range {p1 .. p6}, Lcom/discord/utilities/media/AudioOutputState;-><init>(ZZZZLjava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/media/AudioOutputState;ZZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/media/AudioOutputState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/utilities/media/AudioOutputState;->copy(ZZZZLjava/lang/String;)Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZZZZLjava/lang/String;)Lcom/discord/utilities/media/AudioOutputState;
    .locals 7

    new-instance v6, Lcom/discord/utilities/media/AudioOutputState;

    move-object v0, v6

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/media/AudioOutputState;-><init>(ZZZZLjava/lang/String;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/media/AudioOutputState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/media/AudioOutputState;

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    iget-boolean v1, p1, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    iget-boolean v1, p1, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    iget-boolean v1, p1, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    iget-boolean v1, p1, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBluetoothDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCanBluetoothScoStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->canBluetoothScoStart:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isBluetoothAdapterDisabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    return v0
.end method

.method public final isBluetoothHeadsetDisconnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    return v0
.end method

.method public final isBluetoothOutputConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected:Z

    return v0
.end method

.method public final isBluetoothScoDisconnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    return v0
.end method

.method public final isExternalAudioOutputConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isExternalAudioOutputConnected:Z

    return v0
.end method

.method public final isHeadsetUnplugged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "AudioOutputState(isBluetoothAdapterDisabled="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothAdapterDisabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isBluetoothHeadsetDisconnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothHeadsetDisconnected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isBluetoothScoDisconnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothScoDisconnected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isHeadsetUnplugged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", bluetoothDeviceName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/media/AudioOutputState;->bluetoothDeviceName:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
