.class public final Lcom/discord/utilities/media/AppSound;
.super Ljava/lang/Object;
.source "AppSound.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/media/AppSound$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/media/AppSound$Companion;

.field private static final SOUND_CALL_CALLING:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_CALL_RINGING:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_DEAFEN:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_MUTE:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_STREAM_ENDED:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_STREAM_STARTED:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_STREAM_USER_JOINED:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_STREAM_USER_LEFT:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_UNDEAFEN:Lcom/discord/utilities/media/AppSound;

.field private static final SOUND_UNMUTE:Lcom/discord/utilities/media/AppSound;


# instance fields
.field private final contentType:I

.field private final resId:I

.field private final shouldLoop:Z

.field private final usage:I


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/discord/utilities/media/AppSound$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/media/AppSound$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v3, 0x7f110003

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/16 v6, 0xd

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_DEAFEN:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v10, 0x7f110d02

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/16 v13, 0xd

    const/4 v14, 0x2

    const/4 v15, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_UNDEAFEN:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v2, 0x7f110cfd

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/16 v5, 0xd

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_MUTE:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v9, 0x7f110d03

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/16 v12, 0xd

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_UNMUTE:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v2, 0x7f110cfe

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_ENDED:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v9, 0x7f110cff

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_STARTED:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v2, 0x7f110d00

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_USER_JOINED:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v9, 0x7f110d01

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/discord/utilities/media/AppSound;-><init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_USER_LEFT:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v1, 0x7f110002

    const/4 v2, 0x1

    const/4 v3, 0x4

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/utilities/media/AppSound;-><init>(IZII)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_CALL_RINGING:Lcom/discord/utilities/media/AppSound;

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    const v1, 0x7f110001

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/utilities/media/AppSound;-><init>(IZII)V

    sput-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_CALL_CALLING:Lcom/discord/utilities/media/AppSound;

    return-void
.end method

.method public constructor <init>(IZII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    iput-boolean p2, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    iput p3, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    iput p4, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    return-void
.end method

.method public synthetic constructor <init>(IZIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/media/AppSound;-><init>(IZII)V

    return-void
.end method

.method public static final synthetic access$getSOUND_CALL_CALLING$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_CALL_CALLING:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_CALL_RINGING$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_CALL_RINGING:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_DEAFEN$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_DEAFEN:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_MUTE$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_MUTE:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_STREAM_ENDED$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_ENDED:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_STREAM_STARTED$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_STARTED:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_STREAM_USER_JOINED$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_USER_JOINED:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_STREAM_USER_LEFT$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_STREAM_USER_LEFT:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_UNDEAFEN$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_UNDEAFEN:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static final synthetic access$getSOUND_UNMUTE$cp()Lcom/discord/utilities/media/AppSound;
    .locals 1

    sget-object v0, Lcom/discord/utilities/media/AppSound;->SOUND_UNMUTE:Lcom/discord/utilities/media/AppSound;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/media/AppSound;IZIIILjava/lang/Object;)Lcom/discord/utilities/media/AppSound;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-boolean p2, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/media/AppSound;->copy(IZII)Lcom/discord/utilities/media/AppSound;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    return v0
.end method

.method public final copy(IZII)Lcom/discord/utilities/media/AppSound;
    .locals 1

    new-instance v0, Lcom/discord/utilities/media/AppSound;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/utilities/media/AppSound;-><init>(IZII)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/media/AppSound;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/media/AppSound;

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    iget v1, p1, Lcom/discord/utilities/media/AppSound;->resId:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    iget-boolean v1, p1, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    iget v1, p1, Lcom/discord/utilities/media/AppSound;->contentType:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    iget p1, p1, Lcom/discord/utilities/media/AppSound;->usage:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContentType()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    return v0
.end method

.method public final getResId()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    return v0
.end method

.method public final getShouldLoop()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    return v0
.end method

.method public final getUsage()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "AppSound(resId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/utilities/media/AppSound;->resId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", shouldLoop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/media/AppSound;->shouldLoop:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", contentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/utilities/media/AppSound;->contentType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", usage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/utilities/media/AppSound;->usage:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
