.class public final Lcom/discord/utilities/spans/VerticalPaddingSpan;
.super Ljava/lang/Object;
.source "VerticalPaddingSpan.kt"

# interfaces
.implements Landroid/text/style/LineHeightSpan;


# instance fields
.field private initialized:Z

.field private origAscent:I

.field private origBottom:I

.field private origDescent:I

.field private origTop:I

.field private final paddingBottom:I

.field private final paddingTop:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->paddingTop:I

    iput p2, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->paddingBottom:I

    const/4 p1, -0x1

    iput p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origTop:I

    iput p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origAscent:I

    iput p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origBottom:I

    iput p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origDescent:I

    return-void
.end method


# virtual methods
.method public chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V
    .locals 0

    const-string/jumbo p4, "text"

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "fontMetrics"

    invoke-static {p6, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of p4, p1, Landroid/text/Spanned;

    if-nez p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    move-object p4, p1

    :goto_0
    check-cast p4, Landroid/text/Spanned;

    if-eqz p4, :cond_4

    iget-boolean p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->initialized:Z

    if-nez p4, :cond_1

    iget p4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origTop:I

    iget p4, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origAscent:I

    iget p4, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iput p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origBottom:I

    iget p4, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origDescent:I

    const/4 p4, 0x1

    iput-boolean p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->initialized:Z

    :cond_1
    check-cast p1, Landroid/text/Spanned;

    invoke-interface {p1, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result p4

    if-ne p4, p2, :cond_2

    iget p2, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget p4, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->paddingTop:I

    sub-int/2addr p2, p4

    iput p2, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget p2, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr p2, p4

    iput p2, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    goto :goto_1

    :cond_2
    iget p2, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origTop:I

    iput p2, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget p2, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origAscent:I

    iput p2, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    :goto_1
    invoke-interface {p1, p0}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result p1

    if-ne p1, p3, :cond_3

    iget p1, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget p2, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->paddingBottom:I

    add-int/2addr p1, p2

    iput p1, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget p1, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int/2addr p1, p2

    iput p1, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    goto :goto_2

    :cond_3
    iget p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origBottom:I

    iput p1, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget p1, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->origDescent:I

    iput p1, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    :cond_4
    :goto_2
    return-void
.end method

.method public final getPaddingBottom()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/spans/VerticalPaddingSpan;->paddingBottom:I

    return v0
.end method
