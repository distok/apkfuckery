.class public final Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;
.super Lx/j/h/a/g;
.source "ApngUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.discord.utilities.apng.ApngUtils$renderApngFromFile$3$1"
    f = "ApngUtils.kt"
    l = {}
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $drawable:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field public final synthetic this$0:Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->this$0:Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;

    iput-object p2, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->$drawable:Lkotlin/jvm/internal/Ref$ObjectRef;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;

    iget-object v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->this$0:Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;

    iget-object v2, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->$drawable:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0, v1, v2, p2}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;-><init>(Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;

    sget-object p2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    iget v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->label:I

    if-nez v1, :cond_1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->this$0:Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;

    iget-object p1, p1, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$imageViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const-string v1, "imageViewRef.get() ?: return@withContext"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->$drawable:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->this$0:Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;

    iget-boolean p1, p1, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$autoPlay:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/utilities/apng/ApngUtils;->INSTANCE:Lcom/discord/utilities/apng/ApngUtils;

    iget-object v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;->$drawable:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Lcom/discord/utilities/apng/ApngUtils;->playApngAnimation(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
