.class public final Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;
.super Lx/j/h/a/g;
.source "ApngUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/apng/ApngUtils;->renderApngFromFile(Ljava/io/File;Landroid/widget/ImageView;Ljava/lang/Integer;Ljava/lang/Integer;Z)Lkotlinx/coroutines/Job;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.discord.utilities.apng.ApngUtils$renderApngFromFile$3"
    f = "ApngUtils.kt"
    l = {
        0x27
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $autoPlay:Z

.field public final synthetic $file:Ljava/io/File;

.field public final synthetic $imageViewRef:Ljava/lang/ref/WeakReference;

.field public L$0:Ljava/lang/Object;

.field public L$1:Ljava/lang/Object;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ljava/io/File;ZLkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$imageViewRef:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$file:Ljava/io/File;

    iput-boolean p3, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$autoPlay:Z

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;

    iget-object v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$imageViewRef:Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$file:Ljava/io/File;

    iget-boolean v3, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$autoPlay:Z

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;-><init>(Ljava/lang/ref/WeakReference;Ljava/io/File;ZLkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;

    sget-object p2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    sget-object v1, Lx/j/g/a;->d:Lx/j/g/a;

    iget v2, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->L$1:Ljava/lang/Object;

    check-cast v1, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->p$:Lkotlinx/coroutines/CoroutineScope;

    :try_start_1
    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    sget-object v4, Le0/a/b/a;->c:Le0/a/b/a$b;

    iget-object v5, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$imageViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v5, :cond_2

    new-instance v6, Ljava/io/FileInputStream;

    iget-object v7, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->$file:Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    invoke-static/range {v4 .. v9}, Le0/a/b/a$b;->a(Le0/a/b/a$b;Landroid/content/Context;Ljava/io/InputStream;FLandroid/graphics/Bitmap$Config;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    sget-object v4, Ly/a/h0;->a:Ly/a/v;

    sget-object v4, Ly/a/s1/j;->b:Ly/a/e1;

    new-instance v5, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v2, v6}, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3$1;-><init>(Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V

    iput-object p1, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->L$0:Ljava/lang/Object;

    iput-object v2, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->L$1:Ljava/lang/Object;

    iput v3, p0, Lcom/discord/utilities/apng/ApngUtils$renderApngFromFile$3;->label:I

    invoke-static {v4, v5, p0}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne p1, v1, :cond_3

    return-object v1

    :cond_2
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_0
    return-object v0
.end method
