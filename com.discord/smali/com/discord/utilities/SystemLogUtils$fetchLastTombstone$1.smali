.class public final Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;
.super Ljava/lang/Object;
.source "SystemLogUtils.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/SystemLogUtils;->fetchLastTombstone()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/LinkedList<",
        "Ljava/lang/String;",
        ">;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/SystemLogUtils$Tombstone;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;

    invoke-direct {v0}, Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;->INSTANCE:Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/LinkedList;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/SystemLogUtils$fetchLastTombstone$1;->call(Ljava/util/LinkedList;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/LinkedList;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/SystemLogUtils$Tombstone;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/SystemLogUtils;->INSTANCE:Lcom/discord/utilities/SystemLogUtils;

    const-string v1, "crashes"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/utilities/SystemLogUtils;->fetchLastTombstone$app_productionDiscordExternalRelease(Ljava/util/Collection;)Lcom/discord/utilities/SystemLogUtils$Tombstone;

    move-result-object p1

    if-nez p1, :cond_0

    sget-object p1, Lg0/l/a/f;->e:Lrx/Observable;

    goto :goto_0

    :cond_0
    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
