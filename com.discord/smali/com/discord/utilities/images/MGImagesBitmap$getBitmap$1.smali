.class public final Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;
.super Ljava/lang/Object;
.source "MGImagesBitmap.kt"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/images/MGImagesBitmap;->getBitmap(Ljava/lang/String;Z)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $dataSource:Lcom/facebook/datasource/DataSource;

.field public final synthetic $imageUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/datasource/DataSource;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->$dataSource:Lcom/facebook/datasource/DataSource;

    iput-object p2, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->$imageUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public final call(Lrx/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->$dataSource:Lcom/facebook/datasource/DataSource;

    new-instance v1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;-><init>(Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;Lrx/Subscriber;)V

    sget-object p1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$2;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$2;

    invoke-interface {v0, v1, p1}, Lcom/facebook/datasource/DataSource;->f(Lf/g/e/f;Ljava/util/concurrent/Executor;)V

    return-void
.end method
