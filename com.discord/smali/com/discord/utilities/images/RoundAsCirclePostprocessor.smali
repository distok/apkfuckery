.class public final Lcom/discord/utilities/images/RoundAsCirclePostprocessor;
.super Lf/g/j/p/a;
.source "RoundAsCirclePostProcessor.kt"


# instance fields
.field private final imageUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "imageUri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lf/g/j/p/a;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/images/RoundAsCirclePostprocessor;->imageUri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public process(Landroid/graphics/Bitmap;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    :cond_1
    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    if-lt v0, v2, :cond_2

    invoke-super {p0, p1}, Lf/g/j/p/a;->process(Landroid/graphics/Bitmap;)V

    :cond_2
    return-void
.end method
