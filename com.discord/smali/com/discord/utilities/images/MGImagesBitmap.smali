.class public final Lcom/discord/utilities/images/MGImagesBitmap;
.super Ljava/lang/Object;
.source "MGImagesBitmap.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/images/MGImagesBitmap$ImageNotFoundException;,
        Lcom/discord/utilities/images/MGImagesBitmap$DecodeException;,
        Lcom/discord/utilities/images/MGImagesBitmap$MissingBitmapException;,
        Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;,
        Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/images/MGImagesBitmap;

    invoke-direct {v0}, Lcom/discord/utilities/images/MGImagesBitmap;-><init>()V

    sput-object v0, Lcom/discord/utilities/images/MGImagesBitmap;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final isValidUri(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_6

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_6

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-static {p1}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 p1, 0x1

    :goto_5
    if-nez p1, :cond_6

    const/4 v1, 0x1

    :cond_6
    return v1
.end method


# virtual methods
.method public final getBitmap(Ljava/lang/String;Z)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Observable<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    const-string v0, "imageUri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/images/MGImagesBitmap;->isValidUri(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "invalid uri"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lrx/Observable;->u(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.error(Illegal\u2026Exception(\"invalid uri\"))"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    sget-object v0, Lf/g/j/e/m;->t:Lf/g/j/e/m;

    const-string v1, "ImagePipelineFactory was not initialized!"

    invoke-static {v0, v1}, Ls/a/b/b/a;->i(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lf/g/j/e/m;->a()Lf/g/j/e/i;

    move-result-object v1

    iput-object v1, v0, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    :cond_1
    iget-object v2, v0, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    const/4 v0, 0x0

    invoke-static {p1, v0, v0, v0}, Lcom/discord/utilities/images/MGImages;->getImageRequest(Ljava/lang/String;IIZ)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v0

    if-eqz p2, :cond_2

    new-instance p2, Lcom/discord/utilities/images/RoundAsCirclePostprocessor;

    invoke-direct {p2, p1}, Lcom/discord/utilities/images/RoundAsCirclePostprocessor;-><init>(Ljava/lang/String;)V

    iput-object p2, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->j:Lf/g/j/r/b;

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/imagepipeline/request/ImageRequest$c;->d:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lf/g/j/e/i;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Lcom/facebook/imagepipeline/request/ImageRequest$c;Lf/g/j/l/e;Ljava/lang/String;)Lcom/facebook/datasource/DataSource;

    move-result-object p2

    new-instance v0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;

    invoke-direct {v0, p2, p1}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;-><init>(Lcom/facebook/datasource/DataSource;Ljava/lang/String;)V

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.unsafeCreate \u2026y emits the bitmap.\n    }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getBitmaps(Ljava/util/Set;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;",
            ">;"
        }
    .end annotation

    const-string v0, "imageRequests"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;

    invoke-virtual {v2}, Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;->getImageUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance p1, Lg0/l/a/t;

    invoke-direct {p1, v0}, Lg0/l/a/t;-><init>(Ljava/lang/Iterable;)V

    invoke-static {p1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$2;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$2;

    sget-object v1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$3;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$3;

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->b0(Lg0/k/b;Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$4;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$4;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->S(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n        .from\u2026Schedulers.computation())"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
