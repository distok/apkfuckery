.class public final Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;
.super Ljava/lang/Object;
.source "MGImagesBitmap.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/images/MGImagesBitmap;->getBitmaps(Ljava/util/Set;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;",
        "Lrx/Observable<",
        "+",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/String;",
        "+",
        "Landroid/graphics/Bitmap;",
        ">;>;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;

    invoke-direct {v0}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;->call(Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/images/MGImagesBitmap;->INSTANCE:Lcom/discord/utilities/images/MGImagesBitmap;

    invoke-virtual {p1}, Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;->getImageUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;->getRoundAsCircle()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/images/MGImagesBitmap;->getBitmap(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1$1;

    invoke-direct {v1, p1}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1$1;-><init>(Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
