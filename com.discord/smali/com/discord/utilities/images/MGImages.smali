.class public final Lcom/discord/utilities/images/MGImages;
.super Ljava/lang/Object;
.source "MGImages.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/images/MGImages$ChangeDetector;,
        Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;,
        Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/images/MGImages;

.field private static final imageEncoder:Lcom/discord/utilities/images/ImageEncoder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/images/MGImages;

    invoke-direct {v0}, Lcom/discord/utilities/images/MGImages;-><init>()V

    sput-object v0, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    new-instance v0, Lcom/discord/utilities/images/ImageEncoder;

    invoke-direct {v0}, Lcom/discord/utilities/images/ImageEncoder;-><init>()V

    sput-object v0, Lcom/discord/utilities/images/MGImages;->imageEncoder:Lcom/discord/utilities/images/ImageEncoder;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getDrawee(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            ")",
            "Lcom/facebook/drawee/view/DraweeView<",
            "*>;"
        }
    .end annotation

    const-string v0, "null cannot be cast to non-null type com.facebook.drawee.view.DraweeView<*>"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/facebook/drawee/view/DraweeView;

    return-object p1
.end method

.method private final getHierarchy(Landroid/widget/ImageView;)Lcom/facebook/drawee/generic/GenericDraweeHierarchy;
    .locals 2

    invoke-direct {p0, p1}, Lcom/discord/utilities/images/MGImages;->getDrawee(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->hasHierarchy()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lf/g/g/f/a;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {v1, p1}, Lf/g/g/f/a;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1}, Lf/g/g/f/a;->a()Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type com.facebook.drawee.generic.GenericDraweeHierarchy"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    return-object p1
.end method

.method public static final getImageRequest(Ljava/lang/String;IIZ)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;
    .locals 3

    const-string/jumbo v0, "url"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v0

    sget-object v1, Lcom/facebook/imagepipeline/request/ImageRequest$c;->d:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iput-object v1, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    const-string v1, "requestBuilder"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    if-nez p3, :cond_1

    const/4 p3, 0x2

    const-string v2, "gif"

    invoke-static {p0, v2, v1, p3}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/facebook/imagepipeline/request/ImageRequest$b;->e:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    goto :goto_1

    :cond_1
    :goto_0
    sget-object p0, Lcom/facebook/imagepipeline/request/ImageRequest$b;->d:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    :goto_1
    iput-object p0, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->f:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    if-lez p1, :cond_2

    if-lez p2, :cond_2

    const/4 v1, 0x1

    :cond_2
    if-eqz v1, :cond_3

    new-instance p0, Lf/g/j/d/e;

    invoke-direct {p0, p1, p2}, Lf/g/j/d/e;-><init>(II)V

    iput-object p0, v0, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->c:Lf/g/j/d/e;

    :cond_3
    return-object v0
.end method

.method public static final prepareImageUpload(Landroid/net/Uri;Ljava/lang/String;Landroidx/fragment/app/FragmentManager;Lcom/miguelgaeta/media_picker/MediaPicker$Provider;Lrx/functions/Action1;Lcom/discord/dialogs/ImageUploadDialog$PreviewType;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroidx/fragment/app/FragmentManager;",
            "Lcom/miguelgaeta/media_picker/MediaPicker$Provider;",
            "Lrx/functions/Action1<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/discord/dialogs/ImageUploadDialog$PreviewType;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "uri"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mimeType"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "provider"

    invoke-static {p3, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "previewType"

    invoke-static {p5, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/discord/dialogs/ImageUploadDialog;->n:Lcom/discord/dialogs/ImageUploadDialog$b;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p5, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-direct {v0}, Lcom/discord/dialogs/ImageUploadDialog;-><init>()V

    const-string v1, "<set-?>"

    invoke-static {p0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p0, v0, Lcom/discord/dialogs/ImageUploadDialog;->h:Landroid/net/Uri;

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, v0, Lcom/discord/dialogs/ImageUploadDialog;->i:Lcom/miguelgaeta/media_picker/MediaPicker$Provider;

    iput-object p1, v0, Lcom/discord/dialogs/ImageUploadDialog;->j:Ljava/lang/String;

    iput-object p4, v0, Lcom/discord/dialogs/ImageUploadDialog;->k:Lrx/functions/Action1;

    iput-object p5, v0, Lcom/discord/dialogs/ImageUploadDialog;->l:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;

    const-class p0, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p2, p0}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public static final requestAvatarCrop(Landroid/content/Context;Lcom/miguelgaeta/media_picker/MediaPicker$Provider;Landroid/net/Uri;)V
    .locals 7

    const-string v0, "provider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputUri"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Lf/n/a/a;

    invoke-direct {v6}, Lf/n/a/a;-><init>()V

    const v0, 0x7f060057

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iget-object v2, v6, Lf/n/a/a;->a:Landroid/os/Bundle;

    const-string v3, "com.yalantis.ucrop.ToolbarColor"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const v1, 0x7f06005c

    invoke-static {p0, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iget-object v2, v6, Lf/n/a/a;->a:Landroid/os/Bundle;

    const-string v3, "com.yalantis.ucrop.StatusBarColor"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const v1, 0x7f060292

    invoke-static {p0, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iget-object v2, v6, Lf/n/a/a;->a:Landroid/os/Bundle;

    const-string v3, "com.yalantis.ucrop.UcropToolbarWidgetColor"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iget-object v1, v6, Lf/n/a/a;->a:Landroid/os/Bundle;

    const-string v2, "com.yalantis.ucrop.UcropColorWidgetActive"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Lcom/discord/utilities/images/MGImages$requestAvatarCrop$1;

    invoke-direct {v5, p0}, Lcom/discord/utilities/images/MGImages$requestAvatarCrop$1;-><init>(Landroid/content/Context;)V

    const/16 v3, 0x400

    const/16 v4, 0x400

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v1 .. v6}, Lcom/miguelgaeta/media_picker/MediaPicker;->startForImageCrop(Lcom/miguelgaeta/media_picker/MediaPicker$Provider;Landroid/net/Uri;IILcom/miguelgaeta/media_picker/MediaPicker$OnError;Lf/n/a/a;)V

    return-void
.end method

.method public static final requestDataUrl(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lrx/functions/Action1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lrx/functions/Action1<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_1

    const/16 v0, 0xc

    const/4 v1, 0x1

    const v2, 0x7f12028d

    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_1

    if-eqz p3, :cond_0

    :try_start_1
    sget-object v5, Lcom/discord/utilities/images/MGImages;->imageEncoder:Lcom/discord/utilities/images/ImageEncoder;

    const-string v6, "it"

    invoke-static {p1, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, p2, p1}, Lcom/discord/utilities/images/ImageEncoder;->getDataUrl(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, p2}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p2

    :try_start_2
    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p3

    :try_start_3
    invoke-static {p1, p2}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p3

    :cond_0
    :goto_0
    invoke-static {p1, v3}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    new-array p2, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p2, v4

    invoke-virtual {p0, v2, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v4, v3, v0}, Lf/a/b/p;->j(Landroid/content/Context;Ljava/lang/CharSequence;ILcom/discord/utilities/view/ToastManager;I)V

    goto :goto_1

    :catch_1
    move-exception p1

    new-array p2, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p2, v4

    invoke-virtual {p0, v2, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v4, v3, v0}, Lf/a/b/p;->j(Landroid/content/Context;Ljava/lang/CharSequence;ILcom/discord/utilities/view/ToastManager;I)V

    :cond_1
    :goto_1
    return-void
.end method

.method public static final setCornerRadius(Landroid/widget/ImageView;FZLjava/lang/Integer;)V
    .locals 5
    .param p3    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    sget-object v0, Lf/g/g/f/c$a;->d:Lf/g/g/f/c$a;

    const-string/jumbo v1, "view"

    invoke-static {p0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    new-instance p1, Lf/g/g/f/c;

    invoke-direct {p1}, Lf/g/g/f/c;-><init>()V

    iput-boolean v1, p1, Lf/g/g/f/c;->b:Z

    iput-object v0, p1, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    goto :goto_0

    :cond_0
    new-instance p2, Lf/g/g/f/c;

    invoke-direct {p2}, Lf/g/g/f/c;-><init>()V

    iget-object v2, p2, Lf/g/g/f/c;->c:[F

    if-nez v2, :cond_1

    const/16 v2, 0x8

    new-array v2, v2, [F

    iput-object v2, p2, Lf/g/g/f/c;->c:[F

    :cond_1
    iget-object v2, p2, Lf/g/g/f/c;->c:[F

    invoke-static {v2, p1}, Ljava/util/Arrays;->fill([FF)V

    move-object p1, p2

    :goto_0
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1c

    const-string v3, "roundingParams"

    if-ne p2, v2, :cond_2

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean v1, p1, Lf/g/g/f/c;->h:Z

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    iput p2, p1, Lf/g/g/f/c;->d:I

    iput-object v0, p1, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    :cond_3
    sget-object p2, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-direct {p2, p0}, Lcom/discord/utilities/images/MGImages;->getHierarchy(Landroid/widget/ImageView;)Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object p0

    iput-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    iget-object p2, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->d:Lf/g/g/f/b;

    sget-object p3, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    iget-object p3, p2, Lf/g/g/e/g;->d:Landroid/graphics/drawable/Drawable;

    iget-object v1, p1, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    if-ne v1, v0, :cond_5

    instance-of v0, p3, Lf/g/g/e/m;

    if-eqz v0, :cond_4

    check-cast p3, Lf/g/g/e/m;

    invoke-static {p3, p1}, Lf/g/g/f/d;->b(Lf/g/g/e/j;Lf/g/g/f/c;)V

    iget p1, p1, Lf/g/g/f/c;->d:I

    iput p1, p3, Lf/g/g/e/m;->r:I

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    goto :goto_1

    :cond_4
    sget-object p3, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p3}, Lf/g/g/e/g;->o(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-static {p3, p1}, Lf/g/g/f/d;->d(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p2, p1}, Lf/g/g/e/g;->o(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_5
    instance-of p1, p3, Lf/g/g/e/m;

    if-eqz p1, :cond_6

    check-cast p3, Lf/g/g/e/m;

    sget-object p1, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p3, p1}, Lf/g/g/e/g;->o(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-virtual {p2, p3}, Lf/g/g/e/g;->o(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_6
    :goto_1
    const/4 p1, 0x0

    const/4 p2, 0x0

    :goto_2
    iget-object p3, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    iget-object p3, p3, Lf/g/g/e/b;->f:[Landroid/graphics/drawable/Drawable;

    array-length p3, p3

    if-ge p2, p3, :cond_c

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->l(I)Lf/g/g/e/d;

    move-result-object p3

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    iget-object v1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    :goto_3
    invoke-interface {p3}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eq v2, p3, :cond_8

    instance-of v3, v2, Lf/g/g/e/d;

    if-nez v3, :cond_7

    goto :goto_4

    :cond_7
    move-object p3, v2

    check-cast p3, Lf/g/g/e/d;

    goto :goto_3

    :cond_8
    :goto_4
    invoke-interface {p3}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v0, :cond_a

    iget-object v3, v0, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    sget-object v4, Lf/g/g/f/c$a;->e:Lf/g/g/f/c$a;

    if-ne v3, v4, :cond_a

    instance-of v3, v2, Lf/g/g/e/j;

    if-eqz v3, :cond_9

    check-cast v2, Lf/g/g/e/j;

    invoke-static {v2, v0}, Lf/g/g/f/d;->b(Lf/g/g/e/j;Lf/g/g/f/c;)V

    goto :goto_5

    :cond_9
    if-eqz v2, :cond_b

    sget-object v3, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {p3, v3}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v0, v1}, Lf/g/g/f/d;->a(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {p3, v0}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_5

    :cond_a
    instance-of p3, v2, Lf/g/g/e/j;

    if-eqz p3, :cond_b

    check-cast v2, Lf/g/g/e/j;

    invoke-interface {v2, p1}, Lf/g/g/e/j;->c(Z)V

    const/4 p3, 0x0

    invoke-interface {v2, p3}, Lf/g/g/e/j;->j(F)V

    invoke-interface {v2, p1, p3}, Lf/g/g/e/j;->a(IF)V

    invoke-interface {v2, p3}, Lf/g/g/e/j;->i(F)V

    invoke-interface {v2, p1}, Lf/g/g/e/j;->f(Z)V

    invoke-interface {v2, p1}, Lf/g/g/e/j;->e(Z)V

    :cond_b
    :goto_5
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_c
    return-void
.end method

.method public static synthetic setCornerRadius$default(Landroid/widget/ImageView;FZLjava/lang/Integer;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x8

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/discord/utilities/images/MGImages;->setCornerRadius(Landroid/widget/ImageView;FZLjava/lang/Integer;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 9

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x7c

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;Ljava/lang/String;I)V
    .locals 9

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x78

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;Ljava/lang/String;II)V
    .locals 9

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x70

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;Ljava/lang/String;IIZ)V
    .locals 9

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x60

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Ljava/lang/String;",
            "IIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    const/16 v7, 0x40

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Ljava/lang/String;",
            "IIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/images/MGImages$ChangeDetector;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v6, p6

    const-string/jumbo v2, "view"

    invoke-static {p0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "changeDetector"

    invoke-static {v6, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, p0, p1}, Lcom/discord/utilities/images/MGImages$ChangeDetector;->track(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    if-eqz v1, :cond_2

    invoke-static {p1}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v7, 0x0

    const/16 v8, 0x80

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, v2

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    :goto_0
    sget-object v1, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-direct {v1, p0}, Lcom/discord/utilities/images/MGImages;->getDrawee(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    :goto_1
    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;)V
    .locals 10

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xfc

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;I)V
    .locals 10

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xf8

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;II)V
    .locals 10

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xf0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;IIZ)V
    .locals 10

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xe0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "[",
            "Ljava/lang/String;",
            "IIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "[",
            "Ljava/lang/String;",
            "IIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/images/MGImages$ChangeDetector;",
            ")V"
        }
    .end annotation

    const/4 v7, 0x0

    const/16 v8, 0x80

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method public static final setImage(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "[",
            "Ljava/lang/String;",
            "IIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/images/MGImages$ChangeDetector;",
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "Lcom/facebook/imagepipeline/image/ImageInfo;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "urls"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeDetector"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p6, p0, p1}, Lcom/discord/utilities/images/MGImages$ChangeDetector;->track(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p6

    if-nez p6, :cond_0

    return-void

    :cond_0
    array-length p6, p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p6, :cond_1

    const/4 p6, 0x1

    goto :goto_0

    :cond_1
    const/4 p6, 0x0

    :goto_0
    if-eqz p6, :cond_2

    sget-object p1, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-direct {p1, p0}, Lcom/discord/utilities/images/MGImages;->getDrawee(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    return-void

    :cond_2
    new-instance p6, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {p6, v2}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_4

    aget-object v4, p1, v3

    invoke-static {v4, p2, p3, p4}, Lcom/discord/utilities/images/MGImages;->getImageRequest(Ljava/lang/String;IIZ)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v4

    if-eqz p5, :cond_3

    invoke-interface {p5, v4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lkotlin/Unit;

    :cond_3
    invoke-virtual {v4}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v4

    invoke-interface {p6, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    new-array p1, v0, [Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-interface {p6, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, [Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-static {}, Lf/g/g/a/a/b;->a()Lf/g/g/a/a/d;

    move-result-object p2

    sget-object p3, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-direct {p3, p0}, Lcom/discord/utilities/images/MGImages;->getDrawee(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getController()Lcom/facebook/drawee/interfaces/DraweeController;

    move-result-object p4

    iput-object p4, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->k:Lcom/facebook/drawee/interfaces/DraweeController;

    iput-object p7, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->h:Lcom/facebook/drawee/controller/ControllerListener;

    iput-boolean v1, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->j:Z

    array-length p4, p1

    if-lez p4, :cond_5

    const/4 v0, 0x1

    :cond_5
    const-string p4, "No requests specified!"

    invoke-static {v0, p4}, Ls/a/b/b/a;->h(ZLjava/lang/Object;)V

    iput-object p1, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->f:[Ljava/lang/Object;

    iput-boolean v1, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->g:Z

    invoke-direct {p3, p0}, Lcom/discord/utilities/images/MGImages;->getDrawee(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;

    move-result-object p0

    invoke-virtual {p2}, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->a()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    return-void
.end method

.method public static synthetic setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V
    .locals 5

    and-int/lit8 v0, p7, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move v0, p2

    :goto_0
    and-int/lit8 v2, p7, 0x8

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    move v2, p3

    :goto_1
    and-int/lit8 v3, p7, 0x10

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    move v1, p4

    :goto_2
    and-int/lit8 v3, p7, 0x20

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    move-object v3, p5

    :goto_3
    and-int/lit8 v4, p7, 0x40

    if-eqz v4, :cond_4

    sget-object v4, Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;->INSTANCE:Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;

    goto :goto_4

    :cond_4
    move-object v4, p6

    :goto_4
    move-object p2, p0

    move-object p3, p1

    move p4, v0

    move p5, v2

    move p6, v1

    move-object p7, v3

    move-object p8, v4

    invoke-static/range {p2 .. p8}, Lcom/discord/utilities/images/MGImages;->setImage(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V

    return-void
.end method

.method public static synthetic setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V
    .locals 7

    move v0, p8

    and-int/lit8 v1, v0, 0x4

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    move v1, p2

    :goto_0
    and-int/lit8 v3, v0, 0x8

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    move v3, p3

    :goto_1
    and-int/lit8 v4, v0, 0x10

    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    move v2, p4

    :goto_2
    and-int/lit8 v4, v0, 0x20

    const/4 v5, 0x0

    if-eqz v4, :cond_3

    move-object v4, v5

    goto :goto_3

    :cond_3
    move-object v4, p5

    :goto_3
    and-int/lit8 v6, v0, 0x40

    if-eqz v6, :cond_4

    sget-object v6, Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;->INSTANCE:Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;

    goto :goto_4

    :cond_4
    move-object v6, p6

    :goto_4
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_5

    goto :goto_5

    :cond_5
    move-object v5, p7

    :goto_5
    move-object p2, p0

    move-object p3, p1

    move p4, v1

    move p5, v3

    move p6, v2

    move-object p7, v4

    move-object p8, v6

    move-object/from16 p9, v5

    invoke-static/range {p2 .. p9}, Lcom/discord/utilities/images/MGImages;->setImage(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;)V

    return-void
.end method

.method public static synthetic setImage$default(Lcom/discord/utilities/images/MGImages;Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;->INSTANCE:Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/images/MGImages;->setImage(Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;)V

    return-void
.end method

.method public static synthetic setImage$default(Lcom/discord/utilities/images/MGImages;Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;->INSTANCE:Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/images/MGImages;->setImage(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V

    return-void
.end method

.method public static synthetic setImage$default(Lcom/discord/utilities/images/MGImages;Landroid/widget/ImageView;Landroid/net/Uri;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;->INSTANCE:Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/images/MGImages;->setImage(Landroid/widget/ImageView;Landroid/net/Uri;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V

    return-void
.end method

.method public static final setScaleType(Landroid/widget/ImageView;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scaleType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-direct {v0, p0}, Lcom/discord/utilities/images/MGImages;->getHierarchy(Landroid/widget/ImageView;)Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->m(I)Lf/g/g/e/p;

    move-result-object p0

    invoke-virtual {p0, p1}, Lf/g/g/e/p;->r(Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V

    return-void
.end method


# virtual methods
.method public final centerBitmapInTransparentBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    .locals 3

    const-string/jumbo v0, "src"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-le p5, p3, :cond_0

    if-le p4, p2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {p5, p4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sub-int/2addr p5, p3

    int-to-float p5, p5

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr p5, v2

    sub-int/2addr p4, p2

    int-to-float p4, p4

    div-float/2addr p4, v2

    new-instance v2, Landroid/graphics/RectF;

    int-to-float p3, p3

    add-float/2addr p3, p5

    int-to-float p2, p2

    add-float/2addr p2, p4

    invoke-direct {v2, p5, p4, p3, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 p2, 0x0

    invoke-virtual {v1, p1, p2, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const-string p1, "dest"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot fit bitmap of size "

    const-string v1, " x "

    const-string v2, " inside "

    invoke-static {v0, p3, v1, p2, v2}, Lf/e/c/a/a;->J(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    const-string p3, "bitmap of size "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final setImage(Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;)V
    .locals 1
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeDetector"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, p1, v0}, Lcom/discord/utilities/images/MGImages$ChangeDetector;->track(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-direct {p0, p1}, Lcom/discord/utilities/images/MGImages;->getHierarchy(Landroid/widget/ImageView;)Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object p1

    iget-object p3, p1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p1, p3, p2}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->o(ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public final setImage(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeDetector"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, p1, p2}, Lcom/discord/utilities/images/MGImages$ChangeDetector;->track(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-direct {p0, p1}, Lcom/discord/utilities/images/MGImages;->getHierarchy(Landroid/widget/ImageView;)Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object p1

    const/4 p3, 0x1

    invoke-virtual {p1, p3, p2}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->o(ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public final setImage(Landroid/widget/ImageView;Landroid/net/Uri;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V
    .locals 9

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uri"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeDetector"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.resource"

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-static {p2}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/images/MGImages;->setImage(Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x3c

    const/4 v8, 0x0

    move-object v0, p1

    move-object v6, p3

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :goto_1
    return-void
.end method
