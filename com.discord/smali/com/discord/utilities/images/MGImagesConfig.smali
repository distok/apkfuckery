.class public final Lcom/discord/utilities/images/MGImagesConfig;
.super Ljava/lang/Object;
.source "MGImagesConfig.kt"


# static fields
.field private static final CACHE_DIR:Ljava/lang/String; = "app_images_cache"

.field private static final CACHE_DIR_SMALL:Ljava/lang/String; = "app_images_cache_small"

.field public static final INSTANCE:Lcom/discord/utilities/images/MGImagesConfig;

.field private static final MAX_BITMAP_MEM_CACHE_SIZE_RATIO:I = 0x3

.field private static final MAX_DISK_CACHE_SIZE:J = 0x2800000L


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/images/MGImagesConfig;

    invoke-direct {v0}, Lcom/discord/utilities/images/MGImagesConfig;-><init>()V

    sput-object v0, Lcom/discord/utilities/images/MGImagesConfig;->INSTANCE:Lcom/discord/utilities/images/MGImagesConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getAppBitmapMemoryCacheParamsSupplier(Landroid/content/Context;)Lcom/facebook/imagepipeline/cache/DefaultBitmapMemoryCacheParamsSupplier;
    .locals 1

    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type android.app.ActivityManager"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Landroid/app/ActivityManager;

    new-instance v0, Lcom/discord/utilities/images/MGImagesConfig$getAppBitmapMemoryCacheParamsSupplier$1;

    invoke-direct {v0, p1, p1}, Lcom/discord/utilities/images/MGImagesConfig$getAppBitmapMemoryCacheParamsSupplier$1;-><init>(Landroid/app/ActivityManager;Landroid/app/ActivityManager;)V

    return-object v0
.end method

.method private final newDiskCacheConfig(Landroid/content/Context;Ljava/lang/String;)Lcom/facebook/cache/disk/DiskCacheConfig;
    .locals 2

    new-instance v0, Lcom/facebook/cache/disk/DiskCacheConfig$b;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/facebook/cache/disk/DiskCacheConfig$b;-><init>(Landroid/content/Context;Lcom/facebook/cache/disk/DiskCacheConfig$a;)V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p1

    new-instance v1, Lf/g/d/d/k;

    invoke-direct {v1, p1}, Lf/g/d/d/k;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/facebook/cache/disk/DiskCacheConfig$b;->b:Lcom/facebook/common/internal/Supplier;

    iput-object p2, v0, Lcom/facebook/cache/disk/DiskCacheConfig$b;->a:Ljava/lang/String;

    const-wide/32 p1, 0x2800000

    iput-wide p1, v0, Lcom/facebook/cache/disk/DiskCacheConfig$b;->c:J

    new-instance p1, Lcom/facebook/cache/disk/DiskCacheConfig;

    invoke-direct {p1, v0}, Lcom/facebook/cache/disk/DiskCacheConfig;-><init>(Lcom/facebook/cache/disk/DiskCacheConfig$b;)V

    const-string p2, "DiskCacheConfig\n        \u2026HE_SIZE)\n        .build()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final init(Landroid/app/Application;)V
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/g/j/e/k$a;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lf/g/j/e/k$a;-><init>(Landroid/content/Context;Lf/g/j/e/j;)V

    const/4 v2, 0x1

    iput-boolean v2, v0, Lf/g/j/e/k$a;->c:Z

    const-string v3, "app_images_cache"

    invoke-direct {p0, p1, v3}, Lcom/discord/utilities/images/MGImagesConfig;->newDiskCacheConfig(Landroid/content/Context;Ljava/lang/String;)Lcom/facebook/cache/disk/DiskCacheConfig;

    move-result-object v3

    iput-object v3, v0, Lf/g/j/e/k$a;->d:Lcom/facebook/cache/disk/DiskCacheConfig;

    const-string v3, "app_images_cache_small"

    invoke-direct {p0, p1, v3}, Lcom/discord/utilities/images/MGImagesConfig;->newDiskCacheConfig(Landroid/content/Context;Ljava/lang/String;)Lcom/facebook/cache/disk/DiskCacheConfig;

    move-result-object v3

    iput-object v3, v0, Lf/g/j/e/k$a;->e:Lcom/facebook/cache/disk/DiskCacheConfig;

    invoke-direct {p0, p1}, Lcom/discord/utilities/images/MGImagesConfig;->getAppBitmapMemoryCacheParamsSupplier(Landroid/content/Context;)Lcom/facebook/imagepipeline/cache/DefaultBitmapMemoryCacheParamsSupplier;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v3, v0, Lf/g/j/e/k$a;->a:Lcom/facebook/common/internal/Supplier;

    iget-object v0, v0, Lf/g/j/e/k$a;->f:Lf/g/j/e/l$b;

    iput-boolean v2, v0, Lf/g/j/e/l$b;->b:Z

    iget-object v0, v0, Lf/g/j/e/l$b;->a:Lf/g/j/e/k$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lf/g/j/e/k;

    invoke-direct {v3, v0, v1}, Lf/g/j/e/k;-><init>(Lf/g/j/e/k$a;Lf/g/j/e/j;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    sget-boolean v0, Lf/g/g/a/a/b;->b:Z

    if-eqz v0, :cond_0

    const-class v0, Lf/g/g/a/a/b;

    const-string v4, "Fresco has already been initialized! `Fresco.initialize(...)` should only be called 1 single time to avoid memory leaks!"

    invoke-static {v0, v4}, Lf/g/d/e/a;->k(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sput-boolean v2, Lf/g/g/a/a/b;->b:Z

    :goto_0
    sput-boolean v2, Lf/g/j/e/n;->a:Z

    invoke-static {}, Lf/g/m/n/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    :try_start_0
    const-string v0, "com.facebook.imagepipeline.nativecode.NativeCodeInitializer"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v4, "init"

    new-array v5, v2, [Ljava/lang/Class;

    const-class v6, Landroid/content/Context;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    :try_start_1
    new-instance v0, Lf/g/m/n/c;

    invoke-direct {v0}, Lf/g/m/n/c;-><init>()V

    invoke-static {v0}, Lf/g/m/n/a;->a(Lf/g/m/n/b;)V

    goto :goto_1

    :catch_1
    new-instance v0, Lf/g/m/n/c;

    invoke-direct {v0}, Lf/g/m/n/c;-><init>()V

    invoke-static {v0}, Lf/g/m/n/a;->a(Lf/g/m/n/b;)V

    goto :goto_1

    :catch_2
    new-instance v0, Lf/g/m/n/c;

    invoke-direct {v0}, Lf/g/m/n/c;-><init>()V

    invoke-static {v0}, Lf/g/m/n/a;->a(Lf/g/m/n/b;)V

    goto :goto_1

    :catch_3
    new-instance v0, Lf/g/m/n/c;

    invoke-direct {v0}, Lf/g/m/n/c;-><init>()V

    invoke-static {v0}, Lf/g/m/n/a;->a(Lf/g/m/n/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    goto :goto_3

    :goto_2
    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1

    :cond_1
    :goto_3
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v3}, Lf/g/j/e/m;->j(Lf/g/j/e/k;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v0, Lf/g/g/a/a/e;

    invoke-direct {v0, p1}, Lf/g/g/a/a/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lf/g/g/a/a/b;->a:Lf/g/g/a/a/e;

    invoke-static {v0}, Lcom/facebook/drawee/view/SimpleDraweeView;->initialize(Lcom/facebook/common/internal/Supplier;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    const/16 v0, 0x28

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3c

    if-eq p1, v0, :cond_0

    const/16 v0, 0x50

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lf/g/j/e/m;->t:Lf/g/j/e/m;

    const-string v0, "ImagePipelineFactory was not initialized!"

    invoke-static {p1, v0}, Ls/a/b/b/a;->i(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lf/g/j/e/m;->a()Lf/g/j/e/i;

    move-result-object v0

    iput-object v0, p1, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    :cond_1
    iget-object p1, p1, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    new-instance v0, Lf/g/j/e/h;

    invoke-direct {v0, p1}, Lf/g/j/e/h;-><init>(Lf/g/j/e/i;)V

    iget-object v1, p1, Lf/g/j/e/i;->e:Lf/g/j/c/t;

    invoke-interface {v1, v0}, Lf/g/j/c/t;->b(Lf/g/d/d/j;)I

    iget-object p1, p1, Lf/g/j/e/i;->f:Lf/g/j/c/t;

    invoke-interface {p1, v0}, Lf/g/j/c/t;->b(Lf/g/d/d/j;)I

    :goto_0
    return-void
.end method
