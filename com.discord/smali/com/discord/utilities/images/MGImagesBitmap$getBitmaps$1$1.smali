.class public final Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1$1;
.super Ljava/lang/Object;
.source "MGImagesBitmap.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1;->call(Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Landroid/graphics/Bitmap;",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/String;",
        "+",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $imageRequest:Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1$1;->$imageRequest:Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1$1;->call(Landroid/graphics/Bitmap;)Lkotlin/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final call(Landroid/graphics/Bitmap;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmaps$1$1;->$imageRequest:Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;

    invoke-virtual {v0}, Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;->getImageUri()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
