.class public final Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;
.super Lf/g/j/f/c;
.source "MGImagesBitmap.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->call(Lrx/Subscriber;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $emitter:Lrx/Subscriber;

.field public final synthetic this$0:Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;Lrx/Subscriber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->this$0:Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;

    iput-object p2, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->$emitter:Lrx/Subscriber;

    invoke-direct {p0}, Lf/g/j/f/c;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailureImpl(Lcom/facebook/datasource/DataSource;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "dataSource"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/facebook/datasource/DataSource;->d()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "404"

    invoke-static {v0, v3, v1, v2}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->$emitter:Lrx/Subscriber;

    new-instance v0, Lcom/discord/utilities/images/MGImagesBitmap$ImageNotFoundException;

    iget-object v1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->this$0:Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;

    iget-object v1, v1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->$imageUri:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/discord/utilities/images/MGImagesBitmap$ImageNotFoundException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->$emitter:Lrx/Subscriber;

    invoke-interface {p1}, Lcom/facebook/datasource/DataSource;->d()Ljava/lang/Throwable;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/utilities/images/MGImagesBitmap$DecodeException;

    iget-object v1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->this$0:Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;

    iget-object v1, v1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->$imageUri:Ljava/lang/String;

    invoke-direct {p1, v1}, Lcom/discord/utilities/images/MGImagesBitmap$DecodeException;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public onNewResultImpl(Landroid/graphics/Bitmap;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->$emitter:Lrx/Subscriber;

    invoke-static {p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->$emitter:Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->$emitter:Lrx/Subscriber;

    new-instance v0, Lcom/discord/utilities/images/MGImagesBitmap$MissingBitmapException;

    iget-object v1, p0, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1$1;->this$0:Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;

    iget-object v1, v1, Lcom/discord/utilities/images/MGImagesBitmap$getBitmap$1;->$imageUri:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/discord/utilities/images/MGImagesBitmap$MissingBitmapException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
