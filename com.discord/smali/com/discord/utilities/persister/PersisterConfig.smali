.class public final Lcom/discord/utilities/persister/PersisterConfig;
.super Ljava/lang/Object;
.source "PersisterConfig.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/persister/PersisterConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/persister/PersisterConfig;

    invoke-direct {v0}, Lcom/discord/utilities/persister/PersisterConfig;-><init>()V

    sput-object v0, Lcom/discord/utilities/persister/PersisterConfig;->INSTANCE:Lcom/discord/utilities/persister/PersisterConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getPersistenceStrategy()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/discord/utilities/persister/PersisterConfig;->isNotActive()Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v1}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/persister/PersisterConfig$persistenceStrategy$1;->INSTANCE:Lcom/discord/utilities/persister/PersisterConfig$persistenceStrategy$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/Observable;->E(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .merg\u2026  .map { true }\n        )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final isNotActive()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->get()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/Observable;->N(I)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/b/o;->c:Lf/a/b/o;

    sget-object v1, Lf/a/b/o;->b:Lrx/subjects/BehaviorSubject;

    sget-object v2, Lf/a/b/n;->d:Lf/a/b/n;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    const-string v2, "numGatewayConnectionCons\u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/utilities/persister/PersisterConfig$isNotActive$1;->INSTANCE:Lcom/discord/utilities/persister/PersisterConfig$isNotActive$1;

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final init(Landroid/content/Context;Lcom/discord/utilities/time/Clock;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/persister/Persister;->Companion:Lcom/discord/utilities/persister/Persister$Companion;

    sget-object v1, Lcom/discord/utilities/persister/PersisterConfig$init$1;->INSTANCE:Lcom/discord/utilities/persister/PersisterConfig$init$1;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister$Companion;->setKryoConfig(Lkotlin/jvm/functions/Function1;)V

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "[Persister]"

    const-string/jumbo v2, "tag"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/a/b/h;

    invoke-direct {v2, v1}, Lf/a/b/h;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/persister/Persister$Companion;->setLogger(Lkotlin/jvm/functions/Function3;)V

    invoke-direct {p0}, Lcom/discord/utilities/persister/PersisterConfig;->getPersistenceStrategy()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/utilities/persister/Persister$Companion;->init(Landroid/content/Context;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V

    return-void
.end method
