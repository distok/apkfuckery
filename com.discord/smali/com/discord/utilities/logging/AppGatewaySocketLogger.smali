.class public final Lcom/discord/utilities/logging/AppGatewaySocketLogger;
.super Ljava/lang/Object;
.source "AppGatewaySocketLogger.kt"

# interfaces
.implements Lcom/discord/gateway/GatewaySocketLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;

.field private static final INSTANCE:Lcom/discord/utilities/logging/AppGatewaySocketLogger;


# instance fields
.field private final logLevel:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;->Companion:Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;

    new-instance v0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;

    invoke-direct {v0}, Lcom/discord/utilities/logging/AppGatewaySocketLogger;-><init>()V

    sput-object v0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;->INSTANCE:Lcom/discord/utilities/logging/AppGatewaySocketLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/discord/gateway/GatewaySocketLogger$LogLevel;->NONE:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    iput-object v0, p0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;->logLevel:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/discord/utilities/logging/AppGatewaySocketLogger;
    .locals 1

    sget-object v0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;->INSTANCE:Lcom/discord/utilities/logging/AppGatewaySocketLogger;

    return-object v0
.end method


# virtual methods
.method public getLogLevel()Lcom/discord/gateway/GatewaySocketLogger$LogLevel;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;->logLevel:Lcom/discord/gateway/GatewaySocketLogger$LogLevel;

    return-object v0
.end method

.method public logInboundMessage(Ljava/lang/String;)V
    .locals 1

    const-string v0, "rawMessage"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public logMessageInflateFailed(Ljava/lang/Throwable;)V
    .locals 1

    const-string/jumbo v0, "throwable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public logOutboundMessage(Ljava/lang/String;)V
    .locals 1

    const-string v0, "rawMessage"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
