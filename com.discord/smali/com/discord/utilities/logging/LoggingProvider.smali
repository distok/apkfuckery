.class public final Lcom/discord/utilities/logging/LoggingProvider;
.super Ljava/lang/Object;
.source "LoggingProvider.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/logging/LoggingProvider;

.field private static logger:Lcom/discord/utilities/logging/Logger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/logging/LoggingProvider;

    invoke-direct {v0}, Lcom/discord/utilities/logging/LoggingProvider;-><init>()V

    sput-object v0, Lcom/discord/utilities/logging/LoggingProvider;->INSTANCE:Lcom/discord/utilities/logging/LoggingProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Lcom/discord/utilities/logging/Logger;
    .locals 1

    sget-object v0, Lcom/discord/utilities/logging/LoggingProvider;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "logger"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final init(Lcom/discord/utilities/logging/Logger;)V
    .locals 1

    const-string v0, "logger"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object p1, Lcom/discord/utilities/logging/LoggingProvider;->logger:Lcom/discord/utilities/logging/Logger;

    return-void
.end method
