.class public final synthetic Lcom/discord/utilities/presence/PresenceUtils$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/discord/models/domain/ModelPresence$Status;->values()[Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/utilities/presence/PresenceUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/models/domain/ModelPresence$Status;->ONLINE:Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v1, Lcom/discord/models/domain/ModelPresence$Status;->IDLE:Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v1, 0x2

    aput v1, v0, v2

    sget-object v2, Lcom/discord/models/domain/ModelPresence$Status;->DND:Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v2, 0x3

    aput v2, v0, v1

    return-void
.end method
