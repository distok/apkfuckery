.class public final Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;
.super Lx/m/c/k;
.source "PresenceUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/presence/PresenceUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/activity/ModelActivity;",
        "Ljava/lang/Comparable<",
        "*>;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;

    invoke-direct {v0}, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ")",
            "Ljava/lang/Comparable<",
            "*>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;->invoke(Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/Comparable;

    move-result-object p1

    return-object p1
.end method
