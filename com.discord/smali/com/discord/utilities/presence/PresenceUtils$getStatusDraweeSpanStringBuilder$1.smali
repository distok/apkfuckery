.class public final Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;
.super Ljava/lang/Object;
.source "PresenceUtils.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/presence/PresenceUtils;->getStatusDraweeSpanStringBuilder(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZZ)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $animateCustomStatusEmoji:Z

.field public final synthetic $context:Landroid/content/Context;

.field private final context:Landroid/content/Context;

.field private final isAnimationEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;->$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;->$animateCustomStatusEmoji:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;->context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;->isAnimationEnabled:Z

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isAnimationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;->isAnimationEnabled:Z

    return v0
.end method
