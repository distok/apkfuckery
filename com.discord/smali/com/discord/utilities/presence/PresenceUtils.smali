.class public final Lcom/discord/utilities/presence/PresenceUtils;
.super Ljava/lang/Object;
.source "PresenceUtils.kt"


# static fields
.field private static final ACTIVITY_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/discord/utilities/presence/PresenceUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/discord/utilities/presence/PresenceUtils;

    invoke-direct {v0}, Lcom/discord/utilities/presence/PresenceUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/presence/PresenceUtils;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils;

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/jvm/functions/Function1;

    sget-object v1, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$1;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$2;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$2;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$3;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils$ACTIVITY_COMPARATOR$3;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lf/h/a/f/f/n/g;->compareBy([Lkotlin/jvm/functions/Function1;)Ljava/util/Comparator;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/presence/PresenceUtils;->ACTIVITY_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getActivityHeader(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/CharSequence;
    .locals 5

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v0

    const v1, 0x7f121932

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_3

    const/4 v4, 0x2

    if-eq v0, v4, :cond_2

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    const/4 v4, 0x5

    if-eq v0, v4, :cond_0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_1

    :cond_0
    const v0, 0x7f12192f

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_1
    const v0, 0x7f121936

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_2
    const v0, 0x7f121930

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_3
    const v0, 0x7f121931

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getPlatform()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_6

    sget-object v0, Lcom/discord/utilities/platform/Platform;->Companion:Lcom/discord/utilities/platform/Platform$Companion;

    const-string v4, "serverPlatformName"

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/utilities/platform/Platform$Companion;->from(Ljava/lang/String;)Lcom/discord/utilities/platform/Platform;

    move-result-object v0

    sget-object v4, Lcom/discord/utilities/platform/Platform;->NONE:Lcom/discord/utilities/platform/Platform;

    if-ne v0, v4, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/discord/utilities/platform/Platform;->getProperName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_6

    const v0, 0x7f121933

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_6

    move-object p0, p1

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_1
    const-string/jumbo p1, "when (activity.type) {\n \u2026ity_header_playing)\n    }"

    invoke-static {p0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/discord/utilities/textprocessing/Parsers;->parseBoldMarkdown(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private final getActivityString(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/CharSequence;
    .locals 5

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_7

    if-eq v1, v3, :cond_5

    const/4 v4, 0x2

    if-eq v1, v4, :cond_4

    const/4 v4, 0x3

    if-eq v1, v4, :cond_2

    const/4 v4, 0x5

    if-eq v1, v4, :cond_1

    move-object p1, v0

    goto :goto_2

    :cond_1
    const v1, 0x7f1204ea

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v3, v2

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_2
    const v1, 0x7f121a71

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v4

    :goto_0
    aput-object v4, v3, v2

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_4
    const v1, 0x7f120f25

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v3, v2

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_5
    const v1, 0x7f1217a0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    goto :goto_1

    :cond_6
    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v4

    :goto_1
    aput-object v4, v3, v2

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_7
    const v1, 0x7f1212cf

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v3, v2

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_2
    if-eqz p1, :cond_8

    invoke-static {p1}, Lcom/discord/utilities/textprocessing/Parsers;->parseBoldMarkdown(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_8
    return-object v0
.end method

.method private final getApplicationStreamingString(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;)Ljava/lang/CharSequence;
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPresence;->getPlayingActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p2

    if-eqz p2, :cond_0

    const v0, 0x7f1217a0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const p2, 0x7f1217a1

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    :goto_0
    const-string p1, "presence?.playingActivit\u2026.string.streaming_a_game)"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/textprocessing/Parsers;->parseBoldMarkdown(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final getStatusDraweeSpanStringBuilder(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZZ)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 6

    new-instance v0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-direct {v0}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;-><init>()V

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPresence;->getCustomStatusActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivity;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v3, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const-string v4, "it"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v3, v2, v4, v5, v1}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->from$default(Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;Lcom/discord/models/domain/ModelMessageReaction$Emoji;IILjava/lang/Object;)Lcom/discord/utilities/textprocessing/node/EmojiNode;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    new-instance v2, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;

    invoke-direct {v2, p1, p5}, Lcom/discord/utilities/presence/PresenceUtils$getStatusDraweeSpanStringBuilder$1;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v1, v0, v2}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/presence/PresenceUtils;->getStatusText(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZ)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz v1, :cond_2

    const/16 p2, 0x2002

    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_2
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    return-object v0
.end method

.method public static synthetic getStatusDraweeSpanStringBuilder$default(Lcom/discord/utilities/presence/PresenceUtils;Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZZILjava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 7

    and-int/lit8 p7, p6, 0x8

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p4

    :goto_0
    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    move v6, p5

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/presence/PresenceUtils;->getStatusDraweeSpanStringBuilder(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZZ)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object p0

    return-object p0
.end method

.method private final getStatusText(Lcom/discord/models/domain/ModelPresence;)I
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    :goto_1
    const p1, 0x7f1216f0

    goto :goto_2

    :cond_2
    const p1, 0x7f1216eb

    goto :goto_2

    :cond_3
    const p1, 0x7f1216ed

    goto :goto_2

    :cond_4
    const p1, 0x7f1216f1

    :goto_2
    return p1
.end method

.method private final getStatusText(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZ)Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPresence;->getCustomStatusActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    return-object v1

    :cond_1
    if-eqz p3, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/presence/PresenceUtils;->getApplicationStreamingString(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p3

    goto :goto_1

    :cond_3
    move-object p3, v0

    :goto_1
    invoke-direct {p0, p1, p3}, Lcom/discord/utilities/presence/PresenceUtils;->getActivityString(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/CharSequence;

    move-result-object p3

    if-eqz p3, :cond_4

    return-object p3

    :cond_4
    if-eqz p4, :cond_5

    invoke-direct {p0, p2}, Lcom/discord/utilities/presence/PresenceUtils;->getStatusText(Lcom/discord/models/domain/ModelPresence;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_5
    return-object v0
.end method

.method public static synthetic getStatusText$default(Lcom/discord/utilities/presence/PresenceUtils;Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZILjava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/presence/PresenceUtils;->getStatusText(Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZ)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static final setPresenceText(Lcom/discord/models/domain/ModelPresence;ZLcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Z)V
    .locals 9

    const-string/jumbo v0, "textView"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/utilities/presence/PresenceUtils;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils;

    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v0, "textView.context"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v3, p0

    move v4, p1

    move v5, p3

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/presence/PresenceUtils;->getStatusDraweeSpanStringBuilder$default(Lcom/discord/utilities/presence/PresenceUtils;Landroid/content/Context;Lcom/discord/models/domain/ModelPresence;ZZZILjava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result p0

    const/4 p1, 0x0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const/16 p1, 0x8

    :goto_1
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public static synthetic setPresenceText$default(Lcom/discord/models/domain/ModelPresence;ZLcom/discord/utilities/view/text/SimpleDraweeSpanTextView;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x8

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/discord/utilities/presence/PresenceUtils;->setPresenceText(Lcom/discord/models/domain/ModelPresence;ZLcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Z)V

    return-void
.end method


# virtual methods
.method public final getACTIVITY_COMPARATOR$app_productionDiscordExternalRelease()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/presence/PresenceUtils;->ACTIVITY_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public final getStatusStringResForPresence(Lcom/discord/models/domain/ModelPresence;)I
    .locals 1
    .annotation build Landroidx/annotation/StringRes;
    .end annotation

    const-string v0, "presence"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/presence/PresenceUtils;->getStatusText(Lcom/discord/models/domain/ModelPresence;)I

    move-result p1

    return p1
.end method

.method public final shouldShowRichPresenceIcon(Lcom/discord/models/domain/ModelPresence;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivity;->isRichPresence()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 p1, 0x1

    :goto_0
    if-ne p1, v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    return v0
.end method
