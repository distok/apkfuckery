.class public final Lcom/discord/utilities/duration/DurationUtilsKt;
.super Ljava/lang/Object;
.source "DurationUtils.kt"


# static fields
.field private static final FEW_SECONDS_BOUNDARY:I = 0x1e


# direct methods
.method public static final humanizeDuration(Landroid/content/Context;J)Ljava/lang/String;
    .locals 8

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-wide/16 v1, 0x7530

    cmp-long v3, p1, v1

    if-gtz v3, :cond_0

    const p1, 0x7f120ce8

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string p1, "context.getString(\n     \u2026ation_a_few_seconds\n    )"

    invoke-static {p0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_0
    const-wide/32 v1, 0xea60

    const-string p0, "resources.getString(\n   \u2026LLIS).toInt()\n    )\n    )"

    const/4 v3, 0x0

    const/4 v4, 0x1

    cmp-long v5, p1, v1

    if-gtz v5, :cond_1

    const v1, 0x7f120cec

    new-array v2, v4, [Ljava/lang/Object;

    const v5, 0x7f1000a2

    const-wide/16 v6, 0x3e8

    div-long/2addr p1, v6

    long-to-int p2, p1

    new-array p1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v3

    invoke-virtual {v0, v5, p2, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    move-object p0, p1

    goto :goto_1

    :cond_1
    const-wide/32 v5, 0x36ee80

    cmp-long v7, p1, v5

    if-gtz v7, :cond_2

    const v5, 0x7f120ceb

    new-array v6, v4, [Ljava/lang/Object;

    const v7, 0x7f1000a1

    div-long/2addr p1, v1

    long-to-int p2, p1

    new-array p1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v3

    invoke-virtual {v0, v7, p2, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v3

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-wide/32 v1, 0x5265c00

    cmp-long v7, p1, v1

    if-gtz v7, :cond_3

    const v1, 0x7f120cea

    new-array v2, v4, [Ljava/lang/Object;

    const v7, 0x7f1000a0

    div-long/2addr p1, v5

    long-to-int p2, p1

    new-array p1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v3

    invoke-virtual {v0, v7, p2, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const v5, 0x7f120ce9

    new-array v6, v4, [Ljava/lang/Object;

    const v7, 0x7f10009f

    div-long/2addr p1, v1

    long-to-int p2, p1

    new-array p1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v3

    invoke-virtual {v0, v7, p2, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v3

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :goto_1
    return-object p0
.end method
