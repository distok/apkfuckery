.class public final Lcom/discord/utilities/channel/ChannelSelector$Companion;
.super Ljava/lang/Object;
.source "ChannelSelector.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/channel/ChannelSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/channel/ChannelSelector$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInstance()Lcom/discord/utilities/channel/ChannelSelector;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/channel/ChannelSelector;->access$getINSTANCE$cp()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "INSTANCE"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final init(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/channel/ChannelSelector;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/utilities/channel/ChannelSelector;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    invoke-static {v0}, Lcom/discord/utilities/channel/ChannelSelector;->access$setINSTANCE$cp(Lcom/discord/utilities/channel/ChannelSelector;)V

    return-void
.end method
