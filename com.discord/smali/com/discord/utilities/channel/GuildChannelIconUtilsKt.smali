.class public final Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;
.super Ljava/lang/Object;
.source "GuildChannelIconUtils.kt"


# direct methods
.method public static final getChannelType(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/utilities/channel/GuildChannelIconType;
    .locals 4

    if-nez p0, :cond_0

    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$Text;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Text;

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    const/4 v1, 0x5

    const-wide/16 v2, 0x400

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Text;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Text;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelPermissionOverwrite;

    invoke-static {p0, v2, v3}, Lcom/discord/models/domain/ModelPermissionOverwrite;->denies(Lcom/discord/models/domain/ModelPermissionOverwrite;J)Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Text;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Text;

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$Text;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Text;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Announcements;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Announcements;

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelPermissionOverwrite;

    invoke-static {p0, v2, v3}, Lcom/discord/models/domain/ModelPermissionOverwrite;->denies(Lcom/discord/models/domain/ModelPermissionOverwrite;J)Z

    move-result p0

    if-eqz p0, :cond_5

    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Announcements;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Announcements;

    goto :goto_0

    :cond_5
    sget-object p0, Lcom/discord/utilities/channel/GuildChannelIconType$Announcements;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Announcements;

    :goto_0
    return-object p0
.end method

.method public static final guildChannelIcon(Lcom/discord/models/domain/ModelChannel;)I
    .locals 0

    invoke-static {p0}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->getChannelType(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object p0

    invoke-static {p0}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->mapGuildChannelTypeToIcon(Lcom/discord/utilities/channel/GuildChannelIconType;)I

    move-result p0

    return p0
.end method

.method public static final mapGuildChannelTypeToIcon(Lcom/discord/utilities/channel/GuildChannelIconType;)I
    .locals 1
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    const-string v0, "guildChannelIconType"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/channel/GuildChannelIconType$Text;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Text;

    invoke-static {p0, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p0, 0x7f080296

    goto :goto_0

    :cond_0
    instance-of v0, p0, Lcom/discord/utilities/channel/GuildChannelIconType$Announcements;

    if-eqz v0, :cond_1

    const p0, 0x7f080291

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Text;

    if-eqz v0, :cond_2

    const p0, 0x7f08029b

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Announcements;

    if-eqz v0, :cond_3

    const p0, 0x7f080293

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Text;

    if-eqz v0, :cond_4

    const p0, 0x7f08029a

    goto :goto_0

    :cond_4
    instance-of p0, p0, Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Announcements;

    if-eqz p0, :cond_5

    const p0, 0x7f080292

    :goto_0
    return p0

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
