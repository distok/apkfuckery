.class public final Lcom/discord/utilities/channel/ChannelUtils$Companion;
.super Ljava/lang/Object;
.source "ChannelUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/channel/ChannelUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;-><init>()V

    return-void
.end method

.method public static synthetic getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final getINSTANCE()Lcom/discord/utilities/channel/ChannelUtils;
    .locals 2

    invoke-static {}, Lcom/discord/utilities/channel/ChannelUtils;->access$getINSTANCE$cp()Lkotlin/Lazy;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/channel/ChannelUtils;

    return-object v0
.end method


# virtual methods
.method public final get()Lcom/discord/utilities/channel/ChannelUtils;
    .locals 1

    invoke-direct {p0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getINSTANCE()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v0

    return-object v0
.end method

.method public final getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$getDisplayName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result p1

    invoke-virtual {v0, p2, v1, p1, p3}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName(Landroid/content/Context;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
