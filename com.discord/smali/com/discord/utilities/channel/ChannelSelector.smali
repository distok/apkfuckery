.class public final Lcom/discord/utilities/channel/ChannelSelector;
.super Ljava/lang/Object;
.source "ChannelSelector.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/channel/ChannelSelector$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

.field private static INSTANCE:Lcom/discord/utilities/channel/ChannelSelector;


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final stream:Lcom/discord/stores/StoreStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/channel/ChannelSelector$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/channel/ChannelSelector$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/channel/ChannelSelector;->stream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/utilities/channel/ChannelSelector;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/utilities/channel/ChannelSelector;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/discord/utilities/channel/ChannelSelector;
    .locals 1

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->INSTANCE:Lcom/discord/utilities/channel/ChannelSelector;

    return-object v0
.end method

.method public static final synthetic access$gotoChannel(Lcom/discord/utilities/channel/ChannelSelector;JJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/channel/ChannelSelector;->gotoChannel(JJ)V

    return-void
.end method

.method public static final synthetic access$setINSTANCE$cp(Lcom/discord/utilities/channel/ChannelSelector;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/channel/ChannelSelector;->INSTANCE:Lcom/discord/utilities/channel/ChannelSelector;

    return-void
.end method

.method public static synthetic findAndSetDirectMessage$default(Lcom/discord/utilities/channel/ChannelSelector;Landroid/content/Context;JILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const-wide/16 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/channel/ChannelSelector;->findAndSetDirectMessage(Landroid/content/Context;J)V

    return-void
.end method

.method public static final getInstance()Lcom/discord/utilities/channel/ChannelSelector;
    .locals 1

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v0

    return-object v0
.end method

.method private final gotoChannel(JJ)V
    .locals 8

    iget-object v0, p0, Lcom/discord/utilities/channel/ChannelSelector;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/utilities/channel/ChannelSelector$gotoChannel$1;

    move-object v1, v7

    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/channel/ChannelSelector$gotoChannel$1;-><init>(Lcom/discord/utilities/channel/ChannelSelector;JJ)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic selectChannel$default(Lcom/discord/utilities/channel/ChannelSelector;JJIILjava/lang/Object;)V
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(JJI)V

    return-void
.end method


# virtual methods
.method public final findAndSet(J)V
    .locals 11

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/discord/utilities/channel/ChannelSelector;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x2

    new-array v4, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/discord/utilities/channel/ChannelSelector;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/discord/utilities/channel/ChannelSelector;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getPermissions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePermissions;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$1;

    invoke-direct {v8, p0, p1, p2}, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$1;-><init>(Lcom/discord/utilities/channel/ChannelSelector;J)V

    const/16 v9, 0xe

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$2;->INSTANCE:Lcom/discord/utilities/channel/ChannelSelector$findAndSet$2;

    invoke-virtual {p1, p2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string p1, "observationDeck\n        \u2026   .filter { it != null }"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/utilities/channel/ChannelSelector;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v6, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$3;

    invoke-direct {v6, p0}, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$3;-><init>(Lcom/discord/utilities/channel/ChannelSelector;)V

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final findAndSetDirectMessage(Landroid/content/Context;J)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/discord/utilities/channel/ChannelUtils;->createPrivateChannel(J)Lrx/Observable;

    move-result-object p2

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/utilities/channel/ChannelSelector;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/utilities/channel/ChannelSelector$findAndSetDirectMessage$1;

    invoke-direct {v6, p0}, Lcom/discord/utilities/channel/ChannelSelector$findAndSetDirectMessage$1;-><init>(Lcom/discord/utilities/channel/ChannelSelector;)V

    const/16 v7, 0x1c

    const/4 v8, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getDispatcher()Lcom/discord/stores/Dispatcher;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/channel/ChannelSelector;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object v0
.end method

.method public final getObservationDeck()Lcom/discord/stores/updates/ObservationDeck;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/channel/ChannelSelector;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    return-object v0
.end method

.method public final getStream()Lcom/discord/stores/StoreStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/channel/ChannelSelector;->stream:Lcom/discord/stores/StoreStream;

    return-object v0
.end method

.method public final selectChannel(JJI)V
    .locals 23

    move-object/from16 v7, p0

    move-wide/from16 v5, p3

    iget-object v0, v7, Lcom/discord/utilities/channel/ChannelSelector;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getNavigation$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNavigation;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreNavigation$PanelAction;->CLOSE:Lcom/discord/stores/StoreNavigation$PanelAction;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/stores/StoreNavigation;->setNavigationPanelAction$default(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;ILjava/lang/Object;)V

    const-wide/16 v0, 0x0

    cmp-long v3, p1, v0

    if-nez v3, :cond_0

    cmp-long v3, v5, v0

    if-nez v3, :cond_0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, v2}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string v1, "Observable.just(null)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/utilities/channel/ChannelSelector;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$1;

    move-object v0, v14

    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    move-wide/from16 v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$1;-><init>(Lcom/discord/utilities/channel/ChannelSelector;JJ)V

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, v7, Lcom/discord/utilities/channel/ChannelSelector;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v8

    const-string v0, "filter { it != null }.map { it!! }"

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, 0x0

    invoke-static/range {v8 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v14

    const-class v15, Lcom/discord/utilities/channel/ChannelSelector;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v20, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move/from16 v2, p5

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;-><init>(Lcom/discord/utilities/channel/ChannelSelector;IJJ)V

    const/16 v21, 0x1e

    const/16 v22, 0x0

    invoke-static/range {v14 .. v22}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final selectChannel(Lcom/discord/models/domain/ModelChannel;)V
    .locals 8

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "channel?.guildId ?: return"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v7

    move-object v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(JJI)V

    :cond_0
    return-void
.end method
