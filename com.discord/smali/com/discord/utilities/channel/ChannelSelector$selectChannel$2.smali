.class public final Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;
.super Lx/m/c/k;
.source "ChannelSelector.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(JJI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $guildId:J

.field public final synthetic $type:I

.field public final synthetic this$0:Lcom/discord/utilities/channel/ChannelSelector;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/channel/ChannelSelector;IJJ)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->this$0:Lcom/discord/utilities/channel/ChannelSelector;

    iput p2, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$type:I

    iput-wide p3, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$guildId:J

    iput-wide p5, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$channelId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->invoke(Lcom/discord/models/domain/ModelChannel;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelChannel;)V
    .locals 5

    iget p1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$type:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-wide v1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$guildId:J

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$channelId:J

    :goto_0
    iget-object p1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->this$0:Lcom/discord/utilities/channel/ChannelSelector;

    iget-wide v3, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$guildId:J

    invoke-static {p1, v3, v4, v1, v2}, Lcom/discord/utilities/channel/ChannelSelector;->access$gotoChannel(Lcom/discord/utilities/channel/ChannelSelector;JJ)V

    iget p1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$type:I

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->this$0:Lcom/discord/utilities/channel/ChannelSelector;

    invoke-virtual {p1}, Lcom/discord/utilities/channel/ChannelSelector;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/utilities/channel/ChannelSelector$selectChannel$2;->$channelId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreVoiceChannelSelected;->selectVoiceChannel(J)Lrx/Observable;

    :cond_1
    return-void
.end method
