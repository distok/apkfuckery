.class public final Lcom/discord/utilities/channel/ChannelSelector$findAndSet$3;
.super Lx/m/c/k;
.source "ChannelSelector.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/channel/ChannelSelector;->findAndSet(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/channel/ChannelSelector;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/channel/ChannelSelector;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$3;->this$0:Lcom/discord/utilities/channel/ChannelSelector;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$3;->invoke(Lcom/discord/models/domain/ModelChannel;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/channel/ChannelSelector$findAndSet$3;->this$0:Lcom/discord/utilities/channel/ChannelSelector;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method
