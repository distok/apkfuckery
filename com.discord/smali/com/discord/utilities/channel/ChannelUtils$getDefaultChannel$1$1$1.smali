.class public final Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1$1;
.super Lx/m/c/k;
.source "ChannelUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1;->call(Ljava/util/Map;)Lcom/discord/models/domain/ModelChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1$1;->this$0:Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1$1;->invoke(Lcom/discord/models/domain/ModelChannel;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelChannel;)Z
    .locals 3

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1$1;->this$0:Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1;

    iget-object v0, v0, Lcom/discord/utilities/channel/ChannelUtils$getDefaultChannel$1$1;->$guildChannelPermissions:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    const-wide/16 v0, 0x400

    invoke-static {v0, v1, p1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p1

    return p1
.end method
