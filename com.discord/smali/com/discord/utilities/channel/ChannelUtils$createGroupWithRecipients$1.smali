.class public final Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;
.super Ljava/lang/Object;
.source "ChannelUtils.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/channel/ChannelUtils;->createGroupWithRecipients(Ljava/util/List;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;

    invoke-direct {v0}, Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;->INSTANCE:Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelChannel;)V
    .locals 2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    const-string v1, "channel"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannels;->onGroupCreated(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/channel/ChannelUtils$createGroupWithRecipients$1;->call(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method
