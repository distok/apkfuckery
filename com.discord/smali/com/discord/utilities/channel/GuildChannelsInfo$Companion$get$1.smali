.class public final Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;
.super Ljava/lang/Object;
.source "GuildChannelsInfo.kt"

# interfaces
.implements Lrx/functions/Func8;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/channel/GuildChannelsInfo$Companion;->get(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "T8:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func8<",
        "Lcom/discord/models/domain/ModelUser$Me;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/models/domain/ModelNotificationSettings;",
        "Ljava/lang/Boolean;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildRole;",
        ">;",
        "Ljava/lang/Long;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;",
        "Lcom/discord/utilities/channel/GuildChannelsInfo;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;

    invoke-direct {v0}, Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelNotificationSettings;Ljava/lang/Boolean;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Ljava/util/List;)Lcom/discord/utilities/channel/GuildChannelsInfo;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser$Me;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;)",
            "Lcom/discord/utilities/channel/GuildChannelsInfo;"
        }
    .end annotation

    move-object/from16 v7, p6

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser$Me;->isMfaEnabled()Z

    move-result v2

    if-ne v2, v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v3

    move v5, v3

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    const-wide/16 v3, 0x10

    invoke-static {v3, v4, v2, v5}, Lcom/discord/utilities/permissions/PermissionUtils;->isElevated(JZI)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v3, v4, v7}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    const-wide/16 v8, 0x1

    invoke-static {v8, v9, v7}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v4

    const/4 v6, 0x0

    if-nez v4, :cond_5

    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getVanityUrlCode()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_3
    move-object v4, v6

    :goto_3
    if-eqz v4, :cond_4

    goto :goto_4

    :cond_4
    const/4 v8, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 v8, 0x1

    :goto_5
    if-eqz p2, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v9, p5

    invoke-interface {v9, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelGuildRole;

    move-object v9, v4

    goto :goto_6

    :cond_6
    move-object v9, v6

    :goto_6
    if-nez v2, :cond_7

    if-eqz v3, :cond_7

    const/4 v10, 0x1

    goto :goto_7

    :cond_7
    const/4 v10, 0x0

    :goto_7
    if-eqz p2, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->isVerifiedServer()Z

    move-result v2

    if-ne v2, v1, :cond_8

    const/4 v11, 0x1

    goto :goto_8

    :cond_8
    const/4 v11, 0x0

    :goto_8
    sget-object v2, Lcom/discord/utilities/permissions/ManageGuildContext;->Companion:Lcom/discord/utilities/permissions/ManageGuildContext$Companion;

    const-string v3, "categories"

    move-object/from16 v4, p8

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_9

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :cond_9
    if-eqz v6, :cond_a

    if-eqz p1, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v12

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v14

    cmp-long v3, v12, v14

    if-nez v3, :cond_a

    const/4 v3, 0x1

    goto :goto_9

    :cond_a
    const/4 v3, 0x0

    :goto_9
    const-string v1, "channelPermissions"

    move-object/from16 v12, p7

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser$Me;->isMfaEnabled()Z

    move-result v0

    move v6, v0

    goto :goto_a

    :cond_b
    const/4 v6, 0x0

    :goto_a
    move-object v0, v2

    move-object/from16 v1, p8

    move v2, v3

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    invoke-virtual/range {v0 .. v6}, Lcom/discord/utilities/permissions/ManageGuildContext$Companion;->from(Ljava/util/List;ZLjava/lang/Long;Ljava/util/Map;IZ)Lcom/discord/utilities/permissions/ManageGuildContext;

    move-result-object v13

    const-wide/32 v0, 0x4000000

    invoke-static {v0, v1, v7}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v14

    new-instance v15, Lcom/discord/utilities/channel/GuildChannelsInfo;

    const-string v0, "guildSettings"

    move-object/from16 v3, p3

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hideMuted"

    move-object/from16 v1, p4

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move-object v0, v15

    move-object/from16 v1, p2

    move-object v2, v9

    move-object/from16 v5, p7

    move v6, v8

    move v7, v10

    move v8, v11

    move-object v9, v13

    move v10, v14

    invoke-direct/range {v0 .. v10}, Lcom/discord/utilities/channel/GuildChannelsInfo;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelNotificationSettings;ZLjava/util/Map;ZZZLcom/discord/utilities/permissions/ManageGuildContext;Z)V

    return-object v15
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser$Me;

    check-cast p2, Lcom/discord/models/domain/ModelGuild;

    check-cast p3, Lcom/discord/models/domain/ModelNotificationSettings;

    check-cast p4, Ljava/lang/Boolean;

    check-cast p5, Ljava/util/Map;

    check-cast p6, Ljava/lang/Long;

    check-cast p7, Ljava/util/Map;

    check-cast p8, Ljava/util/List;

    invoke-virtual/range {p0 .. p8}, Lcom/discord/utilities/channel/GuildChannelsInfo$Companion$get$1;->call(Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelNotificationSettings;Ljava/lang/Boolean;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Ljava/util/List;)Lcom/discord/utilities/channel/GuildChannelsInfo;

    move-result-object p1

    return-object p1
.end method
