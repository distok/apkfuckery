.class public final Lcom/discord/utilities/dimen/DimenUtils;
.super Ljava/lang/Object;
.source "DimenUtils.kt"


# static fields
.field private static final DENSITY:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "Resources.getSystem()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/discord/utilities/dimen/DimenUtils;->DENSITY:F

    return-void
.end method

.method public static final dpToPixels(F)I
    .locals 1

    sget v0, Lcom/discord/utilities/dimen/DimenUtils;->DENSITY:F

    mul-float p0, p0, v0

    float-to-int p0, p0

    return p0
.end method

.method public static final dpToPixels(I)I
    .locals 1

    int-to-float p0, p0

    sget v0, Lcom/discord/utilities/dimen/DimenUtils;->DENSITY:F

    mul-float p0, p0, v0

    float-to-int p0, p0

    return p0
.end method

.method public static final pixelsToDp(I)I
    .locals 1

    int-to-float p0, p0

    sget v0, Lcom/discord/utilities/dimen/DimenUtils;->DENSITY:F

    div-float/2addr p0, v0

    float-to-int p0, p0

    return p0
.end method
