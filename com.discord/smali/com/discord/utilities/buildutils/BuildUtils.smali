.class public final Lcom/discord/utilities/buildutils/BuildUtils;
.super Ljava/lang/Object;
.source "BuildUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/buildutils/BuildUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/buildutils/BuildUtils;

    invoke-direct {v0}, Lcom/discord/utilities/buildutils/BuildUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/buildutils/BuildUtils;->INSTANCE:Lcom/discord/utilities/buildutils/BuildUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final isValidBuildVersionName(Ljava/lang/String;)Z
    .locals 2

    const-string/jumbo v0, "versionName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^\\d+[.]\\d+$"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method
