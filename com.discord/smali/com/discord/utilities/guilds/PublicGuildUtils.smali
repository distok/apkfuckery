.class public final Lcom/discord/utilities/guilds/PublicGuildUtils;
.super Ljava/lang/Object;
.source "PublicGuildUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/guilds/PublicGuildUtils;

.field private static final PUBLIC_GUILD_ANNOUNCEMENTS_GUILD_ID:J = 0x943a6b05080000bL

.field private static final PUBLIC_GUILD_UPDATES_WEBHOOK_USER_ID:J = 0x94afe6191800000L


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/guilds/PublicGuildUtils;

    invoke-direct {v0}, Lcom/discord/utilities/guilds/PublicGuildUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/guilds/PublicGuildUtils;->INSTANCE:Lcom/discord/utilities/guilds/PublicGuildUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isPublicGuildSystemMessage(Lcom/discord/models/domain/ModelMessage;)Z
    .locals 5

    const-string v0, "message"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-wide v1, 0x943a6b05080000bL

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    const-wide v2, 0x94afe6191800000L

    cmp-long p0, v0, v2

    if-nez p0, :cond_2

    goto :goto_2

    :cond_2
    const/4 p0, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p0, 0x1

    :goto_3
    return p0
.end method
