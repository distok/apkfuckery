.class public final Lcom/discord/utilities/guilds/GuildGatingUtils;
.super Ljava/lang/Object;
.source "GuildGatingUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/guilds/GuildGatingUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/guilds/GuildGatingUtils;

    invoke-direct {v0}, Lcom/discord/utilities/guilds/GuildGatingUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/guilds/GuildGatingUtils;->INSTANCE:Lcom/discord/utilities/guilds/GuildGatingUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic observeShouldShowGuildGate$default(Lcom/discord/utilities/guilds/GuildGatingUtils;JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;ILjava/lang/Object;)Lrx/Observable;
    .locals 6

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p3

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p6, 0x4

    if-eqz p3, :cond_1

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object p4

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p5

    :cond_2
    move-object v5, p5

    move-object v0, p0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/guilds/GuildGatingUtils;->observeShouldShowGuildGate(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic shouldShowGuildGate$default(Lcom/discord/utilities/guilds/GuildGatingUtils;JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;ILjava/lang/Object;)Z
    .locals 6

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p3

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p6, 0x4

    if-eqz p3, :cond_1

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object p4

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p5

    :cond_2
    move-object v5, p5

    move-object v0, p0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/guilds/GuildGatingUtils;->shouldShowGuildGate(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final observeShouldShowGuildGate(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/stores/StoreGuilds;",
            "Lcom/discord/stores/StoreExperiments;",
            "Lcom/discord/stores/StoreUser;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "guildStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "experimentStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p3, v2, v0

    const/4 p3, 0x1

    aput-object p4, v2, p3

    const/4 p3, 0x2

    aput-object p5, v2, p3

    new-instance v6, Lcom/discord/utilities/guilds/GuildGatingUtils$observeShouldShowGuildGate$1;

    invoke-direct {v6, p1, p2}, Lcom/discord/utilities/guilds/GuildGatingUtils$observeShouldShowGuildGate$1;-><init>(J)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final shouldShowGuildGate(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;)Z
    .locals 6

    const-string v0, "guildStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "experimentStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "2020-09_guild_member_verification_user_experiment"

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object p4

    if-eqz p4, :cond_2

    invoke-virtual {p3}, Lcom/discord/stores/StoreGuilds;->getGuilds()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    if-eqz p1, :cond_2

    invoke-virtual {p5}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-virtual {p3}, Lcom/discord/stores/StoreGuilds;->getMembers()Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    if-eqz p2, :cond_2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz p2, :cond_2

    invoke-virtual {p4}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result p3

    const/4 p5, 0x1

    if-ne p3, p5, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-virtual {p1, v2, v3}, Lcom/discord/models/domain/ModelGuild;->isOwner(J)Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isGatingEnabled()Z

    move-result p1

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->isPending()Z

    move-result p2

    if-eqz p1, :cond_1

    invoke-virtual {p4}, Lcom/discord/models/experiments/domain/Experiment;->getTrigger()Lkotlin/jvm/functions/Function0;

    move-result-object p4

    invoke-interface {p4}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_1
    if-eqz p3, :cond_2

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method
