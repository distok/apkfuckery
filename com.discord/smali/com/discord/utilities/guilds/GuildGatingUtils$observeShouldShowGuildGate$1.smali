.class public final Lcom/discord/utilities/guilds/GuildGatingUtils$observeShouldShowGuildGate$1;
.super Lx/m/c/k;
.source "GuildGatingUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/guilds/GuildGatingUtils;->observeShouldShowGuildGate(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/utilities/guilds/GuildGatingUtils$observeShouldShowGuildGate$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/guilds/GuildGatingUtils$observeShouldShowGuildGate$1;->invoke()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Z
    .locals 8

    sget-object v0, Lcom/discord/utilities/guilds/GuildGatingUtils;->INSTANCE:Lcom/discord/utilities/guilds/GuildGatingUtils;

    iget-wide v1, p0, Lcom/discord/utilities/guilds/GuildGatingUtils$observeShouldShowGuildGate$1;->$guildId:J

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/guilds/GuildGatingUtils;->shouldShowGuildGate$default(Lcom/discord/utilities/guilds/GuildGatingUtils;JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method
