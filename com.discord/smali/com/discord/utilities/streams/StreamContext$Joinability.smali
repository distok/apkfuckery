.class public final enum Lcom/discord/utilities/streams/StreamContext$Joinability;
.super Ljava/lang/Enum;
.source "StreamContext.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/streams/StreamContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Joinability"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/streams/StreamContext$Joinability;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/streams/StreamContext$Joinability;

.field public static final enum CAN_CONNECT:Lcom/discord/utilities/streams/StreamContext$Joinability;

.field public static final enum MISSING_PERMISSIONS:Lcom/discord/utilities/streams/StreamContext$Joinability;

.field public static final enum VOICE_CHANNEL_FULL:Lcom/discord/utilities/streams/StreamContext$Joinability;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/utilities/streams/StreamContext$Joinability;

    new-instance v1, Lcom/discord/utilities/streams/StreamContext$Joinability;

    const-string v2, "CAN_CONNECT"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/streams/StreamContext$Joinability;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->CAN_CONNECT:Lcom/discord/utilities/streams/StreamContext$Joinability;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/streams/StreamContext$Joinability;

    const-string v2, "VOICE_CHANNEL_FULL"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/streams/StreamContext$Joinability;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->VOICE_CHANNEL_FULL:Lcom/discord/utilities/streams/StreamContext$Joinability;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/streams/StreamContext$Joinability;

    const-string v2, "MISSING_PERMISSIONS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/streams/StreamContext$Joinability;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->MISSING_PERMISSIONS:Lcom/discord/utilities/streams/StreamContext$Joinability;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/utilities/streams/StreamContext$Joinability;->$VALUES:[Lcom/discord/utilities/streams/StreamContext$Joinability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/streams/StreamContext$Joinability;
    .locals 1

    const-class v0, Lcom/discord/utilities/streams/StreamContext$Joinability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/streams/StreamContext$Joinability;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/streams/StreamContext$Joinability;
    .locals 1

    sget-object v0, Lcom/discord/utilities/streams/StreamContext$Joinability;->$VALUES:[Lcom/discord/utilities/streams/StreamContext$Joinability;

    invoke-virtual {v0}, [Lcom/discord/utilities/streams/StreamContext$Joinability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/streams/StreamContext$Joinability;

    return-object v0
.end method
