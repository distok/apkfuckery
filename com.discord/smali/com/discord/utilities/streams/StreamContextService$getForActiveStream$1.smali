.class public final Lcom/discord/utilities/streams/StreamContextService$getForActiveStream$1;
.super Ljava/lang/Object;
.source "StreamContextService.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/streams/StreamContextService;->getForActiveStream()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/streams/StreamContext;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/streams/StreamContextService;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/streams/StreamContextService;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/streams/StreamContextService$getForActiveStream$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/streams/StreamContextService$getForActiveStream$1;->call(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v0

    iget-object p1, p0, Lcom/discord/utilities/streams/StreamContextService$getForActiveStream$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/utilities/streams/StreamContextService;->getForUser(JZ)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
