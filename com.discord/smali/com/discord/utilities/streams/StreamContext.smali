.class public final Lcom/discord/utilities/streams/StreamContext;
.super Ljava/lang/Object;
.source "StreamContext.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/streams/StreamContext$Joinability;
    }
.end annotation


# instance fields
.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final isCurrentUserParticipating:Z

.field private final isSelfStream:Z

.field private final joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

.field private final preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

.field private final stream:Lcom/discord/models/domain/ModelApplicationStream;

.field private final user:Lcom/discord/models/domain/ModelUser;

.field private final userNickname:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZ)V
    .locals 1

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "joinability"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "user"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    iput-object p2, p0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p3, p0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    iput-object p4, p0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    iput-object p5, p0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    iput-object p6, p0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    iput-boolean p8, p0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/discord/utilities/streams/StreamContext;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/discord/utilities/streams/StreamContext;->copy(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZ)Lcom/discord/utilities/streams/StreamContext;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component3()Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    return-object v0
.end method

.method public final component4()Lcom/discord/utilities/streams/StreamContext$Joinability;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    return-object v0
.end method

.method public final component5()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZ)Lcom/discord/utilities/streams/StreamContext;
    .locals 10

    const-string/jumbo v0, "stream"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "joinability"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "user"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/streams/StreamContext;

    move-object v1, v0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/discord/utilities/streams/StreamContext;-><init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/streams/StreamContext;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/streams/StreamContext;

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    iget-object v1, p1, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    iget-object v1, p1, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    iget-object v1, p1, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    iget-boolean v1, p1, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    iget-boolean p1, p1, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getJoinability()Lcom/discord/utilities/streams/StreamContext$Joinability;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    return-object v0
.end method

.method public final getPreview()Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    return-object v0
.end method

.method public final getStream()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public final getUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getUserNickname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    if-eqz v1, :cond_7

    goto :goto_5

    :cond_7
    move v2, v1

    :goto_5
    add-int/2addr v0, v2

    return v0
.end method

.method public final isCurrentUserParticipating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    return v0
.end method

.method public final isSelfStream()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StreamContext(stream="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/streams/StreamContext;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/streams/StreamContext;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preview="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/streams/StreamContext;->preview:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", joinability="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/streams/StreamContext;->joinability:Lcom/discord/utilities/streams/StreamContext$Joinability;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/streams/StreamContext;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userNickname="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/streams/StreamContext;->userNickname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isCurrentUserParticipating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSelfStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/streams/StreamContext;->isSelfStream:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
