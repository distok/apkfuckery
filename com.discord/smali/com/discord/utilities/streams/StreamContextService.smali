.class public final Lcom/discord/utilities/streams/StreamContextService;
.super Ljava/lang/Object;
.source "StreamContextService.kt"


# instance fields
.field private final applicationStreamPreviewStore:Lcom/discord/stores/StoreApplicationStreamPreviews;

.field private final applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

.field private final channelStore:Lcom/discord/stores/StoreChannels;

.field private final guildStore:Lcom/discord/stores/StoreGuilds;

.field private final permissionsStore:Lcom/discord/stores/StorePermissions;

.field private final userStore:Lcom/discord/stores/StoreUser;

.field private final voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private final voiceStateStore:Lcom/discord/stores/StoreVoiceStates;


# direct methods
.method public constructor <init>()V
    .locals 11

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    const/4 v10, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;)V
    .locals 1

    const-string v0, "applicationStreamingStore"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionsStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voiceChannelSelectedStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voiceStateStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelStore"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applicationStreamPreviewStore"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object p2, p0, Lcom/discord/utilities/streams/StreamContextService;->guildStore:Lcom/discord/stores/StoreGuilds;

    iput-object p3, p0, Lcom/discord/utilities/streams/StreamContextService;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iput-object p4, p0, Lcom/discord/utilities/streams/StreamContextService;->userStore:Lcom/discord/stores/StoreUser;

    iput-object p5, p0, Lcom/discord/utilities/streams/StreamContextService;->voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-object p6, p0, Lcom/discord/utilities/streams/StreamContextService;->voiceStateStore:Lcom/discord/stores/StoreVoiceStates;

    iput-object p7, p0, Lcom/discord/utilities/streams/StreamContextService;->channelStore:Lcom/discord/stores/StoreChannels;

    iput-object p8, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamPreviewStore:Lcom/discord/stores/StoreApplicationStreamPreviews;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 8

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, p2

    :goto_1
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_2

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v3

    goto :goto_2

    :cond_2
    move-object v3, p3

    :goto_2
    and-int/lit8 v4, v0, 0x8

    if-eqz v4, :cond_3

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v4

    goto :goto_3

    :cond_3
    move-object v4, p4

    :goto_3
    and-int/lit8 v5, v0, 0x10

    if-eqz v5, :cond_4

    sget-object v5, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v5}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v5

    goto :goto_4

    :cond_4
    move-object v5, p5

    :goto_4
    and-int/lit8 v6, v0, 0x20

    if-eqz v6, :cond_5

    sget-object v6, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v6}, Lcom/discord/stores/StoreStream$Companion;->getVoiceStates()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v6

    goto :goto_5

    :cond_5
    move-object v6, p6

    :goto_5
    and-int/lit8 v7, v0, 0x40

    if-eqz v7, :cond_6

    sget-object v7, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v7}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v7

    goto :goto_6

    :cond_6
    move-object v7, p7

    :goto_6
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreamPreviews()Lcom/discord/stores/StoreApplicationStreamPreviews;

    move-result-object v0

    goto :goto_7

    :cond_7
    move-object/from16 v0, p8

    :goto_7
    move-object p1, p0

    move-object p2, v1

    move-object p3, v2

    move-object p4, v3

    move-object p5, v4

    move-object p6, v5

    move-object p7, v6

    move-object/from16 p8, v7

    move-object/from16 p9, v0

    invoke-direct/range {p1 .. p9}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;)V

    return-void
.end method

.method public static final synthetic access$getApplicationStreamPreviewStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreApplicationStreamPreviews;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamPreviewStore:Lcom/discord/stores/StoreApplicationStreamPreviews;

    return-object p0
.end method

.method public static final synthetic access$getApplicationStreamingStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreApplicationStreaming;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    return-object p0
.end method

.method public static final synthetic access$getChannelStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreChannels;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->channelStore:Lcom/discord/stores/StoreChannels;

    return-object p0
.end method

.method public static final synthetic access$getGuildStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreGuilds;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->guildStore:Lcom/discord/stores/StoreGuilds;

    return-object p0
.end method

.method public static final synthetic access$getPermissionsStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StorePermissions;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->permissionsStore:Lcom/discord/stores/StorePermissions;

    return-object p0
.end method

.method public static final synthetic access$getPreviewObservable(Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/models/domain/ModelApplicationStream;ZLcom/discord/stores/StoreApplicationStreamPreviews;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/streams/StreamContextService;->getPreviewObservable(Lcom/discord/models/domain/ModelApplicationStream;ZLcom/discord/stores/StoreApplicationStreamPreviews;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getUserStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreUser;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->userStore:Lcom/discord/stores/StoreUser;

    return-object p0
.end method

.method public static final synthetic access$getVoiceChannelSelectedStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreVoiceChannelSelected;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    return-object p0
.end method

.method public static final synthetic access$getVoiceStateStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreVoiceStates;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/streams/StreamContextService;->voiceStateStore:Lcom/discord/stores/StoreVoiceStates;

    return-object p0
.end method

.method private final getPreviewObservable(Lcom/discord/models/domain/ModelApplicationStream;ZLcom/discord/stores/StoreApplicationStreamPreviews;)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            "Z",
            "Lcom/discord/stores/StoreApplicationStreamPreviews;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-virtual {p3, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews;->get(Lcom/discord/models/domain/ModelApplicationStream;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    new-instance p2, Lg0/l/e/j;

    invoke-direct {p2, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(null)"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p1, p2

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final getForActiveStream()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getActiveStream()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/streams/StreamContextService$getForActiveStream$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/streams/StreamContextService$getForActiveStream$1;-><init>(Lcom/discord/utilities/streams/StreamContextService;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "applicationStreamingStor\u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getForAllStreamingUsers()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getStreamsByUser()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/streams/StreamContextService$getForAllStreamingUsers$1;->INSTANCE:Lcom/discord/utilities/streams/StreamContextService$getForAllStreamingUsers$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/streams/StreamContextService$getForAllStreamingUsers$2;

    invoke-direct {v1, p0}, Lcom/discord/utilities/streams/StreamContextService$getForAllStreamingUsers$2;-><init>(Lcom/discord/utilities/streams/StreamContextService;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "applicationStreamingStor\u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getForUser(JZ)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContextService;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreApplicationStreaming;->getStreamsForUser(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;-><init>(Lcom/discord/utilities/streams/StreamContextService;ZJ)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "applicationStreamingStor\u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
