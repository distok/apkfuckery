.class public final Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;
.super Lx/m/c/k;
.source "StreamContextService.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function10;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->call(Lcom/discord/models/domain/ModelApplicationStream;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function10<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/String;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelVoice$State;",
        ">;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Ljava/lang/Long;",
        "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
        "Lcom/discord/utilities/streams/StreamContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $stream:Lcom/discord/models/domain/ModelApplicationStream;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelApplicationStream;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;->$stream:Lcom/discord/models/domain/ModelApplicationStream;

    const/16 p1, 0xa

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/Map;Lcom/discord/models/domain/ModelChannel;JLcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Lcom/discord/utilities/streams/StreamContext;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;",
            "Lcom/discord/models/domain/ModelChannel;",
            "J",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            ")",
            "Lcom/discord/utilities/streams/StreamContext;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p3

    const-string v2, "me"

    move-object/from16 v3, p5

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "voiceStates"

    move-object/from16 v4, p7

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;->$stream:Lcom/discord/models/domain/ModelApplicationStream;

    instance-of v2, v2, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-nez p4, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_6

    :cond_2
    const/4 v2, 0x0

    if-eqz p8, :cond_3

    invoke-virtual/range {p8 .. p8}, Lcom/discord/models/domain/ModelChannel;->getUserLimit()I

    move-result v5

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x1

    if-lez v5, :cond_4

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->size()I

    move-result v4

    if-lt v4, v5, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_1
    if-eqz p8, :cond_5

    invoke-virtual/range {p8 .. p8}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    cmp-long v5, v7, p9

    if-nez v5, :cond_5

    const/4 v5, 0x1

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    :goto_2
    const-wide/32 v7, 0x100000

    invoke-static {v7, v8, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v7

    const-wide/16 v8, 0x10

    invoke-static {v8, v9, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v1

    iget-object v8, v0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;->$stream:Lcom/discord/models/domain/ModelApplicationStream;

    instance-of v8, v8, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    if-eqz v8, :cond_6

    if-nez v7, :cond_6

    if-nez v5, :cond_6

    sget-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->MISSING_PERMISSIONS:Lcom/discord/utilities/streams/StreamContext$Joinability;

    goto :goto_3

    :cond_6
    if-eqz v4, :cond_7

    if-nez v1, :cond_7

    sget-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->VOICE_CHANNEL_FULL:Lcom/discord/utilities/streams/StreamContext$Joinability;

    goto :goto_3

    :cond_7
    sget-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->CAN_CONNECT:Lcom/discord/utilities/streams/StreamContext$Joinability;

    :goto_3
    move-object v7, v1

    if-eqz p11, :cond_8

    invoke-virtual/range {p11 .. p11}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual/range {p11 .. p11}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    iget-object v4, v0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;->$stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-static {v1, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v10, 0x1

    goto :goto_4

    :cond_8
    const/4 v10, 0x0

    :goto_4
    new-instance v1, Lcom/discord/utilities/streams/StreamContext;

    iget-object v4, v0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;->$stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v8

    invoke-virtual/range {p5 .. p5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v11

    cmp-long v3, v8, v11

    if-nez v3, :cond_9

    const/4 v11, 0x1

    goto :goto_5

    :cond_9
    const/4 v11, 0x0

    :goto_5
    move-object v3, v1

    move-object v5, p1

    move-object v6, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p6

    invoke-direct/range {v3 .. v11}, Lcom/discord/utilities/streams/StreamContext;-><init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZ)V

    :goto_6
    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    move-object v1, p1

    check-cast v1, Lcom/discord/models/domain/ModelGuild;

    move-object v2, p2

    check-cast v2, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    move-object v3, p3

    check-cast v3, Ljava/lang/Long;

    move-object/from16 v4, p4

    check-cast v4, Lcom/discord/models/domain/ModelUser;

    move-object/from16 v5, p5

    check-cast v5, Lcom/discord/models/domain/ModelUser;

    move-object/from16 v6, p6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v7, p7

    check-cast v7, Ljava/util/Map;

    move-object/from16 v8, p8

    check-cast v8, Lcom/discord/models/domain/ModelChannel;

    move-object/from16 v0, p9

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v9

    move-object/from16 v11, p10

    check-cast v11, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-object v0, p0

    invoke-virtual/range {v0 .. v11}, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;->invoke(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/Map;Lcom/discord/models/domain/ModelChannel;JLcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Lcom/discord/utilities/streams/StreamContext;

    move-result-object v0

    return-object v0
.end method
