.class public final Lcom/discord/utilities/streams/StreamContextService$getForUser$1;
.super Ljava/lang/Object;
.source "StreamContextService.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/streams/StreamContextService;->getForUser(JZ)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelApplicationStream;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/streams/StreamContext;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $includePreview:Z

.field public final synthetic $userId:J

.field public final synthetic this$0:Lcom/discord/utilities/streams/StreamContextService;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/streams/StreamContextService;ZJ)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    iput-boolean p2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->$includePreview:Z

    iput-wide p3, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->$userId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->call(Lcom/discord/models/domain/ModelApplicationStream;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelApplicationStream;)Lrx/Observable;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_0
    instance-of v0, p1, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;->getGuildId()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/models/domain/ModelApplicationStream$CallStream;

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getGuildStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v3

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    iget-boolean v4, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->$includePreview:Z

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getApplicationStreamPreviewStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreApplicationStreamPreviews;

    move-result-object v5

    invoke-static {v2, p1, v4, v5}, Lcom/discord/utilities/streams/StreamContextService;->access$getPreviewObservable(Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/models/domain/ModelApplicationStream;ZLcom/discord/stores/StoreApplicationStreamPreviews;)Lrx/Observable;

    move-result-object v4

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getPermissionsStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StorePermissions;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v5

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getUserStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreUser;

    move-result-object v2

    iget-wide v6, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->$userId:J

    invoke-virtual {v2, v6, v7}, Lcom/discord/stores/StoreUser;->observeUser(J)Lrx/Observable;

    move-result-object v6

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getUserStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v7

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getGuildStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v2

    new-instance v8, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$1;

    invoke-direct {v8, p0}, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$1;-><init>(Lcom/discord/utilities/streams/StreamContextService$getForUser$1;)V

    invoke-virtual {v2, v8}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    sget-object v8, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$2;->INSTANCE:Lcom/discord/utilities/streams/StreamContextService$getForUser$1$2;

    invoke-virtual {v2, v8}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v8

    const-string v2, "guildStore\n             \u2026  .distinctUntilChanged()"

    invoke-static {v8, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v2}, Lcom/discord/utilities/streams/StreamContextService;->access$getVoiceStateStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreVoiceStates;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v9

    invoke-virtual {v2, v0, v1, v9, v10}, Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;

    move-result-object v9

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v0}, Lcom/discord/utilities/streams/StreamContextService;->access$getChannelStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v10

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v0}, Lcom/discord/utilities/streams/StreamContextService;->access$getVoiceChannelSelectedStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedVoiceChannelId()Lrx/Observable;

    move-result-object v11

    iget-object v0, p0, Lcom/discord/utilities/streams/StreamContextService$getForUser$1;->this$0:Lcom/discord/utilities/streams/StreamContextService;

    invoke-static {v0}, Lcom/discord/utilities/streams/StreamContextService;->access$getApplicationStreamingStore$p(Lcom/discord/utilities/streams/StreamContextService;)Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getActiveStream()Lrx/Observable;

    move-result-object v12

    new-instance v13, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;

    invoke-direct {v13, p1}, Lcom/discord/utilities/streams/StreamContextService$getForUser$1$3;-><init>(Lcom/discord/models/domain/ModelApplicationStream;)V

    invoke-static/range {v3 .. v13}, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function10;)Lrx/Observable;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
