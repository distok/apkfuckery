.class public final Lcom/discord/utilities/SystemLogUtils$fetch$2;
.super Ljava/lang/Object;
.source "SystemLogUtils.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/SystemLogUtils;->fetch(Lkotlin/text/Regex;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/util/LinkedList<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $filter:Lkotlin/text/Regex;

.field public final synthetic $logErrors:Z

.field public final synthetic $output:Ljava/util/LinkedList;

.field public final synthetic $systemLogPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/LinkedList;Lkotlin/text/Regex;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$systemLogPath:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$output:Ljava/util/LinkedList;

    iput-object p3, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$filter:Lkotlin/text/Regex;

    iput-boolean p4, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$logErrors:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/SystemLogUtils$fetch$2;->call()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljava/util/LinkedList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/lang/ProcessBuilder;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$systemLogPath:Ljava/lang/String;

    aput-object v4, v3, v0

    const-string v4, "-d"

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-direct {v2, v3}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/ProcessBuilder;->redirectErrorStream(Z)Ljava/lang/ProcessBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/SystemLogUtils;->INSTANCE:Lcom/discord/utilities/SystemLogUtils;

    const-string v3, "logcatProccess"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v1}, Lcom/discord/utilities/SystemLogUtils;->access$waitFor(Lcom/discord/utilities/SystemLogUtils;Ljava/lang/Process;)V

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    const-string v4, "logcatProccess.inputStream"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lx/s/a;->a:Ljava/nio/charset/Charset;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    const/16 v3, 0x2000

    instance-of v4, v5, Ljava/io/BufferedReader;

    if-eqz v4, :cond_0

    check-cast v5, Ljava/io/BufferedReader;

    goto :goto_0

    :cond_0
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v5, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v5, v4

    :goto_0
    iget-object v3, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$output:Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$filter:Lkotlin/text/Regex;

    invoke-virtual {v2, v5, v3, v4}, Lcom/discord/utilities/SystemLogUtils;->processLogs$app_productionDiscordExternalRelease(Ljava/io/BufferedReader;Ljava/util/LinkedList;Lkotlin/text/Regex;)V

    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V

    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_0
    move-exception v2

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    iget-boolean v3, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$logErrors:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$output:Ljava/util/LinkedList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception getting system logs \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v5, 0x27

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const-string v3, "e.stackTrace"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v3, v2

    :goto_2
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v5, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$output:Ljava/util/LinkedList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/discord/utilities/SystemLogUtils$fetch$2;->$output:Ljava/util/LinkedList;

    return-object v0

    :goto_4
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V

    :cond_3
    throw v0
.end method
