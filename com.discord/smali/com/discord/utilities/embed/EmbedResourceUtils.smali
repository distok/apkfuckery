.class public final Lcom/discord/utilities/embed/EmbedResourceUtils;
.super Ljava/lang/Object;
.source "EmbedResourceUtils.kt"


# static fields
.field public static final FILE_SCHEME:Ljava/lang/String; = "res:///"

.field public static final INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

.field public static final MAX_IMAGE_SIZE:I = 0x5a0

.field private static final MAX_IMAGE_VIEW_HEIGHT_PX:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/embed/EmbedResourceUtils;

    invoke-direct {v0}, Lcom/discord/utilities/embed/EmbedResourceUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/embed/EmbedResourceUtils;->INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

    const/16 v0, 0x140

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    sput v0, Lcom/discord/utilities/embed/EmbedResourceUtils;->MAX_IMAGE_VIEW_HEIGHT_PX:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final asImageItem(Lcom/discord/models/domain/ModelMessageAttachment;)Lcom/discord/models/domain/ModelMessageEmbed$Item;
    .locals 4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getType()Lcom/discord/models/domain/ModelMessageAttachment$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getFilename()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/embed/EmbedResourceUtils;->createFileImageItem(Ljava/lang/String;)Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/discord/models/domain/ModelMessageEmbed$Item;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getProxyUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getHeight()I

    move-result p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method private final asVideoItem(Lcom/discord/models/domain/ModelMessageAttachment;)Lcom/discord/models/domain/ModelMessageEmbed$Item;
    .locals 4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getType()Lcom/discord/models/domain/ModelMessageAttachment$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/models/domain/ModelMessageEmbed$Item;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getProxyUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getHeight()I

    move-result p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public static synthetic calculateScaledSize$default(Lcom/discord/utilities/embed/EmbedResourceUtils;IIIILandroid/content/res/Resources;IILjava/lang/Object;)Lkotlin/Pair;
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    move v6, p6

    :goto_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/utilities/embed/EmbedResourceUtils;->calculateScaledSize(IIIILandroid/content/res/Resources;I)Lkotlin/Pair;

    move-result-object p0

    return-object p0
.end method

.method private final createFileImageItem(Ljava/lang/String;)Lcom/discord/models/domain/ModelMessageEmbed$Item;
    .locals 4

    const-string v0, "res:///"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/discord/utilities/embed/EmbedResourceUtils;->getFileDrawable(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/discord/models/domain/ModelMessageEmbed$Item;

    const/4 v1, 0x0

    const/16 v2, 0x1e

    const/16 v3, 0x28

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/discord/models/domain/ModelMessageEmbed$Item;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v0
.end method


# virtual methods
.method public final calculateScaledSize(IIIILandroid/content/res/Resources;I)Lkotlin/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII",
            "Landroid/content/res/Resources;",
            "I)",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "resources"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p5

    iget p5, p5, Landroid/util/DisplayMetrics;->density:F

    int-to-float v0, p1

    mul-float v1, p5, v0

    int-to-float v2, p2

    mul-float p5, p5, v2

    div-int/lit8 v3, p3, 0x2

    int-to-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    cmpl-float v3, v1, v3

    if-gtz v3, :cond_1

    div-int/lit8 v3, p4, 0x2

    int-to-float v3, v3

    cmpl-float v3, p5, v3

    if-lez v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    if-lez p6, :cond_2

    if-ge p1, p6, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-lez p2, :cond_3

    div-float v7, v0, v2

    goto :goto_3

    :cond_3
    const/4 v7, 0x0

    :goto_3
    if-eqz v3, :cond_7

    if-le p1, p2, :cond_4

    const/4 v4, 0x1

    :cond_4
    if-eqz v4, :cond_5

    int-to-float p1, p3

    goto :goto_4

    :cond_5
    int-to-float p1, p4

    mul-float p1, p1, v7

    :goto_4
    if-eqz v4, :cond_6

    int-to-float p2, p3

    div-float/2addr p2, v7

    goto :goto_5

    :cond_6
    int-to-float p2, p4

    :goto_5
    float-to-int p1, p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    float-to-int p2, p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance p3, Lkotlin/Pair;

    invoke-direct {p3, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_6

    :cond_7
    if-eqz v6, :cond_a

    int-to-float p1, p6

    div-float p2, p1, v0

    mul-float p2, p2, v2

    int-to-float p3, p4

    cmpl-float p4, p2, p3

    if-lez p4, :cond_8

    const/4 v4, 0x1

    :cond_8
    if-eqz v4, :cond_9

    div-float/2addr p3, p2

    mul-float p1, p1, p3

    mul-float p2, p2, p3

    :cond_9
    float-to-int p1, p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    float-to-int p2, p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance p3, Lkotlin/Pair;

    invoke-direct {p3, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_6

    :cond_a
    float-to-int p1, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    float-to-int p2, p5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance p3, Lkotlin/Pair;

    invoke-direct {p3, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_6
    return-object p3
.end method

.method public final computeMaximumImageWidthPx(Landroid/content/Context;)I
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070203

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f070079

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {p1}, Lcom/discord/utilities/display/DisplayUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    add-int/2addr v1, v0

    sub-int/2addr p1, v1

    const/16 v0, 0x5a0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method public final createAttachmentEmbed(Lcom/discord/models/domain/ModelMessageAttachment;)Lcom/discord/models/domain/ModelMessageEmbed;
    .locals 3

    const-string v0, "attachment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/embed/EmbedResourceUtils;->asVideoItem(Lcom/discord/models/domain/ModelMessageAttachment;)Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/discord/utilities/embed/EmbedResourceUtils;->asImageItem(Lcom/discord/models/domain/ModelMessageAttachment;)Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v1

    new-instance v2, Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-direct {v2, p1, v0, v1}, Lcom/discord/models/domain/ModelMessageEmbed;-><init>(Lcom/discord/models/domain/ModelMessageAttachment;Lcom/discord/models/domain/ModelMessageEmbed$Item;Lcom/discord/models/domain/ModelMessageEmbed$Item;)V

    return-object v2
.end method

.method public final getFileDrawable(Ljava/lang/String;)I
    .locals 2
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    const-string v0, ""

    if-eqz p1, :cond_0

    const/16 v1, 0x2e

    invoke-static {p1, v1, v0}, Lx/s/r;->substringAfterLast(Ljava/lang/String;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object p1, Lcom/discord/utilities/embed/FileType;->Companion:Lcom/discord/utilities/embed/FileType$Companion;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/embed/FileType$Companion;->getFromExtension(Ljava/lang/String;)Lcom/discord/utilities/embed/FileType;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/utilities/embed/FileType;->getFileDrawable()I

    move-result p1

    goto :goto_0

    :cond_1
    const p1, 0x7f080317

    :goto_0
    return p1
.end method

.method public final getMAX_IMAGE_VIEW_HEIGHT_PX()I
    .locals 1

    sget v0, Lcom/discord/utilities/embed/EmbedResourceUtils;->MAX_IMAGE_VIEW_HEIGHT_PX:I

    return v0
.end method

.method public final getPreviewUrls(Ljava/lang/String;II)[Ljava/lang/String;
    .locals 3

    const-string v0, "originalUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "?width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "&height="

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "res:///"

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p3, v0, v1}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result p3

    const/4 v2, 0x1

    if-nez p3, :cond_1

    const-string p3, ".gif"

    invoke-static {p1, p3, v0, v1}, Lx/s/m;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-array p1, v1, [Ljava/lang/String;

    const-string p3, "&format="

    invoke-static {p2, p3}, Lf/e/c/a/a;->L(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-static {}, Lcom/discord/utilities/string/StringUtilsKt;->getSTATIC_IMAGE_EXTENSION()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v0

    aput-object p2, p1, v2

    goto :goto_1

    :cond_1
    :goto_0
    new-array p1, v2, [Ljava/lang/String;

    aput-object p2, p1, v0

    :goto_1
    return-object p1
.end method
