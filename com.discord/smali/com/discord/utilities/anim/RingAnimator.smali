.class public final Lcom/discord/utilities/anim/RingAnimator;
.super Ljava/lang/Object;
.source "RingAnimator.kt"


# instance fields
.field private final fadeAnim:Landroid/view/animation/AlphaAnimation;

.field private isAnimating:Z

.field private final ringingPredicate:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ringingPredicate"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/anim/RingAnimator;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/discord/utilities/anim/RingAnimator;->ringingPredicate:Lkotlin/jvm/functions/Function0;

    new-instance p1, Landroid/view/animation/AlphaAnimation;

    const/high16 p2, 0x3f800000    # 1.0f

    const v0, 0x3dcccccd    # 0.1f

    invoke-direct {p1, p2, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v0, 0x3e8

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    new-instance p2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p1, p2}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance p2, Lcom/discord/utilities/anim/RingAnimator$$special$$inlined$apply$lambda$1;

    invoke-direct {p2, p0}, Lcom/discord/utilities/anim/RingAnimator$$special$$inlined$apply$lambda$1;-><init>(Lcom/discord/utilities/anim/RingAnimator;)V

    invoke-virtual {p1, p2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object p1, p0, Lcom/discord/utilities/anim/RingAnimator;->fadeAnim:Landroid/view/animation/AlphaAnimation;

    return-void
.end method

.method public static final synthetic access$isAnimating$p(Lcom/discord/utilities/anim/RingAnimator;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/utilities/anim/RingAnimator;->isAnimating:Z

    return p0
.end method

.method public static final synthetic access$setAnimating$p(Lcom/discord/utilities/anim/RingAnimator;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/utilities/anim/RingAnimator;->isAnimating:Z

    return-void
.end method


# virtual methods
.method public final getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/anim/RingAnimator;->view:Landroid/view/View;

    return-object v0
.end method

.method public final onUpdate()V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/anim/RingAnimator;->ringingPredicate:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/discord/utilities/anim/RingAnimator;->isAnimating:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/utilities/anim/RingAnimator;->isAnimating:Z

    iget-object v0, p0, Lcom/discord/utilities/anim/RingAnimator;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/discord/utilities/anim/RingAnimator;->fadeAnim:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/anim/RingAnimator;->fadeAnim:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->cancel()V

    iget-object v0, p0, Lcom/discord/utilities/anim/RingAnimator;->fadeAnim:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->reset()V

    :cond_1
    :goto_0
    return-void
.end method
