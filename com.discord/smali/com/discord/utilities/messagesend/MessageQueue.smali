.class public final Lcom/discord/utilities/messagesend/MessageQueue;
.super Ljava/lang/Object;
.source "MessageQueue.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;,
        Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;,
        Lcom/discord/utilities/messagesend/MessageQueue$Companion;
    }
.end annotation


# static fields
.field private static final Companion:Lcom/discord/utilities/messagesend/MessageQueue$Companion;

.field private static final DEFAULT_MESSAGE_TIMEOUT_MS:J = 0x36ee80L

.field private static final DEFAULT_NETWORK_INITIAL_FAILURE_RETRY_MS:J = 0x1388L

.field private static final DEFAULT_RETRY_MS:J = 0x64L


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final contentResolver:Landroid/content/ContentResolver;

.field private final executorService:Ljava/util/concurrent/ExecutorService;

.field private inFlightRequest:Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;

.field private isDraining:Z

.field private final networkBackoff:Lcom/discord/utilities/networking/Backoff;

.field private final queue:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/discord/utilities/messagesend/MessageRequest;",
            ">;"
        }
    .end annotation
.end field

.field private retrySubscription:Lrx/Subscription;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/messagesend/MessageQueue$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/messagesend/MessageQueue$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/messagesend/MessageQueue;->Companion:Lcom/discord/utilities/messagesend/MessageQueue$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/time/Clock;)V
    .locals 10

    const-string v0, "contentResolver"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executorService"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->contentResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/discord/utilities/messagesend/MessageQueue;->executorService:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/discord/utilities/messagesend/MessageQueue;->clock:Lcom/discord/utilities/time/Clock;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->queue:Ljava/util/ArrayDeque;

    new-instance p1, Lcom/discord/utilities/networking/Backoff;

    const-wide/16 v1, 0x1388

    const-wide/32 v3, 0x36ee80

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1c

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/discord/utilities/networking/Backoff;-><init>(JJIZLcom/discord/utilities/networking/Backoff$Scheduler;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->networkBackoff:Lcom/discord/utilities/networking/Backoff;

    return-void
.end method

.method public static final synthetic access$getExecutorService$p(Lcom/discord/utilities/messagesend/MessageQueue;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->executorService:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

.method public static final synthetic access$getInFlightRequest$p(Lcom/discord/utilities/messagesend/MessageQueue;)Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->inFlightRequest:Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;

    return-object p0
.end method

.method public static final synthetic access$getNetworkBackoff$p(Lcom/discord/utilities/messagesend/MessageQueue;)Lcom/discord/utilities/networking/Backoff;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->networkBackoff:Lcom/discord/utilities/networking/Backoff;

    return-object p0
.end method

.method public static final synthetic access$getQueue$p(Lcom/discord/utilities/messagesend/MessageQueue;)Ljava/util/ArrayDeque;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->queue:Ljava/util/ArrayDeque;

    return-object p0
.end method

.method public static final synthetic access$getRetrySubscription$p(Lcom/discord/utilities/messagesend/MessageQueue;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->retrySubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$handleError(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/error/Error;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/messagesend/MessageQueue;->handleError(Lcom/discord/utilities/error/Error;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    return-void
.end method

.method public static final synthetic access$handleSuccess(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/models/domain/ModelMessage;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/messagesend/MessageQueue;->handleSuccess(Lcom/discord/models/domain/ModelMessage;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    return-void
.end method

.method public static final synthetic access$onDrainingCompleted(Lcom/discord/utilities/messagesend/MessageQueue;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/messagesend/MessageQueue;->onDrainingCompleted()V

    return-void
.end method

.method public static final synthetic access$processNextRequest(Lcom/discord/utilities/messagesend/MessageQueue;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/messagesend/MessageQueue;->processNextRequest()V

    return-void
.end method

.method public static final synthetic access$setInFlightRequest$p(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->inFlightRequest:Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;

    return-void
.end method

.method public static final synthetic access$setRetrySubscription$p(Lcom/discord/utilities/messagesend/MessageQueue;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->retrySubscription:Lrx/Subscription;

    return-void
.end method

.method private final doEdit(Lcom/discord/utilities/messagesend/MessageRequest$Edit;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    sget-object v2, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->getChannelId()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->getMessageId()J

    move-result-wide v6

    new-instance v2, Lcom/discord/restapi/RestAPIParams$Message;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->getContent()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x7c

    const/16 v17, 0x0

    move-object v8, v2

    invoke-direct/range {v8 .. v17}, Lcom/discord/restapi/RestAPIParams$Message;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/restapi/RestAPIParams$Message$Activity;Ljava/util/List;Lcom/discord/restapi/RestAPIParams$Message$MessageReference;Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual/range {v3 .. v8}, Lcom/discord/utilities/rest/RestAPI;->editMessage(JJLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/utilities/messagesend/MessageQueue;

    new-instance v10, Lcom/discord/utilities/messagesend/MessageQueue$doEdit$1;

    invoke-direct {v10, v0, v1}, Lcom/discord/utilities/messagesend/MessageQueue$doEdit$1;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    new-instance v8, Lcom/discord/utilities/messagesend/MessageQueue$doEdit$2;

    invoke-direct {v8, v0, v1}, Lcom/discord/utilities/messagesend/MessageQueue$doEdit$2;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    new-instance v7, Lcom/discord/utilities/messagesend/MessageQueue$doEdit$3;

    move-object/from16 v2, p1

    invoke-direct {v7, v0, v2, v1}, Lcom/discord/utilities/messagesend/MessageQueue$doEdit$3;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageRequest$Edit;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x12

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final doSend(Lcom/discord/utilities/messagesend/MessageRequest$Send;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Send;->validateMessage()Lcom/discord/utilities/messagesend/MessageResult$ValidationError;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;->complete(Lcom/discord/utilities/messagesend/MessageResult;)V

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Send;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v6

    const-string v7, "it"

    const/4 v8, 0x0

    if-eqz v6, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Send;->getActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    const-string v10, "request.activity?.sessionId ?: return@let null"

    invoke-static {v9, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v10, Lcom/discord/restapi/RestAPIParams$Message$Activity;

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelMessage$Activity;->getType()I

    move-result v11

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelMessage$Activity;->getPartyId()Ljava/lang/String;

    move-result-object v6

    const-string v12, "it.partyId"

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v10, v11, v6, v9}, Lcom/discord/restapi/RestAPIParams$Message$Activity;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v10, v8

    :goto_1
    move-object v15, v10

    goto :goto_2

    :cond_3
    move-object v15, v8

    :goto_2
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v6

    if-eqz v6, :cond_4

    new-instance v9, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getGuildId()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getChannelId()Ljava/lang/Long;

    move-result-object v11

    invoke-static {v11}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v12, "it.channelId!!"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getMessageId()Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v9, v10, v11, v12, v6}, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;-><init>(Ljava/lang/Long;JLjava/lang/Long;)V

    move-object/from16 v17, v9

    goto :goto_3

    :cond_4
    move-object/from16 v17, v8

    :goto_3
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v6

    if-eqz v6, :cond_5

    sget-object v9, Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;->Companion:Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions$Companion;

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions$Companion;->create(Lcom/discord/models/domain/ModelAllowedMentions;)Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;

    move-result-object v6

    move-object/from16 v18, v6

    goto :goto_4

    :cond_5
    move-object/from16 v18, v8

    :goto_4
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    goto :goto_5

    :cond_6
    const-string v6, ""

    :goto_5
    move-object v12, v6

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v14, v6

    goto :goto_6

    :cond_7
    move-object v14, v8

    :goto_6
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getStickers()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_8

    new-instance v8, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v6, v7}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_8
    move-object/from16 v16, v8

    new-instance v6, Lcom/discord/restapi/RestAPIParams$Message;

    move-object v11, v6

    invoke-direct/range {v11 .. v18}, Lcom/discord/restapi/RestAPIParams$Message;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/restapi/RestAPIParams$Message$Activity;Ljava/util/List;Lcom/discord/restapi/RestAPIParams$Message$MessageReference;Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;)V

    sget-object v7, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    iget-object v8, v0, Lcom/discord/utilities/messagesend/MessageQueue;->contentResolver:Landroid/content/ContentResolver;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/messagesend/MessageRequest$Send;->getAttachments()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v7, v8, v6, v9}, Lcom/discord/utilities/rest/SendUtils;->getSendPayload(Landroid/content/ContentResolver;Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)Lrx/Observable;

    move-result-object v6

    new-instance v7, Lcom/discord/utilities/messagesend/MessageQueue$doSend$1;

    invoke-direct {v7, v1}, Lcom/discord/utilities/messagesend/MessageQueue$doSend$1;-><init>(Lcom/discord/utilities/messagesend/MessageRequest$Send;)V

    invoke-virtual {v6, v7}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v6

    const-string v7, "SendUtils\n        .getSe\u2026  }\n          }\n        }"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/discord/utilities/messagesend/MessageQueue$doSend$$inlined$filterIs$1;->INSTANCE:Lcom/discord/utilities/messagesend/MessageQueue$doSend$$inlined$filterIs$1;

    invoke-virtual {v6, v7}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v6

    sget-object v7, Lcom/discord/utilities/messagesend/MessageQueue$doSend$$inlined$filterIs$2;->INSTANCE:Lcom/discord/utilities/messagesend/MessageQueue$doSend$$inlined$filterIs$2;

    invoke-virtual {v6, v7}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v6

    const-string v7, "filter { it is T }.map { it as T }"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/discord/utilities/messagesend/MessageQueue$doSend$2;

    invoke-direct {v6, v3}, Lcom/discord/utilities/messagesend/MessageQueue$doSend$2;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {v5, v6}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    const-string v5, "SendUtils\n        .getSe\u2026ge)\n          }\n        }"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object v6

    const-class v7, Lcom/discord/utilities/messagesend/MessageQueue;

    const/4 v8, 0x0

    new-instance v12, Lcom/discord/utilities/messagesend/MessageQueue$doSend$3;

    invoke-direct {v12, v0, v2}, Lcom/discord/utilities/messagesend/MessageQueue$doSend$3;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    new-instance v10, Lcom/discord/utilities/messagesend/MessageQueue$doSend$4;

    invoke-direct {v10, v0, v2}, Lcom/discord/utilities/messagesend/MessageQueue$doSend$4;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    const/4 v11, 0x0

    new-instance v9, Lcom/discord/utilities/messagesend/MessageQueue$doSend$5;

    invoke-direct {v9, v0, v1, v2}, Lcom/discord/utilities/messagesend/MessageQueue$doSend$5;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageRequest$Send;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    const/16 v13, 0x12

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleError(Lcom/discord/utilities/error/Error;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v0

    const-string v1, "error.response"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/utilities/error/Error$Response;->getCode()I

    move-result v0

    const-string v2, "error.response.retryAfterMs ?: DEFAULT_RETRY_MS"

    const-wide/16 v3, 0x64

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/16 v4, 0x4e30

    if-ne v0, v4, :cond_1

    new-instance v0, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error$Response;->getRetryAfterMs()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object v3, p1

    :cond_0
    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;-><init>(J)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object v0

    sget-object v4, Lcom/discord/utilities/error/Error$Type;->RATE_LIMITED:Lcom/discord/utilities/error/Error$Type;

    if-ne v0, v4, :cond_3

    new-instance v0, Lcom/discord/utilities/messagesend/MessageResult$RateLimited;

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error$Response;->getRetryAfterMs()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_2

    move-object v3, p1

    :cond_2
    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/discord/utilities/messagesend/MessageResult$RateLimited;-><init>(J)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/error/Error$Type;->NETWORK:Lcom/discord/utilities/error/Error$Type;

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/discord/utilities/messagesend/MessageResult$NetworkFailure;->INSTANCE:Lcom/discord/utilities/messagesend/MessageResult$NetworkFailure;

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;

    invoke-direct {v0, p1}, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;-><init>(Lcom/discord/utilities/error/Error;)V

    :goto_0
    invoke-virtual {p2, v0}, Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;->complete(Lcom/discord/utilities/messagesend/MessageResult;)V

    return-void
.end method

.method private final handleSuccess(Lcom/discord/models/domain/ModelMessage;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V
    .locals 1

    new-instance v0, Lcom/discord/utilities/messagesend/MessageResult$Success;

    invoke-direct {v0, p1}, Lcom/discord/utilities/messagesend/MessageResult$Success;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {p2, v0}, Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;->complete(Lcom/discord/utilities/messagesend/MessageResult;)V

    return-void
.end method

.method private final onDrainingCompleted()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->isDraining:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->inFlightRequest:Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;

    return-void
.end method

.method private final processNextRequest()V
    .locals 6

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->queue:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->retrySubscription:Lrx/Subscription;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->networkBackoff:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->isPending()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->isDraining:Z

    if-eqz v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->queue:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/messagesend/MessageRequest;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/discord/utilities/messagesend/MessageRequest;->getAttemptTimestamp()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x36ee80

    cmp-long v5, v1, v3

    if-lez v5, :cond_3

    invoke-virtual {v0}, Lcom/discord/utilities/messagesend/MessageRequest;->getOnCompleted()Lkotlin/jvm/functions/Function2;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/messagesend/MessageResult$Timeout;->INSTANCE:Lcom/discord/utilities/messagesend/MessageResult$Timeout;

    iget-object v2, p0, Lcom/discord/utilities/messagesend/MessageQueue;->queue:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->networkBackoff:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    invoke-direct {p0}, Lcom/discord/utilities/messagesend/MessageQueue;->processNextRequest()V

    return-void

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/discord/utilities/messagesend/MessageQueue;->isDraining:Z

    new-instance v1, Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;

    new-instance v2, Lcom/discord/utilities/messagesend/MessageQueue$processNextRequest$listener$1;

    invoke-direct {v2, p0, v0}, Lcom/discord/utilities/messagesend/MessageQueue$processNextRequest$listener$1;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageRequest;)V

    invoke-direct {v1, v2}, Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;-><init>(Lkotlin/jvm/functions/Function1;)V

    instance-of v2, v0, Lcom/discord/utilities/messagesend/MessageRequest$Send;

    if-eqz v2, :cond_4

    check-cast v0, Lcom/discord/utilities/messagesend/MessageRequest$Send;

    invoke-direct {p0, v0, v1}, Lcom/discord/utilities/messagesend/MessageQueue;->doSend(Lcom/discord/utilities/messagesend/MessageRequest$Send;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    goto :goto_0

    :cond_4
    instance-of v2, v0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;

    if-eqz v2, :cond_5

    check-cast v0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;

    invoke-direct {p0, v0, v1}, Lcom/discord/utilities/messagesend/MessageQueue;->doEdit(Lcom/discord/utilities/messagesend/MessageRequest$Edit;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method public final cancel(Ljava/lang/String;)V
    .locals 2

    const-string v0, "requestId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/discord/utilities/messagesend/MessageQueue$cancel$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/utilities/messagesend/MessageQueue$cancel$1;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public final enqueue(Lcom/discord/utilities/messagesend/MessageRequest;)V
    .locals 2

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/discord/utilities/messagesend/MessageQueue$enqueue$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/utilities/messagesend/MessageQueue$enqueue$1;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;Lcom/discord/utilities/messagesend/MessageRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public final handleConnected()V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;-><init>(Lcom/discord/utilities/messagesend/MessageQueue;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method
