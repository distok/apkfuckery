.class public final Lcom/discord/utilities/messagesend/MessageRequest$Edit;
.super Lcom/discord/utilities/messagesend/MessageRequest;
.source "MessageQueue.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/messagesend/MessageRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Edit"
.end annotation


# instance fields
.field private final channelId:J

.field private final content:Ljava/lang/String;

.field private final messageId:J


# direct methods
.method public constructor <init>(JLjava/lang/String;JJ)V
    .locals 7

    const-string v0, "content"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/discord/utilities/messagesend/MessageRequest$Edit$1;->INSTANCE:Lcom/discord/utilities/messagesend/MessageRequest$Edit$1;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v4, p6

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/messagesend/MessageRequest;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function2;JLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->channelId:J

    iput-object p3, p0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->content:Ljava/lang/String;

    iput-wide p4, p0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->messageId:J

    return-void
.end method


# virtual methods
.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->channelId:J

    return-wide v0
.end method

.method public final getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final getMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/messagesend/MessageRequest$Edit;->messageId:J

    return-wide v0
.end method
