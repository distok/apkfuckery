.class public abstract Lcom/discord/utilities/messagesend/MessageRequest;
.super Ljava/lang/Object;
.source "MessageQueue.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/messagesend/MessageRequest$Send;,
        Lcom/discord/utilities/messagesend/MessageRequest$Edit;
    }
.end annotation


# instance fields
.field private final attemptTimestamp:J

.field private final onCompleted:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final requestId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function2;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageRequest;->requestId:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/utilities/messagesend/MessageRequest;->onCompleted:Lkotlin/jvm/functions/Function2;

    iput-wide p3, p0, Lcom/discord/utilities/messagesend/MessageRequest;->attemptTimestamp:J

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function2;JLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/messagesend/MessageRequest;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function2;J)V

    return-void
.end method


# virtual methods
.method public final getAttemptTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/messagesend/MessageRequest;->attemptTimestamp:J

    return-wide v0
.end method

.method public final getOnCompleted()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageRequest;->onCompleted:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageRequest;->requestId:Ljava/lang/String;

    return-object v0
.end method
