.class public final Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;
.super Ljava/lang/Object;
.source "MessageQueue.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/messagesend/MessageQueue;->handleConnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/messagesend/MessageQueue;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/messagesend/MessageQueue;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;->this$0:Lcom/discord/utilities/messagesend/MessageQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;->this$0:Lcom/discord/utilities/messagesend/MessageQueue;

    invoke-static {v0}, Lcom/discord/utilities/messagesend/MessageQueue;->access$getNetworkBackoff$p(Lcom/discord/utilities/messagesend/MessageQueue;)Lcom/discord/utilities/networking/Backoff;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->isPending()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;->this$0:Lcom/discord/utilities/messagesend/MessageQueue;

    invoke-static {v0}, Lcom/discord/utilities/messagesend/MessageQueue;->access$getNetworkBackoff$p(Lcom/discord/utilities/messagesend/MessageQueue;)Lcom/discord/utilities/networking/Backoff;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    iget-object v0, p0, Lcom/discord/utilities/messagesend/MessageQueue$handleConnected$1;->this$0:Lcom/discord/utilities/messagesend/MessageQueue;

    invoke-static {v0}, Lcom/discord/utilities/messagesend/MessageQueue;->access$processNextRequest(Lcom/discord/utilities/messagesend/MessageQueue;)V

    :cond_0
    return-void
.end method
