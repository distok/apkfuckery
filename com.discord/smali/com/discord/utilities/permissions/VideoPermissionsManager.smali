.class public final Lcom/discord/utilities/permissions/VideoPermissionsManager;
.super Ljava/lang/Object;
.source "VideoPermissionsManager.kt"


# instance fields
.field private final permissionsManager:Lcom/discord/utilities/permissions/PermissionsManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/discord/utilities/permissions/VideoPermissionsManager;-><init>(Lcom/discord/utilities/permissions/PermissionsManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/utilities/permissions/PermissionsManager;)V
    .locals 1

    const-string v0, "permissionsManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/permissions/VideoPermissionsManager;->permissionsManager:Lcom/discord/utilities/permissions/PermissionsManager;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/permissions/PermissionsManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    new-instance p1, Lcom/discord/utilities/permissions/PermissionsManager;

    invoke-direct {p1}, Lcom/discord/utilities/permissions/PermissionsManager;-><init>()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/permissions/VideoPermissionsManager;-><init>(Lcom/discord/utilities/permissions/PermissionsManager;)V

    return-void
.end method


# virtual methods
.method public final hasVideoPermission(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Ljava/lang/Long;)Z
    .locals 4

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long p2, v0, v2

    if-nez p2, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_2
    iget-object p1, p0, Lcom/discord/utilities/permissions/VideoPermissionsManager;->permissionsManager:Lcom/discord/utilities/permissions/PermissionsManager;

    const-wide/16 v0, 0x200

    invoke-virtual {p1, v0, v1, p3}, Lcom/discord/utilities/permissions/PermissionsManager;->can(JLjava/lang/Long;)Z

    move-result p1

    return p1
.end method
