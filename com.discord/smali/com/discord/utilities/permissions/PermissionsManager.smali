.class public final Lcom/discord/utilities/permissions/PermissionsManager;
.super Ljava/lang/Object;
.source "PermissionsManager.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final can(JLjava/lang/Long;)Z
    .locals 0

    invoke-static {p1, p2, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p1

    return p1
.end method
