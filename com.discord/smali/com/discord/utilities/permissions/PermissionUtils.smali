.class public Lcom/discord/utilities/permissions/PermissionUtils;
.super Ljava/lang/Object;
.source "PermissionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static applyEveryone(JLjava/util/Map;)J
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;)J"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelGuildRole;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide p0

    return-wide p0

    :cond_1
    const-wide/32 p0, 0x637de41

    return-wide p0
.end method

.method private static applyEveryoneOverwrites(JLjava/util/Map;J)J
    .locals 0
    .param p2    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;J)J"
        }
    .end annotation

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelPermissionOverwrite;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getDeny()J

    move-result-wide p1

    and-long/2addr p1, p3

    xor-long/2addr p1, p3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getAllow()J

    move-result-wide p3

    or-long/2addr p3, p1

    :cond_0
    return-wide p3
.end method

.method private static applyRoleOverwrites(Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;JJ)J
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;JJ)J"
        }
    .end annotation

    const-wide/16 v0, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    move-wide v2, v0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelPermissionOverwrite;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getAllow()J

    move-result-wide v5

    or-long/2addr v0, v5

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getDeny()J

    move-result-wide v4

    or-long/2addr v2, v4

    goto :goto_0

    :cond_1
    move-wide v7, v0

    move-wide v0, v2

    move-wide v2, v7

    goto :goto_1

    :cond_2
    move-wide v2, v0

    :goto_1
    and-long/2addr v0, p2

    xor-long/2addr p2, v0

    or-long/2addr p2, v2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelPermissionOverwrite;

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getDeny()J

    move-result-wide p4

    and-long/2addr p4, p2

    xor-long p1, p2, p4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getAllow()J

    move-result-wide p3

    or-long p2, p1, p3

    :cond_3
    return-wide p2
.end method

.method private static applyRoles(Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;J)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;J)J"
        }
    .end annotation

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    if-eqz p1, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuildRole;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v0

    or-long/2addr p2, v0

    goto :goto_0

    :cond_2
    return-wide p2
.end method

.method public static can(JJJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)Z
    .locals 0
    .param p10    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJJ",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;)Z"
        }
    .end annotation

    invoke-static/range {p2 .. p10}, Lcom/discord/utilities/permissions/PermissionUtils;->computePermissions(JJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)J

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p0

    return p0
.end method

.method public static can(JLjava/lang/Long;)Z
    .locals 2
    .param p2    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p2, :cond_0

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    and-long/2addr v0, p0

    cmp-long p2, v0, p0

    if-nez p2, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static canAndIsElevated(JLjava/lang/Long;ZI)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {p0, p1, p3, p4}, Lcom/discord/utilities/permissions/PermissionUtils;->isElevated(JZI)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static canEveryone(JJLjava/util/Map;Ljava/util/Map;)Z
    .locals 2
    .param p4    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;)Z"
        }
    .end annotation

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p4, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/models/domain/ModelGuildRole;

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v0

    and-long/2addr v0, p0

    cmp-long p2, v0, p0

    if-eqz p2, :cond_0

    return p3

    :cond_0
    invoke-interface {p5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/discord/models/domain/ModelPermissionOverwrite;

    invoke-static {p4, p0, p1}, Lcom/discord/models/domain/ModelPermissionOverwrite;->denies(Lcom/discord/models/domain/ModelPermissionOverwrite;J)Z

    move-result p4

    if-eqz p4, :cond_1

    return p3

    :cond_2
    const/4 p0, 0x1

    return p0
.end method

.method public static canManageGuildMembers(ZZILjava/lang/Long;)Z
    .locals 2
    .param p3    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p0, :cond_1

    const-wide/16 v0, 0x8

    invoke-static {v0, v1, p3, p1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result p0

    if-nez p0, :cond_1

    const-wide/16 v0, 0x2

    invoke-static {v0, v1, p3, p1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result p0

    if-nez p0, :cond_1

    const-wide/16 v0, 0x4

    invoke-static {v0, v1, p3, p1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result p0

    if-nez p0, :cond_1

    const-wide/32 v0, 0x10000000

    invoke-static {v0, v1, p3, p1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result p0

    if-nez p0, :cond_1

    const-wide/32 p0, 0x8000000

    invoke-static {p0, p1, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static canRole(JLcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelPermissionOverwrite;)Z
    .locals 4
    .param p2    # Lcom/discord/models/domain/ModelGuildRole;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/discord/models/domain/ModelPermissionOverwrite;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    :cond_0
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v1

    and-long/2addr v1, p0

    const/4 p2, 0x1

    cmp-long v3, v1, p0

    if-nez v3, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-static {p3, p0, p1}, Lcom/discord/models/domain/ModelPermissionOverwrite;->allows(Lcom/discord/models/domain/ModelPermissionOverwrite;J)Z

    move-result v2

    invoke-static {p3, p0, p1}, Lcom/discord/models/domain/ModelPermissionOverwrite;->denies(Lcom/discord/models/domain/ModelPermissionOverwrite;J)Z

    move-result p0

    if-nez v1, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    if-nez p0, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public static computePermissions(JJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)J
    .locals 6
    .param p7    # Ljava/util/Map;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/Map;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;)J"
        }
    .end annotation

    const-wide/32 v0, 0x7ff7feff

    cmp-long v2, p4, p0

    if-nez v2, :cond_0

    return-wide v0

    :cond_0
    invoke-static {p2, p3, p7}, Lcom/discord/utilities/permissions/PermissionUtils;->applyEveryone(JLjava/util/Map;)J

    move-result-wide p4

    invoke-static {p6, p7, p4, p5}, Lcom/discord/utilities/permissions/PermissionUtils;->applyRoles(Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;J)J

    move-result-wide p4

    const-wide/16 v2, 0x8

    and-long v4, p4, v2

    cmp-long p7, v4, v2

    if-nez p7, :cond_1

    return-wide v0

    :cond_1
    if-eqz p8, :cond_2

    invoke-interface {p8}, Ljava/util/Map;->isEmpty()Z

    move-result p7

    if-nez p7, :cond_2

    invoke-static {p2, p3, p8, p4, p5}, Lcom/discord/utilities/permissions/PermissionUtils;->applyEveryoneOverwrites(JLjava/util/Map;J)J

    move-result-wide v2

    move-object v0, p6

    move-object v1, p8

    move-wide v4, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/permissions/PermissionUtils;->applyRoleOverwrites(Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;JJ)J

    move-result-wide p4

    :cond_2
    return-wide p4
.end method

.method public static computePermissions(JJJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)J
    .locals 10
    .param p7    # Ljava/util/Map;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/Map;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;)J"
        }
    .end annotation

    move-object/from16 v0, p6

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuildMember$Computed;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v7, v0

    move-wide v1, p0

    move-wide v3, p2

    move-wide v5, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/permissions/PermissionUtils;->computePermissions(JJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;)Z
    .locals 2
    .param p1    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyChannel()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isCategory()Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const-wide/32 v0, 0x100000

    goto :goto_1

    :cond_2
    :goto_0
    const-wide/16 v0, 0x400

    :goto_1
    invoke-static {v0, v1, p1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p0

    return p0
.end method

.method public static hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-static {p0, p1}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;)Z

    move-result p0

    return p0
.end method

.method public static hasAccessWrite(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;)Z
    .locals 2
    .param p1    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result p0

    if-nez p0, :cond_1

    const-wide/16 v0, 0xc00

    invoke-static {v0, v1, p1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static hasBypassSlowmodePermissions(Ljava/lang/Long;)Z
    .locals 2
    .param p0    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const-wide/16 v0, 0x10

    invoke-static {v0, v1, p0}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x2000

    invoke-static {v0, v1, p0}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static isElevated(JZI)Z
    .locals 3

    const-wide/32 v0, 0x1000203e

    and-long/2addr v0, p0

    cmp-long v2, v0, p0

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    invoke-static {p2, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->isElevated(ZI)Z

    move-result p0

    return p0
.end method

.method public static isElevated(ZI)Z
    .locals 0

    if-nez p1, :cond_0

    const/4 p0, 0x1

    :cond_0
    return p0
.end method
