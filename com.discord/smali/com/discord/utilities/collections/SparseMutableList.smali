.class public final Lcom/discord/utilities/collections/SparseMutableList;
.super Ljava/lang/Object;
.source "SparseMutableList.kt"

# interfaces
.implements Ljava/util/List;
.implements Lx/m/c/x/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/collections/SparseMutableList$Chunk;,
        Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/List<",
        "TT;>;",
        "Lx/m/c/x/c;"
    }
.end annotation


# instance fields
.field private final chunks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final expectedChunkSize:I

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v0, v0, v1, v2}, Lcom/discord/utilities/collections/SparseMutableList;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/discord/utilities/collections/SparseMutableList;->expectedChunkSize:I

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    iput p1, p0, Lcom/discord/utilities/collections/SparseMutableList;->size:I

    return-void
.end method

.method public synthetic constructor <init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x5

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList;-><init>(II)V

    return-void
.end method

.method private final addChunk(ILcom/discord/utilities/collections/SparseMutableList$Chunk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->sort(Ljava/util/List;)V

    return-void
.end method

.method private final addChunk(Lcom/discord/utilities/collections/SparseMutableList$Chunk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->sort(Ljava/util/List;)V

    return-void
.end method

.method private final decrementChunksFromIndex(I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-ltz p1, :cond_1

    if-ge v0, p1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-gt p1, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->decrementStartIndex()V

    if-eq p1, v0, :cond_1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private final getChunkIndex(I)I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->containsListIndex(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    return v1
.end method

.method private final getFirstChunkIndexAfter(I)I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->beginsAfterListIndex(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    return v1
.end method

.method private final incrementChunksFromIndex(I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-ltz p1, :cond_1

    if-ge v0, p1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-gt p1, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->incrementStartIndex()V

    if-eq p1, v0, :cond_1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private final resolveChunks()V
    .locals 5

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->sort(Ljava/util/List;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    sget-object v4, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->Companion:Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;

    invoke-virtual {v4, v1, v2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;->tryMergeChunks(Lcom/discord/utilities/collections/SparseMutableList$Chunk;Lcom/discord/utilities/collections/SparseMutableList$Chunk;)Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/discord/utilities/collections/SparseMutableList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    if-gt p1, v0, :cond_6

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getChunkIndex(I)I

    move-result v0

    const/4 v1, -0x1

    if-eqz p2, :cond_3

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v1, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->addAtListIndex(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/utilities/collections/SparseMutableList;->incrementChunksFromIndex(I)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->Companion:Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;

    iget v2, p0, Lcom/discord/utilities/collections/SparseMutableList;->expectedChunkSize:I

    invoke-virtual {v0, p2, p1, v2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;->create(Ljava/lang/Object;II)Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    move-result-object p2

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getFirstChunkIndexAfter(I)I

    move-result p1

    if-ne p1, v1, :cond_2

    invoke-direct {p0, p2}, Lcom/discord/utilities/collections/SparseMutableList;->addChunk(Lcom/discord/utilities/collections/SparseMutableList$Chunk;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList;->addChunk(ILcom/discord/utilities/collections/SparseMutableList$Chunk;)V

    add-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->incrementChunksFromIndex(I)V

    goto :goto_0

    :cond_3
    if-ltz v0, :cond_4

    iget-object p2, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->addAtListIndex(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/utilities/collections/SparseMutableList;->incrementChunksFromIndex(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getFirstChunkIndexAfter(I)I

    move-result p1

    if-eq p1, v1, :cond_5

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->incrementChunksFromIndex(I)V

    :cond_5
    :goto_0
    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->setSize(I)V

    invoke-direct {p0}, Lcom/discord/utilities/collections/SparseMutableList;->resolveChunks()V

    return-void

    :cond_6
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    return p1
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection<",
            "+TT;>;)Z"
        }
    .end annotation

    const-string p1, "elements"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+TT;>;)Z"
        }
    .end annotation

    const-string v0, "elements"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->addAll(ILjava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/discord/utilities/collections/SparseMutableList;->setSize(I)V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "elements"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result p1

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v1

    const/4 v2, 0x0

    if-le p1, v1, :cond_0

    return v2

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    return v2
.end method

.method public final deepCopy()Lcom/discord/utilities/collections/SparseMutableList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/utilities/collections/SparseMutableList;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    iget-object v3, v0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-virtual {v2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->deepCopy()Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final deepCopy(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/SparseMutableList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)",
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "TR;>;"
        }
    .end annotation

    const-string/jumbo v0, "transform"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/utilities/collections/SparseMutableList;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    iget-object v3, v0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->deepCopy(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    invoke-static {p0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-gt p1, v0, :cond_2

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getChunkIndex(I)I

    move-result v0

    const/4 v1, 0x0

    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->getAtListIndex(I)Ljava/lang/Object;

    move-result-object v1

    :cond_1
    return-object v1

    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "index "

    const-string v2, " invalid in list of size "

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->size:I

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 5

    const/4 v0, -0x1

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->firstListIndexOf(Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_0

    return v2

    :cond_1
    return v0

    :cond_2
    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    if-ltz v1, :cond_4

    invoke-static {v3, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    return v1

    :cond_3
    move v1, v4

    goto :goto_0

    :cond_4
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 p1, 0x0

    throw p1

    :cond_5
    return v0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;-><init>(Lcom/discord/utilities/collections/SparseMutableList;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 3

    const/4 v0, -0x1

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    const-string v2, "$this$asReversed"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lx/h/q;

    invoke-direct {v2, v1}, Lx/h/q;-><init>(Ljava/util/List;)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->lastListIndexOf(Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_0

    return v2

    :cond_1
    return v0

    :cond_2
    invoke-static {p0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v1

    :goto_0
    if-ltz v1, :cond_4

    invoke-virtual {p0, v1}, Lcom/discord/utilities/collections/SparseMutableList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    return v1

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_4
    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;-><init>(Lcom/discord/utilities/collections/SparseMutableList;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator<",
            "TT;>;"
        }
    .end annotation

    new-instance p1, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p1, p0, v0, v1, v2}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;-><init>(Lcom/discord/utilities/collections/SparseMutableList;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public final bridge remove(I)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->removeAt(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;-><init>(Lcom/discord/utilities/collections/SparseMutableList;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_0
    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;->remove()V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "elements"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;-><init>(Lcom/discord/utilities/collections/SparseMutableList;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList$SparseMutableListIterator;->remove()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public removeAt(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    if-ltz p1, :cond_2

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getChunkIndex(I)I

    move-result v0

    const/4 v1, -0x1

    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->removeAtListIndex(I)Ljava/lang/Object;

    move-result-object p1

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/utilities/collections/SparseMutableList;->decrementChunksFromIndex(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getFirstChunkIndexAfter(I)I

    move-result p1

    if-eq p1, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->decrementChunksFromIndex(I)V

    :cond_1
    move-object p1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/discord/utilities/collections/SparseMutableList;->setSize(I)V

    invoke-direct {p0}, Lcom/discord/utilities/collections/SparseMutableList;->resolveChunks()V

    return-object p1

    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "index: "

    const-string v2, " -- size: "

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "elements"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/discord/utilities/collections/SparseMutableList;->setSize(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->getChunkIndex(I)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->setAtListIndex(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/utilities/collections/SparseMutableList;->resolveChunks()V

    return-object p1

    :cond_1
    if-eqz p2, :cond_2

    sget-object v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->Companion:Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;

    iget v1, p0, Lcom/discord/utilities/collections/SparseMutableList;->expectedChunkSize:I

    invoke-virtual {v0, p2, p1, v1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;->create(Ljava/lang/Object;II)Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->addChunk(Lcom/discord/utilities/collections/SparseMutableList$Chunk;)V

    invoke-direct {p0}, Lcom/discord/utilities/collections/SparseMutableList;->resolveChunks()V

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public setSize(I)V
    .locals 2

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->size:I

    if-le p1, v0, :cond_0

    iput p1, p0, Lcom/discord/utilities/collections/SparseMutableList;->size:I

    goto :goto_1

    :cond_0
    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList;->chunks:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {v1, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->removeAfterInclusive(I)V

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/discord/utilities/collections/SparseMutableList;->size:I

    invoke-direct {p0}, Lcom/discord/utilities/collections/SparseMutableList;->resolveChunks()V

    :cond_2
    :goto_1
    return-void
.end method

.method public final bridge size()I
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList;->getSize()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    invoke-static {p0}, Lx/m/c/f;->toArray(Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    invoke-static {p0, p1}, Lx/m/c/f;->toArray(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
