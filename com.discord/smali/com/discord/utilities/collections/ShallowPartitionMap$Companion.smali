.class public final Lcom/discord/utilities/collections/ShallowPartitionMap$Companion;
.super Ljava/lang/Object;
.source "ShallowPartitionMap.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/collections/ShallowPartitionMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/collections/ShallowPartitionMap$Companion;-><init>()V

    return-void
.end method

.method public static synthetic create$default(Lcom/discord/utilities/collections/ShallowPartitionMap$Companion;IIILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/utilities/collections/ShallowPartitionMap;
    .locals 2

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/16 p2, 0x64

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    if-lez p1, :cond_1

    move p3, p1

    goto :goto_0

    :cond_1
    const/4 p3, 0x1

    :goto_0
    int-to-float p3, p3

    int-to-float p6, p2

    div-float/2addr p3, p6

    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float p3, v0

    invoke-static {p3}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    invoke-direct {p0, p3}, Lcom/discord/utilities/collections/ShallowPartitionMap$Companion;->getHashCodePartitionStrategy(I)Lkotlin/jvm/functions/Function1;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/collections/ShallowPartitionMap$Companion;->create(IIILkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/ShallowPartitionMap;

    move-result-object p0

    return-object p0
.end method

.method private final getHashCodePartitionStrategy(I)Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lkotlin/jvm/functions/Function1<",
            "TK;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/collections/ShallowPartitionMap$Companion$getHashCodePartitionStrategy$1;

    invoke-direct {v0, p1}, Lcom/discord/utilities/collections/ShallowPartitionMap$Companion$getHashCodePartitionStrategy$1;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final create(IIILkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/ShallowPartitionMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(III",
            "Lkotlin/jvm/functions/Function1<",
            "-TK;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/discord/utilities/collections/ShallowPartitionMap<",
            "TK;TV;>;"
        }
    .end annotation

    const-string p1, "partitionStrategy"

    invoke-static {p4, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lkotlin/ranges/IntRange;

    const/4 v0, 0x0

    invoke-direct {p1, v0, p3}, Lkotlin/ranges/IntRange;-><init>(II)V

    new-instance p3, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lx/h/o;

    invoke-virtual {v0}, Lx/h/o;->nextInt()I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(I)V

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/utilities/collections/ShallowPartitionMap;

    invoke-direct {p1, p3, p4}, Lcom/discord/utilities/collections/ShallowPartitionMap;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    return-object p1
.end method
