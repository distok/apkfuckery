.class public final Lcom/discord/utilities/collections/SparseMutableList$Chunk;
.super Ljava/lang/Object;
.source "SparseMutableList.kt"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/collections/SparseMutableList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Chunk"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
        "TT;>;>;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;


# instance fields
.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private startIndex:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->Companion:Lcom/discord/utilities/collections/SparseMutableList$Chunk$Companion;

    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "list"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    iput-object p2, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getStartIndex$p(Lcom/discord/utilities/collections/SparseMutableList$Chunk;)I
    .locals 0

    iget p0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    return p0
.end method

.method public static final synthetic access$setStartIndex$p(Lcom/discord/utilities/collections/SparseMutableList$Chunk;I)V
    .locals 0

    iput p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    return-void
.end method

.method private final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    return v0
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/collections/SparseMutableList$Chunk;ILjava/util/List;ILjava/lang/Object;)Lcom/discord/utilities/collections/SparseMutableList$Chunk;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->copy(ILjava/util/List;)Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final addAtListIndex(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    sub-int/2addr p1, v0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->incrementStartIndex()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final beginsAfterListIndex(I)Z
    .locals 1

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public compareTo(Lcom/discord/utilities/collections/SparseMutableList$Chunk;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TT;>;)I"
        }
    .end annotation

    const-string v0, "other"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    iget p1, p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    sub-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->compareTo(Lcom/discord/utilities/collections/SparseMutableList$Chunk;)I

    move-result p1

    return p1
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    return-object v0
.end method

.method public final containsListIndex(I)Z
    .locals 3

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-static {v1}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v1

    iget v2, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    add-int/2addr v1, v2

    if-le v0, p1, :cond_0

    goto :goto_0

    :cond_0
    if-lt v1, p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public final copy(ILjava/util/List;)Lcom/discord/utilities/collections/SparseMutableList$Chunk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "TT;>;)",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "list"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    invoke-direct {v0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;-><init>(ILjava/util/List;)V

    return-object v0
.end method

.method public final decrementStartIndex()V
    .locals 1

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    return-void
.end method

.method public final deepCopy()Lcom/discord/utilities/collections/SparseMutableList$Chunk;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    iget v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v1, v2}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;-><init>(ILjava/util/List;)V

    return-object v0
.end method

.method public final deepCopy(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/SparseMutableList$Chunk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)",
            "Lcom/discord/utilities/collections/SparseMutableList$Chunk<",
            "TR;>;"
        }
    .end annotation

    const-string/jumbo v0, "transform"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    iget v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    invoke-direct {p1, v1, v0}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;-><init>(ILjava/util/List;)V

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    iget v1, p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final firstListIndexOf(Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-ltz v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    add-int/2addr p1, v1

    return p1

    :cond_0
    if-eq v1, v0, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public final getAtListIndex(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    iget v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    return-object v0
.end method

.method public final getSize()I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final incrementStartIndex()V
    .locals 1

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final lastListIndexOf(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    add-int/2addr p1, v0

    return p1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public final removeAfterInclusive(I)V
    .locals 2

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-le p1, v0, :cond_0

    return-void

    :cond_0
    if-gtz p1, :cond_1

    iget-object p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    return-void
.end method

.method public final removeAtListIndex(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final setAtListIndex(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    iget v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    sub-int/2addr p1, v0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->incrementStartIndex()V

    iget-object p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final setList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Chunk(startIndex="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->startIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/collections/SparseMutableList$Chunk;->list:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
