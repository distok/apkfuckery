.class public final Lcom/discord/utilities/rx/Holder;
.super Ljava/lang/Object;
.source "ObservableCombineLatestOverloads.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "T8:",
        "Ljava/lang/Object;",
        "T9:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final t1:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT1;"
        }
    .end annotation
.end field

.field private final t2:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT2;"
        }
    .end annotation
.end field

.field private final t3:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT3;"
        }
    .end annotation
.end field

.field private final t4:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT4;"
        }
    .end annotation
.end field

.field private final t5:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT5;"
        }
    .end annotation
.end field

.field private final t6:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT6;"
        }
    .end annotation
.end field

.field private final t7:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT7;"
        }
    .end annotation
.end field

.field private final t8:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT8;"
        }
    .end annotation
.end field

.field private final t9:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT9;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;TT3;TT4;TT5;TT6;TT7;TT8;TT9;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/rx/Holder;->t1:Ljava/lang/Object;

    iput-object p2, p0, Lcom/discord/utilities/rx/Holder;->t2:Ljava/lang/Object;

    iput-object p3, p0, Lcom/discord/utilities/rx/Holder;->t3:Ljava/lang/Object;

    iput-object p4, p0, Lcom/discord/utilities/rx/Holder;->t4:Ljava/lang/Object;

    iput-object p5, p0, Lcom/discord/utilities/rx/Holder;->t5:Ljava/lang/Object;

    iput-object p6, p0, Lcom/discord/utilities/rx/Holder;->t6:Ljava/lang/Object;

    iput-object p7, p0, Lcom/discord/utilities/rx/Holder;->t7:Ljava/lang/Object;

    iput-object p8, p0, Lcom/discord/utilities/rx/Holder;->t8:Ljava/lang/Object;

    iput-object p9, p0, Lcom/discord/utilities/rx/Holder;->t9:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getT1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT1;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t1:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT2()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT2;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t2:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT3()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT3;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t3:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT4()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT4;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t4:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT5()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT5;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t5:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT6()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT6;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t6:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT7()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT7;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t7:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT8()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT8;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t8:Ljava/lang/Object;

    return-object v0
.end method

.method public final getT9()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT9;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rx/Holder;->t9:Ljava/lang/Object;

    return-object v0
.end method
