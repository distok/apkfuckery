.class public final Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;
.super Ljava/lang/Object;
.source "LeadingEdgeThrottle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/rx/LeadingEdgeThrottle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DebounceState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public emitting:Z

.field public hasValue:Z

.field public index:I

.field public terminate:Z

.field public value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->value:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->hasValue:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public emit(ILrx/Subscriber;Lrx/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/Subscriber<",
            "TT;>;",
            "Lrx/Subscriber<",
            "*>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->emitting:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->hasValue:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->index:I

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->value:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->value:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->hasValue:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->emitting:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-interface {p2, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-enter p0

    :try_start_2
    iget-boolean p1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->terminate:Z

    if-nez p1, :cond_1

    iput-boolean v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->emitting:Z

    monitor-exit p0

    return-void

    :cond_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {p2}, Lg0/g;->onCompleted()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    :catchall_1
    move-exception p2

    invoke-static {p2, p3, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void

    :cond_2
    :goto_0
    :try_start_4
    monitor-exit p0

    return-void

    :catchall_2
    move-exception p1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw p1
.end method

.method public emitAndComplete(Lrx/Subscriber;Lrx/Subscriber;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "TT;>;",
            "Lrx/Subscriber<",
            "*>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->emitting:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->terminate:Z

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->value:Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->hasValue:Z

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->value:Ljava/lang/Object;

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->hasValue:Z

    iput-boolean v1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->emitting:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_1

    :try_start_1
    invoke-interface {p1, v0}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {p1, p2, v0}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void

    :cond_1
    :goto_0
    invoke-interface {p1}, Lg0/g;->onCompleted()V

    return-void

    :catchall_1
    move-exception p1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1
.end method

.method public declared-synchronized next(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->value:Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->hasValue:Z

    iget v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->index:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/discord/utilities/rx/LeadingEdgeThrottle$DebounceState;->index:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
