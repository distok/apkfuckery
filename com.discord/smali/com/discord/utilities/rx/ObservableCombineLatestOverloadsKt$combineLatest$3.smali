.class public final Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt$combineLatest$3;
.super Ljava/lang/Object;
.source "ObservableCombineLatestOverloads.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function12;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/utilities/rx/Holder<",
        "TT1;TT2;TT3;TT4;TT5;TT6;TT7;TT8;TT9;>;TT10;TT11;TT12;TR;>;"
    }
.end annotation


# instance fields
.field public final synthetic $combineFunction:Lkotlin/jvm/functions/Function12;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function12;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt$combineLatest$3;->$combineFunction:Lkotlin/jvm/functions/Function12;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/utilities/rx/Holder;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/rx/Holder<",
            "TT1;TT2;TT3;TT4;TT5;TT6;TT7;TT8;TT9;>;TT10;TT11;TT12;)TR;"
        }
    .end annotation

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt$combineLatest$3;->$combineFunction:Lkotlin/jvm/functions/Function12;

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT1()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT2()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT3()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT4()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT5()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT6()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT7()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT8()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {p1}, Lcom/discord/utilities/rx/Holder;->getT9()Ljava/lang/Object;

    move-result-object v10

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    invoke-interface/range {v1 .. v13}, Lkotlin/jvm/functions/Function12;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/rx/Holder;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt$combineLatest$3;->call(Lcom/discord/utilities/rx/Holder;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
