.class public final Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;
.super Lx/m/c/k;
.source "DiffCreator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/recycler/DiffCreator;->dispatchDiffUpdatesAsync(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;Lcom/discord/app/AppComponent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/recyclerview/widget/DiffUtil$DiffResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

.field public final synthetic $newItems:Ljava/util/List;

.field public final synthetic $setItems:Lkotlin/jvm/functions/Function1;

.field public final synthetic this$0:Lcom/discord/utilities/recycler/DiffCreator;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/recycler/DiffCreator;Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->this$0:Lcom/discord/utilities/recycler/DiffCreator;

    iput-object p2, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->$adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    iput-object p3, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->$setItems:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->$newItems:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->invoke(Landroidx/recyclerview/widget/DiffUtil$DiffResult;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroidx/recyclerview/widget/DiffUtil$DiffResult;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->this$0:Lcom/discord/utilities/recycler/DiffCreator;

    iget-object v1, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->$adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    iget-object v2, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->$setItems:Lkotlin/jvm/functions/Function1;

    iget-object v3, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$2;->$newItems:Ljava/util/List;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/discord/utilities/recycler/DiffCreator;->access$setItemsAndDispatchUpdate(Lcom/discord/utilities/recycler/DiffCreator;Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Landroidx/recyclerview/widget/DiffUtil$DiffResult;)V

    return-void
.end method
