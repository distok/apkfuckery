.class public final Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;
.super Landroidx/recyclerview/widget/LinearLayoutManager;
.source "SelfHealingLinearLayoutManager.kt"


# instance fields
.field private final adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$Adapter;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;IZ)V"
        }
    .end annotation

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object p1, p0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$Adapter;IZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x1

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$Adapter;IZ)V

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;)Landroidx/recyclerview/widget/RecyclerView$Adapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    return-object p0
.end method

.method public static final synthetic access$getRecyclerView$p(Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method private final logWarning(Ljava/lang/Throwable;)V
    .locals 3

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Triggered in adapter: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->adapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget-object p1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "Invalid adapter configuration."

    invoke-virtual {p1, v1, v0}, Lcom/discord/app/AppLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private final resetAdapter(Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager$resetAdapter$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager$resetAdapter$1;-><init>(Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    invoke-direct {p0, p1}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->logWarning(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public onLayoutChildren(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 4

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "state"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->onLayoutChildren(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    const-string/jumbo v3, "trying to unhide"

    invoke-static {p2, v3, v2, v1}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result p2

    if-eq p2, v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_2

    const-string/jumbo v3, "trying to hide"

    invoke-static {p2, v3, v2, v1}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result p2

    if-ne p2, v0, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->resetAdapter(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    throw p1

    :catch_1
    move-exception p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->resetAdapter(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;->resetAdapter(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
