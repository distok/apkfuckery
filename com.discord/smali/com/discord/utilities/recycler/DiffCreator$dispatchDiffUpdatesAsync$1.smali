.class public final Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;
.super Ljava/lang/Object;
.source "DiffCreator.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/recycler/DiffCreator;->dispatchDiffUpdatesAsync(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;Lcom/discord/app/AppComponent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroidx/recyclerview/widget/DiffUtil$DiffResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $newItems:Ljava/util/List;

.field public final synthetic $oldItems:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/utilities/recycler/DiffCreator;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/recycler/DiffCreator;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->this$0:Lcom/discord/utilities/recycler/DiffCreator;

    iput-object p2, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->$oldItems:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->$newItems:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Landroidx/recyclerview/widget/DiffUtil$DiffResult;
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->this$0:Lcom/discord/utilities/recycler/DiffCreator;

    iget-object v1, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->$oldItems:Ljava/util/List;

    iget-object v2, p0, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->$newItems:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/recycler/DiffCreator;->calculateDiffResult(Ljava/util/List;Ljava/util/List;)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/recycler/DiffCreator$dispatchDiffUpdatesAsync$1;->call()Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    return-object v0
.end method
