.class public final Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;
.super Landroidx/recyclerview/widget/DiffUtil$Callback;
.source "DiffCreator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/recycler/DiffCreator;->createDiffUtilCallback(Ljava/util/List;Ljava/util/List;)Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $newItems:Ljava/util/List;

.field public final synthetic $oldItems:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$oldItems:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$newItems:Ljava/util/List;

    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$oldItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/recycler/DiffKeyProvider;

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$newItems:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/utilities/recycler/DiffKeyProvider;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public areItemsTheSame(II)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$oldItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/recycler/DiffKeyProvider;

    invoke-interface {p1}, Lcom/discord/utilities/recycler/DiffKeyProvider;->getKey()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$newItems:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/utilities/recycler/DiffKeyProvider;

    invoke-interface {p2}, Lcom/discord/utilities/recycler/DiffKeyProvider;->getKey()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getNewListSize()I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$newItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;->$oldItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
