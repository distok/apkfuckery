.class public final Lcom/discord/utilities/StoreUIEventHandler;
.super Ljava/lang/Object;
.source "StoreUIEventHandler.kt"


# instance fields
.field private final context:Landroid/content/Context;

.field private final mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/stores/StoreMediaEngine;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaEngineStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/StoreUIEventHandler;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/utilities/StoreUIEventHandler;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-direct {p0}, Lcom/discord/utilities/StoreUIEventHandler;->subscribeToStoreEvents()V

    return-void
.end method

.method public static final synthetic access$handleKrispStatusEvent(Lcom/discord/utilities/StoreUIEventHandler;Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/StoreUIEventHandler;->handleKrispStatusEvent(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    return-void
.end method

.method private final handleKrispStatusEvent(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const p1, 0x7f121069

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const p1, 0x7f12106e

    goto :goto_0

    :cond_2
    const p1, 0x7f12106d

    :goto_0
    iget-object v0, p0, Lcom/discord/utilities/StoreUIEventHandler;->context:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {v0, p1, v1, v2, v3}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    return-void
.end method

.method private final subscribeToStoreEvents()V
    .locals 10

    iget-object v0, p0, Lcom/discord/utilities/StoreUIEventHandler;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaEngine;->onKrispStatusEvent()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/utilities/StoreUIEventHandler;

    new-instance v7, Lcom/discord/utilities/StoreUIEventHandler$subscribeToStoreEvents$1;

    invoke-direct {v7, p0}, Lcom/discord/utilities/StoreUIEventHandler$subscribeToStoreEvents$1;-><init>(Lcom/discord/utilities/StoreUIEventHandler;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
