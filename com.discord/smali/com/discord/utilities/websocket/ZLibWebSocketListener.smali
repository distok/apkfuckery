.class public final Lcom/discord/utilities/websocket/ZLibWebSocketListener;
.super Lokhttp3/WebSocketListener;
.source "ZLibWebSocketListener.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;,
        Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;
    }
.end annotation


# instance fields
.field private final inflater:Ljava/util/zip/Inflater;

.field private final listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

.field private final loggingInflater:Ljava/util/zip/Inflater;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lokhttp3/WebSocketListener;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    new-instance p1, Ljava/util/zip/Inflater;

    invoke-direct {p1}, Ljava/util/zip/Inflater;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->loggingInflater:Ljava/util/zip/Inflater;

    new-instance p1, Ljava/util/zip/Inflater;

    invoke-direct {p1}, Ljava/util/zip/Inflater;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->inflater:Ljava/util/zip/Inflater;

    return-void
.end method

.method private final resetInflaters()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->inflater:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->reset()V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->loggingInflater:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->reset()V

    return-void
.end method


# virtual methods
.method public final getListener()Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    return-object v0
.end method

.method public onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->resetInflaters()V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v0, p1, p2, p3}, Lokhttp3/WebSocketListener;->onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V

    return-void
.end method

.method public onClosing(Lokhttp3/WebSocket;ILjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->resetInflaters()V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v0, p1, p2, p3}, Lokhttp3/WebSocketListener;->onClosing(Lokhttp3/WebSocket;ILjava/lang/String;)V

    return-void
.end method

.method public onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "t"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->resetInflaters()V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v0, p1, p2, p3}, Lokhttp3/WebSocketListener;->onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V

    return-void
.end method

.method public onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "text"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;->onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V

    return-void
.end method

.method public onMessage(Lokhttp3/WebSocket;Lokio/ByteString;)V
    .locals 4

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bytes"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;->getRawMessageHandler()Lcom/discord/utilities/websocket/RawMessageHandler;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;

    iget-object v2, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->loggingInflater:Ljava/util/zip/Inflater;

    invoke-direct {v0, p2, v2}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;-><init>(Lokio/ByteString;Ljava/util/zip/Inflater;)V

    invoke-virtual {v0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;->toReader()Ljava/io/InputStreamReader;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {v0}, Lf/h/a/f/f/n/g;->readText(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v3}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;->getRawMessageHandler()Lcom/discord/utilities/websocket/RawMessageHandler;

    move-result-object v3

    invoke-static {v3}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v3, v2}, Lcom/discord/utilities/websocket/RawMessageHandler;->onRawMessage(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v0

    iget-object v2, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v2}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;->getRawMessageHandler()Lcom/discord/utilities/websocket/RawMessageHandler;

    move-result-object v2

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v2, v0}, Lcom/discord/utilities/websocket/RawMessageHandler;->onRawMessageInflateFailed(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    :try_start_5
    new-instance v0, Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;

    iget-object v2, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->inflater:Ljava/util/zip/Inflater;

    invoke-direct {v0, p2, v2}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;-><init>(Lokio/ByteString;Ljava/util/zip/Inflater;)V

    invoke-virtual {v0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$ZLibByteStream;->toReader()Ljava/io/InputStreamReader;

    move-result-object v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :try_start_6
    iget-object v2, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {p2}, Lokio/ByteString;->j()I

    move-result p2

    invoke-virtual {v2, p1, v0, p2}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;->onInflatedMessage(Lokhttp3/WebSocket;Ljava/io/InputStreamReader;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-void

    :catchall_3
    move-exception p1

    :try_start_7
    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :catchall_4
    move-exception p2

    invoke-static {v0, p1}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p2

    :catch_0
    move-exception p1

    iget-object p2, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {p2, p1}, Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;->onInflateError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->resetInflaters()V

    iget-object v0, p0, Lcom/discord/utilities/websocket/ZLibWebSocketListener;->listener:Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;

    invoke-virtual {v0, p1, p2}, Lokhttp3/WebSocketListener;->onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V

    return-void
.end method
