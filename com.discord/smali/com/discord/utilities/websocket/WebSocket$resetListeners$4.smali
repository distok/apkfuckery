.class public final Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;
.super Lx/m/c/k;
.source "WebSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/websocket/WebSocket;->resetListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/websocket/WebSocket$Error;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;

    invoke-direct {v0}, Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;-><init>()V

    sput-object v0, Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;->INSTANCE:Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/websocket/WebSocket$Error;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/websocket/WebSocket$resetListeners$4;->invoke(Lcom/discord/utilities/websocket/WebSocket$Error;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/websocket/WebSocket$Error;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
