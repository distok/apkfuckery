.class public final Lcom/discord/utilities/rest/SendUtils;
.super Ljava/lang/Object;
.source "SendUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/rest/SendUtils$FileUpload;,
        Lcom/discord/utilities/rest/SendUtils$SendPayload;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/rest/SendUtils;

.field public static final MAX_MESSAGE_CHARACTER_COUNT:I = 0x7d0


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/rest/SendUtils;

    invoke-direct {v0}, Lcom/discord/utilities/rest/SendUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getPart(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/rest/SendUtils$FileUpload;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/rest/SendUtils$getPart$1;

    invoke-direct {v0, p1, p3, p2}, Lcom/discord/utilities/rest/SendUtils$getPart$1;-><init>(Lcom/lytefast/flexinput/model/Attachment;Ljava/lang/String;Landroid/content/ContentResolver;)V

    sget-object p1, Lrx/Emitter$BackpressureMode;->f:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, p1}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic handleSendError$default(Lcom/discord/utilities/rest/SendUtils;Lcom/discord/utilities/error/Error;Landroid/content/Context;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/rest/SendUtils;->handleSendError(Lcom/discord/utilities/error/Error;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final showFilesTooLargeDialog(Landroid/content/Context;I)V
    .locals 5

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eq p2, v0, :cond_2

    const/16 v0, 0x32

    if-eq p2, v0, :cond_1

    const/16 v0, 0x64

    if-eq p2, v0, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const p2, 0x7f12074b

    goto :goto_0

    :cond_1
    const p2, 0x7f12074a

    goto :goto_0

    :cond_2
    const p2, 0x7f12074c

    :goto_0
    const v0, 0x7f0d01b3

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v2, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v2

    const-string v3, "AlertDialog.Builder(cont\u2026View(dialogView).create()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f0a0237

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "dialogHelp"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f1218f9

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v4, v1

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_3

    const p2, 0x7f06026f

    invoke-virtual {p1, p2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_3
    :try_start_0
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const-string p1, "Could not show FilesTooLargeDialog"

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public final compressImageAttachments(Landroid/content/Context;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachments"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCompressed"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ly/a/r0;->d:Ly/a/r0;

    sget-object v0, Ly/a/h0;->a:Ly/a/v;

    sget-object v2, Ly/a/s1/j;->b:Ly/a/e1;

    const/4 v3, 0x0

    new-instance v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;

    const/4 v0, 0x0

    invoke-direct {v4, p2, p1, p3, v0}, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;-><init>(Ljava/util/List;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lf/h/a/f/f/n/g;->M(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Ly/a/x;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    return-void
.end method

.method public final getSendPayload(Landroid/content/ContentResolver;Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)Lrx/Observable;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/rest/SendUtils$SendPayload;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v11, p3

    sget-object v12, Lx/h/l;->d:Lx/h/l;

    const-string v2, "contentResolver"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "apiParamMessage"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "Observable.just(SendPayl\u2026ramMessage, emptyList()))"

    if-nez v11, :cond_0

    new-instance v0, Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;

    invoke-direct {v0, v1, v12}, Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;-><init>(Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)V

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    invoke-static {v1, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    :cond_0
    invoke-static {v11, v0}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->extractLinks(Ljava/util/List;Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Lcom/discord/restapi/RestAPIParams$Message;->getContent()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, ""

    :goto_0
    invoke-static {v2, v14}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->appendLinks(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/discord/restapi/RestAPIParams$Message;->getContent()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const/4 v15, 0x1

    xor-int/2addr v3, v15

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7e

    const/4 v10, 0x0

    move-object/from16 v1, p2

    invoke-static/range {v1 .. v10}, Lcom/discord/restapi/RestAPIParams$Message;->copy$default(Lcom/discord/restapi/RestAPIParams$Message;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/restapi/RestAPIParams$Message$Activity;Ljava/util/List;Lcom/discord/restapi/RestAPIParams$Message$MessageReference;Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;ILjava/lang/Object;)Lcom/discord/restapi/RestAPIParams$Message;

    move-result-object v1

    :cond_2
    invoke-static {v11, v14}, Lx/h/f;->minus(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v15

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    if-eq v3, v15, :cond_3

    new-instance v3, Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v3, v5, v4, v4}, Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-static {v2}, Lx/h/f;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lytefast/flexinput/model/Attachment;

    new-instance v5, Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;

    invoke-virtual {v3}, Lcom/lytefast/flexinput/model/Attachment;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/lytefast/flexinput/model/Attachment;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3}, Lcom/lytefast/flexinput/model/Attachment;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v7, v3}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->getMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v15, v6, v3}, Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    move-object v3, v5

    :goto_1
    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, v3}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    new-instance v3, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v2, v6}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v6, 0x0

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    add-int/lit8 v8, v6, 0x1

    if-ltz v6, :cond_4

    check-cast v7, Lcom/lytefast/flexinput/model/Attachment;

    sget-object v9, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    const-string v10, "file"

    invoke-static {v10, v6}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, v7, v0, v6}, Lcom/discord/utilities/rest/SendUtils;->getPart(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;Ljava/lang/String;)Lrx/Observable;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v6, v8

    goto :goto_2

    :cond_4
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    throw v4

    :cond_5
    new-instance v0, Lg0/l/a/t;

    invoke-direct {v0, v3}, Lg0/l/a/t;-><init>(Ljava/lang/Iterable;)V

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->l(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/discord/utilities/rest/SendUtils$getSendPayload$2;

    invoke-direct {v2, v1}, Lcom/discord/utilities/rest/SendUtils$getSendPayload$2;-><init>(Lcom/discord/restapi/RestAPIParams$Message;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-static {v5, v0}, Lrx/Observable;->m(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.concat(\n     \u2026)\n              }\n      )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    new-instance v0, Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;

    invoke-direct {v0, v1, v12}, Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;-><init>(Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)V

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    invoke-static {v1, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    :goto_3
    return-object v0
.end method

.method public final handleSendError(Lcom/discord/utilities/error/Error;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/error/Error;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/error/Error$Type;->REQUEST_TOO_LARGE:Lcom/discord/utilities/error/Error$Type;

    if-ne v1, v2, :cond_0

    if-eqz p3, :cond_3

    invoke-interface {p3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lkotlin/Unit;

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p3

    const-string v1, "error.response"

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/utilities/error/Error$Response;->isKnownResponse()Z

    move-result p3

    if-nez p3, :cond_2

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object p3

    sget-object v1, Lcom/discord/utilities/error/Error$Type;->NETWORK:Lcom/discord/utilities/error/Error$Type;

    if-ne p3, v1, :cond_1

    goto :goto_0

    :cond_1
    const p3, 0x7f1210c1

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-static {p2, p3, v0, v1, v2}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p3, 0x1

    invoke-virtual {p1, p3}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    :cond_3
    :goto_1
    invoke-virtual {p1, p2}, Lcom/discord/utilities/error/Error;->showToasts(Landroid/content/Context;)V

    return-void
.end method

.method public final tryShowFilesTooLargeDialog(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;FIFZLjava/util/List;ZZLkotlin/jvm/functions/Function0;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroidx/fragment/app/FragmentManager;",
            "FIFZ",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)Z"
        }
    .end annotation

    move-object v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p4

    move/from16 v11, p6

    move-object/from16 v12, p10

    const-string v3, "context"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fragmentManager"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "attachments"

    move-object/from16 v4, p7

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v3, v2

    cmpg-float v3, p3, v3

    if-gtz v3, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    sget-object v13, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v13}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v3

    if-eqz v11, :cond_1

    sget-object v5, Lcom/discord/utilities/rest/FileUploadAlertType;->OVER_MAX_SIZE:Lcom/discord/utilities/rest/FileUploadAlertType;

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/discord/utilities/rest/FileUploadAlertType;->NITRO_UPSELL:Lcom/discord/utilities/rest/FileUploadAlertType;

    :goto_0
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v6

    const/high16 v4, 0x100000

    int-to-float v4, v4

    mul-float v7, p5, v4

    float-to-int v7, v7

    mul-float v4, v4, p3

    float-to-int v8, v4

    move-object v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p6

    invoke-virtual/range {v3 .. v10}, Lcom/discord/stores/StoreAnalytics;->trackFileUploadAlertViewed(Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZ)V

    invoke-virtual {v13}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v3

    const-string v4, "2020-09_mobile_image_compression"

    const/4 v13, 0x1

    invoke-virtual {v3, v4, v13}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x2

    if-nez v11, :cond_6

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v6

    if-eq v6, v5, :cond_3

    :cond_2
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v5

    if-ne v5, v4, :cond_4

    :cond_3
    sget-object v0, Lf/a/a/f;->o:Lf/a/a/f$b;

    invoke-virtual {v0, v1, v11, v2, v12}, Lf/a/a/f$b;->a(Landroidx/fragment/app/FragmentManager;ZILkotlin/jvm/functions/Function0;)V

    goto/16 :goto_1

    :cond_4
    const v2, 0x7f121462

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v3

    if-ne v3, v13, :cond_5

    sget-object v3, Lf/a/a/e/c;->n:Lf/a/a/e/c$b;

    const/4 v4, 0x3

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd8

    const-string v11, "File Upload Popout"

    move-object v0, v3

    move-object/from16 v1, p2

    move v2, v4

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v11

    move-object v7, v8

    move-object v8, v9

    move-object/from16 v9, p10

    invoke-static/range {v0 .. v10}, Lf/a/a/e/c$b;->a(Lf/a/a/e/c$b;Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;I)V

    goto :goto_1

    :cond_5
    sget-object v3, Lf/a/a/e/c;->n:Lf/a/a/e/c$b;

    const/4 v4, 0x3

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1d8

    const-string v12, "File Upload Popout"

    move-object v0, v3

    move-object/from16 v1, p2

    move v2, v4

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v12

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move v10, v11

    invoke-static/range {v0 .. v10}, Lf/a/a/e/c$b;->a(Lf/a/a/e/c$b;Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;I)V

    goto :goto_1

    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v6

    if-eq v6, v5, :cond_8

    :cond_7
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v3

    if-ne v3, v4, :cond_9

    :cond_8
    sget-object v0, Lf/a/a/f;->o:Lf/a/a/f$b;

    invoke-virtual {v0, v1, v11, v2, v12}, Lf/a/a/f$b;->a(Landroidx/fragment/app/FragmentManager;ZILkotlin/jvm/functions/Function0;)V

    :goto_1
    move-object v1, p0

    goto :goto_2

    :cond_9
    move-object v1, p0

    invoke-direct {p0, p1, v2}, Lcom/discord/utilities/rest/SendUtils;->showFilesTooLargeDialog(Landroid/content/Context;I)V

    :goto_2
    return v13
.end method
