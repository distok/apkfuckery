.class public final Lcom/discord/utilities/rest/RestAPI;
.super Ljava/lang/Object;
.source "RestAPI.kt"

# interfaces
.implements Lcom/discord/restapi/RestAPIInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/rest/RestAPI$HarvestState;,
        Lcom/discord/utilities/rest/RestAPI$AppHeadersProvider;,
        Lcom/discord/utilities/rest/RestAPI$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

.field public static api:Lcom/discord/utilities/rest/RestAPI;

.field public static apiClientVersions:Lcom/discord/restapi/RestAPIInterface$Dynamic;

.field public static apiFiles:Lcom/discord/restapi/RestAPIInterface$Files;

.field public static apiRtcLatency:Lcom/discord/restapi/RestAPIInterface$RtcLatency;

.field public static apiSerializeNulls:Lcom/discord/utilities/rest/RestAPI;

.field public static apiSpotify:Lcom/discord/utilities/rest/RestAPI;


# instance fields
.field private final _api:Lcom/discord/restapi/RestAPIInterface;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/rest/RestAPI$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/rest/RestAPI$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/restapi/RestAPIInterface;)V
    .locals 1

    const-string v0, "_api"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    return-void
.end method

.method public static final synthetic access$getApi$cp()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public static final synthetic access$getApiClientVersions$cp()Lcom/discord/restapi/RestAPIInterface$Dynamic;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiClientVersions:Lcom/discord/restapi/RestAPIInterface$Dynamic;

    return-object v0
.end method

.method public static final synthetic access$getApiFiles$cp()Lcom/discord/restapi/RestAPIInterface$Files;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiFiles:Lcom/discord/restapi/RestAPIInterface$Files;

    return-object v0
.end method

.method public static final synthetic access$getApiRtcLatency$cp()Lcom/discord/restapi/RestAPIInterface$RtcLatency;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiRtcLatency:Lcom/discord/restapi/RestAPIInterface$RtcLatency;

    return-object v0
.end method

.method public static final synthetic access$getApiSerializeNulls$cp()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiSerializeNulls:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public static final synthetic access$getApiSpotify$cp()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiSpotify:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public static final synthetic access$setApi$cp(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public static final synthetic access$setApiClientVersions$cp(Lcom/discord/restapi/RestAPIInterface$Dynamic;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiClientVersions:Lcom/discord/restapi/RestAPIInterface$Dynamic;

    return-void
.end method

.method public static final synthetic access$setApiFiles$cp(Lcom/discord/restapi/RestAPIInterface$Files;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiFiles:Lcom/discord/restapi/RestAPIInterface$Files;

    return-void
.end method

.method public static final synthetic access$setApiRtcLatency$cp(Lcom/discord/restapi/RestAPIInterface$RtcLatency;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiRtcLatency:Lcom/discord/restapi/RestAPIInterface$RtcLatency;

    return-void
.end method

.method public static final synthetic access$setApiSerializeNulls$cp(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiSerializeNulls:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public static final synthetic access$setApiSpotify$cp(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiSpotify:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public static synthetic addRelationship$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p4

    :goto_0
    and-int/lit8 p4, p6, 0x8

    if-eqz p4, :cond_1

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, p5

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/rest/RestAPI;->addRelationship(Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final buildAnalyticsInterceptor()Lokhttp3/Interceptor;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildAnalyticsInterceptor()Lokhttp3/Interceptor;

    move-result-object v0

    return-object v0
.end method

.method public static final buildLoggingInterceptor()Lokhttp3/Interceptor;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic editChannel$default(Lcom/discord/utilities/rest/RestAPI;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;
    .locals 11

    and-int/lit8 v0, p9, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 v0, p9, 0x4

    if-eqz v0, :cond_1

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object v6, p4

    :goto_1
    and-int/lit8 v0, p9, 0x8

    if-eqz v0, :cond_2

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object/from16 v7, p5

    :goto_2
    and-int/lit8 v0, p9, 0x10

    if-eqz v0, :cond_3

    move-object v8, v1

    goto :goto_3

    :cond_3
    move-object/from16 v8, p6

    :goto_3
    and-int/lit8 v0, p9, 0x20

    if-eqz v0, :cond_4

    move-object v9, v1

    goto :goto_4

    :cond_4
    move-object/from16 v9, p7

    :goto_4
    and-int/lit8 v0, p9, 0x40

    if-eqz v0, :cond_5

    move-object v10, v1

    goto :goto_5

    :cond_5
    move-object/from16 v10, p8

    :goto_5
    move-object v2, p0

    move-wide v3, p1

    invoke-virtual/range {v2 .. v10}, Lcom/discord/utilities/rest/RestAPI;->editChannel(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public static final getApi()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "api"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public static final getApiSerializeNulls()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiSerializeNulls:Lcom/discord/utilities/rest/RestAPI;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiSerializeNulls"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public static final getApiSpotify()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiSpotify:Lcom/discord/utilities/rest/RestAPI;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiSpotify"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public static synthetic getAuditLogs$default(Lcom/discord/utilities/rest/RestAPI;JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;
    .locals 7

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x4

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_2

    move-object v6, v0

    goto :goto_2

    :cond_2
    move-object v6, p5

    :goto_2
    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/rest/RestAPI;->getAuditLogs(JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final varargs jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v3

    :try_start_0
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v8

    sget-object v5, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    const-string v6, "RestAPI"

    const-string v7, "Unable to serialize context property."

    invoke-static/range {v5 .. v11}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "it.toString()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lx/s/a;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    const-string v0, "(this as java.lang.String).getBytes(charset)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    const-string v0, "JSONObject().apply {\n   \u2026toByteArray(), NO_WRAP) }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static synthetic postInviteCode$default(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, "mobile"

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->postInviteCode(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final setApi(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public static final setApiSerializeNulls(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiSerializeNulls:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public static final setApiSpotify(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/rest/RestAPI;->apiSpotify:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method private final setConsent(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    if-eqz p2, :cond_1

    invoke-static {p2}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :cond_1
    iget-object p2, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Consents;

    invoke-direct {v1, p1, v0}, Lcom/discord/restapi/RestAPIParams$Consents;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-interface {p2, v1}, Lcom/discord/restapi/RestAPIInterface;->setConsents(Lcom/discord/restapi/RestAPIParams$Consents;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic setConsent$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    move-object p2, v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->setConsent(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic userActivityActionJoin$default(Lcom/discord/utilities/rest/RestAPI;JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;
    .locals 10

    and-int/lit8 v0, p8, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v8, v1

    goto :goto_0

    :cond_0
    move-object/from16 v8, p6

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    move-object v9, v1

    goto :goto_1

    :cond_1
    move-object/from16 v9, p7

    :goto_1
    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    move-object v7, p5

    invoke-virtual/range {v2 .. v9}, Lcom/discord/utilities/rest/RestAPI;->userActivityActionJoin(JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public acceptGift(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "entitlements/gift-codes/{code}/redeem"
    .end annotation

    const-string v0, "code"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->acceptGift(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public ackGuild(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/ack"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->ackGuild(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public addChannelPin(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/pins/{messageId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->addChannelPin(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public addChannelRecipient(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "recipientId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/recipients/{recipientId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->addChannelRecipient(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public addReaction(JJLjava/lang/String;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "reaction"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/messages/{messageId}/reactions/{reaction}/@me"
    .end annotation

    const-string v0, "reaction"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->addReaction(JJLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public addRelationship(JLcom/discord/restapi/RestAPIParams$UserRelationship;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UserRelationship;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UserRelationship;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "users/@me/relationships/{userId}"
    .end annotation

    const-string v0, "relationship"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->addRelationship(JLcom/discord/restapi/RestAPIParams$UserRelationship;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final addRelationship(Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "location"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v2, Lcom/discord/restapi/RestAPIParams$UserRelationship;

    invoke-direct {v2, p4, p5}, Lcom/discord/restapi/RestAPIParams$UserRelationship;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    const/4 p4, 0x1

    new-array p5, p4, [Lkotlin/Pair;

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x0

    aput-object v3, p5, p1

    invoke-direct {p0, p5}, Lcom/discord/utilities/rest/RestAPI;->jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;

    move-result-object p5

    invoke-interface {v1, p2, p3, v2, p5}, Lcom/discord/restapi/RestAPIInterface;->addRelationship(JLcom/discord/restapi/RestAPIParams$UserRelationship;Ljava/lang/String;)Lrx/Observable;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p2, p1, p4, p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public authorizeConnection(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUrl;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "connections/{connection}/authorize"
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->authorizeConnection(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public banGuildMember(JJLcom/discord/restapi/RestAPIParams$BanGuildMember;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$BanGuildMember;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$BanGuildMember;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/bans/{userId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->banGuildMember(JJLcom/discord/restapi/RestAPIParams$BanGuildMember;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public batchUpdateRole(JLjava/util/List;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$Role;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/roles"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->batchUpdateRole(JLjava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelCall$Ringable;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/call"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->call(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public cancelSubscriptionSlot(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "subscriptionSlotId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/guilds/premium/subscription-slots/{subscriptionSlotId}/cancel"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->cancelSubscriptionSlot(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public changeGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMember;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$GuildMember;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$GuildMember;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/members/{userId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->changeGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMember;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public changeGuildNickname(JLcom/discord/restapi/RestAPIParams$Nick;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Nick;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Nick;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/members/@me/nick"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->changeGuildNickname(JLcom/discord/restapi/RestAPIParams$Nick;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public claimSku(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "skuId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "store/skus/{skuId}/purchase"
    .end annotation

    const-string v0, "emptyBody"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->claimSku(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public confirmCommunityGating(JLcom/discord/restapi/RestAPIParams$CommunityGating;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$CommunityGating;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$CommunityGating;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/requests/@me"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->confirmCommunityGating(JLcom/discord/restapi/RestAPIParams$CommunityGating;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public convertDMToGroup(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "recipientId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/recipients/{recipientId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->convertDMToGroup(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public createChannelFollower(JLcom/discord/restapi/RestAPIParams$ChannelFollowerPost;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/followers"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->createChannelFollower(JLcom/discord/restapi/RestAPIParams$ChannelFollowerPost;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public createGuild(Lcom/discord/restapi/RestAPIParams$CreateGuild;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$CreateGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$CreateGuild;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->createGuild(Lcom/discord/restapi/RestAPIParams$CreateGuild;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public createGuildChannel(JLcom/discord/restapi/RestAPIParams$CreateGuildChannel;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/channels"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->createGuildChannel(JLcom/discord/restapi/RestAPIParams$CreateGuildChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public createGuildFromTemplate(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "guildTemplateCode"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/templates/{guildTemplateCode}"
    .end annotation

    const-string v0, "guildTemplateCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->createGuildFromTemplate(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public createPurchaseMetadata(Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "google-play/purchase-metadata"
    .end annotation

    const-string v0, "purchaseMetadataBody"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->createPurchaseMetadata(Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public createRole(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/roles"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->createRole(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public crosspostMessage(JLjava/lang/Long;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages/{messageId}/crosspost"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->crosspostMessage(JLjava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteAccount(Lcom/discord/restapi/RestAPIParams$DisableAccount;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$DisableAccount;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DisableAccount;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/delete"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->deleteAccount(Lcom/discord/restapi/RestAPIParams$DisableAccount;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteChannel(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->deleteChannel(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteChannelPin(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/pins/{messageId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->deleteChannelPin(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteConnection(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connectionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/connections/{connection}/{connectionId}"
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionId"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->deleteConnection(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteGuild(JLcom/discord/restapi/RestAPIParams$DeleteGuild;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$DeleteGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$DeleteGuild;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/delete"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->deleteGuild(JLcom/discord/restapi/RestAPIParams$DeleteGuild;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteGuildEmoji(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "emojiId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/emojis/{emojiId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->deleteGuildEmoji(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteGuildIntegration(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/integrations/{integrationId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->deleteGuildIntegration(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteMessage(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channel_id"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "message_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channel_id}/messages/{message_id}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->deleteMessage(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteOAuthToken(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "oauthId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "oauth2/tokens/{oauthId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->deleteOAuthToken(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deletePaymentSource(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "paymentSourceId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/billing/payment-sources/{paymentSourceId}"
    .end annotation

    const-string v0, "paymentSourceId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->deletePaymentSource(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deletePermissionOverwrites(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "targetId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/permissions/{targetId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->deletePermissionOverwrites(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteRole(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "roleId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/roles/{roleId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->deleteRole(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteSubscription(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "subscriptionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/billing/subscriptions/{subscriptionId}"
    .end annotation

    const-string/jumbo v0, "subscriptionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->deleteSubscription(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public disableAccount(Lcom/discord/restapi/RestAPIParams$DisableAccount;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$DisableAccount;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DisableAccount;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/disable"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->disableAccount(Lcom/discord/restapi/RestAPIParams$DisableAccount;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public disableMFA(Lcom/discord/restapi/RestAPIParams$AuthCode;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$AuthCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$AuthCode;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/totp/disable"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->disableMFA(Lcom/discord/restapi/RestAPIParams$AuthCode;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public disableMfaSMS(Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/sms/disable"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->disableMfaSMS(Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public disconnectGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/members/{userId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->disconnectGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public downgradeSubscription(Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "google-play/downgrade-subscription"
    .end annotation

    const-string v0, "downgradeSubscriptionBody"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->downgradeSubscription(Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public editChannel(JLcom/discord/restapi/RestAPIParams$Channel;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Channel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Channel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "channels/{channelId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->editChannel(JLcom/discord/restapi/RestAPIParams$Channel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final editChannel(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v9, Lcom/discord/restapi/RestAPIParams$Channel;

    move-object v2, v9

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v2 .. v8}, Lcom/discord/restapi/RestAPIParams$Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    move-wide v2, p1

    invoke-interface {v1, p1, p2, v9}, Lcom/discord/restapi/RestAPIInterface;->editChannel(JLcom/discord/restapi/RestAPIParams$Channel;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public editGroupDM(JLcom/discord/restapi/RestAPIParams$GroupDM;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$GroupDM;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$GroupDM;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "channels/{channelId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->editGroupDM(JLcom/discord/restapi/RestAPIParams$GroupDM;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public editMessage(JJLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channel_id"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "message_id"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$Message;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "channels/{channel_id}/messages/{message_id}"
    .end annotation

    const-string v0, "message"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->editMessage(JJLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public enableIntegration(JLcom/discord/restapi/RestAPIParams$EnableIntegration;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$EnableIntegration;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$EnableIntegration;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/integrations"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->enableIntegration(JLcom/discord/restapi/RestAPIParams$EnableIntegration;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public enableMFA(Lcom/discord/restapi/RestAPIParams$EnableMFA;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$EnableMFA;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$EnableMFA;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/totp/enable"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->enableMFA(Lcom/discord/restapi/RestAPIParams$EnableMFA;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public enableMfaSMS(Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/sms/enable"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->enableMfaSMS(Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public forgotPassword(Lcom/discord/restapi/RestAPIParams$ForgotPassword;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$ForgotPassword;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$ForgotPassword;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/forgot"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->forgotPassword(Lcom/discord/restapi/RestAPIParams$ForgotPassword;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public generateGiftCode(Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/entitlements/gift-codes"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->generateGiftCode(Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getActivityMetadata(JLjava/lang/String;J)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "sessionId"
        .end annotation
    .end param
    .param p4    # J
        .annotation runtime Lf0/i0/s;
            value = "applicationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelActivityMetaData;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/sessions/{sessionId}/activities/{applicationId}/metadata"
    .end annotation

    const-string v0, "sessionId"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-object v4, p3

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->getActivityMetadata(JLjava/lang/String;J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getApplications(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/t;
            value = "application_ids"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelApplication;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "applications/public"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getApplications(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getAuditLogs(JILjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "before"
        .end annotation
    .end param
    .param p5    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "user_id"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "action_type"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelAuditLog;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/audit-logs"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/discord/restapi/RestAPIInterface;->getAuditLogs(JILjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getAuditLogs(JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelAuditLog;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    move-object v11, v0

    goto :goto_1

    :cond_1
    :goto_0
    move-object/from16 v11, p4

    :goto_1
    if-nez p5, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual/range {p5 .. p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    move-object v12, v0

    move-object v0, p0

    goto :goto_3

    :cond_3
    :goto_2
    move-object v0, p0

    move-object/from16 v12, p5

    :goto_3
    iget-object v6, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    const/16 v9, 0x32

    move-wide v7, p1

    move-object/from16 v10, p3

    invoke-interface/range {v6 .. v12}, Lcom/discord/restapi/RestAPIInterface;->getAuditLogs(JILjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public getBackupCodes(Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelBackupCodes;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/codes"
    .end annotation

    const-string v0, "backupCodesRequest"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->getBackupCodes(Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getBans(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelBan;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/bans"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getBans(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getChannelFollowerStats(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannelFollowerStatsDto;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/follower-stats"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getChannelFollowerStats(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getChannelMessages(JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
    .locals 6
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "before"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "after"
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/discord/restapi/RestAPIInterface;->getChannelMessages(JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getChannelMessagesAround(JIJ)Lrx/Observable;
    .locals 6
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .param p4    # J
        .annotation runtime Lf0/i0/t;
            value = "around"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJ)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/discord/restapi/RestAPIInterface;->getChannelMessagesAround(JIJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getChannelPins(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/pins"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getChannelPins(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getClientVersion()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiClientVersions:Lcom/discord/restapi/RestAPIInterface$Dynamic;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "https://dl.discordapp.net/apps/android/versions.json"

    invoke-interface {v0, v2}, Lcom/discord/restapi/RestAPIInterface$Dynamic;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/utilities/rest/RestAPI$getClientVersion$1;->INSTANCE:Lcom/discord/utilities/rest/RestAPI$getClientVersion$1;

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v2, "apiClientVersions\n      \u2026n_version\")?.asInt ?: 0 }"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "apiClientVersions"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public getConnectionAccessToken(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "platformType"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "accountId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelConnectionAccessToken;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/connections/{platformType}/{accountId}/access-token"
    .end annotation

    const-string v0, "platformType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getConnectionAccessToken(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getConnectionState(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "pinNumber"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelConnectionState;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "connections/{connection}/callback-continuation/{pinNumber}"
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinNumber"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getConnectionState(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getConnections()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelConnectedAccount;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/connections"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getConnections()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getConsentRequired()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ConsentRequired;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "auth/consent-required"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getConsentRequired()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getConsents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Consents;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/consent"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getConsents()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getExperiments()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "experiments"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getExperiments()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getGifSearchResults(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "q"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "media_format"
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/GifDto;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/search"
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "provider"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaFormat"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->getGifSearchResults(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGifSuggestedSearchTerms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "q"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/suggest"
    .end annotation

    const-string v0, "provider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "query"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->getGifSuggestedSearchTerms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGifTrendingSearchTerms(Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/trending-search"
    .end annotation

    const-string v0, "provider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->getGifTrendingSearchTerms(Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGifts()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/entitlements/gifts"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getGifts()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getGuildEmojis(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/emoji/ModelEmojiGuild;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/emojis"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildEmojis(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildIntegrations(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildIntegration;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/integrations"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildIntegrations(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildInvites(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/invites"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildInvites(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildMemberVerificationForm(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/member-verification"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildMemberVerificationForm(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildPreview(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildPreview;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/preview"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildPreview(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildTemplateCode(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "guildTemplateCode"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildTemplate;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/templates/{guildTemplateCode}"
    .end annotation

    const-string v0, "guildTemplateCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->getGuildTemplateCode(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildVoiceRegions()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelVoiceRegion;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "voice/regions"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getGuildVoiceRegions()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getGuildVoiceRegions(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelVoiceRegion;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/regions"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildVoiceRegions(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getGuildWelcomeScreen(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildWelcomeScreen;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/welcome-screen"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getGuildWelcomeScreen(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getHarvestStatus()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Harvest;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/harvest"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getHarvestStatus()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getHarvestStatusGuarded()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/rest/RestAPI$HarvestState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getHarvestStatus()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rest/RestAPI$getHarvestStatusGuarded$1;->INSTANCE:Lcom/discord/utilities/rest/RestAPI$getHarvestStatusGuarded$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "_api.getHarvestStatus()\n\u2026erRequested()\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getInviteCode(Ljava/lang/String;Z)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_counts"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "invites/{code}"
    .end annotation

    const-string v0, "code"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getInviteCode(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getInvoicePreview(Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/billing/invoices/preview"
    .end annotation

    const-string v0, "invoicePreviewBody"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->getInvoicePreview(Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getLibrary()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelLibraryApplication;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/library"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getLibrary()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getMentions(IZZLjava/lang/Long;Ljava/lang/Long;)Lrx/Observable;
    .locals 6
    .param p1    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lf0/i0/t;
            value = "roles"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "everyone"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "guild_id"
        .end annotation
    .end param
    .param p5    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "before"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZ",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/mentions"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/discord/restapi/RestAPIInterface;->getMentions(IZZLjava/lang/Long;Ljava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getMyEntitlements(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "applicationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/applications/{applicationId}/entitlements"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getMyEntitlements(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getMyStickerPacks()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelUserStickerPack;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/sticker-packs"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getMyStickerPacks()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthTokens()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelOAuth2Token;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "oauth2/tokens"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getOAuthTokens()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getOauth2Authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "client_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "state"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "response_type"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "redirect_uri"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "prompt"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "scope"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "permissions"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "oauth2/authorize"
    .end annotation

    const-string v0, "clientId"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prompt"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    move-object v7, p6

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v8, p7

    invoke-interface/range {v1 .. v8}, Lcom/discord/restapi/RestAPIInterface;->getOauth2Authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public getOauth2SamsungAuthorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "client_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "state"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "response_type"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "redirect_uri"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "prompt"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "scope"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "oauth2/samsung/authorize"
    .end annotation

    const-string v0, "clientId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prompt"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-interface/range {v1 .. v7}, Lcom/discord/restapi/RestAPIInterface;->getOauth2SamsungAuthorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getPaymentSources()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/PaymentSourceRaw;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/billing/payment-sources"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getPaymentSources()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getPruneCount(JI)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "days"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild$PruneCountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/prune"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->getPruneCount(JI)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getReactionUsers(JJLjava/lang/String;Ljava/lang/Integer;)Lrx/Observable;
    .locals 8
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "emoji"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages/{messageId}/reactions/{emoji}"
    .end annotation

    const-string v0, "emoji"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-interface/range {v1 .. v7}, Lcom/discord/restapi/RestAPIInterface;->getReactionUsers(JJLjava/lang/String;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getRelationships()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/relationships"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getRelationships()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getRelationships(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/relationships"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getRelationships(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getRtcLatencyTestRegionsIps()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelRtcLatencyRegion;",
            ">;>;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->apiRtcLatency:Lcom/discord/restapi/RestAPIInterface$RtcLatency;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "https://latency.discord.media/rtc"

    invoke-interface {v0, v2}, Lcom/discord/restapi/RestAPIInterface$RtcLatency;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "apiRtcLatency"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public getSpotifyTrack(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/spotify/ModelSpotifyTrack;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "tracks/{id}"
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->getSpotifyTrack(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getStickerPack(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "packId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "sticker-packs/{packId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getStickerPack(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getStickerPacks(II)Lrx/Observable;
    .locals 1
    .param p1    # I
        .annotation runtime Lf0/i0/t;
            value = "offset"
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "sticker-packs"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getStickerPacks(II)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getStickerStoreDirectoryLayoutV2(JZLjava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "storeDirectoryLayoutId"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_store_listings"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "sticker-packs/directory-v2/{storeDirectoryLayoutId}"
    .end annotation

    const-string v0, "locale"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->getStickerStoreDirectoryLayoutV2(JZLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getStreamPreview(Ljava/lang/String;J)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "streamKey"
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lf0/i0/t;
            value = "version"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelApplicationStreamPreview;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "streams/{streamKey}/preview"
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->getStreamPreview(Ljava/lang/String;J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getSubscriptionSlots()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/guilds/premium/subscription-slots"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getSubscriptionSlots()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getSubscriptions()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/billing/subscriptions"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getSubscriptions()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getTrendingGifCategories(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "media_format"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/trending"
    .end annotation

    const-string v0, "provider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaFormat"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->getTrendingGifCategories(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getTrendingGifCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "media_format"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/GifDto;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/trending-gifs"
    .end annotation

    const-string v0, "provider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaFormat"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->getTrendingGifCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getUserAffinities()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserAffinities;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/affinities/users"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->getUserAffinities()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getUserNote(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserNote;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/notes/{userId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->getUserNote(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public joinGuild(JZLjava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "lurker"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "session_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/members/@me"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->joinGuild(JZLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public joinGuildFromIntegration(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "integrations/{integrationId}/join"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->joinGuildFromIntegration(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public kickGuildMember(JJLjava/lang/String;)Lrx/Observable;
    .locals 6
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "reason"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/members/{userId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/discord/restapi/RestAPIInterface;->kickGuildMember(JJLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public leaveGuild(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/guilds/{guildId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->leaveGuild(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public logout(Lcom/discord/restapi/RestAPIParams$UserDevices;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserDevices;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserDevices;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/logout"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->logout(Lcom/discord/restapi/RestAPIParams$UserDevices;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public patchGuildEmoji(JJLcom/discord/restapi/RestAPIParams$PatchGuildEmoji;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "emojiId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$PatchGuildEmoji;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$PatchGuildEmoji;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/emoji/ModelEmojiGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/emojis/{emojiId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->patchGuildEmoji(JJLcom/discord/restapi/RestAPIParams$PatchGuildEmoji;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public patchUser(Lcom/discord/restapi/RestAPIParams$UserInfo;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserInfo;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserInfo;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me"
    .end annotation

    const-string/jumbo v0, "userInfo"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->patchUser(Lcom/discord/restapi/RestAPIParams$UserInfo;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public phoneVerificationsVerify(Lcom/discord/restapi/RestAPIParams$VerificationCode;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$VerificationCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$VerificationCode;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPhoneVerificationToken;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "phone-verifications/verify"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->phoneVerificationsVerify(Lcom/discord/restapi/RestAPIParams$VerificationCode;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postAuthFingerprint(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Fingerprint;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/fingerprint"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postAuthFingerprint(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postAuthHandoff(Ljava/util/Map;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$TokenHandoff;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/handoff"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postAuthHandoff(Ljava/util/Map;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postAuthLogin(Lcom/discord/restapi/RestAPIParams$AuthLogin;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$AuthLogin;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$AuthLogin;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/auth/ModelLoginResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/login"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postAuthLogin(Lcom/discord/restapi/RestAPIParams$AuthLogin;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postAuthRegister(Lcom/discord/restapi/RestAPIParams$AuthRegister;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$AuthRegister;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$AuthRegister;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/register"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postAuthRegister(Lcom/discord/restapi/RestAPIParams$AuthRegister;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postAuthVerifyResend(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/verify/resend"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postAuthVerifyResend(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postChannelInvite(JLcom/discord/restapi/RestAPIParams$Invite;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Invite;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Invite;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/invites"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->postChannelInvite(JLcom/discord/restapi/RestAPIParams$Invite;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postChannelMessagesAck(JLjava/lang/Long;Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p4    # Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages/{messageId}/ack"
    .end annotation

    const-string v0, "body"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->postChannelMessagesAck(JLjava/lang/Long;Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postGuildEmoji(JLcom/discord/restapi/RestAPIParams$PostGuildEmoji;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$PostGuildEmoji;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$PostGuildEmoji;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/emoji/ModelEmojiGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/emojis"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->postGuildEmoji(JLcom/discord/restapi/RestAPIParams$PostGuildEmoji;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final postInviteCode(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelInvite;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    const-string v0, "invite"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelInvite;->getCode()Ljava/lang/String;

    move-result-object v2

    const-string v3, "invite.code"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/discord/restapi/RestAPIParams$EmptyBody;

    invoke-direct {v3}, Lcom/discord/restapi/RestAPIParams$EmptyBody;-><init>()V

    const/4 v4, 0x4

    new-array v4, v4, [Lkotlin/Pair;

    new-instance v5, Lkotlin/Pair;

    invoke-direct {v5, v0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p2, 0x0

    aput-object v5, v4, p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelInvite;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v5

    :goto_0
    new-instance v6, Lkotlin/Pair;

    const-string v7, "location_guild_id"

    invoke-direct {v6, v7, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x1

    aput-object v6, v4, v0

    const/4 v6, 0x2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelInvite;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_1

    :cond_1
    move-object v7, v5

    :goto_1
    new-instance v8, Lkotlin/Pair;

    const-string v9, "location_channel_id"

    invoke-direct {v8, v9, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v8, v4, v6

    const/4 v6, 0x3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelInvite;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_2

    :cond_2
    move-object p1, v5

    :goto_2
    new-instance v7, Lkotlin/Pair;

    const-string v8, "location_channel_type"

    invoke-direct {v7, v8, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v4, v6

    invoke-direct {p0, v4}, Lcom/discord/utilities/rest/RestAPI;->jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, v3, p1}, Lcom/discord/restapi/RestAPIInterface;->postInviteCode(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$EmptyBody;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p2, v0, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postInviteCode(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$EmptyBody;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "invites/{code}"
    .end annotation

    const-string v0, "code"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->postInviteCode(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$EmptyBody;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postMFACode(Lcom/discord/restapi/RestAPIParams$MFALogin;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$MFALogin;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$MFALogin;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/auth/ModelLoginResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/mfa/totp"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postMFACode(Lcom/discord/restapi/RestAPIParams$MFALogin;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postOauth2Authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lrx/Observable;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "client_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "state"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "response_type"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "redirect_uri"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "prompt"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "scope"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "permissions"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "code_challenge"
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "code_challenge_method"
        .end annotation
    .end param
    .param p10    # Ljava/util/Map;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponsePost;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "oauth2/authorize"
    .end annotation

    const-string v0, "clientId"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prompt"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-interface/range {v1 .. v11}, Lcom/discord/restapi/RestAPIInterface;->postOauth2Authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public postRemoteAuthCancel(Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/remote-auth/cancel"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postRemoteAuthCancel(Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postRemoteAuthFinish(Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/remote-auth/finish"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postRemoteAuthFinish(Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postRemoteAuthInitialize(Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelRemoteAuthHandshake;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/remote-auth"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->postRemoteAuthInitialize(Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public postStreamPreview(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$Thumbnail;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "streamKey"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$Thumbnail;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$Thumbnail;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "streams/{streamKey}/preview"
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "thumbnail"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->postStreamPreview(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$Thumbnail;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final postStreamPreview(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "streamKey"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "thumbnail"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Thumbnail;

    invoke-direct {v1, p2}, Lcom/discord/restapi/RestAPIParams$Thumbnail;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Lcom/discord/restapi/RestAPIInterface;->postStreamPreview(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$Thumbnail;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public pruneMembers(JLcom/discord/restapi/RestAPIParams$PruneGuild;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$PruneGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$PruneGuild;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/prune"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->pruneMembers(JLcom/discord/restapi/RestAPIParams$PruneGuild;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public removeAllReactions(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/messages/{messageId}/reactions"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->removeAllReactions(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public removeChannelRecipient(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "recipientId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/recipients/{recipientId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->removeChannelRecipient(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public removeReaction(JJLjava/lang/String;J)Lrx/Observable;
    .locals 9
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "reaction"
        .end annotation
    .end param
    .param p6    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/messages/{messageId}/reactions/{reaction}/{userId}"
    .end annotation

    const-string v0, "reaction"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-wide v7, p6

    invoke-interface/range {v1 .. v8}, Lcom/discord/restapi/RestAPIInterface;->removeReaction(JJLjava/lang/String;J)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public removeRelationship(JLjava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/relationships/{userId}"
    .end annotation

    const-string v0, "context"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->removeRelationship(JLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final removeRelationship(Ljava/lang/String;J)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "location"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    const/4 v2, 0x1

    new-array v3, v2, [Lkotlin/Pair;

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x0

    aput-object v4, v3, p1

    invoke-direct {p0, v3}, Lcom/discord/utilities/rest/RestAPI;->jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p2, p3, v0}, Lcom/discord/restapi/RestAPIInterface;->removeRelationship(JLjava/lang/String;)Lrx/Observable;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p2, p1, v2, p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public removeSelfReaction(JJLjava/lang/String;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "reaction"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/messages/{messageId}/reactions/{reaction}/@me"
    .end annotation

    const-string v0, "reaction"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->removeSelfReaction(JJLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public reorderChannels(JLjava/util/List;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$ChannelPosition;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/channels"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->reorderChannels(JLjava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public requestHarvest()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Harvest;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/harvest"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0}, Lcom/discord/restapi/RestAPIInterface;->requestHarvest()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public resolveGiftCode(Ljava/lang/String;ZZ)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_application"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_subscription_plan"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "entitlements/gift-codes/{code}"
    .end annotation

    const-string v0, "code"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->resolveGiftCode(Ljava/lang/String;ZZ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public resolveSkuIdGift(JLjava/lang/Long;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/t;
            value = "sku_id"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "subscription_plan_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/entitlements/gift-codes"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->resolveSkuIdGift(JLjava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public revokeGiftCode(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/entitlements/gift-codes/{code}"
    .end annotation

    const-string v0, "code"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->revokeGiftCode(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public revokeInvite(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "inviteCode"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "invites/{inviteCode}"
    .end annotation

    const-string v0, "inviteCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->revokeInvite(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final ring(JJLjava/util/List;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Ring;

    invoke-direct {v1, p5}, Lcom/discord/restapi/RestAPIParams$Ring;-><init>(Ljava/util/List;)V

    const/4 p5, 0x1

    new-array v2, p5, [Lkotlin/Pair;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lkotlin/Pair;

    const-string v3, "message_id"

    invoke-direct {p4, v3, p3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p3, 0x0

    aput-object p4, v2, p3

    invoke-direct {p0, v2}, Lcom/discord/utilities/rest/RestAPI;->jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, v1, p4}, Lcom/discord/restapi/RestAPIInterface;->ring(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p3, p5, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public ring(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Ring;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Ring;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/call/ring"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->ring(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public science(Lcom/discord/restapi/RestAPIParams$Science;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$Science;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$Science;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "science"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->science(Lcom/discord/restapi/RestAPIParams$Science;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public searchChannelMessages(JLjava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)Lrx/Observable;
    .locals 12
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "max_id"
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "author_id"
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "mentions"
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "has"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "context_size"
        .end annotation
    .end param
    .param p8    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "content"
        .end annotation
    .end param
    .param p9    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "attempts"
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Lf0/i0/t;
            value = "include_nsfw"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages/search"
    .end annotation

    const-string v0, "contextSize"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-interface/range {v1 .. v11}, Lcom/discord/restapi/RestAPIInterface;->searchChannelMessages(JLjava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public searchGuildMessages(JLjava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)Lrx/Observable;
    .locals 13
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "max_id"
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "author_id"
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "mentions"
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "channel_id"
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "has"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "context_size"
        .end annotation
    .end param
    .param p9    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "content"
        .end annotation
    .end param
    .param p10    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "attempts"
        .end annotation
    .end param
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lf0/i0/t;
            value = "include_nsfw"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/messages/search"
    .end annotation

    const-string v0, "contextSize"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-interface/range {v1 .. v12}, Lcom/discord/restapi/RestAPIInterface;->searchGuildMessages(JLjava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public sendMessage(JLcom/discord/restapi/PayloadJSON;[Lokhttp3/MultipartBody$Part;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/PayloadJSON;
        .annotation runtime Lf0/i0/q;
            value = "payload_json"
        .end annotation
    .end param
    .param p4    # [Lokhttp3/MultipartBody$Part;
        .annotation runtime Lf0/i0/q;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/PayloadJSON<",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            ">;[",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/l;
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages"
    .end annotation

    const-string v0, "payloadJson"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "files"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->sendMessage(JLcom/discord/restapi/PayloadJSON;[Lokhttp3/MultipartBody$Part;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public sendMessage(JLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Message;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages"
    .end annotation

    const-string v0, "message"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->sendMessage(JLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public sendRelationshipRequest(Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/relationships"
    .end annotation

    const-string v0, "relationship"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->sendRelationshipRequest(Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final sendRelationshipRequest(Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "location"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "username"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v2, Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;

    invoke-direct {v2, p2, p3}, Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;-><init>(Ljava/lang/String;I)V

    const/4 p2, 0x1

    new-array p3, p2, [Lkotlin/Pair;

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x0

    aput-object v3, p3, p1

    invoke-direct {p0, p3}, Lcom/discord/utilities/rest/RestAPI;->jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;

    move-result-object p3

    invoke-interface {v1, v2, p3}, Lcom/discord/restapi/RestAPIInterface;->sendRelationshipRequest(Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;Ljava/lang/String;)Lrx/Observable;

    move-result-object p3

    const/4 v0, 0x0

    invoke-static {p3, p1, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final setConsent(ZLjava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "consentType"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    invoke-static {p0, p2, v0, p1, v0}, Lcom/discord/utilities/rest/RestAPI;->setConsent$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-static {p0, v0, p2, p1, v0}, Lcom/discord/utilities/rest/RestAPI;->setConsent$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public setConsents(Lcom/discord/restapi/RestAPIParams$Consents;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$Consents;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$Consents;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/consent"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->setConsents(Lcom/discord/restapi/RestAPIParams$Consents;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public setMfaLevel(JLcom/discord/restapi/RestAPIParams$GuildMFA;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$GuildMFA;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$GuildMFA;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/mfa"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->setMfaLevel(JLcom/discord/restapi/RestAPIParams$GuildMFA;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public setUserTyping(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelTypingResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/typing"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->setUserTyping(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final stopRinging(JJLjava/util/List;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Ring;

    invoke-direct {v1, p5}, Lcom/discord/restapi/RestAPIParams$Ring;-><init>(Ljava/util/List;)V

    const/4 p5, 0x1

    new-array v2, p5, [Lkotlin/Pair;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lkotlin/Pair;

    const-string v3, "message_id"

    invoke-direct {p4, v3, p3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p3, 0x0

    aput-object p4, v2, p3

    invoke-direct {p0, v2}, Lcom/discord/utilities/rest/RestAPI;->jsonObjectOf([Lkotlin/Pair;)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, v1, p4}, Lcom/discord/restapi/RestAPIInterface;->stopRinging(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p3, p5, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public stopRinging(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Ring;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Ring;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/call/stop-ringing"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->stopRinging(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public submitConnectionState(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$ConnectionState;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$ConnectionState;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$ConnectionState;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "connections/{connection}/callback"
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "state"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->submitConnectionState(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$ConnectionState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public subscribeToGuild(JLcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/premium/subscriptions"
    .end annotation

    const-string v0, "premiumGuildSubscribe"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->subscribeToGuild(JLcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public syncIntegration(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/integrations/{integrationId}/sync"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->syncIntegration(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public transferGuildOwnership(JLcom/discord/restapi/RestAPIParams$TransferGuildOwnership;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}"
    .end annotation

    const-string/jumbo v0, "transferGuildOwnership"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->transferGuildOwnership(JLcom/discord/restapi/RestAPIParams$TransferGuildOwnership;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public unbanUser(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/bans/{userId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->unbanUser(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public uncancelSubscriptionSlot(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "subscriptionSlotId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/guilds/premium/subscription-slots/{subscriptionSlotId}/uncancel"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->uncancelSubscriptionSlot(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public unsubscribeToGuild(JJ)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "subscriptionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/premium/subscriptions/{subscriptionId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/discord/restapi/RestAPIInterface;->unsubscribeToGuild(JJ)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateConnection(Ljava/lang/String;Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$ConnectedAccount;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connectionId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$ConnectedAccount;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$ConnectedAccount;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelConnectedAccount;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/connections/{connection}/{connectionId}"
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionId"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectedAccount"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->updateConnection(Ljava/lang/String;Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$ConnectedAccount;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UpdateGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UpdateGuild;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateGuildIntegration(JJLcom/discord/restapi/RestAPIParams$GuildIntegration;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$GuildIntegration;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$GuildIntegration;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/integrations/{integrationId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->updateGuildIntegration(JJLcom/discord/restapi/RestAPIParams$GuildIntegration;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updatePaymentSource(Ljava/lang/String;Lcom/discord/models/domain/PatchPaymentSourceRaw;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "paymentSourceId"
        .end annotation
    .end param
    .param p2    # Lcom/discord/models/domain/PatchPaymentSourceRaw;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/PatchPaymentSourceRaw;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/billing/payment-sources/{paymentSourceId}"
    .end annotation

    const-string v0, "paymentSourceId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "PatchPaymentSourceRaw"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->updatePaymentSource(Ljava/lang/String;Lcom/discord/models/domain/PatchPaymentSourceRaw;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updatePermissionOverwrites(JJLcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "targetId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/permissions/{targetId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->updatePermissionOverwrites(JJLcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updatePrivateChannelSettings(Lcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserGuildSettings;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserGuildSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/guilds/@me/settings"
    .end annotation

    const-string/jumbo v0, "userGuildSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->updatePrivateChannelSettings(Lcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateRole(JJLcom/discord/restapi/RestAPIParams$Role;)Lrx/Observable;
    .locals 7
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "roleId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$Role;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$Role;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/roles/{roleId}"
    .end annotation

    const-string v0, "body"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/discord/restapi/RestAPIInterface;->updateRole(JJLcom/discord/restapi/RestAPIParams$Role;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "subscriptionId"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$UpdateSubscription;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$UpdateSubscription;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/billing/subscriptions/{subscriptionId}"
    .end annotation

    const-string/jumbo v0, "subscriptionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateSubscription"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateUserGuildSettings(JLcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UserGuildSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "userGuildSettings"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {p1, p3}, Lcom/discord/restapi/RestAPIInterface;->updatePrivateChannelSettings(Lcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->updateUserGuildSettings(JLcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public updateUserNotes(JLcom/discord/restapi/RestAPIParams$UserNoteUpdate;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "users/@me/notes/{userId}"
    .end annotation

    const-string/jumbo v0, "userNoteUpdate"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->updateUserNotes(JLcom/discord/restapi/RestAPIParams$UserNoteUpdate;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserSettings;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/settings"
    .end annotation

    const-string/jumbo v0, "userSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateUserSettingsCustomStatus(Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/settings"
    .end annotation

    const-string/jumbo v0, "userSettingsCustomStatus"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->updateUserSettingsCustomStatus(Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateVanityUrl(JLcom/discord/restapi/RestAPIParams$VanityUrl;)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$VanityUrl;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$VanityUrl;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild$VanityUrlResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/vanity-url"
    .end annotation

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIInterface;->updateVanityUrl(JLcom/discord/restapi/RestAPIParams$VanityUrl;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public uploadLog(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "filename"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "debug-logs/4/{filename}"
    .end annotation

    const-string v0, "filename"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "content"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->uploadLog(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public uploadLogs([Lokhttp3/MultipartBody$Part;)Lrx/Observable;
    .locals 1
    .param p1    # [Lokhttp3/MultipartBody$Part;
        .annotation runtime Lf0/i0/q;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/l;
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "debug-logs/multi/4"
    .end annotation

    const-string v0, "files"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->uploadLogs([Lokhttp3/MultipartBody$Part;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userActivityAction(JJLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;
    .locals 10
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "applicationId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "sessionId"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/s;
            value = "actionType"
        .end annotation
    .end param
    .param p7    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "channel_id"
        .end annotation
    .end param
    .param p8    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "message_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/sessions/{sessionId}/activities/{applicationId}/{actionType}"
    .end annotation

    const-string v0, "sessionId"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-interface/range {v1 .. v9}, Lcom/discord/restapi/RestAPIInterface;->userActivityAction(JJLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public final userActivityActionJoin(JJLjava/lang/String;)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x18

    const/4 v9, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/rest/RestAPI;->userActivityActionJoin$default(Lcom/discord/utilities/rest/RestAPI;JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final userActivityActionJoin(JJLjava/lang/String;Ljava/lang/Long;)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v9}, Lcom/discord/utilities/rest/RestAPI;->userActivityActionJoin$default(Lcom/discord/utilities/rest/RestAPI;JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final userActivityActionJoin(JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;",
            ">;"
        }
    .end annotation

    const-string v0, "sessionId"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-interface/range {v1 .. v9}, Lcom/discord/restapi/RestAPIInterface;->userActivityAction(JJLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method

.method public userAddPhone(Lcom/discord/restapi/RestAPIParams$Phone;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$Phone;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$Phone;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/phone"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userAddPhone(Lcom/discord/restapi/RestAPIParams$Phone;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userAgreements(Lcom/discord/restapi/RestAPIParams$UserAgreements;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserAgreements;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserAgreements;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/agreements"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userAgreements(Lcom/discord/restapi/RestAPIParams$UserAgreements;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userCaptchaVerify(Lcom/discord/restapi/RestAPIParams$CaptchaCode;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$CaptchaCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$CaptchaCode;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/captcha/verify"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userCaptchaVerify(Lcom/discord/restapi/RestAPIParams$CaptchaCode;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userCreateChannel(Lcom/discord/restapi/RestAPIParams$CreateChannel;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$CreateChannel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$CreateChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/channels"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userCreateChannel(Lcom/discord/restapi/RestAPIParams$CreateChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userCreateDevice(Lcom/discord/restapi/RestAPIParams$UserDevices;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserDevices;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserDevices;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/devices"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userCreateDevice(Lcom/discord/restapi/RestAPIParams$UserDevices;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userGet(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->userGet(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userPhoneDelete(Lcom/discord/restapi/RestAPIParams$DeletePhone;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$DeletePhone;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DeletePhone;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/h;
        hasBody = true
        method = "DELETE"
        path = "users/@me/phone"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userPhoneDelete(Lcom/discord/restapi/RestAPIParams$DeletePhone;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userPhoneWithToken(Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/phone"
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->userPhoneWithToken(Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public userProfileGet(J)Lrx/Observable;
    .locals 1
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserProfile;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/profile"
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1, p2}, Lcom/discord/restapi/RestAPIInterface;->userProfileGet(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public verifyPurchaseToken(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;)Lrx/Observable;
    .locals 1
    .param p1    # Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "google-play/verify-purchase-token"
    .end annotation

    const-string/jumbo v0, "verifyPurchaseBody"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/rest/RestAPI;->_api:Lcom/discord/restapi/RestAPIInterface;

    invoke-interface {v0, p1}, Lcom/discord/restapi/RestAPIInterface;->verifyPurchaseToken(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
