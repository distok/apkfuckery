.class public final Lcom/discord/utilities/rest/RestAPI$Companion;
.super Ljava/lang/Object;
.source "RestAPI.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/rest/RestAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/rest/RestAPI$Companion;-><init>()V

    return-void
.end method

.method public static synthetic getApi$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic getApiSerializeNulls$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic getApiSpotify$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final buildAnalyticsInterceptor()Lokhttp3/Interceptor;
    .locals 1

    sget v0, Lokhttp3/Interceptor;->a:I

    new-instance v0, Lcom/discord/utilities/rest/RestAPI$Companion$buildAnalyticsInterceptor$$inlined$invoke$1;

    invoke-direct {v0}, Lcom/discord/utilities/rest/RestAPI$Companion$buildAnalyticsInterceptor$$inlined$invoke$1;-><init>()V

    return-object v0
.end method

.method public final buildLoggingInterceptor()Lokhttp3/Interceptor;
    .locals 3

    new-instance v0, Lb0/h0/a;

    new-instance v1, Lcom/discord/utilities/rest/RestAPI$Companion$buildLoggingInterceptor$1;

    invoke-direct {v1}, Lcom/discord/utilities/rest/RestAPI$Companion$buildLoggingInterceptor$1;-><init>()V

    invoke-direct {v0, v1}, Lb0/h0/a;-><init>(Lb0/h0/a$b;)V

    sget-object v1, Lb0/h0/a$a;->e:Lb0/h0/a$a;

    const-string v2, "<set-?>"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lb0/h0/a;->c:Lb0/h0/a$a;

    return-object v0
.end method

.method public final getApi()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->access$getApi$cp()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "api"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getApiClientVersions()Lcom/discord/restapi/RestAPIInterface$Dynamic;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->access$getApiClientVersions$cp()Lcom/discord/restapi/RestAPIInterface$Dynamic;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiClientVersions"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getApiFiles()Lcom/discord/restapi/RestAPIInterface$Files;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->access$getApiFiles$cp()Lcom/discord/restapi/RestAPIInterface$Files;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiFiles"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getApiRtcLatency()Lcom/discord/restapi/RestAPIInterface$RtcLatency;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->access$getApiRtcLatency$cp()Lcom/discord/restapi/RestAPIInterface$RtcLatency;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiRtcLatency"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getApiSerializeNulls()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->access$getApiSerializeNulls$cp()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiSerializeNulls"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getApiSpotify()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->access$getApiSpotify$cp()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "apiSpotify"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final init(Landroid/content/Context;)V
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/restapi/RequiredHeadersInterceptor;

    sget-object v3, Lcom/discord/utilities/rest/RestAPI$AppHeadersProvider;->INSTANCE:Lcom/discord/utilities/rest/RestAPI$AppHeadersProvider;

    invoke-direct {v2, v3}, Lcom/discord/restapi/RequiredHeadersInterceptor;-><init>(Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;)V

    new-instance v4, Lcom/discord/restapi/BreadcrumbInterceptor;

    sget-object v5, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-direct {v4, v5}, Lcom/discord/restapi/BreadcrumbInterceptor;-><init>(Lcom/discord/utilities/logging/Logger;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildAnalyticsInterceptor()Lokhttp3/Interceptor;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Lokhttp3/Interceptor;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    aput-object v5, v7, v2

    invoke-virtual/range {p0 .. p0}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v5

    const/4 v9, 0x2

    aput-object v5, v7, v9

    const/4 v5, 0x3

    aput-object v4, v7, v5

    invoke-static {v7}, Lx/h/f;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-array v7, v9, [Lokhttp3/Interceptor;

    aput-object v6, v7, v8

    aput-object v4, v7, v2

    invoke-static {v7}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    new-instance v7, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;

    new-instance v10, Lcom/franmontiel/persistentcookiejar/cache/SetCookieCache;

    invoke-direct {v10}, Lcom/franmontiel/persistentcookiejar/cache/SetCookieCache;-><init>()V

    new-instance v11, Lcom/franmontiel/persistentcookiejar/persistence/SharedPrefsCookiePersistor;

    invoke-direct {v11, v1}, Lcom/franmontiel/persistentcookiejar/persistence/SharedPrefsCookiePersistor;-><init>(Landroid/content/Context;)V

    invoke-direct {v7, v10, v11}, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;-><init>(Lcom/franmontiel/persistentcookiejar/cache/CookieCache;Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;)V

    new-instance v1, Lcom/discord/restapi/RestAPIBuilder;

    const-string v10, "https://discord.com/api/"

    invoke-direct {v1, v10, v7}, Lcom/discord/restapi/RestAPIBuilder;-><init>(Ljava/lang/String;Lb0/p;)V

    new-instance v15, Lcom/discord/utilities/rest/RestAPI;

    const-class v11, Lcom/discord/restapi/RestAPIInterface;

    const/4 v12, 0x0

    const-wide/16 v13, 0x0

    const-string v16, "client_base"

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x66

    const/16 v20, 0x0

    move-object v10, v1

    move-object v2, v15

    move-object v15, v5

    invoke-static/range {v10 .. v20}, Lcom/discord/restapi/RestAPIBuilder;->build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/discord/restapi/RestAPIInterface;

    invoke-direct {v2, v10}, Lcom/discord/utilities/rest/RestAPI;-><init>(Lcom/discord/restapi/RestAPIInterface;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->setApi(Lcom/discord/utilities/rest/RestAPI;)V

    new-instance v2, Lcom/discord/utilities/rest/RestAPI;

    const-class v11, Lcom/discord/restapi/RestAPIInterface;

    const/4 v12, 0x1

    const-string v16, "client_serialize_nulls"

    const/16 v19, 0x64

    move-object v10, v1

    invoke-static/range {v10 .. v20}, Lcom/discord/restapi/RestAPIBuilder;->build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/restapi/RestAPIInterface;

    invoke-direct {v2, v5}, Lcom/discord/utilities/rest/RestAPI;-><init>(Lcom/discord/restapi/RestAPIInterface;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->setApiSerializeNulls(Lcom/discord/utilities/rest/RestAPI;)V

    const-class v11, Lcom/discord/restapi/RestAPIInterface$Dynamic;

    const/4 v12, 0x0

    const-string v16, "client_dynamic"

    const/16 v19, 0x66

    move-object v15, v4

    invoke-static/range {v10 .. v20}, Lcom/discord/restapi/RestAPIBuilder;->build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/restapi/RestAPIInterface$Dynamic;

    invoke-virtual {v0, v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->setApiClientVersions(Lcom/discord/restapi/RestAPIInterface$Dynamic;)V

    const-class v11, Lcom/discord/restapi/RestAPIInterface$RtcLatency;

    const-string v16, "client_rtc_latency"

    invoke-static/range {v10 .. v20}, Lcom/discord/restapi/RestAPIBuilder;->build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/restapi/RestAPIInterface$RtcLatency;

    invoke-virtual {v0, v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->setApiRtcLatency(Lcom/discord/restapi/RestAPIInterface$RtcLatency;)V

    new-instance v2, Lcom/discord/utilities/rest/RestAPI;

    new-instance v10, Lcom/discord/restapi/RestAPIBuilder;

    const-string v4, "https://api.spotify.com/v1/"

    invoke-direct {v10, v4, v7}, Lcom/discord/restapi/RestAPIBuilder;-><init>(Ljava/lang/String;Lb0/p;)V

    const-class v11, Lcom/discord/restapi/RestAPIInterface;

    new-array v4, v9, [Lokhttp3/Interceptor;

    aput-object v6, v4, v8

    new-instance v5, Lcom/discord/restapi/SpotifyTokenInterceptor;

    invoke-direct {v5, v3}, Lcom/discord/restapi/SpotifyTokenInterceptor;-><init>(Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;)V

    const/4 v3, 0x1

    aput-object v5, v4, v3

    invoke-static {v4}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    const-string v16, "client_spotify"

    const/16 v19, 0x46

    invoke-static/range {v10 .. v20}, Lcom/discord/restapi/RestAPIBuilder;->build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/restapi/RestAPIInterface;

    invoke-direct {v2, v3}, Lcom/discord/utilities/rest/RestAPI;-><init>(Lcom/discord/restapi/RestAPIInterface;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->setApiSpotify(Lcom/discord/utilities/rest/RestAPI;)V

    const-class v13, Lcom/discord/restapi/RestAPIInterface$Files;

    invoke-static {v6}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v17

    const/4 v14, 0x0

    const-wide/16 v15, 0x0

    const-string v18, "client_files"

    const/16 v19, 0x0

    const/16 v21, 0x66

    const/16 v22, 0x0

    move-object v12, v1

    invoke-static/range {v12 .. v22}, Lcom/discord/restapi/RestAPIBuilder;->build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/restapi/RestAPIInterface$Files;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->setApiFiles(Lcom/discord/restapi/RestAPIInterface$Files;)V

    return-void
.end method

.method public final setApi(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rest/RestAPI;->access$setApi$cp(Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method public final setApiClientVersions(Lcom/discord/restapi/RestAPIInterface$Dynamic;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rest/RestAPI;->access$setApiClientVersions$cp(Lcom/discord/restapi/RestAPIInterface$Dynamic;)V

    return-void
.end method

.method public final setApiFiles(Lcom/discord/restapi/RestAPIInterface$Files;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rest/RestAPI;->access$setApiFiles$cp(Lcom/discord/restapi/RestAPIInterface$Files;)V

    return-void
.end method

.method public final setApiRtcLatency(Lcom/discord/restapi/RestAPIInterface$RtcLatency;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rest/RestAPI;->access$setApiRtcLatency$cp(Lcom/discord/restapi/RestAPIInterface$RtcLatency;)V

    return-void
.end method

.method public final setApiSerializeNulls(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rest/RestAPI;->access$setApiSerializeNulls$cp(Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method public final setApiSpotify(Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rest/RestAPI;->access$setApiSpotify$cp(Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method public final uploadSystemLog()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/SystemLogUtils;->INSTANCE:Lcom/discord/utilities/SystemLogUtils;

    invoke-virtual {v0}, Lcom/discord/utilities/SystemLogUtils;->fetch()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rest/RestAPI$Companion$uploadSystemLog$1;->INSTANCE:Lcom/discord/utilities/rest/RestAPI$Companion$uploadSystemLog$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "SystemLogUtils.fetch().s\u2026stSubscribeOn()\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
