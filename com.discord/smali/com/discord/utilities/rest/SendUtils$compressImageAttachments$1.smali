.class public final Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;
.super Lx/j/h/a/g;
.source "SendUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/rest/SendUtils;->compressImageAttachments(Landroid/content/Context;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.discord.utilities.rest.SendUtils$compressImageAttachments$1"
    f = "SendUtils.kt"
    l = {
        0x12a
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $attachments:Ljava/util/List;

.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $onCompressed:Lkotlin/jvm/functions/Function1;

.field public L$0:Ljava/lang/Object;

.field public L$1:Ljava/lang/Object;

.field public L$2:Ljava/lang/Object;

.field public L$3:Ljava/lang/Object;

.field public L$4:Ljava/lang/Object;

.field public L$5:Ljava/lang/Object;

.field public L$6:Ljava/lang/Object;

.field public L$7:Ljava/lang/Object;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$attachments:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$onCompressed:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;

    iget-object v1, p0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$attachments:Ljava/util/List;

    iget-object v2, p0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$onCompressed:Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;-><init>(Ljava/util/List;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;

    sget-object p2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 17

    move-object/from16 v0, p0

    sget-object v1, Lx/j/g/a;->d:Lx/j/g/a;

    iget v2, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$7:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    iget-object v4, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$6:Ljava/lang/Object;

    check-cast v4, Lcom/lytefast/flexinput/model/Attachment;

    iget-object v5, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$4:Ljava/lang/Object;

    check-cast v5, Ljava/util/Iterator;

    iget-object v6, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$3:Ljava/lang/Object;

    check-cast v6, Ljava/util/Collection;

    iget-object v7, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$2:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Iterable;

    iget-object v8, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$1:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Iterable;

    iget-object v9, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$0:Ljava/lang/Object;

    check-cast v9, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static/range {p1 .. p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v10, v4

    move-object v11, v9

    move-object v4, v0

    move-object v9, v8

    move-object v8, v7

    move-object v7, v6

    move-object/from16 v6, p1

    goto/16 :goto_1

    :catch_0
    move-object v11, v9

    move-object v9, v8

    move-object v8, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v0

    goto/16 :goto_2

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static/range {p1 .. p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    iget-object v2, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object v4, v0, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$attachments:Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v4, v6}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v9, v2

    move-object v7, v4

    move-object v8, v7

    move-object v2, v5

    move-object v5, v6

    move-object v4, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v10, v6

    check-cast v10, Lcom/lytefast/flexinput/model/Attachment;

    iget-object v11, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$context:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isImage(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v10}, Lcom/lytefast/flexinput/model/Attachment;->getData()Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/String;

    if-eqz v11, :cond_4

    :try_start_1
    iget-object v11, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$context:Landroid/content/Context;

    new-instance v12, Ljava/io/File;

    invoke-virtual {v10}, Lcom/lytefast/flexinput/model/Attachment;->getData()Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_3

    check-cast v13, Ljava/lang/String;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v9, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$0:Ljava/lang/Object;

    iput-object v8, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$1:Ljava/lang/Object;

    iput-object v7, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$2:Ljava/lang/Object;

    iput-object v2, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$3:Ljava/lang/Object;

    iput-object v5, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$4:Ljava/lang/Object;

    iput-object v6, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$5:Ljava/lang/Object;

    iput-object v10, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$6:Ljava/lang/Object;

    iput-object v2, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->L$7:Ljava/lang/Object;

    iput v3, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->label:I

    sget-object v6, Ly/a/h0;->b:Ly/a/v;

    sget-object v13, Lv/a/a/a;->d:Lv/a/a/a;

    new-instance v14, Lv/a/a/b;

    const/4 v15, 0x0

    invoke-direct {v14, v13, v11, v12, v15}, Lv/a/a/b;-><init>(Lkotlin/jvm/functions/Function1;Landroid/content/Context;Ljava/io/File;Lkotlin/coroutines/Continuation;)V

    invoke-static {v6, v14, v4}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-ne v6, v1, :cond_2

    return-object v1

    :cond_2
    move-object v11, v9

    move-object v9, v8

    move-object v8, v7

    move-object v7, v2

    :goto_1
    :try_start_2
    check-cast v6, Ljava/io/File;

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    sget-object v12, Lcom/lytefast/flexinput/model/Attachment;->Companion:Lcom/lytefast/flexinput/model/Attachment$Companion;

    const-string v13, "resultUri"

    invoke-static {v6, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v13, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$context:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "context.contentResolver"

    invoke-static {v13, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12, v6, v13}, Lcom/lytefast/flexinput/model/Attachment$Companion;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v10, v6

    move-object v6, v5

    move-object v5, v4

    goto :goto_3

    :catch_1
    move-object v6, v5

    move-object v5, v4

    move-object v4, v10

    goto :goto_2

    :cond_3
    :try_start_3
    new-instance v6, Ljava/lang/NullPointerException;

    const-string v11, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v6, v11}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-object v6, v5

    move-object v11, v9

    move-object v5, v4

    move-object v9, v8

    move-object v4, v10

    move-object v8, v7

    move-object v7, v2

    :goto_2
    move-object v10, v4

    :goto_3
    move-object v4, v2

    move-object v2, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v11

    move-object/from16 v16, v4

    move-object v4, v2

    move-object/from16 v2, v16

    goto :goto_4

    :cond_4
    move-object v6, v5

    move-object v5, v4

    move-object v4, v2

    :goto_4
    invoke-interface {v2, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v2, v4

    move-object v4, v5

    move-object v5, v6

    goto/16 :goto_0

    :cond_5
    check-cast v2, Ljava/util/List;

    iget-object v1, v4, Lcom/discord/utilities/rest/SendUtils$compressImageAttachments$1;->$onCompressed:Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v1
.end method
