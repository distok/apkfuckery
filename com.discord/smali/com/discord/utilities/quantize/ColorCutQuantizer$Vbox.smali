.class public Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;
.super Ljava/lang/Object;
.source "ColorCutQuantizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/quantize/ColorCutQuantizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Vbox"
.end annotation


# instance fields
.field private lowerIndex:I

.field private maxBlue:I

.field private maxGreen:I

.field private maxRed:I

.field private minBlue:I

.field private minGreen:I

.field private minRed:I

.field public final synthetic this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

.field private upperIndex:I


# direct methods
.method public constructor <init>(Lcom/discord/utilities/quantize/ColorCutQuantizer;II)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    iput p3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->fitBox()V

    return-void
.end method


# virtual methods
.method public canSplit()Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->getColorCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public findSplitPoint()I
    .locals 5

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->getLongestColorDimension()I

    move-result v0

    iget-object v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    iget v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    invoke-static {v1, v0, v2, v3}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$100(Lcom/discord/utilities/quantize/ColorCutQuantizer;III)V

    iget-object v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    invoke-static {v1}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$000(Lcom/discord/utilities/quantize/ColorCutQuantizer;)[I

    move-result-object v1

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    iget v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->sort([III)V

    iget-object v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    iget v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    invoke-static {v1, v0, v2, v3}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$100(Lcom/discord/utilities/quantize/ColorCutQuantizer;III)V

    invoke-virtual {p0, v0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->midPoint(I)I

    move-result v1

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    :goto_0
    iget v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    if-ge v2, v3, :cond_4

    iget-object v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    invoke-static {v3}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$000(Lcom/discord/utilities/quantize/ColorCutQuantizer;)[I

    move-result-object v3

    aget v3, v3, v2

    const/4 v4, -0x3

    if-eq v0, v4, :cond_2

    const/4 v4, -0x2

    if-eq v0, v4, :cond_1

    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    if-le v3, v1, :cond_3

    return v2

    :cond_1
    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    if-lt v3, v1, :cond_3

    return v2

    :cond_2
    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    if-lt v3, v1, :cond_3

    return v2

    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    return v0
.end method

.method public fitBox()V
    .locals 5

    const/16 v0, 0xff

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minBlue:I

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minGreen:I

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minRed:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxBlue:I

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxGreen:I

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxRed:I

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    :goto_0
    iget v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    if-gt v0, v1, :cond_6

    iget-object v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    invoke-static {v1}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$000(Lcom/discord/utilities/quantize/ColorCutQuantizer;)[I

    move-result-object v1

    aget v1, v1, v0

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    iget v4, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxRed:I

    if-le v2, v4, :cond_0

    iput v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxRed:I

    :cond_0
    iget v4, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minRed:I

    if-ge v2, v4, :cond_1

    iput v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minRed:I

    :cond_1
    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxGreen:I

    if-le v3, v2, :cond_2

    iput v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxGreen:I

    :cond_2
    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minGreen:I

    if-ge v3, v2, :cond_3

    iput v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minGreen:I

    :cond_3
    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxBlue:I

    if-le v1, v2, :cond_4

    iput v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxBlue:I

    :cond_4
    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minBlue:I

    if-ge v1, v2, :cond_5

    iput v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minBlue:I

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    return-void
.end method

.method public getAverageColor()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 8

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    iget v5, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    if-gt v0, v5, :cond_0

    iget-object v5, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    invoke-static {v5}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$000(Lcom/discord/utilities/quantize/ColorCutQuantizer;)[I

    move-result-object v5

    aget v5, v5, v0

    iget-object v6, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    invoke-static {v6}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->access$200(Lcom/discord/utilities/quantize/ColorCutQuantizer;)Landroid/util/SparseIntArray;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    add-int/2addr v2, v6

    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v7

    mul-int v7, v7, v6

    add-int/2addr v1, v7

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v7

    mul-int v7, v7, v6

    add-int/2addr v3, v7

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    mul-int v5, v5, v6

    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v3, v3

    div-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v4, v4

    div-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    new-instance v4, Lcom/discord/utilities/quantize/Palette$Swatch;

    invoke-direct {v4, v0, v3, v1, v2}, Lcom/discord/utilities/quantize/Palette$Swatch;-><init>(IIII)V

    return-object v4
.end method

.method public getColorCount()I
    .locals 2

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    iget v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->lowerIndex:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getLongestColorDimension()I
    .locals 4

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxRed:I

    iget v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minRed:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxGreen:I

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minGreen:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxBlue:I

    iget v3, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minBlue:I

    sub-int/2addr v2, v3

    if-lt v0, v1, :cond_0

    if-lt v0, v2, :cond_0

    const/4 v0, -0x3

    return v0

    :cond_0
    if-lt v1, v0, :cond_1

    if-lt v1, v2, :cond_1

    const/4 v0, -0x2

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getVolume()I
    .locals 3

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxRed:I

    iget v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minRed:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxGreen:I

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minGreen:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int v1, v1, v0

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxBlue:I

    iget v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minBlue:I

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    mul-int v0, v0, v1

    return v0
.end method

.method public midPoint(I)I
    .locals 1

    const/4 v0, -0x2

    if-eq p1, v0, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget p1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minRed:I

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxRed:I

    add-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    return p1

    :cond_0
    iget p1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minBlue:I

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxBlue:I

    add-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    return p1

    :cond_1
    iget p1, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->minGreen:I

    iget v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->maxGreen:I

    add-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    return p1
.end method

.method public splitBox()Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;
    .locals 5

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->canSplit()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->findSplitPoint()I

    move-result v0

    new-instance v1, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;

    iget-object v2, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->this$0:Lcom/discord/utilities/quantize/ColorCutQuantizer;

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;-><init>(Lcom/discord/utilities/quantize/ColorCutQuantizer;II)V

    iput v0, p0, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->upperIndex:I

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->fitBox()V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not split a box with only 1 color"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
