.class public final Lcom/discord/utilities/quantize/ColorHistogram;
.super Ljava/lang/Object;
.source "ColorHistogram.java"


# instance fields
.field private final mColorCounts:[I

.field private final mColors:[I

.field private final mNumberColors:I


# direct methods
.method public constructor <init>([I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Arrays;->sort([I)V

    invoke-static {p1}, Lcom/discord/utilities/quantize/ColorHistogram;->countDistinctColors([I)I

    move-result v0

    iput v0, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mNumberColors:I

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColors:[I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColorCounts:[I

    invoke-direct {p0, p1}, Lcom/discord/utilities/quantize/ColorHistogram;->countFrequencies([I)V

    return-void
.end method

.method private static countDistinctColors([I)I
    .locals 4

    array-length v0, p0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    array-length p0, p0

    return p0

    :cond_0
    const/4 v0, 0x0

    aget v0, p0, v0

    const/4 v1, 0x1

    const/4 v2, 0x1

    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_2

    aget v3, p0, v1

    if-eq v3, v0, :cond_1

    aget v0, p0, v1

    add-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method private countFrequencies([I)V
    .locals 6

    array-length v0, p1

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    aget v1, p1, v0

    iget-object v2, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColors:[I

    aput v1, v2, v0

    iget-object v2, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColorCounts:[I

    const/4 v3, 0x1

    aput v3, v2, v0

    array-length v2, p1

    if-ne v2, v3, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x1

    :goto_0
    array-length v4, p1

    if-ge v2, v4, :cond_3

    aget v4, p1, v2

    if-ne v4, v1, :cond_2

    iget-object v4, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColorCounts:[I

    aget v5, v4, v0

    add-int/2addr v5, v3

    aput v5, v4, v0

    goto :goto_1

    :cond_2
    aget v1, p1, v2

    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColors:[I

    aput v1, v4, v0

    iget-object v4, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColorCounts:[I

    aput v3, v4, v0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method public getColorCounts()[I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColorCounts:[I

    return-object v0
.end method

.method public getColors()[I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mColors:[I

    return-object v0
.end method

.method public getNumberOfColors()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/quantize/ColorHistogram;->mNumberColors:I

    return v0
.end method
