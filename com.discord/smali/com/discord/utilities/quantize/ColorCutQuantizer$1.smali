.class public final Lcom/discord/utilities/quantize/ColorCutQuantizer$1;
.super Ljava/lang/Object;
.source "ColorCutQuantizer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/quantize/ColorCutQuantizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;)I
    .locals 0

    invoke-virtual {p2}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->getVolume()I

    move-result p2

    invoke-virtual {p1}, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;->getVolume()I

    move-result p1

    sub-int/2addr p2, p1

    return p2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;

    check-cast p2, Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/quantize/ColorCutQuantizer$1;->compare(Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;Lcom/discord/utilities/quantize/ColorCutQuantizer$Vbox;)I

    move-result p1

    return p1
.end method
