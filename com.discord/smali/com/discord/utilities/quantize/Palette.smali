.class public final Lcom/discord/utilities/quantize/Palette;
.super Ljava/lang/Object;
.source "Palette.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/quantize/Palette$Swatch;
    }
.end annotation


# static fields
.field private static final CALCULATE_BITMAP_MIN_DIMENSION:I = 0x64

.field private static final DEFAULT_CALCULATE_NUMBER_COLORS:I = 0x10

.field private static final MAX_DARK_LUMA:F = 0.45f

.field private static final MAX_MUTED_SATURATION:F = 0.4f

.field private static final MAX_NORMAL_LUMA:F = 0.7f

.field private static final MIN_LIGHT_LUMA:F = 0.55f

.field private static final MIN_NORMAL_LUMA:F = 0.3f

.field private static final MIN_VIBRANT_SATURATION:F = 0.35f

.field private static final TARGET_DARK_LUMA:F = 0.26f

.field private static final TARGET_LIGHT_LUMA:F = 0.74f

.field private static final TARGET_MUTED_SATURATION:F = 0.3f

.field private static final TARGET_NORMAL_LUMA:F = 0.5f

.field private static final TARGET_VIBRANT_SATURATION:F = 1.0f


# instance fields
.field private mDarkMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

.field private mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

.field private final mHighestPopulation:I

.field private mLightMutedColor:Lcom/discord/utilities/quantize/Palette$Swatch;

.field private mLightVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

.field private mMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

.field private final mSwatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/quantize/Palette$Swatch;",
            ">;"
        }
    .end annotation
.end field

.field private mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/utilities/quantize/Palette$Swatch;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mSwatches:Ljava/util/List;

    invoke-direct {p0}, Lcom/discord/utilities/quantize/Palette;->findMaxPopulation()I

    move-result p1

    iput p1, p0, Lcom/discord/utilities/quantize/Palette;->mHighestPopulation:I

    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f333333    # 0.7f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/quantize/Palette;->findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    const v1, 0x3f3d70a4    # 0.74f

    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/quantize/Palette;->findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mLightVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    const v1, 0x3e851eb8    # 0.26f

    const/4 v2, 0x0

    const v3, 0x3ee66666    # 0.45f

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/quantize/Palette;->findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f333333    # 0.7f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/quantize/Palette;->findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    const v1, 0x3f3d70a4    # 0.74f

    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/quantize/Palette;->findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mLightMutedColor:Lcom/discord/utilities/quantize/Palette$Swatch;

    const v1, 0x3e851eb8    # 0.26f

    const/4 v2, 0x0

    const v3, 0x3ee66666    # 0.45f

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/quantize/Palette;->findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/quantize/Palette;->mDarkMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    invoke-direct {p0}, Lcom/discord/utilities/quantize/Palette;->generateEmptySwatches()V

    return-void
.end method

.method private static checkBitmapParam(Landroid/graphics/Bitmap;)V
    .locals 1

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "bitmap can not be recycled"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "bitmap can not be null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static checkNumberColorsParam(I)V
    .locals 1

    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "numColors must be 1 of greater"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static copyHslValues(Lcom/discord/utilities/quantize/Palette$Swatch;)[F
    .locals 3

    const/4 v0, 0x3

    new-array v1, v0, [F

    invoke-virtual {p0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getHsl()[F

    move-result-object p0

    const/4 v2, 0x0

    invoke-static {p0, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method private static createComparisonValue(FFFFII)F
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [F

    invoke-static {p0, p1}, Lcom/discord/utilities/quantize/Palette;->invertDiff(FF)F

    move-result p0

    const/4 p1, 0x0

    aput p0, v0, p1

    const/4 p0, 0x1

    const/high16 p1, 0x40400000    # 3.0f

    aput p1, v0, p0

    invoke-static {p2, p3}, Lcom/discord/utilities/quantize/Palette;->invertDiff(FF)F

    move-result p0

    const/4 p1, 0x2

    aput p0, v0, p1

    const/4 p0, 0x3

    const/high16 p1, 0x40d00000    # 6.5f

    aput p1, v0, p0

    int-to-float p0, p4

    int-to-float p1, p5

    div-float/2addr p0, p1

    const/4 p1, 0x4

    aput p0, v0, p1

    const/4 p0, 0x5

    const/high16 p1, 0x3f000000    # 0.5f

    aput p1, v0, p0

    invoke-static {v0}, Lcom/discord/utilities/quantize/Palette;->weightedMean([F)F

    move-result p0

    return p0
.end method

.method private findColor(FFFFFF)Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 13

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/utilities/quantize/Palette;->mSwatches:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/utilities/quantize/Palette$Swatch;

    invoke-virtual {v4}, Lcom/discord/utilities/quantize/Palette$Swatch;->getHsl()[F

    move-result-object v5

    const/4 v6, 0x1

    aget v7, v5, v6

    invoke-virtual {v4}, Lcom/discord/utilities/quantize/Palette$Swatch;->getHsl()[F

    move-result-object v5

    const/4 v6, 0x2

    aget v9, v5, v6

    cmpl-float v5, v7, p5

    if-ltz v5, :cond_0

    cmpg-float v5, v7, p6

    if-gtz v5, :cond_0

    cmpl-float v5, v9, p2

    if-ltz v5, :cond_0

    cmpg-float v5, v9, p3

    if-gtz v5, :cond_0

    invoke-direct {p0, v4}, Lcom/discord/utilities/quantize/Palette;->isAlreadySelected(Lcom/discord/utilities/quantize/Palette$Swatch;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4}, Lcom/discord/utilities/quantize/Palette$Swatch;->getPopulation()I

    move-result v11

    iget v12, v0, Lcom/discord/utilities/quantize/Palette;->mHighestPopulation:I

    move/from16 v8, p4

    move v10, p1

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/quantize/Palette;->createComparisonValue(FFFFII)F

    move-result v5

    if-eqz v2, :cond_1

    cmpl-float v6, v5, v3

    if-lez v6, :cond_0

    :cond_1
    move-object v2, v4

    move v3, v5

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method private findMaxPopulation()I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mSwatches:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/quantize/Palette$Swatch;

    invoke-virtual {v2}, Lcom/discord/utilities/quantize/Palette$Swatch;->getPopulation()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static generate(Landroid/graphics/Bitmap;)Lcom/discord/utilities/quantize/Palette;
    .locals 1

    const/16 v0, 0x10

    invoke-static {p0, v0}, Lcom/discord/utilities/quantize/Palette;->generate(Landroid/graphics/Bitmap;I)Lcom/discord/utilities/quantize/Palette;

    move-result-object p0

    return-object p0
.end method

.method public static generate(Landroid/graphics/Bitmap;I)Lcom/discord/utilities/quantize/Palette;
    .locals 1

    invoke-static {p0}, Lcom/discord/utilities/quantize/Palette;->checkBitmapParam(Landroid/graphics/Bitmap;)V

    invoke-static {p1}, Lcom/discord/utilities/quantize/Palette;->checkNumberColorsParam(I)V

    invoke-static {p0}, Lcom/discord/utilities/quantize/Palette;->scaleBitmapDown(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->fromBitmap(Landroid/graphics/Bitmap;I)Lcom/discord/utilities/quantize/ColorCutQuantizer;

    move-result-object p1

    if-eq v0, p0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    new-instance p0, Lcom/discord/utilities/quantize/Palette;

    invoke-virtual {p1}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->getQuantizedColors()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/quantize/Palette;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method private generateEmptySwatches()V
    .locals 4

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/discord/utilities/quantize/Palette;->copyHslValues(Lcom/discord/utilities/quantize/Palette$Swatch;)[F

    move-result-object v0

    const/high16 v3, 0x3f000000    # 0.5f

    aput v3, v0, v2

    new-instance v3, Lcom/discord/utilities/quantize/Palette$Swatch;

    invoke-static {v0}, Lcom/discord/utilities/quantize/ColorUtils;->HSLtoRGB([F)I

    move-result v0

    invoke-direct {v3, v0, v1}, Lcom/discord/utilities/quantize/Palette$Swatch;-><init>(II)V

    iput-object v3, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/discord/utilities/quantize/Palette;->copyHslValues(Lcom/discord/utilities/quantize/Palette$Swatch;)[F

    move-result-object v0

    const v3, 0x3e851eb8    # 0.26f

    aput v3, v0, v2

    new-instance v2, Lcom/discord/utilities/quantize/Palette$Swatch;

    invoke-static {v0}, Lcom/discord/utilities/quantize/ColorUtils;->HSLtoRGB([F)I

    move-result v0

    invoke-direct {v2, v0, v1}, Lcom/discord/utilities/quantize/Palette$Swatch;-><init>(II)V

    iput-object v2, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    :cond_1
    return-void
.end method

.method private static invertDiff(FF)F
    .locals 0

    sub-float/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result p0

    const/high16 p1, 0x3f800000    # 1.0f

    sub-float/2addr p1, p0

    return p1
.end method

.method private isAlreadySelected(Lcom/discord/utilities/quantize/Palette$Swatch;)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mLightVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mLightMutedColor:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private static scaleBitmapDown(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x64

    if-gt v0, v1, :cond_0

    return-object p0

    :cond_0
    const/high16 v1, 0x42c80000    # 100.0f

    int-to-float v0, v0

    div-float/2addr v1, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private static varargs weightedMean([F)F
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    aget v3, p0, v2

    add-int/lit8 v4, v2, 0x1

    aget v4, p0, v4

    mul-float v3, v3, v4

    add-float/2addr v0, v3

    add-float/2addr v1, v4

    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_0
    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getDarkMutedColor(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    :cond_0
    return p1
.end method

.method public getDarkMutedSwatch()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    return-object v0
.end method

.method public getDarkVibrantColor(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    :cond_0
    return p1
.end method

.method public getDarkVibrantSwatch()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mDarkVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    return-object v0
.end method

.method public getLightMutedColor(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mLightMutedColor:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    :cond_0
    return p1
.end method

.method public getLightMutedSwatch()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mLightMutedColor:Lcom/discord/utilities/quantize/Palette$Swatch;

    return-object v0
.end method

.method public getLightVibrantColor(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mLightVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    :cond_0
    return p1
.end method

.method public getLightVibrantSwatch()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mLightVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    return-object v0
.end method

.method public getMutedColor(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    :cond_0
    return p1
.end method

.method public getMutedSwatch()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mMutedSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    return-object v0
.end method

.method public getSwatches()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/quantize/Palette$Swatch;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mSwatches:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVibrantColor(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    :cond_0
    return p1
.end method

.method public getVibrantSwatch()Lcom/discord/utilities/quantize/Palette$Swatch;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/quantize/Palette;->mVibrantSwatch:Lcom/discord/utilities/quantize/Palette$Swatch;

    return-object v0
.end method
