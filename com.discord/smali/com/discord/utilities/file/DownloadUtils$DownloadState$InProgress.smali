.class public final Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;
.super Lcom/discord/utilities/file/DownloadUtils$DownloadState;
.source "DownloadUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/file/DownloadUtils$DownloadState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InProgress"
.end annotation


# instance fields
.field private final progress:F


# direct methods
.method public constructor <init>(F)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;->progress:F

    return-void
.end method


# virtual methods
.method public final getProgress()F
    .locals 1

    iget v0, p0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;->progress:F

    return v0
.end method
