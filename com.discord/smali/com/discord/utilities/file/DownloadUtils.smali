.class public final Lcom/discord/utilities/file/DownloadUtils;
.super Ljava/lang/Object;
.source "DownloadUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/file/DownloadUtils$DownloadState;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/file/DownloadUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/file/DownloadUtils;

    invoke-direct {v0}, Lcom/discord/utilities/file/DownloadUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/file/DownloadUtils;->INSTANCE:Lcom/discord/utilities/file/DownloadUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final downloadFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lrx/Observable;
    .locals 1
    .annotation build Landroidx/annotation/RequiresPermission;
        conditional = true
        value = "android.permission.WRITE_EXTERNAL_STORAGE"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/file/DownloadUtils$DownloadState;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "fileUrl"

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "fileName"

    invoke-static {p2, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "downloadDirectory"

    invoke-static {p3, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;

    invoke-direct {p0, p1, p3, p2}, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;-><init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    sget-object p1, Lrx/Emitter$BackpressureMode;->f:Lrx/Emitter$BackpressureMode;

    invoke-static {p0, p1}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    const-string p1, "Observable.create({ emit\u2026.BackpressureMode.BUFFER)"

    invoke-static {p0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic downloadFile$default(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p4, p4, 0x8

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p3

    const-string p4, "context.cacheDir"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/discord/utilities/file/DownloadUtils;->downloadFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method
