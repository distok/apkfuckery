.class public abstract Lcom/discord/utilities/file/DownloadUtils$DownloadState;
.super Ljava/lang/Object;
.source "DownloadUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/file/DownloadUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DownloadState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;,
        Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;,
        Lcom/discord/utilities/file/DownloadUtils$DownloadState$Failure;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState;-><init>()V

    return-void
.end method
