.class public final Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;
.super Lx/m/c/k;
.source "DownloadUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;->call(Lrx/Emitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokhttp3/ResponseBody;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $emitter:Lrx/Emitter;

.field public final synthetic this$0:Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;Lrx/Emitter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->this$0:Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;

    iput-object p2, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->$emitter:Lrx/Emitter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lokhttp3/ResponseBody;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->invoke(Lokhttp3/ResponseBody;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokhttp3/ResponseBody;)V
    .locals 10

    const-string v0, "responseBody"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->this$0:Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;

    iget-object v2, v1, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;->$downloadDirectory:Ljava/io/File;

    iget-object v1, v1, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1;->$fileName:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/16 v1, 0x2000

    new-array v1, v1, [B

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p1}, Lokhttp3/ResponseBody;->a()J

    move-result-wide v3

    long-to-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->c()Lc0/g;

    move-result-object v6

    invoke-interface {v6}, Lc0/g;->K0()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/io/InputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_0

    iget-object v1, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->$emitter:Lrx/Emitter;

    new-instance v2, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    invoke-direct {v2, v0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;-><init>(Ljava/io/File;)V

    invoke-interface {v1, v2}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->$emitter:Lrx/Emitter;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    goto :goto_1

    :cond_0
    add-int/2addr v5, v6

    iget-object v7, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->$emitter:Lrx/Emitter;

    new-instance v8, Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;

    int-to-float v9, v5

    div-float/2addr v9, v3

    invoke-direct {v8, v9}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;-><init>(F)V

    invoke-interface {v7, v8}, Lg0/g;->onNext(Ljava/lang/Object;)V

    invoke-virtual {v2, v1, v4, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    iget-object v1, p0, Lcom/discord/utilities/file/DownloadUtils$downloadFile$1$1;->$emitter:Lrx/Emitter;

    new-instance v2, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Failure;

    invoke-direct {v2, v0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Failure;-><init>(Ljava/lang/Exception;)V

    invoke-interface {v1, v2}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->close()V

    return-void

    :goto_2
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->close()V

    throw v0
.end method
