.class public final Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;
.super Lx/m/c/k;
.source "UriHandler.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/uri/UriHandler;->handleOrUntrusted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;->$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;->$url:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;->invoke(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Boolean;)V
    .locals 23

    move-object/from16 v0, p0

    const-string v1, "isTrusted"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/discord/utilities/uri/UriHandler;->INSTANCE:Lcom/discord/utilities/uri/UriHandler;

    iget-object v3, v0, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;->$context:Landroid/content/Context;

    iget-object v4, v0, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;->$url:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/uri/UriHandler;->handle$default(Lcom/discord/utilities/uri/UriHandler;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/discord/stores/StoreNotices$Notice;

    sget-object v2, Lf/a/a/d/a;->i:Lf/a/a/d/a$b;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const-wide/16 v18, 0x0

    const/4 v2, 0x5

    new-array v2, v2, [Lx/q/b;

    const/4 v3, 0x0

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v4}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lcom/discord/widgets/user/WidgetUserMentions;

    invoke-static {v4}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Lcom/discord/widgets/search/WidgetSearch;

    invoke-static {v4}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-class v4, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;

    invoke-static {v4}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v4}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    const-wide/16 v16, 0x0

    new-instance v2, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1$notice$1;

    invoke-direct {v2, v0}, Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1$notice$1;-><init>(Lcom/discord/utilities/uri/UriHandler$handleOrUntrusted$1;)V

    const/16 v21, 0x6

    const/16 v22, 0x0

    const-string v9, "WIDGET_SPOOPY_LINKS_DIALOG"

    move-object v8, v1

    move-object/from16 v20, v2

    invoke-direct/range {v8 .. v22}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    :goto_0
    return-void
.end method
