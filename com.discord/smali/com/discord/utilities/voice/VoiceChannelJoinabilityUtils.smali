.class public final Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;
.super Ljava/lang/Object;
.source "VoiceChannelJoinabilityUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;

    invoke-direct {v0}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->INSTANCE:Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic observeJoinability$default(Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceStates;ILjava/lang/Object;)Lrx/Observable;
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object p3

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p7, 0x4

    if-eqz p3, :cond_1

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p4

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p7, 0x8

    if-eqz p3, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object p5

    :cond_2
    move-object v5, p5

    and-int/lit8 p3, p7, 0x10

    if-eqz p3, :cond_3

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getVoiceStates()Lcom/discord/stores/StoreVoiceStates;

    move-result-object p6

    :cond_3
    move-object v6, p6

    move-object v0, p0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->observeJoinability(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceStates;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getJoinability(Lcom/discord/models/domain/ModelChannel;Ljava/util/Collection;Ljava/lang/Long;Ljava/lang/Integer;I)Lcom/discord/utilities/voice/VoiceChannelJoinability;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "I)",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;"
        }
    .end annotation

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelVoiceStates"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CAN_JOIN:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    return-object p1

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-lez p5, :cond_1

    const/4 p5, 0x1

    goto :goto_0

    :cond_1
    const/4 p5, 0x0

    :goto_0
    const-wide/32 v2, 0x100000

    invoke-static {v2, v3, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v2

    if-eqz v2, :cond_10

    if-eqz p5, :cond_2

    goto/16 :goto_9

    :cond_2
    new-instance p5, Ljava/util/ArrayList;

    invoke-direct {p5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/models/domain/ModelVoice$State;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelVoice$State;->getChannelId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    if-nez v3, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v6, v4

    if-nez v3, :cond_5

    const/4 v3, 0x1

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_3

    invoke-interface {p5, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p2

    invoke-interface {p5}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    const/4 p5, 0x0

    goto :goto_4

    :cond_8
    invoke-interface {p5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :cond_9
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelVoice$State;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 p5, 0x1

    :goto_4
    const-wide/32 v2, 0x1000000

    invoke-static {v2, v3, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getUserLimit()I

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getUserLimit()I

    move-result p1

    goto :goto_5

    :cond_a
    const p1, 0x7fffffff

    :goto_5
    if-lt p2, p1, :cond_b

    if-nez v2, :cond_b

    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CHANNEL_FULL:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    return-object p1

    :cond_b
    const-wide/16 v3, 0x8

    invoke-static {v3, v4, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p1

    if-eqz p5, :cond_c

    if-eqz p4, :cond_c

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p3

    if-lt p2, p3, :cond_c

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    :goto_6
    if-eqz v0, :cond_f

    if-nez p1, :cond_f

    if-nez p4, :cond_d

    goto :goto_7

    :cond_d
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p2, p1, :cond_e

    if-eqz v2, :cond_e

    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CAN_JOIN:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    goto :goto_8

    :cond_e
    :goto_7
    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->GUILD_VIDEO_AT_CAPACITY:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    :goto_8
    return-object p1

    :cond_f
    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CAN_JOIN:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    return-object p1

    :cond_10
    :goto_9
    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->PERMISSIONS_MISSING:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    return-object p1
.end method

.method public final observeJoinability(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceStates;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/stores/StoreGuilds;",
            "Lcom/discord/stores/StorePermissions;",
            "Lcom/discord/stores/StoreVoiceStates;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
            ">;"
        }
    .end annotation

    const-string v0, "channelsStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildsStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionsStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voiceStatesStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3, p1, p2}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object p3

    new-instance v6, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;

    move-object v0, v6

    move-object v1, p6

    move-wide v2, p1

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;-><init>(Lcom/discord/stores/StoreVoiceStates;JLcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuilds;)V

    invoke-virtual {p3, v6}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "channelsStore\n        .o\u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
