.class public final Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;
.super Ljava/lang/Object;
.source "CallSoundManager.kt"

# interfaces
.implements Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/CallSoundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreStateGenerator"
.end annotation


# instance fields
.field private final storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

.field private final storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final storeVoiceParticipants:Lcom/discord/stores/StoreVoiceParticipants;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;-><init>(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;)V
    .locals 1

    const-string/jumbo v0, "storeVoiceParticipants"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeRtcConnection"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeApplicationStreaming"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeVoiceParticipants:Lcom/discord/stores/StoreVoiceParticipants;

    iput-object p2, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

    iput-object p3, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object p4, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeUser:Lcom/discord/stores/StoreUser;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getVoiceParticipants()Lcom/discord/stores/StoreVoiceParticipants;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    sget-object p4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p4

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;-><init>(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;)V

    return-void
.end method


# virtual methods
.method public observeStoreState(J)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/voice/CallSoundManager$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeVoiceParticipants:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreVoiceParticipants;->get(J)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {p2}, Lcom/discord/stores/StoreRtcConnection;->getConnectionState()Lrx/Observable;

    move-result-object p2

    iget-object v0, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getState()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;->INSTANCE:Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;

    invoke-static {p1, p2, v0, v1, v2}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026     me\n        )\n      }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
