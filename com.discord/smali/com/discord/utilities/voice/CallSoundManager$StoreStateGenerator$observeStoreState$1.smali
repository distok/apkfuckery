.class public final Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;
.super Ljava/lang/Object;
.source "CallSoundManager.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;->observeStoreState(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;",
        "Lcom/discord/rtcconnection/RtcConnection$State;",
        "Lcom/discord/stores/StoreApplicationStreaming$State;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/utilities/voice/CallSoundManager$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;->INSTANCE:Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreApplicationStreaming$State;Lcom/discord/models/domain/ModelUser;)Lcom/discord/utilities/voice/CallSoundManager$StoreState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Lcom/discord/rtcconnection/RtcConnection$State;",
            "Lcom/discord/stores/StoreApplicationStreaming$State;",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lcom/discord/utilities/voice/CallSoundManager$StoreState;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/voice/CallSoundManager$StoreState;

    const-string/jumbo v1, "voiceParticipants"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rtcConnectionState"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "streamingState"

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "me"

    invoke-static {p4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/utilities/voice/CallSoundManager$StoreState;-><init>(Ljava/util/Map;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreApplicationStreaming$State;Lcom/discord/models/domain/ModelUser;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    check-cast p2, Lcom/discord/rtcconnection/RtcConnection$State;

    check-cast p3, Lcom/discord/stores/StoreApplicationStreaming$State;

    check-cast p4, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator$observeStoreState$1;->call(Ljava/util/Map;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreApplicationStreaming$State;Lcom/discord/models/domain/ModelUser;)Lcom/discord/utilities/voice/CallSoundManager$StoreState;

    move-result-object p1

    return-object p1
.end method
