.class public final Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;
.super Ljava/lang/Object;
.source "VoiceEngineForegroundService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/VoiceEngineForegroundService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Connection"
.end annotation


# instance fields
.field private final connection:Landroid/content/ServiceConnection;

.field private final context:Landroid/content/Context;

.field private isUnbinding:Z

.field private service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->context:Landroid/content/Context;

    new-instance p1, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;

    invoke-direct {p1, p0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;-><init>(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->connection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public static final synthetic access$getService$p(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)Lcom/discord/utilities/voice/VoiceEngineForegroundService;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    return-object p0
.end method

.method public static final synthetic access$setService$p(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Lcom/discord/utilities/voice/VoiceEngineForegroundService;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    return-void
.end method


# virtual methods
.method public final getConnection()Landroid/content/ServiceConnection;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->connection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->context:Landroid/content/Context;

    return-object v0
.end method

.method public final getService()Lcom/discord/utilities/voice/VoiceEngineForegroundService;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    return-object v0
.end method

.method public final declared-synchronized isUnbinding()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->isUnbinding:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setUnbinding(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->isUnbinding:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
