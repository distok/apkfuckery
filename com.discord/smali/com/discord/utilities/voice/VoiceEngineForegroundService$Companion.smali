.class public final Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;
.super Ljava/lang/Object;
.source "VoiceEngineForegroundService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/VoiceEngineForegroundService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION;,
        Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$EXTRA;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getOnDisconnect()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->access$getOnDisconnect$cp()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    return-object v0
.end method

.method public final getOnToggleSelfDeafen()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->access$getOnToggleSelfDeafen$cp()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    return-object v0
.end method

.method public final getOnToggleSelfMute()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->access$getOnToggleSelfMute$cp()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    return-object v0
.end method

.method public final setOnDisconnect(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->access$setOnDisconnect$cp(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final setOnToggleSelfDeafen(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->access$setOnToggleSelfDeafen$cp(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final setOnToggleSelfMute(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->access$setOnToggleSelfMute$cp(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final startForegroundAndBind(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Ljava/lang/String;Ljava/lang/String;ZZZJLjava/lang/Long;Z)V
    .locals 10

    move-object v0, p2

    move-object v1, p3

    const-string v2, "connection"

    move-object v3, p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "title"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "subtitle"

    invoke-static {p3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    sget-object v4, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v5, "DiscordVoiceService"

    const-string v6, "Bind service connection."

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/discord/utilities/logging/Logger;->v$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "com.discord.utilities.voice.action.start_foreground"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "com.discord.utilities.voice.extra.title"

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.title_subtext"

    invoke-virtual {v4, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.item_muted"

    move v1, p4

    invoke-virtual {v4, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.item_deafened"

    move v1, p5

    invoke-virtual {v4, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.item_streaming"

    move/from16 v1, p6

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.proximity_lock_enabled"

    move/from16 v1, p10

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.channel_id"

    move-wide/from16 v5, p7

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "com.discord.utilities.voice.extra.guild_id"

    move-object/from16 v1, p9

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {v2, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v4, Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getConnection()Landroid/content/ServiceConnection;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v2, "DiscordVoiceService"

    const-string v3, "Unable to bind service connection."

    invoke-virtual {v1, v2, v3, v0}, Lcom/discord/utilities/logging/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public final startStream(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionIntent"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.discord.utilities.voice.action.start_stream"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public final stopForegroundAndUnbind(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V
    .locals 7

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getService()Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->isUnbinding()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v2, "DiscordVoiceService"

    const-string v3, "Unbind service connection."

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->v$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->setUnbinding(Z)V

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.discord.utilities.voice.action.stop"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getConnection()Landroid/content/ServiceConnection;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "DiscordVoiceService"

    const-string v2, "Unable to unbind service connection."

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/utilities/logging/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public final stopStream(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V
    .locals 2

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.discord.utilities.voice.action.stop_stream"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
