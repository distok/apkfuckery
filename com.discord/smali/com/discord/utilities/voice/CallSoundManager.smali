.class public final Lcom/discord/utilities/voice/CallSoundManager;
.super Ljava/lang/Object;
.source "CallSoundManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/voice/CallSoundManager$StoreState;,
        Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;,
        Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;,
        Lcom/discord/utilities/voice/CallSoundManager$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/voice/CallSoundManager$Companion;

.field private static final JOIN_LEAVE_USER_LIMIT:I = 0x19


# instance fields
.field private activeStreamKey:Ljava/lang/String;

.field private activeStreamUserId:Ljava/lang/Long;

.field private activeStreamViewerCount:I

.field private final appComponent:Lcom/discord/app/AppComponent;

.field private final appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

.field private final storeStateGenerator:Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;

.field private storeStateSubscription:Lrx/Subscription;

.field private streamingUserIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private voiceChannelId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/voice/CallSoundManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/voice/CallSoundManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/voice/CallSoundManager;->Companion:Lcom/discord/utilities/voice/CallSoundManager$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/app/AppComponent;Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;)V
    .locals 1

    const-string v0, "appComponent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appSoundManager"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStateGenerator"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/CallSoundManager;->appComponent:Lcom/discord/app/AppComponent;

    iput-object p2, p0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    iput-object p3, p0, Lcom/discord/utilities/voice/CallSoundManager;->storeStateGenerator:Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/utilities/voice/CallSoundManager;->streamingUserIds:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/app/AppComponent;Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    sget-object p2, Lcom/discord/utilities/media/AppSoundManager$Provider;->INSTANCE:Lcom/discord/utilities/media/AppSoundManager$Provider;

    invoke-virtual {p2}, Lcom/discord/utilities/media/AppSoundManager$Provider;->get()Lcom/discord/utilities/media/AppSoundManager;

    move-result-object p2

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    new-instance p3, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;-><init>(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/voice/CallSoundManager;-><init>(Lcom/discord/app/AppComponent;Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;)V

    return-void
.end method

.method public static final synthetic access$getAppSoundManager$p(Lcom/discord/utilities/voice/CallSoundManager;)Lcom/discord/utilities/media/AppSoundManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    return-object p0
.end method

.method public static final synthetic access$getStoreStateSubscription$p(Lcom/discord/utilities/voice/CallSoundManager;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/CallSoundManager;->storeStateSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/utilities/voice/CallSoundManager;JLcom/discord/utilities/voice/CallSoundManager$StoreState;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/voice/CallSoundManager;->handleStoreState(JLcom/discord/utilities/voice/CallSoundManager$StoreState;)V

    return-void
.end method

.method public static final synthetic access$setStoreStateSubscription$p(Lcom/discord/utilities/voice/CallSoundManager;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/CallSoundManager;->storeStateSubscription:Lrx/Subscription;

    return-void
.end method

.method private final handleStoreState(JLcom/discord/utilities/voice/CallSoundManager$StoreState;)V
    .locals 16
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/voice/CallSoundManager$StoreState;->getStreamingState()Lcom/discord/stores/StoreApplicationStreaming$State;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/utilities/voice/CallSoundManager;->voiceChannelId:Ljava/lang/Long;

    iget-object v3, v0, Lcom/discord/utilities/voice/CallSoundManager;->streamingUserIds:Ljava/util/List;

    iget v4, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamViewerCount:I

    iget-object v5, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamKey:Ljava/lang/String;

    iget-object v6, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamUserId:Ljava/lang/Long;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v0, Lcom/discord/utilities/voice/CallSoundManager;->voiceChannelId:Ljava/lang/Long;

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$State;->getStreamsByUser()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v9, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    move-object v12, v9

    check-cast v12, Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v12}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v12

    cmp-long v14, v12, p1

    if-nez v14, :cond_1

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    :goto_1
    if-eqz v10, :cond_0

    invoke-interface {v8, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    const/16 v9, 0xa

    invoke-static {v8, v9}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iput-object v7, v0, Lcom/discord/utilities/voice/CallSoundManager;->streamingUserIds:Ljava/util/List;

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$State;->getActiveApplicationStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {v7}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v8}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v8

    if-ne v8, v10, :cond_8

    invoke-virtual {v7}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v8

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamKey:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamUserId:Ljava/lang/Long;

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$State;->getStreamSpectators()Ljava/util/Map;

    move-result-object v1

    iget-object v7, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamKey:Ljava/lang/String;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_7

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Ljava/lang/Number;

    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v12

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/voice/CallSoundManager$StoreState;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v14

    cmp-long v9, v12, v14

    if-eqz v9, :cond_5

    const/4 v9, 0x1

    goto :goto_4

    :cond_5
    const/4 v9, 0x0

    :goto_4
    if-eqz v9, :cond_4

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_5

    :cond_7
    const/4 v1, 0x0

    :goto_5
    iput v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamViewerCount:I

    goto :goto_6

    :cond_8
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamKey:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamUserId:Ljava/lang/Long;

    iput v11, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamViewerCount:I

    :goto_6
    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamKey:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-static {v1, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    goto :goto_7

    :cond_9
    const/4 v1, 0x0

    :goto_7
    iget-object v5, v0, Lcom/discord/utilities/voice/CallSoundManager;->streamingUserIds:Ljava/util/List;

    instance-of v7, v5, Ljava/util/Collection;

    if-eqz v7, :cond_b

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_b

    :cond_a
    const/4 v5, 0x0

    goto :goto_8

    :cond_b
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v10

    if-eqz v7, :cond_c

    const/4 v5, 0x1

    :goto_8
    instance-of v7, v3, Ljava/util/Collection;

    if-eqz v7, :cond_e

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_e

    :cond_d
    const/4 v3, 0x0

    goto :goto_b

    :cond_e
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->longValue()J

    move-result-wide v7

    iget-object v9, v0, Lcom/discord/utilities/voice/CallSoundManager;->streamingUserIds:Ljava/util/List;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_12

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/voice/CallSoundManager$StoreState;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v12

    cmp-long v9, v7, v12

    if-eqz v9, :cond_11

    if-nez v6, :cond_10

    goto :goto_9

    :cond_10
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    cmp-long v9, v7, v12

    if-nez v9, :cond_12

    :cond_11
    const/4 v7, 0x1

    goto :goto_a

    :cond_12
    :goto_9
    const/4 v7, 0x0

    :goto_a
    if-eqz v7, :cond_f

    const/4 v3, 0x1

    :goto_b
    const/16 v6, 0x19

    if-eqz v1, :cond_13

    if-gt v4, v6, :cond_13

    iget v7, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamViewerCount:I

    if-le v7, v4, :cond_13

    const/4 v7, 0x1

    goto :goto_c

    :cond_13
    const/4 v7, 0x0

    :goto_c
    if-eqz v1, :cond_14

    if-gt v4, v6, :cond_14

    iget v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->activeStreamViewerCount:I

    if-ge v1, v4, :cond_14

    goto :goto_d

    :cond_14
    const/4 v10, 0x0

    :goto_d
    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->voiceChannelId:Ljava/lang/Long;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    if-eqz v5, :cond_15

    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    sget-object v2, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_STREAM_STARTED()Lcom/discord/utilities/media/AppSound;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/media/AppSoundManager;->play(Lcom/discord/utilities/media/AppSound;)V

    goto :goto_e

    :cond_15
    if-eqz v3, :cond_16

    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    sget-object v2, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_STREAM_ENDED()Lcom/discord/utilities/media/AppSound;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/media/AppSoundManager;->play(Lcom/discord/utilities/media/AppSound;)V

    goto :goto_e

    :cond_16
    if-eqz v7, :cond_17

    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    sget-object v2, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_STREAM_USER_JOINED()Lcom/discord/utilities/media/AppSound;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/media/AppSoundManager;->play(Lcom/discord/utilities/media/AppSound;)V

    goto :goto_e

    :cond_17
    if-eqz v10, :cond_18

    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    sget-object v2, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_STREAM_USER_LEFT()Lcom/discord/utilities/media/AppSound;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/media/AppSoundManager;->play(Lcom/discord/utilities/media/AppSound;)V

    :cond_18
    :goto_e
    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/voice/CallSoundManager$StoreState;->getVoiceParticipants()Ljava/util/Map;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/voice/CallSoundManager$StoreState;->getRtcConnectionState()Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v2

    sget-object v3, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_19
    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v6}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_f

    :cond_1a
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1b
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v6}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result v6

    if-eqz v6, :cond_1b

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_10

    :cond_1c
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1d

    if-lez v1, :cond_1d

    iget-object v5, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    sget-object v6, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v6}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_CALL_CALLING()Lcom/discord/utilities/media/AppSound;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/discord/utilities/media/AppSoundManager;->isPlaying(Lcom/discord/utilities/media/AppSound;)Z

    move-result v5

    if-nez v5, :cond_1d

    if-eqz v2, :cond_1d

    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    invoke-virtual {v6}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_CALL_CALLING()Lcom/discord/utilities/media/AppSound;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/media/AppSoundManager;->play(Lcom/discord/utilities/media/AppSound;)V

    goto :goto_11

    :cond_1d
    if-eqz v2, :cond_1e

    if-eqz v1, :cond_1e

    if-lt v3, v4, :cond_1f

    :cond_1e
    iget-object v1, v0, Lcom/discord/utilities/voice/CallSoundManager;->appSoundManager:Lcom/discord/utilities/media/AppSoundManager;

    sget-object v2, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_CALL_CALLING()Lcom/discord/utilities/media/AppSound;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/media/AppSoundManager;->stop(Lcom/discord/utilities/media/AppSound;)V

    :cond_1f
    :goto_11
    return-void
.end method


# virtual methods
.method public final subscribeToStoreState(J)V
    .locals 13

    iget-object v0, p0, Lcom/discord/utilities/voice/CallSoundManager;->storeStateSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/voice/CallSoundManager;->storeStateGenerator:Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;

    invoke-interface {v0, p1, p2}, Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;->observeStoreState(J)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "storeStateGenerator\n    \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/CallSoundManager;->appComponent:Lcom/discord/app/AppComponent;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/utilities/voice/CallSoundManager;

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$1;

    invoke-direct {v7, p0}, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$1;-><init>(Lcom/discord/utilities/voice/CallSoundManager;)V

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$2;

    invoke-direct {v9, p0}, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$2;-><init>(Lcom/discord/utilities/voice/CallSoundManager;)V

    new-instance v10, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;

    invoke-direct {v10, p0, p1, p2}, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;-><init>(Lcom/discord/utilities/voice/CallSoundManager;J)V

    const/16 v11, 0xa

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
