.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;
.super Ljava/lang/Object;
.source "VoiceEngineServiceController.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;->call(Lcom/discord/rtcconnection/RtcConnection$State;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $connectionState:Lcom/discord/rtcconnection/RtcConnection$State;

.field public final synthetic this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;

    iput-object p2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->$connectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->call(Ljava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Long;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;

    iget-object v0, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-static {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getMediaSettingsStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreMediaSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings;->getVoiceConfig()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;

    iget-object v0, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-static {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getAudioDevicesStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreAudioDevices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices;->getAudioDevicesState()Lrx/Observable;

    move-result-object v2

    sget-object v0, Lcom/discord/widgets/voice/model/CallModel;->Companion:Lcom/discord/widgets/voice/model/CallModel$Companion;

    const-string v3, "selectedVoiceChannelId"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/discord/widgets/voice/model/CallModel$Companion;->get(J)Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1$1;

    invoke-direct {v4, p0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1$1;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;)V

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x12c

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
