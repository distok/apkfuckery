.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;
.super Lx/m/c/k;
.source "VoiceEngineServiceController.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    iput-object p2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;->$context:Landroid/content/Context;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-static {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getMediaSettingsStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreMediaSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfMuted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;->$context:Landroid/content/Context;

    const v1, 0x7f1219f8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0xc

    invoke-static {v0, v1, v2, v3, v4}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    :cond_0
    return-void
.end method
