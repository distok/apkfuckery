.class public final Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;
.super Ljava/lang/Object;
.source "VoiceChannelJoinabilityUtils.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->observeJoinability(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceStates;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $guildsStore:Lcom/discord/stores/StoreGuilds;

.field public final synthetic $permissionsStore:Lcom/discord/stores/StorePermissions;

.field public final synthetic $voiceStatesStore:Lcom/discord/stores/StoreVoiceStates;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceStates;JLcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuilds;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$voiceStatesStore:Lcom/discord/stores/StoreVoiceStates;

    iput-wide p2, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$channelId:J

    iput-object p4, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$permissionsStore:Lcom/discord/stores/StorePermissions;

    iput-object p5, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$guildsStore:Lcom/discord/stores/StoreGuilds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CHANNEL_DOES_NOT_EXIST:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$voiceStatesStore:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$channelId:J

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$permissionsStore:Lcom/discord/stores/StorePermissions;

    iget-wide v3, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$channelId:J

    invoke-virtual {v1, v3, v4}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->$guildsStore:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->getVerificationLevelTriggered(J)Lrx/Observable;

    move-result-object v2

    new-instance v4, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1$1;

    invoke-direct {v4, p1}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1$1;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-static {v0, v1, v3, v2, v4}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
