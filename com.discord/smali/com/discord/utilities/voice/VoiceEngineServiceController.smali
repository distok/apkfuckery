.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController;
.super Ljava/lang/Object;
.source "VoiceEngineServiceController.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;,
        Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;

.field private static final NOTIFICATION_DATA_DISCONNECTED:Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;


# instance fields
.field private final audioDevicesStore:Lcom/discord/stores/StoreAudioDevices;

.field private final mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

.field private final notificationDataObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
            ">;"
        }
    .end annotation
.end field

.field private serviceBinding:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

.field private final voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->Companion:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->INSTANCE$delegate:Lkotlin/Lazy;

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    new-instance v2, Lcom/discord/rtcconnection/RtcConnection$State$d;

    const/4 v1, 0x0

    invoke-direct {v2, v1}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v11}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;-><init>(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;Z)V

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->NOTIFICATION_DATA_DISCONNECTED:Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;)V
    .locals 1

    const-string v0, "audioDevicesStore"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaSettingsStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voiceChannelSelectedStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtcConnectionStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->audioDevicesStore:Lcom/discord/stores/StoreAudioDevices;

    iput-object p2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    iput-object p3, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {p4}, Lcom/discord/stores/StoreRtcConnection;->getConnectionState()Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;

    invoke-direct {p2, p0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;)V

    invoke-virtual {p1, p2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "rtcConnectionStore\n     \u2026            }\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->notificationDataObservable:Lrx/Observable;

    return-void
.end method

.method public static final synthetic access$getAudioDevicesStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreAudioDevices;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->audioDevicesStore:Lcom/discord/stores/StoreAudioDevices;

    return-object p0
.end method

.method public static final synthetic access$getINSTANCE$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->INSTANCE$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$getMediaSettingsStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreMediaSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    return-object p0
.end method

.method public static final synthetic access$getNOTIFICATION_DATA_DISCONNECTED$cp()Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;
    .locals 1

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->NOTIFICATION_DATA_DISCONNECTED:Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    return-object v0
.end method

.method public static final synthetic access$getServiceBinding$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->serviceBinding:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "serviceBinding"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getVoiceChannelSelectedStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreVoiceChannelSelected;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    return-object p0
.end method

.method public static final synthetic access$setServiceBinding$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->serviceBinding:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    return-void
.end method


# virtual methods
.method public final init(Landroid/content/Context;)V
    .locals 10

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    new-instance v1, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$1;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->setOnDisconnect(Lkotlin/jvm/functions/Function0;)V

    new-instance v1, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$2;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$2;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->setOnToggleSelfDeafen(Lkotlin/jvm/functions/Function0;)V

    new-instance v1, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;

    invoke-direct {v1, p0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$3;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->setOnToggleSelfMute(Lkotlin/jvm/functions/Function0;)V

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    invoke-direct {v0, p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->serviceBinding:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->notificationDataObservable:Lrx/Observable;

    const-string v0, "notificationDataObservable"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v2, Lcom/discord/utilities/voice/VoiceEngineServiceController;

    new-instance v7, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;

    invoke-direct {v7, p0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;Landroid/content/Context;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final startStream(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "permissionIntent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->serviceBinding:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->startStream(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string p1, "serviceBinding"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public final stopStream()V
    .locals 2

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->serviceBinding:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->stopStream(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V

    return-void

    :cond_0
    const-string v0, "serviceBinding"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
