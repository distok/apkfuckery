.class public final Lcom/discord/utilities/voice/ScreenShareManager$State;
.super Ljava/lang/Object;
.source "ScreenShareManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/ScreenShareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation


# instance fields
.field private final activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

.field private final meId:Ljava/lang/Long;

.field private final rtcConnection:Lcom/discord/rtcconnection/RtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    iput-object p2, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    iput-object p3, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/voice/ScreenShareManager$State;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;ILjava/lang/Object;)Lcom/discord/utilities/voice/ScreenShareManager$State;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/voice/ScreenShareManager$State;->copy(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)Lcom/discord/utilities/voice/ScreenShareManager$State;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-object v0
.end method

.method public final component2()Lcom/discord/rtcconnection/RtcConnection;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)Lcom/discord/utilities/voice/ScreenShareManager$State;
    .locals 1

    new-instance v0, Lcom/discord/utilities/voice/ScreenShareManager$State;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/utilities/voice/ScreenShareManager$State;-><init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/voice/ScreenShareManager$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/voice/ScreenShareManager$State;

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    iget-object v1, p1, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, p1, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-object v0
.end method

.method public final getMeId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getRtcConnection()Lcom/discord/rtcconnection/RtcConnection;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "State(activeStream="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rtcConnection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", meId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$State;->meId:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
