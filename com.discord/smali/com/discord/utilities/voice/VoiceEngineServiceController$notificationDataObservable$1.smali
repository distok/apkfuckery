.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;
.super Ljava/lang/Object;
.source "VoiceEngineServiceController.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController;-><init>(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/rtcconnection/RtcConnection$State;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;->call(Lcom/discord/rtcconnection/RtcConnection$State;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/rtcconnection/RtcConnection$State;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/RtcConnection$State;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$h;->a:Lcom/discord/rtcconnection/RtcConnection$State$h;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getNOTIFICATION_DATA_DISCONNECTED$cp()Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    move-result-object p1

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-static {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getVoiceChannelSelectedStore$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedVoiceChannelId()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;-><init>(Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1;Lcom/discord/rtcconnection/RtcConnection$State;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
