.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;
.super Ljava/lang/Object;
.source "VoiceEngineServiceController.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotificationData"
.end annotation


# instance fields
.field private final channelId:J

.field private final channelName:Ljava/lang/String;

.field private final guildId:Ljava/lang/Long;

.field private final isSelfDeafened:Z

.field private final isSelfMuted:Z

.field private final isSelfStreaming:Z

.field private final isVideo:Z

.field private final proximityLockEnabled:Z

.field private final rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

.field private final stateString:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;Z)V
    .locals 1

    const-string v0, "rtcConnectionState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelName"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    iput-object p2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    iput-boolean p4, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    iput-boolean p5, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    iput-boolean p6, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    iput-wide p7, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    iput-object p9, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    iput-boolean p10, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    instance-of p2, p1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-eqz p2, :cond_0

    const p1, 0x7f120519

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$b;->a:Lcom/discord/rtcconnection/RtcConnection$State$b;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    const p1, 0x7f120516

    goto :goto_0

    :cond_1
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$a;->a:Lcom/discord/rtcconnection/RtcConnection$State$a;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const p1, 0x7f120515

    goto :goto_0

    :cond_2
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$c;->a:Lcom/discord/rtcconnection/RtcConnection$State$c;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    const p1, 0x7f120518

    goto :goto_0

    :cond_3
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$h;->a:Lcom/discord/rtcconnection/RtcConnection$State$h;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const p1, 0x7f12051d

    goto :goto_0

    :cond_4
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$g;->a:Lcom/discord/rtcconnection/RtcConnection$State$g;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    const p1, 0x7f12051c

    goto :goto_0

    :cond_5
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_8

    if-eqz p5, :cond_6

    const p1, 0x7f12051f

    goto :goto_0

    :cond_6
    if-eqz p6, :cond_7

    const p1, 0x7f120520

    goto :goto_0

    :cond_7
    const p1, 0x7f120521

    goto :goto_0

    :cond_8
    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$e;->a:Lcom/discord/rtcconnection/RtcConnection$State$e;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    const p1, 0x7f12051b

    :goto_0
    iput p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->stateString:I

    return-void

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;ZILjava/lang/Object;)Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-wide v8, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    goto :goto_6

    :cond_6
    move-wide/from16 v8, p7

    :goto_6
    and-int/lit16 v10, v1, 0x80

    if-eqz v10, :cond_7

    iget-object v10, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    goto :goto_7

    :cond_7
    move-object/from16 v10, p9

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-boolean v1, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    goto :goto_8

    :cond_8
    move/from16 v1, p10

    :goto_8
    move-object p1, v2

    move-object p2, v3

    move p3, v4

    move p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move-wide/from16 p7, v8

    move-object/from16 p9, v10

    move/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->copy(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;Z)Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/rtcconnection/RtcConnection$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    return v0
.end method

.method public final component7()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    return-wide v0
.end method

.method public final component8()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    return v0
.end method

.method public final copy(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;Z)Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;
    .locals 12

    const-string v0, "rtcConnectionState"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelName"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    move-object v1, v0

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-wide/from16 v8, p7

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;-><init>(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    iget-object v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    iget-boolean v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    iget-boolean v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    iget-boolean v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    iget-boolean v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    iget-wide v2, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    iget-boolean p1, p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    return-wide v0
.end method

.method public final getChannelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getProximityLockEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    return v0
.end method

.method public final getRtcConnectionState()Lcom/discord/rtcconnection/RtcConnection$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    return-object v0
.end method

.method public final getStateString()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->stateString:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    invoke-static {v4, v5}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    if-eqz v1, :cond_7

    goto :goto_2

    :cond_7
    move v3, v1

    :goto_2
    add-int/2addr v0, v3

    return v0
.end method

.method public final isSelfDeafened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    return v0
.end method

.method public final isSelfMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    return v0
.end method

.method public final isSelfStreaming()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    return v0
.end method

.method public final isVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "NotificationData(rtcConnectionState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->rtcConnectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isSelfMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSelfDeafened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSelfStreaming="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isVideo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isVideo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->guildId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", proximityLockEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->proximityLockEnabled:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
