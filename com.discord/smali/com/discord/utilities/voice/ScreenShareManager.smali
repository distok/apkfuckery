.class public final Lcom/discord/utilities/voice/ScreenShareManager;
.super Ljava/lang/Object;
.source "ScreenShareManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/voice/ScreenShareManager$State;,
        Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;,
        Lcom/discord/utilities/voice/ScreenShareManager$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/voice/ScreenShareManager$Companion;

.field public static final JPEG_QUALITY:I = 0x5c

.field public static final PREVIEW_DELAY_MS:J = 0x1388L

.field public static final PREVIEW_INTERVAL_MS:J = 0x493e0L

.field public static final THUMBNAIL_HEIGHT_PX:I = 0x120

.field public static final THUMBNAIL_WIDTH_PX:I = 0x200


# instance fields
.field private final appComponent:Lcom/discord/app/AppComponent;

.field private final channelId:J

.field private final compositeSubscription:Lrx/subscriptions/CompositeSubscription;

.field private final guildId:Ljava/lang/Long;

.field private final imageEncoder:Lcom/discord/utilities/images/ImageEncoder;

.field private previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private screenshareIntent:Landroid/content/Intent;

.field private final storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

.field private final storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

.field private final storeStreamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final thumbnailBitmapSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/voice/ScreenShareManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/voice/ScreenShareManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/voice/ScreenShareManager;->Companion:Lcom/discord/utilities/voice/ScreenShareManager$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/app/AppComponent;JLjava/lang/Long;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/images/ImageEncoder;Lcom/discord/tooltips/TooltipManager;)V
    .locals 1

    const-string v0, "appComponent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeApplicationStreaming"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeRtcConnection"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStreamRtcConnection"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageEncoder"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipManager"

    invoke-static {p11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->appComponent:Lcom/discord/app/AppComponent;

    iput-wide p2, p0, Lcom/discord/utilities/voice/ScreenShareManager;->channelId:J

    iput-object p4, p0, Lcom/discord/utilities/voice/ScreenShareManager;->guildId:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object p6, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

    iput-object p7, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeStreamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    iput-object p8, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p9, p0, Lcom/discord/utilities/voice/ScreenShareManager;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object p10, p0, Lcom/discord/utilities/voice/ScreenShareManager;->imageEncoder:Lcom/discord/utilities/images/ImageEncoder;

    iput-object p11, p0, Lcom/discord/utilities/voice/ScreenShareManager;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p1

    const-string p2, "BehaviorSubject.create()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->thumbnailBitmapSubject:Lrx/subjects/BehaviorSubject;

    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p0}, Lcom/discord/utilities/voice/ScreenShareManager;->subscribeToStores()V

    return-void
.end method

.method public constructor <init>(Lcom/discord/app/AppComponent;JLjava/lang/Long;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/images/ImageEncoder;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 14

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v1

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object/from16 v7, p5

    :goto_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v1

    move-object v8, v1

    goto :goto_1

    :cond_1
    move-object/from16 v8, p6

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getStreamRtcConnection()Lcom/discord/stores/StoreStreamRtcConnection;

    move-result-object v1

    move-object v9, v1

    goto :goto_2

    :cond_2
    move-object/from16 v9, p7

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    move-object v10, v1

    goto :goto_3

    :cond_3
    move-object/from16 v10, p8

    :goto_3
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_4

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    move-object v11, v1

    goto :goto_4

    :cond_4
    move-object/from16 v11, p9

    :goto_4
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_5

    new-instance v1, Lcom/discord/utilities/images/ImageEncoder;

    invoke-direct {v1}, Lcom/discord/utilities/images/ImageEncoder;-><init>()V

    move-object v12, v1

    goto :goto_5

    :cond_5
    move-object/from16 v12, p10

    :goto_5
    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_a

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "logger"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_6

    :cond_6
    move-object v1, v2

    :goto_6
    if-nez v1, :cond_7

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v1, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_7
    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string v0, "floatingViewManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/tooltips/TooltipManager;

    :cond_8
    if-nez v2, :cond_9

    new-instance v0, Lcom/discord/tooltips/TooltipManager;

    sget-object v2, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {v2}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/a/l/a;

    sget-object v3, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {v3}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object/from16 p5, v0

    move-object/from16 p6, v2

    move-object/from16 p7, v3

    move/from16 p8, v4

    move-object/from16 p9, v1

    move/from16 p10, v5

    invoke-direct/range {p5 .. p10}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    move-object v2, v0

    :cond_9
    move-object v13, v2

    goto :goto_7

    :cond_a
    move-object/from16 v13, p11

    :goto_7
    move-object v2, p0

    move-object v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    invoke-direct/range {v2 .. v13}, Lcom/discord/utilities/voice/ScreenShareManager;-><init>(Lcom/discord/app/AppComponent;JLjava/lang/Long;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/images/ImageEncoder;Lcom/discord/tooltips/TooltipManager;)V

    return-void
.end method

.method public static final synthetic access$createThumbnailEmitter(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/voice/ScreenShareManager;->createThumbnailEmitter()Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCompositeSubscription$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method

.method public static final synthetic access$getImageEncoder$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/images/ImageEncoder;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->imageEncoder:Lcom/discord/utilities/images/ImageEncoder;

    return-object p0
.end method

.method public static final synthetic access$getPreviousState$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/voice/ScreenShareManager$State;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    return-object p0
.end method

.method public static final synthetic access$getRestAPI$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/rest/RestAPI;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object p0
.end method

.method public static final synthetic access$getScreenshareIntent$p(Lcom/discord/utilities/voice/ScreenShareManager;)Landroid/content/Intent;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->screenshareIntent:Landroid/content/Intent;

    return-object p0
.end method

.method public static final synthetic access$getThumbnailBitmapSubject$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->thumbnailBitmapSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$handleStartStream(Lcom/discord/utilities/voice/ScreenShareManager;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/voice/ScreenShareManager;->handleStartStream(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic access$handleStateUpdate(Lcom/discord/utilities/voice/ScreenShareManager;Lcom/discord/utilities/voice/ScreenShareManager$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/voice/ScreenShareManager;->handleStateUpdate(Lcom/discord/utilities/voice/ScreenShareManager$State;)V

    return-void
.end method

.method public static final synthetic access$setPreviousState$p(Lcom/discord/utilities/voice/ScreenShareManager;Lcom/discord/utilities/voice/ScreenShareManager$State;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    return-void
.end method

.method public static final synthetic access$setScreenshareIntent$p(Lcom/discord/utilities/voice/ScreenShareManager;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->screenshareIntent:Landroid/content/Intent;

    return-void
.end method

.method public static final synthetic access$uploadScreenSharePreviews(Lcom/discord/utilities/voice/ScreenShareManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/voice/ScreenShareManager;->uploadScreenSharePreviews()V

    return-void
.end method

.method private final createThumbnailEmitter()Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;
    .locals 11

    new-instance v10, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;

    new-instance v7, Lcom/discord/utilities/voice/ScreenShareManager$createThumbnailEmitter$1;

    invoke-direct {v7, p0}, Lcom/discord/utilities/voice/ScreenShareManager$createThumbnailEmitter$1;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    const/16 v1, 0x200

    const/16 v2, 0x120

    const-wide/32 v3, 0x493e0

    const-wide/16 v5, 0x1388

    const/4 v8, 0x0

    const/16 v9, 0x20

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;-><init>(IIJJLkotlin/jvm/functions/Function1;Lcom/discord/utilities/time/Clock;I)V

    return-object v10
.end method

.method private final handleStartStream(Landroid/content/Intent;)V
    .locals 7
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    iget-wide v1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->channelId:J

    iget-object v3, p0, Lcom/discord/utilities/voice/ScreenShareManager;->guildId:Ljava/lang/Long;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreApplicationStreaming;->createStream$default(Lcom/discord/stores/StoreApplicationStreaming;JLjava/lang/Long;Ljava/lang/String;ILjava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->screenshareIntent:Landroid/content/Intent;

    return-void
.end method

.method private final handleStateUpdate(Lcom/discord/utilities/voice/ScreenShareManager$State;)V
    .locals 6
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    iget-object v4, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    move-object v4, v1

    :goto_2
    invoke-static {v0, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getRtcConnection()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1, v1}, Lcom/discord/rtcconnection/RtcConnection;->m(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V

    :cond_4
    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_4

    :cond_5
    move-object v0, v1

    :goto_4
    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getMeId()Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    return-void

    :cond_6
    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getRtcConnection()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v1

    :cond_7
    if-nez v1, :cond_8

    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getRtcConnection()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v2, 0x1

    :cond_8
    if-eqz v2, :cond_9

    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getRtcConnection()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v0

    if-eqz v0, :cond_9

    new-instance v1, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->b(Lcom/discord/rtcconnection/RtcConnection$b;)V

    :cond_9
    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    return-void
.end method

.method private final subscribeToStores()V
    .locals 13

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getActiveStream()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeStreamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStreamRtcConnection;->observeRtcConnection()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v2}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/utilities/voice/ScreenShareManager$subscribeToStores$1;->INSTANCE:Lcom/discord/utilities/voice/ScreenShareManager$subscribeToStores$1;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->appComponent:Lcom/discord/app/AppComponent;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/utilities/voice/ScreenShareManager;

    new-instance v7, Lcom/discord/utilities/voice/ScreenShareManager$subscribeToStores$2;

    invoke-direct {v7, p0}, Lcom/discord/utilities/voice/ScreenShareManager$subscribeToStores$2;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    new-instance v10, Lcom/discord/utilities/voice/ScreenShareManager$subscribeToStores$3;

    invoke-direct {v10, p0}, Lcom/discord/utilities/voice/ScreenShareManager$subscribeToStores$3;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1a

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final uploadScreenSharePreviews()V
    .locals 13

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->thumbnailBitmapSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "thumbnailBitmapSubject\n \u2026>()\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->appComponent:Lcom/discord/app/AppComponent;

    invoke-static {v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/utilities/voice/ScreenShareManager;

    new-instance v7, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$2;

    invoke-direct {v7, p0}, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$2;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    sget-object v10, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$3;->INSTANCE:Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$3;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1a

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->channelId:J

    return-wide v0
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final release()V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    return-void
.end method

.method public final startStream(Landroid/content/Intent;)V
    .locals 13

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcConnection;->observeRtcConnectionMetadata()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "storeRtcConnection.obser\u2026       }\n        .take(1)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->appComponent:Lcom/discord/app/AppComponent;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/utilities/voice/ScreenShareManager;

    new-instance v7, Lcom/discord/utilities/voice/ScreenShareManager$startStream$2;

    invoke-direct {v7, p0}, Lcom/discord/utilities/voice/ScreenShareManager$startStream$2;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;)V

    new-instance v10, Lcom/discord/utilities/voice/ScreenShareManager$startStream$3;

    invoke-direct {v10, p0, p1}, Lcom/discord/utilities/voice/ScreenShareManager$startStream$3;-><init>(Lcom/discord/utilities/voice/ScreenShareManager;Landroid/content/Intent;)V

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1a

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final stopStream()V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->previousState:Lcom/discord/utilities/voice/ScreenShareManager$State;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreApplicationStreaming;->stopStream(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager;->screenshareIntent:Landroid/content/Intent;

    :cond_0
    return-void
.end method
