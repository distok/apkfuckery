.class public final Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$2;
.super Lx/m/c/k;
.source "CallSoundManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/CallSoundManager;->subscribeToStoreState(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/CallSoundManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/CallSoundManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$2;->this$0:Lcom/discord/utilities/voice/CallSoundManager;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$2;->this$0:Lcom/discord/utilities/voice/CallSoundManager;

    invoke-static {v0}, Lcom/discord/utilities/voice/CallSoundManager;->access$getAppSoundManager$p(Lcom/discord/utilities/voice/CallSoundManager;)Lcom/discord/utilities/media/AppSoundManager;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/media/AppSound;->Companion:Lcom/discord/utilities/media/AppSound$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/media/AppSound$Companion;->getSOUND_CALL_CALLING()Lcom/discord/utilities/media/AppSound;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/media/AppSoundManager;->stop(Lcom/discord/utilities/media/AppSound;)V

    return-void
.end method
