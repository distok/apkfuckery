.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;
.super Lx/m/c/k;
.source "VoiceEngineServiceController.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/utilities/voice/VoiceEngineServiceController;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;

    invoke-direct {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;-><init>()V

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/utilities/voice/VoiceEngineServiceController;
    .locals 5

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getAudioDevices()Lcom/discord/stores/StoreAudioDevices;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v1

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/discord/utilities/voice/VoiceEngineServiceController;-><init>(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion$INSTANCE$2;->invoke()Lcom/discord/utilities/voice/VoiceEngineServiceController;

    move-result-object v0

    return-object v0
.end method
