.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1$1;
.super Ljava/lang/Object;
.source "VoiceEngineServiceController.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->call(Ljava/lang/Long;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
        "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
        "Lcom/discord/widgets/voice/model/CallModel;",
        "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/widgets/voice/model/CallModel;)Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;
    .locals 19

    invoke-virtual/range {p2 .. p2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall()Z

    move-result v3

    if-ne v3, v2, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    :goto_0
    instance-of v0, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    if-nez v10, :cond_1

    if-eqz v0, :cond_1

    const/4 v14, 0x1

    goto :goto_1

    :cond_1
    const/4 v14, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;

    iget-object v5, v3, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1;->$connectionState:Lcom/discord/rtcconnection/RtcConnection$State;

    const-string v3, "connectionState"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    if-eqz p3, :cond_2

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    move-object v4, v3

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    const-string v4, ""

    :goto_3
    move-object v6, v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted()Z

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened()Z

    move-result v8

    if-eqz p3, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v4

    if-ne v4, v2, :cond_4

    const/4 v9, 0x1

    goto :goto_4

    :cond_4
    const/4 v9, 0x0

    :goto_4
    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v11

    goto :goto_5

    :cond_5
    const-wide/16 v11, -0x1

    :goto_5
    if-eqz p3, :cond_6

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v4

    goto :goto_6

    :cond_6
    move-object v4, v3

    :goto_6
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    const-wide/16 v17, 0x0

    cmp-long v13, v15, v17

    if-lez v13, :cond_7

    const/4 v1, 0x1

    :cond_7
    if-eqz v1, :cond_8

    move-object v13, v4

    goto :goto_7

    :cond_8
    move-object v13, v3

    :goto_7
    new-instance v1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    move-object v4, v1

    invoke-direct/range {v4 .. v14}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;-><init>(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;Z)V

    return-object v1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    check-cast p2, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    check-cast p3, Lcom/discord/widgets/voice/model/CallModel;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/voice/VoiceEngineServiceController$notificationDataObservable$1$1$1;->call(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/widgets/voice/model/CallModel;)Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    move-result-object p1

    return-object p1
.end method
