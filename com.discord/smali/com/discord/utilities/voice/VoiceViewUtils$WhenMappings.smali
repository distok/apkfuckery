.class public final synthetic Lcom/discord/utilities/voice/VoiceViewUtils$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/discord/rtcconnection/RtcConnection$Quality;->values()[Lcom/discord/rtcconnection/RtcConnection$Quality;

    const/4 v0, 0x4

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/utilities/voice/VoiceViewUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/rtcconnection/RtcConnection$Quality;->BAD:Lcom/discord/rtcconnection/RtcConnection$Quality;

    const/4 v2, 0x1

    aput v2, v1, v2

    sget-object v3, Lcom/discord/rtcconnection/RtcConnection$Quality;->AVERAGE:Lcom/discord/rtcconnection/RtcConnection$Quality;

    const/4 v3, 0x2

    aput v3, v1, v3

    sget-object v4, Lcom/discord/rtcconnection/RtcConnection$Quality;->FINE:Lcom/discord/rtcconnection/RtcConnection$Quality;

    const/4 v4, 0x3

    aput v4, v1, v4

    sget-object v5, Lcom/discord/rtcconnection/RtcConnection$Quality;->UNKNOWN:Lcom/discord/rtcconnection/RtcConnection$Quality;

    const/4 v5, 0x0

    aput v0, v1, v5

    invoke-static {}, Lcom/discord/rtcconnection/RtcConnection$Quality;->values()[Lcom/discord/rtcconnection/RtcConnection$Quality;

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/utilities/voice/VoiceViewUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    aput v2, v1, v2

    aput v3, v1, v3

    aput v4, v1, v4

    aput v0, v1, v5

    return-void
.end method
