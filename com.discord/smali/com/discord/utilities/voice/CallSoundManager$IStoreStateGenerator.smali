.class public interface abstract Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;
.super Ljava/lang/Object;
.source "CallSoundManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/CallSoundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IStoreStateGenerator"
.end annotation


# virtual methods
.method public abstract observeStoreState(J)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/voice/CallSoundManager$StoreState;",
            ">;"
        }
    .end annotation
.end method
