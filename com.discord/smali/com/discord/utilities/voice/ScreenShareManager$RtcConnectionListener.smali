.class public final Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;
.super Ljava/lang/Object;
.source "ScreenShareManager.kt"

# interfaces
.implements Lcom/discord/rtcconnection/RtcConnection$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/ScreenShareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "RtcConnectionListener"
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/ScreenShareManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/ScreenShareManager;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnalyticsEvent(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "properties"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onFatalClose()V
    .locals 0

    return-void
.end method

.method public onMediaSessionIdReceived()V
    .locals 0

    return-void
.end method

.method public onQualityUpdate(Lcom/discord/rtcconnection/RtcConnection$Quality;)V
    .locals 1

    const-string v0, "quality"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onSpeaking(JZ)V
    .locals 0

    return-void
.end method

.method public onStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 2

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {p1}, Lcom/discord/utilities/voice/ScreenShareManager;->access$getPreviousState$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/voice/ScreenShareManager$State;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getRtcConnection()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->access$getScreenshareIntent$p(Lcom/discord/utilities/voice/ScreenShareManager;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {v1}, Lcom/discord/utilities/voice/ScreenShareManager;->access$createThumbnailEmitter(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->m(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V

    iget-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {p1}, Lcom/discord/utilities/voice/ScreenShareManager;->access$uploadScreenSharePreviews(Lcom/discord/utilities/voice/ScreenShareManager;)V

    goto :goto_1

    :cond_1
    instance-of p1, p1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-virtual {p1}, Lcom/discord/utilities/voice/ScreenShareManager;->stopStream()V

    :cond_2
    :goto_1
    return-void
.end method

.method public onVideoStream(JLjava/lang/Integer;)V
    .locals 0

    return-void
.end method
