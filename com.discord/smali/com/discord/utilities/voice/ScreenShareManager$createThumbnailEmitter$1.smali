.class public final Lcom/discord/utilities/voice/ScreenShareManager$createThumbnailEmitter$1;
.super Lx/m/c/k;
.source "ScreenShareManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/ScreenShareManager;->createThumbnailEmitter()Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/graphics/Bitmap;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/ScreenShareManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/ScreenShareManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$createThumbnailEmitter$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/ScreenShareManager$createThumbnailEmitter$1;->invoke(Landroid/graphics/Bitmap;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/graphics/Bitmap;)V
    .locals 1

    const-string/jumbo v0, "thumbnailBitmap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$createThumbnailEmitter$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->access$getThumbnailBitmapSubject$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
