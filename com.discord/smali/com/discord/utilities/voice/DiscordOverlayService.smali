.class public final Lcom/discord/utilities/voice/DiscordOverlayService;
.super Lcom/discord/overlay/OverlayService;
.source "DiscordOverlayService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/voice/DiscordOverlayService$Companion;
    }
.end annotation


# static fields
.field private static final ACTION_CLOSE:Ljava/lang/String; = "com.discord.actions.OVERLAY_CLOSE"

.field private static final ACTION_OPEN:Ljava/lang/String; = "com.discord.actions.OVERLAY_OPEN"

.field private static final ACTION_SELECTOR:Ljava/lang/String; = "com.discord.actions.OVERLAY_SELECTOR"

.field private static final ACTION_VOICE:Ljava/lang/String; = "com.discord.actions.OVERLAY_VOICE"

.field private static final CLOSE_INTENT_REQ_CODE:I = 0x3f2

.field public static final Companion:Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

.field private static final LOG_TAG:Ljava/lang/String; = "OverlayService"


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/voice/DiscordOverlayService$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/voice/DiscordOverlayService;->Companion:Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/overlay/OverlayService;-><init>()V

    return-void
.end method

.method public static final synthetic access$createMenu(Lcom/discord/utilities/voice/DiscordOverlayService;)Lcom/discord/overlay/views/OverlayDialog;
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/voice/DiscordOverlayService;->createMenu()Lcom/discord/overlay/views/OverlayDialog;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getOverlayManager$p(Lcom/discord/utilities/voice/DiscordOverlayService;)Lcom/discord/overlay/OverlayManager;
    .locals 0

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setOverlayManager$p(Lcom/discord/utilities/voice/DiscordOverlayService;Lcom/discord/overlay/OverlayManager;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/overlay/OverlayService;->setOverlayManager(Lcom/discord/overlay/OverlayManager;)V

    return-void
.end method

.method private final createMenu()Lcom/discord/overlay/views/OverlayDialog;
    .locals 4

    new-instance v0, Lf/a/n/m;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "applicationContext"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lf/a/n/m;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getInsetMargins()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070210

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    new-instance v1, Lcom/discord/utilities/voice/DiscordOverlayService$createMenu$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/DiscordOverlayService$createMenu$1;-><init>(Lcom/discord/utilities/voice/DiscordOverlayService;)V

    invoke-virtual {v0, v1}, Lf/a/n/j;->setOnDialogClosed(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method private final createVoiceBubble()Lcom/discord/overlay/views/OverlayBubbleWrap;
    .locals 5

    invoke-virtual {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    new-instance v1, Lf/a/n/w;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "applicationContext"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lf/a/n/w;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getInsetMargins()Landroid/graphics/Rect;

    move-result-object v2

    neg-int v3, v0

    invoke-virtual {v2, v3, v0, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object v0

    new-instance v2, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;

    const-string v3, "Active Voice Bubble"

    invoke-direct {v2, p0, v3}, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;-><init>(Lcom/discord/utilities/voice/DiscordOverlayService;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v4, "srcBubble"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "anchorViewTag"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "menuBubbleProvider"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lf/a/e/c;

    invoke-direct {v4, v0, v2, v1, v3}, Lf/a/e/c;-><init>(Lcom/discord/overlay/OverlayManager;Lkotlin/jvm/functions/Function1;Lcom/discord/overlay/views/OverlayBubbleWrap;Ljava/lang/Object;)V

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "bubble"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/overlay/OverlayManager;->k:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v2, Lf/a/e/b;

    invoke-direct {v2, v0, v1}, Lf/a/e/b;-><init>(Lcom/discord/overlay/OverlayManager;Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    invoke-virtual {v1, v2}, Lcom/discord/overlay/views/OverlayBubbleWrap;->setOnMovingStateChanged(Lkotlin/jvm/functions/Function1;)V

    return-object v1
.end method

.method private final createVoiceSelector()Lcom/discord/overlay/views/OverlayBubbleWrap;
    .locals 3

    new-instance v0, Lf/a/n/z;

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "applicationContext"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lf/a/n/z;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceSelector$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceSelector$1;-><init>(Lcom/discord/utilities/voice/DiscordOverlayService;)V

    invoke-virtual {v0, v1}, Lf/a/n/j;->setOnDialogClosed(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method


# virtual methods
.method public createNotification(Landroid/content/Intent;)Landroid/app/Notification;
    .locals 5

    sget-object p1, Lcom/discord/utilities/voice/DiscordOverlayService;->Companion:Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

    const-string v0, "com.discord.actions.OVERLAY_CLOSE"

    invoke-static {p1, p0, v0}, Lcom/discord/utilities/voice/DiscordOverlayService$Companion;->access$createOverlayIntent(Lcom/discord/utilities/voice/DiscordOverlayService$Companion;Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const/16 v0, 0x3f2

    const/high16 v1, 0x8000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;

    invoke-virtual {v3}, Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;->getSelectSettingsVoice()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0, v0, v2, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroidx/core/app/NotificationCompat$Builder;

    const-string v2, "Media Connections"

    invoke-direct {v1, p0, v2}, Landroidx/core/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "service"

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setLocalOnly(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const v3, 0x7f0803ab

    invoke-virtual {v1, v3}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const v3, 0x7f060200

    invoke-static {p0, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroidx/core/app/NotificationCompat$Builder;->setColor(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const v3, 0x7f121210

    invoke-virtual {p0, v3}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const v3, 0x7f121222

    invoke-virtual {p0, v3}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setOngoing(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x7f120498

    invoke-virtual {p0, v2}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0802bd

    invoke-virtual {v1, v3, v2, p1}, Landroidx/core/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    const v1, 0x7f121678

    invoke-virtual {p0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08041d

    invoke-virtual {p1, v2, v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    const-string v0, "NotificationCompat.Build\u2026       )\n        .build()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public createOverlayBubble(Landroid/content/Intent;)Lcom/discord/overlay/views/OverlayBubbleWrap;
    .locals 3

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x2cf065dd

    if-eq v1, v2, :cond_6

    const v2, -0x1a3c78e7

    if-eq v1, v2, :cond_2

    const v2, 0x213b4d2e

    if-eq v1, v2, :cond_1

    goto/16 :goto_2

    :cond_1
    const-string v1, "com.discord.actions.OVERLAY_SELECTOR"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-direct {p0}, Lcom/discord/utilities/voice/DiscordOverlayService;->createVoiceSelector()Lcom/discord/overlay/views/OverlayBubbleWrap;

    move-result-object v0

    goto :goto_2

    :cond_2
    const-string v1, "com.discord.actions.OVERLAY_OPEN"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object p1

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/overlay/views/OverlayBubbleWrap;

    instance-of v2, v2, Lf/a/n/w;

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_4
    move-object v1, v0

    :goto_0
    check-cast v1, Lcom/discord/overlay/views/OverlayBubbleWrap;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->performClick()Z

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/discord/utilities/voice/DiscordOverlayService;->createVoiceBubble()Lcom/discord/overlay/views/OverlayBubbleWrap;

    move-result-object v0

    goto :goto_2

    :cond_6
    const-string v1, "com.discord.actions.OVERLAY_VOICE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object p1

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/overlay/views/OverlayBubbleWrap;

    instance-of v2, v2, Lf/a/n/w;

    if-eqz v2, :cond_7

    goto :goto_1

    :cond_8
    move-object v1, v0

    :goto_1
    check-cast v1, Lcom/discord/overlay/views/OverlayBubbleWrap;

    if-eqz v1, :cond_9

    goto :goto_2

    :cond_9
    invoke-direct {p0}, Lcom/discord/utilities/voice/DiscordOverlayService;->createVoiceBubble()Lcom/discord/overlay/views/OverlayBubbleWrap;

    move-result-object v0

    :cond_a
    :goto_2
    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Lcom/discord/overlay/OverlayService;->onCreate()V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p0}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-string v2, "application"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream$Companion;->initialize(Landroid/app/Application;)V

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/voice/DiscordOverlayService$onCreate$1;->INSTANCE:Lcom/discord/utilities/voice/DiscordOverlayService$onCreate$1;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "<set-?>"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/discord/overlay/OverlayManager;->g:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0}, Lcom/discord/overlay/OverlayService;->getOverlayManager()Lcom/discord/overlay/OverlayManager;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/voice/DiscordOverlayService$onCreate$2;

    invoke-direct {v1, p0}, Lcom/discord/utilities/voice/DiscordOverlayService$onCreate$2;-><init>(Lcom/discord/utilities/voice/DiscordOverlayService;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/discord/overlay/OverlayManager;->h:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v0, "onStartCommand: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v7

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v2, "OverlayService"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->v$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    :cond_1
    const-string v0, "com.discord.actions.OVERLAY_CLOSE"

    invoke-static {v7, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v7, 0x2

    if-nez v0, :cond_3

    sget-object v0, Lcom/discord/utilities/device/DeviceUtils;->INSTANCE:Lcom/discord/utilities/device/DeviceUtils;

    invoke-virtual {v0, p0}, Lcom/discord/utilities/device/DeviceUtils;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getMobileOverlay()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lcom/discord/overlay/OverlayService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v3, v0

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v8, 0x0

    const-string v1, "OverlayService"

    const-string v2, "Overlay failed to handle a request."

    move-object v6, v8

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    sget-object v1, Lcom/discord/utilities/voice/DiscordOverlayService;->Companion:Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const-string v3, "com.discord.actions.OVERLAY_CLOSE"

    move-object v2, p0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/voice/DiscordOverlayService$Companion;->tryStartOverlayService$default(Lcom/discord/utilities/voice/DiscordOverlayService$Companion;Landroid/content/Context;Ljava/lang/String;ZILjava/lang/Object;)V

    :goto_1
    return v7

    :cond_3
    :goto_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    invoke-virtual {p0, p3}, Landroid/app/Service;->stopSelf(I)V

    return v7
.end method
