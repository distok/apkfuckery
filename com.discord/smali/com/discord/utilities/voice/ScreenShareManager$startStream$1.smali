.class public final Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;
.super Ljava/lang/Object;
.source "ScreenShareManager.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/ScreenShareManager;->startStream(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/ScreenShareManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/ScreenShareManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;)Ljava/lang/Boolean;
    .locals 5

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getChannelId()Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-virtual {v1}, Lcom/discord/utilities/voice/ScreenShareManager;->getChannelId()J

    move-result-wide v1

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getGuildId()Ljava/lang/Long;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x0

    :goto_2
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/ScreenShareManager$startStream$1;->call(Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
