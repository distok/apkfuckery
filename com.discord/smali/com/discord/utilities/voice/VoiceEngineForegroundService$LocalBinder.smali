.class public final Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;
.super Landroid/os/Binder;
.source "VoiceEngineForegroundService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/VoiceEngineForegroundService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocalBinder"
.end annotation


# instance fields
.field private final service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineForegroundService;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;->service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    return-void
.end method


# virtual methods
.method public final getService()Lcom/discord/utilities/voice/VoiceEngineForegroundService;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;->service:Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    return-object v0
.end method
