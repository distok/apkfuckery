.class public final Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;
.super Ljava/lang/Object;
.source "ScreenShareManager.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/ScreenShareManager;->uploadScreenSharePreviews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Landroid/graphics/Bitmap;",
        "Lrx/Observable<",
        "+",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/ScreenShareManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/ScreenShareManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;->call(Landroid/graphics/Bitmap;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Landroid/graphics/Bitmap;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->access$getPreviousState$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/voice/ScreenShareManager$State;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager$State;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {v1}, Lcom/discord/utilities/voice/ScreenShareManager;->access$getImageEncoder$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/images/ImageEncoder;

    move-result-object v1

    const-string/jumbo v2, "thumbnailBitmap"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x5c

    invoke-virtual {v1, p1, v2}, Lcom/discord/utilities/images/ImageEncoder;->encodeBitmapAsJpegDataUrl(Landroid/graphics/Bitmap;I)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/utilities/voice/ScreenShareManager$uploadScreenSharePreviews$1;->this$0:Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-static {v1}, Lcom/discord/utilities/voice/ScreenShareManager;->access$getRestAPI$p(Lcom/discord/utilities/voice/ScreenShareManager;)Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Lcom/discord/utilities/rest/RestAPI;->postStreamPreview(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    goto :goto_1

    :cond_1
    sget-object p1, Lg0/l/a/f;->e:Lrx/Observable;

    :goto_1
    return-object p1
.end method
