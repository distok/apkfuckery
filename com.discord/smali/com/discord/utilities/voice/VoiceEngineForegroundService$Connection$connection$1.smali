.class public final Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;
.super Ljava/lang/Object;
.source "VoiceEngineForegroundService.kt"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    const-string v0, "className"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "binder"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;

    iget-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    invoke-virtual {p2}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;->getService()Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->access$setService$p(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Lcom/discord/utilities/voice/VoiceEngineForegroundService;)V

    iget-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->setUnbinding(Z)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string v0, "className"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->setUnbinding(Z)V

    iget-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection$connection$1;->this$0:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;->access$setService$p(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Lcom/discord/utilities/voice/VoiceEngineForegroundService;)V

    return-void
.end method
