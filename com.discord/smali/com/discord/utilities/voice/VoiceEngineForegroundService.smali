.class public final Lcom/discord/utilities/voice/VoiceEngineForegroundService;
.super Landroid/app/IntentService;
.source "VoiceEngineForegroundService.kt"

# interfaces
.implements Lcom/discord/app/AppComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;,
        Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;,
        Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

.field private static final LOG_TAG:Ljava/lang/String; = "DiscordVoiceService"

.field private static final NOTIFICATION_ID:I = 0x65

.field private static final WAKELOCK_TIMEOUT:J = 0x6ddd00L

.field private static onDisconnect:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private static onToggleSelfDeafen:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private static onToggleSelfMute:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final binder:Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;

.field private final ringManager:Lcom/discord/utilities/voice/CallSoundManager;

.field private screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

.field private final unsubscribeSignal:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private wakeLockPartial:Landroid/os/PowerManager$WakeLock;

.field private wakeLockProximity:Landroid/os/PowerManager$WakeLock;

.field private wakeLockWifi:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$onDisconnect$1;->INSTANCE:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$onDisconnect$1;

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onDisconnect:Lkotlin/jvm/functions/Function0;

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$onToggleSelfDeafen$1;->INSTANCE:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$onToggleSelfDeafen$1;

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfDeafen:Lkotlin/jvm/functions/Function0;

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$onToggleSelfMute$1;->INSTANCE:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$onToggleSelfMute$1;

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfMute:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    const-string v0, "VoiceEngineForegroundService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    const-string v1, "PublishSubject.create()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->unsubscribeSignal:Lrx/subjects/Subject;

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;-><init>(Lcom/discord/utilities/voice/VoiceEngineForegroundService;)V

    iput-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->binder:Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;

    new-instance v0, Lcom/discord/utilities/voice/CallSoundManager;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/voice/CallSoundManager;-><init>(Lcom/discord/app/AppComponent;Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->ringManager:Lcom/discord/utilities/voice/CallSoundManager;

    return-void
.end method

.method public static final synthetic access$getOnDisconnect$cp()Lkotlin/jvm/functions/Function0;
    .locals 1

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onDisconnect:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public static final synthetic access$getOnToggleSelfDeafen$cp()Lkotlin/jvm/functions/Function0;
    .locals 1

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfDeafen:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public static final synthetic access$getOnToggleSelfMute$cp()Lkotlin/jvm/functions/Function0;
    .locals 1

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfMute:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public static final synthetic access$setOnDisconnect$cp(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onDisconnect:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$setOnToggleSelfDeafen$cp(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfDeafen:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$setOnToggleSelfMute$cp(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    sput-object p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfMute:Lkotlin/jvm/functions/Function0;

    return-void
.end method


# virtual methods
.method public getUnsubscribeSignal()Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->unsubscribeSignal:Lrx/subjects/Subject;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->binder:Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;

    return-object p1
.end method

.method public onCreate()V
    .locals 7

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "DiscordVoiceService"

    const-string v2, "Service created."

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->v$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v0, Lf/a/b/o;->c:Lf/a/b/o;

    invoke-virtual {v0, p0}, Lf/a/b/o;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/app/IntentService;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-string v0, "application"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/system/SystemServiceExtensionsKt;->createWifiLock$default(Landroid/content/Context;ZILjava/lang/String;ILjava/lang/Object;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockWifi:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/IntentService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4, v3}, Lcom/discord/utilities/system/SystemServiceExtensionsKt;->createPartialWakeLock$default(Landroid/content/Context;ZLjava/lang/String;ILjava/lang/Object;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockPartial:Landroid/os/PowerManager$WakeLock;

    const-wide/32 v5, 0x6ddd00

    if-eqz v1, :cond_1

    invoke-virtual {v1, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    :cond_1
    invoke-virtual {p0}, Landroid/app/IntentService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2, v3, v4, v3}, Lcom/discord/utilities/system/SystemServiceExtensionsKt;->createProximityScreenWakeLock$default(Landroid/content/Context;ZLjava/lang/String;ILjava/lang/Object;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockProximity:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v5, v6}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 6

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "DiscordVoiceService"

    const-string v2, "Service destroyed."

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->v$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v0, Lf/a/b/o;->c:Lf/a/b/o;

    invoke-virtual {v0, p0}, Lf/a/b/o;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockWifi:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockPartial:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    iget-object v0, p0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockProximity:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_2
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 19

    move-object/from16 v15, p0

    move-object/from16 v14, p1

    if-eqz v14, :cond_a

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v1, "intent?.action ?: return"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "Received action: "

    invoke-static {v1, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v3, "DiscordVoiceService"

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->v$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    move-object v4, v15

    goto/16 :goto_5

    :sswitch_0
    const-string v1, "com.discord.utilities.voice.action.toggle_muted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfMute:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    :sswitch_1
    const-string v1, "com.discord.utilities.voice.action.start_foreground"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.discord.utilities.voice.extra.proximity_lock_enabled"

    const/4 v1, 0x0

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockProximity:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    const-wide/32 v3, 0x6ddd00

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    goto :goto_1

    :cond_1
    iget-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->wakeLockProximity:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_2
    :goto_1
    const-string v0, "com.discord.utilities.voice.extra.guild_id"

    invoke-virtual {v14, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/Long;

    const-string v0, "com.discord.utilities.voice.extra.channel_id"

    invoke-virtual {v14, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Ljava/lang/Long;

    if-eqz v13, :cond_7

    iget-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->ringManager:Lcom/discord/utilities/voice/CallSoundManager;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/discord/utilities/voice/CallSoundManager;->subscribeToStoreState(J)V

    iget-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->getChannelId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    :goto_2
    invoke-static {v0, v13}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-nez v0, :cond_5

    iget-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    :cond_4
    invoke-static {v1, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_7

    :cond_5
    iget-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->release()V

    :cond_6
    new-instance v12, Lcom/discord/utilities/voice/ScreenShareManager;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v16, 0x3f8

    const/16 v17, 0x0

    move-object v0, v12

    move-object/from16 v1, p0

    move-object/from16 v18, v12

    move/from16 v12, v16

    move-object/from16 v16, v13

    move-object/from16 v13, v17

    invoke-direct/range {v0 .. v13}, Lcom/discord/utilities/voice/ScreenShareManager;-><init>(Lcom/discord/app/AppComponent;JLjava/lang/Long;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/images/ImageEncoder;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

    goto :goto_3

    :cond_7
    move-object/from16 v16, v13

    :goto_3
    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineNotificationBuilder;->INSTANCE:Lcom/discord/utilities/voice/VoiceEngineNotificationBuilder;

    const-class v7, Lcom/discord/utilities/voice/VoiceEngineForegroundService;

    if-eqz v16, :cond_8

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_4

    :cond_8
    const-wide/16 v1, 0x0

    :goto_4
    move-wide v9, v1

    const-string v1, "com.discord.utilities.voice.extra.title"

    invoke-virtual {v14, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "com.discord.utilities.voice.extra.title_subtext"

    invoke-virtual {v14, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-class v13, Lcom/discord/app/AppActivity$Main;

    const-string v1, "com.discord.utilities.voice.extra.item_streaming"

    const/4 v2, 0x0

    invoke-virtual {v14, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    move-object v3, v14

    move v14, v1

    const-string v1, "com.discord.utilities.voice.extra.item_muted"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    move-object v8, v15

    move v15, v1

    const-string v1, "com.discord.utilities.voice.extra.item_deafened"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    const-string v2, "com.discord.utilities.voice.action.main"

    const-string v3, "com.discord.utilities.voice.action.disconnect"

    const-string v4, "com.discord.utilities.voice.action.stop_stream"

    const-string v5, "com.discord.utilities.voice.action.toggle_muted"

    const-string v6, "com.discord.utilities.voice.action.toggle_deafened"

    const-string v1, "Media Connections"

    move-object v8, v1

    move-object/from16 v1, p0

    invoke-virtual/range {v0 .. v16}, Lcom/discord/utilities/voice/VoiceEngineNotificationBuilder;->buildNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Class;ZZZ)Landroid/app/Notification;

    move-result-object v0

    const/16 v1, 0x65

    move-object/from16 v4, p0

    invoke-virtual {v4, v1, v0}, Landroid/app/IntentService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_5

    :sswitch_2
    move-object v3, v14

    move-object v4, v15

    const-string v1, "com.discord.utilities.voice.action.start_stream"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "android.intent.extra.INTENT"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_9

    iget-object v1, v4, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

    if-eqz v1, :cond_9

    invoke-virtual {v1, v0}, Lcom/discord/utilities/voice/ScreenShareManager;->startStream(Landroid/content/Intent;)V

    goto :goto_5

    :sswitch_3
    move-object v4, v15

    const-string v1, "com.discord.utilities.voice.action.disconnect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onDisconnect:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    invoke-virtual {v4, v2}, Landroid/app/IntentService;->stopForeground(Z)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/IntentService;->stopSelf()V

    goto :goto_5

    :sswitch_4
    move-object v4, v15

    const-string v1, "com.discord.utilities.voice.action.stop_stream"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v4, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->screenShareManager:Lcom/discord/utilities/voice/ScreenShareManager;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/discord/utilities/voice/ScreenShareManager;->stopStream()V

    goto :goto_5

    :sswitch_5
    move-object v4, v15

    const-string v1, "com.discord.utilities.voice.action.stop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v4, v2}, Landroid/app/IntentService;->stopForeground(Z)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/IntentService;->stopSelf()V

    goto :goto_5

    :sswitch_6
    move-object v4, v15

    const-string v1, "com.discord.utilities.voice.action.toggle_deafened"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->onToggleSelfDeafen:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_9
    :goto_5
    return-void

    :cond_a
    move-object v4, v15

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x78b14b10 -> :sswitch_6
        -0x593bf795 -> :sswitch_5
        -0x2a79fa2c -> :sswitch_4
        -0x29e659bb -> :sswitch_3
        0x9a8bd66 -> :sswitch_2
        0x20467429 -> :sswitch_1
        0x5f84c829 -> :sswitch_0
    .end sparse-switch
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    invoke-super {p0, p1}, Landroid/app/IntentService;->onUnbind(Landroid/content/Intent;)Z

    move-result p1

    return p1
.end method
