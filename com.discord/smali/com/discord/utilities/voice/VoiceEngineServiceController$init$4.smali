.class public final Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;
.super Lx/m/c/k;
.source "VoiceEngineServiceController.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceEngineServiceController;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/VoiceEngineServiceController;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    iput-object p2, p0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->$context:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->invoke(Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;)V
    .locals 17

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getRtcConnectionState()Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v1

    instance-of v1, v1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/utilities/voice/DiscordOverlayService;->Companion:Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

    iget-object v2, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/discord/utilities/voice/DiscordOverlayService$Companion;->launchForClose(Landroid/content/Context;)V

    sget-object v1, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    iget-object v2, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-static {v2}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getServiceBinding$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->stopForegroundAndUnbind(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->$context:Landroid/content/Context;

    const v2, 0x7f1203c6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getStateString()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v1, "context.getString(\n     \u2026teString)\n              )"

    invoke-static {v8, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/discord/utilities/voice/VoiceEngineForegroundService;->Companion:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;

    iget-object v1, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->this$0:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-static {v1}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->access$getServiceBinding$p(Lcom/discord/utilities/voice/VoiceEngineServiceController;)Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getChannelName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfMuted()Z

    move-result v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfDeafened()Z

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->isSelfStreaming()Z

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getChannelId()J

    move-result-wide v13

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getGuildId()Ljava/lang/Long;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getProximityLockEnabled()Z

    move-result v16

    invoke-virtual/range {v6 .. v16}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;->startForegroundAndBind(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Ljava/lang/String;Ljava/lang/String;ZZZJLjava/lang/Long;Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;->getRtcConnectionState()Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v1

    sget-object v2, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/utilities/voice/DiscordOverlayService;->Companion:Lcom/discord/utilities/voice/DiscordOverlayService$Companion;

    iget-object v2, v0, Lcom/discord/utilities/voice/VoiceEngineServiceController$init$4;->$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/discord/utilities/voice/DiscordOverlayService$Companion;->launchForVoice(Landroid/content/Context;)V

    :cond_1
    :goto_0
    return-void
.end method
