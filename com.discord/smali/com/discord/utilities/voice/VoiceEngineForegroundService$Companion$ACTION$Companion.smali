.class public final Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;
.super Ljava/lang/Object;
.source "VoiceEngineForegroundService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# static fields
.field public static final synthetic $$INSTANCE:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;

.field public static final DISCONNECT:Ljava/lang/String; = "com.discord.utilities.voice.action.disconnect"

.field public static final MAIN_ACTION:Ljava/lang/String; = "com.discord.utilities.voice.action.main"

.field public static final START_FOREGROUND:Ljava/lang/String; = "com.discord.utilities.voice.action.start_foreground"

.field public static final START_STREAM:Ljava/lang/String; = "com.discord.utilities.voice.action.start_stream"

.field public static final STOP_SERVICE:Ljava/lang/String; = "com.discord.utilities.voice.action.stop"

.field public static final STOP_STREAM:Ljava/lang/String; = "com.discord.utilities.voice.action.stop_stream"

.field public static final TOGGLE_DEAFENED:Ljava/lang/String; = "com.discord.utilities.voice.action.toggle_deafened"

.field public static final TOGGLE_MUTED:Ljava/lang/String; = "com.discord.utilities.voice.action.toggle_muted"


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;

    invoke-direct {v0}, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;-><init>()V

    sput-object v0, Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;->$$INSTANCE:Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
