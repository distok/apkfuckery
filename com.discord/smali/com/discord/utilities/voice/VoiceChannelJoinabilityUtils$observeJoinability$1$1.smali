.class public final Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1$1;
.super Ljava/lang/Object;
.source "VoiceChannelJoinabilityUtils.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelVoice$State;",
        ">;",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Ljava/lang/Integer;",
        "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Integer;)Lcom/discord/utilities/voice/VoiceChannelJoinability;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->INSTANCE:Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;

    iget-object v1, p0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuild;->getMaxVideoChannelUsers()Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v4, p1

    const-string/jumbo p1, "verificationLevelTriggered"

    invoke-static {p4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->getJoinability(Lcom/discord/models/domain/ModelChannel;Ljava/util/Collection;Ljava/lang/Long;Ljava/lang/Integer;I)Lcom/discord/utilities/voice/VoiceChannelJoinability;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    check-cast p2, Ljava/lang/Long;

    check-cast p3, Lcom/discord/models/domain/ModelGuild;

    check-cast p4, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils$observeJoinability$1$1;->call(Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Integer;)Lcom/discord/utilities/voice/VoiceChannelJoinability;

    move-result-object p1

    return-object p1
.end method
