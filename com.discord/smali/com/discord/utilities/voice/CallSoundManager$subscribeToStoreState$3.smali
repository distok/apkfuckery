.class public final Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;
.super Lx/m/c/k;
.source "CallSoundManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/CallSoundManager;->subscribeToStoreState(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/voice/CallSoundManager$StoreState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $voiceChannelId:J

.field public final synthetic this$0:Lcom/discord/utilities/voice/CallSoundManager;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/CallSoundManager;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;->this$0:Lcom/discord/utilities/voice/CallSoundManager;

    iput-wide p2, p0, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;->$voiceChannelId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/voice/CallSoundManager$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;->invoke(Lcom/discord/utilities/voice/CallSoundManager$StoreState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/voice/CallSoundManager$StoreState;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;->this$0:Lcom/discord/utilities/voice/CallSoundManager;

    iget-wide v1, p0, Lcom/discord/utilities/voice/CallSoundManager$subscribeToStoreState$3;->$voiceChannelId:J

    const-string/jumbo v3, "storeState"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, v2, p1}, Lcom/discord/utilities/voice/CallSoundManager;->access$handleStoreState(Lcom/discord/utilities/voice/CallSoundManager;JLcom/discord/utilities/voice/CallSoundManager$StoreState;)V

    return-void
.end method
