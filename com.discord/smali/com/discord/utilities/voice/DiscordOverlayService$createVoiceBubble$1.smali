.class public final Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;
.super Lx/m/c/k;
.source "DiscordOverlayService.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/voice/DiscordOverlayService;->createVoiceBubble()Lcom/discord/overlay/views/OverlayBubbleWrap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/overlay/views/OverlayBubbleWrap;",
        "Lcom/discord/overlay/views/OverlayBubbleWrap;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $anchorTag:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/utilities/voice/DiscordOverlayService;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/voice/DiscordOverlayService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;->this$0:Lcom/discord/utilities/voice/DiscordOverlayService;

    iput-object p2, p0, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;->$anchorTag:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/overlay/views/OverlayBubbleWrap;)Lcom/discord/overlay/views/OverlayBubbleWrap;
    .locals 10

    const-string/jumbo v0, "srcBubble"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;->this$0:Lcom/discord/utilities/voice/DiscordOverlayService;

    invoke-static {v0}, Lcom/discord/utilities/voice/DiscordOverlayService;->access$createMenu(Lcom/discord/utilities/voice/DiscordOverlayService;)Lcom/discord/overlay/views/OverlayDialog;

    move-result-object v0

    const v1, 0x7f0a0742

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "dialogBubble.findViewByI\u2026erlay_linked_anchor_view)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v2, p1, Lf/a/n/w;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    move-object v2, v3

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    check-cast v2, Lf/a/n/w;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lf/a/n/w;->getImageView$app_productionDiscordExternalRelease()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v2

    if-eqz v2, :cond_9

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    instance-of v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v6, :cond_1

    move-object v5, v3

    :cond_1
    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    iget v5, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    instance-of v8, v7, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v8, :cond_3

    move-object v7, v3

    :cond_3
    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v7, :cond_4

    iget v7, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    instance-of v9, v8, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v9, :cond_5

    move-object v8, v3

    :cond_5
    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v8, :cond_6

    iget v8, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_3

    :cond_6
    const/4 v8, 0x0

    :goto_3
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    instance-of v9, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v9, :cond_7

    goto :goto_4

    :cond_7
    move-object v3, v2

    :goto_4
    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v3, :cond_8

    iget v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    :cond_8
    invoke-direct {v4, v5, v7, v8, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_5

    :cond_9
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    :goto_5
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    iget v5, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v5

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result p1

    iput p1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    iget-object p1, p0, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;->$anchorTag:Ljava/lang/String;

    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/voice/DiscordOverlayService$createVoiceBubble$1;->invoke(Lcom/discord/overlay/views/OverlayBubbleWrap;)Lcom/discord/overlay/views/OverlayBubbleWrap;

    move-result-object p1

    return-object p1
.end method
