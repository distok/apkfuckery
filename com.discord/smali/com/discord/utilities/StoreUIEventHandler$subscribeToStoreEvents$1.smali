.class public final Lcom/discord/utilities/StoreUIEventHandler$subscribeToStoreEvents$1;
.super Lx/m/c/k;
.source "StoreUIEventHandler.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/StoreUIEventHandler;->subscribeToStoreEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/KrispOveruseDetector$Status;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/StoreUIEventHandler;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/StoreUIEventHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/StoreUIEventHandler$subscribeToStoreEvents$1;->this$0:Lcom/discord/utilities/StoreUIEventHandler;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/rtcconnection/KrispOveruseDetector$Status;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/StoreUIEventHandler$subscribeToStoreEvents$1;->invoke(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/StoreUIEventHandler$subscribeToStoreEvents$1;->this$0:Lcom/discord/utilities/StoreUIEventHandler;

    invoke-static {v0, p1}, Lcom/discord/utilities/StoreUIEventHandler;->access$handleKrispStatusEvent(Lcom/discord/utilities/StoreUIEventHandler;Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    return-void
.end method
