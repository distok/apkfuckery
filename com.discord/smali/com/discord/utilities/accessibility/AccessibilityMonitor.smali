.class public final Lcom/discord/utilities/accessibility/AccessibilityMonitor;
.super Ljava/lang/Object;
.source "AccessibilityMonitor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;


# instance fields
.field private accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

.field private final accessibilityStateSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/utilities/accessibility/AccessibilityState;",
            "Lcom/discord/utilities/accessibility/AccessibilityState;",
            ">;"
        }
    .end annotation
.end field

.field private final animationScaleObserver:Landroid/database/ContentObserver;

.field private contentResolver:Landroid/content/ContentResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->Companion:Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;

    sget-object v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor$animationScaleObserver$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor$animationScaleObserver$1;-><init>(Lcom/discord/utilities/accessibility/AccessibilityMonitor;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->animationScaleObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/discord/utilities/accessibility/AccessibilityState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v1}, Lcom/discord/utilities/accessibility/AccessibilityState;-><init>(Ljava/util/EnumSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    new-instance v1, Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-direct {v1, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityStateSubject:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->INSTANCE$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$handleReduceMotionUpdated(Lcom/discord/utilities/accessibility/AccessibilityMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->handleReduceMotionUpdated()V

    return-void
.end method

.method public static final synthetic access$handleScreenreaderEnabledUpdate(Lcom/discord/utilities/accessibility/AccessibilityMonitor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->handleScreenreaderEnabledUpdate(Z)V

    return-void
.end method

.method private final declared-synchronized handleInitialState(Landroid/content/Context;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/accessibility/AccessibilityManager;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_4

    iput-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/discord/utilities/accessibility/AccessibilityMonitor$handleInitialState$1;

    invoke-direct {v1, p0}, Lcom/discord/utilities/accessibility/AccessibilityMonitor$handleInitialState$1;-><init>(Lcom/discord/utilities/accessibility/AccessibilityMonitor;)V

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    iget-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->handleScreenreaderEnabledUpdate(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "context.contentResolver"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->contentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v0, "transition_animation_scale"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->contentResolver:Landroid/content/ContentResolver;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->animationScaleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v0, "context.resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    invoke-direct {p0, p1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->handleUIModeUpdate(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string p1, "contentResolver"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_2
    :try_start_2
    const-string p1, "accessibilityManager"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_3
    :try_start_3
    const-string p1, "accessibilityManager"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final handleReduceMotionUpdated()V
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    invoke-virtual {v0}, Lcom/discord/utilities/accessibility/AccessibilityState;->getFeatures()Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->contentResolver:Landroid/content/ContentResolver;

    if-eqz v1, :cond_2

    const-string/jumbo v2, "transition_animation_scale"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0.0"

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "0"

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->REDUCED_MOTION:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->REDUCED_MOTION:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    const-string v2, "features"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/discord/utilities/accessibility/AccessibilityState;->copy(Ljava/util/EnumSet;)Lcom/discord/utilities/accessibility/AccessibilityState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->updateAccessibilityState(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    return-void

    :cond_2
    const-string v0, "contentResolver"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final handleScreenreaderEnabledUpdate(Z)V
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    invoke-virtual {v0}, Lcom/discord/utilities/accessibility/AccessibilityState;->getFeatures()Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string p1, "services"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->SCREENREADER:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->SCREENREADER:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    :goto_0
    iget-object p1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    const-string v1, "features"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/accessibility/AccessibilityState;->copy(Ljava/util/EnumSet;)Lcom/discord/utilities/accessibility/AccessibilityState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->updateAccessibilityState(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    return-void

    :cond_1
    const-string p1, "accessibilityManager"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final handleUIModeUpdate(I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    invoke-virtual {v0}, Lcom/discord/utilities/accessibility/AccessibilityState;->getFeatures()Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    and-int/lit8 p1, p1, 0x30

    const/16 v1, 0x10

    if-eq p1, v1, :cond_1

    const/16 v1, 0x20

    if-eq p1, v1, :cond_0

    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->PREFERS_COLOR_SCHEME_LIGHT:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->PREFERS_COLOR_SCHEME_DARK:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->PREFERS_COLOR_SCHEME_DARK:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->PREFERS_COLOR_SCHEME_LIGHT:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->PREFERS_COLOR_SCHEME_LIGHT:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->PREFERS_COLOR_SCHEME_DARK:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    :goto_0
    iget-object p1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    const-string v1, "features"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/accessibility/AccessibilityState;->copy(Ljava/util/EnumSet;)Lcom/discord/utilities/accessibility/AccessibilityState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->updateAccessibilityState(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    return-void
.end method

.method private final updateAccessibilityState(Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 1

    iput-object p1, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityState:Lcom/discord/utilities/accessibility/AccessibilityState;

    iget-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityStateSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final bindContext(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->handleInitialState(Landroid/content/Context;)V

    return-void
.end method

.method public final observeAccessibilityState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/accessibility/AccessibilityState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->accessibilityStateSubject:Lrx/subjects/SerializedSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "accessibilityStateSubject.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
