.class public final Lcom/discord/utilities/cache/SharedPreferencesProvider;
.super Ljava/lang/Object;
.source "SharedPreferencesProvider.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/cache/SharedPreferencesProvider;

.field private static sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/cache/SharedPreferencesProvider;

    invoke-direct {v0}, Lcom/discord/utilities/cache/SharedPreferencesProvider;-><init>()V

    sput-object v0, Lcom/discord/utilities/cache/SharedPreferencesProvider;->INSTANCE:Lcom/discord/utilities/cache/SharedPreferencesProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Landroid/content/SharedPreferences;
    .locals 1

    sget-object v0, Lcom/discord/utilities/cache/SharedPreferencesProvider;->sharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "sharedPreferences"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final init(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "PreferenceManager.getDef\u2026haredPreferences(context)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object p1, Lcom/discord/utilities/cache/SharedPreferencesProvider;->sharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method
