.class public final Lcom/discord/utilities/string/StringUtilsKt;
.super Ljava/lang/Object;
.source "StringUtils.kt"


# static fields
.field private static final STATIC_IMAGE_EXTENSION:Ljava/lang/String;

.field private static final STRIP_ACCENTS_REGEX:Lkotlin/text/Regex;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "[\\p{InCombiningDiacriticalMarks}]"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/discord/utilities/string/StringUtilsKt;->STRIP_ACCENTS_REGEX:Lkotlin/text/Regex;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0x1c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "png"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "webp"

    :goto_0
    sput-object v0, Lcom/discord/utilities/string/StringUtilsKt;->STATIC_IMAGE_EXTENSION:Ljava/lang/String;

    return-void
.end method

.method public static final encodeToBase32String([B)Ljava/lang/String;
    .locals 7

    const-string v0, "$this$encodeToBase32String"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    array-length v1, p0

    add-int/lit8 v1, v1, 0x7

    mul-int/lit8 v1, v1, 0x8

    div-int/lit8 v1, v1, 0x5

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_5

    aget-byte v4, p0, v2

    if-ltz v4, :cond_0

    aget-byte v4, p0, v2

    goto :goto_1

    :cond_0
    aget-byte v4, p0, v2

    add-int/lit16 v4, v4, 0x100

    :goto_1
    const/4 v5, 0x3

    if-le v3, v5, :cond_3

    add-int/lit8 v2, v2, 0x1

    array-length v5, p0

    if-ge v2, v5, :cond_2

    aget-byte v5, p0, v2

    if-ltz v5, :cond_1

    aget-byte v5, p0, v2

    goto :goto_2

    :cond_1
    aget-byte v5, p0, v2

    add-int/lit16 v5, v5, 0x100

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    const/16 v6, 0xff

    shr-int/2addr v6, v3

    and-int/2addr v4, v6

    add-int/lit8 v3, v3, 0x5

    rem-int/lit8 v3, v3, 0x8

    shl-int/2addr v4, v3

    rsub-int/lit8 v6, v3, 0x8

    shr-int/2addr v5, v6

    or-int/2addr v4, v5

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v3, 0x5

    rsub-int/lit8 v5, v3, 0x8

    shr-int/2addr v4, v5

    and-int/lit8 v4, v4, 0x1f

    rem-int/lit8 v3, v3, 0x8

    if-nez v3, :cond_4

    add-int/lit8 v2, v2, 0x1

    :cond_4
    :goto_3
    const-string v5, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

    invoke-virtual {v5, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "Base32.encodeOriginal(this)"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final filenameSanitized(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$filenameSanitized"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "[/\\\\]"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    const-string v1, "_"

    invoke-virtual {v0, p0, v1}, Lkotlin/text/Regex;->replace(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final format(ILandroid/content/Context;)Ljava/lang/String;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/locale/LocaleManager;

    invoke-direct {v0}, Lcom/discord/utilities/locale/LocaleManager;-><init>()V

    invoke-virtual {v0, p1}, Lcom/discord/utilities/locale/LocaleManager;->getPrimaryLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object p1

    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "NumberFormat.getInstance\u2026le(context)).format(this)"

    invoke-static {p0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getSTATIC_IMAGE_EXTENSION()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/discord/utilities/string/StringUtilsKt;->STATIC_IMAGE_EXTENSION:Ljava/lang/String;

    return-object v0
.end method

.method public static final getStringByLocale(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$getStringByLocale"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object p0

    const-string p2, "createConfigurationContext(configuration)"

    invoke-static {p0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final stripAccents(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$stripAccents"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Ljava/text/Normalizer$Form;->NFKD:Ljava/text/Normalizer$Form;

    invoke-static {p0, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "normalizedString"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/string/StringUtilsKt;->STRIP_ACCENTS_REGEX:Lkotlin/text/Regex;

    const-string v1, ""

    invoke-virtual {v0, p0, v1}, Lkotlin/text/Regex;->replace(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
