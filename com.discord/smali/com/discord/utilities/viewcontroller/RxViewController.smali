.class public abstract Lcom/discord/utilities/viewcontroller/RxViewController;
.super Ljava/lang/Object;
.source "RxViewController.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private subscription:Lrx/Subscription;

.field private final view:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/viewcontroller/RxViewController;->view:Landroid/view/View;

    return-void
.end method

.method private final connectViewRx(Landroid/view/View;Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/Subscription;"
        }
    .end annotation

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v2, Lcom/discord/utilities/viewcontroller/ViewDetachedFromWindowObservable;

    invoke-direct {v2, p1}, Lcom/discord/utilities/viewcontroller/ViewDetachedFromWindowObservable;-><init>(Landroid/view/View;)V

    invoke-virtual {v2}, Lcom/discord/utilities/viewcontroller/ViewDetachedFromWindowObservable;->observe()Lrx/Observable;

    move-result-object v2

    invoke-virtual {p2, v2}, Lrx/Observable;->V(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    const-string v2, "observable\n        .take\u2026servable(view).observe())"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    new-instance v2, Lcom/discord/utilities/viewcontroller/RxViewController$connectViewRx$1;

    invoke-direct {v2, p1}, Lcom/discord/utilities/viewcontroller/RxViewController$connectViewRx$1;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    const-string p1, "observable\n        .take\u2026view.isAttachedToWindow }"

    invoke-static {v3, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    new-instance v6, Lcom/discord/utilities/viewcontroller/RxViewController$connectViewRx$2;

    invoke-direct {v6, v0}, Lcom/discord/utilities/viewcontroller/RxViewController$connectViewRx$2;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;)V

    new-instance v9, Lcom/discord/utilities/viewcontroller/RxViewController$connectViewRx$3;

    invoke-direct {v9, p3}, Lcom/discord/utilities/viewcontroller/RxViewController$connectViewRx$3;-><init>(Lkotlin/jvm/functions/Function1;)V

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1a

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-eqz p1, :cond_0

    check-cast p1, Lrx/Subscription;

    return-object p1

    :cond_0
    const-string/jumbo p1, "subscriptionResult"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final bind()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/viewcontroller/RxViewController;->subscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/viewcontroller/RxViewController;->view:Landroid/view/View;

    invoke-virtual {p0}, Lcom/discord/utilities/viewcontroller/RxViewController;->observeState()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/utilities/viewcontroller/RxViewController$bind$1;

    invoke-direct {v2, p0}, Lcom/discord/utilities/viewcontroller/RxViewController$bind$1;-><init>(Lcom/discord/utilities/viewcontroller/RxViewController;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/utilities/viewcontroller/RxViewController;->connectViewRx(Landroid/view/View;Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/utilities/viewcontroller/RxViewController;->subscription:Lrx/Subscription;

    return-void
.end method

.method public abstract configureView(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public final getView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/viewcontroller/RxViewController;->view:Landroid/view/View;

    return-object v0
.end method

.method public abstract observeState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end method
