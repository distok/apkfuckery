.class public final Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;
.super Landroid/text/style/ReplacementSpan;
.source "SimpleRoundedBackgroundSpan.kt"


# instance fields
.field private final backgroundColor:I

.field private final cornerRadius:F

.field private final marginHorizontal:I

.field private final paddingHorizontal:I

.field private final textColor:I


# direct methods
.method public constructor <init>(IIIIF)V
    .locals 0

    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    iput p1, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->paddingHorizontal:I

    iput p2, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->marginHorizontal:I

    iput p3, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->backgroundColor:I

    iput p4, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->textColor:I

    iput p5, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->cornerRadius:F

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 11

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v7, p9

    const-string v3, "canvas"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "paint"

    invoke-static {v7, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    iget v3, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->marginHorizontal:I

    iget v4, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->paddingHorizontal:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v3

    move v3, p3

    move v5, p4

    invoke-virtual {v7, p2, p3, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v6

    new-instance v8, Landroid/graphics/RectF;

    iget v9, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->marginHorizontal:I

    int-to-float v9, v9

    add-float v9, p5, v9

    move/from16 v10, p6

    int-to-float v10, v10

    add-float v6, p5, v6

    int-to-float v4, v4

    add-float/2addr v6, v4

    move/from16 v4, p8

    int-to-float v4, v4

    invoke-direct {v8, v9, v10, v6, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v4, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->backgroundColor:I

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget v4, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->cornerRadius:F

    invoke-virtual {p1, v8, v4, v4, v7}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget v4, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->marginHorizontal:I

    int-to-float v4, v4

    add-float v4, p5, v4

    iget v6, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->paddingHorizontal:I

    int-to-float v6, v6

    add-float/2addr v6, v4

    iget v4, v0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->textColor:I

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    move/from16 v4, p7

    int-to-float v8, v4

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, v6

    move v6, v8

    move-object/from16 v7, p9

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 0

    const-string p5, "paint"

    invoke-static {p1, p5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2, p3, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result p1

    iget p2, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->marginHorizontal:I

    mul-int/lit8 p2, p2, 0x2

    iget p3, p0, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;->paddingHorizontal:I

    mul-int/lit8 p3, p3, 0x2

    add-int/2addr p3, p2

    add-int/2addr p3, p1

    return p3
.end method
