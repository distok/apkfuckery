.class public final Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;
.super Ljava/lang/Object;
.source "Parsers.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;)Ljava/lang/CharSequence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $changeLogRules:Z

.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $onClickListener:Lkotlin/jvm/functions/Function3;

.field private final context:Landroid/content/Context;

.field private final linkColorAttrResId:I

.field private final onLongPressUrl:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function3;ZLandroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->$onClickListener:Lkotlin/jvm/functions/Function3;

    iput-boolean p2, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->$changeLogRules:Z

    iput-object p3, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->context:Landroid/content/Context;

    const p1, 0x7f040177

    iput p1, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->linkColorAttrResId:I

    new-instance p1, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onLongPressUrl$1;

    invoke-direct {p1, p0}, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onLongPressUrl$1;-><init>(Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;)V

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->onLongPressUrl:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getLinkColorAttrResId()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->linkColorAttrResId:I

    return v0
.end method

.method public getOnClickUrl()Lkotlin/jvm/functions/Function3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function3<",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;

    invoke-direct {v0, p0}, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;-><init>(Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;)V

    return-object v0
.end method

.method public getOnLongPressUrl()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->onLongPressUrl:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
