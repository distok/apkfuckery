.class public final Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;
.super Lx/m/c/k;
.source "Rules.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/Rules;->createCodeBlockRule()Lcom/discord/simpleast/core/parser/Rule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function3<",
        "Lcom/discord/simpleast/code/CodeNode<",
        "TRC;>;",
        "Ljava/lang/Boolean;",
        "TS;",
        "Lcom/discord/simpleast/core/node/Node<",
        "TRC;>;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;

    invoke-direct {v0}, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;-><init>()V

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/simpleast/code/CodeNode;ZLcom/discord/utilities/textprocessing/Rules$BlockQuoteState;)Lcom/discord/simpleast/core/node/Node;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/simpleast/code/CodeNode<",
            "TRC;>;ZTS;)",
            "Lcom/discord/simpleast/core/node/Node<",
            "TRC;>;"
        }
    .end annotation

    const-string v0, "codeNode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "state"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p2, :cond_0

    new-instance p2, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1$1;

    new-array p3, v1, [Lcom/discord/simpleast/core/node/Node;

    aput-object p1, p3, v0

    invoke-direct {p2, p1, p3}, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1$1;-><init>(Lcom/discord/simpleast/code/CodeNode;[Lcom/discord/simpleast/core/node/Node;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;

    invoke-interface {p3}, Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;->isInQuote()Z

    move-result p3

    new-array v1, v1, [Lcom/discord/simpleast/core/node/Node;

    aput-object p1, v1, v0

    invoke-direct {p2, p3, v1}, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;-><init>(Z[Lcom/discord/simpleast/core/node/Node;)V

    :goto_0
    return-object p2
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/simpleast/code/CodeNode;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    check-cast p3, Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;->invoke(Lcom/discord/simpleast/code/CodeNode;ZLcom/discord/utilities/textprocessing/Rules$BlockQuoteState;)Lcom/discord/simpleast/core/node/Node;

    move-result-object p1

    return-object p1
.end method
