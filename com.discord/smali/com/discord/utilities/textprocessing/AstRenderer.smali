.class public final Lcom/discord/utilities/textprocessing/AstRenderer;
.super Ljava/lang/Object;
.source "AstRenderer.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/textprocessing/AstRenderer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/textprocessing/AstRenderer;

    invoke-direct {v0}, Lcom/discord/utilities/textprocessing/AstRenderer;-><init>()V

    sput-object v0, Lcom/discord/utilities/textprocessing/AstRenderer;->INSTANCE:Lcom/discord/utilities/textprocessing/AstRenderer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final render(Ljava/util/Collection;Ljava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;>;TT;)",
            "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;"
        }
    .end annotation

    const-string v0, "ast"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-direct {v0}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;-><init>()V

    invoke-static {v0, p0, p1}, Lf/a/j/b/b/g;->a(Landroid/text/SpannableStringBuilder;Ljava/util/Collection;Ljava/lang/Object;)Landroid/text/SpannableStringBuilder;

    sget-object p0, Lcom/discord/utilities/textprocessing/AstRenderer;->INSTANCE:Lcom/discord/utilities/textprocessing/AstRenderer;

    invoke-direct {p0, v0}, Lcom/discord/utilities/textprocessing/AstRenderer;->trim(Landroid/text/SpannableStringBuilder;)V

    return-object v0
.end method

.method private final trim(Landroid/text/SpannableStringBuilder;)V
    .locals 5

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-array v0, v2, [C

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4, v0, v1}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    sub-int/2addr v0, v2

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    :cond_2
    return-void
.end method
