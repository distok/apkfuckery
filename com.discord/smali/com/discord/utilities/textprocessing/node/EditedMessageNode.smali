.class public final Lcom/discord/utilities/textprocessing/node/EditedMessageNode;
.super Lcom/discord/simpleast/core/node/StyleNode;
.source "EditedMessageNode.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RC:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/discord/simpleast/core/node/StyleNode<",
        "TRC;",
        "Landroid/text/style/CharacterStyle;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/textprocessing/node/EditedMessageNode;->Companion:Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Lcom/discord/utilities/textprocessing/node/EditedMessageNode;->Companion:Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;->getRelativeSizeSpan$default(Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;FILjava/lang/Object;)Landroid/text/style/RelativeSizeSpan;

    move-result-object v2

    const/4 v4, 0x0

    aput-object v2, v0, v4

    invoke-static {v1, p1}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;->access$getForegroundColorSpan(Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;Landroid/content/Context;)Landroid/text/style/ForegroundColorSpan;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/simpleast/core/node/StyleNode;-><init>(Ljava/util/List;)V

    new-instance v0, Lf/a/j/b/a/a;

    invoke-virtual {v1, p1}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;->getEditedString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lf/a/j/b/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/discord/simpleast/core/node/Node;->addChild(Lcom/discord/simpleast/core/node/Node;)V

    return-void
.end method
