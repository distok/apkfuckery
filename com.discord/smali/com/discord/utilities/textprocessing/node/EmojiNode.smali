.class public final Lcom/discord/utilities/textprocessing/node/EmojiNode;
.super Lf/a/j/b/a/a;
.source "EmojiNode.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/Spoilerable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;,
        Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;",
        ">",
        "Lf/a/j/b/a/a<",
        "TT;>;",
        "Lcom/discord/utilities/textprocessing/node/Spoilerable;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

.field private static final EMOJI_SIZE:I

.field private static final JUMBOIFY_SCALE_FACTOR:I = 0x3


# instance fields
.field private final height:I

.field private isJumbo:Z

.field private isRevealed:Z

.field private final urlProvider:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final width:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    sput v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    const-string v0, "emojiName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "urlProvider"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lf/a/j/b/a/a;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->urlProvider:Lkotlin/jvm/functions/Function3;

    iput p3, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    iput p4, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    sget p3, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    sget p4, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;II)V

    return-void
.end method

.method public static final synthetic access$getEMOJI_SIZE$cp()I
    .locals 1

    sget v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->EMOJI_SIZE:I

    return v0
.end method

.method public static final synthetic access$getHeight$p(Lcom/discord/utilities/textprocessing/node/EmojiNode;)I
    .locals 0

    iget p0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    return p0
.end method

.method public static final synthetic access$getUrlProvider$p(Lcom/discord/utilities/textprocessing/node/EmojiNode;)Lkotlin/jvm/functions/Function3;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->urlProvider:Lkotlin/jvm/functions/Function3;

    return-object p0
.end method

.method public static final synthetic access$getWidth$p(Lcom/discord/utilities/textprocessing/node/EmojiNode;)I
    .locals 0

    iget p0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    return p0
.end method

.method public static final renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;Z)V
    .locals 7

    sget-object v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->renderEmoji$default(Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZIILjava/lang/Object;)V

    return-void
.end method

.method public static final renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZI)V
    .locals 1

    sget-object v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZI)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-virtual {p1}, Lf/a/j/b/a/a;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lf/a/j/b/a/a;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    iget v1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    iget v1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    if-ne v0, v1, :cond_0

    iget-boolean p1, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isJumbo:Z

    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isJumbo:Z

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final isJumbo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isJumbo:Z

    return v0
.end method

.method public isRevealed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed:Z

    return v0
.end method

.method public render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "TT;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "builder"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "renderContext"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface/range {p2 .. p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object v4, v1

    check-cast v4, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lf/a/j/b/a/a;->getContent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-boolean v6, v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isJumbo:Z

    iget v7, v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->width:I

    if-eqz v6, :cond_0

    mul-int/lit8 v7, v7, 0x3

    :cond_0
    iget v8, v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->height:I

    if-eqz v6, :cond_1

    mul-int/lit8 v8, v8, 0x3

    :cond_1
    const/4 v9, 0x1

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x2

    :goto_0
    iget-object v10, v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->urlProvider:Lkotlin/jvm/functions/Function3;

    invoke-interface/range {p2 .. p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->isAnimationEnabled()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-static {v7}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface/range {p2 .. p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-interface {v10, v11, v12, v13}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v10, v11, v11, v9}, Lcom/discord/utilities/images/MGImages;->getImageRequest(Ljava/lang/String;IIZ)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v10

    invoke-static {}, Lf/g/g/a/a/b;->a()Lf/g/g/a/a/d;

    move-result-object v12

    iput-object v10, v12, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed()Z

    move-result v10

    iput-boolean v10, v12, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->j:Z

    invoke-virtual {v12}, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->a()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object v10

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    new-instance v13, Lf/g/g/f/a;

    invoke-direct {v13, v12}, Lf/g/g/f/a;-><init>(Landroid/content/res/Resources;)V

    new-instance v12, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v12, v11}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v12, v13, Lf/g/g/f/a;->d:Landroid/graphics/drawable/Drawable;

    sget-object v12, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v12, Lf/g/g/e/v;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object v12, v13, Lf/g/g/f/a;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual/range {p0 .. p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed()Z

    move-result v12

    if-nez v12, :cond_6

    instance-of v12, v2, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    const/4 v14, 0x0

    if-nez v12, :cond_3

    move-object v15, v14

    goto :goto_1

    :cond_3
    move-object v15, v2

    :goto_1
    check-cast v15, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    if-nez v12, :cond_4

    goto :goto_2

    :cond_4
    move-object v14, v2

    :goto_2
    check-cast v14, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    if-eqz v14, :cond_5

    invoke-interface {v14}, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;->getSpoilerColorRes()I

    move-result v12

    goto :goto_3

    :cond_5
    const v12, 0x7f0405b0

    invoke-static {v3, v12}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v12

    :goto_3
    new-instance v14, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v14, v12}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v13, v14}, Lf/g/g/f/a;->b(Landroid/graphics/drawable/Drawable;)Lf/g/g/f/a;

    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v12

    sub-int/2addr v12, v9

    invoke-virtual {v13}, Lf/g/g/f/a;->a()Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object v13

    new-instance v14, Lcom/facebook/drawee/view/DraweeHolder;

    invoke-direct {v14, v13}, Lcom/facebook/drawee/view/DraweeHolder;-><init>(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    invoke-virtual {v14, v10}, Lcom/facebook/drawee/view/DraweeHolder;->g(Lcom/facebook/drawee/interfaces/DraweeController;)V

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    const/16 v13, 0x21

    if-lt v12, v10, :cond_7

    goto :goto_4

    :cond_7
    invoke-virtual {v14}, Lcom/facebook/drawee/view/DraweeHolder;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    if-eqz v10, :cond_9

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v15

    invoke-virtual {v15}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-virtual {v10, v11, v11, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_8
    iget-object v7, v4, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->e:Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;

    invoke-virtual {v10, v7}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_9
    new-instance v7, Lf/g/g/i/a;

    invoke-direct {v7, v14, v6}, Lf/g/g/i/a;-><init>(Lcom/facebook/drawee/view/DraweeHolder;I)V

    iget-object v6, v14, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    instance-of v10, v6, Lcom/facebook/drawee/controller/AbstractDraweeController;

    if-eqz v10, :cond_a

    check-cast v6, Lcom/facebook/drawee/controller/AbstractDraweeController;

    new-instance v10, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;

    invoke-direct {v10, v4, v7, v11, v8}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;-><init>(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Lf/g/g/i/a;ZI)V

    invoke-virtual {v6, v10}, Lcom/facebook/drawee/controller/AbstractDraweeController;->f(Lcom/facebook/drawee/controller/ControllerListener;)V

    :cond_a
    iget-object v6, v4, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->d:Ljava/util/Set;

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v12, 0x1

    invoke-virtual {v4, v7, v5, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_4
    new-instance v4, Lcom/discord/utilities/spans/ClickableSpan;

    const v6, 0x7f0601dd

    invoke-static {v3, v6}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v3, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;

    invoke-direct {v3, v0, v2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;-><init>(Lcom/discord/utilities/textprocessing/node/EmojiNode;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V

    const/16 v19, 0x4

    const/16 v20, 0x0

    move-object v14, v4

    move-object/from16 v18, v3

    invoke-direct/range {v14 .. v20}, Lcom/discord/utilities/spans/ClickableSpan;-><init>(Ljava/lang/Integer;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    if-gt v5, v12, :cond_b

    add-int/2addr v12, v9

    invoke-virtual {v1, v4, v5, v12, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_5

    :cond_b
    sget-object v6, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v8, Ljava/lang/Exception;

    const-string v1, "Span content: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lf/a/j/b/a/a;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    const-string v7, "Unable to render emoji tappable span."

    invoke-static/range {v6 .. v11}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_5
    return-void
.end method

.method public bridge synthetic render(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V

    return-void
.end method

.method public final setJumbo(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isJumbo:Z

    return-void
.end method

.method public setRevealed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode;->isRevealed:Z

    return-void
.end method
