.class public final Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;
.super Lcom/discord/simpleast/core/node/Node$a;
.source "BlockBackgroundNode.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/Spoilerable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;",
        ">",
        "Lcom/discord/simpleast/core/node/Node$a<",
        "TR;>;",
        "Lcom/discord/utilities/textprocessing/node/Spoilerable;"
    }
.end annotation


# instance fields
.field private final inQuote:Z

.field private isRevealed:Z


# direct methods
.method public varargs constructor <init>(Z[Lcom/discord/simpleast/core/node/Node;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z[",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;)V"
        }
    .end annotation

    const-string v0, "children"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lcom/discord/simpleast/core/node/Node;

    invoke-direct {p0, p2}, Lcom/discord/simpleast/core/node/Node$a;-><init>([Lcom/discord/simpleast/core/node/Node;)V

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->inQuote:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->isRevealed:Z

    return-void
.end method

.method private final ensureEndsWithNewline(Landroid/text/SpannableStringBuilder;)V
    .locals 4

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x6

    new-array v0, v0, [C

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p1, v3, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    aget-char v0, v0, v2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_1
    return-void
.end method


# virtual methods
.method public isRevealed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->isRevealed:Z

    return v0
.end method

.method public render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/BasicRenderContext;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "TR;)V"
        }
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->ensureEndsWithNewline(Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-super {p0, p1, p2}, Lcom/discord/simpleast/core/node/Node$a;->render(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->ensureEndsWithNewline(Landroid/text/SpannableStringBuilder;)V

    invoke-interface {p2}, Lcom/discord/utilities/textprocessing/node/BasicRenderContext;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->isRevealed()Z

    move-result v2

    if-eqz v2, :cond_0

    const p2, 0x7f0405a2

    invoke-static {v1, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p2

    :goto_0
    move v3, p2

    goto :goto_1

    :cond_0
    instance-of v2, p2, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    if-nez v2, :cond_1

    const/4 p2, 0x0

    :cond_1
    check-cast p2, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;

    if-eqz p2, :cond_2

    invoke-interface {p2}, Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;->getSpoilerColorRes()I

    move-result p2

    goto :goto_0

    :cond_2
    const p2, 0x7f0405b0

    invoke-static {v1, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p2

    goto :goto_0

    :goto_1
    const p2, 0x7f0405a3

    invoke-static {v1, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v4

    new-instance p2, Lcom/discord/utilities/spans/BlockBackgroundSpan;

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v5

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v6

    iget-boolean v1, p0, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->inQuote:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/utilities/textprocessing/node/BlockQuoteNode;->Companion:Lcom/discord/utilities/textprocessing/node/BlockQuoteNode$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/textprocessing/node/BlockQuoteNode$Companion;->getTOTAL_LEFT_MARGIN()I

    move-result v1

    move v7, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    const/4 v7, 0x0

    :goto_2
    move-object v2, p2

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/spans/BlockBackgroundSpan;-><init>(IIIII)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v2, 0x21

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance p2, Landroid/text/style/LeadingMarginSpan$Standard;

    const/16 v1, 0xf

    invoke-direct {p2, v1}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const/4 p2, 0x5

    invoke-static {p2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result p2

    new-instance v1, Lcom/discord/utilities/spans/VerticalPaddingSpan;

    invoke-direct {v1, p2, p2}, Lcom/discord/utilities/spans/VerticalPaddingSpan;-><init>(II)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p2

    invoke-virtual {p1, v1, v0, p2, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method public bridge synthetic render(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/textprocessing/node/BasicRenderContext;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/BasicRenderContext;)V

    return-void
.end method

.method public setRevealed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/BlockBackgroundNode;->isRevealed:Z

    return-void
.end method
