.class public final Lcom/discord/utilities/textprocessing/node/UserMentionNode$renderUserMention$1;
.super Lx/m/c/k;
.source "UserMentionNode.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/node/UserMentionNode;->renderUserMention(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/UserMentionNode$RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $onClick:Lkotlin/jvm/functions/Function1;

.field public final synthetic this$0:Lcom/discord/utilities/textprocessing/node/UserMentionNode;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/textprocessing/node/UserMentionNode;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/node/UserMentionNode$renderUserMention$1;->this$0:Lcom/discord/utilities/textprocessing/node/UserMentionNode;

    iput-object p2, p0, Lcom/discord/utilities/textprocessing/node/UserMentionNode$renderUserMention$1;->$onClick:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/textprocessing/node/UserMentionNode$renderUserMention$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/utilities/textprocessing/node/UserMentionNode$renderUserMention$1;->$onClick:Lkotlin/jvm/functions/Function1;

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/node/UserMentionNode$renderUserMention$1;->this$0:Lcom/discord/utilities/textprocessing/node/UserMentionNode;

    invoke-virtual {v0}, Lcom/discord/utilities/textprocessing/node/UserMentionNode;->getUserId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
