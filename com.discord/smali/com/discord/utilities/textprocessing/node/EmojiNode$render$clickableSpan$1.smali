.class public final Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;
.super Lx/m/c/k;
.source "EmojiNode.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/node/EmojiNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $renderContext:Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;

.field public final synthetic this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/textprocessing/node/EmojiNode;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;

    iput-object p2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->$renderContext:Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 5

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/textprocessing/node/EmojiNode;

    iget-object v1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-virtual {v1}, Lf/a/j/b/a/a;->getContent()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-static {v2}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->access$getUrlProvider$p(Lcom/discord/utilities/textprocessing/node/EmojiNode;)Lkotlin/jvm/functions/Function3;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-static {v3}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->access$getWidth$p(Lcom/discord/utilities/textprocessing/node/EmojiNode;)I

    move-result v3

    iget-object v4, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-static {v4}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->access$getHeight$p(Lcom/discord/utilities/textprocessing/node/EmojiNode;)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/utilities/textprocessing/node/EmojiNode;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function3;II)V

    new-instance v1, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-direct {v1}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;-><init>()V

    new-instance v2, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;

    invoke-direct {v2, p0}, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;-><init>(Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V

    const/16 v0, 0x2002

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    const/16 v0, 0x589

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode;

    invoke-virtual {v2}, Lf/a/j/b/a/a;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {p1, v1, v0, v2, v3}, Lf/a/b/p;->j(Landroid/content/Context;Ljava/lang/CharSequence;ILcom/discord/utilities/view/ToastManager;I)V

    return-void
.end method
