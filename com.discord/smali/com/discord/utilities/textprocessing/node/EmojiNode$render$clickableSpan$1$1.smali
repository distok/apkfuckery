.class public final Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;
.super Ljava/lang/Object;
.source "EmojiNode.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->invoke(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final isAnimationEnabled:Z

.field public final synthetic this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;->this$0:Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->$renderContext:Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;

    invoke-interface {v0}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;->context:Landroid/content/Context;

    iget-object p1, p1, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1;->$renderContext:Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;

    invoke-interface {p1}, Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;->isAnimationEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;->isAnimationEnabled:Z

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isAnimationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$render$clickableSpan$1$1;->isAnimationEnabled:Z

    return v0
.end method
