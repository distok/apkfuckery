.class public final Lcom/discord/utilities/textprocessing/node/UrlNode$render$style$1;
.super Lx/m/c/k;
.source "UrlNode.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/node/UrlNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $renderContext:Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;

.field public final synthetic $safeUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/node/UrlNode$render$style$1;->$renderContext:Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;

    iput-object p2, p0, Lcom/discord/utilities/textprocessing/node/UrlNode$render$style$1;->$safeUrl:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/textprocessing/node/UrlNode$render$style$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/utilities/textprocessing/node/UrlNode$render$style$1;->$renderContext:Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;

    invoke-interface {p1}, Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;->getOnLongPressUrl()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/node/UrlNode$render$style$1;->$safeUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
