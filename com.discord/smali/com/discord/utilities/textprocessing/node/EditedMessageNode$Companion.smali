.class public final Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;
.super Ljava/lang/Object;
.source "EditedMessageNode.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/textprocessing/node/EditedMessageNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getForegroundColorSpan(Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;Landroid/content/Context;)Landroid/text/style/ForegroundColorSpan;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;->getForegroundColorSpan(Landroid/content/Context;)Landroid/text/style/ForegroundColorSpan;

    move-result-object p0

    return-object p0
.end method

.method private final getForegroundColorSpan(Landroid/content/Context;)Landroid/text/style/ForegroundColorSpan;
    .locals 2

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v1, 0x7f040178

    invoke-static {p1, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    invoke-direct {v0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    return-object v0
.end method

.method private final getRelativeSizeSpan(F)Landroid/text/style/RelativeSizeSpan;
    .locals 1

    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v0, p1}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    return-object v0
.end method

.method public static synthetic getRelativeSizeSpan$default(Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;FILjava/lang/Object;)Landroid/text/style/RelativeSizeSpan;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/high16 p1, 0x3f400000    # 0.75f

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;->getRelativeSizeSpan(F)Landroid/text/style/RelativeSizeSpan;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getEditedString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f121039

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.string.message_edited)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
