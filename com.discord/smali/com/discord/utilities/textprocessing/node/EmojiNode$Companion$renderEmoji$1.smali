.class public final Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;
.super Ljava/lang/Object;
.source "EmojiNode.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $isAnimationEnabled:Z

.field public final synthetic $this_renderEmoji:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

.field private final context:Landroid/content/Context;

.field private final isAnimationEnabled:Z


# direct methods
.method public constructor <init>(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;->$this_renderEmoji:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iput-boolean p2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;->$isAnimationEnabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;->context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;->isAnimationEnabled:Z

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isAnimationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion$renderEmoji$1;->isAnimationEnabled:Z

    return v0
.end method
