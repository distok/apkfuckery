.class public final Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;
.super Lx/m/c/k;
.source "Parsers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->getOnClickUrl()Lkotlin/jvm/functions/Function3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function3<",
        "Landroid/content/Context;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;->this$0:Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/Context;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;->invoke(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "url"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;->this$0:Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;

    iget-object v0, v0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->$onClickListener:Lkotlin/jvm/functions/Function3;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1$onClickUrl$1;->this$0:Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;

    iget-boolean v0, v0, Lcom/discord/utilities/textprocessing/Parsers$parseMarkdown$renderContext$1;->$changeLogRules:Z

    if-eqz v0, :cond_1

    sget-object v1, Lcom/discord/utilities/uri/UriHandler;->INSTANCE:Lcom/discord/utilities/uri/UriHandler;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/uri/UriHandler;->handle$default(Lcom/discord/utilities/uri/UriHandler;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-static {p1, p2, p3}, Lcom/discord/utilities/uri/UriHandler;->handleOrUntrusted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
