.class public final Lcom/discord/utilities/textprocessing/Rules;
.super Ljava/lang/Object;
.source "Rules.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;,
        Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;,
        Lcom/discord/utilities/textprocessing/Rules$HeaderLineClassedRule;,
        Lcom/discord/utilities/textprocessing/Rules$MarkdownListItemRule;
    }
.end annotation


# static fields
.field private static final HOOKED_LINK:Ljava/lang/String; = "^\\$\\[((?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*)?]\\(\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+[\'\"]([\\s\\S]*?)[\'\"])?\\s*\\)"

.field public static final INSTANCE:Lcom/discord/utilities/textprocessing/Rules;

.field private static final LINK:Ljava/lang/String; = "^\\[((?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*)]\\(\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+[\'\"]([\\s\\S]*?)[\'\"])?\\s*\\)"

.field private static final PATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static final PATTERN_BLOCK_QUOTE:Ljava/util/regex/Pattern;

.field private static final PATTERN_CHANNEL_MENTION:Ljava/util/regex/Pattern;

.field private static final PATTERN_CUSTOM_EMOJI:Ljava/util/regex/Pattern;

.field private static final PATTERN_HOOKED_LINK:Ljava/util/regex/Pattern;

.field private static final PATTERN_MASKED_LINK:Ljava/util/regex/Pattern;

.field private static final PATTERN_MENTION:Ljava/util/regex/Pattern;

.field private static final PATTERN_NAMED_EMOJI:Ljava/util/regex/Pattern;

.field private static final PATTERN_NON_MARKDOWN:Ljava/util/regex/Pattern;

.field private static final PATTERN_ROLE_MENTION:Ljava/util/regex/Pattern;

.field private static final PATTERN_SOFT_HYPHEN:Ljava/util/regex/Pattern;

.field private static final PATTERN_SPOILER:Ljava/util/regex/Pattern;

.field private static final PATTERN_UNESCAPE_EMOTICON:Ljava/util/regex/Pattern;

.field private static final PATTERN_UNICODE_EMOJI$delegate:Lkotlin/Lazy;

.field private static final PATTERN_URL:Ljava/util/regex/Pattern;

.field private static final PATTERN_URL_NO_EMBED:Ljava/util/regex/Pattern;

.field public static final REGEX_CUSTOM_EMOJI:Ljava/lang/String; = "<(a)?:([a-zA-Z_0-9]+):(\\d+)>"

.field private static final REGEX_LINK_HREF_AND_TITLE:Ljava/lang/String; = "\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+[\'\"]([\\s\\S]*?)[\'\"])?\\s*"

.field private static final REGEX_LINK_INSIDE:Ljava/lang/String; = "(?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*"

.field private static final REGEX_URL:Ljava/lang/String; = "(https?://[^\\s<]+[^<.,:;\"\')\\]\\s])"

.field private static emojiDataProvider:Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules;

    invoke-direct {v0}, Lcom/discord/utilities/textprocessing/Rules;-><init>()V

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules;

    const-string v0, "^(?: *>>> +(.*)| *>(?!>>) +([^\\n]*\\n?))"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_BLOCK_QUOTE:Ljava/util/regex/Pattern;

    const-string v0, "^<#(\\d+)>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_CHANNEL_MENTION:Ljava/util/regex/Pattern;

    const-string v0, "^<@&(\\d+)>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_ROLE_MENTION:Ljava/util/regex/Pattern;

    const-string v0, "^<@!?(\\d+)>|^@(everyone|here)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_MENTION:Ljava/util/regex/Pattern;

    sget-object v0, Lcom/discord/utilities/textprocessing/Rules$PATTERN_UNICODE_EMOJI$2;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$PATTERN_UNICODE_EMOJI$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_UNICODE_EMOJI$delegate:Lkotlin/Lazy;

    const-string v0, "^<(a)?:([a-zA-Z_0-9]+):(\\d+)>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_CUSTOM_EMOJI:Ljava/util/regex/Pattern;

    const-string v0, "^:([^\\s:]+?(?:::skin-tone-\\d)?):"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_NAMED_EMOJI:Ljava/util/regex/Pattern;

    const-string v0, "^(\u00af\\\\_\\(\u30c4\\)_/\u00af)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_UNESCAPE_EMOTICON:Ljava/util/regex/Pattern;

    const-string v0, "^(https?://[^\\s<]+[^<.,:;\"\')\\]\\s])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_URL:Ljava/util/regex/Pattern;

    const-string v0, "^\\[((?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*)]\\(\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+[\'\"]([\\s\\S]*?)[\'\"])?\\s*\\)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_MASKED_LINK:Ljava/util/regex/Pattern;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Character;

    const/16 v1, 0x5b

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/16 v1, 0x5d

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS:Ljava/util/Set;

    const-string v0, "^<(https?://[^\\s<]+[^<.,:;\"\')\\]\\s])>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_URL_NO_EMBED:Ljava/util/regex/Pattern;

    const-string v0, "^\\u00AD"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_SOFT_HYPHEN:Ljava/util/regex/Pattern;

    const-string v0, "^\\|\\|([\\s\\S]+?)\\|\\|"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_SPOILER:Ljava/util/regex/Pattern;

    const-string v0, "^\\$\\[((?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*)?]\\(\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+[\'\"]([\\s\\S]*?)[\'\"])?\\s*\\)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_HOOKED_LINK:Ljava/util/regex/Pattern;

    const-string v0, "^!!([\\s\\S]+)!!"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_NON_MARKDOWN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getEmojiDataProvider$p(Lcom/discord/utilities/textprocessing/Rules;)Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;
    .locals 0

    sget-object p0, Lcom/discord/utilities/textprocessing/Rules;->emojiDataProvider:Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "emojiDataProvider"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getPATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS$p(Lcom/discord/utilities/textprocessing/Rules;)Ljava/util/Set;
    .locals 0

    sget-object p0, Lcom/discord/utilities/textprocessing/Rules;->PATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic access$replaceEmojiSurrogates(Lcom/discord/utilities/textprocessing/Rules;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/textprocessing/Rules;->replaceEmojiSurrogates(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setEmojiDataProvider$p(Lcom/discord/utilities/textprocessing/Rules;Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;)V
    .locals 0

    sput-object p1, Lcom/discord/utilities/textprocessing/Rules;->emojiDataProvider:Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;

    return-void
.end method

.method private final getPATTERN_UNICODE_EMOJI()Ljava/util/regex/Pattern;
    .locals 1

    sget-object v0, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_UNICODE_EMOJI$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private final replaceEmojiSurrogates(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->emojiDataProvider:Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;

    const/4 v2, 0x0

    const-string v3, "emojiDataProvider"

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;->getUnicodeEmojisPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/discord/utilities/textprocessing/Rules;->emojiDataProvider:Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;

    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;->getUnicodeEmojiSurrogateMap()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/emoji/ModelEmojiUnicode;

    if-eqz v1, :cond_0

    const-string v4, ":"

    invoke-static {v4}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/models/domain/emoji/ModelEmojiUnicode;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-virtual {p1, v0}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "stringBuffer.toString()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_3
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public static final setEmojiDataProvider(Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;)V
    .locals 1

    const-string v0, "emojiDataProvider"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object p0, Lcom/discord/utilities/textprocessing/Rules;->emojiDataProvider:Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;

    return-void
.end method

.method private final toLongOrDefault(Ljava/lang/String;J)J
    .locals 0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lx/s/l;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public static synthetic toLongOrDefault$default(Lcom/discord/utilities/textprocessing/Rules;Ljava/lang/String;JILjava/lang/Object;)J
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const-wide/16 p2, -0x1

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/textprocessing/Rules;->toLongOrDefault(Ljava/lang/String;J)J

    move-result-wide p0

    return-wide p0
.end method


# virtual methods
.method public final createBlockQuoteRule()Lcom/discord/simpleast/core/parser/Rule$BlockRule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;",
            "S::",
            "Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState<",
            "TS;>;>()",
            "Lcom/discord/simpleast/core/parser/Rule$BlockRule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/BlockQuoteNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createBlockQuoteRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_BLOCK_QUOTE:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_BLOCK_QUOTE"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createBlockQuoteRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createBoldColoredRule(I)Lcom/discord/simpleast/core/parser/Rule;
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    sget-object v0, Lf/a/j/b/b/a;->h:Lf/a/j/b/b/a;

    sget-object v0, Lf/a/j/b/b/a;->a:Ljava/util/regex/Pattern;

    const-string v1, "PATTERN_BOLD"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/utilities/textprocessing/Rules$createBoldColoredRule$1;

    invoke-direct {v1, p1}, Lcom/discord/utilities/textprocessing/Rules$createBoldColoredRule$1;-><init>(I)V

    invoke-static {v0, v1}, Lf/a/j/b/b/a;->d(Ljava/util/regex/Pattern;Lkotlin/jvm/functions/Function0;)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object p1

    return-object p1
.end method

.method public final createChannelMentionRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/ChannelMentionNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/ChannelMentionNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createChannelMentionRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_CHANNEL_MENTION:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_CHANNEL_MENTION"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createChannelMentionRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createCodeBlockRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RC::",
            "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;",
            "S::",
            "Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState<",
            "TS;>;>()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TRC;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TRC;>;TS;>;"
        }
    .end annotation

    new-instance v9, Lf/a/j/a/f;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$1;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$1;

    sget-object v2, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$2;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$2;

    sget-object v3, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$3;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$3;

    sget-object v4, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$4;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$4;

    sget-object v5, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$5;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$5;

    sget-object v6, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;

    sget-object v7, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$7;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$7;

    sget-object v8, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$8;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$8;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lf/a/j/a/f;-><init>(Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    sget-object v11, Lf/a/j/a/e;->f:Lf/a/j/a/e;

    const-string v6, "codeStyleProviders"

    invoke-static {v9, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/j/a/i;->f:Lf/a/j/a/i;

    invoke-static {v9, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/discord/simpleast/core/parser/Rule;

    sget-object v1, Lf/a/j/a/i;->c:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_KOTLIN_COMMENTS"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    const/4 v7, 0x1

    const-string v3, "$this$toMatchGroupRule"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lf/a/j/a/d;

    const/4 v8, 0x0

    invoke-direct {v4, v1, v8, v2, v1}, Lf/a/j/a/d;-><init>(Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;Ljava/util/regex/Pattern;)V

    aput-object v4, v0, v8

    sget-object v1, Lf/a/j/a/i;->e:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_KOTLIN_STRINGS"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->c:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lf/a/j/a/d;

    invoke-direct {v4, v1, v8, v2, v1}, Lf/a/j/a/d;-><init>(Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;Ljava/util/regex/Pattern;)V

    aput-object v4, v0, v7

    sget-object v1, Lf/a/j/a/i;->d:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_KOTLIN_ANNOTATION"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->g:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lf/a/j/a/d;

    invoke-direct {v3, v1, v8, v2, v1}, Lf/a/j/a/d;-><init>(Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;Ljava/util/regex/Pattern;)V

    const/4 v1, 0x2

    aput-object v3, v0, v1

    sget-object v1, Lf/a/j/a/i$a;->b:Lf/a/j/a/i$a$a;

    invoke-static {v9, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/a/j/a/h;

    sget-object v3, Lf/a/j/a/i$a;->a:Ljava/util/regex/Pattern;

    const-string v4, "PATTERN_KOTLIN_FIELD"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1, v9, v3}, Lf/a/j/a/h;-><init>(Lf/a/j/a/i$a$a;Lf/a/j/a/f;Ljava/util/regex/Pattern;)V

    const/4 v1, 0x3

    aput-object v2, v0, v1

    sget-object v1, Lf/a/j/a/i$b;->b:Lf/a/j/a/i$b$a;

    invoke-static {v9, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/a/j/a/j;

    sget-object v3, Lf/a/j/a/i$b;->a:Ljava/util/regex/Pattern;

    invoke-direct {v2, v1, v9, v3}, Lf/a/j/a/j;-><init>(Lf/a/j/a/i$b$a;Lf/a/j/a/f;Ljava/util/regex/Pattern;)V

    const/4 v1, 0x4

    aput-object v2, v0, v1

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string v10, "class"

    const-string v0, "object"

    const-string v1, "interface"

    filled-new-array {v0, v10, v1}, [Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lf/a/j/a/i;->b:[Ljava/lang/String;

    sget-object v0, Lf/a/j/a/i;->a:[Ljava/lang/String;

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, [Ljava/lang/String;

    move-object v0, v11

    move-object v1, v9

    invoke-virtual/range {v0 .. v5}, Lf/a/j/a/e;->a(Lf/a/j/a/f;Ljava/util/List;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/simpleast/core/parser/Rule;

    const-string v13, "//"

    invoke-virtual {v11, v13}, Lf/a/j/a/e;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v14, "createSingleLineCommentPattern(\"//\")"

    invoke-static {v1, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v7}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    aput-object v1, v0, v8

    const-string v15, "^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)"

    invoke-static {v15}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v5, "Pattern.compile(\"\"\"^\"[\\s\u2026*?(?<!\\\\)\"(?=\\W|\\s|$)\"\"\")"

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->c:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v7}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string v0, "message|enum|extend|service"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "true|false"

    const-string/jumbo v1, "string|bool|double|float|bytes"

    const-string v4, "int32|uint32|sint32|int64|unit64|sint64"

    const-string v7, "map"

    filled-new-array {v0, v1, v4, v7}, [Ljava/lang/String;

    move-result-object v4

    const-string v0, "required|repeated|optional|option|oneof|default|reserved"

    const-string v1, "package|import"

    const-string v7, "rpc|returns"

    filled-new-array {v0, v1, v7}, [Ljava/lang/String;

    move-result-object v7

    move-object v0, v11

    move-object v1, v9

    move-object/from16 v16, v5

    move-object v5, v7

    invoke-virtual/range {v0 .. v5}, Lf/a/j/a/e;->a(Lf/a/j/a/f;Ljava/util/List;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/simpleast/core/parser/Rule;

    const-string v1, "#"

    invoke-virtual {v11, v1}, Lf/a/j/a/e;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "createSingleLineCommentPattern(\"#\")"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    const/4 v3, 0x1

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static {v15}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    move-object/from16 v5, v16

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->c:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "^\'[\\s\\S]*?(?<!\\\\)\'(?=\\W|\\s|$)"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "Pattern.compile(\"\"\"^\'[\\s\u2026*?(?<!\\\\)\'(?=\\W|\\s|$)\"\"\")"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->c:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "^@(\\w+)"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "Pattern.compile(\"\"\"^@(\\w+)\"\"\")"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->g:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string v0, "def"

    const-string v1, "lambda"

    filled-new-array {v10, v0, v1}, [Ljava/lang/String;

    move-result-object v3

    const-string v0, "True|False|None"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v4

    const-string v16, "from|import|global|nonlocal"

    const-string v17, "async|await|class|self|cls|def|lambda"

    const-string v18, "for|while|if|else|elif|break|continue|return"

    const-string/jumbo v19, "try|except|finally|raise|pass|yeild"

    const-string v20, "in|as|is|del"

    const-string v21, "and|or|not|assert"

    filled-new-array/range {v16 .. v21}, [Ljava/lang/String;

    move-result-object v10

    move-object v0, v11

    move-object v1, v9

    move-object/from16 v22, v5

    move-object v5, v10

    invoke-virtual/range {v0 .. v5}, Lf/a/j/a/e;->a(Lf/a/j/a/f;Ljava/util/List;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/simpleast/core/parser/Rule;

    invoke-virtual {v11, v13}, Lf/a/j/a/e;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-static {v1, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    const/4 v3, 0x1

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static {v15}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    move-object/from16 v2, v22

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->c:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "^#!?\\[.*?\\]\\n"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "Pattern.compile(\"\"\"^#!?\\[.*?\\]\\n\"\"\")"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v9, Lf/a/j/a/f;->g:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v11, v1, v8, v2, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string/jumbo v0, "struct"

    const-string/jumbo v1, "trait"

    const-string v3, "mod"

    filled-new-array {v0, v1, v3}, [Ljava/lang/String;

    move-result-object v3

    const-string v0, "Self|Result|Ok|Err|Option|None|Some"

    const-string v1, "Copy|Clone|Eq|Hash|Send|Sync|Sized|Debug|Display"

    const-string v4, "Arc|Rc|Box|Pin|Future"

    const-string/jumbo v5, "true|false|bool|usize|i64|u64|u32|i32|str|String"

    filled-new-array {v0, v1, v4, v5}, [Ljava/lang/String;

    move-result-object v4

    const-string v0, "let|mut|static|const|unsafe"

    const-string v1, "crate|mod|extern|pub|pub(super)|use"

    const-string/jumbo v5, "struct|enum|trait|type|where|impl|dyn|async|await|move|self|fn"

    const-string v13, "for|while|loop|if|else|match|break|continue|return|try"

    const-string v14, "in|as|ref"

    filled-new-array {v0, v1, v5, v13, v14}, [Ljava/lang/String;

    move-result-object v5

    move-object v0, v11

    move-object v1, v9

    invoke-virtual/range {v0 .. v5}, Lf/a/j/a/e;->a(Lf/a/j/a/f;Ljava/util/List;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/discord/simpleast/core/parser/Rule;

    sget-object v2, Lf/a/j/a/k;->c:Lf/a/j/a/k;

    sget-object v3, Lf/a/j/a/k;->a:Ljava/util/regex/Pattern;

    iget-object v4, v9, Lf/a/j/a/f;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    const/4 v5, 0x1

    invoke-static {v11, v3, v8, v4, v5}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v3

    aput-object v3, v1, v8

    invoke-static {v9, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lf/a/j/a/l;

    sget-object v4, Lf/a/j/a/k;->b:Ljava/util/regex/Pattern;

    invoke-direct {v3, v2, v9, v4}, Lf/a/j/a/l;-><init>(Lf/a/j/a/k;Lf/a/j/a/f;Ljava/util/regex/Pattern;)V

    aput-object v3, v1, v5

    sget-object v2, Lf/a/j/a/e;->c:Ljava/util/regex/Pattern;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v11, v2, v8, v3, v4}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    sget-object v2, Lf/a/j/a/e;->d:Ljava/util/regex/Pattern;

    const/4 v4, 0x3

    invoke-static {v11, v2, v8, v3, v4}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/16 v2, 0xb

    new-array v2, v2, [Lkotlin/Pair;

    new-instance v3, Lkotlin/Pair;

    const-string v4, "kt"

    invoke-direct {v3, v4, v12}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v2, v8

    new-instance v3, Lkotlin/Pair;

    const-string v4, "kotlin"

    invoke-direct {v3, v4, v12}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x1

    aput-object v3, v2, v4

    new-instance v3, Lkotlin/Pair;

    const-string v4, "protobuf"

    invoke-direct {v3, v4, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x2

    aput-object v3, v2, v4

    new-instance v3, Lkotlin/Pair;

    const-string v4, "proto"

    invoke-direct {v3, v4, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x3

    aput-object v3, v2, v4

    new-instance v3, Lkotlin/Pair;

    const-string v4, "pb"

    invoke-direct {v3, v4, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x4

    aput-object v3, v2, v4

    new-instance v3, Lkotlin/Pair;

    const-string v4, "py"

    invoke-direct {v3, v4, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x5

    aput-object v3, v2, v4

    const/4 v3, 0x6

    new-instance v4, Lkotlin/Pair;

    const-string v5, "python"

    invoke-direct {v4, v5, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lkotlin/Pair;

    const-string v5, "rs"

    invoke-direct {v4, v5, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-instance v4, Lkotlin/Pair;

    const-string v5, "rust"

    invoke-direct {v4, v5, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v2, v3

    const/16 v0, 0x9

    new-instance v3, Lkotlin/Pair;

    const-string/jumbo v4, "xml"

    invoke-direct {v3, v4, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v2, v0

    const/16 v0, 0xa

    new-instance v3, Lkotlin/Pair;

    const-string v4, "http"

    invoke-direct {v3, v4, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v2, v0

    invoke-static {v2}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v12

    iget-object v13, v9, Lf/a/j/a/f;->a:Lcom/discord/simpleast/core/node/StyleNode$a;

    sget-object v14, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$1;

    const-string/jumbo v0, "textStyleProvider"

    invoke-static {v13, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "languageMap"

    invoke-static {v12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "wrapperNodeProvider"

    invoke-static {v14, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/j/a/a;

    sget-object v15, Lf/a/j/a/e;->a:Ljava/util/regex/Pattern;

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Lf/a/j/a/a;-><init>(Lf/a/j/a/e;Ljava/util/Map;Lcom/discord/simpleast/core/node/StyleNode$a;Lkotlin/jvm/functions/Function3;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createCustomEmojiRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/EmojiNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createCustomEmojiRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_CUSTOM_EMOJI:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_CUSTOM_EMOJI"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createCustomEmojiRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createHookedLinkRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createHookedLinkRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_HOOKED_LINK:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_HOOKED_LINK"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createHookedLinkRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createInlineCodeRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RC::",
            "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;",
            "S::",
            "Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState<",
            "TS;>;>()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TRC;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TRC;>;TS;>;"
        }
    .end annotation

    sget-object v0, Lf/a/j/a/e;->f:Lf/a/j/a/e;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules$createInlineCodeRule$1;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createInlineCodeRule$1;

    sget-object v2, Lcom/discord/utilities/textprocessing/Rules$createInlineCodeRule$2;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createInlineCodeRule$2;

    const-string/jumbo v3, "textStyleProvider"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "bgStyleProvider"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lf/a/j/a/c;

    sget-object v4, Lf/a/j/a/e;->b:Ljava/util/regex/Pattern;

    invoke-direct {v3, v0, v1, v2, v4}, Lf/a/j/a/c;-><init>(Lf/a/j/a/e;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Ljava/util/regex/Pattern;)V

    return-object v3
.end method

.method public final createMaskedLinkRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/UrlNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createMaskedLinkRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_MASKED_LINK:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_MASKED_LINK"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createMaskedLinkRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createNamedEmojiRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createNamedEmojiRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_NAMED_EMOJI:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_NAMED_EMOJI"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createNamedEmojiRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createNonMarkdownRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    sget-object v0, Lf/a/j/a/e;->f:Lf/a/j/a/e;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_NON_MARKDOWN:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_NON_MARKDOWN"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v0, v1, v2, v3, v4}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v0

    return-object v0
.end method

.method public final createRoleMentionRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/RoleMentionNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/RoleMentionNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createRoleMentionRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_ROLE_MENTION:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_ROLE_MENTION"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createRoleMentionRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createSoftHyphenRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lf/a/j/b/a/a<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createSoftHyphenRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_SOFT_HYPHEN:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_SOFT_HYPHEN"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createSoftHyphenRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createSpoilerRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/SpoilerNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createSpoilerRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_SPOILER:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_SPOILER"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createSpoilerRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createStrikethroughColoredRule(I)Lcom/discord/simpleast/core/parser/Rule;
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    sget-object v0, Lf/a/j/b/b/a;->h:Lf/a/j/b/b/a;

    sget-object v0, Lf/a/j/b/b/a;->c:Ljava/util/regex/Pattern;

    const-string v1, "PATTERN_STRIKETHRU"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/utilities/textprocessing/Rules$createStrikethroughColoredRule$1;

    invoke-direct {v1, p1}, Lcom/discord/utilities/textprocessing/Rules$createStrikethroughColoredRule$1;-><init>(I)V

    invoke-static {v0, v1}, Lf/a/j/b/b/a;->d(Ljava/util/regex/Pattern;Lkotlin/jvm/functions/Function0;)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object p1

    return-object p1
.end method

.method public final createTextReplacementRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createTextReplacementRule$1;

    sget-object v1, Lf/a/j/b/b/a;->h:Lf/a/j/b/b/a;

    sget-object v1, Lf/a/j/b/b/a;->e:Ljava/util/regex/Pattern;

    const-string v2, "SimpleMarkdownRules.PATTERN_TEXT"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createTextReplacementRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createUnescapeEmoticonRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lf/a/j/b/a/a<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createUnescapeEmoticonRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_UNESCAPE_EMOTICON:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_UNESCAPE_EMOTICON"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createUnescapeEmoticonRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createUnicodeEmojiRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createUnicodeEmojiRule$1;

    invoke-direct {p0}, Lcom/discord/utilities/textprocessing/Rules;->getPATTERN_UNICODE_EMOJI()Ljava/util/regex/Pattern;

    move-result-object v1

    const-string v2, "PATTERN_UNICODE_EMOJI"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createUnicodeEmojiRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createUrlNoEmbedRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/UrlNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createUrlNoEmbedRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_URL_NO_EMBED:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_URL_NO_EMBED"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createUrlNoEmbedRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createUrlRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/utilities/textprocessing/node/UrlNode<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createUrlRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_URL:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_URL"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createUrlRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method public final createUserMentionRule()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/discord/utilities/textprocessing/node/UserMentionNode$RenderContext;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TT;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TT;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createUserMentionRule$1;

    sget-object v1, Lcom/discord/utilities/textprocessing/Rules;->PATTERN_MENTION:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_MENTION"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/discord/utilities/textprocessing/Rules$createUserMentionRule$1;-><init>(Lcom/discord/utilities/textprocessing/Rules;Ljava/util/regex/Pattern;)V

    return-object v0
.end method
