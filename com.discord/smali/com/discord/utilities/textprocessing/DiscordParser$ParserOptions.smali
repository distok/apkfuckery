.class public final enum Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;
.super Ljava/lang/Enum;
.source "DiscordParser.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/textprocessing/DiscordParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParserOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

.field public static final enum ALLOW_MASKED_LINKS:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

.field public static final enum DEFAULT:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

.field public static final enum REPLY:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    new-instance v1, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    const-string v2, "DEFAULT"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->DEFAULT:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    const-string v2, "ALLOW_MASKED_LINKS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->ALLOW_MASKED_LINKS:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    const-string v2, "REPLY"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->REPLY:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->$VALUES:[Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;
    .locals 1

    const-class v0, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;
    .locals 1

    sget-object v0, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->$VALUES:[Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    invoke-virtual {v0}, [Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    return-object v0
.end method
