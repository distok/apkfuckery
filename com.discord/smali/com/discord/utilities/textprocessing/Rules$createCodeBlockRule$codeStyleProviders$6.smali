.class public final Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;
.super Ljava/lang/Object;
.source "Rules.kt"

# interfaces
.implements Lcom/discord/simpleast/core/node/StyleNode$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/textprocessing/Rules;->createCodeBlockRule()Lcom/discord/simpleast/core/parser/Rule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RC:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/discord/simpleast/core/node/StyleNode$a<",
        "TRC;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;

    invoke-direct {v0}, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;-><init>()V

    sput-object v0, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;->INSTANCE:Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Lcom/discord/utilities/textprocessing/node/BasicRenderContext;)Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRC;)",
            "Ljava/lang/Iterable<",
            "*>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-interface {p1}, Lcom/discord/utilities/textprocessing/node/BasicRenderContext;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f13012b

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Iterable;
    .locals 0

    check-cast p1, Lcom/discord/utilities/textprocessing/node/BasicRenderContext;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/textprocessing/Rules$createCodeBlockRule$codeStyleProviders$6;->get(Lcom/discord/utilities/textprocessing/node/BasicRenderContext;)Ljava/lang/Iterable;

    move-result-object p1

    return-object p1
.end method
