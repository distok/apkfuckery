.class public final Lcom/discord/utilities/textprocessing/DiscordParser;
.super Ljava/lang/Object;
.source "DiscordParser.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/textprocessing/DiscordParser;

.field private static final MASKED_LINK_PARSER:Lcom/discord/simpleast/core/parser/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation
.end field

.field private static final REPLY_PARSER:Lcom/discord/simpleast/core/parser/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation
.end field

.field private static final SAFE_LINK_PARSER:Lcom/discord/simpleast/core/parser/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/discord/utilities/textprocessing/DiscordParser;

    invoke-direct {v0}, Lcom/discord/utilities/textprocessing/DiscordParser;-><init>()V

    sput-object v0, Lcom/discord/utilities/textprocessing/DiscordParser;->INSTANCE:Lcom/discord/utilities/textprocessing/DiscordParser;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v0, v1, v0, v2, v3}, Lcom/discord/utilities/textprocessing/Parsers;->createParser$default(ZZZILjava/lang/Object;)Lcom/discord/simpleast/core/parser/Parser;

    move-result-object v4

    sput-object v4, Lcom/discord/utilities/textprocessing/DiscordParser;->SAFE_LINK_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    invoke-static {v1, v1, v0, v2, v3}, Lcom/discord/utilities/textprocessing/Parsers;->createParser$default(ZZZILjava/lang/Object;)Lcom/discord/simpleast/core/parser/Parser;

    move-result-object v2

    sput-object v2, Lcom/discord/utilities/textprocessing/DiscordParser;->MASKED_LINK_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    invoke-static {v0, v1, v0}, Lcom/discord/utilities/textprocessing/Parsers;->createParser(ZZZ)Lcom/discord/simpleast/core/parser/Parser;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/textprocessing/DiscordParser;->REPLY_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final parseChannelMessage(Landroid/content/Context;Ljava/lang/String;Lcom/discord/utilities/textprocessing/MessageRenderContext;Lcom/discord/utilities/textprocessing/MessagePreprocessor;Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 6

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageRenderContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preprocessor"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parserOptions"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Enum;->ordinal()I

    move-result p4

    if-eqz p4, :cond_2

    const/4 v0, 0x1

    if-eq p4, v0, :cond_1

    const/4 v0, 0x2

    if-ne p4, v0, :cond_0

    sget-object p4, Lcom/discord/utilities/textprocessing/DiscordParser;->REPLY_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_1
    sget-object p4, Lcom/discord/utilities/textprocessing/DiscordParser;->MASKED_LINK_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    goto :goto_0

    :cond_2
    sget-object p4, Lcom/discord/utilities/textprocessing/DiscordParser;->SAFE_LINK_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    :goto_0
    move-object v0, p4

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    const-string p1, ""

    :goto_1
    move-object v1, p1

    sget-object p1, Lcom/discord/utilities/textprocessing/MessageParseState;->Companion:Lcom/discord/utilities/textprocessing/MessageParseState$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->process(Ljava/util/Collection;)V

    if-eqz p5, :cond_4

    new-instance p3, Lcom/discord/utilities/textprocessing/node/EditedMessageNode;

    invoke-direct {p3, p0}, Lcom/discord/utilities/textprocessing/node/EditedMessageNode;-><init>(Landroid/content/Context;)V

    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance p0, Lcom/discord/utilities/textprocessing/node/ZeroSpaceWidthNode;

    invoke-direct {p0}, Lcom/discord/utilities/textprocessing/node/ZeroSpaceWidthNode;-><init>()V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p1, p2}, Lcom/discord/utilities/textprocessing/AstRenderer;->render(Ljava/util/Collection;Ljava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object p0

    return-object p0
.end method
