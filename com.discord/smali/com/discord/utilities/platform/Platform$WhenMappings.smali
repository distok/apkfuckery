.class public final synthetic Lcom/discord/utilities/platform/Platform$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/discord/utilities/platform/Platform;->values()[Lcom/discord/utilities/platform/Platform;

    const/16 v0, 0x12

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/utilities/platform/Platform$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/platform/Platform;->FACEBOOK:Lcom/discord/utilities/platform/Platform;

    const/4 v1, 0x1

    const/4 v2, 0x2

    aput v1, v0, v2

    sget-object v1, Lcom/discord/utilities/platform/Platform;->SPOTIFY:Lcom/discord/utilities/platform/Platform;

    const/16 v1, 0xa

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/platform/Platform;->STEAM:Lcom/discord/utilities/platform/Platform;

    const/16 v1, 0xb

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/platform/Platform;->YOUTUBE:Lcom/discord/utilities/platform/Platform;

    const/16 v1, 0xf

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/platform/Platform;->REDDIT:Lcom/discord/utilities/platform/Platform;

    const/4 v1, 0x5

    const/4 v2, 0x7

    aput v1, v0, v2

    sget-object v1, Lcom/discord/utilities/platform/Platform;->TWITTER:Lcom/discord/utilities/platform/Platform;

    const/16 v1, 0xd

    const/4 v3, 0x6

    aput v3, v0, v1

    sget-object v1, Lcom/discord/utilities/platform/Platform;->TWITCH:Lcom/discord/utilities/platform/Platform;

    const/16 v1, 0xc

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/platform/Platform;->GITHUB:Lcom/discord/utilities/platform/Platform;

    const/16 v1, 0x10

    const/16 v2, 0x8

    aput v2, v0, v1

    return-void
.end method
