.class public final Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;
.super Ljava/lang/Object;
.source "ContentResizingCoordinatorLayout.kt"

# interfaces
.implements Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->onMeasure(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $heightMeasureSpec:I

.field public final synthetic $widthMeasureSpec:I

.field public final synthetic this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;II)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    iput p2, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->$widthMeasureSpec:I

    iput p3, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->$heightMeasureSpec:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onOffsetChanged(Lcom/google/android/material/appbar/AppBarLayout;I)V
    .locals 6

    iget-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-static {p1, p2}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$setCurrentVerticalOffset$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;I)V

    iget-object v0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-static {v0}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$getAppBarLayout$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    iget-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    iget v4, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->$widthMeasureSpec:I

    iget v5, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;->$heightMeasureSpec:I

    move v1, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$updateContentLayoutParams(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;IIIII)V

    return-void
.end method
