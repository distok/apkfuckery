.class public final Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;
.super Landroidx/recyclerview/widget/RecyclerView$SimpleOnItemTouchListener;
.source "StickyHeaderItemDecoration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/views/StickyHeaderItemDecoration;->blockClicks(Landroidx/recyclerview/widget/RecyclerView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/views/StickyHeaderItemDecoration;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$SimpleOnItemTouchListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 5

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    if-eq v0, v1, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object p1, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-static {p1}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$resetActionDownCoordinates(Lcom/discord/utilities/views/StickyHeaderItemDecoration;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-static {v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$getActionDownRawX$p(Lcom/discord/utilities/views/StickyHeaderItemDecoration;)Ljava/lang/Float;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-static {v3}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$getActionDownRawY$p(Lcom/discord/utilities/views/StickyHeaderItemDecoration;)Ljava/lang/Float;

    move-result-object v3

    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v4, 0x7f070096

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    cmpg-float p1, v3, p1

    if-gez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-static {v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$resetActionDownCoordinates(Lcom/discord/utilities/views/StickyHeaderItemDecoration;)V

    if-eqz p1, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iget-object p2, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-static {p2}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$getStickyHeaderBottom$p(Lcom/discord/utilities/views/StickyHeaderItemDecoration;)F

    move-result p2

    cmpg-float p1, p1, p2

    if-gtz p1, :cond_4

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$setActionDownRawX$p(Lcom/discord/utilities/views/StickyHeaderItemDecoration;Ljava/lang/Float;)V

    iget-object p1, p0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$blockClicks$1;->this$0:Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->access$setActionDownRawY$p(Lcom/discord/utilities/views/StickyHeaderItemDecoration;Ljava/lang/Float;)V

    :cond_4
    :goto_1
    const/4 v1, 0x0

    :goto_2
    return v1
.end method
