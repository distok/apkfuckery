.class public final synthetic Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$1;
.super Lx/m/c/m;
.source "ContentResizingCoordinatorLayout.kt"


# direct methods
.method public constructor <init>(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)V
    .locals 6

    const-class v2, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    const-string v3, "appBarLayout"

    const-string v4, "getAppBarLayout()Lcom/google/android/material/appbar/AppBarLayout;"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lx/m/c/m;-><init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-static {v0}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$getAppBarLayout$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    check-cast p1, Lcom/google/android/material/appbar/AppBarLayout;

    invoke-static {v0, p1}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$setAppBarLayout$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;Lcom/google/android/material/appbar/AppBarLayout;)V

    return-void
.end method
