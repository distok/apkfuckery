.class public final Lcom/discord/utilities/views/ViewVisibilityObserverProvider;
.super Ljava/lang/Object;
.source "ViewVisibilityObserverProvider.kt"


# static fields
.field public static final INLINE_VOICE_FEATURE:Ljava/lang/String; = "INLINE_VOICE_FEATURE"

.field public static final INSTANCE:Lcom/discord/utilities/views/ViewVisibilityObserverProvider;

.field private static final featureNameToObserverRefMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/discord/utilities/views/ViewVisibilityObserver;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;

    invoke-direct {v0}, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;-><init>()V

    sput-object v0, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;->INSTANCE:Lcom/discord/utilities/views/ViewVisibilityObserverProvider;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;->featureNameToObserverRefMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/String;)Lcom/discord/utilities/views/ViewVisibilityObserver;
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "featureName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;->featureNameToObserverRefMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/views/ViewVisibilityObserver;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Lcom/discord/utilities/views/ViewVisibilityObserver;

    invoke-direct {v1}, Lcom/discord/utilities/views/ViewVisibilityObserver;-><init>()V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v1
.end method
