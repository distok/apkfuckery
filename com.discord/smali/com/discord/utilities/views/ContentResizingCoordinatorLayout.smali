.class public final Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;
.super Landroidx/coordinatorlayout/widget/CoordinatorLayout;
.source "ContentResizingCoordinatorLayout.kt"


# instance fields
.field private appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

.field private content:Landroid/view/View;

.field private currentVerticalOffset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$getAppBarLayout$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)Lcom/google/android/material/appbar/AppBarLayout;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "appBarLayout"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getCurrentVerticalOffset$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)I
    .locals 0

    iget p0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->currentVerticalOffset:I

    return p0
.end method

.method public static final synthetic access$setAppBarLayout$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;Lcom/google/android/material/appbar/AppBarLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    return-void
.end method

.method public static final synthetic access$setCurrentVerticalOffset$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;I)V
    .locals 0

    iput p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->currentVerticalOffset:I

    return-void
.end method

.method public static final synthetic access$updateContentLayoutParams(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;IIIII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->updateContentLayoutParams(IIIII)V

    return-void
.end method

.method private final updateContentLayoutParams(IIIII)V
    .locals 2

    add-int/2addr p2, p1

    iget-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->content:Landroid/view/View;

    const/4 v0, 0x0

    const-string v1, "content"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    sub-int/2addr p3, p2

    iput p3, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object p2, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->content:Landroid/view/View;

    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->content:Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p4, p5}, Landroid/view/ViewGroup;->measureChild(Landroid/view/View;II)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public onMeasure(II)V
    .locals 8

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    iget-object v0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_5

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Lcom/google/android/material/appbar/AppBarLayout;

    iput-object v1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.view.View"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->content:Landroid/view/View;

    iget-object v1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    const/4 v6, 0x0

    const-string v7, "appBarLayout"

    if-eqz v1, :cond_4

    invoke-virtual {p0, v1, p1, p2}, Landroid/view/ViewGroup;->measureChild(Landroid/view/View;II)V

    iget-object v1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    const/4 v1, 0x0

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->updateContentLayoutParams(IIIII)V

    iget-object v0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$2;-><init>(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/material/appbar/AppBarLayout;->addOnOffsetChangedListener(Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;)V

    iget-object v0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->appBarLayout:Lcom/google/android/material/appbar/AppBarLayout;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;-><init>(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;II)V

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addOnHeightChangedListener(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V

    goto :goto_0

    :cond_0
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_1
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_2
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_3
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_4
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_5
    :goto_0
    invoke-super {p0, p1, p2}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onMeasure(II)V

    return-void
.end method
