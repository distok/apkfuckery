.class public final Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;
.super Lx/m/c/k;
.source "ContentResizingCoordinatorLayout.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->onMeasure(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $heightMeasureSpec:I

.field public final synthetic $widthMeasureSpec:I

.field public final synthetic this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;II)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    iput p2, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->$widthMeasureSpec:I

    iput p3, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->$heightMeasureSpec:I

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 6

    iget-object v0, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-static {v0}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$getCurrentVerticalOffset$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)I

    move-result v1

    iget-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-static {p1}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$getAppBarLayout$p(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;)Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    iget-object p1, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->this$0:Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    iget v4, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->$widthMeasureSpec:I

    iget v5, p0, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout$onMeasure$3;->$heightMeasureSpec:I

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;->access$updateContentLayoutParams(Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;IIIII)V

    return-void
.end method
