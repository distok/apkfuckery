.class public final Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;
.super Lx/m/c/k;
.source "ShareUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3;->call(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/utilities/ChannelShortcutInfo;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;

    invoke-direct {v0}, Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;-><init>()V

    sput-object v0, Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;->INSTANCE:Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/utilities/ChannelShortcutInfo;
    .locals 3

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/ChannelShortcutInfo;

    const/16 v1, 0x14

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/discord/utilities/ChannelShortcutInfo;-><init>(Lcom/discord/models/domain/ModelChannel;IZ)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5;->invoke(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/utilities/ChannelShortcutInfo;

    move-result-object p1

    return-object p1
.end method
