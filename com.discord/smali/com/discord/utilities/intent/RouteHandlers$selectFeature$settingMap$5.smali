.class public final Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;
.super Lx/m/c/k;
.source "RouteHandlers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/intent/RouteHandlers;->selectFeature(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/fragment/app/FragmentActivity;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;

    invoke-direct {v0}, Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;-><init>()V

    sput-object v0, Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;->INSTANCE:Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/intent/RouteHandlers$selectFeature$settingMap$5;->invoke(Landroidx/fragment/app/FragmentActivity;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroidx/fragment/app/FragmentActivity;)V
    .locals 9

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->Companion:Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "Deeplink"

    const/4 v6, 0x0

    const/16 v7, 0x16

    const/4 v8, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;->show$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;Landroid/content/Context;Lcom/discord/widgets/guilds/create/StockGuildTemplate;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method
