.class public final Lcom/discord/utilities/intent/IntentUtils;
.super Ljava/lang/Object;
.source "IntentUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/intent/IntentUtils$RouteBuilders;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

.field private static final pathRouterMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lkotlin/text/Regex;",
            "Lkotlin/jvm/functions/Function3<",
            "Landroid/net/Uri;",
            "Lkotlin/text/MatchResult;",
            "Landroid/content/Context;",
            "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/discord/utilities/intent/IntentUtils;

    invoke-direct {v0}, Lcom/discord/utilities/intent/IntentUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    const/16 v0, 0xd

    new-array v0, v0, [Lkotlin/Pair;

    sget-object v1, Lf/a/b/q0/b;->E:Lf/a/b/q0/b;

    sget-object v1, Lf/a/b/q0/b;->t:Lkotlin/text/Regex;

    sget-object v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$1;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$1;

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    aput-object v3, v0, v1

    sget-object v1, Lf/a/b/q0/b;->u:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$2;

    sget-object v3, Lcom/discord/utilities/intent/RouteHandlers;->INSTANCE:Lcom/discord/utilities/intent/RouteHandlers;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$2;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x1

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->w:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$3;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$3;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x2

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->x:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$4;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$4;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x3

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->y:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$5;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$5;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x4

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->z:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$6;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$6;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x5

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->s:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$7;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$7;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x6

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->v:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$8;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$8;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x7

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->B:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$9;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$9;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0x8

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->C:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$10;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$10;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0x9

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->D:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$11;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$11;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0xa

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->A:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$12;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$12;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0xb

    aput-object v4, v0, v1

    sget-object v1, Lf/a/b/q0/b;->m:Lkotlin/text/Regex;

    new-instance v2, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$13;

    invoke-direct {v2, v3}, Lcom/discord/utilities/intent/IntentUtils$pathRouterMap$13;-><init>(Lcom/discord/utilities/intent/RouteHandlers;)V

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0xc

    aput-object v3, v0, v1

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/intent/IntentUtils;->pathRouterMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic consumeRoutingIntent$default(Lcom/discord/utilities/intent/IntentUtils;Landroid/content/Intent;Landroid/content/Context;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/utilities/intent/IntentUtils$consumeRoutingIntent$1;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils$consumeRoutingIntent$1;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/intent/IntentUtils;->consumeRoutingIntent(Landroid/content/Intent;Landroid/content/Context;Lkotlin/jvm/functions/Function2;)Z

    move-result p0

    return p0
.end method

.method private final externalize(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    const-string v0, "https"

    invoke-virtual {p1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    sget-object v0, Lf/a/b/q0/b;->E:Lf/a/b/q0/b;

    sget-object v0, Lf/a/b/q0/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method private final isHttpDomainUrl(Landroid/net/Uri;)Z
    .locals 3

    sget-object v0, Lx/s/f;->d:Lx/s/f;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "https?"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    const-string/jumbo v2, "uri.scheme ?: \"\""

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lf/a/b/q0/b;->E:Lf/a/b/q0/b;

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/a/b/q0/b;->a(Ljava/lang/String;)Z

    move-result p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final notifyFirebaseUserActionStatus(Landroid/content/Intent;Z)V
    .locals 11

    const-string v0, "actions.fulfillment.extra.ACTION_TOKEN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_8

    const-string v0, "intent.getStringExtra(In\u2026A_VOICE_ACTION) ?: return"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string p2, "http://schema.org/CompletedActionStatus"

    goto :goto_0

    :cond_0
    const-string p2, "http://schema.org/FailedActionStatus"

    :goto_0
    new-instance v0, Lf/h/c/l/c/a;

    invoke-direct {v0}, Lf/h/c/l/c/a;-><init>()V

    iput-object p1, v0, Lf/h/c/l/c/a;->f:Ljava/lang/String;

    iput-object p2, v0, Lf/h/c/l/a$a;->e:Ljava/lang/String;

    const-string p2, "setActionToken is required before calling build()."

    invoke-static {p1, p2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Ljava/lang/String;

    iget-object p2, v0, Lf/h/c/l/a$a;->e:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const-string p2, "setActionStatus is required before calling build()."

    invoke-static {p1, p2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    new-array p2, p1, [Ljava/lang/String;

    iget-object v1, v0, Lf/h/c/l/c/a;->f:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, p2, v2

    const-string v1, "actionToken"

    invoke-virtual {v0, v1, p2}, Lf/h/c/l/a$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lf/h/c/l/a$a;

    iget-object p2, v0, Lf/h/c/l/a$a;->c:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez p2, :cond_1

    move-object p2, v1

    goto :goto_1

    :cond_1
    new-instance p2, Ljava/lang/String;

    iget-object v3, v0, Lf/h/c/l/a$a;->c:Ljava/lang/String;

    invoke-direct {p2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_1
    if-nez p2, :cond_2

    const-string p2, "AssistAction"

    iput-object p2, v0, Lf/h/c/l/a$a;->c:Ljava/lang/String;

    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object p2

    const-string v3, "name"

    invoke-virtual {v0, v3, p2}, Lf/h/c/l/a$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lf/h/c/l/a$a;

    :cond_2
    iget-object p2, v0, Lf/h/c/l/a$a;->d:Ljava/lang/String;

    if-nez p2, :cond_3

    move-object p2, v1

    goto :goto_2

    :cond_3
    new-instance p2, Ljava/lang/String;

    iget-object v3, v0, Lf/h/c/l/a$a;->d:Ljava/lang/String;

    invoke-direct {p2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_2
    if-nez p2, :cond_5

    const-string p2, "https://developers.google.com/actions?invocation="

    iget-object v3, v0, Lf/h/c/l/c/a;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :cond_4
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object p2, v3

    :goto_3
    const-string v3, "null reference"

    invoke-static {p2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p2, v0, Lf/h/c/l/a$a;->d:Ljava/lang/String;

    new-array v3, p1, [Ljava/lang/String;

    aput-object p2, v3, v2

    const-string/jumbo p2, "url"

    invoke-virtual {v0, p2, v3}, Lf/h/c/l/a$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lf/h/c/l/a$a;

    :cond_5
    iget-object p2, v0, Lf/h/c/l/a$a;->c:Ljava/lang/String;

    const-string v2, "setObject is required before calling build()."

    invoke-static {p2, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, v0, Lf/h/c/l/a$a;->d:Ljava/lang/String;

    invoke-static {p2, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p2, Lcom/google/firebase/appindexing/internal/zza;

    iget-object v4, v0, Lf/h/c/l/a$a;->b:Ljava/lang/String;

    iget-object v5, v0, Lf/h/c/l/a$a;->c:Ljava/lang/String;

    iget-object v6, v0, Lf/h/c/l/a$a;->d:Ljava/lang/String;

    new-instance v8, Lcom/google/firebase/appindexing/internal/zzc;

    invoke-direct {v8, p1}, Lcom/google/firebase/appindexing/internal/zzc;-><init>(Z)V

    iget-object v9, v0, Lf/h/c/l/a$a;->e:Ljava/lang/String;

    iget-object v10, v0, Lf/h/c/l/a$a;->a:Landroid/os/Bundle;

    const/4 v7, 0x0

    move-object v3, p2

    invoke-direct/range {v3 .. v10}, Lcom/google/firebase/appindexing/internal/zza;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/firebase/appindexing/internal/zzc;Ljava/lang/String;Landroid/os/Bundle;)V

    const-class p1, Lf/h/c/l/b;

    monitor-enter p1

    :try_start_0
    sget-object v0, Lf/h/c/l/b;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_6

    goto :goto_4

    :cond_6
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lf/h/c/l/b;

    :goto_4
    if-nez v1, :cond_7

    invoke-static {}, Lf/h/c/c;->b()Lf/h/c/c;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->a:Landroid/content/Context;

    new-instance v1, Lf/h/c/l/d/b;

    invoke-direct {v1, v0}, Lf/h/c/l/d/b;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lf/h/c/l/b;->a:Ljava/lang/ref/WeakReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_7
    monitor-exit p1

    invoke-virtual {v1, p2}, Lf/h/c/l/b;->a(Lf/h/c/l/a;)Lcom/google/android/gms/tasks/Task;

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_8
    return-void
.end method

.method public static final performChooserSendIntent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1, v0}, Lcom/discord/utilities/intent/IntentUtils;->performChooserSendIntent$default(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final performChooserSendIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooserText"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/discord/utilities/intent/IntentUtils;->sendText(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic performChooserSendIntent$default(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_0

    const p2, 0x7f12169a

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "context.getString(R.string.share)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/discord/utilities/intent/IntentUtils;->performChooserSendIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final sendText(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "text/plain"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, ""

    :goto_0
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p1
.end method


# virtual methods
.method public final consumeExternalRoutingIntent(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getDynamicLinkCache()Lcom/discord/stores/StoreDynamicLink;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreDynamicLink;->storeLinkIfExists(Landroid/content/Intent;Landroid/content/Context;)V

    new-instance v0, Lcom/discord/utilities/intent/IntentUtils$consumeExternalRoutingIntent$1;

    invoke-direct {v0, p1}, Lcom/discord/utilities/intent/IntentUtils$consumeExternalRoutingIntent$1;-><init>(Landroid/content/Intent;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/discord/utilities/intent/IntentUtils;->consumeRoutingIntent(Landroid/content/Intent;Landroid/content/Context;Lkotlin/jvm/functions/Function2;)Z

    move-result p1

    return p1
.end method

.method public final consumeRoutingIntent(Landroid/content/Intent;Landroid/content/Context;Lkotlin/jvm/functions/Function2;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/net/Uri;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    :goto_0
    const-string/jumbo v1, "uri"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/discord/utilities/intent/IntentUtils;->isDiscordAppUri(Landroid/net/Uri;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lcom/discord/utilities/intent/IntentUtils;->isHttpDomainUrl(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {p3, v0, v4}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_6

    sget-object p3, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-class v1, Lcom/discord/utilities/intent/IntentUtils;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v4, "javaClass.simpleName"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    const-string v4, "<null>"

    :goto_3
    const-string/jumbo v5, "uri?.toString() ?: \"<null>\""

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3, v1, v4}, Lcom/discord/app/AppLog;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p3, Lcom/discord/utilities/intent/IntentUtils;->pathRouterMap:Ljava/util/Map;

    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_4
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/text/Regex;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    if-eqz v5, :cond_5

    const-string v7, "it"

    invoke-static {v5, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lkotlin/text/Regex;->matchEntire(Ljava/lang/CharSequence;)Lkotlin/text/MatchResult;

    move-result-object v4

    goto :goto_4

    :cond_5
    move-object v4, v6

    :goto_4
    if-eqz v4, :cond_4

    :try_start_0
    invoke-interface {v1, v0, v4, p2}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    sget-object p2, Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;->Companion:Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata$Companion;

    invoke-virtual {p2}, Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata$Companion;->getUNKNOWN()Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;

    move-result-object p2

    :goto_5
    sget-object p3, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {p1, p3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    sget-object p3, Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;->Companion:Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata$Companion;

    invoke-virtual {p3}, Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata$Companion;->getUNKNOWN()Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;

    move-result-object p3

    invoke-static {p2, p3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    xor-int/2addr p3, v2

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Intent handler activated for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", consumed: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v1, v0, v6, v2, v6}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-direct {p0, p1, p3}, Lcom/discord/utilities/intent/IntentUtils;->notifyFirebaseUserActionStatus(Landroid/content/Intent;Z)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreAnalytics;->deepLinkReceived(Landroid/content/Intent;Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;)V

    return p3

    :cond_6
    invoke-direct {p0, p1, v3}, Lcom/discord/utilities/intent/IntentUtils;->notifyFirebaseUserActionStatus(Landroid/content/Intent;Z)V

    return v3
.end method

.method public final getDirectShareId(Landroid/content/Intent;)Ljava/lang/Long;
    .locals 1

    const-string v0, "$this$getDirectShareId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android.intent.extra.shortcut.ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lx/s/l;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final isDiscordAppUri(Landroid/net/Uri;)Z
    .locals 4

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "discord"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "Locale.ENGLISH"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v3, -0x54d081ca

    if-eq v0, v3, :cond_3

    const v3, 0x17a21

    if-eq v0, v3, :cond_2

    goto :goto_2

    :cond_2
    const-string v0, "app"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_1

    :cond_3
    const-string v0, "action"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    :goto_1
    const/4 p1, 0x1

    goto :goto_3

    :cond_4
    :goto_2
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    return v2
.end method

.method public final toExternalizedSend(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    const-string v0, "$this$toExternalizedSend"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    invoke-direct {v1, v0}, Lcom/discord/utilities/intent/IntentUtils;->externalize(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    sget-object v0, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-direct {v0, p1, v1}, Lcom/discord/utilities/intent/IntentUtils;->sendText(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    return-object p1
.end method
