.class public final Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;
.super Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
.source "ChannelMemberList.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RoleHeader"
.end annotation


# instance fields
.field private final memberCount:I

.field private final roleId:J

.field private final roleName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;I)V
    .locals 2

    const-string v0, "roleName"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    iput-object p3, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    iput p4, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;JLjava/lang/String;IILjava/lang/Object;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-wide p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p3, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget p4, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->copy(JLjava/lang/String;I)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    return v0
.end method

.method public final copy(JLjava/lang/String;I)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;
    .locals 1

    const-string v0, "roleName"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;-><init>(JLjava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    iget-wide v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    iget-wide v2, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    iget p1, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMemberCount()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    return v0
.end method

.method public final getRoleId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    return-wide v0
.end method

.method public final getRoleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "RoleHeader(roleId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", roleName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->roleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", memberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->memberCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
