.class public final Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;
.super Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
.source "ChannelMemberList.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StatusHeader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;
    }
.end annotation


# instance fields
.field private final memberCount:I

.field private final rowId:Ljava/lang/String;

.field private final type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;I)V
    .locals 1

    const-string v0, "rowId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->rowId:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    iput p3, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;IILjava/lang/Object;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getRowId()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->copy(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;I)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getRowId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;I)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;
    .locals 1

    const-string v0, "rowId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;-><init>(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getRowId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getRowId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    iget-object v1, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    iget p1, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMemberCount()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    return v0
.end method

.method public getRowId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->rowId:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getRowId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StatusHeader(rowId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getRowId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->type:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", memberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->memberCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
