.class public final Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;
.super Ljava/lang/Object;
.source "ChannelMemberList.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    }
.end annotation


# instance fields
.field private groupIndices:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private groups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
            ">;"
        }
    .end annotation
.end field

.field private final listId:Ljava/lang/String;

.field private final logger:Lcom/discord/utilities/logging/Logger;

.field private rows:Lcom/discord/utilities/collections/SparseMutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;)V
    .locals 7

    const-string v0, "other"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    iget-object v4, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;-><init>(Ljava/lang/String;ILcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object v0, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList;->deepCopy()Lcom/discord/utilities/collections/SparseMutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groups:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groups:Ljava/util/Map;

    new-instance v0, Ljava/util/TreeMap;

    iget-object p1, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/SortedMap;)V

    iput-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/discord/utilities/logging/Logger;)V
    .locals 1

    const-string v0, "listId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance p1, Lcom/discord/utilities/collections/SparseMutableList;

    const/16 p3, 0x64

    invoke-direct {p1, p2, p3}, Lcom/discord/utilities/collections/SparseMutableList;-><init>(II)V

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groups:Ljava/util/Map;

    new-instance p1, Ljava/util/TreeMap;

    invoke-direct {p1}, Ljava/util/TreeMap;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;-><init>(Ljava/lang/String;ILcom/discord/utilities/logging/Logger;)V

    return-void
.end method


# virtual methods
.method public final add(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ADD"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final delete(I)V
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " DELETE index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method public final getGroupIndices()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    return-object v0
.end method

.method public final getListId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRows()Lcom/discord/utilities/collections/SparseMutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    return-object v0
.end method

.method public final getSize()I
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SparseMutableList;->size()I

    move-result v0

    return v0
.end method

.method public final insert(ILcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " INSERT: index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList;->add(ILjava/lang/Object;)V

    return-void
.end method

.method public final invalidate(Lkotlin/ranges/IntRange;)V
    .locals 3

    const-string v0, "range"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " INVALIDATE range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p1, Lkotlin/ranges/IntProgression;->e:I

    iget-object v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-static {v1}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget p1, p1, Lkotlin/ranges/IntProgression;->d:I

    if-gt p1, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/discord/utilities/collections/SparseMutableList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    if-eq p1, v0, :cond_1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final rebuildMembers(Lkotlin/jvm/functions/Function1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
            ">;)V"
        }
    .end annotation

    const-string v0, "makeMember"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-ltz v1, :cond_1

    check-cast v2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    instance-of v4, v2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;

    if-eqz v4, :cond_0

    check-cast v2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;

    invoke-virtual {v2}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getUserId()J

    move-result-wide v4

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lcom/discord/utilities/collections/SparseMutableList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    return-void
.end method

.method public final setGroupIndices(Ljava/util/SortedMap;)V
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "groupIndices"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    return-void
.end method

.method public final setGroups(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;",
            "+",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
            ">;)V"
        }
    .end annotation

    const-string v0, "groups"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "makeGroup"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " SET_GROUPS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    const/16 v1, 0x10

    :cond_1
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groupIndices:Ljava/util/SortedMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    invoke-interface {p2, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iput-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->groups:Ljava/util/Map;

    iget-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/collections/SparseMutableList;->setSize(I)V

    return-void
.end method

.method public final sync(ILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
            ">;)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " SYNC: startIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " -- items size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    if-ltz v0, :cond_1

    check-cast v1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    add-int/2addr v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->update(ILcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)V

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    return-void
.end method

.method public final update(ILcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->logger:Lcom/discord/utilities/logging/Logger;

    if-eqz v0, :cond_0

    const-string v1, "memberListId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->listId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " UPDATE index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ChannelMemberList"

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rows:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/collections/SparseMutableList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
