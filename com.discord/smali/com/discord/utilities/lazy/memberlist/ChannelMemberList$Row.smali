.class public abstract Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
.super Ljava/lang/Object;
.source "ChannelMemberList.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Row"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;,
        Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;,
        Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;
    }
.end annotation


# instance fields
.field private final rowId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;->rowId:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getRowId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;->rowId:Ljava/lang/String;

    return-object v0
.end method
