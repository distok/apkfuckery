.class public final Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;
.super Ljava/lang/Object;
.source "GuildSubscriptionsManager.kt"


# instance fields
.field private final activityGuilds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

.field private final guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

.field private final onChange:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingEmissions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;",
            ">;"
        }
    .end annotation
.end field

.field private final subscribedGuilds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final typingGuilds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function2;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onChange"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->onChange:Lkotlin/jvm/functions/Function2;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    new-instance v0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildChannelSubscriptionsManager$1;

    invoke-direct {v0, p0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildChannelSubscriptionsManager$1;-><init>(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;)V

    invoke-direct {p1, v0}, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;-><init>(Lkotlin/jvm/functions/Function2;)V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    new-instance v2, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildMemberSubscriptionsManager$1;

    invoke-direct {v2, p0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildMemberSubscriptionsManager$1;-><init>(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;)V

    new-instance v3, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildMemberSubscriptionsManager$2;

    invoke-direct {v3, p0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildMemberSubscriptionsManager$2;-><init>(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;-><init>(Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Lrx/Scheduler;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribedGuilds:Ljava/util/HashSet;

    return-void
.end method

.method public static final synthetic access$handleChannelSubscriptionsChange(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;JLjava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->handleChannelSubscriptionsChange(JLjava/util/Map;)V

    return-void
.end method

.method public static final synthetic access$handleMemberSubscriptionsChange(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;JLjava/util/Set;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->handleMemberSubscriptionsChange(JLjava/util/Set;Z)V

    return-void
.end method

.method public static final synthetic access$requestFlushUnsubscriptions(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->requestFlushUnsubscriptions(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final declared-synchronized handleChannelSubscriptionsChange(JLjava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/List<",
            "Lkotlin/ranges/IntRange;",
            ">;>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    if-eqz v2, :cond_0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v3, p3

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->copy$default(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;-><init>(Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleMemberSubscriptionsChange(JLjava/util/Set;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v6, p3

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->copy$default(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v2, p1

    move-object v6, p3

    invoke-direct/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;-><init>(Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized requestFlushUnsubscriptions(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final declared-synchronized flush()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    iget-object v4, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribedGuilds:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->onChange:Lkotlin/jvm/functions/Function2;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized get(J)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    iget-object v1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    invoke-virtual {v1, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;->get(J)Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    invoke-virtual {v2, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;->get(J)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, v2, v3, p1, v1}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;-><init>(Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized queueExistingSubscriptions()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribedGuilds:Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0, v1, v2}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->get(J)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized remove(J)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribedGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;->remove(J)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;->remove(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribedGuilds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    invoke-virtual {v0}, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;->reset()V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    invoke-virtual {v0}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized retainAll(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "guildIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribedGuilds:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;->retainAll(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;->retainAll(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized subscribeActivities(J)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->activityGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v6, 0x0

    const/16 v7, 0xb

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->copy$default(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v6, 0x0

    const/16 v7, 0xb

    const/4 v8, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;-><init>(Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized subscribeChannel(JJLjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Lkotlin/ranges/IntRange;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "ranges"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildChannelSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;->subscribe(JJLjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized subscribeMember(JJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;->subscribe(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized subscribeTyping(J)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->typingGuilds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->pendingEmissions:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xd

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->copy$default(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xd

    const/4 v8, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;-><init>(Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized unsubscribeMember(JJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->guildMemberSubscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;->unsubscribe(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
