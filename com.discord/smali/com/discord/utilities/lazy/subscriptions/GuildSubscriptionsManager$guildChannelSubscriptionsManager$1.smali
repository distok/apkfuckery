.class public final synthetic Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildChannelSubscriptionsManager$1;
.super Lx/m/c/i;
.source "GuildSubscriptionsManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;-><init>(Lkotlin/jvm/functions/Function2;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Long;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/util/List<",
        "+",
        "Lkotlin/ranges/IntRange;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;)V
    .locals 7

    const-class v3, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    const/4 v1, 0x2

    const-string v4, "handleChannelSubscriptionsChange"

    const-string v5, "handleChannelSubscriptionsChange(JLjava/util/Map;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, v0, v1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildChannelSubscriptionsManager$1;->invoke(JLjava/util/Map;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(JLjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/List<",
            "Lkotlin/ranges/IntRange;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "p2"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-static {v0, p1, p2, p3}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->access$handleChannelSubscriptionsChange(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;JLjava/util/Map;)V

    return-void
.end method
