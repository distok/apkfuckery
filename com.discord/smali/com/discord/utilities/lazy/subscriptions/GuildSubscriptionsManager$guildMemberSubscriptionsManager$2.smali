.class public final synthetic Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildMemberSubscriptionsManager$2;
.super Lx/m/c/i;
.source "GuildSubscriptionsManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;-><init>(Lkotlin/jvm/functions/Function2;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/jvm/functions/Function0<",
        "+",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;)V
    .locals 7

    const-class v3, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    const/4 v1, 0x1

    const-string v4, "requestFlushUnsubscriptions"

    const-string v5, "requestFlushUnsubscriptions(Lkotlin/jvm/functions/Function0;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager$guildMemberSubscriptionsManager$2;->invoke(Lkotlin/jvm/functions/Function0;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-static {v0, p1}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->access$requestFlushUnsubscriptions(Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
