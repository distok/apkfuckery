.class public final Lcom/discord/utilities/analytics/Traits$Location$Section;
.super Ljava/lang/Object;
.source "Traits.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/analytics/Traits$Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Section"
.end annotation


# static fields
.field public static final FOOTER:Ljava/lang/String; = "Footer"

.field public static final HEADER:Ljava/lang/String; = "Header"

.field public static final INSTANCE:Lcom/discord/utilities/analytics/Traits$Location$Section;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/Traits$Location$Section;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/Traits$Location$Section;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/Traits$Location$Section;->INSTANCE:Lcom/discord/utilities/analytics/Traits$Location$Section;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
