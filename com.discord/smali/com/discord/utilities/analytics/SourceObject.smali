.class public final enum Lcom/discord/utilities/analytics/SourceObject;
.super Ljava/lang/Enum;
.source "SourceObject.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/analytics/SourceObject;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/analytics/SourceObject;

.field public static final enum GIF_PICKER:Lcom/discord/utilities/analytics/SourceObject;


# instance fields
.field private final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/discord/utilities/analytics/SourceObject;

    new-instance v1, Lcom/discord/utilities/analytics/SourceObject;

    const-string v2, "GIF_PICKER"

    const/4 v3, 0x0

    const-string v4, "GIF Picker"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/analytics/SourceObject;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/utilities/analytics/SourceObject;->GIF_PICKER:Lcom/discord/utilities/analytics/SourceObject;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/utilities/analytics/SourceObject;->$VALUES:[Lcom/discord/utilities/analytics/SourceObject;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/utilities/analytics/SourceObject;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/analytics/SourceObject;
    .locals 1

    const-class v0, Lcom/discord/utilities/analytics/SourceObject;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/analytics/SourceObject;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/analytics/SourceObject;
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/SourceObject;->$VALUES:[Lcom/discord/utilities/analytics/SourceObject;

    invoke-virtual {v0}, [Lcom/discord/utilities/analytics/SourceObject;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/analytics/SourceObject;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/analytics/SourceObject;->analyticsName:Ljava/lang/String;

    return-object v0
.end method
