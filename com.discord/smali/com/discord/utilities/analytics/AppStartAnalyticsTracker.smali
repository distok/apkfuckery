.class public final Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;
.super Ljava/lang/Object;
.source "AppStartAnalyticsTracker.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;

.field private static final instance$delegate:Lkotlin/Lazy;


# instance fields
.field private appOpenTimestamp:Ljava/lang/Long;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final openAppLoadId:Ljava/lang/String;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;

.field private final tracker:Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->Companion:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;

    sget-object v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;->INSTANCE:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->instance$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public constructor <init>(Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreUserSettings;)V
    .locals 1

    const-string/jumbo v0, "tracker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->tracker:Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    iput-object p2, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p3, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UUID.randomUUID().toString()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->openAppLoadId:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getAppOpenTimestamp$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Ljava/lang/Long;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->appOpenTimestamp:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Lcom/discord/utilities/time/Clock;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$getInstance$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->instance$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$getOpenAppLoadId$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->openAppLoadId:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getStoreUserSettings$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Lcom/discord/stores/StoreUserSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    return-object p0
.end method

.method public static final synthetic access$setAppOpenTimestamp$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->appOpenTimestamp:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public final appOpen(Landroid/net/Uri;ZZ)V
    .locals 5

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/Pair;

    const-string v1, "app_opened"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->tracker:Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    const-wide/32 v2, 0x493e0

    new-instance v4, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;

    invoke-direct {v4, p0, p3, p2, p1}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;-><init>(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;ZZLandroid/net/Uri;)V

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;->track(Lkotlin/Pair;JLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final appUiViewed(Ljava/lang/String;J)V
    .locals 4

    const-string v0, "screenName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->appOpenTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    sub-long/2addr p2, v0

    goto :goto_0

    :cond_0
    const-wide/16 p2, -0x1

    :goto_0
    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/Pair;

    const/4 v1, 0x0

    new-instance v2, Lkotlin/Pair;

    const-string v3, "screen_name"

    invoke-direct {v2, v3, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    const/4 p1, 0x1

    iget-object v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->openAppLoadId:Ljava/lang/String;

    new-instance v2, Lkotlin/Pair;

    const-string v3, "load_id"

    invoke-direct {v2, v3, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, p1

    const/4 p1, 0x2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    new-instance p3, Lkotlin/Pair;

    const-string v1, "duration_ms_since_app_opened"

    invoke-direct {p3, v1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p3, v0, p1

    const/4 p1, 0x3

    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    new-instance p3, Lkotlin/Pair;

    const-string v1, "has_cached_data"

    invoke-direct {p3, v1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p3, v0, p1

    const/4 p1, 0x4

    iget-object p2, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {p2}, Lcom/discord/stores/StoreUserSettings;->getTheme()Ljava/lang/String;

    move-result-object p2

    new-instance p3, Lkotlin/Pair;

    const-string/jumbo v1, "theme"

    invoke-direct {p3, v1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p3, v0, p1

    invoke-static {v0}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->tracker:Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    const-string p3, "app_ui_viewed"

    invoke-virtual {p2, p3, p1}, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;->track(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
