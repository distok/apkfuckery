.class public final Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;
.super Lx/m/c/k;
.source "HardwareAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/analytics/HardwareAnalytics;->runHardwareAnalytics(Landroid/content/Context;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/hardware_analytics/DecoderCountInfo;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $buildInfo:Lcom/discord/hardware_analytics/BuildInfo;

.field public final synthetic $cache:Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;

.field public final synthetic $logger:Lcom/discord/utilities/logging/Logger;

.field public final synthetic $memoryInfo:Lcom/discord/hardware_analytics/MemoryInfo;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/logging/Logger;Lcom/discord/hardware_analytics/BuildInfo;Lcom/discord/hardware_analytics/MemoryInfo;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$logger:Lcom/discord/utilities/logging/Logger;

    iput-object p2, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$buildInfo:Lcom/discord/hardware_analytics/BuildInfo;

    iput-object p3, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$memoryInfo:Lcom/discord/hardware_analytics/MemoryInfo;

    iput-object p4, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$cache:Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/hardware_analytics/DecoderCountInfo;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->invoke(Lcom/discord/hardware_analytics/DecoderCountInfo;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/hardware_analytics/DecoderCountInfo;)V
    .locals 7

    const-string v0, "decoderCountInfo"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$logger:Lcom/discord/utilities/logging/Logger;

    const-string v0, "buildInfo="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$buildInfo:Lcom/discord/hardware_analytics/BuildInfo;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", memoryInfo="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$memoryInfo:Lcom/discord/hardware_analytics/MemoryInfo;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", decoderCountInfo="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v2, "HardwareAnalytics"

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v1, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$buildInfo:Lcom/discord/hardware_analytics/BuildInfo;

    iget-object v2, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$memoryInfo:Lcom/discord/hardware_analytics/MemoryInfo;

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->hardwareAnalytics(Lcom/discord/hardware_analytics/BuildInfo;Lcom/discord/hardware_analytics/MemoryInfo;Lcom/discord/hardware_analytics/DecoderCountInfo;)V

    iget-object p1, p0, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;->$cache:Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    sget-object v0, Lf/a/c/c;->d:Lf/a/c/c;

    invoke-static {p1, v0}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    :cond_0
    return-void
.end method
