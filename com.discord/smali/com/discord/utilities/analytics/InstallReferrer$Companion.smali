.class public final Lcom/discord/utilities/analytics/InstallReferrer$Companion;
.super Ljava/lang/Object;
.source "InstallReferrer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/analytics/InstallReferrer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer$Companion;-><init>()V

    return-void
.end method

.method private final getINSTANCE()Lcom/discord/utilities/analytics/InstallReferrer;
    .locals 2

    invoke-static {}, Lcom/discord/utilities/analytics/InstallReferrer;->access$getINSTANCE$cp()Lkotlin/Lazy;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/InstallReferrer;->Companion:Lcom/discord/utilities/analytics/InstallReferrer$Companion;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/analytics/InstallReferrer;

    return-object v0
.end method


# virtual methods
.method public final init(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onReceivedInstallReferrer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer$Companion;->getINSTANCE()Lcom/discord/utilities/analytics/InstallReferrer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/analytics/InstallReferrer;->init(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
