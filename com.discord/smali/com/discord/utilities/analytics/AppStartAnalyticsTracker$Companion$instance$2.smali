.class public final Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;
.super Lx/m/c/k;
.source "AppStartAnalyticsTracker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;->INSTANCE:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;
    .locals 4

    new-instance v0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;->Companion:Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker$Companion;->getInstance()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    move-result-object v1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v2

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;-><init>(Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreUserSettings;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion$instance$2;->invoke()Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    move-result-object v0

    return-object v0
.end method
