.class public final Lcom/discord/utilities/analytics/AnalyticSuperProperties;
.super Ljava/lang/Object;
.source "AnalyticSuperProperties.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final INSTANCE:Lcom/discord/utilities/analytics/AnalyticSuperProperties;

.field private static final PROPERTY_ACCESSIBILITY_FEATURES:Ljava/lang/String; = "accessibility_features"

.field private static final PROPERTY_ACCESSIBILITY_SUPPORT_ENABLED:Ljava/lang/String; = "accessibility_support_enabled"

.field private static final PROPERTY_BROWSER:Ljava/lang/String; = "browser"

.field private static final PROPERTY_BROWSER_USER_AGENT:Ljava/lang/String; = "browser_user_agent"

.field private static final PROPERTY_CLIENT_BUILD_NUMBER:Ljava/lang/String; = "client_build_number"

.field private static final PROPERTY_CLIENT_VERSION:Ljava/lang/String; = "client_version"

.field private static final PROPERTY_DEVICE:Ljava/lang/String; = "device"

.field private static final PROPERTY_DEVICE_ADVERTISER_ID:Ljava/lang/String; = "device_advertiser_id"

.field private static final PROPERTY_LOCATION:Ljava/lang/String; = "location"

.field private static final PROPERTY_MP_KEYWORD:Ljava/lang/String; = "mp_keyword"

.field private static final PROPERTY_OS:Ljava/lang/String; = "os"

.field private static final PROPERTY_OS_SDK_VERSION:Ljava/lang/String; = "os_sdk_version"

.field private static final PROPERTY_OS_VERSION:Ljava/lang/String; = "os_version"

.field private static final PROPERTY_REFERRER:Ljava/lang/String; = "referrer"

.field private static final PROPERTY_REFERRING_DOMAIN:Ljava/lang/String; = "referring_domain"

.field private static final PROPERTY_SEARCH_ENGINE:Ljava/lang/String; = "search_engine"

.field private static final PROPERTY_UTM_CAMPAIGN:Ljava/lang/String; = "utm_campaign"

.field private static final PROPERTY_UTM_CONTENT:Ljava/lang/String; = "utm_content"

.field private static final PROPERTY_UTM_MEDIUM:Ljava/lang/String; = "utm_medium"

.field private static final PROPERTY_UTM_SOURCE:Ljava/lang/String; = "utm_source"

.field private static final PROPERTY_UTM_TERM:Ljava/lang/String; = "utm_term"

.field private static final superProperties$delegate:Lkotlin/properties/ReadWriteProperty;

.field private static superPropertiesString:Ljava/lang/String;

.field private static superPropertiesStringBase64:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/o;

    const-class v2, Lcom/discord/utilities/analytics/AnalyticSuperProperties;

    const-string/jumbo v3, "superProperties"

    const-string v4, "getSuperProperties()Ljava/util/Map;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/o;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticSuperProperties;

    const-string v1, ""

    sput-object v1, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesString:Ljava/lang/String;

    sput-object v1, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesStringBase64:Ljava/lang/String;

    sget-object v1, Lx/h/m;->d:Lx/h/m;

    new-instance v2, Lcom/discord/utilities/analytics/AnalyticSuperProperties$$special$$inlined$observable$1;

    invoke-direct {v2, v1, v1}, Lcom/discord/utilities/analytics/AnalyticSuperProperties$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v2, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superProperties$delegate:Lkotlin/properties/ReadWriteProperty;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->setBaselineProperties()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getSuperPropertiesString$p(Lcom/discord/utilities/analytics/AnalyticSuperProperties;)Ljava/lang/String;
    .locals 0

    sget-object p0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesString:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getSuperPropertiesStringBase64$p(Lcom/discord/utilities/analytics/AnalyticSuperProperties;)Ljava/lang/String;
    .locals 0

    sget-object p0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesStringBase64:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$setSuperPropertiesString$p(Lcom/discord/utilities/analytics/AnalyticSuperProperties;Ljava/lang/String;)V
    .locals 0

    sput-object p1, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesString:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setSuperPropertiesStringBase64$p(Lcom/discord/utilities/analytics/AnalyticSuperProperties;Ljava/lang/String;)V
    .locals 0

    sput-object p1, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesStringBase64:Ljava/lang/String;

    return-void
.end method

.method private final setBaselineProperties()V
    .locals 5

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/Pair;

    new-instance v1, Lkotlin/Pair;

    const-string v2, "browser"

    const-string v3, "Discord Android"

    invoke-direct {v1, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Lkotlin/Pair;

    const-string v3, "browser_user_agent"

    const-string v4, "Discord-Android/1351"

    invoke-direct {v2, v3, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x547

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string v4, "client_build_number"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lkotlin/Pair;

    const-string v3, "client_version"

    const-string v4, "52.1"

    invoke-direct {v2, v3, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string v4, "device"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lkotlin/Pair;

    const-string v3, "os"

    const-string v4, "Android"

    invoke-direct {v2, v3, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string v4, "os_sdk_version"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    new-instance v3, Lkotlin/Pair;

    const-string v4, "os_version"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->updateSuperProperties(Ljava/util/Map;)V

    return-void
.end method

.method private final setSuperProperties(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superProperties$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method private final declared-synchronized updateSuperProperties(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->getSuperProperties()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->setSuperProperties(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final getSuperProperties()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superProperties$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public final getSuperPropertiesString()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesString:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuperPropertiesStringBase64()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->superPropertiesStringBase64:Ljava/lang/String;

    return-object v0
.end method

.method public final setAccessibilityProperties(ZLjava/util/EnumSet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/EnumSet<",
            "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;",
            ">;)V"
        }
    .end annotation

    const-string v0, "features"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const-wide/16 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v2}, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->getValue()J

    move-result-wide v2

    or-long/2addr v0, v2

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    new-array p2, p2, [Lkotlin/Pair;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    new-instance v3, Lkotlin/Pair;

    const-string v4, "accessibility_support_enabled"

    invoke-direct {v3, v4, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, p2, v2

    const/4 p1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v1, Lkotlin/Pair;

    const-string v2, "accessibility_features"

    invoke-direct {v1, v2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, p2, p1

    invoke-static {p2}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->updateSuperProperties(Ljava/util/Map;)V

    return-void
.end method

.method public final setAdvertiserId(Ljava/lang/String;)V
    .locals 2

    const-string v0, "advertiserId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/Pair;

    const-string v1, "device_advertiser_id"

    invoke-direct {v0, v1, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->updateSuperProperties(Ljava/util/Map;)V

    return-void
.end method

.method public final setCampaignProperties(Ljava/lang/String;)V
    .locals 7

    const-string v0, "referrerUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/Pair;

    new-instance v2, Lkotlin/Pair;

    const-string v3, "referrer"

    invoke-direct {v2, v3, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "&"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x6

    invoke-static {p1, v2, v3, v3, v4}, Lx/s/r;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZII)Ljava/util/List;

    move-result-object p1

    new-instance v2, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {p1, v5}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "="

    filled-new-array {v6}, [Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3, v3, v4}, Lx/s/r;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZII)Ljava/util/List;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_1

    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    goto :goto_5

    :sswitch_0
    const-string/jumbo v6, "utm_source"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_1
    const-string v6, "location"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_2
    const-string/jumbo v6, "utm_medium"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_3
    const-string v6, "search_engine"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_4
    const-string v6, "mp_keyword"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_5
    const-string/jumbo v6, "utm_term"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_6
    const-string/jumbo v6, "utm_campaign"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_7
    const-string v6, "referring_domain"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :sswitch_8
    const-string/jumbo v6, "utm_content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :goto_4
    const/4 v5, 0x1

    goto :goto_6

    :cond_5
    :goto_5
    const/4 v5, 0x0

    :goto_6
    if-eqz v5, :cond_4

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_7
    invoke-direct {p0, v1}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->updateSuperProperties(Ljava/util/Map;)V

    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5bc8ed18 -> :sswitch_8
        -0x40f32acd -> :sswitch_7
        -0x3db0f7f -> :sswitch_6
        0x31ad945d -> :sswitch_5
        0x3d3a7f4d -> :sswitch_4
        0x3f4764b9 -> :sswitch_3
        0x70a1a726 -> :sswitch_2
        0x714f9fb5 -> :sswitch_1
        0x7b737fcc -> :sswitch_0
    .end sparse-switch
.end method
