.class public final Lcom/discord/utilities/analytics/ChatInputComponentTypes;
.super Ljava/lang/Object;
.source "ChatInputComponentTypes.kt"


# static fields
.field public static final CAMERA:Ljava/lang/String; = "camera"

.field public static final EMOJI:Ljava/lang/String; = "emoji"

.field public static final EMOJI_SEARCH:Ljava/lang/String; = "emoji search"

.field public static final FILES:Ljava/lang/String; = "files"

.field public static final GIF:Ljava/lang/String; = "GIF"

.field public static final GIF_SEARCH:Ljava/lang/String; = "gif search"

.field public static final INSTANCE:Lcom/discord/utilities/analytics/ChatInputComponentTypes;

.field public static final MEDIA_PICKER:Ljava/lang/String; = "media picker"

.field public static final STICKER:Ljava/lang/String; = "sticker"

.field public static final STICKER_SEARCH:Ljava/lang/String; = "sticker search"


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/ChatInputComponentTypes;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/ChatInputComponentTypes;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/ChatInputComponentTypes;->INSTANCE:Lcom/discord/utilities/analytics/ChatInputComponentTypes;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
