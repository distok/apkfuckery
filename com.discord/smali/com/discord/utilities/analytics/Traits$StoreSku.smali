.class public final Lcom/discord/utilities/analytics/Traits$StoreSku;
.super Ljava/lang/Object;
.source "Traits.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/analytics/Traits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreSku"
.end annotation


# instance fields
.field private final applicationId:J

.field private final skuId:J

.field private final skuType:I

.field private final storeTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(JIJLjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "storeTitle"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    iput p3, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    iput-wide p4, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    iput-object p6, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/analytics/Traits$StoreSku;JIJLjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/analytics/Traits$StoreSku;
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-wide p1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p7, 0x2

    if-eqz p1, :cond_1

    iget p3, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    :cond_1
    move v3, p3

    and-int/lit8 p1, p7, 0x4

    if-eqz p1, :cond_2

    iget-wide p4, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    :cond_2
    move-wide v4, p4

    and-int/lit8 p1, p7, 0x8

    if-eqz p1, :cond_3

    iget-object p6, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    :cond_3
    move-object v6, p6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/discord/utilities/analytics/Traits$StoreSku;->copy(JIJLjava/lang/String;)Lcom/discord/utilities/analytics/Traits$StoreSku;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    return-wide v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    return v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    return-wide v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JIJLjava/lang/String;)Lcom/discord/utilities/analytics/Traits$StoreSku;
    .locals 8

    const-string/jumbo v0, "storeTitle"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/analytics/Traits$StoreSku;

    move-object v1, v0

    move-wide v2, p1

    move v4, p3

    move-wide v5, p4

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/utilities/analytics/Traits$StoreSku;-><init>(JIJLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/analytics/Traits$StoreSku;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/analytics/Traits$StoreSku;

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    iget-wide v2, p1, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    iget v1, p1, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    iget-wide v2, p1, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplicationId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    return-wide v0
.end method

.method public final getSkuId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    return-wide v0
.end method

.method public final getSkuType()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    return v0
.end method

.method public final getStoreTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final serializeTo(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "sku_id"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "sku_type"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "application_id"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    const-string/jumbo v1, "store_title"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StoreSku(skuId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", skuType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->skuType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", applicationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->applicationId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", storeTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/analytics/Traits$StoreSku;->storeTitle:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
