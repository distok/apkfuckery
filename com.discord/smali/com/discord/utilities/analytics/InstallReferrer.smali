.class public final Lcom/discord/utilities/analytics/InstallReferrer;
.super Ljava/lang/Object;
.source "InstallReferrer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/analytics/InstallReferrer$Companion;
    }
.end annotation


# static fields
.field private static final CACHE_KEY_HAS_EXECUTED:Ljava/lang/String; = "CACHE_KEY_HAS_EXECUTED"

.field private static final CACHE_KEY_HAS_EXECUTED_ATTEMPTS:Ljava/lang/String; = "CACHE_KEY_HAS_EXECUTED_ATTEMPTS"

.field public static final Companion:Lcom/discord/utilities/analytics/InstallReferrer$Companion;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;

.field private static final MAX_ATTEMPTS:I = 0xa


# instance fields
.field private final context:Landroid/content/Context;

.field private final logger:Lcom/discord/utilities/logging/Logger;

.field private referrerClient:Lcom/android/installreferrer/api/InstallReferrerClient;

.field private final sharedPreferences$delegate:Lkotlin/Lazy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/utilities/analytics/InstallReferrer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/utilities/analytics/InstallReferrer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/utilities/analytics/InstallReferrer;->Companion:Lcom/discord/utilities/analytics/InstallReferrer$Companion;

    sget-object v0, Lcom/discord/utilities/analytics/InstallReferrer$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/utilities/analytics/InstallReferrer$Companion$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/analytics/InstallReferrer;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private constructor <init>(Lcom/discord/utilities/logging/Logger;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer;->logger:Lcom/discord/utilities/logging/Logger;

    iput-object p2, p0, Lcom/discord/utilities/analytics/InstallReferrer;->context:Landroid/content/Context;

    sget-object p1, Lcom/discord/utilities/analytics/InstallReferrer$sharedPreferences$2;->INSTANCE:Lcom/discord/utilities/analytics/InstallReferrer$sharedPreferences$2;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer;->sharedPreferences$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/logging/Logger;Landroid/content/Context;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    sget-object p1, Lcom/discord/utilities/logging/LoggingProvider;->INSTANCE:Lcom/discord/utilities/logging/LoggingProvider;

    invoke-virtual {p1}, Lcom/discord/utilities/logging/LoggingProvider;->get()Lcom/discord/utilities/logging/Logger;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    sget-object p2, Lcom/discord/utilities/app/ApplicationProvider;->INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

    invoke-virtual {p2}, Lcom/discord/utilities/app/ApplicationProvider;->get()Landroid/app/Application;

    move-result-object p2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/analytics/InstallReferrer;-><init>(Lcom/discord/utilities/logging/Logger;Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/InstallReferrer;->INSTANCE$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$getLogger$p(Lcom/discord/utilities/analytics/InstallReferrer;)Lcom/discord/utilities/logging/Logger;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->logger:Lcom/discord/utilities/logging/Logger;

    return-object p0
.end method

.method public static final synthetic access$getReferrerClient$p(Lcom/discord/utilities/analytics/InstallReferrer;)Lcom/android/installreferrer/api/InstallReferrerClient;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->referrerClient:Lcom/android/installreferrer/api/InstallReferrerClient;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "referrerClient"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setFetchInstallReferrerFailed(Lcom/discord/utilities/analytics/InstallReferrer;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/analytics/InstallReferrer;->setFetchInstallReferrerFailed(Ljava/lang/Exception;)V

    return-void
.end method

.method public static final synthetic access$setFetchInstallReferrerSuccessful(Lcom/discord/utilities/analytics/InstallReferrer;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer;->setFetchInstallReferrerSuccessful()V

    return-void
.end method

.method public static final synthetic access$setReferrerClient$p(Lcom/discord/utilities/analytics/InstallReferrer;Lcom/android/installreferrer/api/InstallReferrerClient;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer;->referrerClient:Lcom/android/installreferrer/api/InstallReferrerClient;

    return-void
.end method

.method private final createReferrerStateListener(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;"
        }
    .end annotation

    new-instance v0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;-><init>(Lcom/discord/utilities/analytics/InstallReferrer;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method private final getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->sharedPreferences$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private final setFetchInstallReferrerFailed(Ljava/lang/Exception;)V
    .locals 4

    sget-object v0, Lcom/discord/utilities/cache/SharedPreferencesProvider;->INSTANCE:Lcom/discord/utilities/cache/SharedPreferencesProvider;

    invoke-virtual {v0}, Lcom/discord/utilities/cache/SharedPreferencesProvider;->get()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CACHE_KEY_HAS_EXECUTED_ATTEMPTS"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/discord/utilities/analytics/InstallReferrer;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to resolve referrer details, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " attempt."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/discord/utilities/logging/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object p1

    new-instance v1, Lcom/discord/utilities/analytics/InstallReferrer$setFetchInstallReferrerFailed$1;

    invoke-direct {v1, v0}, Lcom/discord/utilities/analytics/InstallReferrer$setFetchInstallReferrerFailed$1;-><init>(I)V

    invoke-static {p1, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final setFetchInstallReferrerSuccessful()V
    .locals 4

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->referrerClient:Lcom/android/installreferrer/api/InstallReferrerClient;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/installreferrer/api/InstallReferrerClient;->b()Lf/e/b/a/c;

    move-result-object v0

    const-string v2, "referrerClient.installReferrer"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lf/e/b/a/c;->a:Landroid/os/Bundle;

    const-string v2, "install_referrer"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/utilities/analytics/InstallReferrer;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v3, "Retrieved install referrer, "

    invoke-static {v3, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v2, v0, v1, v3, v1}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/InstallReferrer$setFetchInstallReferrerSuccessful$1;->INSTANCE:Lcom/discord/utilities/analytics/InstallReferrer$setFetchInstallReferrerSuccessful$1;

    invoke-static {v0, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_0
    const-string v0, "referrerClient"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final shouldFetchInstallReferrer()Z
    .locals 4

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CACHE_KEY_HAS_EXECUTED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "CACHE_KEY_HAS_EXECUTED_ATTEMPTS"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v0, :cond_0

    const/16 v0, 0xa

    if-ge v1, v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method


# virtual methods
.method public final init(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onReceivedInstallReferrer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/utilities/analytics/InstallReferrer;->shouldFetchInstallReferrer()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->context:Landroid/content/Context;

    if-eqz v0, :cond_2

    new-instance v1, Lf/e/b/a/a;

    invoke-direct {v1, v0}, Lf/e/b/a/a;-><init>(Landroid/content/Context;)V

    const-string v0, "InstallReferrerClient.newBuilder(context).build()"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/discord/utilities/analytics/InstallReferrer;->referrerClient:Lcom/android/installreferrer/api/InstallReferrerClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/discord/utilities/analytics/InstallReferrer;->createReferrerStateListener(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/android/installreferrer/api/InstallReferrerClient;->c(Lf/e/b/a/b;)V

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    :cond_1
    const-string p1, "referrerClient"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 p1, 0x0

    throw p1

    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v1, "Unable to start connection to referrer client."

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/logging/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Please provide a valid Context."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v1, "Unable to initialize referrer client."

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/logging/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
