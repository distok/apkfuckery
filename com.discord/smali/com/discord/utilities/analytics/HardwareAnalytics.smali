.class public final Lcom/discord/utilities/analytics/HardwareAnalytics;
.super Ljava/lang/Object;
.source "HardwareAnalytics.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/analytics/HardwareAnalytics;

.field private static final TAG:Ljava/lang/String; = "HardwareAnalytics"

.field private static final initialized:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/HardwareAnalytics;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/HardwareAnalytics;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/HardwareAnalytics;->INSTANCE:Lcom/discord/utilities/analytics/HardwareAnalytics;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/HardwareAnalytics;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$maybeRunHardwareAnalytics(Lcom/discord/utilities/analytics/HardwareAnalytics;Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/analytics/HardwareAnalytics;->maybeRunHardwareAnalytics(Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method

.method private final maybeRunHardwareAnalytics(Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 7

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/discord/utilities/app/ApplicationProvider;->INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

    invoke-virtual {v0}, Lcom/discord/utilities/app/ApplicationProvider;->get()Landroid/app/Application;

    move-result-object v0

    new-instance v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;

    invoke-direct {v2, v0}, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->isOverride()Z

    move-result p1

    const/4 v3, 0x0

    const-string v4, "HardwareSurveyService.ATTEMPTS"

    if-nez p1, :cond_1

    iget-object p1, v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    const/4 v5, -0x1

    const-string v6, "HardwareSurveyService.VERSION_KEY"

    invoke-interface {p1, v6, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-ge p1, v1, :cond_0

    iget-object p1, v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    invoke-interface {p1, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    const/4 v5, 0x2

    if-ge p1, v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    :cond_1
    iget-object p1, v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    invoke-interface {p1, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    iget-object v1, v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    new-instance v3, Lf/a/c/d;

    invoke-direct {v3, p1}, Lf/a/c/d;-><init>(I)V

    invoke-static {v1, v3}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p0, v0, v2}, Lcom/discord/utilities/analytics/HardwareAnalytics;->runHardwareAnalytics(Landroid/content/Context;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;)V

    :cond_2
    return-void
.end method

.method private final observeExperiment(Lcom/discord/stores/StoreExperiments;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreExperiments;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/experiments/domain/Experiment;",
            ">;"
        }
    .end annotation

    const-string v0, "2020-11_androidhardwaresurveyv1"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "filter { it != null }.map { it!! }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "storeExperiments.observe\u2026Null()\n          .take(1)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic runHardwareAnalytics$default(Lcom/discord/utilities/analytics/HardwareAnalytics;Landroid/content/Context;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    new-instance p2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;

    invoke-direct {p2, p1}, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;-><init>(Landroid/content/Context;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/analytics/HardwareAnalytics;->runHardwareAnalytics(Landroid/content/Context;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;)V

    return-void
.end method


# virtual methods
.method public final init()V
    .locals 10
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    sget-object v0, Lcom/discord/utilities/analytics/HardwareAnalytics;->initialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/utilities/analytics/HardwareAnalytics;->observeExperiment(Lcom/discord/stores/StoreExperiments;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/utilities/analytics/HardwareAnalytics;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/discord/utilities/analytics/HardwareAnalytics$init$1;->INSTANCE:Lcom/discord/utilities/analytics/HardwareAnalytics$init$1;

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final runHardwareAnalytics(Landroid/content/Context;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;)V
    .locals 11
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v10, Lcom/discord/hardware_analytics/BuildInfo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7f

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/discord/hardware_analytics/BuildInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    new-instance v1, Lcom/discord/hardware_analytics/MemoryInfo;

    invoke-direct {v1, p1}, Lcom/discord/hardware_analytics/MemoryInfo;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/discord/utilities/logging/LoggingProvider;->INSTANCE:Lcom/discord/utilities/logging/LoggingProvider;

    invoke-virtual {v2}, Lcom/discord/utilities/logging/LoggingProvider;->get()Lcom/discord/utilities/logging/Logger;

    move-result-object v3

    sget-object v5, Lcom/discord/hardware_analytics/HardwareSurveyService;->f:Lcom/discord/hardware_analytics/HardwareSurveyService$a;

    new-instance v4, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;

    invoke-direct {v4, v3, v10, v1, p2}, Lcom/discord/utilities/analytics/HardwareAnalytics$runHardwareAnalytics$1;-><init>(Lcom/discord/utilities/logging/Logger;Lcom/discord/hardware_analytics/BuildInfo;Lcom/discord/hardware_analytics/MemoryInfo;Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;)V

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "onComplete"

    invoke-static {v4, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    new-instance v6, Lf/a/c/a;

    invoke-direct {v6, v9, v4, p1}, Lf/a/c/a;-><init>(Landroid/os/Looper;Lkotlin/jvm/functions/Function1;Landroid/content/Context;)V

    const-string p2, "appContext"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "callbackLooper"

    invoke-static {v9, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lf/a/c/b;

    const-string v8, "HardwareSurveyService"

    move-object v4, p2

    move-object v7, v9

    invoke-direct/range {v4 .. v9}, Lf/a/c/b;-><init>(Lcom/discord/hardware_analytics/HardwareSurveyService$a;Lkotlin/jvm/functions/Function1;Landroid/os/Looper;Ljava/lang/String;Landroid/os/Looper;)V

    sget-object v1, Lf/a/c/e;->e:Lf/a/c/e$a;

    const-class v1, Lcom/discord/hardware_analytics/HardwareSurveyService;

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceClass"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/utilities/logging/LoggingProvider;->get()Lcom/discord/utilities/logging/Logger;

    move-result-object v3

    const-string v0, "Starting service in remote process: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", app pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    const-string v4, "RemoteIntentService"

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    new-instance v2, Landroid/os/Messenger;

    iget-object p2, p2, Lf/a/c/e$b;->a:Landroid/os/Handler;

    invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    const-string p2, "com.discord.utilities.analytics.RemoteIntentService.MESSENGER_KEY"

    invoke-virtual {v1, p2, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
