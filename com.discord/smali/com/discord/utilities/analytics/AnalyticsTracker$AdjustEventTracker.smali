.class public final Lcom/discord/utilities/analytics/AnalyticsTracker$AdjustEventTracker;
.super Ljava/lang/Object;
.source "AnalyticsTracker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/analytics/AnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AdjustEventTracker"
.end annotation


# static fields
.field private static final EVENT_TOKEN_LOGIN:Ljava/lang/String; = "ctt5aq"

.field private static final EVENT_TOKEN_REGISTER:Ljava/lang/String; = "ebn8ke"

.field public static final INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker$AdjustEventTracker;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/AnalyticsTracker$AdjustEventTracker;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker$AdjustEventTracker;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker$AdjustEventTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker$AdjustEventTracker;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final trackLogin()V
    .locals 3

    new-instance v0, Lf/c/a/o;

    const-string v1, "ctt5aq"

    invoke-direct {v0, v1}, Lf/c/a/o;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ls/a/b/b/a;->v()Lf/c/a/s;

    move-result-object v1

    invoke-virtual {v1}, Lf/c/a/s;->a()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, v1, Lf/c/a/s;->a:Lf/c/a/h0;

    invoke-interface {v1, v0}, Lf/c/a/h0;->o(Lf/c/a/o;)V

    :goto_0
    return-void
.end method

.method public final trackRegister()V
    .locals 3

    new-instance v0, Lf/c/a/o;

    const-string v1, "ebn8ke"

    invoke-direct {v0, v1}, Lf/c/a/o;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ls/a/b/b/a;->v()Lf/c/a/s;

    move-result-object v1

    invoke-virtual {v1}, Lf/c/a/s;->a()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, v1, Lf/c/a/s;->a:Lf/c/a/h0;

    invoke-interface {v1, v0}, Lf/c/a/h0;->o(Lf/c/a/o;)V

    :goto_0
    return-void
.end method
