.class public final Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;
.super Ljava/lang/Object;
.source "InstallReferrer.kt"

# interfaces
.implements Lf/e/b/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/analytics/InstallReferrer;->createReferrerStateListener(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $onReceivedInstallReferrer:Lkotlin/jvm/functions/Function1;

.field public final synthetic this$0:Lcom/discord/utilities/analytics/InstallReferrer;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/analytics/InstallReferrer;Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    iput-object p2, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->$onReceivedInstallReferrer:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInstallReferrerServiceDisconnected()V
    .locals 4

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    invoke-static {v0}, Lcom/discord/utilities/analytics/InstallReferrer;->access$getLogger$p(Lcom/discord/utilities/analytics/InstallReferrer;)Lcom/discord/utilities/logging/Logger;

    move-result-object v0

    const-string v1, "Install referrer service disconnected."

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void
.end method

.method public onInstallReferrerSetupFinished(I)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/utilities/analytics/InstallReferrer;->access$setFetchInstallReferrerFailed(Lcom/discord/utilities/analytics/InstallReferrer;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->$onReceivedInstallReferrer:Lkotlin/jvm/functions/Function1;

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    invoke-static {v0}, Lcom/discord/utilities/analytics/InstallReferrer;->access$getReferrerClient$p(Lcom/discord/utilities/analytics/InstallReferrer;)Lcom/android/installreferrer/api/InstallReferrerClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/installreferrer/api/InstallReferrerClient;->b()Lf/e/b/a/c;

    move-result-object v0

    const-string v1, "referrerClient.installReferrer"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lf/e/b/a/c;->a:Landroid/os/Bundle;

    const-string v1, "install_referrer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "referrerClient.installReferrer.installReferrer"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    invoke-static {p1}, Lcom/discord/utilities/analytics/InstallReferrer;->access$setFetchInstallReferrerSuccessful(Lcom/discord/utilities/analytics/InstallReferrer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    invoke-static {v0, p1}, Lcom/discord/utilities/analytics/InstallReferrer;->access$setFetchInstallReferrerFailed(Lcom/discord/utilities/analytics/InstallReferrer;Ljava/lang/Exception;)V

    :goto_0
    :try_start_1
    iget-object p1, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    invoke-static {p1}, Lcom/discord/utilities/analytics/InstallReferrer;->access$getReferrerClient$p(Lcom/discord/utilities/analytics/InstallReferrer;)Lcom/android/installreferrer/api/InstallReferrerClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/installreferrer/api/InstallReferrerClient;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    iget-object v0, p0, Lcom/discord/utilities/analytics/InstallReferrer$createReferrerStateListener$1;->this$0:Lcom/discord/utilities/analytics/InstallReferrer;

    invoke-static {v0}, Lcom/discord/utilities/analytics/InstallReferrer;->access$getLogger$p(Lcom/discord/utilities/analytics/InstallReferrer;)Lcom/discord/utilities/logging/Logger;

    move-result-object v0

    const-string v1, "Unable to end connection, likely already dead."

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/logging/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
