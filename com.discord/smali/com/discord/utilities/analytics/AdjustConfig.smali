.class public final Lcom/discord/utilities/analytics/AdjustConfig;
.super Ljava/lang/Object;
.source "AdjustConfig.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/analytics/AdjustConfig$AdjustLifecycleListener;
    }
.end annotation


# static fields
.field private static final ADJUST_APP_TOKEN:Ljava/lang/String; = "d8fcx8xdmrr4"

.field private static final ADJUST_ENVIRONMENT:Ljava/lang/String;

.field public static final INSTANCE:Lcom/discord/utilities/analytics/AdjustConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/AdjustConfig;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/AdjustConfig;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/AdjustConfig;->INSTANCE:Lcom/discord/utilities/analytics/AdjustConfig;

    const-string v0, "production"

    sput-object v0, Lcom/discord/utilities/analytics/AdjustConfig;->ADJUST_ENVIRONMENT:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final init(Landroid/app/Application;Z)V
    .locals 6

    const-string v0, "application"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    return-void

    :cond_0
    new-instance p2, Lf/c/a/n;

    sget-object v0, Lcom/discord/utilities/analytics/AdjustConfig;->ADJUST_ENVIRONMENT:Ljava/lang/String;

    const-string v1, "d8fcx8xdmrr4"

    invoke-direct {p2, p1, v1, v0}, Lf/c/a/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/analytics/AdjustConfig$init$1$1;->INSTANCE:Lcom/discord/utilities/analytics/AdjustConfig$init$1$1;

    iput-object v0, p2, Lf/c/a/n;->e:Lf/c/a/s0;

    invoke-static {}, Ls/a/b/b/a;->v()Lf/c/a/s;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lf/c/a/n;->a()Z

    move-result v1

    const-string v2, "AdjustConfig not initialized correctly"

    const/4 v3, 0x0

    if-nez v1, :cond_1

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object p2

    new-array v0, v3, [Ljava/lang/Object;

    invoke-interface {p2, v2, v0}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v1, v0, Lf/c/a/s;->a:Lf/c/a/h0;

    if-eqz v1, :cond_2

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object p2

    new-array v0, v3, [Ljava/lang/Object;

    const-string v1, "Adjust already initialized"

    invoke-interface {p2, v1, v0}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p2}, Lf/c/a/n;->a()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    invoke-interface {v4, v2, v5}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    new-instance v1, Lf/c/a/a;

    invoke-direct {v1, p2}, Lf/c/a/a;-><init>(Lf/c/a/n;)V

    :goto_0
    iput-object v1, v0, Lf/c/a/s;->a:Lf/c/a/h0;

    iget-object p2, p2, Lf/c/a/n;->a:Landroid/content/Context;

    new-instance v1, Lf/c/a/q;

    invoke-direct {v1, v0, p2}, Lf/c/a/q;-><init>(Lf/c/a/s;Landroid/content/Context;)V

    invoke-static {v1}, Lf/c/a/l1;->A(Ljava/lang/Runnable;)V

    :goto_1
    sget-object p2, Lcom/discord/utilities/analytics/AdjustConfig$init$2;->INSTANCE:Lcom/discord/utilities/analytics/AdjustConfig$init$2;

    sget-object v0, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_4

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "GoogleAdId being read in the background"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lf/c/a/l1;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GoogleAdId read "

    invoke-static {v2, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v0, v2, v3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p2, v1}, Lf/c/a/t0;->onGoogleAdIdRead(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "GoogleAdId being read in the foreground"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lf/c/a/m1;

    invoke-direct {v0, p2}, Lf/c/a/m1;-><init>(Lf/c/a/t0;)V

    const/4 p2, 0x1

    new-array p2, p2, [Landroid/content/Context;

    aput-object p1, p2, v3

    invoke-virtual {v0, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_2
    sget-object p2, Lcom/discord/utilities/analytics/InstallReferrer;->Companion:Lcom/discord/utilities/analytics/InstallReferrer$Companion;

    new-instance v0, Lcom/discord/utilities/analytics/AdjustConfig$init$3;

    invoke-direct {v0, p1}, Lcom/discord/utilities/analytics/AdjustConfig$init$3;-><init>(Landroid/app/Application;)V

    invoke-virtual {p2, v0}, Lcom/discord/utilities/analytics/InstallReferrer$Companion;->init(Lkotlin/jvm/functions/Function1;)V

    new-instance p2, Lcom/discord/utilities/analytics/AdjustConfig$AdjustLifecycleListener;

    invoke-direct {p2}, Lcom/discord/utilities/analytics/AdjustConfig$AdjustLifecycleListener;-><init>()V

    invoke-virtual {p1, p2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method
