.class public final Lcom/discord/utilities/analytics/AdjustConfig$init$3;
.super Lx/m/c/k;
.source "AdjustConfig.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/analytics/AdjustConfig;->init(Landroid/app/Application;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $application:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/analytics/AdjustConfig$init$3;->$application:Landroid/app/Application;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/analytics/AdjustConfig$init$3;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 8

    const-string v0, "referrerUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/utilities/analytics/AdjustConfig$init$3;->$application:Landroid/app/Application;

    invoke-static {}, Ls/a/b/b/a;->v()Lf/c/a/s;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v7, Lf/c/a/r;

    move-object v1, v7

    move-object v2, v0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lf/c/a/r;-><init>(Lf/c/a/s;Landroid/content/Context;Ljava/lang/String;J)V

    invoke-static {v7}, Lf/c/a/l1;->A(Ljava/lang/Runnable;)V

    const-string v1, "referrer"

    invoke-virtual {v0, v1}, Lf/c/a/s;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lf/c/a/s;->a:Lf/c/a/h0;

    invoke-interface {v1}, Lf/c/a/h0;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lf/c/a/s;->a:Lf/c/a/h0;

    invoke-interface {v0}, Lf/c/a/h0;->k()V

    :cond_1
    :goto_0
    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticSuperProperties;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->setCampaignProperties(Ljava/lang/String;)V

    return-void
.end method
