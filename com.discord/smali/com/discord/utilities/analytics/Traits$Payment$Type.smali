.class public final Lcom/discord/utilities/analytics/Traits$Payment$Type;
.super Ljava/lang/Object;
.source "Traits.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/analytics/Traits$Payment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Type"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/analytics/Traits$Payment$Type;

.field public static final STICKER:Ljava/lang/String; = "sticker"

.field public static final SUBSCRIPTION:Ljava/lang/String; = "subscription"


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/analytics/Traits$Payment$Type;

    invoke-direct {v0}, Lcom/discord/utilities/analytics/Traits$Payment$Type;-><init>()V

    sput-object v0, Lcom/discord/utilities/analytics/Traits$Payment$Type;->INSTANCE:Lcom/discord/utilities/analytics/Traits$Payment$Type;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
