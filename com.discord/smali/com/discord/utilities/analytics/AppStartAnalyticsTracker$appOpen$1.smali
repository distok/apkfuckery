.class public final Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;
.super Lx/m/c/k;
.source "AppStartAnalyticsTracker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->appOpen(Landroid/net/Uri;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "+",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $isNotificationRoute:Z

.field public final synthetic $uri:Landroid/net/Uri;

.field public final synthetic $uriCanBeRouted:Z

.field public final synthetic this$0:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;ZZLandroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->this$0:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    iput-boolean p2, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->$isNotificationRoute:Z

    iput-boolean p3, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->$uriCanBeRouted:Z

    iput-object p4, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->$uri:Landroid/net/Uri;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->invoke()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/Pair;

    iget-boolean v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->$isNotificationRoute:Z

    if-eqz v1, :cond_0

    const-string v1, "notification"

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->$uriCanBeRouted:Z

    if-eqz v1, :cond_1

    const-string v1, "deeplink"

    goto :goto_0

    :cond_1
    const-string v1, "launcher"

    :goto_0
    new-instance v2, Lkotlin/Pair;

    const-string v3, "opened_from"

    invoke-direct {v2, v3, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->this$0:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    invoke-static {v2}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->access$getStoreUserSettings$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Lcom/discord/stores/StoreUserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string/jumbo v4, "theme"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    invoke-static {v0}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->Companion:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;

    iget-object v2, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->$uri:Landroid/net/Uri;

    invoke-static {v1, v0, v2}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;->access$insertUriProperties(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$Companion;Ljava/util/Map;Landroid/net/Uri;)Ljava/util/Map;

    iget-object v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->this$0:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    invoke-static {v1}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->access$getAppOpenTimestamp$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->this$0:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    invoke-static {v1}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->access$getOpenAppLoadId$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "load_id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker$appOpen$1;->this$0:Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;

    invoke-static {v1}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->access$getClock$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;)Lcom/discord/utilities/time/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;->access$setAppOpenTimestamp$p(Lcom/discord/utilities/analytics/AppStartAnalyticsTracker;Ljava/lang/Long;)V

    :cond_2
    return-object v0
.end method
