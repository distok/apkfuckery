.class public final Lcom/discord/utilities/birthday/BirthdayHelper;
.super Ljava/lang/Object;
.source "BirthdayHelper.kt"


# static fields
.field public static final DATE_OF_BIRTH_KEY:Ljava/lang/String; = "date_of_birth"

.field public static final INSTANCE:Lcom/discord/utilities/birthday/BirthdayHelper;

.field public static final USER_DEFAULT_AGE:I = 0xa

.field public static final USER_MIN_AGE_NSFW:I = 0x12


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/birthday/BirthdayHelper;

    invoke-direct {v0}, Lcom/discord/utilities/birthday/BirthdayHelper;-><init>()V

    sput-object v0, Lcom/discord/utilities/birthday/BirthdayHelper;->INSTANCE:Lcom/discord/utilities/birthday/BirthdayHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final defaultInputAge()J
    .locals 2

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/discord/utilities/birthday/BirthdayHelper;->subtractYearsFromToday(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getAge(J)I
    .locals 5

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    const-string v4, "nowCalendar"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v0, "dobCalendar"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 p1, 0x2

    invoke-virtual {v3, p1}, Ljava/util/Calendar;->get(I)I

    move-result p2

    invoke-virtual {v2, p1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    if-gt p2, v0, :cond_1

    invoke-virtual {v3, p1}, Ljava/util/Calendar;->get(I)I

    move-result p2

    invoke-virtual {v2, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    if-ne p2, p1, :cond_0

    const/4 p1, 0x5

    invoke-virtual {v3, p1}, Ljava/util/Calendar;->get(I)I

    move-result p2

    invoke-virtual {v2, p1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    if-le p2, p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result p2

    sub-int/2addr p1, p2

    return p1

    :cond_1
    :goto_0
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result p2

    sub-int/2addr p1, p2

    sub-int/2addr p1, v1

    return p1
.end method

.method public final getMaxDateOfBirth()J
    .locals 2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/discord/utilities/birthday/BirthdayHelper;->subtractYearsFromToday(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final subtractYearsFromToday(I)J
    .locals 4

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const-string v3, "calendar"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sub-int/2addr v1, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method
