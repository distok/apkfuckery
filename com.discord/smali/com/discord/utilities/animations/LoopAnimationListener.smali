.class public final Lcom/discord/utilities/animations/LoopAnimationListener;
.super Ljava/lang/Object;
.source "LottieAnimationUtils.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final animationView:Lcom/airbnb/lottie/LottieAnimationView;

.field private final loopFrames:Lkotlin/ranges/IntRange;

.field private final triggerFrame:I


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/LottieAnimationView;ILkotlin/ranges/IntRange;)V
    .locals 1

    const-string v0, "animationView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loopFrames"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->animationView:Lcom/airbnb/lottie/LottieAnimationView;

    iput p2, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->triggerFrame:I

    iput-object p3, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->loopFrames:Lkotlin/ranges/IntRange;

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->animationView:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->getFrame()I

    move-result p1

    iget v0, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->triggerFrame:I

    if-lt p1, v0, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->animationView:Lcom/airbnb/lottie/LottieAnimationView;

    iget-object v0, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->loopFrames:Lkotlin/ranges/IntRange;

    iget v1, v0, Lkotlin/ranges/IntProgression;->d:I

    iget v0, v0, Lkotlin/ranges/IntProgression;->e:I

    iget-object p1, p1, Lcom/airbnb/lottie/LottieAnimationView;->h:Lf/d/a/j;

    invoke-virtual {p1, v1, v0}, Lf/d/a/j;->p(II)V

    iget-object p1, p0, Lcom/discord/utilities/animations/LoopAnimationListener;->animationView:Lcom/airbnb/lottie/LottieAnimationView;

    iget-object p1, p1, Lcom/airbnb/lottie/LottieAnimationView;->h:Lf/d/a/j;

    iget-object p1, p1, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iget-object p1, p1, Lf/d/a/b0/a;->d:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
