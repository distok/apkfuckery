.class public final Lcom/discord/utilities/animations/LottieAnimationUtilsKt;
.super Ljava/lang/Object;
.source "LottieAnimationUtils.kt"


# direct methods
.method public static final loopFrom(Lcom/airbnb/lottie/LottieAnimationView;ILkotlin/ranges/IntRange;)V
    .locals 1

    const-string v0, "$this$loopFrom"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loopFrames"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/animations/LoopAnimationListener;

    invoke-direct {v0, p0, p1, p2}, Lcom/discord/utilities/animations/LoopAnimationListener;-><init>(Lcom/airbnb/lottie/LottieAnimationView;ILkotlin/ranges/IntRange;)V

    iget-object p0, p0, Lcom/airbnb/lottie/LottieAnimationView;->h:Lf/d/a/j;

    iget-object p0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iget-object p0, p0, Lf/d/a/b0/a;->d:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method
