.class public final enum Lcom/discord/utilities/experiments/RegisteredExperiment$Type;
.super Ljava/lang/Enum;
.source "ExperimentRegistry.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/experiments/RegisteredExperiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/utilities/experiments/RegisteredExperiment$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

.field public static final enum GUILD:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

.field public static final enum USER:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    new-instance v1, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    const-string v2, "GUILD"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;->GUILD:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    const-string v2, "USER"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;->USER:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;->$VALUES:[Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/utilities/experiments/RegisteredExperiment$Type;
    .locals 1

    const-class v0, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    return-object p0
.end method

.method public static values()[Lcom/discord/utilities/experiments/RegisteredExperiment$Type;
    .locals 1

    sget-object v0, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;->$VALUES:[Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    invoke-virtual {v0}, [Lcom/discord/utilities/experiments/RegisteredExperiment$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    return-object v0
.end method
