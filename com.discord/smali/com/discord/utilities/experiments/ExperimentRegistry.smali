.class public final Lcom/discord/utilities/experiments/ExperimentRegistry;
.super Ljava/lang/Object;
.source "ExperimentRegistry.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/experiments/ExperimentRegistry;

.field private static final registeredExperiments:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/experiments/RegisteredExperiment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    new-instance v0, Lcom/discord/utilities/experiments/ExperimentRegistry;

    invoke-direct {v0}, Lcom/discord/utilities/experiments/ExperimentRegistry;-><init>()V

    sput-object v0, Lcom/discord/utilities/experiments/ExperimentRegistry;->INSTANCE:Lcom/discord/utilities/experiments/ExperimentRegistry;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/discord/utilities/experiments/ExperimentRegistry;->registeredExperiments:Ljava/util/LinkedHashMap;

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/discord/utilities/experiments/RegisteredExperiment;

    new-instance v8, Lcom/discord/utilities/experiments/RegisteredExperiment;

    sget-object v9, Lcom/discord/utilities/experiments/RegisteredExperiment$Type;->USER:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    const-string v10, "Control"

    const-string v2, "Treatment 1: 0.5 seconds delay"

    const-string v3, "Treatment 2: 1 second delay"

    const-string v4, "Treatment 3: 2 seconds delay"

    const-string v5, "Treatment 4: 4 seconds delay"

    filled-new-array {v10, v2, v3, v4, v5}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v3, "Slow TTI Experiment"

    const-string v4, "2020-08_android_tti_delay"

    const/4 v7, 0x0

    move-object v2, v8

    move-object v5, v9

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    const/4 v2, 0x0

    aput-object v8, v1, v2

    new-instance v8, Lcom/discord/utilities/experiments/RegisteredExperiment;

    const-string v2, "Treatment 1: Minimal UI Changes"

    const-string v3, "Treatment 2: Full UI Changes"

    const-string v4, "Treatment 3: Full UI Changes & User Setting"

    filled-new-array {v10, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v3, "Mobile Image Compression"

    const-string v4, "2020-09_mobile_image_compression"

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    const/4 v2, 0x1

    aput-object v8, v1, v2

    new-instance v8, Lcom/discord/utilities/experiments/RegisteredExperiment;

    const-string v2, "Treatment 1: Enable slash commands"

    filled-new-array {v10, v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v3, "App Slash Commands"

    const-string v4, "2020-11_android_app_slash_commands"

    const/4 v7, 0x1

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    const/4 v2, 0x2

    aput-object v8, v1, v2

    new-instance v8, Lcom/discord/utilities/experiments/RegisteredExperiment;

    const-string v2, "Treatment 1: Use compact view"

    filled-new-array {v10, v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v3, "Compact Invite Widget"

    const-string v4, "2020-01_mobile_invite_suggestion_compact"

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    const/4 v2, 0x3

    aput-object v8, v1, v2

    new-instance v8, Lcom/discord/utilities/experiments/RegisteredExperiment;

    const-string v2, "Treatment 1: Run hardware analytics"

    filled-new-array {v10, v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v3, "Hardware Analytics"

    const-string v4, "2020-11_androidhardwaresurveyv1"

    const/4 v7, 0x0

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    const/4 v2, 0x4

    aput-object v8, v1, v2

    new-instance v8, Lcom/discord/utilities/experiments/RegisteredExperiment;

    const-string v2, "Treatment 1: Enable Screen Share"

    filled-new-array {v10, v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const-string v3, "Android Screen Share"

    const-string v4, "2020-08_android_screenshare"

    const/4 v7, 0x1

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    const/4 v2, 0x5

    aput-object v8, v1, v2

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/utilities/experiments/RegisteredExperiment;

    invoke-virtual {v3}, Lcom/discord/utilities/experiments/RegisteredExperiment;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getRegisteredExperiments()Ljava/util/LinkedHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/experiments/RegisteredExperiment;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/experiments/ExperimentRegistry;->registeredExperiments:Ljava/util/LinkedHashMap;

    return-object v0
.end method
