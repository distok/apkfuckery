.class public final Lcom/discord/utilities/experiments/RegisteredExperiment;
.super Ljava/lang/Object;
.source "ExperimentRegistry.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/experiments/RegisteredExperiment$Type;
    }
.end annotation


# instance fields
.field private final buckets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheExperiment:Z

.field private final name:Ljava/lang/String;

.field private final readableName:Ljava/lang/String;

.field private final type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/experiments/RegisteredExperiment$Type;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "readableName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buckets"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    iput-object p4, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    iput-boolean p5, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/experiments/RegisteredExperiment;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;ZILjava/lang/Object;)Lcom/discord/utilities/experiments/RegisteredExperiment;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/utilities/experiments/RegisteredExperiment;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)Lcom/discord/utilities/experiments/RegisteredExperiment;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/discord/utilities/experiments/RegisteredExperiment$Type;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)Lcom/discord/utilities/experiments/RegisteredExperiment;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/experiments/RegisteredExperiment$Type;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/discord/utilities/experiments/RegisteredExperiment;"
        }
    .end annotation

    const-string v0, "readableName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buckets"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/experiments/RegisteredExperiment;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/experiments/RegisteredExperiment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/experiments/RegisteredExperiment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/experiments/RegisteredExperiment;

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    iget-object v1, p1, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    iget-boolean p1, p1, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBuckets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    return-object v0
.end method

.method public final getCacheExperiment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getReadableName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/discord/utilities/experiments/RegisteredExperiment$Type;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "RegisteredExperiment(readableName="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->readableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->type:Lcom/discord/utilities/experiments/RegisteredExperiment$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", buckets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->buckets:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cacheExperiment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/utilities/experiments/RegisteredExperiment;->cacheExperiment:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
