.class public final Lcom/discord/utilities/experiments/ExperimentUtils;
.super Ljava/lang/Object;
.source "ExperimentUtils.kt"


# static fields
.field public static final BUCKET_NOT_ELIGIBLE:I = -0x1

.field public static final INSTANCE:Lcom/discord/utilities/experiments/ExperimentUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/experiments/ExperimentUtils;

    invoke-direct {v0}, Lcom/discord/utilities/experiments/ExperimentUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/experiments/ExperimentUtils;->INSTANCE:Lcom/discord/utilities/experiments/ExperimentUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final computeGuildExperimentBucket(Ljava/lang/String;JILcom/discord/models/experiments/dto/GuildExperimentDto;)I
    .locals 8

    const-string v0, "experimentName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "experiment"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getOverrides()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->getGuilds()Ljava/util/List;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->getBucket()I

    move-result p1

    return p1

    :cond_1
    invoke-virtual {p5}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getFilters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter;

    instance-of v4, v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;

    const/4 v5, -0x1

    if-eqz v4, :cond_3

    check-cast v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;->getGuildIds()Ljava/util/Set;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v5

    :cond_3
    instance-of v4, v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;

    if-eqz v4, :cond_5

    check-cast v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;->getRange()Lkotlin/ranges/LongRange;

    move-result-object v1

    iget-wide v6, v1, Lx/p/c;->d:J

    cmp-long v4, v6, p2

    if-gtz v4, :cond_4

    iget-wide v6, v1, Lx/p/c;->e:J

    cmp-long v1, p2, v6

    if-gtz v1, :cond_4

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_2

    return v5

    :cond_5
    instance-of v2, v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;->getRange()Lkotlin/ranges/LongRange;

    move-result-object v1

    const-string v2, "$this$contains"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-long v2, p4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/ranges/LongRange;->contains(Ljava/lang/Comparable;)Z

    move-result v1

    if-nez v1, :cond_2

    return v5

    :cond_6
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getHashKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    move-object p1, v0

    :cond_7
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3a

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    invoke-virtual {p2, p1}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide p1

    const/16 p3, 0x2710

    int-to-long p3, p3

    rem-long/2addr p1, p3

    invoke-virtual {p5}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getBuckets()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_8
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_d

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    move-object p5, p4

    check-cast p5, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;

    invoke-virtual {p5}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;->getPositions()Ljava/util/List;

    move-result-object p5

    instance-of v0, p5, Ljava/util/Collection;

    if-eqz v0, :cond_9

    invoke-interface {p5}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_2

    :cond_9
    invoke-interface {p5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :cond_a
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/ranges/IntRange;

    iget v1, v0, Lkotlin/ranges/IntProgression;->d:I

    int-to-long v4, v1

    cmp-long v1, p1, v4

    if-ltz v1, :cond_b

    iget v0, v0, Lkotlin/ranges/IntProgression;->e:I

    int-to-long v0, v0

    cmp-long v4, p1, v0

    if-gez v4, :cond_b

    const/4 v0, 0x1

    goto :goto_1

    :cond_b
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_a

    const/4 p5, 0x1

    goto :goto_3

    :cond_c
    :goto_2
    const/4 p5, 0x0

    :goto_3
    if-eqz p5, :cond_8

    goto :goto_4

    :cond_d
    const/4 p4, 0x0

    :goto_4
    check-cast p4, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;

    if-eqz p4, :cond_e

    invoke-virtual {p4}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;->getBucket()I

    move-result v3

    :cond_e
    return v3
.end method
