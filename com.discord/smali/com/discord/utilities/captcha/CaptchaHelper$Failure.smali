.class public final Lcom/discord/utilities/captcha/CaptchaHelper$Failure;
.super Ljava/lang/Throwable;
.source "CaptchaHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/captcha/CaptchaHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failure"
.end annotation


# instance fields
.field private final errorStringId:I

.field private final reasonCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    const-string v0, "reasonCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Throwable;-><init>()V

    iput p1, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    iput-object p2, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/captcha/CaptchaHelper$Failure;ILjava/lang/String;ILjava/lang/Object;)Lcom/discord/utilities/captcha/CaptchaHelper$Failure;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->copy(ILjava/lang/String;)Lcom/discord/utilities/captcha/CaptchaHelper$Failure;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ILjava/lang/String;)Lcom/discord/utilities/captcha/CaptchaHelper$Failure;
    .locals 1

    const-string v0, "reasonCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;

    invoke-direct {v0, p1, p2}, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;

    iget v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    iget v1, p1, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getErrorStringId()I
    .locals 1

    iget v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    return v0
.end method

.method public final getReasonCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Failure(errorStringId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->errorStringId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", reasonCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;->reasonCode:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
