.class public final Lcom/discord/utilities/captcha/CaptchaHelper;
.super Ljava/lang/Object;
.source "CaptchaHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/captcha/CaptchaHelper$Failure;
    }
.end annotation


# static fields
.field public static final CAPTCHA_KEY:Ljava/lang/String; = "captcha_key"

.field private static final CAPTCHA_SITE_KEY:Ljava/lang/String; = "6Lff5jIUAAAAAImNXvYYPv2VW2En3Dexy4oX2o4s"

.field private static final FAILED_CAPTCHA_EXPIRED:Ljava/lang/String; = "expired"

.field private static final FAILED_DEVICE_UNSUPPORTED:Ljava/lang/String; = "unsupported_device"

.field private static final FAILED_MISSING_DEPS:Ljava/lang/String; = "missing_dependencies"

.field public static final INSTANCE:Lcom/discord/utilities/captcha/CaptchaHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/captcha/CaptchaHelper;

    invoke-direct {v0}, Lcom/discord/utilities/captcha/CaptchaHelper;-><init>()V

    sput-object v0, Lcom/discord/utilities/captcha/CaptchaHelper;->INSTANCE:Lcom/discord/utilities/captcha/CaptchaHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$ensurePlayServicesAvailable(Lcom/discord/utilities/captcha/CaptchaHelper;Lcom/google/android/gms/common/GoogleApiAvailability;Landroid/app/Activity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/utilities/captcha/CaptchaHelper;->ensurePlayServicesAvailable(Lcom/google/android/gms/common/GoogleApiAvailability;Landroid/app/Activity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$showCaptcha(Lcom/discord/utilities/captcha/CaptchaHelper;Lcom/google/android/gms/safetynet/SafetyNetClient;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/utilities/captcha/CaptchaHelper;->showCaptcha(Lcom/google/android/gms/safetynet/SafetyNetClient;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final ensurePlayServicesAvailable(Lcom/google/android/gms/common/GoogleApiAvailability;Landroid/app/Activity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            "Landroid/app/Activity;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/captcha/CaptchaHelper$Failure;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1, p2}, Lcom/google/android/gms/common/GoogleApiAvailability;->c(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/GoogleApiAvailability;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, p2}, Lcom/google/android/gms/common/GoogleApiAvailability;->e(Landroid/app/Activity;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance p2, Lcom/discord/utilities/captcha/CaptchaHelper$ensurePlayServicesAvailable$1;

    invoke-direct {p2, p3}, Lcom/discord/utilities/captcha/CaptchaHelper$ensurePlayServicesAvailable$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lf/h/a/f/p/b0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p3, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, p3, p2}, Lf/h/a/f/p/b0;->g(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;

    new-instance p2, Lcom/discord/utilities/captcha/CaptchaHelper$ensurePlayServicesAvailable$2;

    invoke-direct {p2, p4}, Lcom/discord/utilities/captcha/CaptchaHelper$ensurePlayServicesAvailable$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1, p3, p2}, Lf/h/a/f/p/b0;->e(Ljava/util/concurrent/Executor;Lf/h/a/f/p/d;)Lcom/google/android/gms/tasks/Task;

    const-string p2, "makeGooglePlayServicesAv\u2026ISSING_DEPS))\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    invoke-interface {p3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;

    const p2, 0x7f1203fd

    const-string/jumbo p3, "unsupported_device"

    invoke-direct {p1, p2, p3}, Lcom/discord/utilities/captcha/CaptchaHelper$Failure;-><init>(ILjava/lang/String;)V

    invoke-interface {p4, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method private final showCaptcha(Lcom/google/android/gms/safetynet/SafetyNetClient;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/captcha/CaptchaHelper$Failure;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/h/a/f/m/a;->d:Lcom/google/android/gms/safetynet/SafetyNetApi;

    iget-object p1, p1, Lf/h/a/f/f/h/b;->g:Lf/h/a/f/f/h/c;

    check-cast v0, Lf/h/a/f/i/n/i;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "6Lff5jIUAAAAAImNXvYYPv2VW2En3Dexy4oX2o4s"

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lf/h/a/f/i/n/j;

    invoke-direct {v1, p1, v0}, Lf/h/a/f/i/n/j;-><init>(Lf/h/a/f/f/h/c;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lf/h/a/f/f/h/c;->a(Lf/h/a/f/f/h/i/d;)Lf/h/a/f/f/h/i/d;

    move-result-object p1

    new-instance v0, Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;

    invoke-direct {v0}, Lcom/google/android/gms/safetynet/SafetyNetApi$RecaptchaTokenResponse;-><init>()V

    new-instance v1, Lf/h/a/f/f/k/u;

    invoke-direct {v1, v0}, Lf/h/a/f/f/k/u;-><init>(Lf/h/a/f/f/h/f;)V

    sget-object v0, Lf/h/a/f/f/k/k;->a:Lf/h/a/f/f/k/k$b;

    new-instance v2, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v3, Lf/h/a/f/f/k/s;

    invoke-direct {v3, p1, v2, v1, v0}, Lf/h/a/f/f/k/s;-><init>(Lf/h/a/f/f/h/d;Lcom/google/android/gms/tasks/TaskCompletionSource;Lf/h/a/f/f/k/k$a;Lf/h/a/f/f/k/k$b;)V

    invoke-virtual {p1, v3}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->c(Lf/h/a/f/f/h/d$a;)V

    iget-object p1, v2, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    new-instance v0, Lcom/discord/utilities/captcha/CaptchaHelper$showCaptcha$1;

    invoke-direct {v0, p2}, Lcom/discord/utilities/captcha/CaptchaHelper$showCaptcha$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, p2, v0}, Lf/h/a/f/p/b0;->g(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;

    new-instance v0, Lcom/discord/utilities/captcha/CaptchaHelper$showCaptcha$2;

    invoke-direct {v0, p3}, Lcom/discord/utilities/captcha/CaptchaHelper$showCaptcha$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1, p2, v0}, Lf/h/a/f/p/b0;->e(Ljava/util/concurrent/Executor;Lf/h/a/f/p/d;)Lcom/google/android/gms/tasks/Task;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Null or empty site key in verifyWithRecaptcha"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic showCaptchaHelpDialog$default(Lcom/discord/utilities/captcha/CaptchaHelper;Lcom/discord/app/AppActivity;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/captcha/CaptchaHelper;->showCaptchaHelpDialog(Lcom/discord/app/AppActivity;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public final showCaptchaHelpDialog(Lcom/discord/app/AppActivity;Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppActivity;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    invoke-direct {v1, p1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120400

    invoke-virtual {v1, v2}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setTitle(I)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v1

    const v2, 0x7f120401

    invoke-virtual {v1, v2}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setMessage(I)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object v1

    const v2, 0x7f1203ff

    new-instance v3, Lcom/discord/utilities/captcha/CaptchaHelper$showCaptchaHelpDialog$$inlined$let$lambda$1;

    invoke-direct {v3, p1, p2}, Lcom/discord/utilities/captcha/CaptchaHelper$showCaptchaHelpDialog$$inlined$let$lambda$1;-><init>(Lcom/discord/app/AppActivity;Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v1, v2, v3}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setPositiveButton(ILkotlin/jvm/functions/Function1;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object p1

    const p2, 0x7f1203f1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, p2, v2, v1, v2}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setNegativeButton$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;ILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object p1

    const-string p2, "it"

    invoke-static {v0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->show(Landroidx/fragment/app/FragmentManager;)V

    :cond_0
    return-void
.end method

.method public final tryShowCaptcha(Landroid/app/Activity;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(null)"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/discord/utilities/captcha/CaptchaHelper$tryShowCaptcha$1;

    invoke-direct {v0, p1}, Lcom/discord/utilities/captcha/CaptchaHelper$tryShowCaptcha$1;-><init>(Landroid/app/Activity;)V

    sget-object p1, Lrx/Emitter$BackpressureMode;->f:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, p1}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable.create({ emit\u2026.BackpressureMode.BUFFER)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
