.class public final Lcom/discord/utilities/press/RepeatingOnTouchListener$subscribe$1;
.super Ljava/lang/Object;
.source "RepeatingOnTouchListener.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/press/RepeatingOnTouchListener;->subscribe()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "+",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/utilities/press/RepeatingOnTouchListener;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/press/RepeatingOnTouchListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/press/RepeatingOnTouchListener$subscribe$1;->this$0:Lcom/discord/utilities/press/RepeatingOnTouchListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/press/RepeatingOnTouchListener$subscribe$1;->call(Ljava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Long;)Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object p1, p0, Lcom/discord/utilities/press/RepeatingOnTouchListener$subscribe$1;->this$0:Lcom/discord/utilities/press/RepeatingOnTouchListener;

    invoke-virtual {p1}, Lcom/discord/utilities/press/RepeatingOnTouchListener;->getRepeatRate()J

    move-result-wide v2

    iget-object p1, p0, Lcom/discord/utilities/press/RepeatingOnTouchListener$subscribe$1;->this$0:Lcom/discord/utilities/press/RepeatingOnTouchListener;

    invoke-virtual {p1}, Lcom/discord/utilities/press/RepeatingOnTouchListener;->getTimeUnit()Ljava/util/concurrent/TimeUnit;

    move-result-object v4

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v5

    move-wide v0, v2

    invoke-static/range {v0 .. v5}, Lrx/Observable;->B(JJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
