.class public final synthetic Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$1;
.super Lx/m/c/i;
.source "StickerUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/dsti/StickerUtils;->getShowUpsellDialogCallback(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lkotlin/jvm/functions/Function3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function3<",
        "Landroidx/fragment/app/FragmentManager;",
        "Lkotlin/jvm/functions/Function0<",
        "+",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/jvm/functions/Function0<",
        "+",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog$Companion;)V
    .locals 7

    const-class v3, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog$Companion;

    const/4 v1, 0x3

    const-string v4, "show"

    const-string v5, "show(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroidx/fragment/app/FragmentManager;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$1;->invoke(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog$Companion;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "fragmentManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onUpgradeClickListener"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog;

    invoke-direct {v0}, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog;-><init>()V

    iput-object p2, v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog;->d:Lkotlin/jvm/functions/Function0;

    iput-object p3, v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog;->e:Lkotlin/jvm/functions/Function0;

    const-class p2, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
