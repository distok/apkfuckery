.class public final Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;
.super Lx/m/c/k;
.source "StickerUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/dsti/StickerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;

    invoke-direct {v0}, Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;-><init>()V

    sput-object v0, Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()I
    .locals 1

    const/16 v0, 0xa0

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    return v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;->invoke()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
