.class public final Lcom/discord/utilities/dsti/StickerUtils;
.super Ljava/lang/Object;
.source "StickerUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;
    }
.end annotation


# static fields
.field private static final CREATE_METADATA_FAILED_NOTICE:Ljava/lang/String; = "CREATE_METADATA_FAILED_NOTICE"

.field private static final DEFAULT_HEADER_SIZE_PX$delegate:Lkotlin/Lazy;

.field private static final DEFAULT_STICKER_SIZE_PX$delegate:Lkotlin/Lazy;

.field public static final INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

.field public static final PREMIUM_TIER_2_STICKERS_DISCOUNT_PERCENTAGE:I = 0x21


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/utilities/dsti/StickerUtils;

    invoke-direct {v0}, Lcom/discord/utilities/dsti/StickerUtils;-><init>()V

    sput-object v0, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_STICKER_SIZE_PX$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/dsti/StickerUtils;->DEFAULT_STICKER_SIZE_PX$delegate:Lkotlin/Lazy;

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_HEADER_SIZE_PX$2;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils$DEFAULT_HEADER_SIZE_PX$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/utilities/dsti/StickerUtils;->DEFAULT_HEADER_SIZE_PX$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$purchaseStickerPack(Lcom/discord/utilities/dsti/StickerUtils;Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/utilities/dsti/StickerUtils;->purchaseStickerPack(Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-void
.end method

.method private final canPurchaseStickerPack(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z
    .locals 3

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p2, v0, :cond_0

    sget-object p2, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {p2, p3, v0}, Lcom/discord/utilities/premium/PremiumUtils;->isPremiumTierAtLeast(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result p1

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    return v1
.end method

.method public static synthetic getBannerCDNAssetUrl$default(Lcom/discord/utilities/dsti/StickerUtils;JLcom/discord/models/store/dto/ModelStoreAsset;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/dsti/StickerUtils;->getBannerCDNAssetUrl(JLcom/discord/models/store/dto/ModelStoreAsset;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic getCDNAssetUrl$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x1

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/utilities/dsti/StickerUtils;->getCDNAssetUrl(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final getPaymentStartedLocationTrait(Lcom/discord/utilities/analytics/Traits$Location;Z)Lcom/discord/utilities/analytics/Traits$Location;
    .locals 9

    if-eqz p2, :cond_0

    const-string v0, "Nitro Upsell"

    goto :goto_0

    :cond_0
    const-string v0, "Sticker Purchase Button"

    :goto_0
    move-object v5, v0

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->getSection()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->EXPRESSION_PICKER:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v1}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->get_object()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v4, v0

    goto :goto_2

    :cond_1
    if-eqz p2, :cond_2

    const-string p2, "Sticker Picker Upsell Button"

    goto :goto_1

    :cond_2
    const-string p2, "Sticker Picker Purchase Button"

    :goto_1
    move-object v4, p2

    :goto_2
    new-instance p2, Lcom/discord/utilities/analytics/Traits$Location;

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->getPage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->getSection()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p2

    invoke-direct/range {v1 .. v8}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2

    :cond_3
    sget-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v1}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->get_object()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    move-object v4, v0

    goto :goto_4

    :cond_4
    if-eqz p2, :cond_5

    const-string p2, "Sticker Popout Upsell Button"

    goto :goto_3

    :cond_5
    const-string p2, "Sticker Popout Purchase Button"

    :goto_3
    move-object v4, p2

    :goto_4
    new-instance p2, Lcom/discord/utilities/analytics/Traits$Location;

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->getPage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/utilities/analytics/Traits$Location;->getSection()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p2

    invoke-direct/range {v1 .. v8}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2

    :cond_6
    return-object p1
.end method

.method private final getPurchaseDetails(Lcom/discord/models/domain/ModelSku;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->getInAppSku(Lcom/discord/models/domain/ModelSku;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku;->getId()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;-><init>(JLjava/lang/String;Lcom/android/billingclient/api/SkuDetails;)V

    return-object v1

    :cond_0
    new-instance p1, Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sku Details not loaded for Google Play In-App Sku: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/lang/Exception;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Google Play In-App Sku not found for sticker pack sku: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private final getShowUpsellDialogCallback(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lkotlin/jvm/functions/Function3;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")",
            "Lkotlin/jvm/functions/Function3<",
            "Landroidx/fragment/app/FragmentManager;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v1, 0x0

    if-eq p2, v0, :cond_3

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-ne p3, v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, p1, v2}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {v3, p3, v2}, Lcom/discord/utilities/premium/PremiumUtils;->isPremiumTierAtLeast(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v1, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$1;

    sget-object p1, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog;->m:Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog$Companion;

    invoke-direct {v1, p1}, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$1;-><init>(Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier1Dialog$Companion;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {p1, p3, v0}, Lcom/discord/utilities/premium/PremiumUtils;->isPremiumTierAtLeast(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result p1

    if-nez p1, :cond_2

    new-instance v1, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$2;

    sget-object p1, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;->n:Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog$Companion;

    invoke-direct {v1, p1}, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$2;-><init>(Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog$Companion;)V

    goto :goto_0

    :cond_2
    if-ne p2, v0, :cond_3

    sget-object p1, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {p1, p3, v0}, Lcom/discord/utilities/premium/PremiumUtils;->isPremiumTierAtLeast(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result p1

    if-nez p1, :cond_3

    new-instance v1, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$3;

    sget-object p1, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;->n:Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog$Companion;

    invoke-direct {v1, p1}, Lcom/discord/utilities/dsti/StickerUtils$getShowUpsellDialogCallback$3;-><init>(Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog$Companion;)V

    :cond_3
    :goto_0
    return-object v1
.end method

.method private final getStickerPackPriceForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/Integer;
    .locals 1

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku;->getPrice()Lcom/discord/models/domain/ModelSku$Price;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku$Price;->getPremium()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->getTierInt()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->getAmount()I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSku$Price;->getAmount()I

    move-result p1

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public static synthetic isStoreListingExpiringSoon$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLcom/discord/utilities/time/Clock;ILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p4

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/dsti/StickerUtils;->isStoreListingExpiringSoon(Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLcom/discord/utilities/time/Clock;)Z

    move-result p0

    return p0
.end method

.method private final purchaseStickerPack(Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p4, p5, p6}, Lcom/discord/utilities/dsti/StickerUtils;->canPurchaseStickerPack(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result p5

    if-nez p5, :cond_0

    return-void

    :cond_0
    new-instance p5, Lcom/android/billingclient/api/BillingFlowParams$a;

    const/4 p6, 0x0

    invoke-direct {p5, p6}, Lcom/android/billingclient/api/BillingFlowParams$a;-><init>(Lf/e/a/a/o;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, p5, Lcom/android/billingclient/api/BillingFlowParams$a;->e:Ljava/util/ArrayList;

    sget-object p3, Lcom/discord/utilities/users/UserUtils;->INSTANCE:Lcom/discord/utilities/users/UserUtils;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p6

    :cond_1
    invoke-virtual {p3, p6}, Lcom/discord/utilities/users/UserUtils;->getObfuscatedUserId(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_2

    iput-object p3, p5, Lcom/android/billingclient/api/BillingFlowParams$a;->a:Ljava/lang/String;

    invoke-virtual {p5}, Lcom/android/billingclient/api/BillingFlowParams$a;->a()Lcom/android/billingclient/api/BillingFlowParams;

    move-result-object p3

    sget-object v0, Lcom/discord/utilities/billing/BillingUtils;->INSTANCE:Lcom/discord/utilities/billing/BillingUtils;

    invoke-virtual {p4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getSkuId()J

    move-result-wide v2

    new-instance v4, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;

    invoke-direct {v4, p1, p3}, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;-><init>(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)V

    new-instance v5, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2;

    invoke-direct {v5, p2}, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2;-><init>(Ljava/lang/String;)V

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/billing/BillingUtils;->createPendingPurchaseMetadata(Ljava/lang/String;JLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void

    :cond_2
    new-instance p1, Ljava/lang/Exception;

    const-string p2, "Obfuscated user id is null"

    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final trackPaymentFlowStarted(Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;)V
    .locals 11

    invoke-virtual {p1}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->component1()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->component2()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->component3()Lcom/android/billingclient/api/SkuDetails;

    move-result-object p1

    new-instance v10, Lcom/discord/utilities/analytics/Traits$Payment;

    sget-object v0, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {p1}, Lcom/android/billingclient/api/SkuDetails;->c()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/discord/utilities/premium/PremiumUtils;->microAmountToMinor(J)I

    move-result v6

    iget-object v4, p1, Lcom/android/billingclient/api/SkuDetails;->b:Lorg/json/JSONObject;

    const-string v5, "original_price_micros"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/android/billingclient/api/SkuDetails;->b:Lorg/json/JSONObject;

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/android/billingclient/api/SkuDetails;->c()J

    move-result-wide v4

    :goto_0
    invoke-virtual {v0, v4, v5}, Lcom/discord/utilities/premium/PremiumUtils;->microAmountToMinor(J)I

    move-result v7

    iget-object p1, p1, Lcom/android/billingclient/api/SkuDetails;->b:Lorg/json/JSONObject;

    const-string v0, "price_currency_code"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "skuDetails.priceCurrencyCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v4, "Locale.ROOT"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    const-string p1, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v8, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x0

    const-string/jumbo v5, "sticker"

    move-object v4, v10

    invoke-direct/range {v4 .. v9}, Lcom/discord/utilities/analytics/Traits$Payment;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    move-object v4, p2

    move-object v5, p3

    move-object v6, v10

    invoke-virtual/range {v0 .. v6}, Lcom/discord/stores/StoreGooglePlayPurchases;->trackPaymentFlowStarted(Ljava/lang/String;JLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V

    return-void
.end method


# virtual methods
.method public final calculatePremiumStickerPackDiscount()I
    .locals 3

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, v0}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPrice(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)I

    move-result v0

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, v1}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPrice(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)I

    move-result v1

    sub-int v1, v0, v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v1, v1, v2

    int-to-float v0, v0

    div-float/2addr v1, v0

    float-to-int v0, v1

    return v0
.end method

.method public final claimOrPurchaseStickerPack(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/utilities/analytics/Traits$Location;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v1, p1

    move-object/from16 v0, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v2, p6

    const-string v3, "activity"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fragmentManager"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "stickerPack"

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "purchasePremiumTier"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "userPremiumTier"

    invoke-static {v6, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "baseAnalytics"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v4, v5, v6}, Lcom/discord/utilities/dsti/StickerUtils;->getShowUpsellDialogCallback(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lkotlin/jvm/functions/Function3;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v8

    if-eqz v8, :cond_3

    new-instance v15, Lcom/discord/utilities/analytics/Traits$StoreSku;

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSku;->getId()J

    move-result-wide v10

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSku;->getType()I

    move-result v12

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSku;->getApplicationId()J

    move-result-wide v13

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSku;->getName()Ljava/lang/String;

    move-result-object v16

    move-object v9, v15

    move-object/from16 v17, v15

    move-object/from16 v15, v16

    invoke-direct/range {v9 .. v15}, Lcom/discord/utilities/analytics/Traits$StoreSku;-><init>(JIJLjava/lang/String;)V

    invoke-virtual {v7, v4, v6}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v9

    const/4 v10, 0x0

    if-eqz v9, :cond_0

    invoke-direct {v7, v2, v10}, Lcom/discord/utilities/dsti/StickerUtils;->getPaymentStartedLocationTrait(Lcom/discord/utilities/analytics/Traits$Location;Z)Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/analytics/Traits$Payment;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const-string/jumbo v9, "sticker"

    const-string/jumbo v12, "usd"

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/discord/utilities/analytics/Traits$Payment;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sget-object v9, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const/4 v11, 0x0

    const/4 v14, 0x2

    const/4 v15, 0x0

    move-object v10, v0

    move-object/from16 v12, v17

    move-object v13, v1

    invoke-static/range {v9 .. v15}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowStarted$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v2

    new-instance v3, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$1;

    move-object/from16 v9, v17

    invoke-direct {v3, v0, v9, v1}, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$1;-><init>(Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V

    new-instance v5, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$2;

    invoke-direct {v5, v0, v9, v1}, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$2;-><init>(Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V

    invoke-virtual {v2, v4, v6, v3, v5}, Lcom/discord/stores/StoreStickers;->claimFreePack(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_0
    move-object/from16 v9, v17

    if-eqz v3, :cond_2

    sget-object v5, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-direct {v7, v8, v5}, Lcom/discord/utilities/dsti/StickerUtils;->getPurchaseDetails(Lcom/discord/models/domain/ModelSku;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    move-result-object v5

    new-instance v8, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;

    invoke-direct {v8, v5, v1, v4, v6}, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;-><init>(Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;Landroid/app/Activity;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v8, 0x0

    :cond_1
    new-instance v4, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onUpgradeClickListener$1;

    invoke-direct {v4, v1}, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onUpgradeClickListener$1;-><init>(Landroid/app/Activity;)V

    const/4 v1, 0x1

    invoke-direct {v7, v2, v1}, Lcom/discord/utilities/dsti/StickerUtils;->getPaymentStartedLocationTrait(Lcom/discord/utilities/analytics/Traits$Location;Z)Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v1

    invoke-direct {v7, v5, v1, v9}, Lcom/discord/utilities/dsti/StickerUtils;->trackPaymentFlowStarted(Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;)V

    invoke-interface {v3, v0, v8, v4}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-direct {v7, v8, v5}, Lcom/discord/utilities/dsti/StickerUtils;->getPurchaseDetails(Lcom/discord/models/domain/ModelSku;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    move-result-object v0

    invoke-direct {v7, v2, v10}, Lcom/discord/utilities/dsti/StickerUtils;->getPaymentStartedLocationTrait(Lcom/discord/utilities/analytics/Traits$Location;Z)Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v2

    invoke-direct {v7, v0, v2, v9}, Lcom/discord/utilities/dsti/StickerUtils;->trackPaymentFlowStarted(Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;)V

    invoke-virtual {v0}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/dsti/StickerUtils;->purchaseStickerPack(Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    :goto_0
    return-void

    :cond_3
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SKU not found for Sticker Pack: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final fetchSticker(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelSticker;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/file/DownloadUtils$DownloadState;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "sticker"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "stickers"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Lcom/discord/models/sticker/dto/ModelSticker;->getAssetUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance p1, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    invoke-direct {p1, v1}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;-><init>(Ljava/io/File;)V

    new-instance p2, Lg0/l/e/j;

    invoke-direct {p2, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(Download\u2026oadState.Completed(file))"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/dsti/StickerUtils;->getCDNAssetUrl$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/discord/models/sticker/dto/ModelSticker;->getAssetUrl()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, v1, p2, v0}, Lcom/discord/utilities/file/DownloadUtils;->downloadFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lrx/Observable;

    move-result-object p2

    :goto_0
    return-object p2
.end method

.method public final getBannerCDNAssetUrl(JLcom/discord/models/store/dto/ModelStoreAsset;Ljava/lang/Integer;)Ljava/lang/String;
    .locals 2

    const-string v0, "asset"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://cdn.discordapp.com/app-assets/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "/store/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/discord/models/store/dto/ModelStoreAsset;->getId()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/discord/utilities/string/StringUtilsKt;->getSTATIC_IMAGE_EXTENSION()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p4, :cond_0

    const-string p1, "?size="

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-static {p2}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getCDNAssetUrl(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;Z)Ljava/lang/String;
    .locals 6

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    const-string v2, ""

    const/16 v3, 0x2f

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 p2, 0x3

    if-eq v0, p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, "https://discord.com/stickers/"

    invoke-static {p2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getAssetUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v0, "https://media.discordapp.net/stickers/"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getAssetUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "?passthrough="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_2

    const-string p1, "&size="

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-static {p2}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2
.end method

.method public final getDEFAULT_HEADER_SIZE_PX()I
    .locals 1

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils;->DEFAULT_HEADER_SIZE_PX$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getDEFAULT_STICKER_SIZE_PX()I
    .locals 1

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils;->DEFAULT_STICKER_SIZE_PX$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getLimitedTimeLeftString(Landroid/content/res/Resources;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;)Ljava/lang/CharSequence;
    .locals 10

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getUnpublishedAt()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    const-string p1, ""

    return-object p1

    :cond_1
    invoke-virtual {p2}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getUnpublishedAtDate()J

    move-result-wide v0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p2

    invoke-interface {p2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-object p2, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual {p2, v0, v1}, Lcom/discord/utilities/time/TimeUtils;->getDaysFromMillis(J)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-lez v2, :cond_2

    const p2, 0x7f100030

    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p1, p2, v2, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getQuantityStr\u2026days, daysAway, daysAway)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_2
    invoke-virtual {p2, v0, v1}, Lcom/discord/utilities/time/TimeUtils;->getHoursFromMillis(J)I

    move-result v2

    int-to-long v5, v2

    const-wide/32 v7, 0x36ee80

    mul-long v5, v5, v7

    sub-long/2addr v0, v5

    invoke-virtual {p2, v0, v1}, Lcom/discord/utilities/time/TimeUtils;->getMinutesFromMillis(J)I

    move-result v5

    int-to-long v6, v5

    const-wide/32 v8, 0xea60

    mul-long v6, v6, v8

    sub-long/2addr v0, v6

    invoke-virtual {p2, v0, v1}, Lcom/discord/utilities/time/TimeUtils;->getSecondsFromMillis(J)I

    move-result p2

    if-ltz v2, :cond_4

    if-ltz v5, :cond_4

    if-gez p2, :cond_3

    goto :goto_1

    :cond_3
    const v0, 0x7f120652

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v3

    const-string v2, "%02d"

    const-string v7, "java.lang.String.format(format, *args)"

    invoke-static {v6, v4, v2, v7}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v3

    invoke-static {v6, v4, v2, v7}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v5, 0x2

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v6, v3

    invoke-static {v6, v4, v2, v7}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, v5

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getString(\n   \u2026%02d\", secondsAway)\n    )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_4
    :goto_1
    const p2, 0x7f121719

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getString(R.st\u2026icker_pack_expiring_soon)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getStickerPackPremiumPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/CharSequence;
    .locals 8

    const-string v3, "context"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "stickerPack"

    invoke-static {p2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "currentPremiumTier"

    invoke-static {p3, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq p3, v3, :cond_3

    invoke-direct {p0, p2, v3}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPriceForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, p2, p3}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPriceForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v3, :cond_3

    if-eqz v6, :cond_3

    invoke-static {v3, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v5

    if-eqz v7, :cond_3

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPack()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1, p1}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    :goto_0
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f060292

    if-nez v2, :cond_1

    const v2, 0x7f12170b

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v4

    invoke-virtual {p1, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026d, formattedRegularPrice)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {p1, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x34

    const/4 v7, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPackDiscounted()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, p1}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_1
    const v6, 0x7f12170d

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v4

    aput-object v1, v7, v5

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(\n     \u2026egularPrice\n            )"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {p1, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x34

    const/4 v7, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-virtual {p0, p2, p3}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v3

    if-eqz v3, :cond_4

    const v1, 0x7f1216fd

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026.sticker_pack_price_free)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_4
    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, p2, v3}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v6, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {v6, p3, v3}, Lcom/discord/utilities/premium/PremiumUtils;->isPremiumTierAtLeast(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v3

    if-nez v3, :cond_5

    const v1, 0x7f1216fe

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026free_with_premium_tier_1)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_5
    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, p2, v3}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {v1, p3, v3}, Lcom/discord/utilities/premium/PremiumUtils;->isPremiumTierAtLeast(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v1

    if-nez v1, :cond_6

    const v1, 0x7f1216ff

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026free_with_premium_tier_2)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_6
    invoke-virtual {p0, v3}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPrice(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)I

    move-result v1

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPackDiscounted()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    goto :goto_2

    :cond_7
    invoke-static {v1, p1}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v6

    :goto_2
    if-ne p3, v3, :cond_8

    goto :goto_3

    :cond_8
    const v1, 0x7f12171d

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v6, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "context.getString(R.stri\u2026m_tier_2, formattedPrice)"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    return-object v6
.end method

.method public final getStickerPackPrice(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)I
    .locals 1

    const-string v0, "premiumTier"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/16 p1, 0x12b

    goto :goto_0

    :cond_0
    const/16 p1, 0xc7

    :goto_0
    return p1
.end method

.method public final getStickerPackPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/String;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerPack"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentPremiumTier"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p2, p3}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p2, 0x7f1216fd

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026.sticker_pack_price_free)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    invoke-virtual {p2}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result p2

    if-eqz p2, :cond_1

    const p2, 0x7f1216ff

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026free_with_premium_tier_2)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_1
    invoke-virtual {p0, p3}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPrice(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)I

    move-result p2

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlayInAppSkuKt;->getInAppStickerPack()Lcom/discord/utilities/billing/GooglePlayInAppSku;

    move-result-object p3

    invoke-virtual {p3}, Lcom/discord/utilities/billing/GooglePlayInAppSku;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object p3

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {p2, p1}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p3

    :goto_0
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z
    .locals 3

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "premiumTier"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPriceForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x0

    :goto_2
    if-nez v0, :cond_4

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :cond_4
    :goto_3
    return v1
.end method

.method public final isStickerPackNew(JLjava/util/Map;J)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;J)Z"
        }
    .end annotation

    const-string/jumbo v0, "viewedPurchaseableStickerPackIds"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    cmp-long p3, p1, p4

    if-ltz p3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_1
    return v2
.end method

.method public final isStoreListingExpiringSoon(Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLcom/discord/utilities/time/Clock;)Z
    .locals 6

    const-string v0, "clock"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getUnpublishedAtDate()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getUnpublishedAtDate()J

    move-result-wide v1

    invoke-interface {p4}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v3

    add-long/2addr v3, p2

    cmp-long p1, v1, v3

    if-gtz p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public final parseFromMessageNotificationJson(Ljava/lang/String;)Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 13

    const-string/jumbo v0, "tags"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo p1, "stickers"

    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object p1

    const-string v2, "JSONObject(data).getJSON\u2026ickers\").getJSONObject(0)"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v2, Lcom/discord/models/sticker/dto/ModelSticker;

    const-string v3, "id"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v3, "pack_id"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v3, "name"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "jsonSticker.getString(\"name\")"

    invoke-static {v8, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "description"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "jsonSticker.getString(\"description\")"

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "asset"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v3, "format_type"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v12, p1

    goto :goto_0

    :cond_1
    move-object v12, v1

    :goto_0
    move-object v3, v2

    invoke-direct/range {v3 .. v12}, Lcom/discord/models/sticker/dto/ModelSticker;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v2

    goto :goto_1

    :catch_0
    move-exception p1

    move-object v4, p1

    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v3, "Error parsing sticker from notification"

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :catch_1
    :goto_1
    return-object v1
.end method
