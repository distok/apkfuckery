.class public final Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2;
.super Lx/m/c/k;
.source "StickerUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/dsti/StickerUtils;->purchaseStickerPack(Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $paymentGatewaySkuId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2;->$paymentGatewaySkuId:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 18

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v1

    move-object/from16 v2, p0

    iget-object v3, v2, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2;->$paymentGatewaySkuId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/discord/stores/StoreGooglePlayPurchases;->trackPaymentFlowFailed(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreNotices$Notice;

    sget-object v15, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2$1;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$2$1;

    const-string v4, "CREATE_METADATA_FAILED_NOTICE"

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/16 v16, 0x36

    const/16 v17, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v17}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    return-void
.end method
