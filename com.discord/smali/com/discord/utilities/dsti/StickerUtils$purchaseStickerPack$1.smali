.class public final Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;
.super Lx/m/c/k;
.source "StickerUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/dsti/StickerUtils;->purchaseStickerPack(Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $activity:Landroid/app/Activity;

.field public final synthetic $params:Lcom/android/billingclient/api/BillingFlowParams;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;->$activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;->$params:Lcom/android/billingclient/api/BillingFlowParams;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    iget-object v1, p0, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;->$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/discord/utilities/dsti/StickerUtils$purchaseStickerPack$1;->$params:Lcom/android/billingclient/api/BillingFlowParams;

    const-string v3, "params"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->launchBillingFlow(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    :cond_0
    return-void
.end method
