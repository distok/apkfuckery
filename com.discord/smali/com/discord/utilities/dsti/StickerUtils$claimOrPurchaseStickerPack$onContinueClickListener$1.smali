.class public final Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;
.super Lx/m/c/k;
.source "StickerUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/utilities/dsti/StickerUtils;->claimOrPurchaseStickerPack(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/utilities/analytics/Traits$Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $activity:Landroid/app/Activity;

.field public final synthetic $purchaseDetails:Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

.field public final synthetic $stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

.field public final synthetic $userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;Landroid/app/Activity;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$purchaseDetails:Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    iput-object p2, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iput-object p4, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 11

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$purchaseDetails:Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    invoke-virtual {v1}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "premium_upsell"

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/stores/StoreGooglePlayPurchases;->trackPaymentFlowStep(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    iget-object v5, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$activity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$purchaseDetails:Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    invoke-virtual {v0}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->getPaymentGatewaySkuId()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$purchaseDetails:Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    invoke-virtual {v0}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->getSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v7

    iget-object v8, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    sget-object v9, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object v10, p0, Lcom/discord/utilities/dsti/StickerUtils$claimOrPurchaseStickerPack$onContinueClickListener$1;->$userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-static/range {v4 .. v10}, Lcom/discord/utilities/dsti/StickerUtils;->access$purchaseStickerPack(Lcom/discord/utilities/dsti/StickerUtils;Landroid/app/Activity;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-void
.end method
