.class public final Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;
.super Ljava/lang/Object;
.source "StickerUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/utilities/dsti/StickerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseDetails"
.end annotation


# instance fields
.field private final paymentGatewaySkuId:Ljava/lang/String;

.field private final skuDetails:Lcom/android/billingclient/api/SkuDetails;

.field private final stickerPackSkuId:J


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/android/billingclient/api/SkuDetails;)V
    .locals 1

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    iput-object p3, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;JLjava/lang/String;Lcom/android/billingclient/api/SkuDetails;ILjava/lang/Object;)Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-wide p1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p3, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-object p4, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->copy(JLjava/lang/String;Lcom/android/billingclient/api/SkuDetails;)Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;Lcom/android/billingclient/api/SkuDetails;)Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;
    .locals 1

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;-><init>(JLjava/lang/String;Lcom/android/billingclient/api/SkuDetails;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;

    iget-wide v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    iget-wide v2, p1, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    iget-object p1, p1, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPaymentGatewaySkuId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSkuDetails()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final getStickerPackSkuId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/billingclient/api/SkuDetails;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "PurchaseDetails(stickerPackSkuId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->stickerPackSkuId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", paymentGatewaySkuId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->paymentGatewaySkuId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", skuDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/utilities/dsti/StickerUtils$PurchaseDetails;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
