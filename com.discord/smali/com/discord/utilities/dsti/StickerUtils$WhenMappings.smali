.class public final synthetic Lcom/discord/utilities/dsti/StickerUtils$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/discord/models/sticker/dto/ModelSticker$Type;->values()[Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v0, 0x4

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/utilities/dsti/StickerUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->LOTTIE:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v3, Lcom/discord/models/sticker/dto/ModelSticker$Type;->APNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v3, 0x2

    aput v3, v0, v3

    sget-object v4, Lcom/discord/models/sticker/dto/ModelSticker$Type;->PNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    aput v1, v0, v2

    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->values()[Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    new-array v0, v1, [I

    sput-object v0, Lcom/discord/utilities/dsti/StickerUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    aput v2, v0, v3

    return-void
.end method
