.class public final Lcom/discord/player/AppMediaPlayer;
.super Ljava/lang/Object;
.source "AppMediaPlayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/player/AppMediaPlayer$Event;
    }
.end annotation


# instance fields
.field public final a:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/player/AppMediaPlayer$Event;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lrx/Subscription;

.field public final c:Lrx/subscriptions/CompositeSubscription;

.field public final d:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/discord/player/MediaSource;

.field public final f:Lf/h/a/c/s0;

.field public final g:Lf/a/g/k;

.field public final h:Lf/h/a/c/h1/j$a;

.field public final i:Lrx/Scheduler;

.field public final j:Lcom/discord/utilities/logging/Logger;


# direct methods
.method public constructor <init>(Lf/h/a/c/s0;Lf/a/g/k;Lf/h/a/c/h1/j$a;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;)V
    .locals 1

    const-string v0, "exoPlayer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rxPlayerEventListener"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataSourceFactory"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timerScheduler"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    iput-object p2, p0, Lcom/discord/player/AppMediaPlayer;->g:Lf/a/g/k;

    iput-object p3, p0, Lcom/discord/player/AppMediaPlayer;->h:Lf/h/a/c/h1/j$a;

    iput-object p4, p0, Lcom/discord/player/AppMediaPlayer;->i:Lrx/Scheduler;

    iput-object p5, p0, Lcom/discord/player/AppMediaPlayer;->j:Lcom/discord/utilities/logging/Logger;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p3

    const-string p4, "PublishSubject.create()"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    new-instance p3, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p3}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p3, p0, Lcom/discord/player/AppMediaPlayer;->c:Lrx/subscriptions/CompositeSubscription;

    iget p1, p1, Lf/h/a/c/s0;->x:F

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    const-string p4, "BehaviorSubject.create(exoPlayer.volume)"

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/player/AppMediaPlayer;->d:Lrx/subjects/BehaviorSubject;

    iget-object p1, p2, Lf/a/g/k;->d:Lrx/subjects/PublishSubject;

    const-string p4, "playerStateChangeSubject"

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lf/a/g/e;

    invoke-direct {p4, p0}, Lf/a/g/e;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    new-instance p5, Lf/a/g/f;

    invoke-direct {p5, p0}, Lf/a/g/f;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    invoke-virtual {p1, p4, p5}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    invoke-virtual {p3, p1}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    iget-object p1, p2, Lf/a/g/k;->e:Lrx/subjects/PublishSubject;

    const-string p4, "isPlayingChangeSubject"

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lf/a/g/a;

    invoke-direct {p4, p0}, Lf/a/g/a;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    new-instance p5, Lf/a/g/b;

    invoke-direct {p5, p0}, Lf/a/g/b;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    invoke-virtual {p1, p4, p5}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    invoke-virtual {p3, p1}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    iget-object p1, p0, Lcom/discord/player/AppMediaPlayer;->e:Lcom/discord/player/MediaSource;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/discord/player/MediaSource;->e:Ljava/lang/String;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    iget-object p2, p2, Lf/a/g/k;->f:Lrx/subjects/PublishSubject;

    const-string p4, "playerErrorSubject"

    invoke-static {p2, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lf/a/g/c;

    invoke-direct {p4, p0, p1}, Lf/a/g/c;-><init>(Lcom/discord/player/AppMediaPlayer;Ljava/lang/String;)V

    new-instance p1, Lf/a/g/d;

    invoke-direct {p1, p0}, Lf/a/g/d;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    invoke-virtual {p2, p4, p1}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    invoke-virtual {p3, p1}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    return-void
.end method

.method public static synthetic b(Lcom/discord/player/AppMediaPlayer;Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;I)V
    .locals 10

    and-int/lit8 v0, p8, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p2

    :goto_0
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_1

    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    move v5, p3

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x0

    move-wide v6, v0

    goto :goto_2

    :cond_2
    move-wide v6, p4

    :goto_2
    and-int/lit8 v0, p8, 0x20

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v8, p6

    invoke-virtual/range {v2 .. v9}, Lcom/discord/player/AppMediaPlayer;->a(Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;)V
    .locals 22
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p4

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    const-string v6, "mediaSource"

    invoke-static {v1, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "playerView"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/discord/player/AppMediaPlayer;->e:Lcom/discord/player/MediaSource;

    iget-object v6, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v6}, Lf/h/a/c/s0;->S()V

    iget-object v7, v6, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v8, v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_0
    const/4 v11, 0x2

    const/4 v12, 0x1

    if-ge v10, v8, :cond_1

    aget-object v13, v7, v10

    invoke-interface {v13}, Lf/h/a/c/p0;->u()I

    move-result v14

    if-ne v14, v11, :cond_0

    iget-object v11, v6, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v11, v13}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v11

    const/4 v13, 0x4

    invoke-virtual {v11, v13}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Lf/h/a/c/n0;->d(Ljava/lang/Object;)Lf/h/a/c/n0;

    invoke-virtual {v11}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_1
    iget-object v6, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v4, v6}, Lcom/google/android/exoplayer2/ui/PlayerView;->setPlayer(Lf/h/a/c/m0;)V

    if-eqz v5, :cond_2

    iget-object v4, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v5, v4}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->setPlayer(Lf/h/a/c/m0;)V

    :cond_2
    iget-object v15, v0, Lcom/discord/player/AppMediaPlayer;->h:Lf/h/a/c/h1/j$a;

    new-instance v16, Lf/h/a/c/a1/f;

    invoke-direct/range {v16 .. v16}, Lf/h/a/c/a1/f;-><init>()V

    sget-object v17, Lf/h/a/c/z0/b;->a:Lf/h/a/c/z0/b;

    new-instance v18, Lf/h/a/c/h1/r;

    invoke-direct/range {v18 .. v18}, Lf/h/a/c/h1/r;-><init>()V

    const/high16 v20, 0x100000

    iget-object v14, v1, Lcom/discord/player/MediaSource;->d:Landroid/net/Uri;

    new-instance v1, Lf/h/a/c/d1/s;

    const/16 v19, 0x0

    const/16 v21, 0x0

    move-object v13, v1

    invoke-direct/range {v13 .. v21}, Lf/h/a/c/d1/s;-><init>(Landroid/net/Uri;Lf/h/a/c/h1/j$a;Lf/h/a/c/a1/j;Lf/h/a/c/z0/b;Lf/h/a/c/h1/u;Ljava/lang/String;ILjava/lang/Object;)V

    iget-object v4, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v4}, Lf/h/a/c/s0;->S()V

    iget-object v5, v4, Lf/h/a/c/s0;->y:Lf/h/a/c/d1/p;

    if-eqz v5, :cond_3

    iget-object v6, v4, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    invoke-interface {v5, v6}, Lf/h/a/c/d1/p;->c(Lf/h/a/c/d1/q;)V

    iget-object v5, v4, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Ljava/util/ArrayList;

    iget-object v7, v5, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v7, v7, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/c/v0/a$a;

    iget v8, v7, Lf/h/a/c/v0/a$a;->c:I

    iget-object v7, v7, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v5, v8, v7}, Lf/h/a/c/v0/a;->K(ILf/h/a/c/d1/p$a;)V

    goto :goto_1

    :cond_3
    iput-object v1, v4, Lf/h/a/c/s0;->y:Lf/h/a/c/d1/p;

    iget-object v5, v4, Lf/h/a/c/s0;->d:Landroid/os/Handler;

    iget-object v6, v4, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    invoke-virtual {v1, v5, v6}, Lf/h/a/c/d1/k;->g(Landroid/os/Handler;Lf/h/a/c/d1/q;)V

    iget-object v5, v4, Lf/h/a/c/s0;->o:Lf/h/a/c/r;

    invoke-virtual {v4}, Lf/h/a/c/s0;->f()Z

    move-result v6

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v6, :cond_5

    iget v6, v5, Lf/h/a/c/r;->d:I

    if-eqz v6, :cond_4

    invoke-virtual {v5, v12}, Lf/h/a/c/r;->a(Z)V

    :cond_4
    const/4 v5, 0x1

    goto :goto_2

    :cond_5
    const/4 v5, -0x1

    :goto_2
    invoke-virtual {v4}, Lf/h/a/c/s0;->f()Z

    move-result v6

    invoke-virtual {v4, v6, v5}, Lf/h/a/c/s0;->R(ZI)V

    iget-object v13, v4, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-static {v13}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v13, v12, v12, v12, v11}, Lf/h/a/c/a0;->I(ZZZI)Lf/h/a/c/i0;

    move-result-object v14

    iput-boolean v12, v13, Lf/h/a/c/a0;->p:Z

    iget v4, v13, Lf/h/a/c/a0;->o:I

    add-int/2addr v4, v12

    iput v4, v13, Lf/h/a/c/a0;->o:I

    iget-object v4, v13, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    iget-object v4, v4, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v4, v4, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    invoke-virtual {v4, v9, v12, v12, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    const/4 v15, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x1

    const/16 v18, 0x0

    invoke-virtual/range {v13 .. v18}, Lf/h/a/c/a0;->P(Lf/h/a/c/i0;ZIIZ)V

    if-eqz p2, :cond_6

    iget-object v1, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v1, v12}, Lf/h/a/c/s0;->n(Z)V

    :cond_6
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_7

    iget-object v1, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v1}, Lf/h/a/c/s0;->m()I

    move-result v4

    invoke-virtual {v1, v4, v2, v3}, Lf/h/a/c/s0;->e(IJ)V

    :cond_7
    iget-object v1, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v1}, Lf/h/a/c/s0;->S()V

    iget-object v1, v1, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    move/from16 v2, p3

    invoke-virtual {v1, v2}, Lf/h/a/c/a0;->u(I)V

    return-void
.end method

.method public final c()V
    .locals 8
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/c/s0;->n(Z)V

    iget-object v0, p0, Lcom/discord/player/AppMediaPlayer;->b:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/player/AppMediaPlayer;->c:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    iget-object v0, p0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->S()V

    iget-object v2, v0, Lf/h/a/c/s0;->n:Lf/h/a/c/q;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v3, v2, Lf/h/a/c/q;->c:Z

    if-eqz v3, :cond_1

    iget-object v3, v2, Lf/h/a/c/q;->a:Landroid/content/Context;

    iget-object v4, v2, Lf/h/a/c/q;->b:Lf/h/a/c/q$a;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v1, v2, Lf/h/a/c/q;->c:Z

    :cond_1
    iget-object v2, v0, Lf/h/a/c/s0;->o:Lf/h/a/c/r;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lf/h/a/c/r;->a(Z)V

    iget-object v2, v0, Lf/h/a/c/s0;->p:Lf/h/a/c/u0;

    iput-boolean v1, v2, Lf/h/a/c/u0;->a:Z

    iget-object v2, v0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "ExoPlayerImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Release "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "ExoPlayerLib/2.11.3"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "] ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lf/h/a/c/i1/a0;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "] ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lf/h/a/c/c0;->a:Ljava/util/HashSet;

    const-class v6, Lf/h/a/c/c0;

    monitor-enter v6

    :try_start_0
    sget-object v7, Lf/h/a/c/c0;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v6

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, v2, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    monitor-enter v4

    :try_start_1
    iget-boolean v5, v4, Lf/h/a/c/b0;->z:Z

    if-nez v5, :cond_5

    iget-object v5, v4, Lf/h/a/c/b0;->k:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_1

    :cond_2
    iget-object v5, v4, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/x;->c(I)Z

    const/4 v5, 0x0

    :goto_0
    iget-boolean v6, v4, Lf/h/a/c/b0;->z:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_3

    :try_start_2
    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    if-eqz v5, :cond_4

    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    monitor-exit v4

    goto :goto_2

    :cond_5
    :goto_1
    monitor-exit v4

    :goto_2
    iget-object v4, v2, Lf/h/a/c/a0;->e:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-virtual {v2, v1, v1, v1, v3}, Lf/h/a/c/a0;->I(ZZZI)Lf/h/a/c/i0;

    move-result-object v1

    iput-object v1, v2, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->K()V

    iget-object v1, v0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    if-eqz v1, :cond_7

    iget-boolean v2, v0, Lf/h/a/c/s0;->r:Z

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    :cond_6
    iput-object v5, v0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    :cond_7
    iget-object v1, v0, Lf/h/a/c/s0;->y:Lf/h/a/c/d1/p;

    if-eqz v1, :cond_8

    iget-object v2, v0, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    invoke-interface {v1, v2}, Lf/h/a/c/d1/p;->c(Lf/h/a/c/d1/q;)V

    iput-object v5, v0, Lf/h/a/c/s0;->y:Lf/h/a/c/d1/p;

    :cond_8
    iget-boolean v1, v0, Lf/h/a/c/s0;->D:Z

    if-nez v1, :cond_9

    iget-object v1, v0, Lf/h/a/c/s0;->l:Lf/h/a/c/h1/e;

    iget-object v2, v0, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    invoke-interface {v1, v2}, Lf/h/a/c/h1/e;->d(Lf/h/a/c/h1/e$a;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lf/h/a/c/s0;->z:Ljava/util/List;

    return-void

    :cond_9
    throw v5

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final d(F)V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->S()V

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p1, v1, v2}, Lf/h/a/c/i1/a0;->e(FFF)F

    move-result v1

    iget v2, v0, Lf/h/a/c/s0;->x:F

    cmpl-float v2, v2, v1

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    iput v1, v0, Lf/h/a/c/s0;->x:F

    invoke-virtual {v0}, Lf/h/a/c/s0;->L()V

    iget-object v0, v0, Lf/h/a/c/s0;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/w0/k;

    invoke-interface {v2, v1}, Lf/h/a/c/w0/k;->m(F)V

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/discord/player/AppMediaPlayer;->d:Lrx/subjects/BehaviorSubject;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
