.class public final enum Lcom/discord/player/MediaType;
.super Ljava/lang/Enum;
.source "MediaType.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/player/MediaType$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/player/MediaType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/player/MediaType;

.field public static final CREATOR:Lcom/discord/player/MediaType$a;

.field public static final enum GIFV:Lcom/discord/player/MediaType;

.field public static final enum VIDEO:Lcom/discord/player/MediaType;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/player/MediaType;

    new-instance v1, Lcom/discord/player/MediaType;

    const-string v2, "VIDEO"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/player/MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/player/MediaType;->VIDEO:Lcom/discord/player/MediaType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/player/MediaType;

    const-string v2, "GIFV"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/player/MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/player/MediaType;->GIFV:Lcom/discord/player/MediaType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/player/MediaType;->$VALUES:[Lcom/discord/player/MediaType;

    new-instance v0, Lcom/discord/player/MediaType$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/player/MediaType$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/player/MediaType;->CREATOR:Lcom/discord/player/MediaType$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/player/MediaType;
    .locals 1

    const-class v0, Lcom/discord/player/MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/player/MediaType;

    return-object p0
.end method

.method public static values()[Lcom/discord/player/MediaType;
    .locals 1

    sget-object v0, Lcom/discord/player/MediaType;->$VALUES:[Lcom/discord/player/MediaType;

    invoke-virtual {v0}, [Lcom/discord/player/MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/player/MediaType;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
