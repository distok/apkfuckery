.class public final enum Lcom/discord/hardware_analytics/media_codec/VideoRes;
.super Ljava/lang/Enum;
.source "VideoRes.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/hardware_analytics/media_codec/VideoRes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/hardware_analytics/media_codec/VideoRes;

.field public static final enum RESOLUTION_1080:Lcom/discord/hardware_analytics/media_codec/VideoRes;

.field public static final enum RESOLUTION_480:Lcom/discord/hardware_analytics/media_codec/VideoRes;

.field public static final enum RESOLUTION_4K:Lcom/discord/hardware_analytics/media_codec/VideoRes;

.field public static final enum RESOLUTION_720:Lcom/discord/hardware_analytics/media_codec/VideoRes;


# instance fields
.field private final height:I

.field private final width:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/hardware_analytics/media_codec/VideoRes;

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const-string v2, "RESOLUTION_480"

    const/4 v3, 0x0

    const/16 v4, 0x350

    const/16 v5, 0x1e0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/discord/hardware_analytics/media_codec/VideoRes;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_480:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const-string v2, "RESOLUTION_720"

    const/4 v3, 0x1

    const/16 v4, 0x500

    const/16 v5, 0x2d0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/discord/hardware_analytics/media_codec/VideoRes;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_720:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const-string v2, "RESOLUTION_1080"

    const/4 v3, 0x2

    const/16 v4, 0x780

    const/16 v5, 0x438

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/discord/hardware_analytics/media_codec/VideoRes;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_1080:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const-string v2, "RESOLUTION_4K"

    const/4 v3, 0x3

    const/16 v4, 0xf00

    const/16 v5, 0x870

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/discord/hardware_analytics/media_codec/VideoRes;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_4K:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/hardware_analytics/media_codec/VideoRes;->$VALUES:[Lcom/discord/hardware_analytics/media_codec/VideoRes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/hardware_analytics/media_codec/VideoRes;->width:I

    iput p4, p0, Lcom/discord/hardware_analytics/media_codec/VideoRes;->height:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/hardware_analytics/media_codec/VideoRes;
    .locals 1

    const-class v0, Lcom/discord/hardware_analytics/media_codec/VideoRes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/hardware_analytics/media_codec/VideoRes;

    return-object p0
.end method

.method public static values()[Lcom/discord/hardware_analytics/media_codec/VideoRes;
    .locals 1

    sget-object v0, Lcom/discord/hardware_analytics/media_codec/VideoRes;->$VALUES:[Lcom/discord/hardware_analytics/media_codec/VideoRes;

    invoke-virtual {v0}, [Lcom/discord/hardware_analytics/media_codec/VideoRes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/hardware_analytics/media_codec/VideoRes;

    return-object v0
.end method


# virtual methods
.method public final getHeight()I
    .locals 1

    iget v0, p0, Lcom/discord/hardware_analytics/media_codec/VideoRes;->height:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    iget v0, p0, Lcom/discord/hardware_analytics/media_codec/VideoRes;->width:I

    return v0
.end method
