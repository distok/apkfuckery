.class public final enum Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;
.super Ljava/lang/Enum;
.source "VideoCodecMimeType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

.field public static final enum H264:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

.field public static final enum VP8:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

.field public static final enum VP9:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;


# instance fields
.field private final mimeType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    const-string v2, "VP8"

    const/4 v3, 0x0

    const-string/jumbo v4, "video/x-vnd.on2.vp8"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->VP8:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    const-string v2, "VP9"

    const/4 v3, 0x1

    const-string/jumbo v4, "video/x-vnd.on2.vp9"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->VP9:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    const-string v2, "H264"

    const/4 v3, 0x2

    const-string/jumbo v4, "video/avc"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->H264:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->$VALUES:[Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->mimeType:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;
    .locals 1

    const-class v0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    return-object p0
.end method

.method public static values()[Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;
    .locals 1

    sget-object v0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->$VALUES:[Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    invoke-virtual {v0}, [Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    return-object v0
.end method


# virtual methods
.method public final getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->mimeType:Ljava/lang/String;

    return-object v0
.end method
