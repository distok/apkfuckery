.class public final Lcom/discord/hardware_analytics/DecoderCountInfo;
.super Ljava/lang/Object;
.source "DecoderCountInfo.kt"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/discord/hardware_analytics/DecoderCountInfo;-><init>(IIIIIIII)V

    return-void
.end method

.method public constructor <init>(IIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->a:I

    iput p2, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->b:I

    iput p3, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->c:I

    iput p4, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->d:I

    iput p5, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->e:I

    iput p6, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->f:I

    iput p7, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->g:I

    return-void
.end method

.method public constructor <init>(IIIIIIII)V
    .locals 2

    and-int/lit8 v0, p8, 0x1

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    const/4 p1, -0x1

    :cond_0
    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_1

    const/4 p2, -0x1

    :cond_1
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_2

    const/4 p3, -0x1

    :cond_2
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    const/4 p4, -0x1

    :cond_3
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_4

    const/4 p5, -0x1

    :cond_4
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_5

    const/4 p6, -0x1

    :cond_5
    and-int/lit8 p8, p8, 0x40

    if-eqz p8, :cond_6

    const/4 p7, -0x1

    :cond_6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->a:I

    iput p2, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->b:I

    iput p3, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->c:I

    iput p4, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->d:I

    iput p5, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->e:I

    iput p6, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->f:I

    iput p7, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->g:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/hardware_analytics/DecoderCountInfo;

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->a:I

    iget v1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->a:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->b:I

    iget v1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->c:I

    iget v1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->c:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->d:I

    iget v1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->d:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->e:I

    iget v1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->e:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->f:I

    iget v1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->f:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->g:I

    iget p1, p1, Lcom/discord/hardware_analytics/DecoderCountInfo;->g:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->f:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->g:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "DecoderCountInfo(nHardwareDecoders1080="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nHardwareDecoders720="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nHardwareDecoders480="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nDecoders1080="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nDecoders720="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nDecoders480="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", testDurationMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/hardware_analytics/DecoderCountInfo;->g:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
