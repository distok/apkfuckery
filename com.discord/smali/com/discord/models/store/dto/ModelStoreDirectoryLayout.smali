.class public final Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;
.super Ljava/lang/Object;
.source "ModelStoreDirectoryLayout.kt"


# instance fields
.field private final allSkus:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final id:J


# direct methods
.method public constructor <init>(JLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "allSkus"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    iput-object p3, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;JLjava/util/List;ILjava/lang/Object;)Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-wide p1, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-object p3, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->copy(JLjava/util/List;)Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    return-wide v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    return-object v0
.end method

.method public final copy(JLjava/util/List;)Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;"
        }
    .end annotation

    const-string v0, "allSkus"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;-><init>(JLjava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;

    iget-wide v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    iget-wide v2, p1, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllSkus()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelStoreDirectoryLayout(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", allSkus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->allSkus:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
