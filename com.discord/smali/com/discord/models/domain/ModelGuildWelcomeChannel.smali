.class public final Lcom/discord/models/domain/ModelGuildWelcomeChannel;
.super Ljava/lang/Object;
.source "ModelGuildWelcomeChannel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;
    }
.end annotation


# static fields
.field public static final Parser:Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;


# instance fields
.field private final channelId:J

.field private final description:Ljava/lang/String;

.field private final emojiId:Ljava/lang/Long;

.field private final emojiName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->Parser:Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    iput-object p3, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelGuildWelcomeChannel;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/models/domain/ModelGuildWelcomeChannel;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget-object p3, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    :cond_1
    move-object v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget-object p4, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    :cond_2
    move-object v4, p4

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    iget-object p5, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    :cond_3
    move-object v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->copy(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildWelcomeChannel;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->Parser:Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;

    invoke-virtual {v0, p0}, Lcom/discord/models/domain/ModelGuildWelcomeChannel$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lcom/discord/models/domain/ModelGuildWelcomeChannel;
    .locals 7

    new-instance v6, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-object v0, v6

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;-><init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    return-wide v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getEmojiId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getEmojiName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    return-object v0
.end method

.method public final getEmojiUri(I)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->getImageUri(JZI)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelGuildWelcomeChannel(channelId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", emojiId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emojiName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->emojiName:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
