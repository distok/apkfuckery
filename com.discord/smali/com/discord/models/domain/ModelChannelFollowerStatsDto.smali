.class public final Lcom/discord/models/domain/ModelChannelFollowerStatsDto;
.super Ljava/lang/Object;
.source "ModelChannelFollowerStatsDto.kt"


# instance fields
.field private final channelsFollowing:Ljava/lang/Integer;

.field private final guildMembers:Ljava/lang/Integer;

.field private final guildsFollowing:Ljava/lang/Integer;

.field private final subscribersGainedSinceLastPost:Ljava/lang/Integer;

.field private final subscribersLostSinceLastPost:Ljava/lang/Integer;

.field private final usersSeenEver:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    iput-object p6, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelChannelFollowerStatsDto;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/discord/models/domain/ModelChannelFollowerStatsDto;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->copy(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/discord/models/domain/ModelChannelFollowerStatsDto;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component3()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component4()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/discord/models/domain/ModelChannelFollowerStatsDto;
    .locals 8

    new-instance v7, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelsFollowing()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getGuildMembers()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getGuildsFollowing()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSubscribersGainedSinceLastPost()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSubscribersLostSinceLastPost()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getUsersSeenEver()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelChannelFollowerStatsDto(guildsFollowing="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildsFollowing:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelsFollowing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->channelsFollowing:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildMembers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->guildMembers:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", usersSeenEver="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->usersSeenEver:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscribersGainedSinceLastPost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersGainedSinceLastPost:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscribersLostSinceLastPost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->subscribersLostSinceLastPost:Ljava/lang/Integer;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->w(Ljava/lang/StringBuilder;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
