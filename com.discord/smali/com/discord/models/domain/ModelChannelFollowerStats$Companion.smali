.class public final Lcom/discord/models/domain/ModelChannelFollowerStats$Companion;
.super Ljava/lang/Object;
.source "ModelChannelFollowerStats.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelChannelFollowerStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/domain/ModelChannelFollowerStats$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromResponse(Lcom/discord/models/domain/ModelChannelFollowerStatsDto;)Lcom/discord/models/domain/ModelChannelFollowerStats;
    .locals 23

    if-nez p1, :cond_0

    new-instance v11, Lcom/discord/models/domain/ModelChannelFollowerStats;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7f

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/discord/models/domain/ModelChannelFollowerStats;-><init>(JLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v11

    :cond_0
    new-instance v0, Lcom/discord/models/domain/ModelChannelFollowerStats;

    const-wide/16 v13, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->getChannelsFollowing()Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->getGuildMembers()Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->getGuildsFollowing()Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->getUsersSeenEver()Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->getSubscribersGainedSinceLastPost()Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannelFollowerStatsDto;->getSubscribersLostSinceLastPost()Ljava/lang/Integer;

    move-result-object v20

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v22}, Lcom/discord/models/domain/ModelChannelFollowerStats;-><init>(JLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
