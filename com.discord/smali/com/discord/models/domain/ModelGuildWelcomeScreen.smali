.class public final Lcom/discord/models/domain/ModelGuildWelcomeScreen;
.super Ljava/lang/Object;
.source "ModelGuildWelcomeScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;
    }
.end annotation


# static fields
.field public static final Parser:Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;


# instance fields
.field private final description:Ljava/lang/String;

.field private final welcomeChannels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->Parser:Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelGuildWelcomeScreen;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/models/domain/ModelGuildWelcomeScreen;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->copy(Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildWelcomeScreen;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->Parser:Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;

    invoke-virtual {v0, p0}, Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/domain/ModelGuildWelcomeScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;)",
            "Lcom/discord/models/domain/ModelGuildWelcomeScreen;"
        }
    .end annotation

    new-instance v0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    invoke-direct {v0, p1, p2}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getWelcomeChannels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelGuildWelcomeScreen(description="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", welcomeChannels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->welcomeChannels:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
