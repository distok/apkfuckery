.class public final Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;
.super Ljava/lang/Object;
.source "ModelConnectedAccountIntegration.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$Parser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelConnectedAccountIntegration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$Parser<",
        "Lcom/discord/models/domain/ModelConnectedAccountIntegration;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final INSTANCE:Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/m;

    const-class v2, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;

    const-string v3, "id"

    const-string v4, "<v#0>"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/m;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;->INSTANCE:Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelConnectedAccountIntegration;
    .locals 14

    const-string v0, "reader"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lx/n/a;

    invoke-direct {v0}, Lx/n/a;-><init>()V

    sget-object v1, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v8, v1, v2

    new-instance v9, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v9}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v10, 0x0

    iput-object v10, v9, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v11, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v11}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v10, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v12, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v12}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v10, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v13, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser$parse$1;

    move-object v1, v13

    move-object v2, v0

    move-object v3, v8

    move-object v4, p1

    move-object v5, v9

    move-object v6, v11

    move-object v7, v12

    invoke-direct/range {v1 .. v7}, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser$parse$1;-><init>(Lkotlin/properties/ReadWriteProperty;Lkotlin/reflect/KProperty;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, v13}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    invoke-virtual {v0, v10, v8}, Lx/n/a;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    iget-object v0, v9, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    iget-object v0, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    iget-object v0, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/discord/models/domain/ModelConnectedAccountIntegration;-><init>(JLjava/lang/String;Lcom/discord/models/domain/ModelConnectedIntegrationAccount;Lcom/discord/models/domain/ModelConnectedIntegrationGuild;)V

    return-object p1
.end method

.method public bridge synthetic parse(Lcom/discord/models/domain/Model$JsonReader;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    move-result-object p1

    return-object p1
.end method
