.class public final Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;
.super Ljava/lang/Object;
.source "ModelConnectedIntegrationAccount.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$Parser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelConnectedIntegrationAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$Parser<",
        "Lcom/discord/models/domain/ModelConnectedIntegrationAccount;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final INSTANCE:Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/m;

    const-string v3, "id"

    const-string v4, "<v#0>"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/m;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/m;

    const-string v6, "name"

    const-string v7, "<v#1>"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/m;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;->INSTANCE:Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelConnectedIntegrationAccount;
    .locals 11

    const-string v0, "reader"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lx/n/a;

    invoke-direct {v0}, Lx/n/a;-><init>()V

    sget-object v1, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v7, v1, v2

    new-instance v8, Lx/n/a;

    invoke-direct {v8}, Lx/n/a;-><init>()V

    const/4 v2, 0x1

    aget-object v9, v1, v2

    new-instance v10, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser$parse$1;

    move-object v1, v10

    move-object v2, v0

    move-object v3, v7

    move-object v4, p1

    move-object v5, v8

    move-object v6, v9

    invoke-direct/range {v1 .. v6}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser$parse$1;-><init>(Lkotlin/properties/ReadWriteProperty;Lkotlin/reflect/KProperty;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/properties/ReadWriteProperty;Lkotlin/reflect/KProperty;)V

    invoke-virtual {p1, v10}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance p1, Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v7}, Lx/n/a;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-virtual {v8, v1, v9}, Lx/n/a;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p1, v2, v3, v0}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount;-><init>(JLjava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic parse(Lcom/discord/models/domain/Model$JsonReader;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    move-result-object p1

    return-object p1
.end method
