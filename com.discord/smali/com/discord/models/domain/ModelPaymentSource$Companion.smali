.class public final Lcom/discord/models/domain/ModelPaymentSource$Companion;
.super Ljava/lang/Object;
.source "ModelPaymentSource.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelPaymentSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/domain/ModelPaymentSource$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final wrap(Lcom/discord/models/domain/PaymentSourceRaw;)Lcom/discord/models/domain/ModelPaymentSource;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/discord/models/domain/ModelPaymentSource$UnableToWrapException;
        }
    .end annotation

    const-string v0, "raw"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourcePaypal;

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getInvalid()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getBillingAddress()Lcom/discord/models/domain/billing/ModelBillingAddress;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getDefault()Z

    move-result v7

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourcePaypal;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/discord/models/domain/billing/ModelBillingAddress;Z)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unsupported payment source type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getType()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v9, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourceCard;

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getBrand()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getLast_4()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getExpiresMonth()I

    move-result v3

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getExpiresYear()I

    move-result v4

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getInvalid()Z

    move-result v6

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getBillingAddress()Lcom/discord/models/domain/billing/ModelBillingAddress;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/models/domain/PaymentSourceRaw;->getDefault()Z

    move-result v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourceCard;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ZLcom/discord/models/domain/billing/ModelBillingAddress;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v9

    :goto_0
    return-object v0

    :catchall_0
    move-exception p1

    new-instance v0, Lcom/discord/models/domain/ModelPaymentSource$UnableToWrapException;

    invoke-direct {v0, p1}, Lcom/discord/models/domain/ModelPaymentSource$UnableToWrapException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
