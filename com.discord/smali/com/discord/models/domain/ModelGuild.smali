.class public Lcom/discord/models/domain/ModelGuild;
.super Ljava/lang/Object;
.source "ModelGuild.java"

# interfaces
.implements Lcom/discord/models/domain/Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelGuild$VanityUrlResponse;,
        Lcom/discord/models/domain/ModelGuild$PruneCountResponse;
    }
.end annotation


# static fields
.field public static final CHANNEL_UNSET:I = 0x0

.field private static final EMPTY_CHANNELS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_MEMBERS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXPLICIT_CONTENT_FILTER_ALL:I = 0x2

.field public static final EXPLICIT_CONTENT_FILTER_NONE:I = 0x0

.field public static final EXPLICIT_CONTENT_FILTER_SOME:I = 0x1

.field public static final FEATURE_ANIMATED_ICON:I = 0xc

.field public static final FEATURE_BANNER:I = 0x5

.field public static final FEATURE_COMMERCE:I = 0x8

.field public static final FEATURE_COMMUNITY:I = 0x7

.field public static final FEATURE_DISCOVERABLE:I = 0xa

.field public static final FEATURE_FEATURABLE:I = 0xb

.field public static final FEATURE_INVITE_SPLASH:I = 0x1

.field public static final FEATURE_MEMBER_VERIFICATION_GATE_ENABLED:I = 0xd

.field public static final FEATURE_MORE_EMOJI:I = 0x4

.field public static final FEATURE_NEWS:I = 0x9

.field public static final FEATURE_PARTNERED:I = 0x6

.field public static final FEATURE_VANITY_URL:I = 0x2

.field public static final FEATURE_VERIFIED_SERVER:I = 0x3

.field public static final FEATURE_VIP_REGIONS:I = 0x0

.field public static final FILE_SIZE_PREMIUM_100MB:I = 0x64

.field public static final FILE_SIZE_PREMIUM_50MB:I = 0x32

.field public static final FILE_SIZE_STANDARD:I = 0x8

.field public static final ICON_UNSET:Ljava/lang/String; = "ICON_UNSET"

.field public static final LARGE_GUILD_THRESHOLD:I = 0xc8

.field public static final MAX_MEMBERS_NOTIFY_ALL_MESSAGES:I = 0x9c4

.field private static final MAX_VIDEO_CHANNEL_USERS_UNLIMITED:I = -0x1

.field public static final SYSTEM_CHANNEL_FLAG_SUPPRESS_BOOST:I = 0x2

.field public static final SYSTEM_CHANNEL_FLAG_SUPPRESS_JOIN:I = 0x1

.field public static final TIER_1_BOOSTS:I = 0x2

.field public static final TIER_2_BOOSTS:I = 0xf

.field public static final TIER_3_BOOSTS:I = 0x1e

.field public static final VERIFICATION_LEVEL_HIGH:I = 0x3

.field public static final VERIFICATION_LEVEL_HIGHEST:I = 0x4

.field public static final VERIFICATION_LEVEL_LOW:I = 0x1

.field public static final VERIFICATION_LEVEL_MEDIUM:I = 0x2

.field public static final VERIFICATION_LEVEL_NONE:I


# instance fields
.field private afkChannelId:Ljava/lang/Long;

.field private afkTimeout:I

.field private banner:Ljava/lang/String;

.field private transient channelUpdates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation
.end field

.field private channels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation
.end field

.field private defaultMessageNotifications:Ljava/lang/Integer;

.field private description:Ljava/lang/String;

.field private emojis:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom;",
            ">;"
        }
    .end annotation
.end field

.field private explicitContentFilter:Ljava/lang/Integer;

.field private features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private transient guildHashes:Lcom/discord/models/domain/ModelGuildHash;

.field private icon:Ljava/lang/String;

.field private id:J

.field private joinedAt:Ljava/lang/String;

.field private lazy:Ljava/lang/Boolean;

.field private maxVideoChannelUsers:Ljava/lang/Integer;

.field private memberCount:I

.field private members:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation
.end field

.field private mfaLevel:I

.field private name:Ljava/lang/String;

.field private ownerId:J

.field private transient permissions:Ljava/lang/Integer;

.field private preferredLocale:Ljava/lang/String;

.field private premiumSubscriptionCount:Ljava/lang/Integer;

.field private premiumTier:Ljava/lang/Integer;

.field private presences:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation
.end field

.field private publicUpdatesChannelId:Ljava/lang/Long;

.field private region:Ljava/lang/String;

.field private roles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation
.end field

.field private rulesChannelId:Ljava/lang/Long;

.field private final shortName:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private splash:Ljava/lang/String;

.field private systemChannelFlags:Ljava/lang/Integer;

.field private systemChannelId:Ljava/lang/Long;

.field private unavailable:Z

.field private vanityUrlCode:Ljava/lang/String;

.field private verificationLevel:Ljava/lang/Integer;

.field private voiceStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;"
        }
    .end annotation
.end field

.field private welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelGuild;->EMPTY_MEMBERS:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelGuild;->EMPTY_CHANNELS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    const-string v0, "ICON_UNSET"

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    const-string v0, "ICON_UNSET"

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->roles:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->roles:Ljava/util/List;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    iget-wide v0, p1, Lcom/discord/models/domain/ModelGuild;->id:J

    iput-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->id:J

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    iget-wide v0, p1, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    iput-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->presences:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->presences:Ljava/util/List;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->members:Ljava/util/Map;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->members:Ljava/util/Map;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->voiceStates:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->voiceStates:Ljava/util/List;

    iget-boolean v0, p1, Lcom/discord/models/domain/ModelGuild;->unavailable:Z

    iput-boolean v0, p0, Lcom/discord/models/domain/ModelGuild;->unavailable:Z

    iget v0, p1, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    iput v0, p0, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    iget v0, p1, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    iput v0, p0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->lazy:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->lazy:Ljava/lang/Boolean;

    iget v0, p1, Lcom/discord/models/domain/ModelGuild;->memberCount:I

    iput v0, p0, Lcom/discord/models/domain/ModelGuild;->memberCount:I

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->premiumSubscriptionCount:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->premiumSubscriptionCount:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->permissions:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->permissions:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->guildHashes:Lcom/discord/models/domain/ModelGuildHash;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->guildHashes:Lcom/discord/models/domain/ModelGuildHash;

    iget-object v0, p1, Lcom/discord/models/domain/ModelGuild;->channelUpdates:Ljava/util/List;

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->channelUpdates:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    return-void
.end method

.method public static compareGuildNames(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;)I
    .locals 3

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x1

    if-nez p1, :cond_2

    return v2

    :cond_2
    iget-object p0, p0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    if-nez p0, :cond_4

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_4
    if-nez p1, :cond_5

    return v1

    :cond_5
    invoke-virtual {p0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static computeFeature(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 18

    move-object/from16 v0, p0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v2, -0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/16 v4, 0xd

    const/16 v5, 0xc

    const/16 v6, 0xb

    const/16 v7, 0xa

    const/16 v8, 0x9

    const/16 v9, 0x8

    const/4 v10, 0x7

    const/4 v11, 0x6

    const/4 v12, 0x5

    const/4 v13, 0x4

    const/4 v14, 0x3

    const/4 v15, 0x2

    const/16 v16, 0x1

    const/16 v17, 0x0

    sparse-switch v3, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v3, "BANNER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v2, 0xd

    goto/16 :goto_0

    :sswitch_1
    const-string v3, "MORE_EMOJI"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v2, 0xc

    goto/16 :goto_0

    :sswitch_2
    const-string v3, "MEMBER_VERIFICATION_GATE_ENABLED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v2, 0xb

    goto/16 :goto_0

    :sswitch_3
    const-string v3, "COMMUNITY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v2, 0xa

    goto/16 :goto_0

    :sswitch_4
    const-string v3, "ANIMATED_ICON"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v2, 0x9

    goto/16 :goto_0

    :sswitch_5
    const-string v3, "PARTNERED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v2, 0x8

    goto/16 :goto_0

    :sswitch_6
    const-string v3, "FEATURABLE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    goto :goto_0

    :cond_7
    const/4 v2, 0x7

    goto :goto_0

    :sswitch_7
    const-string v3, "COMMERCE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    goto :goto_0

    :cond_8
    const/4 v2, 0x6

    goto :goto_0

    :sswitch_8
    const-string v3, "VANITY_URL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    goto :goto_0

    :cond_9
    const/4 v2, 0x5

    goto :goto_0

    :sswitch_9
    const-string v3, "NEWS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    goto :goto_0

    :cond_a
    const/4 v2, 0x4

    goto :goto_0

    :sswitch_a
    const-string v3, "VIP_REGIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    goto :goto_0

    :cond_b
    const/4 v2, 0x3

    goto :goto_0

    :sswitch_b
    const-string v3, "INVITE_SPLASH"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    goto :goto_0

    :cond_c
    const/4 v2, 0x2

    goto :goto_0

    :sswitch_c
    const-string v3, "DISCOVERABLE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    goto :goto_0

    :cond_d
    const/4 v2, 0x1

    goto :goto_0

    :sswitch_d
    const-string v3, "VERIFIED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    goto :goto_0

    :cond_e
    const/4 v2, 0x0

    :goto_0
    packed-switch v2, :pswitch_data_0

    return-object v1

    :pswitch_0
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_1
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_3
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_5
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_6
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_7
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_8
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_9
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_a
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_b
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_c
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :pswitch_d
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4839f138 -> :sswitch_d
        -0x2fab273d -> :sswitch_c
        -0x1cfad3a3 -> :sswitch_b
        -0x4c01663 -> :sswitch_a
        0x2482d3 -> :sswitch_9
        0x6b879bb -> :sswitch_8
        0xabddadb -> :sswitch_7
        0x13ecb129 -> :sswitch_6
        0x3b10e747 -> :sswitch_5
        0x4d6a6195 -> :sswitch_4
        0x4ddd3fc9 -> :sswitch_3
        0x578da1ec -> :sswitch_2
        0x69708abc -> :sswitch_1
        0x7458732c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static computeFeatureInv(I)Ljava/lang/String;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    const-string p0, "MEMBER_VERIFICATION_GATE_ENABLED"

    return-object p0

    :pswitch_1
    const-string p0, "ANIMATED_ICON"

    return-object p0

    :pswitch_2
    const-string p0, "FEATURABLE"

    return-object p0

    :pswitch_3
    const-string p0, "DISCOVERABLE"

    return-object p0

    :pswitch_4
    const-string p0, "NEWS"

    return-object p0

    :pswitch_5
    const-string p0, "COMMERCE"

    return-object p0

    :pswitch_6
    const-string p0, "COMMUNITY"

    return-object p0

    :pswitch_7
    const-string p0, "PARTNERED"

    return-object p0

    :pswitch_8
    const-string p0, "BANNER"

    return-object p0

    :pswitch_9
    const-string p0, "MORE_EMOJI"

    return-object p0

    :pswitch_a
    const-string p0, "VERIFIED"

    return-object p0

    :pswitch_b
    const-string p0, "VANITY_URL"

    return-object p0

    :pswitch_c
    const-string p0, "INVITE_SPLASH"

    return-object p0

    :pswitch_d
    const-string p0, "VIP_REGIONS"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static computeShortName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    :goto_0
    if-ge v4, v1, :cond_4

    aget-char v6, p0, v4

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Character;->charValue()C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_1

    :goto_1
    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    invoke-virtual {v6}, Ljava/lang/Character;->charValue()C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->isLetter(C)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createForTesting(JLjava/lang/String;)Lcom/discord/models/domain/ModelGuild;
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    new-instance v0, Lcom/discord/models/domain/ModelGuild;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuild;-><init>()V

    iput-wide p0, v0, Lcom/discord/models/domain/ModelGuild;->id:J

    iput-object p2, v0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    return-object v0
.end method

.method public static createForTesting(JLjava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/ModelGuild;
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/discord/models/domain/ModelGuild;->createForTesting(JLjava/lang/String;)Lcom/discord/models/domain/ModelGuild;

    move-result-object p0

    iput-object p3, p0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    return-object p0
.end method

.method public static createPartial(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/models/domain/ModelGuild;
    .locals 6

    if-nez p0, :cond_0

    new-instance p0, Lcom/discord/models/domain/ModelGuild;

    invoke-direct {p0}, Lcom/discord/models/domain/ModelGuild;-><init>()V

    :cond_0
    if-nez p1, :cond_1

    new-instance p1, Lcom/discord/models/domain/ModelGuild;

    invoke-direct {p1}, Lcom/discord/models/domain/ModelGuild;-><init>()V

    :cond_1
    new-instance v0, Lcom/discord/models/domain/ModelGuild;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuild;-><init>()V

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    :goto_0
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    :goto_1
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    :goto_2
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    iget-wide v1, p1, Lcom/discord/models/domain/ModelGuild;->id:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_5

    goto :goto_3

    :cond_5
    iget-wide v1, p0, Lcom/discord/models/domain/ModelGuild;->id:J

    :goto_3
    iput-wide v1, v0, Lcom/discord/models/domain/ModelGuild;->id:J

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    if-eqz v1, :cond_6

    goto :goto_4

    :cond_6
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    :goto_4
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    iget-wide v1, p1, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_7

    goto :goto_5

    :cond_7
    iget-wide v1, p0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    :goto_5
    iput-wide v1, v0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    goto :goto_6

    :cond_8
    const-string v2, "ICON_UNSET"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    goto :goto_6

    :cond_9
    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    :goto_6
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    goto :goto_7

    :cond_a
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    :goto_7
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    goto :goto_8

    :cond_b
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    :goto_8
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    iget v1, p1, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    iput v1, v0, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    iget v1, p1, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    if-eqz v1, :cond_c

    goto :goto_9

    :cond_c
    iget v1, p0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    :goto_9
    iput v1, v0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    if-eqz v1, :cond_d

    goto :goto_a

    :cond_d
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    :goto_a
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    if-eqz v1, :cond_e

    goto :goto_b

    :cond_e
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    :goto_b
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v1, :cond_f

    goto :goto_c

    :cond_f
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    :goto_c
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->lazy:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    goto :goto_d

    :cond_10
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->lazy:Ljava/lang/Boolean;

    :goto_d
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->lazy:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    if-eqz v1, :cond_11

    goto :goto_e

    :cond_11
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    :goto_e
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    if-eqz v1, :cond_12

    goto :goto_f

    :cond_12
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    :goto_f
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    goto :goto_10

    :cond_13
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    :goto_10
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_14

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v1

    goto :goto_11

    :cond_14
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v1

    :goto_11
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->premiumSubscriptionCount:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    goto :goto_12

    :cond_15
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    :goto_12
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    if-eqz v1, :cond_16

    goto :goto_13

    :cond_16
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    :goto_13
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    if-eqz v1, :cond_17

    goto :goto_14

    :cond_17
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    :goto_14
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    if-eqz v1, :cond_18

    goto :goto_15

    :cond_18
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    :goto_15
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    if-eqz v1, :cond_19

    goto :goto_16

    :cond_19
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    :goto_16
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    if-eqz v1, :cond_1a

    goto :goto_17

    :cond_1a
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    :goto_17
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    if-eqz v1, :cond_1b

    goto :goto_18

    :cond_1b
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    :goto_18
    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    if-eqz p1, :cond_1c

    goto :goto_19

    :cond_1c
    iget-object p1, p0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    :goto_19
    iput-object p1, v0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    return-object v0
.end method

.method public static getEmojiMaxCount(IZ)I
    .locals 1

    const/4 v0, 0x2

    if-eqz p1, :cond_0

    if-gt p0, v0, :cond_0

    const/16 p0, 0xc8

    return p0

    :cond_0
    const/4 p1, 0x1

    if-eq p0, p1, :cond_3

    if-eq p0, v0, :cond_2

    const/4 p1, 0x3

    if-eq p0, p1, :cond_1

    const/16 p0, 0x32

    return p0

    :cond_1
    const/16 p0, 0xfa

    return p0

    :cond_2
    const/16 p0, 0x96

    return p0

    :cond_3
    const/16 p0, 0x64

    return p0
.end method

.method public static getMaxFileSizeMB(Ljava/lang/Integer;)I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/16 p0, 0x8

    return p0

    :cond_0
    const/16 p0, 0x64

    return p0

    :cond_1
    const/16 p0, 0x32

    return p0
.end method

.method public static getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/16 v0, 0x180

    if-eqz p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/4 p1, 0x1

    if-eq p0, p1, :cond_3

    const/4 p1, 0x2

    if-eq p0, p1, :cond_2

    const/4 p1, 0x3

    if-eq p0, p1, :cond_1

    const/16 p0, 0x60

    return p0

    :cond_1
    return v0

    :cond_2
    const/16 p0, 0x100

    return p0

    :cond_3
    const/16 p0, 0x80

    return p0
.end method

.method public static getVerificationLevel(Lcom/discord/models/domain/ModelGuild;)I
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private hasMoreEmoji()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public assignField(Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v1, "preferred_locale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v3, 0x23

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "owner_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v3, 0x22

    goto/16 :goto_0

    :sswitch_2
    const-string/jumbo v1, "welcome_screen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v3, 0x21

    goto/16 :goto_0

    :sswitch_3
    const-string/jumbo v1, "vanity_url_code"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v3, 0x20

    goto/16 :goto_0

    :sswitch_4
    const-string v1, "channels"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v3, 0x1f

    goto/16 :goto_0

    :sswitch_5
    const-string v1, "default_message_notifications"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v3, 0x1e

    goto/16 :goto_0

    :sswitch_6
    const-string v1, "premium_subscription_count"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v3, 0x1d

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "public_updates_channel_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v3, 0x1c

    goto/16 :goto_0

    :sswitch_8
    const-string v1, "members"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v3, 0x1b

    goto/16 :goto_0

    :sswitch_9
    const-string v1, "mfa_level"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    goto/16 :goto_0

    :cond_9
    const/16 v3, 0x1a

    goto/16 :goto_0

    :sswitch_a
    const-string v1, "rules_channel_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    goto/16 :goto_0

    :cond_a
    const/16 v3, 0x19

    goto/16 :goto_0

    :sswitch_b
    const-string/jumbo v1, "voice_states"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v3, 0x18

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "roles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    goto/16 :goto_0

    :cond_c
    const/16 v3, 0x17

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    goto/16 :goto_0

    :cond_d
    const/16 v3, 0x16

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "icon"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    goto/16 :goto_0

    :cond_e
    const/16 v3, 0x15

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    goto/16 :goto_0

    :cond_f
    const/16 v3, 0x14

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "member_count"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    goto/16 :goto_0

    :cond_10
    const/16 v3, 0x13

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "afk_timeout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    goto/16 :goto_0

    :cond_11
    const/16 v3, 0x12

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "features"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    goto/16 :goto_0

    :cond_12
    const/16 v3, 0x11

    goto/16 :goto_0

    :sswitch_13
    const-string v1, "joined_at"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    goto/16 :goto_0

    :cond_13
    const/16 v3, 0x10

    goto/16 :goto_0

    :sswitch_14
    const-string/jumbo v1, "verification_level"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    goto/16 :goto_0

    :cond_14
    const/16 v3, 0xf

    goto/16 :goto_0

    :sswitch_15
    const-string/jumbo v1, "unavailable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    goto/16 :goto_0

    :cond_15
    const/16 v3, 0xe

    goto/16 :goto_0

    :sswitch_16
    const-string v1, "guild_hashes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    goto/16 :goto_0

    :cond_16
    const/16 v3, 0xd

    goto/16 :goto_0

    :sswitch_17
    const-string/jumbo v1, "splash"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    goto/16 :goto_0

    :cond_17
    const/16 v3, 0xc

    goto/16 :goto_0

    :sswitch_18
    const-string v1, "presences"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    goto/16 :goto_0

    :cond_18
    const/16 v3, 0xb

    goto/16 :goto_0

    :sswitch_19
    const-string v1, "region"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    goto/16 :goto_0

    :cond_19
    const/16 v3, 0xa

    goto/16 :goto_0

    :sswitch_1a
    const-string/jumbo v1, "system_channel_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    goto/16 :goto_0

    :cond_1a
    const/16 v3, 0x9

    goto/16 :goto_0

    :sswitch_1b
    const-string v1, "premium_tier"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    goto/16 :goto_0

    :cond_1b
    const/16 v3, 0x8

    goto/16 :goto_0

    :sswitch_1c
    const-string v1, "emojis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    goto :goto_0

    :cond_1c
    const/4 v3, 0x7

    goto :goto_0

    :sswitch_1d
    const-string v1, "banner"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    goto :goto_0

    :cond_1d
    const/4 v3, 0x6

    goto :goto_0

    :sswitch_1e
    const-string v1, "afk_channel_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    goto :goto_0

    :cond_1e
    const/4 v3, 0x5

    goto :goto_0

    :sswitch_1f
    const-string v1, "explicit_content_filter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    goto :goto_0

    :cond_1f
    const/4 v3, 0x4

    goto :goto_0

    :sswitch_20
    const-string/jumbo v1, "system_channel_flags"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    goto :goto_0

    :cond_20
    const/4 v3, 0x3

    goto :goto_0

    :sswitch_21
    const-string v1, "description"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    goto :goto_0

    :cond_21
    const/4 v3, 0x2

    goto :goto_0

    :sswitch_22
    const-string v1, "channel_updates"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    goto :goto_0

    :cond_22
    const/4 v3, 0x1

    goto :goto_0

    :sswitch_23
    const-string v1, "max_video_channel_users"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    goto :goto_0

    :cond_23
    const/4 v3, 0x0

    :goto_0
    const-wide/16 v0, 0x0

    const-string v4, ""

    packed-switch v3, :pswitch_data_0

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    goto/16 :goto_1

    :pswitch_0
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_1
    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_24

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextNull()V

    goto/16 :goto_1

    :cond_24
    sget-object v0, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->Parser:Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelGuildWelcomeScreen$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_4
    new-instance v0, Lf/a/d/a/x;

    invoke-direct {v0, p1}, Lf/a/d/a/x;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextIntOrNull()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual {p1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->premiumSubscriptionCount:Ljava/lang/Integer;

    goto/16 :goto_1

    :pswitch_7
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    goto/16 :goto_1

    :pswitch_8
    new-instance v0, Lf/a/d/a/b0;

    invoke-direct {v0, p1}, Lf/a/d/a/b0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    sget-object v1, Lf/a/d/a/v;->a:Lf/a/d/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextListAsMap(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;Lcom/discord/models/domain/Model$JsonReader$KeySelector;)Ljava/util/HashMap;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->members:Ljava/util/Map;

    goto/16 :goto_1

    :pswitch_9
    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    iput p1, p0, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    goto/16 :goto_1

    :pswitch_a
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    goto/16 :goto_1

    :pswitch_b
    new-instance v0, Lf/a/d/a/a0;

    invoke-direct {v0, p1}, Lf/a/d/a/a0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->voiceStates:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_c
    new-instance v0, Lf/a/d/a/y;

    invoke-direct {v0, p1}, Lf/a/d/a/y;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->roles:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_d
    invoke-virtual {p1, v4}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_e
    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_f
    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->id:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->id:J

    goto/16 :goto_1

    :pswitch_10
    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->memberCount:I

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    iput p1, p0, Lcom/discord/models/domain/ModelGuild;->memberCount:I

    goto/16 :goto_1

    :pswitch_11
    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    iput p1, p0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    goto/16 :goto_1

    :pswitch_12
    new-instance v0, Ljava/util/ArrayList;

    new-instance v1, Lf/a/d/a/w;

    invoke-direct {v1, p1}, Lf/a/d/a/w;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_13
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_14
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextIntOrNull()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    goto/16 :goto_1

    :pswitch_15
    iget-boolean v0, p0, Lcom/discord/models/domain/ModelGuild;->unavailable:Z

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextBoolean(Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/models/domain/ModelGuild;->unavailable:Z

    goto/16 :goto_1

    :pswitch_16
    sget-object v0, Lcom/discord/models/domain/ModelGuildHash;->Parser:Lcom/discord/models/domain/ModelGuildHash$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelGuildHash$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->guildHashes:Lcom/discord/models/domain/ModelGuildHash;

    goto/16 :goto_1

    :pswitch_17
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_18
    new-instance v0, Lf/a/d/a/c0;

    invoke-direct {v0, p1}, Lf/a/d/a/c0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->presences:Ljava/util/List;

    goto :goto_1

    :pswitch_19
    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    goto :goto_1

    :pswitch_1a
    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    goto :goto_1

    :pswitch_1b
    invoke-virtual {p1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_1c
    new-instance v0, Lf/a/d/a/z;

    invoke-direct {v0, p1}, Lf/a/d/a/z;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    goto :goto_1

    :pswitch_1d
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    goto :goto_1

    :pswitch_1e
    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    goto :goto_1

    :pswitch_1f
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextIntOrNull()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_20
    invoke-virtual {p1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    goto :goto_1

    :pswitch_21
    invoke-virtual {p1, v4}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    goto :goto_1

    :pswitch_22
    new-instance v0, Lf/a/d/a/u;

    invoke-direct {v0, p1}, Lf/a/d/a/u;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->channelUpdates:Ljava/util/List;

    goto :goto_1

    :pswitch_23
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextIntOrNull()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x748e1453 -> :sswitch_23
        -0x695dae72 -> :sswitch_22
        -0x66ca7c04 -> :sswitch_21
        -0x66a3f9c5 -> :sswitch_20
        -0x65a25c97 -> :sswitch_1f
        -0x5db95d30 -> :sswitch_1e
        -0x533a80d4 -> :sswitch_1d
        -0x4d727713 -> :sswitch_1c
        -0x4a4d6536 -> :sswitch_1b
        -0x4191ce19 -> :sswitch_1a
        -0x37b7d90c -> :sswitch_19
        -0x36f3fd28 -> :sswitch_18
        -0x3565d599 -> :sswitch_17
        -0x2f9843f8 -> :sswitch_16
        -0x27aa27b0 -> :sswitch_15
        -0x1e2c6ba0 -> :sswitch_14
        -0x1d260717 -> :sswitch_13
        -0x11531bc3 -> :sswitch_12
        -0x93bd8f8 -> :sswitch_11
        -0x3899cd6 -> :sswitch_10
        0xd1b -> :sswitch_f
        0x313c79 -> :sswitch_e
        0x337a8b -> :sswitch_d
        0x67a8ebd -> :sswitch_c
        0xbb68daf -> :sswitch_b
        0x2a14ce1f -> :sswitch_a
        0x3855966d -> :sswitch_9
        0x388ec919 -> :sswitch_8
        0x38ae6602 -> :sswitch_7
        0x3d7e9655 -> :sswitch_6
        0x47f6cf32 -> :sswitch_5
        0x556423d0 -> :sswitch_4
        0x5e3755d1 -> :sswitch_3
        0x5f54efe9 -> :sswitch_2
        0x63219e27 -> :sswitch_1
        0x6ad91bd8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/models/domain/ModelGuild;

    return p1
.end method

.method public canHaveAnimatedServerIcon()Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public canHaveServerBanner()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->isVerifiedServer()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public canHaveSplash()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->isVerifiedServer()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    if-lt v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public canHaveVanityURL()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/models/domain/ModelGuild;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1, p0}, Lcom/discord/models/domain/ModelGuild;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRoles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getRoles()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    return v2

    :cond_4
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getEmojis()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getEmojis()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_5

    if-eqz v3, :cond_6

    goto :goto_1

    :cond_5
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_1
    return v2

    :cond_6
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_7

    if-eqz v3, :cond_8

    goto :goto_2

    :cond_7
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    :goto_2
    return v2

    :cond_8
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getDescription()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_9

    if-eqz v3, :cond_a

    goto :goto_3

    :cond_9
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    :goto_3
    return v2

    :cond_a
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v3

    if-eq v1, v3, :cond_b

    return v2

    :cond_b
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_c

    return v2

    :cond_c
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getRegion()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_d

    if-eqz v3, :cond_e

    goto :goto_4

    :cond_d
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    :goto_4
    return v2

    :cond_e
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_f

    return v2

    :cond_f
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_10

    if-eqz v3, :cond_11

    goto :goto_5

    :cond_10
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    :goto_5
    return v2

    :cond_11
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v3

    if-eq v1, v3, :cond_12

    return v2

    :cond_12
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getExplicitContentFilter()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getExplicitContentFilter()I

    move-result v3

    if-eq v1, v3, :cond_13

    return v2

    :cond_13
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPresences()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPresences()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_14

    if-eqz v3, :cond_15

    goto :goto_6

    :cond_14
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    :goto_6
    return v2

    :cond_15
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_16

    if-eqz v3, :cond_17

    goto :goto_7

    :cond_16
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    :goto_7
    return v2

    :cond_17
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMembers()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMembers()Ljava/util/Map;

    move-result-object v3

    if-nez v1, :cond_18

    if-eqz v3, :cond_19

    goto :goto_8

    :cond_18
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    :goto_8
    return v2

    :cond_19
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVoiceStates()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getVoiceStates()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_1a

    if-eqz v3, :cond_1b

    goto :goto_9

    :cond_1a
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    :goto_9
    return v2

    :cond_1b
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->isUnavailable()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isUnavailable()Z

    move-result v3

    if-eq v1, v3, :cond_1c

    return v2

    :cond_1c
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v3

    if-eq v1, v3, :cond_1d

    return v2

    :cond_1d
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getAfkTimeout()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getAfkTimeout()I

    move-result v3

    if-eq v1, v3, :cond_1e

    return v2

    :cond_1e
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getAfkChannelId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getAfkChannelId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1f

    return v2

    :cond_1f
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_20

    return v2

    :cond_20
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getJoinedAt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getJoinedAt()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_21

    if-eqz v3, :cond_22

    goto :goto_a

    :cond_21
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    :goto_a
    return v2

    :cond_22
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getFeatures()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getFeatures()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_23

    if-eqz v3, :cond_24

    goto :goto_b

    :cond_23
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_24

    :goto_b
    return v2

    :cond_24
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getLazy()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getLazy()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v1, :cond_25

    if-eqz v3, :cond_26

    goto :goto_c

    :cond_25
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    :goto_c
    return v2

    :cond_26
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMemberCount()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMemberCount()I

    move-result v3

    if-eq v1, v3, :cond_27

    return v2

    :cond_27
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_28

    if-eqz v3, :cond_29

    goto :goto_d

    :cond_28
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_29

    :goto_d
    return v2

    :cond_29
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSplash()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getSplash()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_2a

    if-eqz v3, :cond_2b

    goto :goto_e

    :cond_2a
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2b

    :goto_e
    return v2

    :cond_2b
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v3

    if-eq v1, v3, :cond_2c

    return v2

    :cond_2c
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v3

    if-nez v1, :cond_2d

    if-eqz v3, :cond_2e

    goto :goto_f

    :cond_2d
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2e

    :goto_f
    return v2

    :cond_2e
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelFlags()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelFlags()Ljava/lang/Integer;

    move-result-object v3

    if-nez v1, :cond_2f

    if-eqz v3, :cond_30

    goto :goto_10

    :cond_2f
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_30

    :goto_10
    return v2

    :cond_30
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_31

    if-eqz v3, :cond_32

    goto :goto_11

    :cond_31
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    :goto_11
    return v2

    :cond_32
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_33

    if-eqz v3, :cond_34

    goto :goto_12

    :cond_33
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_34

    :goto_12
    return v2

    :cond_34
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPreferredLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPreferredLocale()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_35

    if-eqz v3, :cond_36

    goto :goto_13

    :cond_35
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_36

    :goto_13
    return v2

    :cond_36
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getWelcomeScreen()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getWelcomeScreen()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object v3

    if-nez v1, :cond_37

    if-eqz v3, :cond_38

    goto :goto_14

    :cond_37
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_38

    :goto_14
    return v2

    :cond_38
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMaxVideoChannelUsers()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMaxVideoChannelUsers()Ljava/lang/Integer;

    move-result-object v3

    if-nez v1, :cond_39

    if-eqz v3, :cond_3a

    goto :goto_15

    :cond_39
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3a

    :goto_15
    return v2

    :cond_3a
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVanityUrlCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getVanityUrlCode()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_3b

    if-eqz v3, :cond_3c

    goto :goto_16

    :cond_3b
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3c

    :goto_16
    return v2

    :cond_3c
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object p1

    if-nez v1, :cond_3d

    if-eqz p1, :cond_3e

    goto :goto_17

    :cond_3d
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3e

    :goto_17
    return v2

    :cond_3e
    return v0
.end method

.method public getAfkChannelId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public getAfkTimeout()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    return v0
.end method

.method public getBanner()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelUpdates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->channelUpdates:Ljava/util/List;

    return-object v0
.end method

.method public getChannels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelGuild;->EMPTY_CHANNELS:Ljava/util/List;

    :goto_0
    return-object v0
.end method

.method public getDefaultMessageNotifications()I
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_ALL:I

    :goto_0
    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEmojiMaxCount()I
    .locals 2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    invoke-direct {p0}, Lcom/discord/models/domain/ModelGuild;->hasMoreEmoji()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v0

    return v0
.end method

.method public getEmojis()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    return-object v0
.end method

.method public getExplicitContentFilter()I
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getFeatures()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    return-object v0
.end method

.method public getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->guildHashes:Lcom/discord/models/domain/ModelGuildHash;

    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->id:J

    return-wide v0
.end method

.method public getJoinedAt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->joinedAt:Ljava/lang/String;

    return-object v0
.end method

.method public getLazy()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->lazy:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getMaxFileSizeMB()I
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public getMaxVideoChannelUsers()Ljava/lang/Integer;
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxVoiceBitrateKbps()I
    .locals 2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->isVip()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v0

    return v0
.end method

.method public getMemberCount()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->memberCount:I

    return v0
.end method

.method public getMembers()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->members:Ljava/util/Map;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelGuild;->EMPTY_MEMBERS:Ljava/util/Map;

    :goto_0
    return-object v0
.end method

.method public getMfaLevel()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    return-wide v0
.end method

.method public getPermissions()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->permissions:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPreferredLocale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getPremiumSubscriptionCount()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->premiumSubscriptionCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPremiumTier()I
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getPresences()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->presences:Ljava/util/List;

    return-object v0
.end method

.method public getPublicUpdatesChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    return-object v0
.end method

.method public getRoles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->roles:Ljava/util/List;

    return-object v0
.end method

.method public getRulesChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    return-object v0
.end method

.method public getShortName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/discord/models/domain/ModelGuild;->computeShortName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    :cond_0
    iget-object v2, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/discord/models/domain/ModelGuild;->shortName:Ljava/util/concurrent/atomic/AtomicReference;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    :cond_3
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSplash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    return-object v0
.end method

.method public getSystemChannelFlags()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSystemChannelId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public getVanityUrlCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    return-object v0
.end method

.method public getVerificationLevel()I
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getVoiceStates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->voiceStates:Ljava/util/List;

    return-object v0
.end method

.method public getWelcomeScreen()Lcom/discord/models/domain/ModelGuildWelcomeScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->welcomeScreen:Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    return-object v0
.end method

.method public hasIcon()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "ICON_UNSET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 8

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRoles()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x2b

    if-nez v0, :cond_0

    const/16 v0, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    const/16 v2, 0x3b

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getEmojis()Ljava/util/List;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_1

    const/16 v3, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_1
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_2

    const/16 v3, 0x2b

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_2
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getDescription()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_3

    const/16 v3, 0x2b

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v4

    mul-int/lit8 v3, v3, 0x3b

    const/16 v0, 0x20

    ushr-long v6, v4, v0

    xor-long/2addr v4, v6

    long-to-int v5, v4

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRegion()Ljava/lang/String;

    move-result-object v4

    mul-int/lit8 v3, v3, 0x3b

    if-nez v4, :cond_4

    const/16 v4, 0x2b

    goto :goto_4

    :cond_4
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_4
    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v4

    mul-int/lit8 v3, v3, 0x3b

    ushr-long v6, v4, v0

    xor-long/2addr v4, v6

    long-to-int v5, v4

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v4

    mul-int/lit8 v3, v3, 0x3b

    if-nez v4, :cond_5

    const/16 v4, 0x2b

    goto :goto_5

    :cond_5
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_5
    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v4

    add-int/2addr v4, v3

    mul-int/lit8 v4, v4, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getExplicitContentFilter()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPresences()Ljava/util/List;

    move-result-object v4

    mul-int/lit8 v3, v3, 0x3b

    if-nez v4, :cond_6

    const/16 v4, 0x2b

    goto :goto_6

    :cond_6
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_6
    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v4

    mul-int/lit8 v3, v3, 0x3b

    if-nez v4, :cond_7

    const/16 v4, 0x2b

    goto :goto_7

    :cond_7
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_7
    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMembers()Ljava/util/Map;

    move-result-object v4

    mul-int/lit8 v3, v3, 0x3b

    if-nez v4, :cond_8

    const/16 v4, 0x2b

    goto :goto_8

    :cond_8
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_8
    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVoiceStates()Ljava/util/List;

    move-result-object v4

    mul-int/lit8 v3, v3, 0x3b

    if-nez v4, :cond_9

    const/16 v4, 0x2b

    goto :goto_9

    :cond_9
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_9
    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->isUnavailable()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0x4f

    goto :goto_a

    :cond_a
    const/16 v4, 0x61

    :goto_a
    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v4

    add-int/2addr v4, v3

    mul-int/lit8 v4, v4, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getAfkTimeout()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getAfkChannelId()J

    move-result-wide v4

    mul-int/lit8 v3, v3, 0x3b

    ushr-long v6, v4, v0

    xor-long/2addr v4, v6

    long-to-int v5, v4

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelId()J

    move-result-wide v4

    mul-int/lit8 v3, v3, 0x3b

    ushr-long v6, v4, v0

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getJoinedAt()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_b

    const/16 v0, 0x2b

    goto :goto_b

    :cond_b
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getFeatures()Ljava/util/List;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_c

    const/16 v0, 0x2b

    goto :goto_c

    :cond_c
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getLazy()Ljava/lang/Boolean;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_d

    const/16 v0, 0x2b

    goto :goto_d

    :cond_d
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMemberCount()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_e

    const/16 v3, 0x2b

    goto :goto_e

    :cond_e
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_e
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSplash()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_f

    const/16 v3, 0x2b

    goto :goto_f

    :cond_f
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_f
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_10

    const/16 v0, 0x2b

    goto :goto_10

    :cond_10
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelFlags()Ljava/lang/Integer;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_11

    const/16 v0, 0x2b

    goto :goto_11

    :cond_11
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_12

    const/16 v0, 0x2b

    goto :goto_12

    :cond_12
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_13

    const/16 v0, 0x2b

    goto :goto_13

    :cond_13
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPreferredLocale()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_14

    const/16 v0, 0x2b

    goto :goto_14

    :cond_14
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_14
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getWelcomeScreen()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_15

    const/16 v0, 0x2b

    goto :goto_15

    :cond_15
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->hashCode()I

    move-result v0

    :goto_15
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMaxVideoChannelUsers()Ljava/lang/Integer;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_16

    const/16 v0, 0x2b

    goto :goto_16

    :cond_16
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_16
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVanityUrlCode()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_17

    const/16 v0, 0x2b

    goto :goto_17

    :cond_17
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_17
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_18

    goto :goto_18

    :cond_18
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_18
    add-int/2addr v3, v1

    return v3
.end method

.method public hydrate(Lcom/discord/models/domain/ModelGuild;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;Ljava/util/Map;)Lcom/discord/models/domain/ModelGuild;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/Collection<",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;)",
            "Lcom/discord/models/domain/ModelGuild;"
        }
    .end annotation

    new-instance v0, Lcom/discord/models/domain/ModelGuild;

    invoke-direct {v0, p0}, Lcom/discord/models/domain/ModelGuild;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->afkChannelId:Ljava/lang/Long;

    iget v1, p1, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    iput v1, v0, Lcom/discord/models/domain/ModelGuild;->afkTimeout:I

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->banner:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->defaultMessageNotifications:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->explicitContentFilter:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->icon:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->maxVideoChannelUsers:Ljava/lang/Integer;

    iget v1, p1, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    iput v1, v0, Lcom/discord/models/domain/ModelGuild;->mfaLevel:I

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->name:Ljava/lang/String;

    iget-wide v1, p1, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    iput-wide v1, v0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->preferredLocale:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->premiumTier:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->publicUpdatesChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->region:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->rulesChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->splash:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->systemChannelFlags:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->systemChannelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/discord/models/domain/ModelGuild;->verificationLevel:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    iput-object p1, v0, Lcom/discord/models/domain/ModelGuild;->vanityUrlCode:Ljava/lang/String;

    :cond_0
    if-eqz p2, :cond_1

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, v0, Lcom/discord/models/domain/ModelGuild;->emojis:Ljava/util/List;

    :cond_1
    if-eqz p3, :cond_5

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iget-object p2, v0, Lcom/discord/models/domain/ModelGuild;->channelUpdates:Ljava/util/List;

    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    iput-object p2, v0, Lcom/discord/models/domain/ModelGuild;->channelUpdates:Ljava/util/List;

    :cond_3
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, v0, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p3, v2}, Lcom/discord/models/domain/ModelChannel;->hydrate(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/models/domain/ModelChannel;

    move-result-object p3

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/discord/models/domain/ModelGuild;->channels:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    if-eqz p4, :cond_6

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, v0, Lcom/discord/models/domain/ModelGuild;->roles:Ljava/util/List;

    :cond_6
    if-eqz p5, :cond_7

    iput-object p5, v0, Lcom/discord/models/domain/ModelGuild;->presences:Ljava/util/List;

    :cond_7
    if-eqz p6, :cond_8

    iput-object p6, v0, Lcom/discord/models/domain/ModelGuild;->members:Ljava/util/Map;

    :cond_8
    return-object v0
.end method

.method public isCommunityServer()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isGatingEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isLarge()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/ModelGuild;->memberCount:I

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOwner(J)Z
    .locals 3

    iget-wide v0, p0, Lcom/discord/models/domain/ModelGuild;->ownerId:J

    cmp-long v2, v0, p1

    if-nez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isPartneredServer()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isUnavailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelGuild;->unavailable:Z

    return v0
.end method

.method public isVerifiedServer()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVip()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuild;->features:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelGuild(roles="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRoles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emojis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getEmojis()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultMessageNotifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", region="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ownerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", verificationLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", explicitContentFilter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getExplicitContentFilter()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", presences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPresences()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", members="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMembers()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voiceStates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVoiceStates()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unavailable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->isUnavailable()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", mfaLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", afkTimeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getAfkTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", afkChannelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getAfkChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", systemChannelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", joinedAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getJoinedAt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getFeatures()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lazy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getLazy()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", memberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMemberCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", banner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", splash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSplash()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", premiumTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", premiumSubscriptionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", systemChannelFlags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getSystemChannelFlags()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rulesChannelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", publicUpdatesChannelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preferredLocale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPreferredLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", welcomeScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getWelcomeScreen()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxVideoChannelUsers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getMaxVideoChannelUsers()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", vanityUrlCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getVanityUrlCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getPermissions()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildHashes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelUpdates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getChannelUpdates()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shortName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
