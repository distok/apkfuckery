.class public final Lcom/discord/models/domain/NonceGenerator;
.super Ljava/lang/Object;
.source "NonceGenerator.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/models/domain/NonceGenerator;

.field private static previousNonce:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/domain/NonceGenerator;

    invoke-direct {v0}, Lcom/discord/models/domain/NonceGenerator;-><init>()V

    sput-object v0, Lcom/discord/models/domain/NonceGenerator;->INSTANCE:Lcom/discord/models/domain/NonceGenerator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final computeNonce()J
    .locals 5

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x57b12c00

    add-long/2addr v0, v2

    const-wide v2, 0x14aa2cab000L

    sub-long/2addr v0, v2

    const/16 v2, 0x16

    shl-long/2addr v0, v2

    sget-wide v2, Lcom/discord/models/domain/NonceGenerator;->previousNonce:J

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    :cond_0
    sput-wide v0, Lcom/discord/models/domain/NonceGenerator;->previousNonce:J

    return-wide v0
.end method
