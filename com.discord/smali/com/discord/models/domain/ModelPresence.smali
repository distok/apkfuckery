.class public final Lcom/discord/models/domain/ModelPresence;
.super Ljava/lang/Object;
.source "ModelPresence.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelPresence$Status;,
        Lcom/discord/models/domain/ModelPresence$ClientStatuses;,
        Lcom/discord/models/domain/ModelPresence$Parser;
    }
.end annotation


# instance fields
.field private final activities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

.field private final customStatusActivity$delegate:Lkotlin/Lazy;

.field private final guildId:J

.field private final listeningActivity$delegate:Lkotlin/Lazy;

.field private final playingActivity$delegate:Lkotlin/Lazy;

.field private final primaryActivity$delegate:Lkotlin/Lazy;

.field private final status:Lcom/discord/models/domain/ModelPresence$Status;

.field private final streamingActivity$delegate:Lkotlin/Lazy;

.field private final user:Lcom/discord/models/domain/ModelUser;

.field private final userId:Ljava/lang/Long;

.field private final watchingActivity$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelPresence$Status;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;",
            "Lcom/discord/models/domain/ModelPresence$ClientStatuses;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/Long;",
            "J)V"
        }
    .end annotation

    const-string/jumbo v0, "status"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientStatuses"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    iput-object p2, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    iput-object p4, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    iput-object p5, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    iput-wide p6, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    new-instance p1, Lcom/discord/models/domain/ModelPresence$playingActivity$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelPresence$playingActivity$2;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->playingActivity$delegate:Lkotlin/Lazy;

    new-instance p1, Lcom/discord/models/domain/ModelPresence$streamingActivity$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelPresence$streamingActivity$2;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->streamingActivity$delegate:Lkotlin/Lazy;

    new-instance p1, Lcom/discord/models/domain/ModelPresence$listeningActivity$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelPresence$listeningActivity$2;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->listeningActivity$delegate:Lkotlin/Lazy;

    new-instance p1, Lcom/discord/models/domain/ModelPresence$watchingActivity$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelPresence$watchingActivity$2;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->watchingActivity$delegate:Lkotlin/Lazy;

    new-instance p1, Lcom/discord/models/domain/ModelPresence$customStatusActivity$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelPresence$customStatusActivity$2;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->customStatusActivity$delegate:Lkotlin/Lazy;

    new-instance p1, Lcom/discord/models/domain/ModelPresence$primaryActivity$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelPresence$primaryActivity$2;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence;->primaryActivity$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->Companion:Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;->empty()Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 v0, p8, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    move-object v5, v1

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    move-object v6, v1

    goto :goto_2

    :cond_2
    move-object v6, p5

    :goto_2
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x0

    move-wide v7, v0

    goto :goto_3

    :cond_3
    move-wide v7, p6

    :goto_3
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)V

    return-void
.end method

.method public static final synthetic access$getActivityByType(Lcom/discord/models/domain/ModelPresence;I)Lcom/discord/models/domain/activity/ModelActivity;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/models/domain/ModelPresence;->getActivityByType(I)Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p0

    return-object p0
.end method

.method private final component5()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILjava/lang/Object;)Lcom/discord/models/domain/ModelPresence;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-wide p6, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    :cond_5
    move-wide v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-wide p8, v3

    invoke-virtual/range {p2 .. p9}, Lcom/discord/models/domain/ModelPresence;->copy(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)Lcom/discord/models/domain/ModelPresence;

    move-result-object p0

    return-object p0
.end method

.method private final getActivityByType(I)Lcom/discord/models/domain/activity/ModelActivity;
    .locals 4

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v3

    if-ne v3, p1, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    move-object v1, v2

    :cond_2
    check-cast v1, Lcom/discord/models/domain/activity/ModelActivity;

    :cond_3
    return-object v1
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelPresence$ClientStatuses;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    return-object v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    return-wide v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)Lcom/discord/models/domain/ModelPresence;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelPresence$Status;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;",
            "Lcom/discord/models/domain/ModelPresence$ClientStatuses;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/Long;",
            "J)",
            "Lcom/discord/models/domain/ModelPresence;"
        }
    .end annotation

    const-string/jumbo v0, "status"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientStatuses"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/domain/ModelPresence;

    move-object v1, v0

    move-object v3, p2

    move-object v5, p4

    move-object v6, p5

    move-wide v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelPresence;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelPresence;

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelPresence;->guildId:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActivities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    return-object v0
.end method

.method public final getClientStatuses()Lcom/discord/models/domain/ModelPresence$ClientStatuses;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    return-object v0
.end method

.method public final getCustomStatusActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->customStatusActivity$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    return-wide v0
.end method

.method public final getListeningActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->listeningActivity$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final getPlayingActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->playingActivity$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->primaryActivity$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final getStatus()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final getStreamingActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->streamingActivity$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final getUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getWatchingActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->watchingActivity$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    const/16 v3, 0x20

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v2, v1

    add-int/2addr v0, v2

    return v0
.end method

.method public final hydrate(Ljava/util/Map;)Lcom/discord/models/domain/ModelPresence;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;)",
            "Lcom/discord/models/domain/ModelPresence;"
        }
    .end annotation

    const-string/jumbo v0, "users"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/discord/models/domain/ModelUser;

    const-wide/16 v7, 0x0

    const/16 v9, 0x27

    const/4 v10, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v10}, Lcom/discord/models/domain/ModelPresence;->copy$default(Lcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILjava/lang/Object;)Lcom/discord/models/domain/ModelPresence;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p0

    :goto_0
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ModelPresence(status="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence;->status:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", activities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence;->activities:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", clientStatuses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence;->clientStatuses:Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence;->userId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/domain/ModelPresence;->guildId:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
