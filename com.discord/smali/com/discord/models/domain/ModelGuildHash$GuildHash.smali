.class public final Lcom/discord/models/domain/ModelGuildHash$GuildHash;
.super Ljava/lang/Object;
.source "ModelGuildHash.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelGuildHash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GuildHash"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelGuildHash$GuildHash$Parser;
    }
.end annotation


# instance fields
.field private final hash:Ljava/lang/String;

.field private final omitted:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "hash"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelGuildHash$GuildHash;Ljava/lang/String;ZILjava/lang/Object;)Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->copy(Ljava/lang/String;Z)Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Z)Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    const-string v0, "hash"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-direct {v0, p1, p2}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    iget-boolean p1, p1, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    return-object v0
.end method

.method public final getOmitted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GuildHash(hash="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hash:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", omitted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->omitted:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
