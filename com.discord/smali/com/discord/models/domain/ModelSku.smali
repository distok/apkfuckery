.class public final Lcom/discord/models/domain/ModelSku;
.super Ljava/lang/Object;
.source "ModelSku.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelSku$Parser;,
        Lcom/discord/models/domain/ModelSku$SkuCategory;,
        Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;,
        Lcom/discord/models/domain/ModelSku$Price;,
        Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;,
        Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;
    }
.end annotation


# instance fields
.field private final application:Lcom/discord/models/domain/ModelApplication;

.field private final applicationId:J

.field private final externalSkuStrategies:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private final flags:I

.field private final id:J

.field private final name:Ljava/lang/String;

.field private final premium:Z

.field private final price:Lcom/discord/models/domain/ModelSku$Price;

.field private final type:I


# direct methods
.method public constructor <init>(JLjava/lang/String;JLcom/discord/models/domain/ModelApplication;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "J",
            "Lcom/discord/models/domain/ModelApplication;",
            "ZI",
            "Lcom/discord/models/domain/ModelSku$Price;",
            "I",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;",
            ">;)V"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    iput-object p3, p0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    iput-wide p4, p0, Lcom/discord/models/domain/ModelSku;->id:J

    iput-object p6, p0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    iput-boolean p7, p0, Lcom/discord/models/domain/ModelSku;->premium:Z

    iput p8, p0, Lcom/discord/models/domain/ModelSku;->type:I

    iput-object p9, p0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    iput p10, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    iput-object p11, p0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelSku;JLjava/lang/String;JLcom/discord/models/domain/ModelApplication;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;ILjava/lang/Object;)Lcom/discord/models/domain/ModelSku;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    goto :goto_0

    :cond_0
    move-wide v2, p1

    :goto_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v4, p3

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/discord/models/domain/ModelSku;->id:J

    goto :goto_2

    :cond_2
    move-wide/from16 v5, p4

    :goto_2
    and-int/lit8 v7, v1, 0x8

    if-eqz v7, :cond_3

    iget-object v7, v0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    goto :goto_3

    :cond_3
    move-object/from16 v7, p6

    :goto_3
    and-int/lit8 v8, v1, 0x10

    if-eqz v8, :cond_4

    iget-boolean v8, v0, Lcom/discord/models/domain/ModelSku;->premium:Z

    goto :goto_4

    :cond_4
    move/from16 v8, p7

    :goto_4
    and-int/lit8 v9, v1, 0x20

    if-eqz v9, :cond_5

    iget v9, v0, Lcom/discord/models/domain/ModelSku;->type:I

    goto :goto_5

    :cond_5
    move/from16 v9, p8

    :goto_5
    and-int/lit8 v10, v1, 0x40

    if-eqz v10, :cond_6

    iget-object v10, v0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    goto :goto_6

    :cond_6
    move-object/from16 v10, p9

    :goto_6
    and-int/lit16 v11, v1, 0x80

    if-eqz v11, :cond_7

    iget v11, v0, Lcom/discord/models/domain/ModelSku;->flags:I

    goto :goto_7

    :cond_7
    move/from16 v11, p10

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p11

    :goto_8
    move-wide p1, v2

    move-object p3, v4

    move-wide/from16 p4, v5

    move-object/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move-object/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/discord/models/domain/ModelSku;->copy(JLjava/lang/String;JLcom/discord/models/domain/ModelApplication;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;)Lcom/discord/models/domain/ModelSku;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->id:J

    return-wide v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelSku;->premium:Z

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->type:I

    return v0
.end method

.method public final component7()Lcom/discord/models/domain/ModelSku$Price;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    return-object v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    return v0
.end method

.method public final component9()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;JLcom/discord/models/domain/ModelApplication;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;)Lcom/discord/models/domain/ModelSku;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "J",
            "Lcom/discord/models/domain/ModelApplication;",
            "ZI",
            "Lcom/discord/models/domain/ModelSku$Price;",
            "I",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;",
            ">;)",
            "Lcom/discord/models/domain/ModelSku;"
        }
    .end annotation

    const-string v0, "name"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/domain/ModelSku;

    move-object v1, v0

    move-wide v2, p1

    move-wide/from16 v5, p4

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v1 .. v12}, Lcom/discord/models/domain/ModelSku;-><init>(JLjava/lang/String;JLcom/discord/models/domain/ModelApplication;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelSku;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelSku;

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelSku;->applicationId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->id:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelSku;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    iget-object v1, p1, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelSku;->premium:Z

    iget-boolean v1, p1, Lcom/discord/models/domain/ModelSku;->premium:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->type:I

    iget v1, p1, Lcom/discord/models/domain/ModelSku;->type:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    iget-object v1, p1, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    iget v1, p1, Lcom/discord/models/domain/ModelSku;->flags:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    iget-object p1, p1, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplication()Lcom/discord/models/domain/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    return-object v0
.end method

.method public final getApplicationId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    return-wide v0
.end method

.method public final getExternalSkuStrategies()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    return-object v0
.end method

.method public final getFlags()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    return v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->id:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getPremium()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelSku;->premium:Z

    return v0
.end method

.method public final getPrice()Lcom/discord/models/domain/ModelSku$Price;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    return-object v0
.end method

.method public final getSkuCategory()Lcom/discord/models/domain/ModelSku$SkuCategory;
    .locals 5

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->id:J

    const-wide v2, 0x73df901f4840001L

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    sget-object v0, Lcom/discord/models/domain/ModelSku$SkuCategory;->NITRO_CLASSIC:Lcom/discord/models/domain/ModelSku$SkuCategory;

    goto :goto_0

    :cond_0
    const-wide v2, 0x73df94b70420027L

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    sget-object v0, Lcom/discord/models/domain/ModelSku$SkuCategory;->NITRO:Lcom/discord/models/domain/ModelSku$SkuCategory;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/models/domain/ModelSku$SkuCategory;->GAME:Lcom/discord/models/domain/ModelSku$SkuCategory;

    :goto_0
    return-object v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v4, p0, Lcom/discord/models/domain/ModelSku;->id:J

    ushr-long v6, v4, v2

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplication;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelSku;->premium:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :cond_2
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->type:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSku$Price;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_4
    add-int/2addr v1, v3

    return v1
.end method

.method public final isAvailable()Z
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isStickerPack()Z
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    and-int/lit8 v0, v0, 0x10

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelSku(applicationId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/domain/ModelSku;->applicationId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelSku;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/domain/ModelSku;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelSku;->application:Lcom/discord/models/domain/ModelApplication;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", premium="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/models/domain/ModelSku;->premium:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/ModelSku;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelSku;->price:Lcom/discord/models/domain/ModelSku$Price;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/ModelSku;->flags:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", externalSkuStrategies="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelSku;->externalSkuStrategies:Ljava/util/Map;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->B(Ljava/lang/StringBuilder;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
