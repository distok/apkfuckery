.class public final Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;
.super Ljava/lang/Object;
.source "ModelPresence.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;->deserialize(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelPresence$ClientStatuses;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $desktopStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $mobileStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $reader:Lcom/discord/models/domain/Model$JsonReader;

.field public final synthetic $webStatus:Lkotlin/jvm/internal/Ref$ObjectRef;


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$desktopStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p2, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    iput-object p3, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$webStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p4, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$mobileStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x3fb56f5e

    const-string v2, ""

    if-eq v0, v1, :cond_3

    const v1, 0x1cb54

    if-eq v0, v1, :cond_2

    const v1, 0x5ccf901c

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "desktop"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$desktopStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

    sget-object v0, Lcom/discord/models/domain/ModelPresence$Status;->Companion:Lcom/discord/models/domain/ModelPresence$Status$Companion;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelPresence$Status$Companion;->deserialize(Ljava/lang/String;)Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v0

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "web"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$webStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

    sget-object v0, Lcom/discord/models/domain/ModelPresence$Status;->Companion:Lcom/discord/models/domain/ModelPresence$Status$Companion;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelPresence$Status$Companion;->deserialize(Ljava/lang/String;)Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v0

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_0

    :cond_3
    const-string v0, "mobile"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$mobileStatus:Lkotlin/jvm/internal/Ref$ObjectRef;

    sget-object v0, Lcom/discord/models/domain/ModelPresence$Status;->Companion:Lcom/discord/models/domain/ModelPresence$Status$Companion;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelPresence$Status$Companion;->deserialize(Ljava/lang/String;)Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v0

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    :cond_4
    :goto_0
    return-void
.end method
