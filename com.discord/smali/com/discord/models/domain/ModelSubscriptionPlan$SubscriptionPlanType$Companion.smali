.class public final Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;
.super Ljava/lang/Object;
.source "ModelSubscriptionPlan.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final from(J)Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;
    .locals 6

    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->values()[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_2

    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v3

    cmp-long v5, v3, p1

    if-nez v5, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_3

    return-object v2

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "unsupported type plan id: "

    invoke-static {v1, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getLEGACY_PLANS()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->access$getLEGACY_PLANS$cp()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getPREMIUM_GUILD_PLANS()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->access$getPREMIUM_GUILD_PLANS$cp()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getPREMIUM_PLANS()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->access$getPREMIUM_PLANS$cp()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
