.class public synthetic Lcom/discord/models/domain/ModelMessageEmbed$1;
.super Ljava/lang/Object;
.source "ModelMessageEmbed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelMessageEmbed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1009
    name = null
.end annotation


# static fields
.field public static final synthetic $SwitchMap$com$discord$models$domain$ModelMessageAttachment$Type:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/discord/models/domain/ModelMessageAttachment$Type;->values()[Lcom/discord/models/domain/ModelMessageAttachment$Type;

    const/4 v0, 0x3

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/models/domain/ModelMessageEmbed$1;->$SwitchMap$com$discord$models$domain$ModelMessageAttachment$Type:[I

    const/4 v2, 0x1

    const/4 v3, 0x2

    :try_start_0
    sget-object v4, Lcom/discord/models/domain/ModelMessageAttachment$Type;->VIDEO:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    aput v2, v1, v3
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v1, Lcom/discord/models/domain/ModelMessageEmbed$1;->$SwitchMap$com$discord$models$domain$ModelMessageAttachment$Type:[I

    sget-object v4, Lcom/discord/models/domain/ModelMessageAttachment$Type;->IMAGE:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    const/4 v4, 0x0

    aput v3, v1, v4
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v1, Lcom/discord/models/domain/ModelMessageEmbed$1;->$SwitchMap$com$discord$models$domain$ModelMessageAttachment$Type:[I

    sget-object v3, Lcom/discord/models/domain/ModelMessageAttachment$Type;->FILE:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    aput v0, v1, v2
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
