.class public final Lcom/discord/models/domain/ModelUserAffinity;
.super Ljava/lang/Object;
.source "ModelUserAffinity.kt"


# instance fields
.field private final affinity:F

.field private final userId:J


# direct methods
.method public constructor <init>(JF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    iput p3, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelUserAffinity;JFILjava/lang/Object;)Lcom/discord/models/domain/ModelUserAffinity;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-wide p1, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget p3, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/models/domain/ModelUserAffinity;->copy(JF)Lcom/discord/models/domain/ModelUserAffinity;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    return-wide v0
.end method

.method public final component2()F
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    return v0
.end method

.method public final copy(JF)Lcom/discord/models/domain/ModelUserAffinity;
    .locals 1

    new-instance v0, Lcom/discord/models/domain/ModelUserAffinity;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/models/domain/ModelUserAffinity;-><init>(JF)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelUserAffinity;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelUserAffinity;

    iget-wide v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    iget p1, p1, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAffinity()F
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    return v0
.end method

.method public final getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelUserAffinity(userId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/domain/ModelUserAffinity;->userId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", affinity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/ModelUserAffinity;->affinity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
