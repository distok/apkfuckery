.class public final Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;
.super Ljava/lang/Object;
.source "ModelSku.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSku;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ModelPremiumSkuPrice"
.end annotation


# instance fields
.field private final amount:I

.field private final percentage:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    iput p2, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;IIILjava/lang/Object;)Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->copy(II)Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    return v0
.end method

.method public final copy(II)Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;
    .locals 1

    new-instance v0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;

    invoke-direct {v0, p1, p2}, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    iget v1, p1, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    iget p1, p1, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    return v0
.end method

.method public final getPercentage()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelPremiumSkuPrice(amount="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->amount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;->percentage:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
