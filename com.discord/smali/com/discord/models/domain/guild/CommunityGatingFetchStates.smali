.class public final enum Lcom/discord/models/domain/guild/CommunityGatingFetchStates;
.super Ljava/lang/Enum;
.source "CommunityGatingFetchStates.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/domain/guild/CommunityGatingFetchStates;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

.field public static final enum FAILED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

.field public static final enum FETCHING:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

.field public static final enum SUCCEEDED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    new-instance v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const-string v2, "FETCHING"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->FETCHING:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const-string v2, "FAILED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->FAILED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const-string v2, "SUCCEEDED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->SUCCEEDED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->$VALUES:[Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/domain/guild/CommunityGatingFetchStates;
    .locals 1

    const-class v0, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/domain/guild/CommunityGatingFetchStates;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->$VALUES:[Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    invoke-virtual {v0}, [Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    return-object v0
.end method
