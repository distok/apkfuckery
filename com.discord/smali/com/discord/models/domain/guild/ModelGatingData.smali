.class public final Lcom/discord/models/domain/guild/ModelGatingData;
.super Ljava/lang/Object;
.source "ModelGatingData.kt"


# instance fields
.field private final data:Lcom/discord/models/domain/ModelMemberVerificationForm;

.field private final fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)V
    .locals 1

    const-string v0, "fetchState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    iput-object p2, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/guild/ModelGatingData;Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;ILjava/lang/Object;)Lcom/discord/models/domain/guild/ModelGatingData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/models/domain/guild/ModelGatingData;->copy(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)Lcom/discord/models/domain/guild/ModelGatingData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/guild/CommunityGatingFetchStates;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelMemberVerificationForm;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)Lcom/discord/models/domain/guild/ModelGatingData;
    .locals 1

    const-string v0, "fetchState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/domain/guild/ModelGatingData;

    invoke-direct {v0, p1, p2}, Lcom/discord/models/domain/guild/ModelGatingData;-><init>(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/guild/ModelGatingData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/guild/ModelGatingData;

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    iget-object v1, p1, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    iget-object p1, p1, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getData()Lcom/discord/models/domain/ModelMemberVerificationForm;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    return-object v0
.end method

.method public final getFetchState()Lcom/discord/models/domain/guild/CommunityGatingFetchStates;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMemberVerificationForm;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ModelGatingData(fetchState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/guild/ModelGatingData;->fetchState:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/guild/ModelGatingData;->data:Lcom/discord/models/domain/ModelMemberVerificationForm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
