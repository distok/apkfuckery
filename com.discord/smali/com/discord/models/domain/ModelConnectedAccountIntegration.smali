.class public final Lcom/discord/models/domain/ModelConnectedAccountIntegration;
.super Ljava/lang/Object;
.source "ModelConnectedAccountIntegration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelConnectedAccountIntegration$Parser;,
        Lcom/discord/models/domain/ModelConnectedAccountIntegration$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/models/domain/ModelConnectedAccountIntegration$Companion;

.field public static final TWITCH_URL_PREFIX:Ljava/lang/String; = "twitch.tv/"

.field public static final TYPE_TWITCH:Ljava/lang/String; = "twitch"

.field public static final TYPE_YOUTUBE:Ljava/lang/String; = "youtube"

.field public static final YOUTUBE_URL_PREFIX:Ljava/lang/String; = "youtube.com/channel/"


# instance fields
.field private final account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

.field private final guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

.field private final id:J

.field private final type:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelConnectedAccountIntegration$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->Companion:Lcom/discord/models/domain/ModelConnectedAccountIntegration$Companion;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Lcom/discord/models/domain/ModelConnectedIntegrationAccount;Lcom/discord/models/domain/ModelConnectedIntegrationGuild;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    iput-object p3, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    iput-object p5, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelConnectedAccountIntegration;JLjava/lang/String;Lcom/discord/models/domain/ModelConnectedIntegrationAccount;Lcom/discord/models/domain/ModelConnectedIntegrationGuild;ILjava/lang/Object;)Lcom/discord/models/domain/ModelConnectedAccountIntegration;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget-object p3, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    :cond_1
    move-object v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget-object p4, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    :cond_2
    move-object v4, p4

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    iget-object p5, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    :cond_3
    move-object v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->copy(JLjava/lang/String;Lcom/discord/models/domain/ModelConnectedIntegrationAccount;Lcom/discord/models/domain/ModelConnectedIntegrationGuild;)Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelConnectedIntegrationAccount;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    return-object v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelConnectedIntegrationGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;Lcom/discord/models/domain/ModelConnectedIntegrationAccount;Lcom/discord/models/domain/ModelConnectedIntegrationGuild;)Lcom/discord/models/domain/ModelConnectedAccountIntegration;
    .locals 7

    new-instance v6, Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    move-object v0, v6

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/models/domain/ModelConnectedAccountIntegration;-><init>(JLjava/lang/String;Lcom/discord/models/domain/ModelConnectedIntegrationAccount;Lcom/discord/models/domain/ModelConnectedIntegrationGuild;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    iget-wide v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    iget-object v1, p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    iget-object p1, p1, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAccount()Lcom/discord/models/domain/ModelConnectedIntegrationAccount;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    const-string/jumbo v1, "twitch"

    invoke-static {v1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "twitch.tv/"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    const-string/jumbo v2, "youtube"

    invoke-static {v2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "youtube.com/channel/"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelConnectedIntegrationGuild;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_4
    :goto_0
    return-object v1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelConnectedIntegrationGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    return-wide v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelConnectedIntegrationAccount;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelConnectedIntegrationGuild;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelConnectedAccountIntegration(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->account:Lcom/discord/models/domain/ModelConnectedIntegrationAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->guild:Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
