.class public final Lcom/discord/models/domain/ModelGuildHash;
.super Ljava/lang/Object;
.source "ModelGuildHash.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelGuildHash$GuildHash;,
        Lcom/discord/models/domain/ModelGuildHash$Parser;
    }
.end annotation


# static fields
.field public static final Parser:Lcom/discord/models/domain/ModelGuildHash$Parser;

.field private static final SUPPORTED_VERSION:I = 0x1


# instance fields
.field private final channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

.field private final metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

.field private final rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelGuildHash$Parser;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelGuildHash$Parser;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelGuildHash;->Parser:Lcom/discord/models/domain/ModelGuildHash$Parser;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    iput-object p2, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    iput-object p3, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelGuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;ILjava/lang/Object;)Lcom/discord/models/domain/ModelGuildHash;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/models/domain/ModelGuildHash;->copy(Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;)Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildHash;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelGuildHash;->Parser:Lcom/discord/models/domain/ModelGuildHash$Parser;

    invoke-virtual {v0, p0}, Lcom/discord/models/domain/ModelGuildHash$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;)Lcom/discord/models/domain/ModelGuildHash;
    .locals 1

    new-instance v0, Lcom/discord/models/domain/ModelGuildHash;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/models/domain/ModelGuildHash;-><init>(Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;Lcom/discord/models/domain/ModelGuildHash$GuildHash;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelGuildHash;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelGuildHash;

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    iget-object v1, p1, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    iget-object p1, p1, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelsHash()Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-object v0
.end method

.method public final getMetadataHash()Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-object v0
.end method

.method public final getRolesHash()Lcom/discord/models/domain/ModelGuildHash$GuildHash;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final toArray()[Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->getHash()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->getHash()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->getHash()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ModelGuildHash(metadataHash="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildHash;->metadataHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelsHash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildHash;->channelsHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rolesHash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelGuildHash;->rolesHash:Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
