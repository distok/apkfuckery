.class public final enum Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
.super Ljava/lang/Enum;
.source "ModelSubscriptionPlan.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSubscriptionPlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PremiumTier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field public static final Companion:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;

.field public static final enum NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field public static final enum TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field public static final enum TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;


# instance fields
.field private final tierInt:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    new-instance v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const-string v2, "TIER_1"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const-string v2, "TIER_2"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->$VALUES:[Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    new-instance v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->Companion:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->tierInt:I

    return-void
.end method

.method public static final from(I)Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->Companion:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;->from(I)Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    const-class v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->$VALUES:[Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0}, [Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method


# virtual methods
.method public final getTierInt()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->tierInt:I

    return v0
.end method
