.class public final Lcom/discord/models/domain/NullableField;
.super Ljava/lang/Object;
.source "NullableField.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private hasBeenSet:Z

.field private value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/NullableField;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public final hasBeenSet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/NullableField;->hasBeenSet:Z

    return v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/models/domain/NullableField;->value:Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/models/domain/NullableField;->hasBeenSet:Z

    return-void
.end method
