.class public final Lcom/discord/models/domain/ModelPresence$primaryActivity$2;
.super Lx/m/c/k;
.source "ModelPresence.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/models/domain/activity/ModelActivity;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/models/domain/ModelPresence;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelPresence;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence$primaryActivity$2;->this$0:Lcom/discord/models/domain/ModelPresence;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 4

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$primaryActivity$2;->this$0:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivity;->isCustomStatus()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    move-object v1, v2

    :cond_1
    check-cast v1, Lcom/discord/models/domain/activity/ModelActivity;

    :cond_2
    return-object v1
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelPresence$primaryActivity$2;->invoke()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v0

    return-object v0
.end method
