.class public final Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;
.super Ljava/lang/Object;
.source "ModelSku.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSku;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExternalSkuStrategy"
.end annotation


# instance fields
.field private final type:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;IILjava/lang/Object;)Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->copy(I)Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    return v0
.end method

.method public final copy(I)Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;
    .locals 1

    new-instance v0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;

    invoke-direct {v0, p1}, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    iget p1, p1, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    return v0
.end method

.method public final getType()Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;
    .locals 2

    sget-object v0, Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;->Companion:Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes$Companion;

    iget v1, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes$Companion;->from(I)Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ExternalSkuStrategy(type="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;->type:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
