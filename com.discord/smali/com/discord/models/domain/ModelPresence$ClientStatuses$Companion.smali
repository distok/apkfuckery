.class public final Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;
.super Ljava/lang/Object;
.source "ModelPresence.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelPresence$ClientStatuses;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelPresence$ClientStatuses;
    .locals 4

    const-string v0, "reader"

    invoke-static {p1, v0}, Lf/e/c/a/a;->N(Lcom/discord/models/domain/Model$JsonReader;Ljava/lang/String;)Lkotlin/jvm/internal/Ref$ObjectRef;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/ModelPresence$Status;->OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v3, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v3}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v1, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v1, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;

    invoke-direct {v1, v2, p1, v3, v0}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion$deserialize$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance p1, Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    iget-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lcom/discord/models/domain/ModelPresence$Status;

    iget-object v2, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v2, Lcom/discord/models/domain/ModelPresence$Status;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Lcom/discord/models/domain/ModelPresence$Status;

    invoke-direct {p1, v1, v2, v0}, Lcom/discord/models/domain/ModelPresence$ClientStatuses;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;)V

    return-object p1
.end method

.method public final empty()Lcom/discord/models/domain/ModelPresence$ClientStatuses;
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    sget-object v1, Lcom/discord/models/domain/ModelPresence$Status;->OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-direct {v0, v1, v1, v1}, Lcom/discord/models/domain/ModelPresence$ClientStatuses;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;)V

    return-object v0
.end method
