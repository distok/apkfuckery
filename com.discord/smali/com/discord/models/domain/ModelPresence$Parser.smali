.class public final Lcom/discord/models/domain/ModelPresence$Parser;
.super Ljava/lang/Object;
.source "ModelPresence.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$Parser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelPresence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$Parser<",
        "Lcom/discord/models/domain/ModelPresence;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/domain/ModelPresence$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/domain/ModelPresence$Parser;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelPresence$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelPresence$Parser;->INSTANCE:Lcom/discord/models/domain/ModelPresence$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelPresence;
    .locals 16

    move-object/from16 v8, p1

    const-string v0, "reader"

    invoke-static {v8, v0}, Lf/e/c/a/a;->N(Lcom/discord/models/domain/Model$JsonReader;Ljava/lang/String;)Lkotlin/jvm/internal/Ref$ObjectRef;

    move-result-object v9

    const/4 v0, 0x0

    iput-object v0, v9, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v10, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v10}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v0, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v11, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v11}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v0, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v12, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v12}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v0, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v13, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v13}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v0, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v14, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v14}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, v14, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    new-instance v15, Lcom/discord/models/domain/ModelPresence$Parser$parse$1;

    move-object v0, v15

    move-object v1, v9

    move-object/from16 v2, p1

    move-object v3, v10

    move-object v4, v11

    move-object v5, v12

    move-object v6, v13

    move-object v7, v14

    invoke-direct/range {v0 .. v7}, Lcom/discord/models/domain/ModelPresence$Parser$parse$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$LongRef;)V

    invoke-virtual {v8, v15}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance v8, Lcom/discord/models/domain/ModelPresence;

    iget-object v0, v9, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, Lcom/discord/models/domain/ModelPresence$Status;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iget-object v0, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Ljava/util/List;

    iget-object v0, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->Companion:Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;->empty()Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    move-result-object v0

    :goto_0
    move-object v3, v0

    iget-object v0, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/discord/models/domain/ModelUser;

    iget-object v0, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Ljava/lang/Long;

    iget-wide v6, v14, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;J)V

    return-object v8
.end method

.method public bridge synthetic parse(Lcom/discord/models/domain/Model$JsonReader;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/models/domain/ModelPresence$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelPresence;

    move-result-object p1

    return-object p1
.end method
