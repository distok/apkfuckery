.class public Lcom/discord/models/domain/ModelMessage;
.super Ljava/lang/Object;
.source "ModelMessage.java"

# interfaces
.implements Lcom/discord/models/domain/Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelMessage$MessageReference;,
        Lcom/discord/models/domain/ModelMessage$Activity;,
        Lcom/discord/models/domain/ModelMessage$Call;,
        Lcom/discord/models/domain/ModelMessage$Content;
    }
.end annotation


# static fields
.field private static final EMPTY_ATTACHMENTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_EMBEDS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageEmbed;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_MENTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_REACTIONS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVERYONE:Ljava/lang/String; = "@everyone"

.field public static final FLAG_CROSSPOSTED:J = 0x1L

.field public static final FLAG_IS_CROSSPOST:J = 0x2L

.field public static final FLAG_SOURCE_MESSAGE_DELETED:J = 0x8L

.field public static final FLAG_SUPPRESS_EMBEDS:J = 0x4L

.field public static final FLAG_URGENT:J = 0x10L

.field public static final HERE:Ljava/lang/String; = "@here"

.field public static final TYPE_APPLICATION_COMMAND:I = 0x14

.field public static final TYPE_CALL:I = 0x3

.field public static final TYPE_CHANNEL_FOLLOW_ADD:I = 0xc

.field public static final TYPE_CHANNEL_ICON_CHANGE:I = 0x5

.field public static final TYPE_CHANNEL_NAME_CHANGE:I = 0x4

.field public static final TYPE_CHANNEL_PINNED_MESSAGE:I = 0x6

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_GUILD_DISCOVERY_DISQUALIFIED:I = 0xe

.field public static final TYPE_GUILD_DISCOVERY_GRACE_PERIOD_FINAL_WARNING:I = 0x11

.field public static final TYPE_GUILD_DISCOVERY_GRACE_PERIOD_INITIAL_WARNING:I = 0x10

.field public static final TYPE_GUILD_DISCOVERY_REQUALIFIED:I = 0xf

.field public static final TYPE_GUILD_STREAM:I = 0xd

.field public static final TYPE_LOCAL:I = -0x1

.field public static final TYPE_LOCAL_INVALID_ATTACHMENTS:I = -0x3

.field public static final TYPE_LOCAL_SEND_FAILED:I = -0x2

.field public static final TYPE_RECIPIENT_ADD:I = 0x1

.field public static final TYPE_RECIPIENT_REMOVE:I = 0x2

.field public static final TYPE_REPLY:I = 0x13

.field public static final TYPE_THREAD_CREATED:I = 0x12

.field public static final TYPE_USER_JOIN:I = 0x7

.field public static final TYPE_USER_PREMIUM_GUILD_SUBSCRIPTION:I = 0x8

.field public static final TYPE_USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_1:I = 0x9

.field public static final TYPE_USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_2:I = 0xa

.field public static final TYPE_USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_3:I = 0xb

.field private static final sortByIds:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private activity:Lcom/discord/models/domain/ModelMessage$Activity;

.field private allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

.field private application:Lcom/discord/models/domain/ModelApplication;

.field private attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private author:Lcom/discord/models/domain/ModelUser;

.field private call:Lcom/discord/models/domain/ModelMessage$Call;

.field private channelId:J

.field private content:Ljava/lang/String;

.field private editedTimestamp:Ljava/lang/String;

.field private transient editedTimestampMilliseconds:Ljava/lang/Long;

.field private embeds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageEmbed;",
            ">;"
        }
    .end annotation
.end field

.field private flags:Ljava/lang/Long;

.field private transient guildId:Ljava/lang/Long;

.field private transient hasLocalUploads:Z

.field private transient hit:Z

.field private id:J

.field private initialAttemptTimestamp:Ljava/lang/Long;

.field private lastManualAttemptTimestamp:Ljava/lang/Long;

.field public localAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/messages/LocalAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private transient member:Lcom/discord/models/domain/ModelGuildMember;

.field private mentionEveryone:Z

.field private mentionRoles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mentions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation
.end field

.field private messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

.field private nonce:Ljava/lang/String;

.field private numRetries:Ljava/lang/Integer;

.field private pinned:Ljava/lang/Boolean;

.field private reactions:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;"
        }
    .end annotation
.end field

.field private referencedMessage:Lcom/discord/models/domain/ModelMessage;

.field private transient stale:Z

.field private stickers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation
.end field

.field private timestamp:Ljava/lang/String;

.field private transient timestampMilliseconds:Ljava/lang/Long;

.field private tts:Z

.field private type:I

.field private webhookId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_MENTIONS:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_EMBEDS:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_ATTACHMENTS:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_REACTIONS:Ljava/util/Map;

    sget-object v0, Lf/a/d/a/p1;->d:Lf/a/d/a/p1;

    sput-object v0, Lcom/discord/models/domain/ModelMessage;->sortByIds:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/discord/models/domain/ModelUser;->EMPTY:Lcom/discord/models/domain/ModelUser;

    iput-object v0, p0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;JILjava/lang/String;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZLcom/discord/models/domain/ModelMessage$Call;ZLjava/util/LinkedHashMap;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;)V
    .locals 4
    .param p8    # Lcom/discord/models/domain/ModelUser;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageAttachment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageEmbed;",
            ">;Z",
            "Lcom/discord/models/domain/ModelMessage$Call;",
            "Z",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelApplication;",
            "Lcom/discord/models/domain/ModelMessage$Activity;",
            "Z",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;Z",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelMessage$MessageReference;",
            "Lcom/discord/models/domain/ModelAllowedMentions;",
            "Ljava/util/List<",
            "Lcom/discord/models/messages/LocalAttachment;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v2, Lcom/discord/models/domain/ModelUser;->EMPTY:Lcom/discord/models/domain/ModelUser;

    iput-object v2, v0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    const-string v2, "author is marked non-null but is null"

    invoke-static {p8, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-wide v2, p1

    iput-wide v2, v0, Lcom/discord/models/domain/ModelMessage;->id:J

    move-object v2, p3

    iput-object v2, v0, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    move-wide v2, p4

    iput-wide v2, v0, Lcom/discord/models/domain/ModelMessage;->channelId:J

    move v2, p6

    iput v2, v0, Lcom/discord/models/domain/ModelMessage;->type:I

    move-object v2, p7

    iput-object v2, v0, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    move-object v1, p9

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    move-object v1, p10

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->timestamp:Ljava/lang/String;

    move-object v1, p11

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->editedTimestamp:Ljava/lang/String;

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    move/from16 v1, p14

    iput-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->tts:Z

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    move-object/from16 v1, p17

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    move-object/from16 v1, p20

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    move-object/from16 v1, p21

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    move/from16 v1, p22

    iput-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->hit:Z

    move-object/from16 v1, p23

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->mentionRoles:Ljava/util/List;

    move/from16 v1, p24

    iput-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->hasLocalUploads:Z

    move-object/from16 v1, p25

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->flags:Ljava/lang/Long;

    move-object/from16 v1, p26

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-object/from16 v1, p27

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    move-object/from16 v1, p32

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->stickers:Ljava/util/List;

    move-object/from16 v1, p28

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    move-object/from16 v1, p29

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->lastManualAttemptTimestamp:Ljava/lang/Long;

    move-object/from16 v1, p30

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->initialAttemptTimestamp:Ljava/lang/Long;

    move-object/from16 v1, p31

    iput-object v1, v0, Lcom/discord/models/domain/ModelMessage;->numRetries:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelMessage;)V
    .locals 35
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iget-wide v2, v0, Lcom/discord/models/domain/ModelMessage;->id:J

    iget-object v4, v0, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    iget-wide v5, v0, Lcom/discord/models/domain/ModelMessage;->channelId:J

    iget v7, v0, Lcom/discord/models/domain/ModelMessage;->type:I

    iget-object v8, v0, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    iget-object v9, v0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    iget-object v10, v0, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    iget-object v11, v0, Lcom/discord/models/domain/ModelMessage;->timestamp:Ljava/lang/String;

    iget-object v12, v0, Lcom/discord/models/domain/ModelMessage;->editedTimestamp:Ljava/lang/String;

    iget-object v13, v0, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    iget-object v14, v0, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    iget-boolean v15, v0, Lcom/discord/models/domain/ModelMessage;->tts:Z

    move-object/from16 v34, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    move-object/from16 v16, v1

    iget-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    move/from16 v17, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    move-object/from16 v22, v1

    iget-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->hit:Z

    move/from16 v23, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->mentionRoles:Ljava/util/List;

    move-object/from16 v24, v1

    iget-boolean v1, v0, Lcom/discord/models/domain/ModelMessage;->hasLocalUploads:Z

    move/from16 v25, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->flags:Ljava/lang/Long;

    move-object/from16 v26, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-object/from16 v27, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    move-object/from16 v28, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    move-object/from16 v29, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->lastManualAttemptTimestamp:Ljava/lang/Long;

    move-object/from16 v30, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->initialAttemptTimestamp:Ljava/lang/Long;

    move-object/from16 v31, v1

    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->numRetries:Ljava/lang/Integer;

    move-object/from16 v32, v1

    iget-object v0, v0, Lcom/discord/models/domain/ModelMessage;->stickers:Ljava/util/List;

    move-object/from16 v33, v0

    move-object/from16 v1, v34

    invoke-direct/range {v1 .. v33}, Lcom/discord/models/domain/ModelMessage;-><init>(JLjava/lang/String;JILjava/lang/String;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZLcom/discord/models/domain/ModelMessage$Call;ZLjava/util/LinkedHashMap;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelMessage;)V
    .locals 39
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    iget-wide v2, v1, Lcom/discord/models/domain/ModelMessage;->id:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v2, v0, Lcom/discord/models/domain/ModelMessage;->id:J

    :goto_0
    move-wide v7, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    :goto_1
    move-object v9, v2

    iget-wide v2, v1, Lcom/discord/models/domain/ModelMessage;->channelId:J

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    goto :goto_2

    :cond_2
    iget-wide v2, v0, Lcom/discord/models/domain/ModelMessage;->channelId:J

    :goto_2
    move-wide v10, v2

    iget v2, v1, Lcom/discord/models/domain/ModelMessage;->type:I

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_3
    iget v2, v0, Lcom/discord/models/domain/ModelMessage;->type:I

    :goto_3
    move v12, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    if-eqz v2, :cond_4

    goto :goto_4

    :cond_4
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    :goto_4
    move-object v13, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    sget-object v3, Lcom/discord/models/domain/ModelUser;->EMPTY:Lcom/discord/models/domain/ModelUser;

    if-eq v2, v3, :cond_5

    goto :goto_5

    :cond_5
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    :goto_5
    move-object v14, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    if-eqz v2, :cond_6

    goto :goto_6

    :cond_6
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    :goto_6
    move-object v15, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->timestamp:Ljava/lang/String;

    if-eqz v2, :cond_7

    goto :goto_7

    :cond_7
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->timestamp:Ljava/lang/String;

    :goto_7
    move-object/from16 v16, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->editedTimestamp:Ljava/lang/String;

    if-eqz v2, :cond_8

    goto :goto_8

    :cond_8
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->editedTimestamp:Ljava/lang/String;

    :goto_8
    move-object/from16 v17, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    if-eqz v2, :cond_9

    goto :goto_9

    :cond_9
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    :goto_9
    move-object/from16 v18, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    if-eqz v2, :cond_a

    goto :goto_a

    :cond_a
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    :goto_a
    move-object/from16 v19, v2

    iget-boolean v2, v1, Lcom/discord/models/domain/ModelMessage;->tts:Z

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_c

    iget-boolean v2, v0, Lcom/discord/models/domain/ModelMessage;->tts:Z

    if-eqz v2, :cond_b

    goto :goto_b

    :cond_b
    const/16 v20, 0x0

    goto :goto_c

    :cond_c
    :goto_b
    const/16 v20, 0x1

    :goto_c
    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    if-eqz v2, :cond_d

    goto :goto_d

    :cond_d
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    :goto_d
    move-object/from16 v21, v2

    iget-boolean v2, v1, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    if-nez v2, :cond_f

    iget-boolean v2, v0, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    if-eqz v2, :cond_e

    goto :goto_e

    :cond_e
    const/16 v22, 0x0

    goto :goto_f

    :cond_f
    :goto_e
    const/16 v22, 0x1

    :goto_f
    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_10

    goto :goto_10

    :cond_10
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    :goto_10
    move-object/from16 v23, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    goto :goto_11

    :cond_11
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    :goto_11
    move-object/from16 v24, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    if-eqz v2, :cond_12

    goto :goto_12

    :cond_12
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    :goto_12
    move-object/from16 v25, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    if-eqz v2, :cond_13

    goto :goto_13

    :cond_13
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    :goto_13
    move-object/from16 v26, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    if-eqz v2, :cond_14

    goto :goto_14

    :cond_14
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    :goto_14
    move-object/from16 v27, v2

    iget-boolean v2, v1, Lcom/discord/models/domain/ModelMessage;->hit:Z

    if-nez v2, :cond_16

    iget-boolean v2, v0, Lcom/discord/models/domain/ModelMessage;->hit:Z

    if-eqz v2, :cond_15

    goto :goto_15

    :cond_15
    const/16 v28, 0x0

    goto :goto_16

    :cond_16
    :goto_15
    const/16 v28, 0x1

    :goto_16
    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->mentionRoles:Ljava/util/List;

    if-eqz v2, :cond_17

    goto :goto_17

    :cond_17
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->mentionRoles:Ljava/util/List;

    :goto_17
    move-object/from16 v29, v2

    iget-boolean v2, v1, Lcom/discord/models/domain/ModelMessage;->hasLocalUploads:Z

    if-nez v2, :cond_19

    iget-boolean v2, v0, Lcom/discord/models/domain/ModelMessage;->hasLocalUploads:Z

    if-eqz v2, :cond_18

    goto :goto_18

    :cond_18
    const/16 v30, 0x0

    goto :goto_19

    :cond_19
    :goto_18
    const/16 v30, 0x1

    :goto_19
    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->flags:Ljava/lang/Long;

    if-eqz v2, :cond_1a

    goto :goto_1a

    :cond_1a
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->flags:Ljava/lang/Long;

    :goto_1a
    move-object/from16 v31, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    if-eqz v2, :cond_1b

    goto :goto_1b

    :cond_1b
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    :goto_1b
    move-object/from16 v32, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    if-eqz v2, :cond_1c

    goto :goto_1c

    :cond_1c
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    :goto_1c
    move-object/from16 v33, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    if-eqz v2, :cond_1d

    goto :goto_1d

    :cond_1d
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    :goto_1d
    move-object/from16 v34, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->lastManualAttemptTimestamp:Ljava/lang/Long;

    if-eqz v2, :cond_1e

    goto :goto_1e

    :cond_1e
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->lastManualAttemptTimestamp:Ljava/lang/Long;

    :goto_1e
    move-object/from16 v35, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->initialAttemptTimestamp:Ljava/lang/Long;

    if-eqz v2, :cond_1f

    goto :goto_1f

    :cond_1f
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->initialAttemptTimestamp:Ljava/lang/Long;

    :goto_1f
    move-object/from16 v36, v2

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->numRetries:Ljava/lang/Integer;

    if-eqz v2, :cond_20

    goto :goto_20

    :cond_20
    iget-object v2, v0, Lcom/discord/models/domain/ModelMessage;->numRetries:Ljava/lang/Integer;

    :goto_20
    move-object/from16 v37, v2

    iget-object v1, v1, Lcom/discord/models/domain/ModelMessage;->stickers:Ljava/util/List;

    if-eqz v1, :cond_21

    goto :goto_21

    :cond_21
    iget-object v1, v0, Lcom/discord/models/domain/ModelMessage;->stickers:Ljava/util/List;

    :goto_21
    move-object/from16 v38, v1

    move-object/from16 v6, p0

    invoke-direct/range {v6 .. v38}, Lcom/discord/models/domain/ModelMessage;-><init>(JLjava/lang/String;JILjava/lang/String;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZLcom/discord/models/domain/ModelMessage$Call;ZLjava/util/LinkedHashMap;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;)V

    const-string v1, "other is marked non-null but is null"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method public static compare(Ljava/lang/Long;Ljava/lang/Long;)I
    .locals 5

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    if-nez p0, :cond_0

    move-object p0, v0

    :cond_0
    if-nez p1, :cond_1

    move-object p1, v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    const/4 p0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    cmp-long v2, v0, p0

    if-gez v2, :cond_3

    const/4 p0, -0x1

    goto :goto_0

    :cond_3
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method public static createInvalidAttachmentsMessage(JLcom/discord/models/domain/ModelUser;Lcom/discord/utilities/time/Clock;Ljava/util/List;)Lcom/discord/models/domain/ModelMessage;
    .locals 34
    .param p2    # Lcom/discord/models/domain/ModelUser;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/utilities/time/Clock;",
            "Ljava/util/List<",
            "Lcom/discord/models/messages/LocalAttachment;",
            ">;)",
            "Lcom/discord/models/domain/ModelMessage;"
        }
    .end annotation

    const-string v0, "author is marked non-null but is null"

    move-object/from16 v9, p2

    invoke-static {v9, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Lcom/discord/models/domain/NonceGenerator;->computeNonce()J

    move-result-wide v4

    move-wide v2, v4

    invoke-static/range {p3 .. p3}, Lcom/discord/utilities/time/TimeUtils;->currentTimeUTCDateString(Lcom/discord/utilities/time/Clock;)Ljava/lang/String;

    move-result-object v11

    new-instance v0, Lcom/discord/models/domain/ModelMessage;

    move-object v1, v0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const/4 v7, -0x3

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x1

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-wide/from16 v5, p0

    move-object/from16 v9, p2

    move-object/from16 v29, p4

    invoke-direct/range {v1 .. v33}, Lcom/discord/models/domain/ModelMessage;-><init>(JLjava/lang/String;JILjava/lang/String;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZLcom/discord/models/domain/ModelMessage$Call;ZLjava/util/LinkedHashMap;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;)V

    return-object v0
.end method

.method public static createLocalMessage(Ljava/lang/String;JLcom/discord/models/domain/ModelUser;Ljava/util/List;ZZLcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/utilities/time/Clock;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;)Lcom/discord/models/domain/ModelMessage;
    .locals 34
    .param p3    # Lcom/discord/models/domain/ModelUser;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;ZZ",
            "Lcom/discord/models/domain/ModelApplication;",
            "Lcom/discord/models/domain/ModelMessage$Activity;",
            "Lcom/discord/utilities/time/Clock;",
            "Ljava/util/List<",
            "Lcom/discord/models/messages/LocalAttachment;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Lcom/discord/models/domain/ModelMessage$MessageReference;",
            "Lcom/discord/models/domain/ModelAllowedMentions;",
            ")",
            "Lcom/discord/models/domain/ModelMessage;"
        }
    .end annotation

    const-string v0, "author is marked non-null but is null"

    move-object/from16 v9, p3

    invoke-static {v9, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Lcom/discord/models/domain/NonceGenerator;->computeNonce()J

    move-result-wide v2

    if-eqz p5, :cond_0

    const/4 v0, -0x2

    const/4 v7, -0x2

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    const/4 v7, -0x1

    :goto_0
    invoke-static/range {p9 .. p9}, Lcom/discord/utilities/time/TimeUtils;->currentTimeUTCDateString(Lcom/discord/utilities/time/Clock;)Ljava/lang/String;

    move-result-object v11

    new-instance v0, Lcom/discord/models/domain/ModelMessage;

    move-object v1, v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v26, 0x0

    move-wide/from16 v5, p1

    move-object/from16 v8, p0

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v21, p7

    move-object/from16 v22, p8

    move/from16 v25, p6

    move-object/from16 v27, p15

    move-object/from16 v28, p16

    move-object/from16 v29, p10

    move-object/from16 v30, p11

    move-object/from16 v31, p12

    move-object/from16 v32, p13

    move-object/from16 v33, p14

    invoke-direct/range {v1 .. v33}, Lcom/discord/models/domain/ModelMessage;-><init>(JLjava/lang/String;JILjava/lang/String;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZLcom/discord/models/domain/ModelMessage$Call;ZLjava/util/LinkedHashMap;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;ZLjava/util/List;ZLjava/lang/Long;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;)V

    return-object v0
.end method

.method public static createWithReactions(Lcom/discord/models/domain/ModelMessage;Ljava/util/LinkedHashMap;)Lcom/discord/models/domain/ModelMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelMessage;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;)",
            "Lcom/discord/models/domain/ModelMessage;"
        }
    .end annotation

    new-instance v0, Lcom/discord/models/domain/ModelMessage;

    invoke-direct {v0, p0}, Lcom/discord/models/domain/ModelMessage;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    iput-object p1, v0, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public static getSortByIds()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/models/domain/ModelMessage;->sortByIds:Ljava/util/Comparator;

    return-object v0
.end method

.method public static isNewer(Ljava/lang/Long;Ljava/lang/Long;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/discord/models/domain/ModelMessage;->compare(Ljava/lang/Long;Ljava/lang/Long;)I

    move-result p0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private isOn(J)Z
    .locals 3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getFlags()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getFlags()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    and-long/2addr v0, p1

    cmp-long v2, v0, p1

    if-nez v2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method


# virtual methods
.method public assignField(Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v1, "mention_everyone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v3, 0x1a

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "application"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v3, 0x19

    goto/16 :goto_0

    :sswitch_2
    const-string/jumbo v1, "stickers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v3, 0x18

    goto/16 :goto_0

    :sswitch_3
    const-string v1, "referenced_message"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v3, 0x17

    goto/16 :goto_0

    :sswitch_4
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v3, 0x16

    goto/16 :goto_0

    :sswitch_5
    const-string/jumbo v1, "webhook_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v3, 0x15

    goto/16 :goto_0

    :sswitch_6
    const-string v1, "nonce"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v3, 0x14

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "flags"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v3, 0x13

    goto/16 :goto_0

    :sswitch_8
    const-string/jumbo v1, "timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v3, 0x12

    goto/16 :goto_0

    :sswitch_9
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    goto/16 :goto_0

    :cond_9
    const/16 v3, 0x11

    goto/16 :goto_0

    :sswitch_a
    const-string v1, "call"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    goto/16 :goto_0

    :cond_a
    const/16 v3, 0x10

    goto/16 :goto_0

    :sswitch_b
    const-string/jumbo v1, "tts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v3, 0xf

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "hit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    goto/16 :goto_0

    :cond_c
    const/16 v3, 0xe

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    goto/16 :goto_0

    :cond_d
    const/16 v3, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "mentions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    goto/16 :goto_0

    :cond_e
    const/16 v3, 0xc

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "attachments"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    goto/16 :goto_0

    :cond_f
    const/16 v3, 0xb

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "message_reference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    goto/16 :goto_0

    :cond_10
    const/16 v3, 0xa

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "pinned"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    goto/16 :goto_0

    :cond_11
    const/16 v3, 0x9

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "member"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    goto/16 :goto_0

    :cond_12
    const/16 v3, 0x8

    goto/16 :goto_0

    :sswitch_13
    const-string v1, "reactions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    goto :goto_0

    :cond_13
    const/4 v3, 0x7

    goto :goto_0

    :sswitch_14
    const-string v1, "embeds"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    goto :goto_0

    :cond_14
    const/4 v3, 0x6

    goto :goto_0

    :sswitch_15
    const-string v1, "guild_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    goto :goto_0

    :cond_15
    const/4 v3, 0x5

    goto :goto_0

    :sswitch_16
    const-string v1, "edited_timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    goto :goto_0

    :cond_16
    const/4 v3, 0x4

    goto :goto_0

    :sswitch_17
    const-string v1, "author"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    goto :goto_0

    :cond_17
    const/4 v3, 0x3

    goto :goto_0

    :sswitch_18
    const-string v1, "activity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    goto :goto_0

    :cond_18
    const/4 v3, 0x2

    goto :goto_0

    :sswitch_19
    const-string v1, "channel_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    goto :goto_0

    :cond_19
    const/4 v3, 0x1

    goto :goto_0

    :sswitch_1a
    const-string v1, "mention_roles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    goto :goto_0

    :cond_1a
    const/4 v3, 0x0

    :goto_0
    const/4 v0, 0x0

    packed-switch v3, :pswitch_data_0

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    goto/16 :goto_1

    :pswitch_0
    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextBoolean(Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    goto/16 :goto_1

    :pswitch_1
    sget-object v0, Lcom/discord/models/domain/ModelApplication$Parser;->INSTANCE:Lcom/discord/models/domain/ModelApplication$Parser;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelApplication$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelApplication;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    goto/16 :goto_1

    :pswitch_2
    new-instance v0, Lf/a/d/a/j0;

    invoke-direct {v0, p1}, Lf/a/d/a/j0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->stickers:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_3
    new-instance v0, Lcom/discord/models/domain/ModelMessage;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessage;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMessage;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->referencedMessage:Lcom/discord/models/domain/ModelMessage;

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_7
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->flags:Ljava/lang/Long;

    goto/16 :goto_1

    :pswitch_8
    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->timestamp:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_9
    iget v0, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    iput p1, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    goto/16 :goto_1

    :pswitch_a
    new-instance v0, Lcom/discord/models/domain/ModelMessage$Call;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessage$Call;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMessage$Call;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    goto/16 :goto_1

    :pswitch_b
    invoke-virtual {p1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextBoolean(Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/models/domain/ModelMessage;->tts:Z

    goto/16 :goto_1

    :pswitch_c
    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->hit:Z

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextBoolean(Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/models/domain/ModelMessage;->hit:Z

    goto/16 :goto_1

    :pswitch_d
    iget-wide v0, p0, Lcom/discord/models/domain/ModelMessage;->id:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/models/domain/ModelMessage;->id:J

    goto/16 :goto_1

    :pswitch_e
    new-instance v0, Lf/a/d/a/m0;

    invoke-direct {v0, p1}, Lf/a/d/a/m0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_f
    new-instance v0, Lf/a/d/a/i0;

    invoke-direct {v0, p1}, Lf/a/d/a/i0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    goto/16 :goto_1

    :pswitch_10
    new-instance v0, Lcom/discord/models/domain/ModelMessage$MessageReference;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessage$MessageReference;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMessage$MessageReference;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    goto/16 :goto_1

    :pswitch_11
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextBooleanOrNull()Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    goto :goto_1

    :pswitch_12
    new-instance v0, Lcom/discord/models/domain/ModelGuildMember;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelGuildMember;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildMember;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->member:Lcom/discord/models/domain/ModelGuildMember;

    goto :goto_1

    :pswitch_13
    new-instance v0, Lf/a/d/a/g0;

    invoke-direct {v0, p1}, Lf/a/d/a/g0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    sget-object v1, Lf/a/d/a/l0;->a:Lf/a/d/a/l0;

    sget-object v2, Lf/a/d/a/o1;->d:Lf/a/d/a/o1;

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextListAsMap(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;Lcom/discord/models/domain/Model$JsonReader$KeySelector;Lrx/functions/Func0;)Ljava/util/Map;

    move-result-object p1

    check-cast p1, Ljava/util/LinkedHashMap;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    goto :goto_1

    :pswitch_14
    new-instance v0, Lf/a/d/a/k0;

    invoke-direct {v0, p1}, Lf/a/d/a/k0;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    goto :goto_1

    :pswitch_15
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->guildId:Ljava/lang/Long;

    goto :goto_1

    :pswitch_16
    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->editedTimestamp:Ljava/lang/String;

    goto :goto_1

    :pswitch_17
    invoke-static {p1}, Lf/e/c/a/a;->a0(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    goto :goto_1

    :pswitch_18
    new-instance v0, Lcom/discord/models/domain/ModelMessage$Activity;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessage$Activity;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMessage$Activity;

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    goto :goto_1

    :pswitch_19
    iget-wide v0, p0, Lcom/discord/models/domain/ModelMessage;->channelId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/models/domain/ModelMessage;->channelId:J

    goto :goto_1

    :pswitch_1a
    new-instance v0, Lf/a/d/a/a;

    invoke-direct {v0, p1}, Lf/a/d/a/a;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelMessage;->mentionRoles:Ljava/util/List;

    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7f5b87d8 -> :sswitch_1a
        -0x7315ce29 -> :sswitch_19
        -0x62b40cf1 -> :sswitch_18
        -0x53d2de75 -> :sswitch_17
        -0x53711b80 -> :sswitch_16
        -0x4de03319 -> :sswitch_15
        -0x4d787346 -> :sswitch_14
        -0x42ef9496 -> :sswitch_13
        -0x403d7566 -> :sswitch_12
        -0x3ae5ec28 -> :sswitch_11
        -0x2e77e2ed -> :sswitch_10
        -0x2c0c3450 -> :sswitch_f
        -0x24016037 -> :sswitch_e
        0xd1b -> :sswitch_d
        0x19393 -> :sswitch_c
        0x1c1f3 -> :sswitch_b
        0x2e7a5e -> :sswitch_a
        0x368f3a -> :sswitch_9
        0x3492916 -> :sswitch_8
        0x5cfee87 -> :sswitch_7
        0x64237ef -> :sswitch_6
        0x1296d3e3 -> :sswitch_5
        0x38b73479 -> :sswitch_4
        0x5215c841 -> :sswitch_3
        0x5b4c1ed6 -> :sswitch_2
        0x5ca40550 -> :sswitch_1
        0x6420bc20 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/models/domain/ModelMessage;

    return p1
.end method

.method public canResend()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/models/domain/ModelMessage;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {p1, p0}, Lcom/discord/models/domain/ModelMessage;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_3

    return v2

    :cond_3
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_4

    if-eqz v3, :cond_5

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    return v2

    :cond_5
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_6

    return v2

    :cond_6
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v3

    if-eq v1, v3, :cond_7

    return v2

    :cond_7
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_8

    if-eqz v3, :cond_9

    goto :goto_1

    :cond_8
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    :goto_1
    return v2

    :cond_9
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    if-nez v1, :cond_a

    if-eqz v3, :cond_b

    goto :goto_2

    :cond_a
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelUser;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    :goto_2
    return v2

    :cond_b
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_c

    if-eqz v3, :cond_d

    goto :goto_3

    :cond_c
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    :goto_3
    return v2

    :cond_d
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_e

    if-eqz v3, :cond_f

    goto :goto_4

    :cond_e
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    :goto_4
    return v2

    :cond_f
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_10

    return v2

    :cond_10
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEditedTimestamp()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getEditedTimestamp()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_11

    return v2

    :cond_11
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAttachments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAttachments()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_12

    if-eqz v3, :cond_13

    goto :goto_5

    :cond_12
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    :goto_5
    return v2

    :cond_13
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEmbeds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getEmbeds()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_14

    if-eqz v3, :cond_15

    goto :goto_6

    :cond_14
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    :goto_6
    return v2

    :cond_15
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isTts()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isTts()Z

    move-result v3

    if-eq v1, v3, :cond_16

    return v2

    :cond_16
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getCall()Lcom/discord/models/domain/ModelMessage$Call;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getCall()Lcom/discord/models/domain/ModelMessage$Call;

    move-result-object v3

    if-nez v1, :cond_17

    if-eqz v3, :cond_18

    goto :goto_7

    :cond_17
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelMessage$Call;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    :goto_7
    return v2

    :cond_18
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isMentionEveryone()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isMentionEveryone()Z

    move-result v3

    if-eq v1, v3, :cond_19

    return v2

    :cond_19
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getReactions()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getReactions()Ljava/util/Map;

    move-result-object v3

    if-nez v1, :cond_1a

    if-eqz v3, :cond_1b

    goto :goto_8

    :cond_1a
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    :goto_8
    return v2

    :cond_1b
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getPinned()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getPinned()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v1, :cond_1c

    if-eqz v3, :cond_1d

    goto :goto_9

    :cond_1c
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    :goto_9
    return v2

    :cond_1d
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getWebhookId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getWebhookId()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_1e

    if-eqz v3, :cond_1f

    goto :goto_a

    :cond_1e
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1f

    :goto_a
    return v2

    :cond_1f
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v3

    if-nez v1, :cond_20

    if-eqz v3, :cond_21

    goto :goto_b

    :cond_20
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelApplication;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    :goto_b
    return v2

    :cond_21
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v3

    if-nez v1, :cond_22

    if-eqz v3, :cond_23

    goto :goto_c

    :cond_22
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelMessage$Activity;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    :goto_c
    return v2

    :cond_23
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getFlags()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getFlags()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_24

    if-eqz v3, :cond_25

    goto :goto_d

    :cond_24
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_25

    :goto_d
    return v2

    :cond_25
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v3

    if-nez v1, :cond_26

    if-eqz v3, :cond_27

    goto :goto_e

    :cond_26
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelMessage$MessageReference;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    :goto_e
    return v2

    :cond_27
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getReferencedMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getReferencedMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v3

    if-nez v1, :cond_28

    if-eqz v3, :cond_29

    goto :goto_f

    :cond_28
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelMessage;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_29

    :goto_f
    return v2

    :cond_29
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getStickers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getStickers()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_2a

    if-eqz v3, :cond_2b

    goto :goto_10

    :cond_2a
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2b

    :goto_10
    return v2

    :cond_2b
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v3

    if-nez v1, :cond_2c

    if-eqz v3, :cond_2d

    goto :goto_11

    :cond_2c
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelAllowedMentions;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2d

    :goto_11
    return v2

    :cond_2d
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getLastManualAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getLastManualAttemptTimestamp()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_2e

    if-eqz v3, :cond_2f

    goto :goto_12

    :cond_2e
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2f

    :goto_12
    return v2

    :cond_2f
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_30

    if-eqz v3, :cond_31

    goto :goto_13

    :cond_30
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_31

    :goto_13
    return v2

    :cond_31
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v3

    if-nez v1, :cond_32

    if-eqz v3, :cond_33

    goto :goto_14

    :cond_32
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_33

    :goto_14
    return v2

    :cond_33
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getLocalAttachments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getLocalAttachments()Ljava/util/List;

    move-result-object p1

    if-nez v1, :cond_34

    if-eqz p1, :cond_35

    goto :goto_15

    :cond_34
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_35

    :goto_15
    return v2

    :cond_35
    return v0
.end method

.method public getActivity()Lcom/discord/models/domain/ModelMessage$Activity;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    return-object v0
.end method

.method public getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    return-object v0
.end method

.method public getApplication()Lcom/discord/models/domain/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    return-object v0
.end method

.method public getAttachments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageAttachment;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_ATTACHMENTS:Ljava/util/List;

    :goto_0
    return-object v0
.end method

.method public getAuthor()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->author:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public getCall()Lcom/discord/models/domain/ModelMessage$Call;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    return-object v0
.end method

.method public getCallDuration()J
    .locals 4

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->call:Lcom/discord/models/domain/ModelMessage$Call;

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage$Call;->getEndedTimestamp()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-wide v1

    :cond_1
    invoke-static {v0}, Lcom/discord/utilities/time/TimeUtils;->parseUTCDate(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelMessage;->channelId:J

    return-wide v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getEditedTimestamp()J
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->editedTimestampMilliseconds:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->editedTimestamp:Ljava/lang/String;

    invoke-static {v0}, Lcom/discord/utilities/time/TimeUtils;->parseUTCDate(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/models/domain/ModelMessage;->editedTimestampMilliseconds:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->editedTimestampMilliseconds:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getEditedTimestampMilliseconds()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->editedTimestampMilliseconds:Ljava/lang/Long;

    return-object v0
.end method

.method public getEmbeds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessageEmbed;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_EMBEDS:Ljava/util/List;

    :goto_0
    return-object v0
.end method

.method public getFlags()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->flags:Ljava/lang/Long;

    return-object v0
.end method

.method public getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelMessage;->id:J

    return-wide v0
.end method

.method public getInitialAttemptTimestamp()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->initialAttemptTimestamp:Ljava/lang/Long;

    return-object v0
.end method

.method public getLastManualAttemptTimestamp()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->lastManualAttemptTimestamp:Ljava/lang/Long;

    return-object v0
.end method

.method public getLocalAttachments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/messages/LocalAttachment;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    return-object v0
.end method

.method public getMember()Lcom/discord/models/domain/ModelGuildMember;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->member:Lcom/discord/models/domain/ModelGuildMember;

    return-object v0
.end method

.method public getMentionRoles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->mentionRoles:Ljava/util/List;

    return-object v0
.end method

.method public getMentions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_MENTIONS:Ljava/util/List;

    :goto_0
    return-object v0
.end method

.method public getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public getNumRetries()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->numRetries:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPinned()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getReactions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->reactions:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/models/domain/ModelMessage;->EMPTY_REACTIONS:Ljava/util/Map;

    :goto_0
    return-object v0
.end method

.method public getReferencedMessage()Lcom/discord/models/domain/ModelMessage;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->referencedMessage:Lcom/discord/models/domain/ModelMessage;

    return-object v0
.end method

.method public getStickers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->stickers:Ljava/util/List;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->timestampMilliseconds:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->timestamp:Ljava/lang/String;

    invoke-static {v0}, Lcom/discord/utilities/time/TimeUtils;->parseUTCDate(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/models/domain/ModelMessage;->timestampMilliseconds:Ljava/lang/Long;

    :cond_0
    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->timestampMilliseconds:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTimestampMilliseconds()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->timestampMilliseconds:Ljava/lang/Long;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    return v0
.end method

.method public getWebhookId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    return-object v0
.end method

.method public hasAttachments()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->attachments:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasEmbeds()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->embeds:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasGameInvite()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->application:Lcom/discord/models/domain/ModelApplication;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasMentions()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->mentions:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 9

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v0

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    const/16 v0, 0x3b

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v1, v1, 0x3b

    const/16 v4, 0x2b

    if-nez v3, :cond_0

    const/16 v3, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_0
    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v5

    mul-int/lit8 v1, v1, 0x3b

    ushr-long v7, v5, v2

    xor-long/2addr v5, v7

    long-to-int v3, v5

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_1

    const/16 v1, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_1
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_2

    const/16 v1, 0x2b

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v1

    :goto_2
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_3

    const/16 v1, 0x2b

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_3
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_4

    const/16 v1, 0x2b

    goto :goto_4

    :cond_4
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_4
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v5

    mul-int/lit8 v3, v3, 0x3b

    ushr-long v7, v5, v2

    xor-long/2addr v5, v7

    long-to-int v1, v5

    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEditedTimestamp()J

    move-result-wide v5

    mul-int/lit8 v3, v3, 0x3b

    ushr-long v1, v5, v2

    xor-long/2addr v1, v5

    long-to-int v2, v1

    add-int/2addr v3, v2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAttachments()Ljava/util/List;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_5

    const/16 v1, 0x2b

    goto :goto_5

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_5
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEmbeds()Ljava/util/List;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_6

    const/16 v1, 0x2b

    goto :goto_6

    :cond_6
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_6
    add-int/2addr v3, v1

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isTts()Z

    move-result v1

    const/16 v2, 0x4f

    const/16 v5, 0x61

    if-eqz v1, :cond_7

    const/16 v1, 0x4f

    goto :goto_7

    :cond_7
    const/16 v1, 0x61

    :goto_7
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getCall()Lcom/discord/models/domain/ModelMessage$Call;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_8

    const/16 v1, 0x2b

    goto :goto_8

    :cond_8
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage$Call;->hashCode()I

    move-result v1

    :goto_8
    add-int/2addr v3, v1

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isMentionEveryone()Z

    move-result v1

    if-eqz v1, :cond_9

    goto :goto_9

    :cond_9
    const/16 v2, 0x61

    :goto_9
    add-int/2addr v3, v2

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getReactions()Ljava/util/Map;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_a

    const/16 v1, 0x2b

    goto :goto_a

    :cond_a
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_a
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getPinned()Ljava/lang/Boolean;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_b

    const/16 v1, 0x2b

    goto :goto_b

    :cond_b
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_b
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getWebhookId()Ljava/lang/Long;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_c

    const/16 v1, 0x2b

    goto :goto_c

    :cond_c
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_c
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_d

    const/16 v1, 0x2b

    goto :goto_d

    :cond_d
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->hashCode()I

    move-result v1

    :goto_d
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_e

    const/16 v1, 0x2b

    goto :goto_e

    :cond_e
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage$Activity;->hashCode()I

    move-result v1

    :goto_e
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getFlags()Ljava/lang/Long;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_f

    const/16 v1, 0x2b

    goto :goto_f

    :cond_f
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_f
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_10

    const/16 v1, 0x2b

    goto :goto_10

    :cond_10
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage$MessageReference;->hashCode()I

    move-result v1

    :goto_10
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getReferencedMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_11

    const/16 v1, 0x2b

    goto :goto_11

    :cond_11
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->hashCode()I

    move-result v1

    :goto_11
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getStickers()Ljava/util/List;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_12

    const/16 v1, 0x2b

    goto :goto_12

    :cond_12
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_12
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_13

    const/16 v1, 0x2b

    goto :goto_13

    :cond_13
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelAllowedMentions;->hashCode()I

    move-result v1

    :goto_13
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getLastManualAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_14

    const/16 v1, 0x2b

    goto :goto_14

    :cond_14
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_14
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_15

    const/16 v1, 0x2b

    goto :goto_15

    :cond_15
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_15
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_16

    const/16 v1, 0x2b

    goto :goto_16

    :cond_16
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_16
    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getLocalAttachments()Ljava/util/List;

    move-result-object v1

    mul-int/lit8 v3, v3, 0x3b

    if-nez v1, :cond_17

    goto :goto_17

    :cond_17
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_17
    add-int/2addr v3, v4

    return v3
.end method

.method public isCrosspost()Z
    .locals 2

    const-wide/16 v0, 0x2

    invoke-direct {p0, v0, v1}, Lcom/discord/models/domain/ModelMessage;->isOn(J)Z

    move-result v0

    return v0
.end method

.method public isCrossposted()Z
    .locals 2

    const-wide/16 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/discord/models/domain/ModelMessage;->isOn(J)Z

    move-result v0

    return v0
.end method

.method public isFailed()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    const/4 v1, -0x3

    if-eq v0, v1, :cond_1

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isHasLocalUploads()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->hasLocalUploads:Z

    return v0
.end method

.method public isHit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->hit:Z

    return v0
.end method

.method public isLocal()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isMentionEveryone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->mentionEveryone:Z

    return v0
.end method

.method public isPinned()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->pinned:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSourceDeleted()Z
    .locals 2

    const-wide/16 v0, 0x8

    invoke-direct {p0, v0, v1}, Lcom/discord/models/domain/ModelMessage;->isOn(J)Z

    move-result v0

    return v0
.end method

.method public isStale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->stale:Z

    return v0
.end method

.method public isSystemMessage()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isUserMessage()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isTts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->tts:Z

    return v0
.end method

.method public isUrgent()Z
    .locals 2

    const-wide/16 v0, 0x10

    invoke-direct {p0, v0, v1}, Lcom/discord/models/domain/ModelMessage;->isOn(J)Z

    move-result v0

    return v0
.end method

.method public isUserMessage()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/ModelMessage;->type:I

    if-eqz v0, :cond_1

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isWebhook()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelMessage;->webhookId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public markStale()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/models/domain/ModelMessage;->stale:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelMessage(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", nonce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", content="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", author="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mentions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mentionRoles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", editedTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEditedTimestamp()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAttachments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", embeds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEmbeds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isTts()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", call="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getCall()Lcom/discord/models/domain/ModelMessage$Call;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mentionEveryone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isMentionEveryone()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", reactions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getReactions()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pinned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getPinned()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", webhookId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getWebhookId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getFlags()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", messageReference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", referencedMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getReferencedMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getStickers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowedMentions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastManualAttemptTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getLastManualAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", initialAttemptTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numRetries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", timestampMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getTimestampMilliseconds()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", editedTimestampMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getEditedTimestampMilliseconds()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isStale()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isHit()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasLocalUploads="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->isHasLocalUploads()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", localAttachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getLocalAttachments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", member="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelMessage;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
