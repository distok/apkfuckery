.class public final Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;
.super Ljava/lang/Object;
.source "ModelSubscriptionPlan.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final from(I)Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    sget-object p1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_0
    return-object p1
.end method
