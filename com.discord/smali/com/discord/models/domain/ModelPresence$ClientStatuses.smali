.class public final Lcom/discord/models/domain/ModelPresence$ClientStatuses;
.super Ljava/lang/Object;
.source "ModelPresence.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelPresence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientStatuses"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;


# instance fields
.field private final desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

.field private final mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

.field private final webStatus:Lcom/discord/models/domain/ModelPresence$Status;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->Companion:Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;)V
    .locals 1

    const-string v0, "desktopStatus"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "webStatus"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mobileStatus"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    iput-object p2, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    iput-object p3, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;ILjava/lang/Object;)Lcom/discord/models/domain/ModelPresence$ClientStatuses;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->copy(Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;)Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;)Lcom/discord/models/domain/ModelPresence$ClientStatuses;
    .locals 1

    const-string v0, "desktopStatus"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "webStatus"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mobileStatus"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/models/domain/ModelPresence$ClientStatuses;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;Lcom/discord/models/domain/ModelPresence$Status;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    iget-object v1, p1, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    iget-object p1, p1, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDesktopStatus()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final getMobileStatus()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public final getWebStatus()Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isMobile()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    sget-object v1, Lcom/discord/models/domain/ModelPresence$Status;->ONLINE:Lcom/discord/models/domain/ModelPresence$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ClientStatuses(desktopStatus="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->desktopStatus:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", webStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->webStatus:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mobileStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->mobileStatus:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
