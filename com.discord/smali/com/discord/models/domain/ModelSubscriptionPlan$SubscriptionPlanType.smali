.class public final enum Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;
.super Ljava/lang/Enum;
.source "ModelSubscriptionPlan.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSubscriptionPlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SubscriptionPlanType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final Companion:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;

.field private static final LEGACY_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum NONE_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum NONE_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field private static final PREMIUM_GUILD_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PREMIUM_GUILD_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum PREMIUM_MONTH_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field private static final PREMIUM_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PREMIUM_YEAR_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field public static final enum PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

.field private static final TIER_1_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field

.field private static final TIER_2_PLANS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final interval:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

.field private final planId:J

.field private final planTypeString:Ljava/lang/String;

.field private final premiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field private final price:I


# direct methods
.method public static constructor <clinit>()V
    .locals 21

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    new-instance v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v11, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->MONTHLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    sget-object v12, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const-string v2, "NONE_MONTH"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v6, "none_month"

    const-wide v7, 0x8b873825d000000L

    move-object v1, v10

    move-object v5, v11

    move-object v9, v12

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->NONE_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v13, 0x0

    aput-object v10, v0, v13

    new-instance v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v14, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->YEARLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    const-string v2, "NONE_YEAR"

    const/4 v3, 0x1

    const-string v6, "none_year"

    const-wide v7, 0x8b875b5c4c00000L

    move-object v1, v10

    move-object v5, v14

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->NONE_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v12, 0x1

    aput-object v10, v0, v12

    new-instance v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v15, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const-string v2, "PREMIUM_MONTH_LEGACY"

    const/4 v3, 0x2

    const/16 v4, 0x1f3

    const-string v6, "premium_month"

    const-wide v7, 0x719c0a6fe400000L    # 1.85953420074464E-274

    move-object v1, v10

    move-object v5, v11

    move-object v9, v15

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v10, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v9, 0x2

    aput-object v10, v0, v9

    new-instance v16, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const-string v2, "PREMIUM_YEAR_LEGACY"

    const/4 v3, 0x3

    const/16 v4, 0x1387

    const-string v6, "premium_year"

    const-wide v7, 0x719c0a80c000000L

    move-object/from16 v1, v16

    move-object v5, v14

    const/4 v12, 0x2

    move-object v9, v15

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v16, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_LEGACY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v1, 0x3

    aput-object v16, v0, v1

    new-instance v17, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    sget-object v18, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const-string v2, "PREMIUM_MONTH_TIER_1"

    const/4 v3, 0x4

    const/16 v4, 0x1f3

    const-string v6, "premium_month_tier_1"

    const-wide v7, 0x719c0aa9f800000L

    move-object/from16 v1, v17

    move-object v5, v11

    move-object/from16 v9, v18

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v17, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v1, 0x4

    aput-object v17, v0, v1

    new-instance v19, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const-string v2, "PREMIUM_YEAR_TIER_1"

    const/4 v3, 0x5

    const/16 v4, 0x1387

    const-string v6, "premium_year_tier_1"

    const-wide v7, 0x719c0abd8800000L

    move-object/from16 v1, v19

    move-object v5, v14

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v19, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v1, 0x5

    aput-object v19, v0, v1

    new-instance v18, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const-string v2, "PREMIUM_MONTH_TIER_2"

    const/4 v3, 0x6

    const/16 v4, 0x3e7

    const-string v6, "premium_month_tier_2"

    const-wide v7, 0x719c0acbe000000L

    move-object/from16 v1, v18

    move-object v5, v11

    move-object v9, v15

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v18, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v1, 0x6

    aput-object v18, v0, v1

    new-instance v20, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const-string v2, "PREMIUM_YEAR_TIER_2"

    const/4 v3, 0x7

    const/16 v4, 0x270f

    const-string v6, "premium_year_tier_2"

    const-wide v7, 0x719c0add1800000L

    move-object/from16 v1, v20

    move-object v5, v14

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v20, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_YEAR_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/4 v1, 0x7

    aput-object v20, v0, v1

    new-instance v15, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const-string v2, "PREMIUM_GUILD_MONTH"

    const/16 v3, 0x8

    const/16 v4, 0x1f3

    const/4 v6, 0x0

    const-wide v7, 0x832772c35420003L

    const/4 v9, 0x0

    move-object v1, v15

    move-object v5, v11

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v15, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/16 v1, 0x8

    aput-object v15, v0, v1

    new-instance v11, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const-string v2, "PREMIUM_GUILD_YEAR"

    const/16 v3, 0x9

    const/16 v4, 0x1387

    const-wide v7, 0x832772d73c00005L

    move-object v1, v11

    move-object v5, v14

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;-><init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sput-object v11, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    const/16 v1, 0x9

    aput-object v11, v0, v1

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->$VALUES:[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    new-instance v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->Companion:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType$Companion;

    new-array v0, v12, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v19, v0, v13

    const/4 v1, 0x1

    aput-object v17, v0, v1

    invoke-static {v0}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->TIER_1_PLANS:Ljava/util/Set;

    new-array v2, v12, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v16, v2, v13

    aput-object v10, v2, v1

    invoke-static {v2}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->LEGACY_PLANS:Ljava/util/Set;

    new-array v3, v12, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v20, v3, v13

    aput-object v18, v3, v1

    invoke-static {v3}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-static {v3, v2}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->TIER_2_PLANS:Ljava/util/Set;

    invoke-static {v0, v2}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_PLANS:Ljava/util/Set;

    new-array v0, v12, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    aput-object v15, v0, v13

    aput-object v11, v0, v1

    invoke-static {v0}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_PLANS:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;Ljava/lang/String;JLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;",
            "Ljava/lang/String;",
            "J",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->price:I

    iput-object p4, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->interval:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    iput-object p5, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->planTypeString:Ljava/lang/String;

    iput-wide p6, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->planId:J

    iput-object p8, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->premiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-void
.end method

.method public static final synthetic access$getLEGACY_PLANS$cp()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->LEGACY_PLANS:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic access$getPREMIUM_GUILD_PLANS$cp()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_PLANS:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic access$getPREMIUM_PLANS$cp()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_PLANS:Ljava/util/Set;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;
    .locals 1

    const-class v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->$VALUES:[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v0}, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    return-object v0
.end method


# virtual methods
.method public final getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->interval:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    return-object v0
.end method

.method public final getPlanId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->planId:J

    return-wide v0
.end method

.method public final getPlanTypeString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->planTypeString:Ljava/lang/String;

    return-object v0
.end method

.method public final getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->premiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final getPrice()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->price:I

    return v0
.end method

.method public final isGrandfathered()Z
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->LEGACY_PLANS:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isPremiumSubscription()Z
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_PLANS:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
