.class public final Lcom/discord/models/domain/billing/ModelInvoicePreview;
.super Ljava/lang/Object;
.source "ModelInvoicePreview.kt"


# instance fields
.field private final currency:Ljava/lang/String;

.field private final id:J

.field private final invoiceItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/billing/ModelInvoiceItem;",
            ">;"
        }
    .end annotation
.end field

.field private final subscriptionPeriodEnd:Ljava/lang/String;

.field private final subscriptionPeriodStart:Ljava/lang/String;

.field private final subtotal:I

.field private final tax:I

.field private final taxInclusive:Z

.field private final total:I


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/billing/ModelInvoiceItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIZI)V"
        }
    .end annotation

    const-string v0, "currency"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceItems"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "subscriptionPeriodEnd"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "subscriptionPeriodStart"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    iput-wide p2, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    iput-object p4, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    iput-object p5, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    iput p7, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    iput p8, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    iput-boolean p9, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    iput p10, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZIILjava/lang/Object;)Lcom/discord/models/domain/billing/ModelInvoicePreview;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    goto :goto_1

    :cond_1
    move-wide v3, p2

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v5, p4

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object/from16 v6, p5

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget v8, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    goto :goto_5

    :cond_5
    move/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget v9, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    goto :goto_6

    :cond_6
    move/from16 v9, p8

    :goto_6
    and-int/lit16 v10, v1, 0x80

    if-eqz v10, :cond_7

    iget-boolean v10, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    goto :goto_7

    :cond_7
    move/from16 v10, p9

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget v1, v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    goto :goto_8

    :cond_8
    move/from16 v1, p10

    :goto_8
    move-object p1, v2

    move-wide p2, v3

    move-object p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->copy(Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZI)Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    return-wide v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/billing/ModelInvoiceItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    return v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    return v0
.end method

.method public final copy(Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZI)Lcom/discord/models/domain/billing/ModelInvoicePreview;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/billing/ModelInvoiceItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIZI)",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;"
        }
    .end annotation

    const-string v0, "currency"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceItems"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "subscriptionPeriodEnd"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "subscriptionPeriodStart"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-object v1, v0

    move-wide v3, p2

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/discord/models/domain/billing/ModelInvoicePreview;-><init>(Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    iget-wide v2, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    iget v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    iget v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    iget-boolean v1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    iget p1, p1, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrency()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    return-wide v0
.end method

.method public final getInvoiceItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/billing/ModelInvoiceItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    return-object v0
.end method

.method public final getSubscriptionPeriodEnd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubscriptionPeriodStart()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubtotal()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    return v0
.end method

.method public final getTax()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    return v0
.end method

.method public final getTaxInclusive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    return v0
.end method

.method public final getTotal()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelInvoicePreview(currency="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->invoiceItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionPeriodEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodEnd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionPeriodStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subscriptionPeriodStart:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", subtotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->subtotal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->tax:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", taxInclusive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->taxInclusive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/billing/ModelInvoicePreview;->total:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
