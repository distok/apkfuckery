.class public final Lcom/discord/models/domain/billing/ModelPlanItem;
.super Ljava/lang/Object;
.source "ModelPlanItem.kt"


# instance fields
.field private final id:J

.field private final planId:J

.field private final status:I

.field private final subscriptionId:J


# direct methods
.method public constructor <init>(JJJI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    iput-wide p3, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    iput-wide p5, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    iput p7, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    return-void
.end method

.method private final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    return v0
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/billing/ModelPlanItem;JJJIILjava/lang/Object;)Lcom/discord/models/domain/billing/ModelPlanItem;
    .locals 8

    move-object v0, p0

    and-int/lit8 v1, p8, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p8, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    goto :goto_1

    :cond_1
    move-wide v3, p3

    :goto_1
    and-int/lit8 v5, p8, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    goto :goto_2

    :cond_2
    move-wide v5, p5

    :goto_2
    and-int/lit8 v7, p8, 0x8

    if-eqz v7, :cond_3

    iget v7, v0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    goto :goto_3

    :cond_3
    move v7, p7

    :goto_3
    move-wide p1, v1

    move-wide p3, v3

    move-wide p5, v5

    move p7, v7

    invoke-virtual/range {p0 .. p7}, Lcom/discord/models/domain/billing/ModelPlanItem;->copy(JJJI)Lcom/discord/models/domain/billing/ModelPlanItem;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    return-wide v0
.end method

.method public final copy(JJJI)Lcom/discord/models/domain/billing/ModelPlanItem;
    .locals 9

    new-instance v8, Lcom/discord/models/domain/billing/ModelPlanItem;

    move-object v0, v8

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/discord/models/domain/billing/ModelPlanItem;-><init>(JJJI)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/billing/ModelPlanItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/billing/ModelPlanItem;

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    iget-wide v2, p1, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    iget-wide v2, p1, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    iget-wide v2, p1, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    iget p1, p1, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    return-wide v0
.end method

.method public final getPlanId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    return-wide v0
.end method

.method public final getStatus()Lcom/discord/models/domain/ModelSubscription$Status;
    .locals 2

    sget-object v0, Lcom/discord/models/domain/ModelSubscription$Status;->Companion:Lcom/discord/models/domain/ModelSubscription$Status$Companion;

    iget v1, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelSubscription$Status$Companion;->from(I)Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v0

    return-object v0
.end method

.method public final getSubscriptionId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    iget-wide v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    ushr-long v5, v3, v2

    xor-long/2addr v3, v5

    long-to-int v0, v3

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelPlanItem(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->subscriptionId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", planId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->planId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/domain/billing/ModelPlanItem;->status:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
