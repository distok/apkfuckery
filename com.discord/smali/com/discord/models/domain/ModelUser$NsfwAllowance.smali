.class public final enum Lcom/discord/models/domain/ModelUser$NsfwAllowance;
.super Ljava/lang/Enum;
.source "ModelUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NsfwAllowance"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/domain/ModelUser$NsfwAllowance;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/domain/ModelUser$NsfwAllowance;

.field public static final enum ALLOWED:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

.field public static final enum DISALLOWED:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

.field public static final enum UNKNOWN:Lcom/discord/models/domain/ModelUser$NsfwAllowance;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/discord/models/domain/ModelUser$NsfwAllowance;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->UNKNOWN:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    new-instance v1, Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    const-string v3, "ALLOWED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/discord/models/domain/ModelUser$NsfwAllowance;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->ALLOWED:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    new-instance v3, Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    const-string v5, "DISALLOWED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/discord/models/domain/ModelUser$NsfwAllowance;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->DISALLOWED:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->$VALUES:[Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/domain/ModelUser$NsfwAllowance;
    .locals 1

    const-class v0, Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/domain/ModelUser$NsfwAllowance;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->$VALUES:[Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    invoke-virtual {v0}, [Lcom/discord/models/domain/ModelUser$NsfwAllowance;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    return-object v0
.end method
