.class public final Lcom/discord/models/domain/auth/ModelLoginResult;
.super Ljava/lang/Object;
.source "ModelLoginResult.kt"


# instance fields
.field private final mfa:Z

.field private final ticket:Ljava/lang/String;

.field private final token:Ljava/lang/String;

.field private final userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    iput-object p2, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    return-void
.end method

.method public synthetic constructor <init>(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/models/domain/auth/ModelLoginResult;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/auth/ModelLoginResult;ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;ILjava/lang/Object;)Lcom/discord/models/domain/auth/ModelLoginResult;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-boolean p1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/models/domain/auth/ModelLoginResult;->copy(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)Lcom/discord/models/domain/auth/ModelLoginResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    return-object v0
.end method

.method public final copy(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)Lcom/discord/models/domain/auth/ModelLoginResult;
    .locals 1

    new-instance v0, Lcom/discord/models/domain/auth/ModelLoginResult;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/models/domain/auth/ModelLoginResult;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/auth/ModelLoginResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/auth/ModelLoginResult;

    iget-boolean v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    iget-boolean v1, p1, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    iget-object p1, p1, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMfa()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    return v0
.end method

.method public final getTicket()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserSettings()Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ModelLoginResult(mfa="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->mfa:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->ticket:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", userSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/auth/ModelLoginResult;->userSettings:Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
