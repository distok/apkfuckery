.class public final enum Lcom/discord/models/domain/ModelPresence$Status;
.super Ljava/lang/Enum;
.source "ModelPresence.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelPresence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelPresence$Status$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/domain/ModelPresence$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/domain/ModelPresence$Status;

.field public static final Companion:Lcom/discord/models/domain/ModelPresence$Status$Companion;

.field public static final enum DND:Lcom/discord/models/domain/ModelPresence$Status;

.field public static final enum IDLE:Lcom/discord/models/domain/ModelPresence$Status;

.field public static final enum INVISIBLE:Lcom/discord/models/domain/ModelPresence$Status;

.field public static final enum OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

.field public static final enum ONLINE:Lcom/discord/models/domain/ModelPresence$Status;


# instance fields
.field private final apiStringRepresentation:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/discord/models/domain/ModelPresence$Status;

    new-instance v1, Lcom/discord/models/domain/ModelPresence$Status;

    const-string v2, "ONLINE"

    const/4 v3, 0x0

    const-string v4, "online"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelPresence$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelPresence$Status;->ONLINE:Lcom/discord/models/domain/ModelPresence$Status;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelPresence$Status;

    const-string v2, "IDLE"

    const/4 v3, 0x1

    const-string v4, "idle"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelPresence$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelPresence$Status;->IDLE:Lcom/discord/models/domain/ModelPresence$Status;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelPresence$Status;

    const-string v2, "DND"

    const/4 v3, 0x2

    const-string v4, "dnd"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelPresence$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelPresence$Status;->DND:Lcom/discord/models/domain/ModelPresence$Status;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelPresence$Status;

    const-string v2, "INVISIBLE"

    const/4 v3, 0x3

    const-string v4, "invisible"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelPresence$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelPresence$Status;->INVISIBLE:Lcom/discord/models/domain/ModelPresence$Status;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelPresence$Status;

    const-string v2, "OFFLINE"

    const/4 v3, 0x4

    const-string v4, "offline"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelPresence$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelPresence$Status;->OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/models/domain/ModelPresence$Status;->$VALUES:[Lcom/discord/models/domain/ModelPresence$Status;

    new-instance v0, Lcom/discord/models/domain/ModelPresence$Status$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelPresence$Status$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelPresence$Status;->Companion:Lcom/discord/models/domain/ModelPresence$Status$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/models/domain/ModelPresence$Status;->apiStringRepresentation:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    const-class v0, Lcom/discord/models/domain/ModelPresence$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelPresence$Status;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/domain/ModelPresence$Status;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelPresence$Status;->$VALUES:[Lcom/discord/models/domain/ModelPresence$Status;

    invoke-virtual {v0}, [Lcom/discord/models/domain/ModelPresence$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/domain/ModelPresence$Status;

    return-object v0
.end method


# virtual methods
.method public final getApiStringRepresentation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$Status;->apiStringRepresentation:Ljava/lang/String;

    return-object v0
.end method

.method public final serialize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelPresence$Status;->apiStringRepresentation:Ljava/lang/String;

    return-object v0
.end method
