.class public final Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;
.super Ljava/lang/Object;
.source "ModelConnectedIntegrationGuild.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelConnectedIntegrationGuild;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $icon:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $id:Lkotlin/properties/ReadWriteProperty;

.field public final synthetic $id$metadata:Lkotlin/reflect/KProperty;

.field public final synthetic $name:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $reader:Lcom/discord/models/domain/Model$JsonReader;


# direct methods
.method public constructor <init>(Lkotlin/properties/ReadWriteProperty;Lkotlin/reflect/KProperty;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$id:Lkotlin/properties/ReadWriteProperty;

    iput-object p2, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$id$metadata:Lkotlin/reflect/KProperty;

    iput-object p3, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    iput-object p4, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$icon:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p5, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$name:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 5

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0xd1b

    if-eq v0, v1, :cond_3

    const v1, 0x313c79

    if-eq v0, v1, :cond_2

    const v1, 0x337a8b

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "name"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$name:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const-string v0, "icon"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$icon:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0}, Lcom/discord/models/domain/Model$JsonReader;->nextStringOrNull()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    const-string v0, "id"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$id:Lkotlin/properties/ReadWriteProperty;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$id$metadata:Lkotlin/reflect/KProperty;

    iget-object v2, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/discord/models/domain/ModelConnectedIntegrationGuild$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    :goto_1
    return-void
.end method
