.class public abstract Lcom/discord/models/domain/ModelApplicationStream;
.super Ljava/lang/Object;
.source "ModelApplicationStream.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelApplicationStream$GuildStream;,
        Lcom/discord/models/domain/ModelApplicationStream$CallStream;,
        Lcom/discord/models/domain/ModelApplicationStream$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;


# instance fields
.field private final channelId:J

.field private final encodedStreamKey$delegate:Lkotlin/Lazy;

.field private final ownerId:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/domain/ModelApplicationStream$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    return-void
.end method

.method private constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/ModelApplicationStream;->channelId:J

    iput-wide p3, p0, Lcom/discord/models/domain/ModelApplicationStream;->ownerId:J

    new-instance p1, Lcom/discord/models/domain/ModelApplicationStream$encodedStreamKey$2;

    invoke-direct {p1, p0}, Lcom/discord/models/domain/ModelApplicationStream$encodedStreamKey$2;-><init>(Lcom/discord/models/domain/ModelApplicationStream;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/ModelApplicationStream;->encodedStreamKey$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(JJLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/models/domain/ModelApplicationStream;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public abstract encodeStreamKey()Ljava/lang/String;
.end method

.method public getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelApplicationStream;->channelId:J

    return-wide v0
.end method

.method public final getEncodedStreamKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelApplicationStream;->encodedStreamKey$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelApplicationStream;->ownerId:J

    return-wide v0
.end method

.method public abstract getType()Ljava/lang/String;
.end method
