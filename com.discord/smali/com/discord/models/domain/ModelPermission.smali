.class public Lcom/discord/models/domain/ModelPermission;
.super Ljava/lang/Object;
.source "ModelPermission.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/ModelPermission$Permission;
    }
.end annotation


# static fields
.field public static final ADD_REACTIONS:J = 0x40L

.field public static final ADMINISTRATOR:J = 0x8L

.field public static final ALL:J = 0x7ff7feffL

.field public static final ATTACH_FILES:J = 0x8000L

.field public static final BAN_MEMBERS:J = 0x4L

.field public static final CHANGE_NICKNAME:J = 0x4000000L

.field public static final CONNECT:J = 0x100000L

.field public static final CREATE_INSTANT_INVITE:J = 0x1L

.field public static final DEAFEN_MEMBERS:J = 0x800000L

.field public static final DEFAULT:J = 0x637de41L

.field public static final ELEVATED:J = 0x1000203eL

.field public static final EMBED_LINKS:J = 0x4000L

.field public static final KICK_MEMBERS:J = 0x2L

.field public static final MANAGE_CHANNELS:J = 0x10L

.field public static final MANAGE_EMOJIS:J = 0x40000000L

.field public static final MANAGE_GUILD:J = 0x20L

.field public static final MANAGE_MESSAGES:J = 0x2000L

.field public static final MANAGE_NICKNAMES:J = 0x8000000L

.field public static final MANAGE_ROLES:J = 0x10000000L

.field public static final MANAGE_WEBHOOKS:J = 0x20000000L

.field public static final MENTION_EVERYONE:J = 0x20000L

.field public static final MODERATOR_PERMISSIONS:J = 0x1002203eL

.field public static final MOVE_MEMBERS:J = 0x1000000L

.field public static final MUTE_MEMBERS:J = 0x400000L

.field public static final NONE:J = 0x0L

.field public static final PRIORITY_SPEAKER:J = 0x100L

.field public static final READ_MESSAGE_HISTORY:J = 0x10000L

.field public static final SEND_MESSAGES:J = 0x800L

.field public static final SEND_TTS_MESSAGES:J = 0x1000L

.field public static final SPEAK:J = 0x200000L

.field public static final STREAM:J = 0x200L

.field public static final USE_EXTERNAL_EMOJIS:J = 0x40000L

.field public static final USE_VAD:J = 0x2000000L

.field public static final VIEW_AUDIT_LOG:J = 0x80L

.field public static final VIEW_CHANNEL:J = 0x400L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
