.class public final Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;
.super Ljava/lang/Object;
.source "ModelSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelSubscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubscriptionRenewalMutations"
.end annotation


# instance fields
.field private final additionalPlans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentGatewayPlanId:Ljava/lang/String;

.field private final planId:J


# direct methods
.method public constructor <init>(JLjava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    iput-object p3, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;JLjava/util/List;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-wide p1, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p3, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-object p4, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->copy(JLjava/util/List;Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    return-wide v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JLjava/util/List;Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;"
        }
    .end annotation

    new-instance v0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;-><init>(JLjava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    iget-wide v2, p1, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAdditionalPlans()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    return-object v0
.end method

.method public final getPaymentGatewayPlanId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlanId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "SubscriptionRenewalMutations(planId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->planId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", additionalPlans="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->additionalPlans:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentGatewayPlanId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->paymentGatewayPlanId:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
