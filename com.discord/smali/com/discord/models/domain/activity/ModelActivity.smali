.class public Lcom/discord/models/domain/activity/ModelActivity;
.super Ljava/lang/Object;
.source "ModelActivity.java"

# interfaces
.implements Lcom/discord/models/domain/Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;
    }
.end annotation


# static fields
.field public static final FLAG_JOIN:I = 0x2

.field public static final FLAG_PLAY:I = 0x20

.field public static final FLAG_SPECTATE:I = 0x4

.field public static final FLAG_SYNC:I = 0x10

.field public static final GAME_PLATFORM_ANDROID:Ljava/lang/String; = "android"

.field public static final GAME_PLATFORM_DESKTOP:Ljava/lang/String; = "desktop"

.field public static final GAME_PLATFORM_IOS:Ljava/lang/String; = "ios"

.field public static final GAME_PLATFORM_SAMSUNG:Ljava/lang/String; = "samsung"

.field public static final GAME_PLATFORM_XBOX:Ljava/lang/String; = "xbox"

.field public static final PLATFORM_SPOTIFY:Ljava/lang/String; = "spotify"

.field public static final TYPE_COMPETING:I = 0x5

.field public static final TYPE_CUSTOM_STATUS:I = 0x4

.field public static final TYPE_LISTENING:I = 0x2

.field public static final TYPE_PLAYING:I = 0x0

.field public static final TYPE_STREAMING:I = 0x1

.field public static final TYPE_WATCHING:I = 0x3

.field private static final XBOX_APPLICATION_ID:J = 0x614867e6c420000L


# instance fields
.field public applicationId:Ljava/lang/Long;

.field private assets:Lcom/discord/models/domain/activity/ModelActivityAssets;

.field private buttons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private createdAt:Ljava/lang/Long;

.field private details:Ljava/lang/String;

.field private emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

.field private flags:I

.field private metadata:Lcom/discord/models/domain/activity/ModelActivityMetadata;

.field public name:Ljava/lang/String;

.field public party:Lcom/discord/models/domain/activity/ModelActivityParty;

.field private platform:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private supportedPlatforms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private syncId:Ljava/lang/String;
    .annotation runtime Lf/h/d/v/b;
        value = "sync_id"
    .end annotation
.end field

.field private timestamps:Lcom/discord/models/domain/activity/ModelActivityTimestamps;

.field public type:I

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->supportedPlatforms:Ljava/util/List;

    return-void
.end method

.method private static computeType(Ljava/lang/String;)I
    .locals 7

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x4

    const/4 v5, 0x3

    const/4 v6, 0x2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "5"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x6

    goto :goto_1

    :pswitch_1
    const-string v0, "4"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x5

    goto :goto_1

    :pswitch_2
    const-string v0, "3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x4

    goto :goto_1

    :pswitch_3
    const-string v0, "2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x3

    goto :goto_1

    :pswitch_4
    const-string v0, "1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x2

    goto :goto_1

    :pswitch_5
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p0, -0x1

    :goto_1
    if-eq p0, v6, :cond_5

    if-eq p0, v5, :cond_4

    if-eq p0, v4, :cond_3

    if-eq p0, v3, :cond_2

    if-eq p0, v2, :cond_1

    return v1

    :cond_1
    return v3

    :cond_2
    return v4

    :cond_3
    return v5

    :cond_4
    return v6

    :cond_5
    const/4 p0, 0x1

    return p0

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static createForCustom(Ljava/lang/String;Lcom/discord/models/domain/ModelMessageReaction$Emoji;)Lcom/discord/models/domain/activity/ModelActivity;
    .locals 2

    new-instance v0, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-direct {v0}, Lcom/discord/models/domain/activity/ModelActivity;-><init>()V

    const-string v1, "Custom Status"

    iput-object v1, v0, Lcom/discord/models/domain/activity/ModelActivity;->name:Ljava/lang/String;

    const/4 v1, 0x4

    iput v1, v0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    iput-object p0, v0, Lcom/discord/models/domain/activity/ModelActivity;->state:Ljava/lang/String;

    iput-object p1, v0, Lcom/discord/models/domain/activity/ModelActivity;->emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    return-object v0
.end method

.method public static createForListening(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/activity/ModelActivity;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-direct {v1}, Lcom/discord/models/domain/activity/ModelActivity;-><init>()V

    const/4 v2, 0x2

    iput v2, v1, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    iput-object p0, v1, Lcom/discord/models/domain/activity/ModelActivity;->name:Ljava/lang/String;

    new-instance p0, Lcom/discord/models/domain/activity/ModelActivityAssets;

    invoke-direct {p0, p3, p4}, Lcom/discord/models/domain/activity/ModelActivityAssets;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, v1, Lcom/discord/models/domain/activity/ModelActivity;->assets:Lcom/discord/models/domain/activity/ModelActivityAssets;

    iput-object p1, v1, Lcom/discord/models/domain/activity/ModelActivity;->details:Ljava/lang/String;

    iput-object p5, v1, Lcom/discord/models/domain/activity/ModelActivity;->state:Ljava/lang/String;

    new-instance p0, Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    invoke-virtual {p7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p8}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p0, p1, p3}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, v1, Lcom/discord/models/domain/activity/ModelActivity;->timestamps:Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    new-instance p0, Lcom/discord/models/domain/activity/ModelActivityParty;

    invoke-direct {p0, p9}, Lcom/discord/models/domain/activity/ModelActivityParty;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/discord/models/domain/activity/ModelActivity;->party:Lcom/discord/models/domain/activity/ModelActivityParty;

    iput-object p2, v1, Lcom/discord/models/domain/activity/ModelActivity;->syncId:Ljava/lang/String;

    const/16 p0, 0x30

    iput p0, v1, Lcom/discord/models/domain/activity/ModelActivity;->flags:I

    new-instance p0, Lcom/discord/models/domain/activity/ModelActivityMetadata;

    invoke-direct {p0, v0, p10, p6}, Lcom/discord/models/domain/activity/ModelActivityMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, v1, Lcom/discord/models/domain/activity/ModelActivity;->metadata:Lcom/discord/models/domain/activity/ModelActivityMetadata;

    return-object v1
.end method

.method public static createForPlaying(Ljava/lang/String;)Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-direct {v0}, Lcom/discord/models/domain/activity/ModelActivity;-><init>()V

    iput-object p0, v0, Lcom/discord/models/domain/activity/ModelActivity;->name:Ljava/lang/String;

    const/4 p0, 0x0

    iput p0, v0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    const-string p0, "android"

    iput-object p0, v0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public assignField(Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v1, "platform"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v0, 0x10

    goto/16 :goto_1

    :sswitch_1
    const-string/jumbo v1, "timestamps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v0, 0xf

    goto/16 :goto_1

    :sswitch_2
    const-string v1, "session_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_3
    const-string v1, "details"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_4
    const-string v1, "created_at"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_5
    const-string/jumbo v1, "supported_platforms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_6
    const-string v1, "buttons"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v0, 0xa

    goto/16 :goto_1

    :sswitch_7
    const-string/jumbo v1, "state"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v0, 0x9

    goto/16 :goto_1

    :sswitch_8
    const-string v1, "party"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0x8

    goto/16 :goto_1

    :sswitch_9
    const-string v1, "flags"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    goto :goto_0

    :cond_9
    const/4 v0, 0x7

    goto :goto_1

    :sswitch_a
    const-string v1, "emoji"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    goto :goto_0

    :cond_a
    const/4 v0, 0x6

    goto :goto_1

    :sswitch_b
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    goto :goto_0

    :cond_b
    const/4 v0, 0x5

    goto :goto_1

    :sswitch_c
    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    goto :goto_0

    :cond_c
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_d
    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    goto :goto_0

    :cond_d
    const/4 v0, 0x3

    goto :goto_1

    :sswitch_e
    const-string v1, "application_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    goto :goto_0

    :cond_e
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_f
    const-string v1, "assets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    goto :goto_0

    :cond_f
    const/4 v0, 0x1

    goto :goto_1

    :sswitch_10
    const-string/jumbo v1, "sync_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    goto :goto_0

    :cond_10
    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    goto/16 :goto_2

    :pswitch_0
    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_1
    new-instance v0, Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    invoke-direct {v0}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->timestamps:Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->sessionId:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->details:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->details:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_4
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->createdAt:Ljava/lang/Long;

    goto/16 :goto_2

    :pswitch_5
    new-instance v0, Lf/a/d/a/q1/b;

    invoke-direct {v0, p1}, Lf/a/d/a/q1/b;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->supportedPlatforms:Ljava/util/List;

    goto/16 :goto_2

    :pswitch_6
    new-instance v0, Lf/a/d/a/q1/b;

    invoke-direct {v0, p1}, Lf/a/d/a/q1/b;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->buttons:Ljava/util/List;

    goto :goto_2

    :pswitch_7
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->state:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->state:Ljava/lang/String;

    goto :goto_2

    :pswitch_8
    new-instance v0, Lcom/discord/models/domain/activity/ModelActivityParty;

    invoke-direct {v0}, Lcom/discord/models/domain/activity/ModelActivityParty;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivityParty;

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->party:Lcom/discord/models/domain/activity/ModelActivityParty;

    goto :goto_2

    :pswitch_9
    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->flags:I

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result p1

    iput p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->flags:I

    goto :goto_2

    :pswitch_a
    new-instance v0, Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-direct {v0}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    goto :goto_2

    :pswitch_b
    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/models/domain/activity/ModelActivity;->computeType(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    goto :goto_2

    :pswitch_c
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->name:Ljava/lang/String;

    goto :goto_2

    :pswitch_d
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->url:Ljava/lang/String;

    goto :goto_2

    :pswitch_e
    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->applicationId:Ljava/lang/Long;

    goto :goto_2

    :pswitch_f
    new-instance v0, Lcom/discord/models/domain/activity/ModelActivityAssets;

    invoke-direct {v0}, Lcom/discord/models/domain/activity/ModelActivityAssets;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivityAssets;

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->assets:Lcom/discord/models/domain/activity/ModelActivityAssets;

    goto :goto_2

    :pswitch_10
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->syncId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivity;->syncId:Ljava/lang/String;

    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x67dc72c1 -> :sswitch_10
        -0x53ef8c7d -> :sswitch_f
        -0x4cb85596 -> :sswitch_e
        0x1c56f -> :sswitch_d
        0x337a8b -> :sswitch_c
        0x368f3a -> :sswitch_b
        0x5c28046 -> :sswitch_a
        0x5cfee87 -> :sswitch_9
        0x6581ae6 -> :sswitch_8
        0x68ac491 -> :sswitch_7
        0xe62bf81 -> :sswitch_6
        0x343af40f -> :sswitch_5
        0x51a3a8ea -> :sswitch_4
        0x5cd8f242 -> :sswitch_3
        0x630ddf64 -> :sswitch_2
        0x65dbfa1d -> :sswitch_1
        0x6fbd6873 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/models/domain/activity/ModelActivity;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/models/domain/activity/ModelActivity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {p1, p0}, Lcom/discord/models/domain/activity/ModelActivity;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    return v2

    :cond_4
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v3

    if-eq v1, v3, :cond_5

    return v2

    :cond_5
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_6

    if-eqz v3, :cond_7

    goto :goto_1

    :cond_6
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    :goto_1
    return v2

    :cond_7
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_8

    if-eqz v3, :cond_9

    goto :goto_2

    :cond_8
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    :goto_2
    return v2

    :cond_9
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v3

    if-nez v1, :cond_a

    if-eqz v3, :cond_b

    goto :goto_3

    :cond_a
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    :goto_3
    return v2

    :cond_b
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v3

    if-nez v1, :cond_c

    if-eqz v3, :cond_d

    goto :goto_4

    :cond_c
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/activity/ModelActivityAssets;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    :goto_4
    return v2

    :cond_d
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_e

    if-eqz v3, :cond_f

    goto :goto_5

    :cond_e
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    :goto_5
    return v2

    :cond_f
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v3

    if-nez v1, :cond_10

    if-eqz v3, :cond_11

    goto :goto_6

    :cond_10
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    :goto_6
    return v2

    :cond_11
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_12

    if-eqz v3, :cond_13

    goto :goto_7

    :cond_12
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    :goto_7
    return v2

    :cond_13
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v3

    if-nez v1, :cond_14

    if-eqz v3, :cond_15

    goto :goto_8

    :cond_14
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/activity/ModelActivityParty;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    :goto_8
    return v2

    :cond_15
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getFlags()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getFlags()I

    move-result v3

    if-eq v1, v3, :cond_16

    return v2

    :cond_16
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object v3

    if-nez v1, :cond_17

    if-eqz v3, :cond_18

    goto :goto_9

    :cond_17
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    :goto_9
    return v2

    :cond_18
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSyncId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getSyncId()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_19

    if-eqz v3, :cond_1a

    goto :goto_a

    :cond_19
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    :goto_a
    return v2

    :cond_1a
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getMetadata()Lcom/discord/models/domain/activity/ModelActivityMetadata;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getMetadata()Lcom/discord/models/domain/activity/ModelActivityMetadata;

    move-result-object v3

    if-nez v1, :cond_1b

    if-eqz v3, :cond_1c

    goto :goto_b

    :cond_1b
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/activity/ModelActivityMetadata;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    :goto_b
    return v2

    :cond_1c
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getPlatform()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getPlatform()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_1d

    if-eqz v3, :cond_1e

    goto :goto_c

    :cond_1d
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    :goto_c
    return v2

    :cond_1e
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSupportedPlatforms()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getSupportedPlatforms()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_1f

    if-eqz v3, :cond_20

    goto :goto_d

    :cond_1f
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_20

    :goto_d
    return v2

    :cond_20
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getButtons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getButtons()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_21

    if-eqz v3, :cond_22

    goto :goto_e

    :cond_21
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    :goto_e
    return v2

    :cond_22
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getCreatedAt()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getCreatedAt()Ljava/lang/Long;

    move-result-object p1

    if-nez v1, :cond_23

    if-eqz p1, :cond_24

    goto :goto_f

    :cond_23
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_24

    :goto_f
    return v2

    :cond_24
    return v0
.end method

.method public getApplicationId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->applicationId:Ljava/lang/Long;

    return-object v0
.end method

.method public getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->assets:Lcom/discord/models/domain/activity/ModelActivityAssets;

    return-object v0
.end method

.method public getButtons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->buttons:Ljava/util/List;

    return-object v0
.end method

.method public getCreatedAt()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->createdAt:Ljava/lang/Long;

    return-object v0
.end method

.method public getDetails()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->details:Ljava/lang/String;

    return-object v0
.end method

.method public getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->flags:I

    return v0
.end method

.method public getGamePlatform()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->isGameActivity()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->isXboxActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "xbox"

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    const-string v0, "desktop"

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMetadata()Lcom/discord/models/domain/activity/ModelActivityMetadata;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->metadata:Lcom/discord/models/domain/activity/ModelActivityMetadata;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParty()Lcom/discord/models/domain/activity/ModelActivityParty;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->party:Lcom/discord/models/domain/activity/ModelActivityParty;

    return-object v0
.end method

.method public getPlatform()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->isXboxActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "xbox"

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedPlatforms()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->supportedPlatforms:Ljava/util/List;

    return-object v0
.end method

.method public getSyncId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->syncId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->timestamps:Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->url:Ljava/lang/String;

    return-object v0
.end method

.method public hasFlag(I)Z
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->flags:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2b

    if-nez v0, :cond_0

    const/16 v0, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    const/16 v2, 0x3b

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_1

    const/16 v0, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getUrl()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_2

    const/16 v0, 0x2b

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_3

    const/16 v0, 0x2b

    goto :goto_3

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_4

    const/16 v0, 0x2b

    goto :goto_4

    :cond_4
    invoke-virtual {v0}, Lcom/discord/models/domain/activity/ModelActivityAssets;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_5

    const/16 v0, 0x2b

    goto :goto_5

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_6

    const/16 v0, 0x2b

    goto :goto_6

    :cond_6
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_7

    const/16 v0, 0x2b

    goto :goto_7

    :cond_7
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v0

    mul-int/lit8 v3, v3, 0x3b

    if-nez v0, :cond_8

    const/16 v0, 0x2b

    goto :goto_8

    :cond_8
    invoke-virtual {v0}, Lcom/discord/models/domain/activity/ModelActivityParty;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x3b

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getFlags()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_9

    const/16 v3, 0x2b

    goto :goto_9

    :cond_9
    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->hashCode()I

    move-result v3

    :goto_9
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSyncId()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_a

    const/16 v3, 0x2b

    goto :goto_a

    :cond_a
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_a
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getMetadata()Lcom/discord/models/domain/activity/ModelActivityMetadata;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_b

    const/16 v3, 0x2b

    goto :goto_b

    :cond_b
    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivityMetadata;->hashCode()I

    move-result v3

    :goto_b
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getPlatform()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_c

    const/16 v3, 0x2b

    goto :goto_c

    :cond_c
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_c
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSupportedPlatforms()Ljava/util/List;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_d

    const/16 v3, 0x2b

    goto :goto_d

    :cond_d
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_d
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getButtons()Ljava/util/List;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_e

    const/16 v3, 0x2b

    goto :goto_e

    :cond_e
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_e
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getCreatedAt()Ljava/lang/Long;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_f

    goto :goto_f

    :cond_f
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_f
    add-int/2addr v0, v1

    return v0
.end method

.method public isCurrentPlatform()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    const-string v1, "android"

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->supportedPlatforms:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isCustomStatus()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isGameActivity()Z
    .locals 1

    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isGamePlatform()Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->isXboxActivity()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isRichPresence()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->isCustomStatus()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->details:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->assets:Lcom/discord/models/domain/activity/ModelActivityAssets;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->party:Lcom/discord/models/domain/activity/ModelActivityParty;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->state:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->details:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isStreaming()Z
    .locals 2

    iget v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isXboxActivity()Z
    .locals 5

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->platform:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "xbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivity;->applicationId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v2, 0x614867e6c420000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ModelActivity(name="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", sessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", applicationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", assets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", emoji="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", party="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", timestamps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", syncId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSyncId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getMetadata()Lcom/discord/models/domain/activity/ModelActivityMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", platform="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getPlatform()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", supportedPlatforms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getSupportedPlatforms()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", buttons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getButtons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivity;->getCreatedAt()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
