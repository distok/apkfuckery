.class public Lcom/discord/models/domain/activity/ModelActivityParty;
.super Ljava/lang/Object;
.source "ModelActivityParty.java"

# interfaces
.implements Lcom/discord/models/domain/Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/domain/activity/ModelActivityParty$Size;
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public assignField(Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "size"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Lf/a/d/a/q1/a;

    invoke-direct {v0, p1}, Lf/a/d/a/q1/a;-><init>(Lcom/discord/models/domain/Model$JsonReader;)V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    invoke-direct {v1, v0}, Lcom/discord/models/domain/activity/ModelActivityParty$Size;-><init>(Ljava/util/List;)V

    iput-object v1, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v0, Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    invoke-direct {v0}, Lcom/discord/models/domain/activity/ModelActivityParty$Size;-><init>()V

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->id:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/models/domain/activity/ModelActivityParty;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/models/domain/activity/ModelActivityParty;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/models/domain/activity/ModelActivityParty;

    invoke-virtual {p1, p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityParty;->getId()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    return v2

    :cond_4
    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->getSize()Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityParty;->getSize()Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    move-result-object p1

    if-nez v1, :cond_5

    if-eqz p1, :cond_6

    goto :goto_1

    :cond_5
    invoke-virtual {v1, p1}, Lcom/discord/models/domain/activity/ModelActivityParty$Size;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    :goto_1
    return v2

    :cond_6
    return v0
.end method

.method public getCurrentSize()J
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/discord/models/domain/activity/ModelActivityParty$Size;->current:J

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxSize()J
    .locals 2

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/discord/models/domain/activity/ModelActivityParty$Size;->max:J

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public getOpenSlots()J
    .locals 5

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    if-eqz v0, :cond_0

    iget-wide v1, v0, Lcom/discord/models/domain/activity/ModelActivityParty$Size;->max:J

    iget-wide v3, v0, Lcom/discord/models/domain/activity/ModelActivityParty$Size;->current:J

    sub-long/2addr v1, v3

    return-wide v1

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getSize()Lcom/discord/models/domain/activity/ModelActivityParty$Size;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/activity/ModelActivityParty;->size:Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->getId()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2b

    if-nez v0, :cond_0

    const/16 v0, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    const/16 v2, 0x3b

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->getSize()Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivityParty$Size;->hashCode()I

    move-result v1

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ModelActivityParty(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/models/domain/activity/ModelActivityParty;->getSize()Lcom/discord/models/domain/activity/ModelActivityParty$Size;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
