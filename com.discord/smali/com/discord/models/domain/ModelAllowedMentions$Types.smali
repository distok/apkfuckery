.class public final enum Lcom/discord/models/domain/ModelAllowedMentions$Types;
.super Ljava/lang/Enum;
.source "ModelAllowedMentions.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/domain/ModelAllowedMentions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Types"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/domain/ModelAllowedMentions$Types;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/domain/ModelAllowedMentions$Types;

.field public static final enum EVERYONE:Lcom/discord/models/domain/ModelAllowedMentions$Types;

.field public static final enum ROLES:Lcom/discord/models/domain/ModelAllowedMentions$Types;

.field public static final enum USERS:Lcom/discord/models/domain/ModelAllowedMentions$Types;


# instance fields
.field private final apiStringRepresentation:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/models/domain/ModelAllowedMentions$Types;

    new-instance v1, Lcom/discord/models/domain/ModelAllowedMentions$Types;

    const-string v2, "USERS"

    const/4 v3, 0x0

    const-string/jumbo v4, "users"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelAllowedMentions$Types;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelAllowedMentions$Types;->USERS:Lcom/discord/models/domain/ModelAllowedMentions$Types;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelAllowedMentions$Types;

    const-string v2, "ROLES"

    const/4 v3, 0x1

    const-string v4, "roles"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelAllowedMentions$Types;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelAllowedMentions$Types;->ROLES:Lcom/discord/models/domain/ModelAllowedMentions$Types;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/domain/ModelAllowedMentions$Types;

    const-string v2, "EVERYONE"

    const/4 v3, 0x2

    const-string v4, "everyone"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/domain/ModelAllowedMentions$Types;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/models/domain/ModelAllowedMentions$Types;->EVERYONE:Lcom/discord/models/domain/ModelAllowedMentions$Types;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/models/domain/ModelAllowedMentions$Types;->$VALUES:[Lcom/discord/models/domain/ModelAllowedMentions$Types;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/models/domain/ModelAllowedMentions$Types;->apiStringRepresentation:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/domain/ModelAllowedMentions$Types;
    .locals 1

    const-class v0, Lcom/discord/models/domain/ModelAllowedMentions$Types;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/domain/ModelAllowedMentions$Types;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/domain/ModelAllowedMentions$Types;
    .locals 1

    sget-object v0, Lcom/discord/models/domain/ModelAllowedMentions$Types;->$VALUES:[Lcom/discord/models/domain/ModelAllowedMentions$Types;

    invoke-virtual {v0}, [Lcom/discord/models/domain/ModelAllowedMentions$Types;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/domain/ModelAllowedMentions$Types;

    return-object v0
.end method


# virtual methods
.method public final serialize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/domain/ModelAllowedMentions$Types;->apiStringRepresentation:Ljava/lang/String;

    return-object v0
.end method
