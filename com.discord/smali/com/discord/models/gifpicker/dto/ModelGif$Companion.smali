.class public final Lcom/discord/models/gifpicker/dto/ModelGif$Companion;
.super Ljava/lang/Object;
.source "ModelGif.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/gifpicker/dto/ModelGif;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/gifpicker/dto/ModelGif$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromGifDto(Lcom/discord/models/gifpicker/dto/GifDto;)Lcom/discord/models/gifpicker/dto/ModelGif;
    .locals 4

    const-string v0, "gifDto"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/gifpicker/dto/ModelGif;

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/GifDto;->getSrc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/GifDto;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/GifDto;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/GifDto;->getHeight()I

    move-result p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/discord/models/gifpicker/dto/ModelGif;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v0
.end method
