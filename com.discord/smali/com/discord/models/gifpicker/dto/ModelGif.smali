.class public final Lcom/discord/models/gifpicker/dto/ModelGif;
.super Ljava/lang/Object;
.source "ModelGif.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/gifpicker/dto/ModelGif$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/models/gifpicker/dto/ModelGif$Companion;


# instance fields
.field private final gifImageUrl:Ljava/lang/String;

.field private final height:I

.field private final tenorGifUrl:Ljava/lang/String;

.field private final width:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/gifpicker/dto/ModelGif$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/gifpicker/dto/ModelGif$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/gifpicker/dto/ModelGif;->Companion:Lcom/discord/models/gifpicker/dto/ModelGif$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    const-string v0, "gifImageUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenorGifUrl"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    iput p3, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    iput p4, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/gifpicker/dto/ModelGif;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/Object;)Lcom/discord/models/gifpicker/dto/ModelGif;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/models/gifpicker/dto/ModelGif;->copy(Ljava/lang/String;Ljava/lang/String;II)Lcom/discord/models/gifpicker/dto/ModelGif;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;II)Lcom/discord/models/gifpicker/dto/ModelGif;
    .locals 1

    const-string v0, "gifImageUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenorGifUrl"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/gifpicker/dto/ModelGif;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/models/gifpicker/dto/ModelGif;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/gifpicker/dto/ModelGif;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/gifpicker/dto/ModelGif;

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    iget v1, p1, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    iget p1, p1, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGifImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getHeight()I
    .locals 1

    iget v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    return v0
.end method

.method public final getTenorGifUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getWidth()I
    .locals 1

    iget v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelGif(gifImageUrl="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->gifImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tenorGifUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->tenorGifUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/gifpicker/dto/ModelGif;->height:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
