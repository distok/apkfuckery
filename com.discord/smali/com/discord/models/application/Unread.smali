.class public final Lcom/discord/models/application/Unread;
.super Ljava/lang/Object;
.source "Unread.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/application/Unread$Marker;,
        Lcom/discord/models/application/Unread$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/models/application/Unread$Companion;


# instance fields
.field private final count:I

.field private final marker:Lcom/discord/models/application/Unread$Marker;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/application/Unread$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/application/Unread$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/application/Unread;->Companion:Lcom/discord/models/application/Unread$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2, v0}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/application/Unread$Marker;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/application/Unread$Marker;I)V
    .locals 1

    const-string v0, "marker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    iput p2, p0, Lcom/discord/models/application/Unread;->count:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/application/Unread$Marker;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    new-instance p1, Lcom/discord/models/application/Unread$Marker;

    invoke-direct {p1}, Lcom/discord/models/application/Unread$Marker;-><init>()V

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;I)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/application/Unread$Marker;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/application/Unread$Marker;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "marker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/application/Unread;->Companion:Lcom/discord/models/application/Unread$Companion;

    invoke-static {v0, p1, p2}, Lcom/discord/models/application/Unread$Companion;->access$getTotalMessages(Lcom/discord/models/application/Unread$Companion;Lcom/discord/models/application/Unread$Marker;Ljava/util/List;)I

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/application/Unread;Lcom/discord/models/application/Unread$Marker;IILjava/lang/Object;)Lcom/discord/models/application/Unread;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/models/application/Unread;->count:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/models/application/Unread;->copy(Lcom/discord/models/application/Unread$Marker;I)Lcom/discord/models/application/Unread;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/application/Unread$Marker;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/models/application/Unread;->count:I

    return v0
.end method

.method public final copy(Lcom/discord/models/application/Unread$Marker;I)Lcom/discord/models/application/Unread;
    .locals 1

    const-string v0, "marker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/application/Unread;

    invoke-direct {v0, p1, p2}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;I)V

    return-object v0
.end method

.method public final createWithEmptyCount()Lcom/discord/models/application/Unread;
    .locals 3

    new-instance v0, Lcom/discord/models/application/Unread;

    iget-object v1, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/application/Unread;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/application/Unread;

    iget-object v0, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    iget-object v1, p1, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/models/application/Unread;->count:I

    iget p1, p1, Lcom/discord/models/application/Unread;->count:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/discord/models/application/Unread;->count:I

    return v0
.end method

.method public final getMarker()Lcom/discord/models/application/Unread$Marker;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/application/Unread$Marker;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/models/application/Unread;->count:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Unread(marker="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/application/Unread;->marker:Lcom/discord/models/application/Unread$Marker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/application/Unread;->count:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
