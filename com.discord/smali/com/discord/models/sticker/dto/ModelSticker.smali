.class public final Lcom/discord/models/sticker/dto/ModelSticker;
.super Ljava/lang/Object;
.source "ModelSticker.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/sticker/dto/ModelSticker$Type;,
        Lcom/discord/models/sticker/dto/ModelSticker$Parser;
    }
.end annotation


# static fields
.field public static final Parser:Lcom/discord/models/sticker/dto/ModelSticker$Parser;


# instance fields
.field private final asset:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final formatType:Ljava/lang/Integer;

.field private final id:J

.field private final name:Ljava/lang/String;

.field private final packId:J

.field private final tags:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/models/sticker/dto/ModelSticker$Parser;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/models/sticker/dto/ModelSticker$Parser;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/models/sticker/dto/ModelSticker;->Parser:Lcom/discord/models/sticker/dto/ModelSticker$Parser;

    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    iput-wide p3, p0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    iput-object p5, p0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    iput-object p8, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    iput-object p9, p0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/sticker/dto/ModelSticker;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 10

    move-object v0, p0

    and-int/lit8 v1, p10, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p10, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    goto :goto_1

    :cond_1
    move-wide v3, p3

    :goto_1
    and-int/lit8 v5, p10, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v5, p5

    :goto_2
    and-int/lit8 v6, p10, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object/from16 v6, p6

    :goto_3
    and-int/lit8 v7, p10, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p7

    :goto_4
    and-int/lit8 v8, p10, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p8

    :goto_5
    and-int/lit8 v9, p10, 0x40

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p9

    :goto_6
    move-wide p1, v1

    move-wide p3, v3

    move-object p5, v5

    move-object/from16 p6, v6

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    move-object/from16 p9, v9

    invoke-virtual/range {p0 .. p9}, Lcom/discord/models/sticker/dto/ModelSticker;->copy(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    sget-object v0, Lcom/discord/models/sticker/dto/ModelSticker;->Parser:Lcom/discord/models/sticker/dto/ModelSticker$Parser;

    invoke-virtual {v0, p0}, Lcom/discord/models/sticker/dto/ModelSticker$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    return-wide v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 11

    const-string v0, "name"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/sticker/dto/ModelSticker;

    move-object v1, v0

    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/discord/models/sticker/dto/ModelSticker;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/sticker/dto/ModelSticker;

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAsset()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    return-object v0
.end method

.method public final getAssetUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const-string v0, ".json"

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_1
    const-string v0, ".png"

    goto :goto_0

    :cond_2
    const-string v0, ""

    :goto_0
    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    invoke-static {v1, v0}, Lx/m/c/j;->stringPlus(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getFormatType()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    return-wide v0
.end method

.method public final getTags()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;
    .locals 5

    invoke-static {}, Lcom/discord/models/sticker/dto/ModelSticker$Type;->values()[Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_2

    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelSticker$Type;->getValue()I

    move-result v3

    iget-object v4, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_1

    return-object v2

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/discord/models/sticker/dto/ModelSticker$Type;->UNKNOWN:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v1, v2

    return v1
.end method

.method public final isAnimated()Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->APNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/discord/models/sticker/dto/ModelSticker;->getType()Lcom/discord/models/sticker/dto/ModelSticker$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->LOTTIE:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelSticker(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", packId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->packId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", asset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->asset:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", formatType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->formatType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelSticker;->tags:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
