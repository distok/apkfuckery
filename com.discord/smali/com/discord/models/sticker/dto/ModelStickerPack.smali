.class public final Lcom/discord/models/sticker/dto/ModelStickerPack;
.super Ljava/lang/Object;
.source "ModelStickerPack.kt"


# instance fields
.field private final coverStickerId:Ljava/lang/Long;

.field private final id:J

.field private final name:Ljava/lang/String;

.field private final skuId:J

.field private final stickers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation
.end field

.field private final storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;


# direct methods
.method public constructor <init>(JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;",
            "J",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "stickers"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    iput-object p3, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    iput-wide p6, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    iput-object p8, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/sticker/dto/ModelStickerPack;JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 9

    move-object v0, p0

    and-int/lit8 v1, p9, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p9, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v3, p3

    :goto_1
    and-int/lit8 v4, p9, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v4, p4

    :goto_2
    and-int/lit8 v5, p9, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    goto :goto_3

    :cond_3
    move-object v5, p5

    :goto_3
    and-int/lit8 v6, p9, 0x10

    if-eqz v6, :cond_4

    iget-wide v6, v0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    goto :goto_4

    :cond_4
    move-wide v6, p6

    :goto_4
    and-int/lit8 v8, p9, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p8

    :goto_5
    move-wide p1, v1

    move-object p3, v3

    move-object p4, v4

    move-object p5, v5

    move-wide p6, v6

    move-object/from16 p8, v8

    invoke-virtual/range {p0 .. p8}, Lcom/discord/models/sticker/dto/ModelStickerPack;->copy(JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;)Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final canBePurchased()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSku;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    return-wide v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    return-object v0
.end method

.method public final component5()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    return-wide v0
.end method

.method public final component6()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;)Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;",
            "J",
            "Ljava/lang/Long;",
            ")",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;"
        }
    .end annotation

    const-string/jumbo v0, "stickers"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-object v1, v0

    move-wide v2, p1

    move-object v6, p5

    move-wide/from16 v7, p6

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/discord/models/sticker/dto/ModelStickerPack;-><init>(JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCoverSticker()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 8

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    check-cast v1, Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/models/sticker/dto/ModelSticker;

    :goto_3
    return-object v1
.end method

.method public final getCoverStickerId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getSkuId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    return-wide v0
.end method

.method public final getStickers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    return-object v0
.end method

.method public final getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v4, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    ushr-long v6, v4, v2

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_3
    add-int/2addr v1, v3

    return v1
.end method

.method public final isAnimatedPack()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelSticker;->isAnimated()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    return v2
.end method

.method public final isLimitedPack()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getUnpublishedAt()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final isPremiumPack()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSku;->getPremium()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelStickerPack(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", stickers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->stickers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", storeListing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->storeListing:Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skuId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->skuId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", coverStickerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelStickerPack;->coverStickerId:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
