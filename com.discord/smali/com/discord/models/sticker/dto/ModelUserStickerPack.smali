.class public final Lcom/discord/models/sticker/dto/ModelUserStickerPack;
.super Ljava/lang/Object;
.source "ModelUserStickerPack.kt"


# instance fields
.field private final entitlementId:J

.field private final hasAccess:Z

.field private final packId:J

.field private final premiumTypeRequired:Ljava/lang/Integer;

.field private final stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

.field private final userId:J


# direct methods
.method public constructor <init>(JJJZLjava/lang/Integer;Lcom/discord/models/sticker/dto/ModelStickerPack;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    iput-wide p3, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    iput-wide p5, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    iput-boolean p7, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    iput-object p8, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    iput-object p9, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/sticker/dto/ModelUserStickerPack;JJJZLjava/lang/Integer;Lcom/discord/models/sticker/dto/ModelStickerPack;ILjava/lang/Object;)Lcom/discord/models/sticker/dto/ModelUserStickerPack;
    .locals 10

    move-object v0, p0

    and-int/lit8 v1, p10, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p10, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    goto :goto_1

    :cond_1
    move-wide v3, p3

    :goto_1
    and-int/lit8 v5, p10, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    goto :goto_2

    :cond_2
    move-wide v5, p5

    :goto_2
    and-int/lit8 v7, p10, 0x8

    if-eqz v7, :cond_3

    iget-boolean v7, v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    goto :goto_3

    :cond_3
    move/from16 v7, p7

    :goto_3
    and-int/lit8 v8, p10, 0x10

    if-eqz v8, :cond_4

    iget-object v8, v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    goto :goto_4

    :cond_4
    move-object/from16 v8, p8

    :goto_4
    and-int/lit8 v9, p10, 0x20

    if-eqz v9, :cond_5

    iget-object v9, v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    goto :goto_5

    :cond_5
    move-object/from16 v9, p9

    :goto_5
    move-wide p1, v1

    move-wide p3, v3

    move-wide p5, v5

    move/from16 p7, v7

    move-object/from16 p8, v8

    move-object/from16 p9, v9

    invoke-virtual/range {p0 .. p9}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->copy(JJJZLjava/lang/Integer;Lcom/discord/models/sticker/dto/ModelStickerPack;)Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    return-wide v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    return v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component6()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public final copy(JJJZLjava/lang/Integer;Lcom/discord/models/sticker/dto/ModelStickerPack;)Lcom/discord/models/sticker/dto/ModelUserStickerPack;
    .locals 11

    new-instance v10, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    move-object v0, v10

    move-wide v1, p1

    move-wide v3, p3

    move-wide/from16 v5, p5

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;-><init>(JJJZLjava/lang/Integer;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    return-object v10
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    iget-wide v2, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-boolean v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    iget-boolean v1, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-object p1, p1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEntitlementId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    return-wide v0
.end method

.method public final getHasAccess()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    return v0
.end method

.method public final getPackId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    return-wide v0
.end method

.method public final getPremiumTypeRequired()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public final getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    iget-wide v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    ushr-long v5, v3, v2

    xor-long/2addr v3, v5

    long-to-int v0, v3

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelUserStickerPack(userId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->userId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", packId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->packId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", entitlementId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->entitlementId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", hasAccess="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->hasAccess:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", premiumTypeRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->premiumTypeRequired:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerPack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
