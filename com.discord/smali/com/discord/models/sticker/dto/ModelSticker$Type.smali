.class public final enum Lcom/discord/models/sticker/dto/ModelSticker$Type;
.super Ljava/lang/Enum;
.source "ModelSticker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/sticker/dto/ModelSticker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/sticker/dto/ModelSticker$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/sticker/dto/ModelSticker$Type;

.field public static final enum APNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

.field public static final enum LOTTIE:Lcom/discord/models/sticker/dto/ModelSticker$Type;

.field public static final enum PNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

.field public static final enum UNKNOWN:Lcom/discord/models/sticker/dto/ModelSticker$Type;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/models/sticker/dto/ModelSticker$Type;

    new-instance v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/sticker/dto/ModelSticker$Type;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->UNKNOWN:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const-string v2, "PNG"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lcom/discord/models/sticker/dto/ModelSticker$Type;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->PNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const-string v2, "APNG"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v3}, Lcom/discord/models/sticker/dto/ModelSticker$Type;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->APNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const-string v2, "LOTTIE"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v3}, Lcom/discord/models/sticker/dto/ModelSticker$Type;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/sticker/dto/ModelSticker$Type;->LOTTIE:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/models/sticker/dto/ModelSticker$Type;->$VALUES:[Lcom/discord/models/sticker/dto/ModelSticker$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/models/sticker/dto/ModelSticker$Type;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/sticker/dto/ModelSticker$Type;
    .locals 1

    const-class v0, Lcom/discord/models/sticker/dto/ModelSticker$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/sticker/dto/ModelSticker$Type;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/sticker/dto/ModelSticker$Type;
    .locals 1

    sget-object v0, Lcom/discord/models/sticker/dto/ModelSticker$Type;->$VALUES:[Lcom/discord/models/sticker/dto/ModelSticker$Type;

    invoke-virtual {v0}, [Lcom/discord/models/sticker/dto/ModelSticker$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/sticker/dto/ModelSticker$Type;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    iget v0, p0, Lcom/discord/models/sticker/dto/ModelSticker$Type;->value:I

    return v0
.end method
