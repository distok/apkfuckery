.class public final synthetic Lcom/discord/models/sticker/dto/ModelSticker$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/discord/models/sticker/dto/ModelSticker$Type;->values()[Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v0, 0x4

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/models/sticker/dto/ModelSticker$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/models/sticker/dto/ModelSticker$Type;->PNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v2, 0x1

    aput v2, v1, v2

    sget-object v2, Lcom/discord/models/sticker/dto/ModelSticker$Type;->APNG:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v2, 0x2

    aput v2, v1, v2

    sget-object v2, Lcom/discord/models/sticker/dto/ModelSticker$Type;->LOTTIE:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v2, 0x3

    aput v2, v1, v2

    sget-object v2, Lcom/discord/models/sticker/dto/ModelSticker$Type;->UNKNOWN:Lcom/discord/models/sticker/dto/ModelSticker$Type;

    const/4 v2, 0x0

    aput v0, v1, v2

    return-void
.end method
