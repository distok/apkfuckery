.class public final Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;
.super Ljava/lang/Object;
.source "ModelGatewayGuildApplicationCommands.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser;
    }
.end annotation


# instance fields
.field private final applications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation
.end field

.field private final gatewayApplicationCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;",
            ">;"
        }
    .end annotation
.end field

.field private final guildId:J

.field private final nonce:Ljava/lang/String;

.field private final updatedAt:J


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;JLjava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;J",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    const-string v0, "gatewayApplicationCommands"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applications"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nonce"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    iput-wide p3, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    iput-object p5, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    iput-wide p6, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;Ljava/util/List;Ljava/util/List;JLjava/lang/String;JILjava/lang/Object;)Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    :cond_3
    move-object v2, p5

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-wide p6, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    :cond_4
    move-wide v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-wide p5, v0

    move-object p7, v2

    move-wide p8, v3

    invoke-virtual/range {p2 .. p9}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->copy(Ljava/util/List;Ljava/util/List;JLjava/lang/String;J)Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    return-wide v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    return-wide v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;JLjava/lang/String;J)Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;J",
            "Ljava/lang/String;",
            "J)",
            "Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;"
        }
    .end annotation

    const-string v0, "gatewayApplicationCommands"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applications"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nonce"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;

    move-object v1, v0

    move-wide v4, p3

    move-wide v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;-><init>(Ljava/util/List;Ljava/util/List;JLjava/lang/String;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    iget-wide v2, p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    iget-wide v2, p1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    return-object v0
.end method

.method public final getGatewayApplicationCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    return-object v0
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    return-wide v0
.end method

.method public final getNonce()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public final getUpdatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    const/16 v4, 0x20

    ushr-long v5, v2, v4

    xor-long/2addr v2, v5

    long-to-int v3, v2

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    ushr-long v3, v1, v4

    xor-long/2addr v1, v3

    long-to-int v2, v1

    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ModelGatewayGuildApplicationCommands(gatewayApplicationCommands="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->gatewayApplicationCommands:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", applications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->applications:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", nonce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->nonce:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", updatedAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->updatedAt:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
