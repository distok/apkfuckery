.class public final Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOptionKt;
.super Ljava/lang/Object;
.source "ModelGatewayApplicationCommandOption.kt"


# direct methods
.method public static final synthetic access$toCommandType(Ljava/lang/Integer;)Lcom/discord/models/slashcommands/ApplicationCommandType;
    .locals 0

    invoke-static {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOptionKt;->toCommandType(Ljava/lang/Integer;)Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object p0

    return-object p0
.end method

.method private static final toCommandType(Ljava/lang/Integer;)Lcom/discord/models/slashcommands/ApplicationCommandType;
    .locals 2

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->SUBCOMMAND:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto/16 :goto_8

    :cond_1
    :goto_0
    if-nez p0, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->SUBCOMMAND_GROUP:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_3
    :goto_1
    if-nez p0, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->STRING:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_5
    :goto_2
    if-nez p0, :cond_6

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->INTEGER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_7
    :goto_3
    if-nez p0, :cond_8

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_9

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->BOOLEAN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_9
    :goto_4
    if-nez p0, :cond_a

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_b

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->USER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_b
    :goto_5
    if-nez p0, :cond_c

    goto :goto_6

    :cond_c
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_d

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->CHANNEL:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_d
    :goto_6
    if-nez p0, :cond_e

    goto :goto_7

    :cond_e
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_f

    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->ROLE:Lcom/discord/models/slashcommands/ApplicationCommandType;

    goto :goto_8

    :cond_f
    :goto_7
    sget-object p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->UNKNOWN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    :goto_8
    return-object p0
.end method
