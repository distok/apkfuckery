.class public final Lcom/discord/models/slashcommands/CommandChoices$Parser;
.super Ljava/lang/Object;
.source "ModelGatewayApplicationCommandOption.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$Parser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/slashcommands/CommandChoices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$Parser<",
        "Lcom/discord/models/slashcommands/CommandChoices;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/slashcommands/CommandChoices$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/slashcommands/CommandChoices$Parser;

    invoke-direct {v0}, Lcom/discord/models/slashcommands/CommandChoices$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/slashcommands/CommandChoices$Parser;->INSTANCE:Lcom/discord/models/slashcommands/CommandChoices$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/slashcommands/CommandChoices;
    .locals 3

    const-string v0, "reader"

    invoke-static {p1, v0}, Lf/e/c/a/a;->N(Lcom/discord/models/domain/Model$JsonReader;Ljava/lang/String;)Lkotlin/jvm/internal/Ref$ObjectRef;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v1, Lcom/discord/models/slashcommands/CommandChoices$Parser$parse$1;

    invoke-direct {v1, v0, p1, v2}, Lcom/discord/models/slashcommands/CommandChoices$Parser$parse$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance p1, Lcom/discord/models/slashcommands/CommandChoices;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-direct {p1, v0, v1}, Lcom/discord/models/slashcommands/CommandChoices;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic parse(Lcom/discord/models/domain/Model$JsonReader;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/models/slashcommands/CommandChoices$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/slashcommands/CommandChoices;

    move-result-object p1

    return-object p1
.end method
