.class public final Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;
.super Ljava/lang/Object;
.source "ModelGatewayGuildApplicationCommands.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $applications:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $gatewayApplicationCommands:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $guildId:Lkotlin/jvm/internal/Ref$LongRef;

.field public final synthetic $nonce:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $reader:Lcom/discord/models/domain/Model$JsonReader;

.field public final synthetic $updatedAt:Lkotlin/jvm/internal/Ref$LongRef;


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$LongRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$LongRef;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$gatewayApplicationCommands:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p2, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    iput-object p3, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$applications:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p4, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$guildId:Lkotlin/jvm/internal/Ref$LongRef;

    iput-object p5, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$nonce:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p6, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$updatedAt:Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    goto/16 :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const-wide/16 v1, 0x0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "application_commands"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$gatewayApplicationCommands:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    new-instance v1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1$1;

    invoke-direct {v1, p0}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1$1;-><init>(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;)V

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object v0

    const-string v1, "reader.nextList { ModelG\u2026nd.Parser.parse(reader) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_1

    :sswitch_1
    const-string v0, "applications"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$applications:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    new-instance v1, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1$2;

    invoke-direct {v1, p0}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1$2;-><init>(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;)V

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    move-result-object v0

    const-string v1, "reader.nextList { ModelA\u2026on.Parser.parse(reader) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_1

    :sswitch_2
    const-string v0, "nonce"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$nonce:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "reader.nextString(\"\")"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_1

    :sswitch_3
    const-string/jumbo v0, "updated_at"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$updatedAt:Lkotlin/jvm/internal/Ref$LongRef;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0, v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    iput-wide v0, p1, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    goto :goto_1

    :sswitch_4
    const-string v0, "guild_id"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$guildId:Lkotlin/jvm/internal/Ref$LongRef;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0, v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextLong(J)J

    move-result-wide v0

    iput-wide v0, p1, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4de03319 -> :sswitch_4
        -0x119c6dc9 -> :sswitch_3
        0x64237ef -> :sswitch_2
        0x37dca523 -> :sswitch_1
        0x43803097 -> :sswitch_0
    .end sparse-switch
.end method
