.class public final Lcom/discord/models/slashcommands/ModelApplicationComparator$Companion;
.super Ljava/lang/Object;
.source "ModelApplicationComparator.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/slashcommands/ModelApplicationComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/discord/models/slashcommands/ModelApplication;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/slashcommands/ModelApplicationComparator$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/models/slashcommands/ModelApplication;)I
    .locals 2

    const-string v0, "a"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "b"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/slashcommands/ModelApplication;->getBuiltIn()Z

    move-result v0

    invoke-virtual {p2}, Lcom/discord/models/slashcommands/ModelApplication;->getBuiltIn()Z

    move-result v1

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/slashcommands/ModelApplication;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/models/slashcommands/ModelApplication;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/discord/models/slashcommands/ModelApplication;

    check-cast p2, Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {p0, p1, p2}, Lcom/discord/models/slashcommands/ModelApplicationComparator$Companion;->compare(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/models/slashcommands/ModelApplication;)I

    move-result p1

    return p1
.end method
