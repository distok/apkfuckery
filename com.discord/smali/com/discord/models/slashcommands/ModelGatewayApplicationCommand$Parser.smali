.class public final Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;
.super Ljava/lang/Object;
.source "ModelGatewayApplicationCommand.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$Parser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$Parser<",
        "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;

    invoke-direct {v0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;->INSTANCE:Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;
    .locals 15

    move-object/from16 v7, p1

    const-string v0, "reader"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v8}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, v8, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    new-instance v9, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v9}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    iput-wide v0, v9, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    new-instance v10, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v10}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v11, 0x0

    iput-object v11, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v12, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v12}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v11, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v13, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v13}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    iput-object v0, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v14, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser$parse$1;

    move-object v0, v14

    move-object v1, v8

    move-object/from16 v2, p1

    move-object v3, v9

    move-object v4, v10

    move-object v5, v12

    move-object v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser$parse$1;-><init>(Lkotlin/jvm/internal/Ref$LongRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$LongRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {v7, v14}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance v14, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    iget-wide v1, v8, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    iget-wide v3, v9, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    iget-object v0, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-eqz v0, :cond_1

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    iget-object v0, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-eqz v0, :cond_0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    iget-object v0, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Ljava/util/List;

    move-object v0, v14

    invoke-direct/range {v0 .. v7}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v14

    :cond_0
    const-string v0, "description"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v11

    :cond_1
    const-string v0, "name"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v11
.end method

.method public bridge synthetic parse(Lcom/discord/models/domain/Model$JsonReader;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    move-result-object p1

    return-object p1
.end method
