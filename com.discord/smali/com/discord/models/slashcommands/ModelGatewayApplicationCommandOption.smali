.class public final Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;
.super Ljava/lang/Object;
.source "ModelGatewayApplicationCommandOption.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption$Parser;
    }
.end annotation


# instance fields
.field private final choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/CommandChoices;",
            ">;"
        }
    .end annotation
.end field

.field private final default:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final optionGateways:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;"
        }
    .end annotation
.end field

.field private final required:Z

.field private final type:Lcom/discord/models/slashcommands/ApplicationCommandType;


# direct methods
.method public constructor <init>(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/slashcommands/ApplicationCommandType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/CommandChoices;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    iput-object p2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    iput-object p5, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    iput-object p7, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object v7, p5

    :goto_1
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_2

    move-object v8, v1

    goto :goto_2

    :cond_2
    move-object/from16 v8, p6

    :goto_2
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_3

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p7

    :goto_3
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p4

    invoke-direct/range {v2 .. v9}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;-><init>(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->copy(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/slashcommands/ApplicationCommandType;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/CommandChoices;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    return-object v0
.end method

.method public final component7()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/slashcommands/ApplicationCommandType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/CommandChoices;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;)",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;"
        }
    .end annotation

    const-string/jumbo v0, "type"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;

    move-object v1, v0

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;-><init>(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    iget-boolean v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChoices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/CommandChoices;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    return-object v0
.end method

.method public final getDefault()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptionGateways()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    return-object v0
.end method

.method public final getRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    return v0
.end method

.method public final getType()Lcom/discord/models/slashcommands/ApplicationCommandType;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelGatewayApplicationCommandOption(type="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->type:Lcom/discord/models/slashcommands/ApplicationCommandType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->required:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->default:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", choices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->choices:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionGateways="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->optionGateways:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
