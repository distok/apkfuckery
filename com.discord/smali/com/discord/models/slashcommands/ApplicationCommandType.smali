.class public final enum Lcom/discord/models/slashcommands/ApplicationCommandType;
.super Ljava/lang/Enum;
.source "ModelGatewayApplicationCommandOption.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/models/slashcommands/ApplicationCommandType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum BOOLEAN:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum CHANNEL:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum INTEGER:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum ROLE:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum STRING:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum SUBCOMMAND:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum SUBCOMMAND_GROUP:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum UNKNOWN:Lcom/discord/models/slashcommands/ApplicationCommandType;

.field public static final enum USER:Lcom/discord/models/slashcommands/ApplicationCommandType;


# instance fields
.field private final type:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/discord/models/slashcommands/ApplicationCommandType;

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "SUBCOMMAND"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->SUBCOMMAND:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "SUBCOMMAND_GROUP"

    const/4 v5, 0x2

    invoke-direct {v1, v2, v4, v5}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->SUBCOMMAND_GROUP:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "STRING"

    const/4 v4, 0x3

    invoke-direct {v1, v2, v5, v4}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->STRING:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "INTEGER"

    const/4 v5, 0x4

    invoke-direct {v1, v2, v4, v5}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->INTEGER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "BOOLEAN"

    const/4 v4, 0x5

    invoke-direct {v1, v2, v5, v4}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->BOOLEAN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "USER"

    const/4 v5, 0x6

    invoke-direct {v1, v2, v4, v5}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->USER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "CHANNEL"

    const/4 v4, 0x7

    invoke-direct {v1, v2, v5, v4}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->CHANNEL:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "ROLE"

    const/16 v5, 0x8

    invoke-direct {v1, v2, v4, v5}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->ROLE:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "UNKNOWN"

    invoke-direct {v1, v2, v5, v3}, Lcom/discord/models/slashcommands/ApplicationCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->UNKNOWN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/models/slashcommands/ApplicationCommandType;->$VALUES:[Lcom/discord/models/slashcommands/ApplicationCommandType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->type:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/models/slashcommands/ApplicationCommandType;
    .locals 1

    const-class v0, Lcom/discord/models/slashcommands/ApplicationCommandType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/models/slashcommands/ApplicationCommandType;

    return-object p0
.end method

.method public static values()[Lcom/discord/models/slashcommands/ApplicationCommandType;
    .locals 1

    sget-object v0, Lcom/discord/models/slashcommands/ApplicationCommandType;->$VALUES:[Lcom/discord/models/slashcommands/ApplicationCommandType;

    invoke-virtual {v0}, [Lcom/discord/models/slashcommands/ApplicationCommandType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/models/slashcommands/ApplicationCommandType;

    return-object v0
.end method


# virtual methods
.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/discord/models/slashcommands/ApplicationCommandType;->type:I

    return v0
.end method
