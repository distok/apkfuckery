.class public final Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;
.super Ljava/lang/Object;
.source "ModelGatewayApplicationCommand.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand$Parser;
    }
.end annotation


# instance fields
.field private final applicationId:J

.field private final description:Ljava/lang/String;

.field private final id:J

.field private final name:Ljava/lang/String;

.field private final optionGateways:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionGateways"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    iput-wide p3, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    iput-object p5, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, p6

    :goto_0
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;
    .locals 8

    move-object v0, p0

    and-int/lit8 v1, p8, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p8, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    goto :goto_1

    :cond_1
    move-wide v3, p3

    :goto_1
    and-int/lit8 v5, p8, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v5, p5

    :goto_2
    and-int/lit8 v6, p8, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v6, p6

    :goto_3
    and-int/lit8 v7, p8, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    goto :goto_4

    :cond_4
    move-object v7, p7

    :goto_4
    move-wide p1, v1

    move-wide p3, v3

    move-object p5, v5

    move-object p6, v6

    move-object p7, v7

    invoke-virtual/range {p0 .. p7}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->copy(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    return-wide v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    return-object v0
.end method

.method public final copy(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;)",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;"
        }
    .end annotation

    const-string v0, "name"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionGateways"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    move-object v1, v0

    move-wide v2, p1

    move-wide v4, p3

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    iget-wide v2, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    iget-wide v2, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplicationId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    return-wide v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptionGateways()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    iget-wide v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ModelGatewayApplicationCommand(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", applicationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->applicationId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", optionGateways="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->optionGateways:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
