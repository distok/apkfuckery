.class public final Lcom/discord/models/experiments/domain/ExperimentHash;
.super Ljava/lang/Object;
.source "ExperimentHash.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/domain/ExperimentHash;

    invoke-direct {v0}, Lcom/discord/models/experiments/domain/ExperimentHash;-><init>()V

    sput-object v0, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final from(Ljava/lang/CharSequence;)J
    .locals 4

    const-string v0, "name"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v1}, Ly/a/g0;->w(Ljava/lang/CharSequence;III)I

    move-result p1

    if-gez p1, :cond_0

    int-to-long v0, p1

    const-wide v2, 0x100000000L

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    int-to-long v0, p1

    :goto_0
    return-wide v0
.end method
