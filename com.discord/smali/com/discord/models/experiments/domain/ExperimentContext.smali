.class public final Lcom/discord/models/experiments/domain/ExperimentContext;
.super Ljava/lang/Object;
.source "ExperimentContext.kt"


# instance fields
.field private final channelId:Ljava/lang/Long;

.field private final guildId:Ljava/lang/Long;

.field private final shouldTrigger:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/models/experiments/domain/ExperimentContext;-><init>(ZLjava/lang/Long;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZLjava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    iput-object p2, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    iput-object p3, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    return-void
.end method

.method public synthetic constructor <init>(ZLjava/lang/Long;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_1

    move-object p2, v0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    move-object p3, v0

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/models/experiments/domain/ExperimentContext;-><init>(ZLjava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/experiments/domain/ExperimentContext;ZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)Lcom/discord/models/experiments/domain/ExperimentContext;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/models/experiments/domain/ExperimentContext;->copy(ZLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/models/experiments/domain/ExperimentContext;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    return v0
.end method

.method public final component2()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(ZLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/models/experiments/domain/ExperimentContext;
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/domain/ExperimentContext;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/models/experiments/domain/ExperimentContext;-><init>(ZLjava/lang/Long;Ljava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/experiments/domain/ExperimentContext;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/experiments/domain/ExperimentContext;

    iget-boolean v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    iget-boolean v1, p1, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getShouldTrigger()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ExperimentContext(shouldTrigger="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->shouldTrigger:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->guildId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/domain/ExperimentContext;->channelId:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
