.class public final Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6$1;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$JsonReader$ItemFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$JsonReader$ItemFactory<",
        "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;


# direct methods
.method public constructor <init>(Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6$1;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;
    .locals 2

    sget-object v0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto$Parser;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6$1;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;

    iget-object v1, v1, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0, v1}, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6$1;->get()Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    move-result-object v0

    return-object v0
.end method
