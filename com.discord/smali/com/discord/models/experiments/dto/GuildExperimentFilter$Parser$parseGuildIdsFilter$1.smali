.class public final Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;
.super Ljava/lang/Object;
.source "GuildExperimentFilter.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseGuildIdsFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $guildIds:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $reader:Lcom/discord/models/domain/Model$JsonReader;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;->$guildIds:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Runnable;

    new-instance v2, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1$1;

    invoke-direct {v2, p0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1$1;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1$2;

    invoke-direct {v2, p0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1$2;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextListIndexed([Ljava/lang/Runnable;)V

    return-void
.end method
