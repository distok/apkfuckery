.class public final Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;
.super Ljava/lang/Object;
.source "GuildExperimentFilter.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;->get()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $fieldHash:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic this$0:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;


# direct methods
.method public constructor <init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->$fieldHash:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->$fieldHash:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    sget-object v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

    invoke-static {v1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->access$getMIN_ID_HASH$p(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)J

    move-result-wide v2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;

    iget-object v1, v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;->$start:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    goto :goto_1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->$fieldHash:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-static {v1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->access$getMAX_ID_HASH$p(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)J

    move-result-wide v1

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;

    iget-object v1, v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;->$end:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v0}, Lcom/discord/models/domain/Model$JsonReader;->nextLongOrNull()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1$2;->$fieldHash:Lkotlin/jvm/internal/Ref$ObjectRef;

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    return-void
.end method
