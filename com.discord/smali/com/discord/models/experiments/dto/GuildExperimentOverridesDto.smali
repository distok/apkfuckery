.class public final Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto$Parser;
    }
.end annotation


# instance fields
.field private final bucket:I

.field private final guilds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "guilds"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;ILjava/util/List;ILjava/lang/Object;)Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->copy(ILjava/util/List;)Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    return v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    return-object v0
.end method

.method public final copy(ILjava/util/List;)Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;"
        }
    .end annotation

    const-string v0, "guilds"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    invoke-direct {v0, p1, p2}, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;-><init>(ILjava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    iget v1, p1, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBucket()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    return v0
.end method

.method public final getGuilds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GuildExperimentOverridesDto(bucket="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->bucket:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", guilds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;->guilds:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
