.class public final Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory$TypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "UserExperimentDto.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter<",
        "Lcom/discord/models/experiments/dto/UserExperimentDto;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/discord/models/experiments/dto/UserExperimentDto;
    .locals 1

    new-instance v0, Lcom/discord/models/domain/Model$JsonReader;

    invoke-direct {v0, p1}, Lcom/discord/models/domain/Model$JsonReader;-><init>(Lcom/google/gson/stream/JsonReader;)V

    invoke-static {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/UserExperimentDto;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory$TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/discord/models/experiments/dto/UserExperimentDto;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/discord/models/experiments/dto/UserExperimentDto;)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/models/experiments/dto/UserExperimentDto;

    invoke-virtual {p0, p1, p2}, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory$TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/discord/models/experiments/dto/UserExperimentDto;)V

    return-void
.end method
