.class public final Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2$1;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$JsonReader$ItemFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$JsonReader$ItemFactory<",
        "Lkotlin/ranges/IntRange;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;


# direct methods
.method public constructor <init>(Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2$1;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2$1;->get()Lkotlin/ranges/IntRange;

    move-result-object v0

    return-object v0
.end method

.method public final get()Lkotlin/ranges/IntRange;
    .locals 2

    sget-object v0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2$1;->this$0:Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;

    iget-object v1, v1, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-static {v0, v1}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;->access$parsePosition(Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/IntRange;

    move-result-object v0

    return-object v0
.end method
