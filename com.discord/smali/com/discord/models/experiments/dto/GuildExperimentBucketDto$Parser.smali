.class public final Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;

    invoke-direct {v0}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$parsePosition(Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/IntRange;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;->parsePosition(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/IntRange;

    move-result-object p0

    return-object p0
.end method

.method private final parsePosition(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/IntRange;
    .locals 3

    new-instance v0, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    const/4 v1, -0x1

    iput v1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    new-instance v2, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    iput v1, v2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    new-instance v1, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;

    invoke-direct {v1, v0, p1, v2}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;-><init>(Lkotlin/jvm/internal/Ref$IntRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$IntRef;)V

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextObject(Lrx/functions/Action1;)V

    new-instance p1, Lkotlin/ranges/IntRange;

    iget v0, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iget v1, v2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-direct {p1, v0, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    return-object p1
.end method


# virtual methods
.method public final parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;
    .locals 5

    const-string v0, "reader"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    const/4 v1, -0x1

    iput v1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    new-instance v1, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v1}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    sget-object v2, Lx/h/l;->d:Lx/h/l;

    iput-object v2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Runnable;

    new-instance v3, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;

    invoke-direct {v3, v0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;-><init>(Lkotlin/jvm/internal/Ref$IntRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    new-instance v3, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;

    invoke-direct {v3, v1, p1}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$2;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {p1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextListIndexed([Ljava/lang/Runnable;)V

    new-instance p1, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;

    iget v0, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    invoke-direct {p1, v0, v1}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;-><init>(ILjava/util/List;)V

    return-object p1
.end method
