.class public final Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;->parsePosition(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/IntRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $end:Lkotlin/jvm/internal/Ref$IntRef;

.field public final synthetic $jsonReader:Lcom/discord/models/domain/Model$JsonReader;

.field public final synthetic $start:Lkotlin/jvm/internal/Ref$IntRef;


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/Ref$IntRef;Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$IntRef;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$start:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    iput-object p3, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$end:Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$start:Lkotlin/jvm/internal/Ref$IntRef;

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    iget v1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result v0

    iput v0, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_1

    :cond_2
    const-string v0, "e"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$end:Lkotlin/jvm/internal/Ref$IntRef;

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    iget v1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result v0

    iput v0, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_1

    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parsePosition$1;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {p1}, Lcom/discord/models/domain/Model$JsonReader;->skipValue()V

    :goto_1
    return-void
.end method
