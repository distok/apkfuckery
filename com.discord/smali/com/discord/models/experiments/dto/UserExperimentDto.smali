.class public final Lcom/discord/models/experiments/dto/UserExperimentDto;
.super Ljava/lang/Object;
.source "UserExperimentDto.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;,
        Lcom/discord/models/experiments/dto/UserExperimentDto$Parser;
    }
.end annotation


# instance fields
.field private final bucket:I

.field private final nameHash:J

.field private final population:I

.field private final revision:I


# direct methods
.method public constructor <init>(JIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    iput p3, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    iput p4, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    iput p5, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/experiments/dto/UserExperimentDto;JIIIILjava/lang/Object;)Lcom/discord/models/experiments/dto/UserExperimentDto;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget p3, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    :cond_1
    move v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget p4, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    :cond_2
    move v4, p4

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    iget p5, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    :cond_3
    move v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/models/experiments/dto/UserExperimentDto;->copy(JIII)Lcom/discord/models/experiments/dto/UserExperimentDto;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    return-wide v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    return v0
.end method

.method public final copy(JIII)Lcom/discord/models/experiments/dto/UserExperimentDto;
    .locals 7

    new-instance v6, Lcom/discord/models/experiments/dto/UserExperimentDto;

    move-object v0, v6

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/models/experiments/dto/UserExperimentDto;-><init>(JIII)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/experiments/dto/UserExperimentDto;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/experiments/dto/UserExperimentDto;

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    iget-wide v2, p1, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    iget v1, p1, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    iget v1, p1, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    iget p1, p1, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBucket()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    return v0
.end method

.method public final getNameHash()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    return-wide v0
.end method

.method public final getPopulation()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    return v0
.end method

.method public final getRevision()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "UserExperimentDto(nameHash="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->nameHash:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->revision:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bucket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->bucket:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", population="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/experiments/dto/UserExperimentDto;->population:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
