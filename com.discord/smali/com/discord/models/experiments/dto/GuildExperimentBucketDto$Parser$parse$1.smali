.class public final Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $bucketInt:Lkotlin/jvm/internal/Ref$IntRef;

.field public final synthetic $reader:Lcom/discord/models/domain/Model$JsonReader;


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/Ref$IntRef;Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;->$bucketInt:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;->$bucketInt:Lkotlin/jvm/internal/Ref$IntRef;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser$parse$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    iget v2, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextInt(I)I

    move-result v1

    iput v1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    return-void
.end method
