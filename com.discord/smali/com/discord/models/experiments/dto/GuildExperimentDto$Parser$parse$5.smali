.class public final Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;->parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentDto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $filters:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic $jsonReader:Lcom/discord/models/domain/Model$JsonReader;


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;->$filters:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;->$filters:Lkotlin/jvm/internal/Ref$ObjectRef;

    sget-object v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

    iget-object v2, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;->$jsonReader:Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {v1, v2}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseFilters(Lcom/discord/models/domain/Model$JsonReader;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    return-void
.end method
