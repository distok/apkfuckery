.class public final Lcom/discord/models/experiments/dto/GuildExperimentDto;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;
    }
.end annotation


# instance fields
.field private final buckets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;",
            ">;"
        }
    .end annotation
.end field

.field private final experimentIdHash:J

.field private final filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final hashKey:Ljava/lang/String;

.field private final overrides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;",
            ">;"
        }
    .end annotation
.end field

.field private final revision:I


# direct methods
.method public constructor <init>(JLjava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;)V"
        }
    .end annotation

    const-string v0, "buckets"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overrides"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filters"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    iput-object p3, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    iput p4, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    iput-object p5, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    iput-object p6, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    iput-object p7, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/models/experiments/dto/GuildExperimentDto;JLjava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/models/experiments/dto/GuildExperimentDto;
    .locals 8

    move-object v0, p0

    and-int/lit8 v1, p8, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p8, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p3

    :goto_1
    and-int/lit8 v4, p8, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    goto :goto_2

    :cond_2
    move v4, p4

    :goto_2
    and-int/lit8 v5, p8, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object v5, p5

    :goto_3
    and-int/lit8 v6, p8, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    goto :goto_4

    :cond_4
    move-object v6, p6

    :goto_4
    and-int/lit8 v7, p8, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    goto :goto_5

    :cond_5
    move-object v7, p7

    :goto_5
    move-wide p1, v1

    move-object p3, v3

    move p4, v4

    move-object p5, v5

    move-object p6, v6

    move-object p7, v7

    invoke-virtual/range {p0 .. p7}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->copy(JLjava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/experiments/dto/GuildExperimentDto;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    return v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/experiments/dto/GuildExperimentDto;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;)",
            "Lcom/discord/models/experiments/dto/GuildExperimentDto;"
        }
    .end annotation

    const-string v0, "buckets"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overrides"

    move-object v7, p6

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filters"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    move-object v1, v0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/experiments/dto/GuildExperimentDto;-><init>(JLjava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    iget-wide v2, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    iget v1, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBuckets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    return-object v0
.end method

.method public final getExperimentIdHash()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    return-wide v0
.end method

.method public final getFilters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    return-object v0
.end method

.method public final getHashKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getOverrides()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentOverridesDto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    return-object v0
.end method

.method public final getRevision()I
    .locals 1

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GuildExperimentDto(experimentIdHash="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->experimentIdHash:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", hashKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->hashKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->revision:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", buckets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->buckets:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", overrides="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->overrides:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", filters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentDto;->filters:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
