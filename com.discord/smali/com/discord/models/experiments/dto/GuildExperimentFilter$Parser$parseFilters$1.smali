.class public final Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;
.super Ljava/lang/Object;
.source "GuildExperimentFilter.kt"

# interfaces
.implements Lcom/discord/models/domain/Model$JsonReader$ItemFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseFilters(Lcom/discord/models/domain/Model$JsonReader;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/discord/models/domain/Model$JsonReader$ItemFactory<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $filters:Ljava/util/List;

.field public final synthetic $reader:Lcom/discord/models/domain/Model$JsonReader;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/Model$JsonReader;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    iput-object p2, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;->$filters:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;->get()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final get()V
    .locals 5

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    iget-object v1, p0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;->$reader:Lcom/discord/models/domain/Model$JsonReader;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Runnable;

    new-instance v3, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1$1;

    invoke-direct {v3, p0, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1$1;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    new-instance v3, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1$2;

    invoke-direct {v3, p0, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1$2;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    const/4 v0, 0x1

    aput-object v3, v2, v0

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/Model$JsonReader;->nextListIndexed([Ljava/lang/Runnable;)V

    return-void
.end method
