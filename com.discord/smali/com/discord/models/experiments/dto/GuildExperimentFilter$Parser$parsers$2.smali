.class public final synthetic Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$2;
.super Lx/m/c/i;
.source "GuildExperimentFilter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/Model$JsonReader;",
        "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)V
    .locals 7

    const-class v3, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

    const/4 v1, 0x1

    const-string v4, "parseGuildIdRangeFilter"

    const-string v5, "parseGuildIdRangeFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

    invoke-static {v0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->access$parseGuildIdRangeFilter(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/Model$JsonReader;

    invoke-virtual {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$2;->invoke(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;

    move-result-object p1

    return-object p1
.end method
