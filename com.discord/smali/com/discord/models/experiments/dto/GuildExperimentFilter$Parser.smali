.class public final Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;
.super Ljava/lang/Object;
.source "GuildExperimentFilter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/experiments/dto/GuildExperimentFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

.field private static final MAX_ID_HASH:J

.field private static final MIN_ID_HASH:J

.field private static final parsers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/Model$JsonReader;",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

    invoke-direct {v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;

    const/4 v1, 0x3

    new-array v1, v1, [Lkotlin/Pair;

    sget-object v2, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    const-string v3, "guild_ids"

    invoke-virtual {v2, v3}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$1;

    invoke-direct {v4, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$1;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)V

    new-instance v5, Lkotlin/Pair;

    invoke-direct {v5, v3, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v3, 0x0

    aput-object v5, v1, v3

    const/4 v3, 0x1

    const-string v4, "guild_id_range"

    invoke-virtual {v2, v4}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$2;

    invoke-direct {v5, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$2;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)V

    new-instance v6, Lkotlin/Pair;

    invoke-direct {v6, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v1, v3

    const/4 v3, 0x2

    const-string v4, "guild_member_count_range"

    invoke-virtual {v2, v4}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$3;

    invoke-direct {v5, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parsers$3;-><init>(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)V

    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v0, v1, v3

    invoke-static {v1}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parsers:Ljava/util/Map;

    const-string v0, "min_id"

    invoke-virtual {v2, v0}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v0

    sput-wide v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->MIN_ID_HASH:J

    const-string v0, "max_id"

    invoke-virtual {v2, v0}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v0

    sput-wide v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->MAX_ID_HASH:J

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getMAX_ID_HASH$p(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)J
    .locals 2

    sget-wide v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->MAX_ID_HASH:J

    return-wide v0
.end method

.method public static final synthetic access$getMIN_ID_HASH$p(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;)J
    .locals 2

    sget-wide v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->MIN_ID_HASH:J

    return-wide v0
.end method

.method public static final synthetic access$parseGuildIdRangeFilter(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseGuildIdRangeFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$parseGuildIdsFilter(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseGuildIdsFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$parseGuildMemberCountRangeFilter(Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseGuildMemberCountRangeFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;

    move-result-object p0

    return-object p0
.end method

.method private final parseGuildIdRangeFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;

    invoke-direct {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseRange(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/LongRange;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;-><init>(Lkotlin/ranges/LongRange;)V

    return-object v0
.end method

.method private final parseGuildIdsFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;
    .locals 4

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Runnable;

    new-instance v2, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;

    invoke-direct {v2, p1, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseGuildIdsFilter$1;-><init>(Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextListIndexed([Ljava/lang/Runnable;)V

    new-instance p1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-static {v0}, Lx/h/f;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;-><init>(Ljava/util/Set;)V

    return-object p1
.end method

.method private final parseGuildMemberCountRangeFilter(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;

    invoke-direct {p0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parseRange(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/LongRange;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;-><init>(Lkotlin/ranges/LongRange;)V

    return-object v0
.end method

.method private final parseRange(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/LongRange;
    .locals 4

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;

    invoke-direct {v1, p1, v0, v2}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseRange$1;-><init>(Lcom/discord/models/domain/Model$JsonReader;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    new-instance p1, Lkotlin/ranges/LongRange;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    iget-object v2, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_1

    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    :goto_1
    invoke-direct {p1, v0, v1, v2, v3}, Lkotlin/ranges/LongRange;-><init>(JJ)V

    return-object p1
.end method


# virtual methods
.method public final getParsers()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/Model$JsonReader;",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;>;"
        }
    .end annotation

    sget-object v0, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;->parsers:Ljava/util/Map;

    return-object v0
.end method

.method public final parseFilters(Lcom/discord/models/domain/Model$JsonReader;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/Model$JsonReader;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentFilter;",
            ">;"
        }
    .end annotation

    const-string v0, "reader"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;

    invoke-direct {v1, p1, v0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser$parseFilters$1;-><init>(Lcom/discord/models/domain/Model$JsonReader;Ljava/util/List;)V

    invoke-virtual {p1, v1}, Lcom/discord/models/domain/Model$JsonReader;->nextList(Lcom/discord/models/domain/Model$JsonReader$ItemFactory;)Ljava/util/List;

    return-object v0
.end method
