.class public final Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;
.super Ljava/lang/Object;
.source "UserExperimentDto.kt"

# interfaces
.implements Lf/h/d/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/experiments/dto/UserExperimentDto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TypeAdapterFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory$TypeAdapter;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;

    invoke-direct {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;-><init>()V

    sput-object v0, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;->INSTANCE:Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Lcom/google/gson/reflect/TypeToken<",
            "TT;>;)",
            "Lcom/google/gson/TypeAdapter<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const-class p1, Lcom/discord/models/experiments/dto/UserExperimentDto;

    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_1

    return-object v0

    :cond_1
    new-instance p1, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory$TypeAdapter;

    invoke-direct {p1}, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory$TypeAdapter;-><init>()V

    return-object p1

    :cond_2
    :goto_0
    return-object v0
.end method
