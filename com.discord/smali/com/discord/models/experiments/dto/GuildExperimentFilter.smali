.class public abstract Lcom/discord/models/experiments/dto/GuildExperimentFilter;
.super Ljava/lang/Object;
.source "GuildExperimentFilter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;,
        Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;,
        Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;,
        Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/models/experiments/dto/GuildExperimentFilter;-><init>()V

    return-void
.end method
