.class public final Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;
.super Ljava/lang/Object;
.source "GuildExperimentDto.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/models/experiments/dto/GuildExperimentDto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parser"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;

    invoke-direct {v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;-><init>()V

    sput-object v0, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;->INSTANCE:Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final parse(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentDto;
    .locals 19

    move-object/from16 v0, p0

    const-string v1, "jsonReader"

    invoke-static {v0, v1}, Lf/e/c/a/a;->N(Lcom/discord/models/domain/Model$JsonReader;Ljava/lang/String;)Lkotlin/jvm/internal/Ref$ObjectRef;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v3, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v3}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v2, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v4, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v4}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v2, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v5, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v5}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v2, v5, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v6, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v6}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v2, v6, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    sget-object v7, Lx/h/l;->d:Lx/h/l;

    iput-object v7, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Runnable;

    new-instance v9, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$1;

    invoke-direct {v9, v1, v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v10, 0x0

    aput-object v9, v8, v10

    new-instance v9, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$2;

    invoke-direct {v9, v3, v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$2;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v10, 0x1

    aput-object v9, v8, v10

    new-instance v9, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$3;

    invoke-direct {v9, v4, v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$3;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v10, 0x2

    aput-object v9, v8, v10

    new-instance v9, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$4;

    invoke-direct {v9, v5, v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$4;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v10, 0x3

    aput-object v9, v8, v10

    new-instance v9, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;

    invoke-direct {v9, v6, v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$5;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v10, 0x4

    aput-object v9, v8, v10

    new-instance v9, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;

    invoke-direct {v9, v2, v0}, Lcom/discord/models/experiments/dto/GuildExperimentDto$Parser$parse$6;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/Model$JsonReader;)V

    const/4 v10, 0x5

    aput-object v9, v8, v10

    invoke-virtual {v0, v8}, Lcom/discord/models/domain/Model$JsonReader;->nextListIndexed([Ljava/lang/Runnable;)V

    new-instance v0, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-object v1, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v14, v1

    check-cast v14, Ljava/lang/String;

    iget-object v1, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v15

    iget-object v1, v5, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object/from16 v16, v1

    check-cast v16, Ljava/util/List;

    invoke-static/range {v16 .. v16}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object/from16 v17, v1

    check-cast v17, Ljava/util/List;

    iget-object v1, v6, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_0

    move-object/from16 v18, v1

    goto :goto_0

    :cond_0
    move-object/from16 v18, v7

    :goto_0
    move-object v11, v0

    invoke-direct/range {v11 .. v18}, Lcom/discord/models/experiments/dto/GuildExperimentDto;-><init>(JLjava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method
