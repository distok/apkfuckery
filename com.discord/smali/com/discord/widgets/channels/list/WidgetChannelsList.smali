.class public final Lcom/discord/widgets/channels/list/WidgetChannelsList;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelsList.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/list/WidgetChannelsList$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final ANALYTICS_SOURCE:Ljava/lang/String; = "Channels List"

.field public static final BANNER_TEXT_SHADOW_DX:F = 0.0f

.field public static final BANNER_TEXT_SHADOW_DY:F = 4.0f

.field public static final BANNER_TEXT_SHADOW_RADIUS:F = 1.0f

.field public static final Companion:Lcom/discord/widgets/channels/list/WidgetChannelsList$Companion;


# instance fields
.field private adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

.field private final appBarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final bottomNavViewObserver:Lcom/discord/widgets/tabs/BottomNavViewObserver;

.field private channelListUnreads:Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;

.field private final channelListUnreadsStub$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelsBanner$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelsList$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelsListDirectMessagesTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelsSearch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channeslBannerForeground$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final collapsingToolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptyFriendsStateView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

.field private isCollapsed:Z

.field private final premiumGuildHint$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final premiumGuildHintButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privateChannelsHeaderContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privateChannelsStartGroupButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final roundedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private selectedGuildId:Ljava/lang/Long;

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xf

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v3, "roundedContainer"

    const-string v4, "getRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "collapsingToolbar"

    const-string v7, "getCollapsingToolbar()Lcom/google/android/material/appbar/CollapsingToolbarLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "appBarLayout"

    const-string v7, "getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channelsHeader"

    const-string v7, "getChannelsHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channelsList"

    const-string v7, "getChannelsList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channelsBanner"

    const-string v7, "getChannelsBanner()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channeslBannerForeground"

    const-string v7, "getChanneslBannerForeground()Landroid/widget/FrameLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channelsSearch"

    const-string v7, "getChannelsSearch()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "privateChannelsHeaderContainer"

    const-string v7, "getPrivateChannelsHeaderContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "privateChannelsStartGroupButton"

    const-string v7, "getPrivateChannelsStartGroupButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channelListUnreadsStub"

    const-string v7, "getChannelListUnreadsStub()Landroid/view/ViewStub;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "channelsListDirectMessagesTitle"

    const-string v7, "getChannelsListDirectMessagesTitle()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "premiumGuildHint"

    const-string v7, "getPremiumGuildHint()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "premiumGuildHintButton"

    const-string v7, "getPremiumGuildHintButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const-string v6, "emptyFriendsStateView"

    const-string v7, "getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/list/WidgetChannelsList$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->Companion:Lcom/discord/widgets/channels/list/WidgetChannelsList$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0bbc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->roundedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02ae

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->collapsingToolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a009b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->appBarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01ee

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01e6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsList$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01eb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsBanner$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01ec

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channeslBannerForeground$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0206

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsSearch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0205

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->privateChannelsHeaderContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0207

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->privateChannelsStartGroupButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a020c

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelListUnreadsStub$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01ed

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsListDirectMessagesTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0201

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->premiumGuildHint$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0202

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->premiumGuildHintButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bbd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->emptyFriendsStateView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/widgets/tabs/BottomNavViewObserver;->Companion:Lcom/discord/widgets/tabs/BottomNavViewObserver$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/BottomNavViewObserver$Companion;->getINSTANCE()Lcom/discord/widgets/tabs/BottomNavViewObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->bottomNavViewObserver:Lcom/discord/widgets/tabs/BottomNavViewObserver;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    sget-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->Provider:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;->get()Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    return-void
.end method

.method public static final synthetic access$ackPremiumGuildHint(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->ackPremiumGuildHint()V

    return-void
.end method

.method public static final synthetic access$configureHeaderColors(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureHeaderColors(Lcom/discord/models/domain/ModelGuild;Z)V

    return-void
.end method

.method public static final synthetic access$configureHeaderIcons(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureHeaderIcons(Lcom/discord/models/domain/ModelGuild;Z)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/widgets/channels/list/WidgetChannelListModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureUI(Lcom/discord/widgets/channels/list/WidgetChannelListModel;)V

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "adapter"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getAppBarLayout$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/discord/views/CustomAppBarLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelListUnreads$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelListUnreads:Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;

    return-object p0
.end method

.method public static final synthetic access$getChannelListUnreadsStub$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Landroid/view/ViewStub;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelListUnreadsStub()Landroid/view/ViewStub;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelsBanner$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsBanner()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSelectedGuildId$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Ljava/lang/Long;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->selectedGuildId:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getStoreNavigation$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/discord/stores/StoreNavigation;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    return-object p0
.end method

.method public static final synthetic access$handleGlobalStatusIndicatorState(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->handleGlobalStatusIndicatorState(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V

    return-void
.end method

.method public static final synthetic access$isCollapsed$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->isCollapsed:Z

    return p0
.end method

.method public static final synthetic access$setAdapter$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    return-void
.end method

.method public static final synthetic access$setChannelListUnreads$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelListUnreads:Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;

    return-void
.end method

.method public static final synthetic access$setCollapsed$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->isCollapsed:Z

    return-void
.end method

.method public static final synthetic access$setSelectedGuildId$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->selectedGuildId:Ljava/lang/Long;

    return-void
.end method

.method private final ackPremiumGuildHint()V
    .locals 2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNux()Lcom/discord/stores/StoreNux;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreNux;->setPremiumGuildHintGuildId(Ljava/lang/Long;)V

    return-void
.end method

.method private final configureBottomNavSpace()V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->bottomNavViewObserver:Lcom/discord/widgets/tabs/BottomNavViewObserver;

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/BottomNavViewObserver;->observeHeight()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    new-instance v9, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureBottomNavSpace$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureBottomNavSpace$1;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final configureHeaderColors(Lcom/discord/models/domain/ModelGuild;Z)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->isCollapsed:Z

    invoke-direct {p0, v1, p1, v2}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getTintColor(Landroid/content/Context;Lcom/discord/models/domain/ModelGuild;Z)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsHeader()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->isCollapsed:Z

    if-nez v0, :cond_0

    const v0, 0x7f060049

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v0

    goto :goto_0

    :cond_0
    const v0, 0x7f04013b

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v0

    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChanneslBannerForeground()Landroid/widget/FrameLayout;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->isCollapsed:Z

    if-nez p2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_2

    goto :goto_2

    :cond_2
    const/16 v0, 0x8

    :goto_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureHeaderIcons(Lcom/discord/models/domain/ModelGuild;Z)V
    .locals 9

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isVerifiedServer()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f080488

    goto/16 :goto_0

    :cond_2
    const v2, 0x7f080489

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isPartneredServer()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f0803c0

    goto/16 :goto_0

    :cond_4
    const v2, 0x7f0803c1

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v1}, Lx/m/c/j;->compare(II)I

    move-result v2

    if-lez v2, :cond_7

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x7f08033b

    goto :goto_0

    :cond_6
    const v2, 0x7f08033c

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    const v2, 0x7f080331

    goto :goto_0

    :cond_8
    const v2, 0x7f080332

    goto :goto_0

    :cond_9
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    if-ne v2, v0, :cond_b

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_a

    const v2, 0x7f080333

    goto :goto_0

    :cond_a
    const v2, 0x7f080334

    goto :goto_0

    :cond_b
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    const v2, 0x7f080335

    goto :goto_0

    :cond_c
    const v2, 0x7f080336

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsHeader()Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    if-nez v2, :cond_d

    move-object v2, v4

    goto :goto_1

    :cond_d
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "requireContext()"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v8, 0x7f0402f2

    invoke-static {v6, v8, v1, v0, v4}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    invoke-static {v5, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v5, p1, p2}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getTintColor(Landroid/content/Context;Lcom/discord/models/domain/ModelGuild;Z)I

    move-result p1

    invoke-static {v0, p1, v1}, Lcom/discord/utilities/color/ColorCompatKt;->setTint(Landroid/graphics/drawable/Drawable;IZ)V

    goto :goto_2

    :cond_e
    move-object v0, v4

    :goto_2
    invoke-virtual {v3, v2, v4, v0, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/channels/list/WidgetChannelListModel;)V
    .locals 10

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getSelectedGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getShowEmptyState()Z

    move-result v6

    const/16 v7, 0x8

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    const/16 v6, 0x8

    :goto_2
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getShowEmptyState()Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x4

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v5, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->selectedGuildId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_4

    :cond_4
    move-object v6, v1

    :goto_4
    invoke-static {v5, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    xor-int/2addr v5, v3

    const-string v6, "adapter"

    if-eqz v5, :cond_8

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/material/appbar/AppBarLayout;->setExpanded(Z)V

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getSelectedGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureHeaderColors(Lcom/discord/models/domain/ModelGuild;Z)V

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_5

    :cond_6
    move-object v5, v1

    :goto_5
    iput-object v5, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->selectedGuildId:Ljava/lang/Long;

    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    iget-object v5, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz v5, :cond_7

    invoke-virtual {v5, v8, v9}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setSelectedGuildId(J)V

    goto :goto_6

    :cond_7
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_8
    :goto_6
    iget-object v5, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz v5, :cond_12

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getItems()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getCollapsingToolbar()Lcom/google/android/material/appbar/CollapsingToolbarLayout;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->isGuildSelected()Z

    move-result v6

    if-eqz v6, :cond_9

    const/4 v6, 0x0

    goto :goto_7

    :cond_9
    const/16 v6, 0x8

    :goto_7
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getPrivateChannelsHeaderContainer()Landroid/view/View;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->isGuildSelected()Z

    move-result v6

    xor-int/2addr v6, v3

    if-eqz v6, :cond_a

    const/4 v6, 0x0

    goto :goto_8

    :cond_a
    const/16 v6, 0x8

    :goto_8
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsSearch()Landroid/view/View;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->isGuildSelected()Z

    move-result v6

    xor-int/2addr v6, v3

    if-eqz v6, :cond_b

    const/4 v6, 0x0

    goto :goto_9

    :cond_b
    const/16 v6, 0x8

    :goto_9
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsHeader()Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$2;

    invoke-direct {v6, p0, v0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$2;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsHeader()Landroid/widget/TextView;

    move-result-object v5

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_c
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0, v4}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureHeaderIcons(Lcom/discord/models/domain/ModelGuild;Z)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsBanner()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    if-eqz v2, :cond_d

    const/4 v5, 0x0

    goto :goto_a

    :cond_d
    const/16 v5, 0x8

    :goto_a
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChanneslBannerForeground()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v2, :cond_e

    iget-boolean v5, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->isCollapsed:Z

    if-nez v5, :cond_e

    goto :goto_b

    :cond_e
    const/4 v3, 0x0

    :goto_b
    if-eqz v3, :cond_f

    const/4 v3, 0x0

    goto :goto_c

    :cond_f
    const/16 v3, 0x8

    :goto_c
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v2, :cond_10

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsBanner()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    sget-object v3, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0701b1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/discord/utilities/icon/IconUtils;->getBannerForGuild(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Ljava/lang/String;)V

    :cond_10
    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v1

    new-instance v3, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;

    invoke-direct {v3, p0, v0, p1, v2}, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;Lcom/discord/widgets/channels/list/WidgetChannelListModel;Z)V

    invoke-virtual {v1, v3}, Lcom/discord/views/CustomAppBarLayout;->setOnPercentCollapsedCallback(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getPremiumGuildHint()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getShowPremiumGuildHint()Z

    move-result p1

    if-eqz p1, :cond_11

    goto :goto_d

    :cond_11
    const/16 v4, 0x8

    :goto_d
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_12
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->appBarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CustomAppBarLayout;

    return-object v0
.end method

.method private final getChannelListUnreadsStub()Landroid/view/ViewStub;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelListUnreadsStub$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    return-object v0
.end method

.method private final getChannelsBanner()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsBanner$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getChannelsHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChannelsList()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsList$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getChannelsListDirectMessagesTitle()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsListDirectMessagesTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelsSearch()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelsSearch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChanneslBannerForeground()Landroid/widget/FrameLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channeslBannerForeground$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private final getCollapsingToolbar()Lcom/google/android/material/appbar/CollapsingToolbarLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->collapsingToolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/appbar/CollapsingToolbarLayout;

    return-object v0
.end method

.method private final getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->emptyFriendsStateView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/friends/EmptyFriendsStateView;

    return-object v0
.end method

.method private final getPremiumGuildHint()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->premiumGuildHint$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPremiumGuildHintButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->premiumGuildHintButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPrivateChannelsHeaderContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->privateChannelsHeaderContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPrivateChannelsStartGroupButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->privateChannelsStartGroupButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->roundedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;

    return-object v0
.end method

.method private final getTintColor(Landroid/content/Context;Lcom/discord/models/domain/ModelGuild;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    if-nez p3, :cond_1

    const p2, 0x7f060292

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_1

    :cond_1
    const p2, 0x7f040155

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    :goto_1
    return p1
.end method

.method private final handleGlobalStatusIndicatorState(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 0

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->unroundPanelCorners()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->roundPanelCorners()V

    :goto_0
    return-void
.end method

.method private final roundPanelCorners()V
    .locals 2

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;->updateTopLeftRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;->updateTopRightRadius(F)V

    return-void
.end method

.method private final unroundPanelCorners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;->updateTopLeftRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/rounded/RoundedCoordinatorLayout;->updateTopRightRadius(F)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d019f

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 13

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;

    move-result-object v0

    const-string v1, "Channels List"

    invoke-virtual {v0, v1}, Lcom/discord/widgets/friends/EmptyFriendsStateView;->updateView(Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    iput-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    const-string v1, "adapter"

    const/4 v2, 0x0

    if-eqz v0, :cond_6

    new-instance v3, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$1;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setOnSelectChannel(Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz v0, :cond_5

    new-instance v3, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$2;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setOnSelectChannelOptions(Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz v0, :cond_4

    new-instance v3, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$3;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$3;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setOnCallChannel(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz p1, :cond_3

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$4;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setOnCollapseCategory(Lkotlin/jvm/functions/Function2;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz p1, :cond_2

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$5;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$5;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setOnSelectInvite(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$6;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$6;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->setOnSelectUserOptions(Lkotlin/jvm/functions/Function2;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsSearch()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$7;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$7;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelListUnreadsStub()Landroid/view/ViewStub;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getAppBarLayout()Lcom/discord/views/CustomAppBarLayout;

    move-result-object v6

    new-instance v7, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$8;

    invoke-direct {v7, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$8;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x70

    const/4 v12, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v12}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;-><init>(Landroid/view/ViewStub;Landroidx/recyclerview/widget/RecyclerView;Lcom/google/android/material/appbar/AppBarLayout;Lkotlin/jvm/functions/Function0;IIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->channelListUnreads:Lcom/discord/widgets/channels/list/WidgetChannelListUnreads;

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$9;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$9;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setOnUpdated(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple$OnUpdated;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getPremiumGuildHintButton()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$10;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$10;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getPrivateChannelsStartGroupButton()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$11;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBound$11;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsHeader()Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setAccessibilityHeading(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->getChannelsListDirectMessagesTitle()Landroid/view/View;

    move-result-object p1

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setAccessibilityHeading(Landroid/view/View;Z)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_6
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    sget-object v0, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->Companion:Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;->get()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->adapter:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-static {v0, p0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v3, "com.discord.intent.extra.EXTRA_OPEN_PANEL"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    sget-object v1, Lcom/discord/stores/StoreNavigation$PanelAction;->OPEN:Lcom/discord/stores/StoreNavigation$PanelAction;

    const/4 v4, 0x2

    invoke-static {v0, v1, v2, v4, v2}, Lcom/discord/stores/StoreNavigation;->setNavigationPanelAction$default(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureBottomNavSpace()V

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList;->globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->observeState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/channels/list/WidgetChannelsList;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsList$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_1
    const-string v0, "adapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
