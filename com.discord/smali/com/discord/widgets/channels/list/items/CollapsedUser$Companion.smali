.class public final Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;
.super Ljava/lang/Object;
.source "CollapsedUser.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/list/items/CollapsedUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;-><init>()V

    return-void
.end method

.method public static synthetic createEmptyUser$default(Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;JILjava/lang/Object;)Lcom/discord/widgets/channels/list/items/CollapsedUser;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const-wide/16 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;->createEmptyUser(J)Lcom/discord/widgets/channels/list/items/CollapsedUser;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final createEmptyUser(J)Lcom/discord/widgets/channels/list/items/CollapsedUser;
    .locals 4

    new-instance v0, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    new-instance v1, Lcom/discord/models/domain/ModelUser;

    invoke-direct {v1}, Lcom/discord/models/domain/ModelUser;-><init>()V

    const-wide/16 v2, 0x63

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJ)V

    return-object v0
.end method
