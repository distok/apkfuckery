.class public final Lcom/discord/widgets/channels/list/items/CollapsedUser;
.super Ljava/lang/Object;
.source "CollapsedUser.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;


# instance fields
.field private final emptySlot:Z

.field private final extraCount:J

.field private final user:Lcom/discord/models/domain/ModelUser;

.field private final userId$delegate:Lkotlin/Lazy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->Companion:Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelUser;ZJ)V
    .locals 1

    const-string/jumbo v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    iput-boolean p2, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    iput-wide p3, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    new-instance p1, Lcom/discord/widgets/channels/list/items/CollapsedUser$userId$2;

    invoke-direct {p1, p0}, Lcom/discord/widgets/channels/list/items/CollapsedUser$userId$2;-><init>(Lcom/discord/widgets/channels/list/items/CollapsedUser;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->userId$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelUser;ZJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    const-wide/16 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/list/items/CollapsedUser;Lcom/discord/models/domain/ModelUser;ZJILjava/lang/Object;)Lcom/discord/widgets/channels/list/items/CollapsedUser;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-wide p3, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/list/items/CollapsedUser;->copy(Lcom/discord/models/domain/ModelUser;ZJ)Lcom/discord/widgets/channels/list/items/CollapsedUser;

    move-result-object p0

    return-object p0
.end method

.method private final getUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->userId$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    return v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    return-wide v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelUser;ZJ)Lcom/discord/widgets/channels/list/items/CollapsedUser;
    .locals 1

    const-string/jumbo v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    iget-object v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    iget-wide v2, p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEmptySlot()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    return v0
.end method

.method public final getExtraCount()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    return-wide v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/items/CollapsedUser;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "CollapsedUser(user="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emptySlot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->emptySlot:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", extraCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/channels/list/items/CollapsedUser;->extraCount:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
