.class public final Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;
.super Ljava/lang/Object;
.source "WidgetChannelListModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/list/WidgetChannelListModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getPrivateChannelList(Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;)Lrx/Observable;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;->getPrivateChannelList()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSelectedGuildChannelList(Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;J)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;->getSelectedGuildChannelList(J)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$guildListBuilder(Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;JLcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/Map;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;
    .locals 0

    invoke-direct/range {p0 .. p12}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;->guildListBuilder(JLcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/Map;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final getPrivateChannelList()Lrx/Observable;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/list/WidgetChannelListModel;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannels;->observePrivateChannels()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->observeId()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserPresence;->observeAllPresences()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessagesMostRecent()Lcom/discord/stores/StoreMessagesMostRecent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessagesMostRecent;->get()Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMentions()Lcom/discord/stores/StoreMentions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMentions;->getCounts()Lrx/Observable;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming;->getStreamsByUser()Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v0

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/discord/stores/StoreUserGuildSettings;->get(J)Lrx/Observable;

    move-result-object v8

    sget-object v9, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$getPrivateChannelList$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$getPrivateChannelList$1;

    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0xfa

    invoke-static/range {v2 .. v12}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "ObservableWithLeadingEdg\u2026ILLISECONDS\n            )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getSelectedGuildChannelList(J)Lrx/Observable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/list/WidgetChannelListModel;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/channel/GuildChannelsInfo;->Companion:Lcom/discord/utilities/channel/GuildChannelsInfo$Companion;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/channel/GuildChannelsInfo$Companion;->get(J)Lrx/Observable;

    move-result-object v1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-wide v3, p1

    invoke-static/range {v2 .. v7}, Lcom/discord/stores/StoreChannels;->observeChannelsForGuild$default(Lcom/discord/stores/StoreChannels;JLjava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreChannelsSelected;->observeId()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedVoiceChannelId()Lrx/Observable;

    move-result-object v4

    sget-object v5, Lcom/discord/widgets/channels/list/WidgetChannelListModel$VoiceStates;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelListModel$VoiceStates;

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-wide v6, p1

    invoke-static/range {v5 .. v10}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$VoiceStates;->get$default(Lcom/discord/widgets/channels/list/WidgetChannelListModel$VoiceStates;JLjava/util/Comparator;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMentions()Lcom/discord/stores/StoreMentions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/stores/StoreMentions;->getCounts()Lrx/Observable;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/stores/StoreReadStates;->getUnreadChannelIds()Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStoreChannelCategories()Lcom/discord/stores/StoreChannelCategories;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Lcom/discord/stores/StoreChannelCategories;->getCollapsedCategories(J)Lrx/Observable;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNux()Lcom/discord/stores/StoreNux;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreNux;->getNuxState()Lrx/Observable;

    move-result-object v9

    new-instance v10, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$getSelectedGuildChannelList$1;

    invoke-direct {v10, p1, p2}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$getSelectedGuildChannelList$1;-><init>(J)V

    invoke-static/range {v1 .. v10}, Lrx/Observable;->c(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026lse\n          )\n        }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final guildListBuilder(JLcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/Map;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;
    .locals 35
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/utilities/channel/GuildChannelsInfo;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;JJ",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceUser;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/channels/list/items/ChannelListItem;",
            ">;"
        }
    .end annotation

    move-object/from16 v15, p12

    new-instance v14, Ljava/util/ArrayList;

    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getUnelevated()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/discord/widgets/channels/list/items/ChannelListItemMfaNotice;

    invoke-direct {v0}, Lcom/discord/widgets/channels/list/items/ChannelListItemMfaNotice;-><init>()V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getAbleToInstantInvite()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/discord/widgets/channels/list/items/ChannelListItemInvite;

    move-wide/from16 v12, p1

    invoke-direct {v0, v12, v13}, Lcom/discord/widgets/channels/list/items/ChannelListItemInvite;-><init>(J)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-wide/from16 v12, p1

    :goto_0
    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/discord/utilities/channel/ChannelUtils;->getSortedCategories(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    new-instance v10, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$1;

    invoke-direct {v10, v14, v11, v15}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$1;-><init>(Ljava/util/ArrayList;Ljava/util/HashSet;Ljava/util/Set;)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    const-wide/16 v3, 0x400

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getChannelPermissions()Ljava/util/Map;

    move-result-object v5

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-static {v3, v4, v5}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v3

    const-wide/16 v4, 0x10

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getChannelPermissions()Ljava/util/Map;

    move-result-object v6

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-static {v4, v5, v6}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v8

    if-eqz v3, :cond_2

    if-eqz v8, :cond_2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getNotificationSettings()Lcom/discord/models/domain/ModelNotificationSettings;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/discord/models/domain/ModelNotificationSettings;->getChannelOverride(J)Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    move-result-object v1

    const/16 v21, 0x1

    const/16 v22, 0x0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->isMuted()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v23, 0x1

    goto :goto_2

    :cond_3
    const/16 v23, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v6

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getChannelPermissions()Ljava/util/Map;

    move-result-object v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/Long;

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getNotificationSettings()Lcom/discord/models/domain/ModelNotificationSettings;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/discord/models/domain/ModelNotificationSettings;->getChannelOverride(J)Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v4, 0x1

    goto :goto_4

    :cond_4
    const/4 v4, 0x0

    :goto_4
    new-instance v3, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1;

    move-object v0, v3

    move-wide v1, v6

    move-object/from16 v25, v3

    move-object v3, v9

    move/from16 p4, v4

    move/from16 v4, v23

    move-object/from16 v26, v5

    move/from16 v5, p4

    move-wide/from16 v27, v6

    move-object/from16 v6, p3

    move-object v7, v11

    move/from16 v29, v8

    move-object/from16 v30, v9

    move-wide/from16 v8, p5

    move-object/from16 v31, v10

    move-object/from16 v10, p10

    move-object/from16 v32, v11

    move-object/from16 v11, p11

    move-wide/from16 v12, p1

    move-object/from16 v33, v14

    move-object/from16 v14, p12

    move-wide/from16 v15, p7

    move-object/from16 v17, p9

    move-object/from16 v18, v33

    move-object/from16 v19, v31

    invoke-direct/range {v0 .. v19}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1;-><init>(JLcom/discord/models/domain/ModelChannel;ZZLcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/HashSet;JLjava/util/Map;Ljava/util/Set;JLjava/util/Set;JLjava/util/Map;Ljava/util/ArrayList;Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$1;)V

    new-instance v14, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2;

    move/from16 v13, p4

    move-object/from16 v0, v25

    move-object/from16 v4, v26

    move-object/from16 v15, v30

    invoke-direct {v14, v15, v4, v0, v13}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2;-><init>(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$1;Z)V

    new-instance v19, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$3;

    move-object/from16 v0, v19

    move-wide/from16 v1, v27

    move-object v3, v15

    move-object/from16 v5, p3

    move-object/from16 v6, v32

    move-wide/from16 v7, p5

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-wide/from16 v11, p1

    move/from16 v34, v13

    move-object/from16 v13, p12

    move-object/from16 v25, v14

    move-wide/from16 v14, p7

    move-object/from16 v16, p9

    move-object/from16 v17, v33

    move-object/from16 v18, v31

    invoke-direct/range {v0 .. v18}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$3;-><init>(JLcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/utilities/channel/GuildChannelsInfo;Ljava/util/HashSet;JLjava/util/Map;Ljava/util/Set;JLjava/util/Set;JLjava/util/Map;Ljava/util/ArrayList;Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$1;)V

    invoke-virtual/range {v30 .. v30}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    move-object/from16 v4, p9

    move-object/from16 v2, p12

    move/from16 v3, v29

    move-object/from16 v1, v33

    goto :goto_7

    :cond_5
    move-object/from16 v4, p9

    move-object/from16 v2, p12

    move/from16 v3, v29

    move-object/from16 v1, v33

    goto :goto_6

    :cond_6
    invoke-virtual/range {v31 .. v31}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$1;->invoke()V

    new-instance v0, Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v2, p12

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    move/from16 v3, v29

    move-object/from16 v4, v30

    move/from16 v5, v34

    invoke-direct {v0, v4, v1, v5, v3}, Lcom/discord/widgets/channels/list/items/ChannelListItemCategory;-><init>(Lcom/discord/models/domain/ModelChannel;ZZZ)V

    move-object/from16 v1, v33

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    move-object/from16 v4, p9

    goto :goto_7

    :cond_8
    move-object/from16 v2, p12

    move/from16 v3, v29

    move-object/from16 v1, v33

    invoke-virtual/range {v19 .. v19}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$3;->invoke()Lcom/discord/widgets/channels/list/items/ChannelListItemVoiceChannel;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object/from16 v4, p9

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_9

    goto :goto_5

    :cond_9
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    :goto_5
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_7

    :goto_6
    invoke-virtual/range {v25 .. v25}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$$inlined$forEach$lambda$2;->invoke()Lcom/discord/widgets/channels/list/items/ChannelListItemTextChannel;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    :goto_7
    move-wide/from16 v12, p1

    move-object v14, v1

    move-object v15, v2

    move v8, v3

    move-object/from16 v10, v31

    move-object/from16 v11, v32

    goto/16 :goto_3

    :cond_b
    move-object/from16 v4, p9

    move-wide/from16 v12, p1

    goto/16 :goto_1

    :cond_c
    move-object/from16 v31, v10

    move-object v1, v14

    invoke-virtual/range {v31 .. v31}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$guildListBuilder$1;->invoke()V

    return-object v1
.end method


# virtual methods
.method public final get()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/list/WidgetChannelListModel;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSelected;->observeSelectedGuildId()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$get$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$get$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n          .g\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
