.class public final Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;
.super Ljava/lang/Object;
.source "WidgetChannelListUnreads.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;-><init>()V

    return-void
.end method

.method private final findIndicator(Ljava/util/List;Lkotlin/ranges/IntProgression;ZZ)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/ranges/IntProgression;",
            "ZZ)",
            "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;"
        }
    .end annotation

    iget v0, p2, Lkotlin/ranges/IntProgression;->d:I

    iget v1, p2, Lkotlin/ranges/IntProgression;->e:I

    iget p2, p2, Lkotlin/ranges/IntProgression;->f:I

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ltz p2, :cond_0

    if-gt v0, v1, :cond_4

    goto :goto_0

    :cond_0
    if-lt v0, v1, :cond_4

    :goto_0
    const/4 v4, -0x1

    :goto_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v6, v5, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;

    if-nez v6, :cond_1

    move-object v5, v2

    :cond_1
    check-cast v5, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;

    if-eqz v5, :cond_3

    invoke-interface {v5}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;->getMentionCount()I

    move-result v6

    if-lez v6, :cond_2

    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;

    const/4 p2, 0x0

    invoke-direct {p1, p2, v0}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;-><init>(II)V

    return-object p1

    :cond_2
    if-eqz p4, :cond_3

    if-nez p3, :cond_3

    if-ne v4, v3, :cond_3

    invoke-interface {v5}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;->isUnread()Z

    move-result v5

    if-eqz v5, :cond_3

    move v4, v0

    :cond_3
    if-eq v0, v1, :cond_5

    add-int/2addr v0, p2

    goto :goto_1

    :cond_4
    const/4 v4, -0x1

    :cond_5
    if-eq v4, v3, :cond_6

    new-instance v2, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;

    const/4 p1, 0x1

    invoke-direct {v2, p1, v4}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;-><init>(II)V

    :cond_6
    return-object v2
.end method

.method public static synthetic get$default(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;Lkotlin/ranges/IntRange;Ljava/util/List;ZILjava/lang/Object;)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;->get(Lkotlin/ranges/IntRange;Ljava/util/List;Z)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;

    move-result-object p0

    return-object p0
.end method

.method private final hasUnread(Ljava/util/List;Lkotlin/ranges/IntRange;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/ranges/IntRange;",
            ")Z"
        }
    .end annotation

    const-string v0, "$this$slice"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "indices"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lkotlin/ranges/IntRange;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lkotlin/ranges/IntRange;->getStart()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Lkotlin/ranges/IntRange;->getEndInclusive()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    add-int/2addr p2, v1

    invoke-interface {p1, v0, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    instance-of v2, p2, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;

    if-nez v2, :cond_4

    const/4 p2, 0x0

    :cond_4
    check-cast p2, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;

    if-eqz p2, :cond_5

    invoke-interface {p2}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$UnreadItem;->isUnread()Z

    move-result p2

    if-ne p2, v1, :cond_5

    const/4 p2, 0x1

    goto :goto_1

    :cond_5
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_3

    :goto_2
    return v1
.end method


# virtual methods
.method public final get(Lkotlin/ranges/IntRange;Ljava/util/List;Z)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/ranges/IntRange;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Object;",
            ">;Z)",
            "Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;"
        }
    .end annotation

    const-string/jumbo v0, "visibleRange"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "items"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    :try_start_0
    invoke-direct {p0, p2, p1}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;->hasUnread(Ljava/util/List;Lkotlin/ranges/IntRange;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget v3, p1, Lkotlin/ranges/IntProgression;->d:I

    invoke-static {v1, v3}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    invoke-direct {p0, p2, v1, v2, p3}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;->findIndicator(Ljava/util/List;Lkotlin/ranges/IntProgression;ZZ)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;

    move-result-object v1

    invoke-static {p2}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v3

    iget p1, p1, Lkotlin/ranges/IntProgression;->e:I

    add-int/2addr p1, v0

    invoke-static {v3, p1}, Lx/p/e;->downTo(II)Lkotlin/ranges/IntProgression;

    move-result-object p1

    invoke-direct {p0, p2, p1, v2, p3}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Companion;->findIndicator(Ljava/util/List;Lkotlin/ranges/IntProgression;ZZ)Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;

    invoke-direct {p2, v1, p1}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    new-instance p2, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;

    const/4 p1, 0x3

    const/4 p3, 0x0

    invoke-direct {p2, p3, p3, p1, p3}, Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model;-><init>(Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;Lcom/discord/widgets/channels/list/WidgetChannelListUnreads$Model$Indicator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_1
    return-object p2
.end method
