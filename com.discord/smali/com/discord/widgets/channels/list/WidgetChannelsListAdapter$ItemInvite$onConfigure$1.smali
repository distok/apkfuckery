.class public final Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetChannelsListAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite;->onConfigure(ILcom/discord/widgets/channels/list/items/ChannelListItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/channels/list/items/ChannelListItem;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/list/items/ChannelListItem;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite$onConfigure$1;->$data:Lcom/discord/widgets/channels/list/items/ChannelListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    sget-object v0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare;->Companion:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;

    const-string v1, "it"

    const-string v2, "it.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite$onConfigure$1;->$data:Lcom/discord/widgets/channels/list/items/ChannelListItem;

    check-cast p1, Lcom/discord/widgets/channels/list/items/ChannelListItemInvite;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/list/items/ChannelListItemInvite;->getGuildId()J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "Guild Header"

    const/16 v7, 0xc

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;->launch$default(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;Landroid/content/Context;JLjava/lang/Long;ZLjava/lang/String;ILjava/lang/Object;)V

    return-void
.end method
