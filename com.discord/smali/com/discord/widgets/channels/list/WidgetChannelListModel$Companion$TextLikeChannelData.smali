.class public final Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;
.super Ljava/lang/Object;
.source "WidgetChannelListModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextLikeChannelData"
.end annotation


# instance fields
.field private final hide:Z

.field private final locked:Z

.field private final mentionCount:I

.field private final selected:Z

.field private final unread:Z


# direct methods
.method public constructor <init>(ZIZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    iput p2, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    iput-boolean p3, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    iput-boolean p4, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    iput-boolean p5, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;ZIZZZILjava/lang/Object;)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget p2, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move p3, p1

    move p4, p7

    move p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->copy(ZIZZZ)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    return v0
.end method

.method public final copy(ZIZZZ)Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;
    .locals 7

    new-instance v6, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;

    move-object v0, v6

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;-><init>(ZIZZZ)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    iget v1, p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHide()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    return v0
.end method

.method public final getLocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    return v0
.end method

.method public final getMentionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    return v0
.end method

.method public final getSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    return v0
.end method

.method public final getUnread()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "TextLikeChannelData(selected="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->selected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", mentionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->mentionCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unread="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->unread:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", locked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->locked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hide="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelListModel$Companion$TextLikeChannelData;->hide:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
