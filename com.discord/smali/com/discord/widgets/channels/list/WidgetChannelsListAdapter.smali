.class public final Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetChannelsListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$Item;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelText;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelCategory;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelPrivate;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelVoice;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemVoiceUser;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemHeader;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemMFA;,
        Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemSpace;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/channels/list/items/ChannelListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private bottomNavHeight:I

.field private onCallChannel:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onCollapseCategory:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSelectChannel:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSelectChannelOptions:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSelectInvite:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSelectUserOptions:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private selectedGuildId:J


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectChannel$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectChannel$1;

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectChannel:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectChannelOptions$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectChannelOptions$1;

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectChannelOptions:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectUserOptions$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectUserOptions$1;

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectUserOptions:Lkotlin/jvm/functions/Function2;

    sget-object p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectInvite$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onSelectInvite$1;

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectInvite:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onCallChannel$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onCallChannel$1;

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCallChannel:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onCollapseCategory$1;->INSTANCE:Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$onCollapseCategory$1;

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCollapseCategory:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static final synthetic access$getBottomNavHeight$p(Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)I
    .locals 0

    iget p0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->bottomNavHeight:I

    return p0
.end method

.method public static final synthetic access$setBottomNavHeight$p(Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->bottomNavHeight:I

    return-void
.end method


# virtual methods
.method public final getOnCallChannel()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCallChannel:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnCollapseCategory()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCollapseCategory:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getOnSelectChannel()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectChannel:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnSelectChannelOptions()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectChannelOptions:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnSelectInvite()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectInvite:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnSelectUserOptions()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectUserOptions:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getSelectedGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->selectedGuildId:J

    return-wide v0
.end method

.method public final handleBottomNavHeight(I)V
    .locals 3

    iput p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->bottomNavHeight:I

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getInternalData()Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/channels/list/items/ChannelListBottomNavSpaceItem;

    iget-wide v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->selectedGuildId:J

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/channels/list/items/ChannelListBottomNavSpaceItem;-><init>(J)V

    invoke-interface {p1, v0}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/widgets/channels/list/items/ChannelListItem;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :pswitch_1
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemSpace;

    const p2, 0x7f0d00de

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemSpace;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_2
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite;

    const p2, 0x7f0d01a6

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemInvite;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_3
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelCategory;

    const p2, 0x7f0d01a1

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelCategory;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_4
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemMFA;

    const p2, 0x7f0d01a7

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemMFA;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_5
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemHeader;

    const p2, 0x7f0d01a5

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemHeader;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_6
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelPrivate;

    const p2, 0x7f0d01a3

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelPrivate;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_7
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemVoiceUser;

    const p2, 0x7f0d01a9

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemVoiceUser;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_8
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelVoice;

    const p2, 0x7f0d01a4

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelVoice;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    goto :goto_0

    :pswitch_9
    new-instance p1, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelText;

    const p2, 0x7f0d01a2

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter$ItemChannelText;-><init>(ILcom/discord/widgets/channels/list/WidgetChannelsListAdapter;)V

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final setOnCallChannel(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCallChannel:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnCollapseCategory(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onCollapseCategory:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setOnSelectChannel(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectChannel:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnSelectChannelOptions(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectChannelOptions:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnSelectInvite(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectInvite:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnSelectUserOptions(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->onSelectUserOptions:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setSelectedGuildId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsListAdapter;->selectedGuildId:J

    return-void
.end method
