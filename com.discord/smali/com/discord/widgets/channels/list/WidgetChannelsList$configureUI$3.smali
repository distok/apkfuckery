.class public final Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;
.super Lx/m/c/k;
.source "WidgetChannelsList.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/list/WidgetChannelsList;->configureUI(Lcom/discord/widgets/channels/list/WidgetChannelListModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Float;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guild:Lcom/discord/models/domain/ModelGuild;

.field public final synthetic $hasBanner:Z

.field public final synthetic $list:Lcom/discord/widgets/channels/list/WidgetChannelListModel;

.field public final synthetic this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;Lcom/discord/widgets/channels/list/WidgetChannelListModel;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    iput-object p2, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->$guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p3, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->$list:Lcom/discord/widgets/channels/list/WidgetChannelListModel;

    iput-boolean p4, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->$hasBanner:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->invoke(F)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(F)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    invoke-static {v0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$getChannelsBanner$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    invoke-static {p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$getAppBarLayout$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Lcom/discord/views/CustomAppBarLayout;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/views/CustomAppBarLayout;->a()Z

    move-result p1

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    invoke-static {v0}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$isCollapsed$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    invoke-static {v0, p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$setCollapsed$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;Z)V

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->$guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {p1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$isCollapsed$p(Lcom/discord/widgets/channels/list/WidgetChannelsList;)Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$configureHeaderIcons(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;Z)V

    iget-object p1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->this$0:Lcom/discord/widgets/channels/list/WidgetChannelsList;

    iget-object v0, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->$list:Lcom/discord/widgets/channels/list/WidgetChannelListModel;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/list/WidgetChannelListModel;->getSelectedGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/channels/list/WidgetChannelsList$configureUI$3;->$hasBanner:Z

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/channels/list/WidgetChannelsList;->access$configureHeaderColors(Lcom/discord/widgets/channels/list/WidgetChannelsList;Lcom/discord/models/domain/ModelGuild;Z)V

    :cond_0
    return-void
.end method
