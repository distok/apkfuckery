.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->observeNavState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;

    invoke-direct {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;-><init>()V

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;
    .locals 2

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    const-string v1, "isOpen"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string v1, "isOnHomeTab"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;-><init>(ZZ)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;->call(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    move-result-object p1

    return-object p1
.end method
