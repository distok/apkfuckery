.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;
.super Lx/m/c/k;
.source "WidgetChannelSidebarActions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->configureUI(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $context:Landroid/content/Context;


# direct methods
.method public constructor <init>(JLandroid/content/Context;)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;->$channelId:J

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;->$context:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;

    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;->$channelId:J

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;->$context:Landroid/content/Context;

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;->launch(JLandroid/content/Context;)V

    return-void
.end method
