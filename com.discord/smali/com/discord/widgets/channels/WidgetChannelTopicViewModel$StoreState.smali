.class public abstract Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "StoreState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;
    }
.end annotation


# instance fields
.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z


# direct methods
.method private constructor <init>(ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened:Z

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;-><init>(ZZ)V

    return-void
.end method


# virtual methods
.method public isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab:Z

    return v0
.end method

.method public isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened:Z

    return v0
.end method
