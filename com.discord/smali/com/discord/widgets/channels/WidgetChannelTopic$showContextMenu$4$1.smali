.class public final Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4$1;
.super Lx/m/c/k;
.source "WidgetChannelTopic.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->onMenuItemClick(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "v"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;

    iget-object v0, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->access$getViewModel$p(Lcom/discord/widgets/channels/WidgetChannelTopic;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v1, "v.context"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->handleClosePrivateChannel(Landroid/content/Context;)Lkotlin/Unit;

    return-void
.end method
