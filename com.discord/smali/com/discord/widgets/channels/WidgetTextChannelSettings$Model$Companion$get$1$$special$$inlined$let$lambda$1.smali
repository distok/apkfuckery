.class public final Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;
.super Ljava/lang/Object;
.source "WidgetTextChannelSettings.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel$inlined:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->this$0:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->$channel$inlined:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;)Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;
    .locals 11

    if-eqz p1, :cond_6

    const-wide/16 v0, 0x10

    const-string v2, "me"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v3

    invoke-static {v0, v1, p3, v2, v3}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result v6

    const-wide/32 v0, 0x10000000

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v2

    invoke-static {v0, v1, p3, p2, v2}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result v7

    iget-object p2, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->$channel$inlined:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result p2

    const/4 p3, 0x0

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuildsNsfw()Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object p2

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->$channel$inlined:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Lcom/discord/stores/StoreGuildsNsfw;->isGuildNsfwGateAgreed(J)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v8, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v8, 0x1

    :goto_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isCommunityServer()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object p2

    if-nez p2, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object p2, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->this$0:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;

    iget-wide v3, p2, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;->$channelId:J

    cmp-long p2, v1, v3

    if-nez p2, :cond_3

    const/4 v9, 0x1

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v9, 0x0

    :goto_3
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isCommunityServer()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->this$0:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;

    iget-wide v1, v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1;->$channelId:J

    cmp-long v3, p1, v1

    if-nez v3, :cond_5

    const/4 v10, 0x1

    goto :goto_5

    :cond_5
    :goto_4
    const/4 v10, 0x0

    :goto_5
    new-instance p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;

    iget-object v5, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->$channel$inlined:Lcom/discord/models/domain/ModelChannel;

    move-object v4, p1

    invoke-direct/range {v4 .. v10}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;-><init>(Lcom/discord/models/domain/ModelChannel;ZZZZZ)V

    goto :goto_6

    :cond_6
    const/4 p1, 0x0

    :goto_6
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Lcom/discord/models/domain/ModelUser;

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion$get$1$$special$$inlined$let$lambda$1;->call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;)Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;

    move-result-object p1

    return-object p1
.end method
