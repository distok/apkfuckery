.class public Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelSettingsEditPermissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;,
        Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForUser;,
        Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;,
        Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;
    }
.end annotation


# static fields
.field private static final INTENT_EXTRA_CHANNEL_ID:Ljava/lang/String; = "INTENT_EXTRA_CHANNEL_ID"

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field private static final INTENT_EXTRA_TARGET_ID:Ljava/lang/String; = "INTENT_EXTRA_TARGET_ID"

.field private static final INTENT_EXTRA_TYPE:Ljava/lang/String; = "INTENT_EXTRA_TYPE"

.field public static final TYPE_ROLE:I = 0x1

.field public static final TYPE_USER:I


# instance fields
.field private channelName:Landroid/widget/TextView;

.field public permissionCheckboxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/views/TernaryCheckBox;",
            ">;"
        }
    .end annotation
.end field

.field private saveFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private state:Lcom/discord/utilities/stateful/StatefulViews;

.field private targetName:Landroid/widget/TextView;

.field private textPermissionsContainer:Landroid/view/View;

.field private userAvatar:Landroid/widget/ImageView;

.field private voicePermissionsContainer:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method

.method public static synthetic access$400(Ljava/lang/String;Landroid/widget/TextView;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->setTextWithFont(Ljava/lang/String;Landroid/widget/TextView;I)V

    return-void
.end method

.method private configureMenu(Lcom/discord/models/domain/ModelChannel;J)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NonConstantResourceId"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0e000a

    goto :goto_0

    :cond_0
    const v0, 0x7f0e000c

    :goto_0
    new-instance v1, Lf/a/o/a/y;

    invoke-direct {v1, p0, p1, p2, p3}, Lf/a/o/a/y;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;Lcom/discord/models/domain/ModelChannel;J)V

    invoke-virtual {p0, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method private configureToolbar(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isCategory()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f12040a

    goto :goto_0

    :cond_0
    const v0, 0x7f120467

    :goto_0
    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;)V
    .locals 6

    if-eqz p1, :cond_b

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->isManageable()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->configureToolbar(Lcom/discord/models/domain/ModelChannel;)V

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getTargetId()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->configureMenu(Lcom/discord/models/domain/ModelChannel;J)V

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->userAvatar:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->targetName:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    invoke-interface {p1, v1, v2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->setupHeader(Landroid/widget/ImageView;Landroid/widget/TextView;)V

    :cond_1
    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->channelName:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->textPermissionsContainer:Landroid/view/View;

    const/16 v2, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x0

    if-eqz v1, :cond_5

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    const/4 v5, 0x5

    if-ne v0, v5, :cond_3

    goto :goto_0

    :cond_3
    const/16 v5, 0x8

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->voicePermissionsContainer:Landroid/view/View;

    if-eqz v1, :cond_8

    const/4 v5, 0x2

    if-eq v0, v5, :cond_6

    if-ne v0, v3, :cond_7

    :cond_6
    const/4 v2, 0x0

    :cond_7
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->permissionCheckboxes:Ljava/util/List;

    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getTargetId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPermissionOverwrite;

    invoke-direct {p0, v1, v2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->setupPermissionCheckedState(Lcom/discord/views/TernaryCheckBox;Lcom/discord/models/domain/ModelPermissionOverwrite;)V

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->getPermission(I)J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->setupPermissionEnabledState(Lcom/discord/views/TernaryCheckBox;J)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->saveFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_a

    new-instance v1, Lf/a/o/a/d0;

    invoke-direct {v1, p0, p1}, Lf/a/o/a/d0;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->state:Lcom/discord/utilities/stateful/StatefulViews;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->saveFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/stateful/StatefulViews;->configureSaveActionView(Landroid/view/View;)V

    return-void

    :cond_b
    :goto_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_c

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_c
    return-void
.end method

.method private static create(Landroid/content/Context;JJJI)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p1, "INTENT_EXTRA_CHANNEL_ID"

    invoke-virtual {v0, p1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p1, "INTENT_EXTRA_TARGET_ID"

    invoke-virtual {v0, p1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p1, "INTENT_EXTRA_TYPE"

    invoke-virtual {v0, p1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-class p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;

    invoke-static {p0, p1, v0}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static createForRole(Landroid/content/Context;JJJ)V
    .locals 8

    const/4 v7, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->create(Landroid/content/Context;JJJI)V

    return-void
.end method

.method public static createForUser(Landroid/content/Context;JJJ)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->create(Landroid/content/Context;JJJI)V

    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->configureUI(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;)V

    return-void
.end method

.method private static getModel(JJJI)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJI)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    if-ne p6, v0, :cond_0

    invoke-static/range {p0 .. p5}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->get(JJJ)Lrx/Observable;

    move-result-object p0

    return-object p0

    :cond_0
    if-nez p6, :cond_1

    invoke-static/range {p0 .. p5}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForUser;->get(JJJ)Lrx/Observable;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, p0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method private static getPermission(I)J
    .locals 2
    .param p0    # I
        .annotation build Landroidx/annotation/IdRes;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NonConstantResourceId"
        }
    .end annotation

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid ID: "

    invoke-static {v1, p0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-wide/16 v0, 0x200

    return-wide v0

    :pswitch_1
    const-wide/32 v0, 0x2000000

    return-wide v0

    :pswitch_2
    const-wide/32 v0, 0x200000

    return-wide v0

    :pswitch_3
    const-wide/32 v0, 0x400000

    return-wide v0

    :pswitch_4
    const-wide/32 v0, 0x1000000

    return-wide v0

    :pswitch_5
    const-wide/32 v0, 0x800000

    return-wide v0

    :pswitch_6
    const-wide/32 v0, 0x100000

    return-wide v0

    :pswitch_7
    const-wide/32 v0, 0x40000

    return-wide v0

    :pswitch_8
    const-wide/16 v0, 0x1000

    return-wide v0

    :pswitch_9
    const-wide/16 v0, 0x800

    return-wide v0

    :pswitch_a
    const-wide/16 v0, 0x400

    return-wide v0

    :pswitch_b
    const-wide/32 v0, 0x10000

    return-wide v0

    :pswitch_c
    const-wide/32 v0, 0x20000

    return-wide v0

    :pswitch_d
    const-wide/16 v0, 0x2000

    return-wide v0

    :pswitch_e
    const-wide/16 v0, 0x4000

    return-wide v0

    :pswitch_f
    const-wide/32 v0, 0x8000

    return-wide v0

    :pswitch_10
    const-wide/16 v0, 0x40

    return-wide v0

    :pswitch_11
    const-wide/32 v0, 0x20000000

    return-wide v0

    :pswitch_12
    const-wide/32 v0, 0x10000000

    return-wide v0

    :pswitch_13
    const-wide/16 v0, 0x10

    return-wide v0

    :pswitch_14
    const-wide/16 v0, 0x1

    return-wide v0

    :pswitch_data_0
    .packed-switch 0x7f0a018a
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getRequestBody(JI)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->permissionCheckboxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    move-wide v3, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/views/TernaryCheckBox;

    invoke-virtual {v5}, Lcom/discord/views/TernaryCheckBox;->b()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getId()I

    move-result v5

    invoke-static {v5}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->getPermission(I)J

    move-result-wide v5

    or-long/2addr v1, v5

    goto :goto_0

    :cond_1
    iget v7, v5, Lcom/discord/views/TernaryCheckBox;->n:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_2

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_0

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getId()I

    move-result v5

    invoke-static {v5}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->getPermission(I)J

    move-result-wide v5

    or-long/2addr v3, v5

    goto :goto_0

    :cond_3
    if-ne p3, v6, :cond_4

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;->createForRole(JLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;

    move-result-object p1

    return-object p1

    :cond_4
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;->createForMember(JLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;

    move-result-object p1

    return-object p1
.end method

.method private static setTextWithFont(Ljava/lang/String;Landroid/widget/TextView;I)V
    .locals 3
    .param p2    # I
        .annotation build Landroidx/annotation/FontRes;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Landroidx/core/content/res/ResourcesCompat;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/discord/utilities/spans/TypefaceSpanCompat;

    invoke-direct {v0, p2}, Lcom/discord/utilities/spans/TypefaceSpanCompat;-><init>(Landroid/graphics/Typeface;)V

    new-instance p2, Landroid/text/SpannableStringBuilder;

    invoke-direct {p2, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 p0, 0x0

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v2, 0x21

    invoke-virtual {p2, v0, p0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget-object p0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method private setupPermissionCheckedState(Lcom/discord/views/TernaryCheckBox;Lcom/discord/models/domain/ModelPermissionOverwrite;)V
    .locals 7

    const-wide/16 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getAllow()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPermissionOverwrite;->getDeny()J

    move-result-wide v2

    goto :goto_0

    :cond_0
    move-wide v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getId()I

    move-result p2

    invoke-static {p2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->getPermission(I)J

    move-result-wide v4

    and-long/2addr v0, v4

    cmp-long v6, v0, v4

    if-nez v6, :cond_1

    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->f()V

    goto :goto_1

    :cond_1
    and-long v0, v4, v2

    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->e()V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->d()V

    :goto_1
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->getSwitchStatus()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/discord/utilities/stateful/StatefulViews;->get(ILjava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/a/o/a/c0;

    invoke-direct {v0, p0, p2}, Lf/a/o/a/c0;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;I)V

    invoke-virtual {p1, v0}, Lcom/discord/views/TernaryCheckBox;->setOnSwitchStatusChangedListener(Lcom/discord/views/TernaryCheckBox$b;)V

    return-void
.end method


# virtual methods
.method public synthetic g(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;Landroid/view/View;)V
    .locals 8

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getTargetId()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getType()I

    move-result p2

    invoke-direct {p0, v0, v1, p2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->getRequestBody(JI)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;

    move-result-object v7

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v2

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-interface {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;->getTargetId()J

    move-result-wide v5

    invoke-virtual/range {v2 .. v7}, Lcom/discord/utilities/rest/RestAPI;->updatePermissionOverwrites(JJLcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lf/a/o/a/a0;->d:Lf/a/o/a/a0;

    invoke-static {p2, p0}, Lf/a/b/r;->m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0199

    return v0
.end method

.method public synthetic h(Ljava/lang/Void;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {p1}, Lcom/discord/utilities/stateful/StatefulViews;->clear()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-virtual {p1}, Lf/a/b/f;->onBackPressed()V

    :cond_0
    return-void
.end method

.method public synthetic i(II)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/stateful/StatefulViews;->put(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->state:Lcom/discord/utilities/stateful/StatefulViews;

    iget-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->saveFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1, p2}, Lcom/discord/utilities/stateful/StatefulViews;->configureSaveActionView(Landroid/view/View;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a019f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->userAvatar:Landroid/widget/ImageView;

    const v0, 0x7f0a01a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->targetName:Landroid/widget/TextView;

    const v0, 0x7f0a01a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->channelName:Landroid/widget/TextView;

    const v0, 0x7f0a01a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->textPermissionsContainer:Landroid/view/View;

    const v0, 0x7f0a01a4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->voicePermissionsContainer:Landroid/view/View;

    const v0, 0x7f0a01a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->saveFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/discord/views/TernaryCheckBox;

    const v1, 0x7f0a018a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f0a018b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const v1, 0x7f0a018c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const v1, 0x7f0a018d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const v1, 0x7f0a018f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const v1, 0x7f0a0190

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const v1, 0x7f0a0191

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const v1, 0x7f0a0192

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const v1, 0x7f0a0193

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const v1, 0x7f0a0194

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const v1, 0x7f0a0195

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const v1, 0x7f0a0196

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const v1, 0x7f0a0197

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const v1, 0x7f0a018e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const v1, 0x7f0a0198

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const v1, 0x7f0a0199

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const v1, 0x7f0a019a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const v1, 0x7f0a019b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const v1, 0x7f0a019c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const v1, 0x7f0a019e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/discord/views/TernaryCheckBox;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const v1, 0x7f0a019d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/TernaryCheckBox;

    const/16 v1, 0x14

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->permissionCheckboxes:Ljava/util/List;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled()Landroidx/appcompat/widget/Toolbar;

    new-instance p1, Lcom/discord/utilities/stateful/StatefulViews;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->permissionCheckboxes:Ljava/util/List;

    invoke-direct {p1, v0}, Lcom/discord/utilities/stateful/StatefulViews;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {p1, p0}, Lcom/discord/utilities/stateful/StatefulViews;->setupUnsavedChangesConfirmation(Lcom/discord/app/AppFragment;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 11

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_CHANNEL_ID"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_TARGET_ID"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_TYPE"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    invoke-static/range {v4 .. v10}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->getModel(JJJI)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observable"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/a/z;

    invoke-direct {v1, p0}, Lf/a/o/a/z;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
