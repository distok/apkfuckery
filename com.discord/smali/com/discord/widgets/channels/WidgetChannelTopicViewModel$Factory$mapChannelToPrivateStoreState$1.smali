.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->mapChannelToPrivateStoreState(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;>;",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;>;)",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;"
        }
    .end annotation

    new-instance v7, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    const-string v0, "channel.dmRecipient"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->access$getStoreUserSettings$p(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;)Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v4

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened()Z

    move-result v5

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab()Z

    move-result v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;Ljava/util/Collection;ZZZ)V

    return-object v7
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;->call(Ljava/util/Map;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
