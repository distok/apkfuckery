.class public final Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;
.super Lx/m/c/k;
.source "WidgetChannelNotificationSettings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->configureNotificationRadios(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function3<",
        "Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;",
        "Lcom/discord/views/CheckedSetting;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;

    check-cast p2, Lcom/discord/views/CheckedSetting;

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;Lcom/discord/views/CheckedSetting;I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;Lcom/discord/views/CheckedSetting;I)V
    .locals 3

    const-string v0, "$this$configureNotificationRadio"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "radio"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1$1;

    invoke-direct {v0, p1, p3}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;I)V

    invoke-virtual {p2, v0}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_ALL:I

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const v2, 0x7f120f05

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v1}, Lcom/discord/views/CheckedSetting;->h(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/discord/views/CheckedSetting;->h(Ljava/lang/CharSequence;Z)V

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getNotificationSetting()I

    move-result v0

    if-ne v0, p3, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getNotificationSettingIsInherited()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x3e99999a    # 0.3f

    goto :goto_1

    :cond_2
    const/high16 p1, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {p2, p1}, Lcom/discord/views/CheckedSetting;->setButtonAlpha(F)V

    if-eqz v1, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->access$getNotificationSettingsRadioManager$p(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)Lcom/discord/views/RadioManager;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    :cond_3
    return-void
.end method
