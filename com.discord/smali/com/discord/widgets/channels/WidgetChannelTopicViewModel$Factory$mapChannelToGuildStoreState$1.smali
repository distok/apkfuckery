.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"

# interfaces
.implements Lrx/functions/Func5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->mapChannelToGuildStoreState(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func5<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildRole;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Boolean;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;"
        }
    .end annotation

    new-instance v9, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    const-string v0, "channels"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "users"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "members"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roles"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allowAnimatedEmojis"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget-object p5, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {p5}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened()Z

    move-result v7

    iget-object p5, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {p5}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab()Z

    move-result v8

    move-object v0, v9

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;-><init>(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZZ)V

    return-object v9
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    check-cast p2, Ljava/util/Map;

    check-cast p3, Ljava/util/Map;

    check-cast p4, Ljava/util/Map;

    check-cast p5, Ljava/lang/Boolean;

    invoke-virtual/range {p0 .. p5}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;->call(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Boolean;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;

    move-result-object p1

    return-object p1
.end method
