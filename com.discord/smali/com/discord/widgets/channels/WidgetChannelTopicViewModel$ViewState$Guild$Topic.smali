.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;
.super Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Topic"
.end annotation


# instance fields
.field private final allowAnimatedEmojis:Z

.field private final ast:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;>;"
        }
    .end annotation
.end field

.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

.field private final channelNames:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isLinkifyConflicting:Z

.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z

.field private final rawTopicString:Ljava/lang/String;

.field private final roles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation
.end field

.field private final userNames:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;Z",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;>;Z",
            "Lcom/discord/utilities/channel/GuildChannelIconType;",
            "Lcom/discord/models/domain/ModelChannel;",
            "ZZ)V"
        }
    .end annotation

    const-string v0, "rawTopicString"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelNames"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userNames"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roles"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ast"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelIconType"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p8, p10, p11, v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;-><init>(Lcom/discord/utilities/channel/GuildChannelIconType;ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    iput-object p4, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    iput-boolean p5, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    iput-object p6, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    iput-boolean p7, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    iput-object p8, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

    iput-object p9, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-boolean p10, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened:Z

    iput-boolean p11, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v9

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened()Z

    move-result v11

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab()Z

    move-result v1

    goto :goto_a

    :cond_a
    move/from16 v1, p11

    :goto_a
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object/from16 p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->copy(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened()Z

    move-result v0

    return v0
.end method

.method public final component11()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab()Z

    move-result v0

    return v0
.end method

.method public final component2()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    return-object v0
.end method

.method public final component3()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    return-object v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    return v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    return v0
.end method

.method public final component8()Lcom/discord/utilities/channel/GuildChannelIconType;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v0

    return-object v0
.end method

.method public final component9()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;Z",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;>;Z",
            "Lcom/discord/utilities/channel/GuildChannelIconType;",
            "Lcom/discord/models/domain/ModelChannel;",
            "ZZ)",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;"
        }
    .end annotation

    const-string v0, "rawTopicString"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelNames"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userNames"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roles"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ast"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelIconType"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    move-object v1, v0

    move/from16 v6, p5

    move/from16 v8, p7

    move/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v1 .. v12}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab()Z

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowAnimatedEmojis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    return v0
.end method

.method public final getAst()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    return-object v0
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

    return-object v0
.end method

.method public final getChannelNames()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    return-object v0
.end method

.method public final getRawTopicString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    return-object v0
.end method

.method public final getRoles()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    return-object v0
.end method

.method public final getUserNames()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :cond_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab()Z

    move-result v1

    if-eqz v1, :cond_a

    goto :goto_6

    :cond_a
    move v3, v1

    :goto_6
    add-int/2addr v0, v3

    return v0
.end method

.method public final isLinkifyConflicting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    return v0
.end method

.method public isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab:Z

    return v0
.end method

.method public isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Topic(rawTopicString="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->rawTopicString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", channelNames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channelNames:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userNames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->userNames:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", roles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->roles:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowAnimatedEmojis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->allowAnimatedEmojis:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ast="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->ast:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isLinkifyConflicting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", channelIconType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isRightPanelOpened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isRightPanelOpened()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isOnHomeTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isOnHomeTab()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
