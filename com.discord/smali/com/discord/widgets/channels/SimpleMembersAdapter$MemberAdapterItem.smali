.class public Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberAdapterItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "SimpleMembersAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/SimpleMembersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MemberAdapterItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/channels/SimpleMembersAdapter;",
        "Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic a:I


# instance fields
.field private memberAvatar:Landroid/widget/ImageView;

.field private memberNameTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/SimpleMembersAdapter;)V
    .locals 1

    const v0, 0x7f0d00e7

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    new-instance v0, Lf/a/o/a/c;

    invoke-direct {v0, p1}, Lf/a/o/a/c;-><init>(Lcom/discord/widgets/channels/SimpleMembersAdapter;)V

    const/4 p1, 0x0

    new-array p1, p1, [Landroid/view/View;

    invoke-virtual {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0675

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberAdapterItem;->memberNameTextView:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0672

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberAdapterItem;->memberAvatar:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberAdapterItem;->memberNameTextView:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;->access$000(Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;)Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberAdapterItem;->memberAvatar:Landroid/widget/ImageView;

    invoke-static {p2}, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;->access$000(Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;)Lcom/discord/models/domain/ModelUser;

    move-result-object p2

    const v0, 0x7f07006b

    invoke-static {p1, p2, v0}, Lcom/discord/utilities/icon/IconUtils;->setIcon(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelUser;I)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/SimpleMembersAdapter$MemberAdapterItem;->onConfigure(ILcom/discord/widgets/channels/SimpleMembersAdapter$MemberItem;)V

    return-void
.end method
