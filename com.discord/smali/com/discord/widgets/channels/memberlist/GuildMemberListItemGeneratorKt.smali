.class public final Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;
.super Ljava/lang/Object;
.source "GuildMemberListItemGenerator.kt"


# static fields
.field private static final PLACEHOLDER_INSTANCES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    const v2, 0x3f333333    # 0.7f

    invoke-direct {v1, v2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v3, 0x0

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    const v3, 0x3e99999a    # 0.3f

    invoke-direct {v1, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v4, 0x1

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    const v4, 0x3f19999a    # 0.6f

    invoke-direct {v1, v4}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v5, 0x2

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    const v5, 0x3ecccccd    # 0.4f

    invoke-direct {v1, v5}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v6, 0x3

    aput-object v1, v0, v6

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    invoke-direct {v1, v4}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v4, 0x4

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    const v4, 0x3f4ccccd    # 0.8f

    invoke-direct {v1, v4}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v4, 0x5

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    invoke-direct {v1, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v3, 0x6

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v1, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/4 v3, 0x7

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    invoke-direct {v1, v2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    invoke-direct {v1, v5}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;-><init>(F)V

    const/16 v2, 0x9

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;->PLACEHOLDER_INSTANCES:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getPLACEHOLDER_INSTANCES$p()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;->PLACEHOLDER_INSTANCES:Ljava/util/List;

    return-object v0
.end method

.method public static final generateGuildMemberListItems(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;
    .locals 6

    const-string v0, "channelMemberList"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getRows()Lcom/discord/utilities/collections/SparseMutableList;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/collections/SparseMutableList;->deepCopy(Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/SparseMutableList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {v0}, Lx/h/f;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getListId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Lcom/discord/utilities/collections/SparseMutableList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getGroupIndices()Ljava/util/SortedMap;

    move-result-object v1

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getSize()I

    move-result v3

    if-eqz p1, :cond_2

    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getListId()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f120e46

    invoke-direct {p1, v4, v5}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v2, p1}, Lcom/discord/utilities/collections/SparseMutableList;->add(ILjava/lang/Object;)V

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lf/h/a/f/f/n/g;->toSortedMap(Ljava/util/Map;)Ljava/util/SortedMap;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    :cond_2
    new-instance p1, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;

    invoke-virtual {p0}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getListId()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0, v3, v0, v1}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;-><init>(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)V

    return-object p1
.end method
