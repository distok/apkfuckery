.class public final Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ChannelMembersListAdapter.kt"

# interfaces
.implements Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;,
        Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;,
        Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;,
        Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$DiffUtilCallback;,
        Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;,
        Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;",
        "Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;"
    }
.end annotation


# instance fields
.field private memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

.field private onAddMemberClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onUserClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

.field private final updatesSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;",
            ">;"
        }
    .end annotation
.end field

.field private updatesSubscription:Lrx/Subscription;


# direct methods
.method public constructor <init>()V
    .locals 11

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->updatesSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->Companion:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList$Companion;

    invoke-virtual {v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList$Companion;->getEMPTY()Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    new-instance v2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation$OverwriteUpdate;

    new-instance v3, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, v4, v1, v5}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;-><init>(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)V

    invoke-direct {v2, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateOperation$OverwriteUpdate;-><init>(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;)V

    sget-object v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$1;

    const-string/jumbo v1, "updatesSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$2;->INSTANCE:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$2;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lg0/l/a/r1;

    invoke-direct {v3, v2, v1}, Lg0/l/a/r1;-><init>(Ljava/lang/Object;Lrx/functions/Func2;)V

    new-instance v1, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v1, v0, v3}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/Observable;->N(I)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "updatesSubject\n        .\u2026      })\n        .skip(1)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    new-instance v8, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$3;

    invoke-direct {v8, p0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$3;-><init>(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)V

    new-instance v5, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$4;

    invoke-direct {v5, p0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$4;-><init>(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1a

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getMemberList$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    return-object p0
.end method

.method public static final synthetic access$getOnAddMemberClicked$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)Lkotlin/jvm/functions/Function0;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->onAddMemberClicked:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$getOnUserClicked$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->onUserClicked:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getStickyHeadersManager$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "stickyHeadersManager"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getUpdatesSubscription$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->updatesSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setMemberList$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    return-void
.end method

.method public static final synthetic access$setOnAddMemberClicked$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->onAddMemberClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$setOnUserClicked$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->onUserClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$setStickyHeadersManager$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    return-void
.end method

.method public static final synthetic access$setUpdatesSubscription$p(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->updatesSubscription:Lrx/Subscription;

    return-void
.end method


# virtual methods
.method public final dispose()Lkotlin/Unit;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->updatesSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getAndBindHeaderView(I)Landroid/view/View;
    .locals 8

    const-string/jumbo v0, "stickyHeadersManager"

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v1, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p1

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->bindStickyHeaderView(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->getCurrentStickyHeaderView()Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :try_start_1
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :catchall_0
    nop

    goto :goto_0

    :catch_0
    :try_start_2
    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v3, "Failed to cast header"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->getCurrentStickyHeaderView()Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->getCurrentStickyHeaderView()Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_3
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public getHeaderPositionForItem(I)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->getHeaderPositionForItem(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->getSize()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;

    const-wide/16 v2, -0x1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;->getUserId()J

    move-result-wide v2

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;->getRoleId()J

    move-result-wide v2

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;->getHeaderType()Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    const-wide/16 v2, -0x4

    goto :goto_0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    const-wide/16 v2, -0x3

    goto :goto_0

    :cond_5
    const-wide/16 v2, -0x2

    goto :goto_0

    :cond_6
    instance-of v1, v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;

    if-eqz v1, :cond_7

    const-wide/16 v2, -0x5

    goto :goto_0

    :cond_7
    instance-of v0, v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderMember;

    if-eqz v0, :cond_8

    add-int/lit8 p1, p1, 0x6

    int-to-long v0, p1

    mul-long v2, v2, v0

    :goto_0
    return-wide v2

    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;->getType()Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    return p1
.end method

.method public isHeader(I)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    if-nez v0, :cond_1

    instance-of p1, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 4

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d018c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d018e

    invoke-virtual {v1, v3, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    const-string v3, "headerView"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "placeholderHeaderView"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0, v1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;-><init>(Landroid/view/View;Landroid/view/View;)V

    iput-object v2, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->stickyHeadersManager:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onAttachedToRecyclerView$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onAttachedToRecyclerView$1;-><init>(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p2

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.Header"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    invoke-virtual {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;->bind(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderMember;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p2

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.Member"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderMember;

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onBindViewHolder$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onBindViewHolder$1;-><init>(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;)V

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderMember;->bind(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading;

    invoke-virtual {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading;->bind(I)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p2

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.RoleHeader"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    invoke-virtual {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;->bind(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderAdd;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->memberList:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    invoke-interface {v0, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p2

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.channels.memberlist.adapter.ChannelMembersListAdapter.Item.AddMember"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderAdd;

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onBindViewHolder$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onBindViewHolder$2;-><init>(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;)V

    invoke-virtual {p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;->getTitle()I

    move-result p2

    invoke-virtual {p1, v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderAdd;->bind(Lkotlin/jvm/functions/Function0;I)V

    :cond_4
    :goto_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;-><init>(Landroid/view/ViewGroup;)V

    sget-object p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;->Companion:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType$Companion;

    invoke-virtual {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType$Companion;->fromOrdinal(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ViewType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const p2, 0x7f0d018c

    if-eqz p1, :cond_5

    const/4 v1, 0x1

    if-eq p1, v1, :cond_4

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 p2, 0x3

    if-eq p1, p2, :cond_2

    const/4 p2, 0x4

    if-eq p1, p2, :cond_1

    const/4 p2, 0x5

    if-ne p1, p2, :cond_0

    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading;

    const p2, 0x7f0d018d

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;->invoke(I)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderLoading;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderAdd;

    const p2, 0x7f0d018b

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;->invoke(I)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderAdd;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderMember;

    const p2, 0x7f0d018f

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;->invoke(I)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderMember;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;->invoke(I)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_4
    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderPlaceholderHeader;

    const p2, 0x7f0d018e

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;->invoke(I)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderPlaceholderHeader;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_5
    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$onCreateViewHolder$1;->invoke(I)Landroid/view/View;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;-><init>(Landroid/view/View;)V

    :goto_0
    return-object p1
.end method

.method public final setData(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)V
    .locals 2

    const-string v0, "listId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rows"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->updatesSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;

    invoke-direct {v1, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$ListUpdateRequest;-><init>(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final setOnAddMemberClicked(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "addMember"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->onAddMemberClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setOnUserClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->onUserClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method
