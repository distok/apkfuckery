.class public final Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;
.super Ljava/lang/Object;
.source "ChannelMembersListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StickyHeadersManager"
.end annotation


# instance fields
.field private currentStickyHeaderView:Landroid/view/View;

.field private final headerView:Landroid/view/View;

.field private final onlineOfflineStickyHeader:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

.field private final placeholderHeaderView:Landroid/view/View;

.field private final roleStickyHeader:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    const-string v0, "headerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "placeholderHeaderView"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->headerView:Landroid/view/View;

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->placeholderHeaderView:Landroid/view/View;

    new-instance p2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;

    invoke-direct {p2, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->roleStickyHeader:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;

    new-instance p2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

    invoke-direct {p2, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->onlineOfflineStickyHeader:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->currentStickyHeaderView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final bindStickyHeaderView(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;)V
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->roleStickyHeader:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderRoleHeader;->bind(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->headerView:Landroid/view/View;

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->currentStickyHeaderView:Landroid/view/View;

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->onlineOfflineStickyHeader:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;

    check-cast p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListViewHolderHeader;->bind(Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->headerView:Landroid/view/View;

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->currentStickyHeaderView:Landroid/view/View;

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$PlaceholderHeader;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->placeholderHeaderView:Landroid/view/View;

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->currentStickyHeaderView:Landroid/view/View;

    goto :goto_0

    :cond_2
    instance-of p1, p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->currentStickyHeaderView:Landroid/view/View;

    :cond_3
    :goto_0
    return-void
.end method

.method public final getCurrentStickyHeaderView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->currentStickyHeaderView:Landroid/view/View;

    return-object v0
.end method

.method public final layoutViews(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 2

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/views/StickyHeaderItemDecoration$LayoutManager;->INSTANCE:Lcom/discord/utilities/views/StickyHeaderItemDecoration$LayoutManager;

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->headerView:Landroid/view/View;

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/views/StickyHeaderItemDecoration$LayoutManager;->layoutHeaderView(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$StickyHeadersManager;->placeholderHeaderView:Landroid/view/View;

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/views/StickyHeaderItemDecoration$LayoutManager;->layoutHeaderView(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method
