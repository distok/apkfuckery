.class public final Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;
.super Ljava/lang/Object;
.source "PrivateChannelMemberListService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;
    }
.end annotation


# instance fields
.field private final storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

.field private final storePresences:Lcom/discord/stores/StoreUserPresence;

.field private final storeUser:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;)V
    .locals 1

    const-string/jumbo v0, "storeUser"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storePresences"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeApplicationStreaming"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storePresences:Lcom/discord/stores/StoreUserPresence;

    iput-object p3, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object p3

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;)V

    return-void
.end method

.method public static final synthetic access$getStoreApplicationStreaming$p(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;)Lcom/discord/stores/StoreApplicationStreaming;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    return-object p0
.end method

.method public static final synthetic access$getStorePresences$p(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;)Lcom/discord/stores/StoreUserPresence;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storePresences:Lcom/discord/stores/StoreUserPresence;

    return-object p0
.end method

.method public static final synthetic access$getStoreUser$p(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;)Lcom/discord/stores/StoreUser;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storeUser:Lcom/discord/stores/StoreUser;

    return-object p0
.end method


# virtual methods
.method public final observeStateForGroupDm(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;",
            ">;"
        }
    .end annotation

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v2, v3, v1}, Lrx/Observable;->X(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getRecipients()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/Observable;->E(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;-><init>(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable.merge(\n      \u2026            }\n          }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
