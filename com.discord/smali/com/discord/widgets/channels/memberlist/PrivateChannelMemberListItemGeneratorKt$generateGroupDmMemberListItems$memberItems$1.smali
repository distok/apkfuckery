.class public final synthetic Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;
.super Lx/m/c/i;
.source "PrivateChannelMemberListItemGenerator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt;->generateGroupDmMemberListItems(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;

    invoke-direct {v0}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Ljava/lang/String;

    const/4 v1, 0x2

    const-string v3, "compareTo"

    const-string v4, "compareTo(Ljava/lang/String;)I"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;->invoke(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
