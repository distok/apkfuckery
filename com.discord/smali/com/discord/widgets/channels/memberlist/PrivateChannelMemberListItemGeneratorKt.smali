.class public final Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt;
.super Ljava/lang/Object;
.source "PrivateChannelMemberListItemGenerator.kt"


# direct methods
.method private static final createGroupDmHeader(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;
    .locals 3

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    sget-object v1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;->GROUP_DM:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;

    const-string v2, "%group_header_key"

    invoke-direct {v0, v2, v1, p0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;-><init>(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;I)V

    return-object v0
.end method

.method private static final createMemberListItem(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;ZLjava/lang/String;Z)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;
    .locals 15

    if-eqz p3, :cond_0

    move-object/from16 v4, p3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x6

    const/4 v2, 0x0

    move-object v3, p0

    invoke-static {p0, v0, v2, v1, v2}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f1217ce

    goto :goto_1

    :cond_1
    const v0, 0x7f120384

    :goto_1
    new-instance v14, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    const-string v1, "name"

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->isBot()Z

    move-result v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->isVerifiedBot()Z

    move-result v8

    const/4 v9, 0x0

    const/4 v12, 0x0

    move-object v1, v14

    move-wide v2, v5

    move v5, v7

    move-object v6, v0

    move v7, v8

    move-object/from16 v8, p1

    move/from16 v11, p2

    move/from16 v13, p4

    invoke-direct/range {v1 .. v13}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;-><init>(JLjava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/domain/ModelPresence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;Z)V

    return-object v14
.end method

.method public static final generateGroupDmMemberListItems(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;Z)",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;"
        }
    .end annotation

    const-string v0, "channel"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "users"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presences"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applicationStreams"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$generateGroupDmMemberListItems$memberItems$1;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$sam$java_util_Comparator$0;

    invoke-direct {v2, v1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt$sam$java_util_Comparator$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelUser;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getUsernameLower()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getDiscriminatorWithPadding()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getNicks()Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {v2, v5}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelChannel$RecipientNick;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel$RecipientNick;->getNick()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getOwnerId()J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v8

    cmp-long v10, v6, v8

    if-nez v10, :cond_2

    const/4 v3, 0x1

    :cond_2
    invoke-static {v2, p2}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v2, v6, v3, v5, v7}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt;->createMemberListItem(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;ZLjava/lang/String;Z)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    new-instance p2, Ljava/util/ArrayList;

    add-int/lit8 p3, p1, 0x1

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz p4, :cond_5

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isGroup()Z

    move-result p3

    if-eqz p3, :cond_4

    const p3, 0x7f120994

    goto :goto_2

    :cond_4
    const p3, 0x7f12056e

    :goto_2
    new-instance p4, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p4, v1, p3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$AddMember;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v3, p4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_5
    invoke-static {p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt;->createGroupDmHeader(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance p1, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItems;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide p3

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0, p2}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItems;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1
.end method
