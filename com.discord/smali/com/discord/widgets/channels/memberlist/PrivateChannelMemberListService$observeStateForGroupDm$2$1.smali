.class public final Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2$1;
.super Ljava/lang/Object;
.source "PrivateChannelMemberListService.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->call(Ljava/util/List;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelPresence;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelApplicationStream;",
        ">;",
        "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2$1;->this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;)",
            "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2$1;->this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;

    iget-object v1, v1, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    const-string/jumbo v2, "users"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "presences"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "applicationStreams"

    invoke-static {p3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;-><init>(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    check-cast p2, Ljava/util/Map;

    check-cast p3, Ljava/util/Map;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2$1;->call(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;

    move-result-object p1

    return-object p1
.end method
