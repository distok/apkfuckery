.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelMembersList.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$Companion;

.field private static final SOURCE:Ljava/lang/String; = "WidgetChannelMembersList"


# instance fields
.field private adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

.field private memberCellHeightPx:I

.field private final recycler$delegate:Lkotlin/Lazy;

.field private final scrollListener:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;

.field private viewModel:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->Companion:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$recycler$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$recycler$2;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->recycler$delegate:Lkotlin/Lazy;

    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;

    invoke-direct {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->scrollListener:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->configureUI(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$scrollToTop(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->scrollToTop()V

    return-void
.end method

.method public static final synthetic access$updateRanges(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->updateRanges()V

    return-void
.end method

.method private final configureLoadedUI(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;)V
    .locals 4

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getChannelId()Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "adapter"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    if-eqz v0, :cond_1

    new-instance v3, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$configureLoadedUI$1;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$configureLoadedUI$1;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->setOnUserClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    if-eqz v0, :cond_0

    new-instance v3, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$configureLoadedUI$2;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$configureLoadedUI$2;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->setOnAddMemberClicked(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getListItems()Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;->getListId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getListItems()Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    move-result-object p1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->setData(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;Z)V

    return-void

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final configureUI(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->configureLoadedUI(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final getRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->recycler$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final scrollToTop()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method private final setupRecyclerView()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->memberCellHeightPx:I

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->scrollListener:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->scrollListener:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    new-instance v0, Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    const/4 v2, 0x0

    const-string v3, "adapter"

    if-eqz v1, :cond_1

    invoke-direct {v0, v1}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;-><init>(Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->blockClicks(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_0
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final updateRanges()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->memberCellHeightPx:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v2

    sub-int/2addr v2, v0

    const/4 v3, 0x0

    if-gez v2, :cond_0

    const/4 v2, 0x0

    :cond_0
    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->viewModel:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    if-eqz v0, :cond_1

    new-instance v3, Lkotlin/ranges/IntRange;

    invoke-direct {v3, v2, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->updateSubscriptions(Lkotlin/ranges/IntRange;)V

    return-void

    :cond_1
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d018a

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    invoke-direct {p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    return-void

    :cond_0
    const-string p1, "adapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->adapter:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter;->dispose()Lkotlin/Unit;

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void

    :cond_0
    const-string v0, "adapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->setupRecyclerView()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    new-instance v12, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7f

    const/4 v11, 0x0

    move-object v2, v12

    invoke-direct/range {v2 .. v11}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelMembers;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserRelationships;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, v1, v12}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(requir\u2026istViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    iput-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->viewModel:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    const-string/jumbo v1, "viewModel"

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->viewModel:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->scrollListener:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$RxOnScrollListener;->observeScrollChanges()Lrx/Observable;

    move-result-object v0

    const-wide/16 v3, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v1}, Lrx/Observable;->o(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "scrollListener\n        .\u2026S, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;

    new-instance v9, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$3;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$3;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
