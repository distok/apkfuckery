.class public final Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;
.super Ljava/lang/Object;
.source "PrivateChannelMemberListService.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->observeStateForGroupDm(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/List<",
        "Ljava/lang/Long;",
        ">;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->call(Ljava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/List;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    invoke-static {v0}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->access$getStoreUser$p(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;)Lcom/discord/stores/StoreUser;

    move-result-object v0

    const-string v1, "ids"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->observeUsers(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    invoke-static {v1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->access$getStorePresences$p(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;)Lcom/discord/stores/StoreUserPresence;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/discord/stores/StoreUserPresence;->observePresencesForUsers(Ljava/util/Collection;)Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;->this$0:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    invoke-static {v1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->access$getStoreApplicationStreaming$p(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;)Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming;->getStreamsByUser()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2$1;-><init>(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$observeStateForGroupDm$2;)V

    invoke-static {v0, p1, v1, v2}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
