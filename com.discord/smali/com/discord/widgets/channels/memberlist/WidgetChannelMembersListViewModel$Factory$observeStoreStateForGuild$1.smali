.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;
.super Ljava/lang/Object;
.source "WidgetChannelMembersListViewModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->observeStoreStateForGuild(ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Ljava/lang/Long;",
        "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;",
        "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isPanelOpen:Z

.field public final synthetic $selectedChannel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $selectedGuild:Lcom/discord/models/domain/ModelGuild;


# direct methods
.method public constructor <init>(ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->$isPanelOpen:Z

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->$selectedGuild:Lcom/discord/models/domain/ModelGuild;

    iput-object p3, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->$selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Long;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;
    .locals 9

    new-instance v8, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->$isPanelOpen:Z

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->$selectedGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->$selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    const-string v0, "channelMemberList"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v8

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;-><init>(ZJJLcom/discord/utilities/lazy/memberlist/ChannelMemberList;Ljava/lang/Long;)V

    return-object v8
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    check-cast p2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;->call(Ljava/lang/Long;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;

    move-result-object p1

    return-object p1
.end method
