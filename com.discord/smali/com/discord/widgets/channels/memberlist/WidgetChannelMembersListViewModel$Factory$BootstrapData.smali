.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;
.super Ljava/lang/Object;
.source "WidgetChannelMembersListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BootstrapData"
.end annotation


# instance fields
.field private final isPanelOpen:Z

.field private final selectedChannel:Lcom/discord/models/domain/ModelChannel;

.field private final selectedGuild:Lcom/discord/models/domain/ModelGuild;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    iput-boolean p3, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->copy(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSelectedChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getSelectedGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isPanelOpen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "BootstrapData(selectedGuild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isPanelOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
