.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;
.super Ljava/lang/Object;
.source "WidgetChannelMembersListViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;->call(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->getSelectedGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->getSelectedChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;->isPanelOpen()Z

    move-result p1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;

    invoke-static {v2, p1, v0, v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->access$observeStoreStateForGuild(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;

    invoke-static {v0, p1, v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->access$observeStoreStateForPrivateChannel(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;ZLcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;

    invoke-direct {v0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;-><init>(Z)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_0
    return-object p1
.end method
