.class public final Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;
.super Lx/m/c/k;
.source "GuildMemberListItemGenerator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;->generateGuildMemberListItems(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
        "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;

    invoke-direct {v0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;
    .locals 14

    const-string v0, "row"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;->getRowId()Ljava/lang/String;

    move-result-object v1

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getType()Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;->OFFLINE:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    sget-object v2, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;->ONLINE:Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;

    :goto_0
    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;->getMemberCount()I

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header;-><init>(Ljava/lang/String;Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Header$Type;I)V

    goto :goto_1

    :cond_2
    instance-of v0, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getUserId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->isBot()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getTagText()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getTagVerified()Z

    move-result v7

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getColor()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getAvatarUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getShowOwnerIndicator()Z

    move-result v11

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->getPremiumSince()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;->isApplicationStreaming()Z

    move-result v13

    move-object v1, v0

    invoke-direct/range {v1 .. v13}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$Member;-><init>(JLjava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/domain/ModelPresence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;Z)V

    goto :goto_1

    :cond_3
    instance-of v0, p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->getRoleId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->getRoleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;->getMemberCount()I

    move-result p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item$RoleHeader;-><init>(JLjava/lang/String;I)V

    :goto_1
    return-object v0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt$generateGuildMemberListItems$listItems$1;->invoke(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    move-result-object p1

    return-object p1
.end method
