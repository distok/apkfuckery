.class public final Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;
.super Ljava/lang/Object;
.source "GuildMemberListItemGenerator.kt"

# interfaces
.implements Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;


# instance fields
.field private final groupIndices:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final listId:Ljava/lang/String;

.field private final listItems:Lcom/discord/utilities/collections/SparseMutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final size:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;",
            ">;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listItems"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupIndices"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listId:Ljava/lang/String;

    iput p2, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->size:I

    iput-object p3, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    iput-object p4, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    return-void
.end method

.method private final component3()Lcom/discord/utilities/collections/SparseMutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    return-object v0
.end method

.method private final component4()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;ILjava/lang/Object;)Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getListId()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getSize()I

    move-result p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->copy(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getListId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getSize()I

    move-result v0

    return v0
.end method

.method public final copy(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/discord/utilities/collections/SparseMutableList<",
            "Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;",
            ">;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;"
        }
    .end annotation

    const-string v0, "listId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listItems"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupIndices"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;-><init>(Ljava/lang/String;ILcom/discord/utilities/collections/SparseMutableList;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getListId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getListId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getSize()I

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getSize()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    iget-object v1, p1, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    iget-object p1, p1, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public get(I)Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/collections/SparseMutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;->access$getPLACEHOLDER_INSTANCES$p()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;->access$getPLACEHOLDER_INSTANCES$p()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/memberlist/adapter/ChannelMembersListAdapter$Item;

    :goto_0
    return-object v0
.end method

.method public getHeaderPositionForItem(I)Ljava/lang/Integer;
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    const-string v1, "groupIndices.keys"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    add-int/lit8 v5, p1, 0x1

    if-ge v3, v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_2
    check-cast v1, Ljava/lang/Integer;

    return-object v1
.end method

.method public getListId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listId:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->size:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getListId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getSize()I

    move-result v2

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v2, v1

    return v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "GuildMemberListItems(listId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getListId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->getSize()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", listItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->listItems:Lcom/discord/utilities/collections/SparseMutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", groupIndices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/GuildMemberListItems;->groupIndices:Ljava/util/SortedMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
