.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;
.super Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;
.source "WidgetChannelMembersListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Empty"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;

    invoke-direct {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;-><init>()V

    sput-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "empty"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
