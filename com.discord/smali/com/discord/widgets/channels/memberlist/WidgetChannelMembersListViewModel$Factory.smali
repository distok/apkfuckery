.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetChannelMembersListViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$BootstrapData;
    }
.end annotation


# instance fields
.field private final privateChannelMemberListService:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

.field private final storeChannelMembers:Lcom/discord/stores/StoreChannelMembers;

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeGuildSelected:Lcom/discord/stores/StoreGuildSelected;

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;

.field private final storePermissions:Lcom/discord/stores/StorePermissions;

.field private final storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelMembers;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserRelationships;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreChannelMembers;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserRelationships;)V
    .locals 1

    const-string/jumbo v0, "storeChannelMembers"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuildSelected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeNavigation"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "privateChannelMemberListService"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storePermissions"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserRelationships"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeChannelMembers:Lcom/discord/stores/StoreChannelMembers;

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeGuildSelected:Lcom/discord/stores/StoreGuildSelected;

    iput-object p3, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p4, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object p5, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->privateChannelMemberListService:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    iput-object p6, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storePermissions:Lcom/discord/stores/StorePermissions;

    iput-object p7, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreChannelMembers;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserRelationships;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 11

    and-int/lit8 v0, p8, 0x1

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelMembers()Lcom/discord/stores/StoreChannelMembers;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    and-int/lit8 v1, p8, 0x2

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, p2

    :goto_1
    and-int/lit8 v2, p8, 0x4

    if-eqz v2, :cond_2

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v2

    goto :goto_2

    :cond_2
    move-object v2, p3

    :goto_2
    and-int/lit8 v3, p8, 0x8

    if-eqz v3, :cond_3

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object v3

    goto :goto_3

    :cond_3
    move-object v3, p4

    :goto_3
    and-int/lit8 v4, p8, 0x10

    if-eqz v4, :cond_4

    new-instance v4, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x7

    const/4 v10, 0x0

    move-object v5, v4

    invoke-direct/range {v5 .. v10}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreApplicationStreaming;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_4

    :cond_4
    move-object/from16 v4, p5

    :goto_4
    and-int/lit8 v5, p8, 0x20

    if-eqz v5, :cond_5

    sget-object v5, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v5}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v5

    goto :goto_5

    :cond_5
    move-object/from16 v5, p6

    :goto_5
    and-int/lit8 v6, p8, 0x40

    if-eqz v6, :cond_6

    sget-object v6, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v6}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v6

    goto :goto_6

    :cond_6
    move-object/from16 v6, p7

    :goto_6
    move-object p1, p0

    move-object p2, v0

    move-object p3, v1

    move-object p4, v2

    move-object/from16 p5, v3

    move-object/from16 p6, v4

    move-object/from16 p7, v5

    move-object/from16 p8, v6

    invoke-direct/range {p1 .. p8}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelMembers;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserRelationships;)V

    return-void
.end method

.method public static final synthetic access$getStoreUserRelationships$p(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;)Lcom/discord/stores/StoreUserRelationships;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    return-object p0
.end method

.method public static final synthetic access$observeStoreStateForGuild(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->observeStoreStateForGuild(ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$observeStoreStateForPrivateChannel(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;ZLcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->observeStoreStateForPrivateChannel(ZLcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeGuildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSelected;->observeSelectedGuild()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v2}, Lcom/discord/stores/StoreNavigation;->observeRightPanelState()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$1;

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$2;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreState$3;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n          .co\u2026            }\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final observeStoreStateForGuild(ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storePermissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->storeChannelMembers:Lcom/discord/stores/StoreChannelMembers;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/discord/stores/StoreChannelMembers;->get(JJ)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;

    invoke-direct {v2, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForGuild$1;-><init>(ZLcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;)V

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026issions\n        )\n      }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final observeStoreStateForPrivateChannel(ZLcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->privateChannelMemberListService:Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;

    invoke-virtual {v0, p2}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService;->observeStateForGroupDm(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;ZLcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "privateChannelMemberList\u2026            )\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    new-instance v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$create$1;

    invoke-direct {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$create$1;-><init>()V

    invoke-direct {p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;Lrx/Observable;)V

    return-object p1
.end method
