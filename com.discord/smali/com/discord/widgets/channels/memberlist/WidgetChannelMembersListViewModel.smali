.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;
.super Lf/a/b/l0;
.source "WidgetChannelMembersListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final guildChannelRangeSubscriber:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;Lrx/Observable;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "guildChannelRangeSubscriber"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->guildChannelRangeSubscriber:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x2

    invoke-static {p1, p0, p2, v0, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;

    new-instance v7, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$1;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->handleStoreState(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;)V
    .locals 13
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Empty;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    const-wide/16 v4, 0x1

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;->getChannelPermissions()Ljava/lang/Long;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v4

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;->getChannelMembers()Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/discord/widgets/channels/memberlist/GuildMemberListItemGeneratorKt;->generateGuildMemberListItems(Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    move-result-object v0

    goto :goto_3

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;

    if-eqz v0, :cond_a

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getRelationships()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    :cond_2
    move-object v5, v3

    :goto_0
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-static {v4, v2}, Lcom/discord/models/domain/ModelUserRelationship;->isType(Ljava/lang/Integer;I)Z

    move-result v4

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->isGroup()Z

    move-result v5

    if-nez v5, :cond_4

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getUsers()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getPresences()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;->getApplicationStreams()Ljava/util/Map;

    move-result-object v0

    invoke-static {v5, v6, v7, v0, v4}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListItemGeneratorKt;->generateGroupDmMemberListItems(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;

    move-result-object v0

    :goto_3
    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;

    new-instance v5, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->isPanelOpen()Z

    move-result v6

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->getChannelId()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->getGuildId()Ljava/lang/Long;

    move-result-object v8

    if-nez v8, :cond_5

    goto :goto_4

    :cond_5
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v12, v8, v10

    if-nez v12, :cond_6

    move-object v8, v3

    goto :goto_5

    :cond_6
    :goto_4
    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->getGuildId()Ljava/lang/Long;

    move-result-object v8

    :goto_5
    invoke-direct {v5, v0, v6, v7, v8}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;-><init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$MemberList;ZLjava/lang/Long;Ljava/lang/Long;)V

    invoke-virtual {p0, v5}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;->getListId()Ljava/lang/String;

    move-result-object v3

    :cond_7
    invoke-virtual {v5}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState;->getListId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->isPanelOpen()Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$ScrollToTop;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$ScrollToTop;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    new-instance p1, Lkotlin/ranges/IntRange;

    const/16 v0, 0x63

    invoke-direct {p1, v1, v0}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->updateSubscriptions(Lkotlin/ranges/IntRange;)V

    goto :goto_6

    :cond_8
    instance-of p1, v4, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    if-eqz p1, :cond_9

    check-cast v4, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    invoke-virtual {v4}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->isOpen()Z

    move-result p1

    if-nez p1, :cond_9

    invoke-virtual {v5}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->isOpen()Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$UpdateRanges;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$UpdateRanges;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_9
    :goto_6
    return-void

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final updateSubscriptions(Lkotlin/ranges/IntRange;)V
    .locals 8
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "range"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getChannelId()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->isOpen()Z

    move-result v1

    if-nez v1, :cond_3

    return-void

    :cond_3
    iget-object v2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;->guildChannelRangeSubscriber:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$ViewState$Loaded;->getChannelId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object v7, p1

    invoke-interface/range {v2 .. v7}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$GuildChannelRangeSubscriber;->subscribeToRange(JJLkotlin/ranges/IntRange;)V

    :cond_4
    return-void
.end method
