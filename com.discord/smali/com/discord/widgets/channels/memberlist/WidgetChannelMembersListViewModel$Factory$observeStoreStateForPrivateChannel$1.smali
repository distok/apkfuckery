.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;
.super Ljava/lang/Object;
.source "WidgetChannelMembersListViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->observeStoreStateForPrivateChannel(ZLcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;",
        "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isPanelOpen:Z

.field public final synthetic $selectedChannel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;ZLcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;

    iput-boolean p2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->$isPanelOpen:Z

    iput-object p3, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->$selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;
    .locals 10

    new-instance v9, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->$isPanelOpen:Z

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->$selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;->getUsers()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;->getPresences()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;->getApplicationStreams()Ljava/util/Map;

    move-result-object v7

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;

    invoke-static {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;->access$getStoreUserRelationships$p(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory;)Lcom/discord/stores/StoreUserRelationships;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreUserRelationships;->getRelationships()Ljava/util/Map;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;-><init>(ZJLcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v9
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Factory$observeStoreStateForPrivateChannel$1;->call(Lcom/discord/widgets/channels/memberlist/PrivateChannelMemberListService$State;)Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;

    move-result-object p1

    return-object p1
.end method
