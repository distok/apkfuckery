.class public final Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;
.super Lx/m/c/k;
.source "WidgetChannelMembersList.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;->invoke(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$ScrollToTop;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$ScrollToTop;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;

    invoke-static {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->access$scrollToTop(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$UpdateRanges;->INSTANCE:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$Event$UpdateRanges;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;

    invoke-static {p1}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;->access$updateRanges(Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersList;)V

    :cond_1
    :goto_0
    return-void
.end method
