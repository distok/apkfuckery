.class public abstract Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;
.super Ljava/lang/Object;
.source "WidgetChannelMembersListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "StoreState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$None;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Guild;,
        Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState$Private;
    }
.end annotation


# instance fields
.field private final channelId:Ljava/lang/Long;

.field private final guildId:Ljava/lang/Long;

.field private final isPanelOpen:Z


# direct methods
.method private constructor <init>(ZLjava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->isPanelOpen:Z

    iput-object p2, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->guildId:Ljava/lang/Long;

    iput-object p3, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->channelId:Ljava/lang/Long;

    return-void
.end method

.method public synthetic constructor <init>(ZLjava/lang/Long;Ljava/lang/Long;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;-><init>(ZLjava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public getChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public isPanelOpen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/memberlist/WidgetChannelMembersListViewModel$StoreState;->isPanelOpen:Z

    return v0
.end method
