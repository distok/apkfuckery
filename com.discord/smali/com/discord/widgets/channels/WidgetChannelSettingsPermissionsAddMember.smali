.class public Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelSettingsPermissionsAddMember.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;
    }
.end annotation


# static fields
.field private static final INTENT_EXTRA_CHANNEL_ID:Ljava/lang/String; = "INTENT_EXTRA_CHANNEL_ID"

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field private static final VIEW_INDEX_MEMBER_LIST:I = 0x0

.field private static final VIEW_INDEX_NO_RESULTS:I = 0x1

.field public static final synthetic d:I


# instance fields
.field private membersAdapter:Lcom/discord/widgets/channels/SimpleMembersAdapter;

.field private membersRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private final nameFilterPublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private searchBox:Lcom/google/android/material/textfield/TextInputLayout;

.field private viewFlipper:Landroid/widget/ViewFlipper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const-string v0, ""

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->nameFilterPublisher:Lrx/subjects/Subject;

    return-void
.end method

.method private configureToolbar(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1

    const v0, 0x7f12008d

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)V
    .locals 3

    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->access$000(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->configureToolbar(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->membersAdapter:Lcom/discord/widgets/channels/SimpleMembersAdapter;

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->access$200(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lf/a/o/a/q0;

    invoke-direct {v2, p0, p1}, Lf/a/o/a/q0;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/channels/SimpleMembersAdapter;->setData(Ljava/util/List;Lrx/functions/Action1;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->viewFlipper:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->access$200(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    :cond_1
    return-void

    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_3
    return-void
.end method

.method public static create(Landroid/content/Context;JJ)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p1, "INTENT_EXTRA_CHANNEL_ID"

    invoke-virtual {v0, p1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-class p1, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;

    invoke-static {p0, p1, v0}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->configureUI(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)V

    return-void
.end method


# virtual methods
.method public synthetic g(Landroid/text/Editable;)Lkotlin/Unit;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->nameFilterPublisher:Lrx/subjects/Subject;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d019a

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a01b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->membersRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a01b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->searchBox:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f0a01ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ViewFlipper;

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled()Landroidx/appcompat/widget/Toolbar;

    new-instance p1, Lcom/discord/widgets/channels/SimpleMembersAdapter;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->membersRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p1, v0}, Lcom/discord/widgets/channels/SimpleMembersAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-static {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/SimpleMembersAdapter;

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->membersAdapter:Lcom/discord/widgets/channels/SimpleMembersAdapter;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 8

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "INTENT_EXTRA_CHANNEL_ID"

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->searchBox:Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v5, Lf/a/o/a/p0;

    invoke-direct {v5, p0}, Lf/a/o/a/p0;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;)V

    invoke-static {v4, p0, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    iget-object v4, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->nameFilterPublisher:Lrx/subjects/Subject;

    iget-object v5, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->searchBox:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->nameFilterPublisher:Lrx/subjects/Subject;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x2ee

    invoke-virtual {v4, v6, v7, v5}, Lrx/Observable;->o(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v4

    sget-object v5, Lf/a/o/a/o0;->d:Lf/a/o/a/o0;

    invoke-virtual {v4, v5}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    new-instance v5, Lf/a/o/a/r0;

    invoke-direct {v5, v0, v1}, Lf/a/o/a/r0;-><init>(J)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v5, v6}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v5

    invoke-virtual {v4, v5}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    iget-object v4, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->nameFilterPublisher:Lrx/subjects/Subject;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->get(JJLrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/a/e0;

    invoke-direct {v1, p0}, Lf/a/o/a/e0;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
