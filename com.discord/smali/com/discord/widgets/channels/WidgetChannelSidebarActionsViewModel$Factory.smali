.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetChannelSidebarActionsViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeGuildNSFW:Lcom/discord/stores/StoreGuildsNsfw;

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;)V
    .locals 1

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeNavigation"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserGuildSettings"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuildNSFW"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    iput-object p4, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeGuildNSFW:Lcom/discord/stores/StoreGuildsNsfw;

    iput-object p5, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuildsNsfw()Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-direct/range {p2 .. p7}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;)V

    return-void
.end method

.method public static final synthetic access$getStoreGuildNSFW$p(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lcom/discord/stores/StoreGuildsNsfw;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeGuildNSFW:Lcom/discord/stores/StoreGuildsNsfw;

    return-object p0
.end method

.method public static final synthetic access$getStoreUser$p(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lcom/discord/stores/StoreUser;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    return-object p0
.end method

.method public static final synthetic access$getStoreUserGuildSettings$p(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lcom/discord/stores/StoreUserGuildSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    return-object p0
.end method

.method public static final synthetic access$observeStoreState(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lrx/Observable;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final observeNavState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreNavigation;->observeRightPanelState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeNavState$1;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeNavState$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "storeNavigation.observeR\u2026= PanelState.Opened\n    }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "storeChannelsSelected\n  \u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->observeNavState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "observeNavState().switch\u2026State().take(1)\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;-><init>(Lrx/Observable;)V

    return-object p1
.end method
