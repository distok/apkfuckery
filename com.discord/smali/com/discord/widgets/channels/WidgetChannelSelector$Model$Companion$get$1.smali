.class public final Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion$get$1;
.super Ljava/lang/Object;
.source "WidgetChannelSelector.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion;->get(JZLcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $filterFunction:Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;

.field public final synthetic $includeNoChannel:Z


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion$get$1;->$filterFunction:Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion$get$1;->$includeNoChannel:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion$get$1;->call(Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/Map;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion$get$1;->$filterFunction:Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/models/domain/ModelChannel;

    invoke-interface {v0, v3}, Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;->includeChannel(Lcom/discord/models/domain/ModelChannel;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-boolean p1, p0, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion$get$1;->$includeNoChannel:Z

    if-eqz p1, :cond_2

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    :cond_2
    sget-object p1, Lx/h/l;->d:Lx/h/l;

    :goto_1
    invoke-static {}, Lcom/discord/models/domain/ModelChannel;->getSortByNameAndType()Ljava/util/Comparator;

    move-result-object v0

    const-string v2, "ModelChannel.getSortByNameAndType()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelChannel;

    new-instance v3, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;

    invoke-direct {v3, v2}, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Item;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {p1, v1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
