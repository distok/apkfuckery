.class public Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;
.super Ljava/lang/Object;
.source "WidgetChannelSettingsEditPermissions.java"

# interfaces
.implements Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ModelForRole"
.end annotation


# instance fields
.field private final base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

.field private final guildRoles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation
.end field

.field private final isEveryoneRole:Z

.field private final meHasRole:Z

.field private final myPermissionsWithRoleDenied:J

.field private final myPermissionsWithRoleNeutral:J

.field private final roleId:Ljava/lang/Long;


# direct methods
.method private constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;J)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;J)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v1, p1

    iput-object v1, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    move-object/from16 v10, p3

    iput-object v10, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->guildRoles:Ljava/util/Map;

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    invoke-static/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$000(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)Lcom/discord/models/domain/ModelUser;

    move-result-object v11

    invoke-static/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)Lcom/discord/models/domain/ModelGuild;

    move-result-object v12

    new-instance v13, Ljava/util/HashMap;

    invoke-static/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$200(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v13, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v1, Lcom/discord/models/domain/ModelPermissionOverwrite;

    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    const-wide/32 v8, 0x7ff7feff

    move-object v2, v1

    move-wide/from16 v4, p4

    invoke-direct/range {v2 .. v9}, Lcom/discord/models/domain/ModelPermissionOverwrite;-><init>(IJJJ)V

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v13, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v11}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v12}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-virtual {v12}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v5

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object v9, v13

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/permissions/PermissionUtils;->computePermissions(JJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleDenied:J

    new-instance v1, Lcom/discord/models/domain/ModelPermissionOverwrite;

    const/4 v15, 0x0

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-object v14, v1

    move-wide/from16 v16, p4

    invoke-direct/range {v14 .. v21}, Lcom/discord/models/domain/ModelPermissionOverwrite;-><init>(IJJJ)V

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v13, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v11}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v12}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-virtual {v12}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v5

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/permissions/PermissionUtils;->computePermissions(JJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleNeutral:J

    invoke-virtual {v12}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    cmp-long v5, p4, v1

    if-nez v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->isEveryoneRole:Z

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v2

    if-nez v1, :cond_2

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :cond_2
    :goto_1
    iput-boolean v3, v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->meHasRole:Z

    return-void
.end method

.method public static synthetic a(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;JLjava/util/Map;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;
    .locals 7

    if-eqz p4, :cond_0

    new-instance v6, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;J)V

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    return-object v6
.end method

.method public static get(JJJ)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$Model;",
            ">;"
        }
    .end annotation

    invoke-static {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$300(JJ)Lrx/Observable;

    move-result-object p2

    new-instance p3, Lf/a/o/a/w;

    invoke-direct {p3, p0, p1, p4, p5}, Lf/a/o/a/w;-><init>(JJ)V

    invoke-virtual {p2, p3}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public canDenyRolePermission(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleDenied:J

    and-long/2addr v0, p1

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-static {v2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$500(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)J

    move-result-wide v2

    and-long/2addr p1, v2

    cmp-long v2, v0, p1

    if-nez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;

    return p1
.end method

.method public canNeutralizeRolePermission(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleNeutral:J

    and-long/2addr v0, p1

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-static {v2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$500(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)J

    move-result-wide v2

    and-long/2addr p1, v2

    cmp-long v2, v0, p1

    if-nez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    iget-object v3, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    return v2

    :cond_4
    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->guildRoles:Ljava/util/Map;

    iget-object v3, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->guildRoles:Ljava/util/Map;

    if-nez v1, :cond_5

    if-eqz v3, :cond_6

    goto :goto_1

    :cond_5
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_1
    return v2

    :cond_6
    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    iget-object v3, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    if-nez v1, :cond_7

    if-eqz v3, :cond_8

    goto :goto_2

    :cond_7
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    :goto_2
    return v2

    :cond_8
    iget-wide v3, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleNeutral:J

    iget-wide v5, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleNeutral:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_9

    return v2

    :cond_9
    iget-wide v3, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleDenied:J

    iget-wide v5, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleDenied:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_a

    return v2

    :cond_a
    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->meHasRole:Z

    iget-boolean v3, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->meHasRole:Z

    if-eq v1, v3, :cond_b

    return v2

    :cond_b
    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->isEveryoneRole:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->isEveryoneRole:Z

    if-eq v1, p1, :cond_c

    return v2

    :cond_c
    return v0
.end method

.method public getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->access$200(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    return-object v0
.end method

.method public getTargetId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 7

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    const/16 v1, 0x2b

    if-nez v0, :cond_0

    const/16 v0, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->hashCode()I

    move-result v0

    :goto_0
    const/16 v2, 0x3b

    add-int/2addr v0, v2

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->guildRoles:Ljava/util/Map;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_1

    const/16 v3, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_1
    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_2
    add-int/2addr v0, v1

    iget-wide v3, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleNeutral:J

    mul-int/lit8 v0, v0, 0x3b

    const/16 v1, 0x20

    ushr-long v5, v3, v1

    xor-long/2addr v3, v5

    long-to-int v4, v3

    add-int/2addr v0, v4

    iget-wide v3, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleDenied:J

    mul-int/lit8 v0, v0, 0x3b

    ushr-long v5, v3, v1

    xor-long/2addr v3, v5

    long-to-int v1, v3

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3b

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->meHasRole:Z

    const/16 v3, 0x4f

    const/16 v4, 0x61

    if-eqz v1, :cond_3

    const/16 v1, 0x4f

    goto :goto_3

    :cond_3
    const/16 v1, 0x61

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3b

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->isEveryoneRole:Z

    if-eqz v1, :cond_4

    goto :goto_4

    :cond_4
    const/16 v3, 0x61

    :goto_4
    add-int/2addr v0, v3

    return v0
.end method

.method public isManageable()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->isManageable()Z

    move-result v0

    return v0
.end method

.method public setupHeader(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 2

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->guildRoles:Ljava/util/Map;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole;->getName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f090003

    invoke-static {v0, p2, v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->access$400(Ljava/lang/String;Landroid/widget/TextView;I)V

    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/guilds/RoleUtils;->getRoleColor(Lcom/discord/models/domain/ModelGuildRole;Landroid/content/Context;)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setupPermissionEnabledState(Lcom/discord/views/TernaryCheckBox;J)V
    .locals 5

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->meHasRole:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-virtual {v0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->getMeHasPermission(J)Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    return-void

    :cond_0
    iget v0, p1, Lcom/discord/views/TernaryCheckBox;->n:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    const v4, 0x7f1203f6

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-virtual {v0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->getMeHasPermission(J)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->canDenyRolePermission(J)Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->c()V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v4}, Lcom/discord/views/TernaryCheckBox;->setOffDisabled(I)V

    goto :goto_1

    :cond_3
    const p2, 0x7f1203f4

    invoke-virtual {p1, p2}, Lcom/discord/views/TernaryCheckBox;->setOffDisabled(I)V

    goto :goto_1

    :cond_4
    const/4 v3, -0x1

    if-ne v0, v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->c()V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->canNeutralizeRolePermission(J)Z

    move-result v0

    invoke-virtual {p0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->canDenyRolePermission(J)Z

    move-result p2

    if-nez v0, :cond_7

    invoke-virtual {p1, v4}, Lcom/discord/views/TernaryCheckBox;->setDisabled(I)V

    goto :goto_1

    :cond_7
    if-eqz v0, :cond_8

    if-nez p2, :cond_8

    invoke-virtual {p1, v4}, Lcom/discord/views/TernaryCheckBox;->setOffDisabled(I)V

    goto :goto_1

    :cond_8
    if-eqz v0, :cond_9

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Lcom/discord/views/TernaryCheckBox;->c()V

    :cond_9
    :goto_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "WidgetChannelSettingsEditPermissions.ModelForRole(base="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->base:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildRoles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->guildRoles:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", roleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->roleId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myPermissionsWithRoleNeutral="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleNeutral:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", myPermissionsWithRoleDenied="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->myPermissionsWithRoleDenied:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", meHasRole="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->meHasRole:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isEveryoneRole="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->isEveryoneRole:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
