.class public final Lcom/discord/widgets/channels/WidgetGroupInviteFriends;
.super Lcom/discord/app/AppFragment;
.source "WidgetGroupInviteFriends.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;,
        Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;,
        Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Companion;

.field private static final INTENT_EXTRA_CHANNEL_ID:Ljava/lang/String; = "INTENT_EXTRA_CHANNEL_ID"

.field private static final MAX_GROUP_MEMBERS:I = 0xa

.field private static final MAX_GROUP_MEMBERS_STAFF:I = 0x19

.field private static final NO_CHANNEL_ID:J = -0x1L

.field private static final VIEW_INDEX_FRIENDS_LIST:I = 0x0

.field private static final VIEW_INDEX_GROUP_FULL:I = 0x2

.field private static final VIEW_INDEX_NO_FRIENDS:I = 0x1


# instance fields
.field private adapter:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

.field private final addedUsers:Lcom/google/gson/internal/LinkedTreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/internal/LinkedTreeMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation
.end field

.field private final addedUsersPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Collection<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;>;"
        }
    .end annotation
.end field

.field private final chipsView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final filterPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final recipientsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final sendFab$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    const-string v3, "recycler"

    const-string v4, "getRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    const-string/jumbo v6, "viewFlipper"

    const-string v7, "getViewFlipper()Landroid/widget/ViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    const-string v6, "sendFab"

    const-string v7, "getSendFab()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    const-string v6, "chipsView"

    const-string v7, "getChipsView()Lcom/discord/utilities/view/chips/ChipsView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    const-string v6, "recipientsContainer"

    const-string v7, "getRecipientsContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->Companion:Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a04bd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04c0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04be

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->sendFab$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04bb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->chipsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04bc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->recipientsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/google/gson/internal/LinkedTreeMap;

    invoke-direct {v0}, Lcom/google/gson/internal/LinkedTreeMap;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsers:Lcom/google/gson/internal/LinkedTreeMap;

    const-string v0, ""

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->filterPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsersPublisher:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->configureUI(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V

    return-void
.end method

.method public static final synthetic access$getFilterPublisher$p(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->filterPublisher:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$selectUser(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->selectUser(Lcom/discord/models/domain/ModelUser;)V

    return-void
.end method

.method public static final synthetic access$unselectUser(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->unselectUser(Lcom/discord/models/domain/ModelUser;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V
    .locals 9

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getSelectedUsers()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-static {v3, v4, v8, v7, v8}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    new-instance v8, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;

    invoke-direct {v8, v3}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v5, v6, v4, v7, v8}, Lcom/discord/utilities/view/chips/ChipsView;->addChip(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/Object;Lcom/discord/utilities/view/chips/ChipsView$DataContract;)V

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getFilterText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-ne v2, v1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/utilities/view/chips/ChipsView;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "chipsView.text"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v4, 0x1

    :cond_3
    if-eqz v4, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getFilterText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/utilities/view/chips/ChipsView;->setText(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/discord/utilities/view/chips/ChipsView;->prune(Ljava/util/Collection;)V

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getPotentialAdditions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getRecipients()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getMaxGroupMemberCount()I

    move-result v3

    invoke-direct {p0, v3, v1, v0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChildToDisplay(IILjava/util/List;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->adapter:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    if-eqz v1, :cond_6

    new-instance v2, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$configureUI$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$configureUI$1;-><init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V

    invoke-virtual {v1, v0, v2}, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;->configure(Ljava/util/List;Lrx/functions/Action2;)V

    :cond_6
    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->setupFAB(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->setupToolbar(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V

    return-void

    :cond_7
    :goto_2
    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_8
    return-void
.end method

.method private final getChildToDisplay(IILjava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List<",
            "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-lt p2, p1, :cond_0

    const/4 v0, 0x2

    goto :goto_2

    :cond_0
    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :cond_3
    :goto_2
    return v0
.end method

.method private final getChipsView()Lcom/discord/utilities/view/chips/ChipsView;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/utilities/view/chips/ChipsView<",
            "Ljava/lang/Long;",
            "Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->chipsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/chips/ChipsView;

    return-object v0
.end method

.method private final getRecipientsContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->recipientsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getSendFab()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->sendFab$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    return-object v0
.end method

.method private final getViewFlipper()Landroid/widget/ViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method private final selectUser(Lcom/discord/models/domain/ModelUser;)V
    .locals 5

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-static {p1, v2, v3, v4, v3}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;

    invoke-direct {v4, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$UserDataContract;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/utilities/view/chips/ChipsView;->addChip(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/Object;Lcom/discord/utilities/view/chips/ChipsView$DataContract;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsers:Lcom/google/gson/internal/LinkedTreeMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsersPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsers:Lcom/google/gson/internal/LinkedTreeMap;

    invoke-virtual {v1}, Ljava/util/AbstractMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final setupFAB(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RestrictedApi"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getSelectedUsers()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getSendFab()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getTotalNumRecipients()I

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getMaxGroupMemberCount()I

    move-result v1

    if-le v0, v1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getSendFab()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$setupFAB$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$setupFAB$1;-><init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getSendFab()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$setupFAB$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$setupFAB$2;-><init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getSendFab()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    return-void
.end method

.method private final setupToolbar(Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;)V
    .locals 11

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    const-string v1, "requireContext()"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    const v0, 0x7f1209a7

    new-array v4, v3, [Ljava/lang/Object;

    sget-object v5, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0, v0, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const v0, 0x7f120e3d

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getRecipients()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getMaxGroupMemberCount()I

    move-result v4

    if-lt v0, v4, :cond_1

    const p1, 0x7f12099a

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(R.string.group_dm_invite_full_main)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getRecipientsContainer()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getMaxGroupMemberCount()I

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->getTotalNumRecipients()I

    move-result p1

    sub-int/2addr v0, p1

    const-string p1, "resources"

    if-lez v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f10006d

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-static {v4, p1, v1, v0, v3}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    if-nez v0, :cond_3

    const p1, 0x7f1209a6

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(R.string.group\u2026_invite_will_fill_mobile)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f10006e

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-static {v4, p1, v1, v0, v3}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getRecipientsContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private final unselectUser(Lcom/discord/models/domain/ModelUser;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsers:Lcom/google/gson/internal/LinkedTreeMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/gson/internal/LinkedTreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsersPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsers:Lcom/google/gson/internal/LinkedTreeMap;

    invoke-virtual {v1}, Ljava/util/AbstractMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0202

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->adapter:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/chips/ChipsView;->setChipDeletedListener(Lcom/discord/utilities/view/chips/ChipsView$ChipDeletedListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->getChipsView()Lcom/discord/utilities/view/chips/ChipsView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/chips/ChipsView;->setTextChangedListener(Lcom/discord/utilities/view/chips/ChipsView$TextChangedListener;)V

    sget-object v0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model;->Companion:Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsersPublisher:Lrx/subjects/BehaviorSubject;

    const-string v2, "addedUsersPublisher"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion;->getFriendChanges(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->adapter:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    invoke-static {v1, p0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    new-instance v10, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$3;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsersPublisher:Lrx/subjects/BehaviorSubject;

    invoke-direct {v10, v1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$3;-><init>(Lrx/subjects/BehaviorSubject;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "INTENT_EXTRA_CHANNEL_ID"

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->addedUsersPublisher:Lrx/subjects/BehaviorSubject;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->filterPublisher:Lrx/subjects/BehaviorSubject;

    const-string v5, "filterPublisher"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$Companion;->get(JLrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;->adapter:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    invoke-static {v0, p0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/channels/WidgetGroupInviteFriends;

    new-instance v8, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$4;

    invoke-direct {v8, p0}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$onViewBoundOrOnResume$4;-><init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriends;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
