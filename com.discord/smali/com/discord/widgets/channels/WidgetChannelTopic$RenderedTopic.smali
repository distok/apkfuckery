.class public final Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
.super Ljava/lang/Object;
.source "WidgetChannelTopic.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RenderedTopic"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic$Companion;

.field public static final MAX_LINES:I = 0x28

.field public static final MIN_LINES:I = 0x2


# instance fields
.field private final autoLinkMask:I

.field private final channelName:Ljava/lang/String;

.field private final topic:Ljava/lang/CharSequence;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->Companion:Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    iput p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const-string p1, ""

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;Ljava/lang/String;Ljava/lang/CharSequence;IILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->copy(Ljava/lang/String;Ljava/lang/CharSequence;I)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/CharSequence;I)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    iget p1, p1, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoLinkMask()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    return v0
.end method

.method public final getChannelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    return-object v0
.end method

.method public final getTopic()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "RenderedTopic(channelName="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->channelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", topic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->topic:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", autoLinkMask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->autoLinkMask:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
