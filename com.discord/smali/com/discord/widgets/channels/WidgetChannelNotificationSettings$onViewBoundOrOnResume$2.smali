.class public final Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;
.super Lx/m/c/k;
.source "WidgetChannelNotificationSettings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;->invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    invoke-static {v0, p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->access$configureUI(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void
.end method
