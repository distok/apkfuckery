.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "WidgetChannelSidebarActionsViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelNotFound;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelNotFound;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->access$getStoreUserGuildSettings$p(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserGuildSettings;->get()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->access$getStoreGuildNSFW$p(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    const-string v3, "channel.guildId"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuildsNsfw;->isGuildNsfwGateAgreed(J)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v2, Lg0/l/e/j;

    invoke-direct {v2, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->access$getStoreUser$p(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    new-instance v3, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1$1;

    invoke-direct {v3, p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$observeStoreState$1$1;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-static {v0, v2, v1, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
