.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;
.super Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoChannel"
.end annotation


# instance fields
.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;-><init>(ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened:Z

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;ZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened()Z

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab()Z

    move-result p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->copy(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened()Z

    move-result v0

    return v0
.end method

.method public final component2()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab()Z

    move-result v0

    return v0
.end method

.method public final copy(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;-><init>(ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab()Z

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab:Z

    return v0
.end method

.method public isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "NoChannel(isRightPanelOpened="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isRightPanelOpened()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isOnHomeTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;->isOnHomeTab()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
