.class public final Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelNotificationSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;,
        Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;

.field private static final INTENT_SHOW_SYSTEM_SETTINGS:Ljava/lang/String; = "SHOW_SYSTEM_SETTING"


# instance fields
.field private final notificationFrequencyWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationMuteSettingsView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private notificationSettingsRadioManager:Lcom/discord/views/RadioManager;

.field private final notificationSettingsRadios$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final systemNotificationsSettings$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const-string v3, "notificationMuteSettingsView"

    const-string v4, "getNotificationMuteSettingsView()Lcom/discord/widgets/servers/NotificationMuteSettingsView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const-string v6, "notificationFrequencyWrap"

    const-string v7, "getNotificationFrequencyWrap()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const-string v6, "notificationSettingsRadios"

    const-string v7, "getNotificationSettingsRadios()Ljava/util/List;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    const-string/jumbo v6, "systemNotificationsSettings"

    const-string v7, "getSystemNotificationsSettings()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0186

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationMuteSettingsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01b5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationFrequencyWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, v0}, Ly/a/g0;->j(Landroidx/fragment/app/Fragment;[I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationSettingsRadios$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0185

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->systemNotificationsSettings$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a01b2
        0x7f0a01b3
        0x7f0a01b4
    .end array-data
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->configureUI(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V

    return-void
.end method

.method public static final synthetic access$getNotificationSettingsRadioManager$p(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)Lcom/discord/views/RadioManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationSettingsRadioManager:Lcom/discord/views/RadioManager;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "notificationSettingsRadioManager"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setNotificationSettingsRadioManager$p(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;Lcom/discord/views/RadioManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationSettingsRadioManager:Lcom/discord/views/RadioManager;

    return-void
.end method

.method private final configureNotificationRadios(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V
    .locals 5

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->getNotificationFrequencyWrap()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannelIsMuted()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->getNotificationSettingsRadios()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/views/CheckedSetting;

    const/4 v4, 0x2

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/views/CheckedSetting;

    sget v4, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_ALL:I

    invoke-virtual {v0, p1, v2, v4}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;Lcom/discord/views/CheckedSetting;I)V

    sget v2, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_MENTIONS:I

    invoke-virtual {v0, p1, v3, v2}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;Lcom/discord/views/CheckedSetting;I)V

    sget v2, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_NOTHING:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureNotificationRadios$1;->invoke(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;Lcom/discord/views/CheckedSetting;I)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    sget-object v8, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v9, "requireContext()"

    invoke-static {v4, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, v8

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v2

    const/4 v10, 0x0

    const/4 v11, 0x1

    const v12, 0x7f1210aa

    if-eqz v2, :cond_0

    const v13, 0x7f1210ab

    new-array v14, v11, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, v8

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v14, v10

    invoke-virtual {v0, v13, v14}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v15, v2

    const-string v13, "if (channel.isTextChanne\u2026g(R.string.mute_category)"

    invoke-static {v15, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v2

    if-eqz v2, :cond_1

    const v12, 0x7f1218cb

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, v8

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v10

    invoke-virtual {v0, v12, v11}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v2, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannelIsMuted()Z

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->getChannelMuteEndTime()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f1207df

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "getString(R.string.form_\u2026bel_mobile_channel_muted)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v19, 0x7f1207e0

    const v6, 0x7f1207e3

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object v13, v3

    move-object v6, v15

    move-object v15, v4

    move-object/from16 v16, v6

    move-object/from16 v17, v2

    move-object/from16 v18, v5

    invoke-direct/range {v13 .. v20}, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    new-instance v2, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureUI$onMute$1;

    invoke-direct {v2, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureUI$onMute$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V

    new-instance v4, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureUI$onUnmute$1;

    invoke-direct {v4, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$configureUI$onUnmute$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->getNotificationMuteSettingsView()Lcom/discord/widgets/servers/NotificationMuteSettingsView;

    move-result-object v5

    invoke-virtual {v5, v3, v2, v4}, Lcom/discord/widgets/servers/NotificationMuteSettingsView;->updateView(Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->configureNotificationRadios(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;)V

    return-void
.end method

.method private final getNotificationFrequencyWrap()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationFrequencyWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNotificationMuteSettingsView()Lcom/discord/widgets/servers/NotificationMuteSettingsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationMuteSettingsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/NotificationMuteSettingsView;

    return-object v0
.end method

.method private final getNotificationSettingsRadios()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/views/CheckedSetting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationSettingsRadios$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getSystemNotificationsSettings()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->systemNotificationsSettings$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0190

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const p1, 0x7f121182

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    new-instance p1, Lcom/discord/views/RadioManager;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->getNotificationSettingsRadios()Ljava/util/List;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->notificationSettingsRadioManager:Lcom/discord/views/RadioManager;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "SHOW_SYSTEM_SETTING"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->getSystemNotificationsSettings()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->getSystemNotificationsSettings()Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v2, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;

    invoke-virtual {v2, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;->get(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;

    new-instance v9, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
