.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;
.super Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultTopic"
.end annotation


# instance fields
.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z


# direct methods
.method public constructor <init>(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)V
    .locals 1

    const-string v0, "channelIconType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, p4, v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;-><init>(Lcom/discord/utilities/channel/GuildChannelIconType;ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-boolean p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened:Z

    iput-boolean p4, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened()Z

    move-result p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab()Z

    move-result p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->copy(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/utilities/channel/GuildChannelIconType;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened()Z

    move-result v0

    return v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab()Z

    move-result v0

    return v0
.end method

.method public final copy(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;
    .locals 1

    const-string v0, "channelIconType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;-><init>(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab()Z

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    return v0
.end method

.method public isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab:Z

    return v0
.end method

.method public isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "DefaultTopic(channelIconType="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isRightPanelOpened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isRightPanelOpened()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isOnHomeTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->isOnHomeTab()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
