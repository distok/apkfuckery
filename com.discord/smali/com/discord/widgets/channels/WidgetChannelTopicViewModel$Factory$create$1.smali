.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;->call(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened()Z

    move-result v0

    const-string v1, "navState"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->access$observeStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->access$observeStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method
