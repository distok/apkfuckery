.class public final Lcom/discord/widgets/channels/WidgetTextChannelSettings;
.super Lcom/discord/app/AppFragment;
.source "WidgetTextChannelSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;,
        Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;

.field private static final ONE_HOUR:I = 0xe10

.field private static final ONE_MINUTE:I = 0x3c

.field private static final SLOWMODE_COOLDOWN_VALUES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final channelSettingsName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsNsfw$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsPermissions$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsPinnedMessages$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsPinnedMessagesContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsPinnedMessagesDisabledOverlay$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsPrivacySafetyContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsSave$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsSlowModeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsSlowModeLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsSlowModeSlider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsTopic$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelSettingsWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final state:Lcom/discord/utilities/stateful/StatefulViews;

.field private final userManagementContainer$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    const/16 v0, 0xe

    new-array v1, v0, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-class v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v4, "channelSettingsPinnedMessagesContainer"

    const-string v5, "getChannelSettingsPinnedMessagesContainer()Landroid/view/ViewGroup;"

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v6

    new-instance v2, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v5, "channelSettingsPinnedMessages"

    const-string v7, "getChannelSettingsPinnedMessages()Landroid/view/View;"

    invoke-direct {v2, v4, v5, v7, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lx/m/c/s;

    const-class v5, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v7, "channelSettingsPinnedMessagesDisabledOverlay"

    const-string v8, "getChannelSettingsPinnedMessagesDisabledOverlay()Landroid/view/View;"

    invoke-direct {v2, v5, v7, v8, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    new-instance v2, Lx/m/c/s;

    const-class v7, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string/jumbo v8, "userManagementContainer"

    const-string v9, "getUserManagementContainer()Landroid/view/View;"

    invoke-direct {v2, v7, v8, v9, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x3

    aput-object v2, v1, v7

    new-instance v2, Lx/m/c/s;

    const-class v8, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v9, "channelSettingsPermissions"

    const-string v10, "getChannelSettingsPermissions()Landroid/view/View;"

    invoke-direct {v2, v8, v9, v10, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v8, 0x4

    aput-object v2, v1, v8

    new-instance v2, Lx/m/c/s;

    const-class v9, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v10, "channelSettingsPrivacySafetyContainer"

    const-string v11, "getChannelSettingsPrivacySafetyContainer()Landroid/view/View;"

    invoke-direct {v2, v9, v10, v11, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v9, 0x5

    aput-object v2, v1, v9

    new-instance v2, Lx/m/c/s;

    const-class v10, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v11, "channelSettingsWrap"

    const-string v12, "getChannelSettingsWrap()Landroid/view/View;"

    invoke-direct {v2, v10, v11, v12, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v10, 0x6

    aput-object v2, v1, v10

    new-instance v2, Lx/m/c/s;

    const-class v11, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v12, "channelSettingsName"

    const-string v13, "getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v2, v11, v12, v13, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v11, 0x7

    aput-object v2, v1, v11

    new-instance v2, Lx/m/c/s;

    const-class v12, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v13, "channelSettingsTopic"

    const-string v14, "getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v2, v12, v13, v14, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v12, 0x8

    aput-object v2, v1, v12

    new-instance v2, Lx/m/c/s;

    const-class v13, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v14, "channelSettingsSave"

    const-string v15, "getChannelSettingsSave()Landroid/view/View;"

    invoke-direct {v2, v13, v14, v15, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v13, 0x9

    aput-object v2, v1, v13

    new-instance v2, Lx/m/c/s;

    const-class v14, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v15, "channelSettingsNsfw"

    const-string v13, "getChannelSettingsNsfw()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v2, v14, v15, v13, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v13, 0xa

    aput-object v2, v1, v13

    new-instance v2, Lx/m/c/s;

    const-class v14, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v15, "channelSettingsSlowModeContainer"

    const-string v12, "getChannelSettingsSlowModeContainer()Landroid/view/View;"

    invoke-direct {v2, v14, v15, v12, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v12, 0xb

    aput-object v2, v1, v12

    new-instance v2, Lx/m/c/s;

    const-class v14, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v15, "channelSettingsSlowModeLabel"

    const-string v12, "getChannelSettingsSlowModeLabel()Landroid/widget/TextView;"

    invoke-direct {v2, v14, v15, v12, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v12, 0xc

    aput-object v2, v1, v12

    new-instance v2, Lx/m/c/s;

    const-class v14, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    const-string v15, "channelSettingsSlowModeSlider"

    const-string v12, "getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;"

    invoke-direct {v2, v14, v15, v12, v6}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v3, 0xd

    aput-object v2, v1, v3

    sput-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    const/16 v1, 0x78

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    const/16 v1, 0x12c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    const/16 v1, 0x258

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const/16 v1, 0x384

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const/16 v1, 0x708

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    const/16 v1, 0xe10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const/16 v1, 0x1c20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const/16 v1, 0x5460

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->SLOWMODE_COOLDOWN_VALUES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a01c1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPinnedMessagesContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPinnedMessages$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPinnedMessagesDisabledOverlay$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->userManagementContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01b7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPermissions$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPrivacySafetyContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01b1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01af

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01b0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsTopic$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSave$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01b6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsNsfw$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSlowModeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSlowModeLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01c8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSlowModeSlider$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/utilities/stateful/StatefulViews;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lcom/discord/utilities/stateful/StatefulViews;-><init>([I)V

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a01af
        0x7f0a01b0
        0x7f0a01c8
    .end array-data
.end method

.method public static final synthetic access$cannotDeleteWarn(Lcom/discord/widgets/channels/WidgetTextChannelSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->cannotDeleteWarn(Z)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/WidgetTextChannelSettings;Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->configureUI(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    return-void
.end method

.method public static final synthetic access$confirmDelete(Lcom/discord/widgets/channels/WidgetTextChannelSettings;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->confirmDelete(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public static final synthetic access$getChannelSettingsName$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelSettingsNsfw$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsNsfw()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelSettingsSave$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSave()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelSettingsSlowModeLabel$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Landroid/widget/TextView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeLabel()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelSettingsSlowModeSlider$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Landroid/widget/SeekBar;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelSettingsTopic$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSLOWMODE_COOLDOWN_VALUES$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->SLOWMODE_COOLDOWN_VALUES:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getState$p(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)Lcom/discord/utilities/stateful/StatefulViews;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    return-object p0
.end method

.method public static final synthetic access$setSlowmodeLabel(Lcom/discord/widgets/channels/WidgetTextChannelSettings;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->setSlowmodeLabel(I)V

    return-void
.end method

.method private final cannotDeleteWarn(Z)V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0197

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    const-string/jumbo v2, "view"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v1

    const-string v2, "AlertDialog.Builder(view\u2026t).setView(view).create()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a01a8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0a01a9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    if-eqz p1, :cond_0

    const p1, 0x7f1205f7

    goto :goto_0

    :cond_0
    const p1, 0x7f1205fd

    :goto_0
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    new-instance p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$cannotDeleteWarn$1;

    invoke-direct {p1, v1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$cannotDeleteWarn$1;-><init>(Landroidx/appcompat/app/AlertDialog;)V

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V
    .locals 9

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v2, v0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f120467

    goto :goto_0

    :cond_2
    const v0, 0x7f12040a

    :goto_0
    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    sget-object v3, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    const-string v0, "requireContext()"

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0e001a

    goto :goto_1

    :cond_3
    const v0, 0x7f0e0003

    :goto_1
    new-instance v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$1;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$1;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings;Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    new-instance v4, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$2;

    invoke-direct {v4, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$2;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    invoke-virtual {p0, v0, v3, v4}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;Lrx/functions/Action1;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/discord/utilities/stateful/StatefulViews;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v3

    if-eqz v3, :cond_4

    const v3, 0x7f1207b6

    goto :goto_2

    :cond_4
    const v3, 0x7f120407

    :goto_2
    invoke-static {v0, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setHint(Lcom/google/android/material/textfield/TextInputLayout;I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getTopic()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    goto :goto_3

    :cond_5
    const-string v5, ""

    :goto_3
    invoke-virtual {v3, v4, v5}, Lcom/discord/utilities/stateful/StatefulViews;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v3

    const/16 v4, 0x8

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    const/16 v3, 0x8

    :goto_4
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsWrap()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManageChannel()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    goto :goto_5

    :cond_7
    const/16 v3, 0x8

    :goto_5
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSave()Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$3;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$3;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings;Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSave()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/discord/utilities/stateful/StatefulViews;->configureSaveActionView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPinnedMessagesContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    goto :goto_6

    :cond_8
    const/16 v3, 0x8

    :goto_6
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPinnedMessages()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPinnedMessages()Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$5;

    invoke-direct {v3, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$5;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPinnedMessagesDisabledOverlay()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled()Z

    move-result v3

    xor-int/2addr v3, v2

    if-eqz v3, :cond_9

    const/4 v3, 0x0

    goto :goto_7

    :cond_9
    const/16 v3, 0x8

    :goto_7
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPinnedMessagesDisabledOverlay()Landroid/view/View;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$6;->INSTANCE:Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$6;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPermissions()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManagePermissions()Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x0

    goto :goto_8

    :cond_a
    const/16 v3, 0x8

    :goto_8
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPermissions()Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$7;

    invoke-direct {v3, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$7;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getUserManagementContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManageChannel()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManagePermissions()Z

    move-result v3

    if-eqz v3, :cond_b

    goto :goto_9

    :cond_b
    const/4 v3, 0x0

    goto :goto_a

    :cond_c
    :goto_9
    const/4 v3, 0x1

    :goto_a
    if-eqz v3, :cond_d

    const/4 v3, 0x0

    goto :goto_b

    :cond_d
    const/16 v3, 0x8

    :goto_b
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsPrivacySafetyContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManageChannel()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v3

    if-eqz v3, :cond_e

    const/4 v3, 0x1

    goto :goto_c

    :cond_e
    const/4 v3, 0x0

    :goto_c
    if-eqz v3, :cond_f

    const/4 v3, 0x0

    goto :goto_d

    :cond_f
    const/16 v3, 0x8

    :goto_d
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsNsfw()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManageChannel()Z

    move-result v3

    if-eqz v3, :cond_10

    const/4 v3, 0x0

    goto :goto_e

    :cond_10
    const/16 v3, 0x8

    :goto_e
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsNsfw()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result v3

    invoke-virtual {v0, v3, v1}, Lcom/discord/views/CheckedSetting;->g(ZZ)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsNsfw()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v3, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$8;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$configureUI$8;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings;Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;)V

    invoke-virtual {v0, v3}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getCanManageChannel()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v3

    if-eqz v3, :cond_11

    const/4 v3, 0x1

    goto :goto_f

    :cond_11
    const/4 v3, 0x0

    :goto_f
    if-eqz v3, :cond_12

    const/4 v4, 0x0

    :cond_12
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    const v3, 0x7f0a01c8

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getRateLimitPerUser()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v3, p1}, Lcom/discord/utilities/stateful/StatefulViews;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->setSlowmodeLabel(I)V

    sget-object v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->SLOWMODE_COOLDOWN_VALUES:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    if-lt v4, p1, :cond_13

    const/4 v4, 0x1

    goto :goto_11

    :cond_13
    const/4 v4, 0x0

    :goto_11
    if-eqz v4, :cond_14

    goto :goto_12

    :cond_14
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_15
    const/4 v3, -0x1

    :goto_12
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSave()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/stateful/StatefulViews;->configureSaveActionView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeLabel()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final confirmDelete(Lcom/discord/models/domain/ModelChannel;)V
    .locals 11

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0198

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    const-string/jumbo v2, "view"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v1

    const-string v2, "AlertDialog.Builder(view\u2026t).setView(view).create()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a01ae

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0a01ab

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0a01ac

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0a01ad

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f1205ea

    goto :goto_0

    :cond_0
    const v5, 0x7f1205e9

    :goto_0
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    new-instance v2, Lcom/discord/widgets/channels/WidgetTextChannelSettings$confirmDelete$1;

    invoke-direct {v2, v1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$confirmDelete$1;-><init>(Landroidx/appcompat/app/AlertDialog;)V

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/discord/widgets/channels/WidgetTextChannelSettings$confirmDelete$2;

    invoke-direct {v2, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$confirmDelete$2;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "dialogBody"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1205eb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v7

    const-string v6, "requireContext()"

    invoke-static {v7, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v6, p1

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v4

    invoke-virtual {p0, v0, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(R.string.delet\u2026ayName(requireContext()))"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lf/a/j/b/b/g;->b(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private final getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getChannelSettingsNsfw()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsNsfw$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getChannelSettingsPermissions()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPermissions$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelSettingsPinnedMessages()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPinnedMessages$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelSettingsPinnedMessagesContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPinnedMessagesContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getChannelSettingsPinnedMessagesDisabledOverlay()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPinnedMessagesDisabledOverlay$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelSettingsPrivacySafetyContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsPrivacySafetyContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelSettingsSave()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSave$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelSettingsSlowModeContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSlowModeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelSettingsSlowModeLabel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSlowModeLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsSlowModeSlider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    return-object v0
.end method

.method private final getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsTopic$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getChannelSettingsWrap()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->channelSettingsWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getUserManagementContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->userManagementContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final launch(JLandroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;->launch(JLandroid/content/Context;)V

    return-void
.end method

.method private final saveChannel(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 13

    move-object v0, p0

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v11, 0x18

    const/4 v12, 0x0

    move-wide v3, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    invoke-static/range {v2 .. v12}, Lcom/discord/utilities/rest/RestAPI;->editChannel$default(Lcom/discord/utilities/rest/RestAPI;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v1, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    sget-object v3, Lf/a/b/r;->a:Lf/a/b/r;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/discord/widgets/channels/WidgetTextChannelSettings$saveChannel$1;

    invoke-direct {v5, p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$saveChannel$1;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)V

    invoke-virtual {v3, v4, v5, v2}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public static synthetic saveChannel$default(Lcom/discord/widgets/channels/WidgetTextChannelSettings;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;ILjava/lang/Object;)V
    .locals 9

    and-int/lit8 v0, p7, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_1

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object v6, p4

    :goto_1
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_2

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object v7, p5

    :goto_2
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_3

    move-object v8, v1

    goto :goto_3

    :cond_3
    move-object v8, p6

    :goto_3
    move-object v2, p0

    move-wide v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->saveChannel(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    return-void
.end method

.method private final setSlowmodeLabel(I)V
    .locals 4

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeLabel()Landroid/widget/TextView;

    move-result-object p1

    const v0, 0x7f120828

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x3c

    if-le v0, p1, :cond_1

    goto :goto_0

    :cond_1
    if-le v2, p1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeLabel()Landroid/widget/TextView;

    move-result-object v0

    const v2, 0x7f100036

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, p1, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setPluralText(Landroid/widget/TextView;II[Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    :goto_0
    const/16 v0, 0xe10

    if-le v2, p1, :cond_3

    goto :goto_1

    :cond_3
    if-le v0, p1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeLabel()Landroid/widget/TextView;

    move-result-object v0

    const v3, 0x7f100033

    div-int/2addr p1, v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, p1, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setPluralText(Landroid/widget/TextView;II[Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeLabel()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f100031

    div-int/2addr p1, v0

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setPluralText(Landroid/widget/TextView;II[Ljava/lang/Object;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02ba

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {v0, p0}, Lcom/discord/utilities/stateful/StatefulViews;->setupUnsavedChangesConfirmation(Lcom/discord/app/AppFragment;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    new-array v1, p1, [Landroid/view/View;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/discord/utilities/stateful/StatefulViews;->addOptionalFields([Landroid/view/View;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSave()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/view/View;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsName()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    aput-object v3, v2, p1

    invoke-virtual {v0, p0, v1, v2}, Lcom/discord/utilities/stateful/StatefulViews;->setupTextWatcherWithSaveAction(Lcom/discord/app/AppFragment;Landroid/view/View;[Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$onViewBound$1;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)V

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsTopic()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->interceptScrollWhenInsideScrollable(Lcom/google/android/material/textfield/TextInputLayout;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->getChannelSettingsSlowModeSlider()Landroid/widget/SeekBar;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->SLOWMODE_COOLDOWN_VALUES:Ljava/util/List;

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    sget-object v2, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;

    invoke-virtual {v2, v0, v1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;->get(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/WidgetTextChannelSettings;

    new-instance v9, Lcom/discord/widgets/channels/WidgetTextChannelSettings$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/WidgetTextChannelSettings;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
