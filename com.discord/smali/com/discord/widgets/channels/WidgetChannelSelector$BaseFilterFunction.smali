.class public final Lcom/discord/widgets/channels/WidgetChannelSelector$BaseFilterFunction;
.super Ljava/lang/Object;
.source "WidgetChannelSelector.kt"

# interfaces
.implements Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BaseFilterFunction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public includeChannel(Lcom/discord/models/domain/ModelChannel;)Z
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction$DefaultImpls;->includeChannel(Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;Lcom/discord/models/domain/ModelChannel;)Z

    move-result p1

    return p1
.end method
