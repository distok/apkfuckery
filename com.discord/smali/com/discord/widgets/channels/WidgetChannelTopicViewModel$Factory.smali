.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;
    }
.end annotation


# instance fields
.field private final storeChannels:Lcom/discord/stores/StoreChannels;

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;

.field private final storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;

.field private final storeUsers:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;)V
    .locals 1

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannels"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUsers"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuilds"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeNavigation"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeTabsNavigation"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeUsers:Lcom/discord/stores/StoreUser;

    iput-object p4, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object p5, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object p6, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object p7, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object p2

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p6

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getTabsNavigation()Lcom/discord/stores/StoreTabsNavigation;

    move-result-object p7

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-direct/range {p2 .. p9}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;)V

    return-void
.end method

.method public static final synthetic access$getStoreUserSettings$p(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;)Lcom/discord/stores/StoreUserSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    return-object p0
.end method

.method public static final synthetic access$mapChannelToGuildStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->mapChannelToGuildStoreState(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$mapChannelToPrivateStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->mapChannelToPrivateStoreState(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$observeStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->observeStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final mapChannelToGuildStoreState(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeChannels:Lcom/discord/stores/StoreChannels;

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-wide v4, v1

    invoke-static/range {v3 .. v8}, Lcom/discord/stores/StoreChannels;->observeChannelsForGuild$default(Lcom/discord/stores/StoreChannels;JLjava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v9

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeUsers:Lcom/discord/stores/StoreUser;

    invoke-virtual {v3}, Lcom/discord/stores/StoreUser;->observeAllUsers()Lrx/Observable;

    move-result-object v10

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v3, v1, v2}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v11

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v3, v1, v2}, Lcom/discord/stores/StoreGuilds;->observeRoles(J)Lrx/Observable;

    move-result-object v12

    iget-object v1, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getAllowAnimatedEmojisObservable()Lrx/Observable;

    move-result-object v13

    new-instance v14, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v14, v1, v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToGuildStoreState$1;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)V

    invoke-static/range {v9 .. v14}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object v1

    const-string v2, "Observable.combineLatest\u2026HomeTab\n        )\n      }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final mapChannelToPrivateStoreState(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isGroup()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v1

    invoke-virtual {p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened()Z

    move-result v2

    invoke-virtual {p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab()Z

    move-result p2

    invoke-direct {v0, p1, v1, v2, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;-><init>(Lcom/discord/models/domain/ModelChannel;ZZZ)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p2, "Observable.just(\n       \u2026            )\n          )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuilds;->observeComputed()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$mapChannelToPrivateStoreState$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo p2, "storeGuilds.observeCompu\u2026            )\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final observeNavState()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreNavigation;->observeRightPanelState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$1;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    invoke-virtual {v1}, Lcom/discord/stores/StoreTabsNavigation;->observeSelectedTab()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$2;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$2;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeNavState$3;

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026sOpen, isOnHomeTab)\n    }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final observeStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "storeChannelsSelected\n  \u2026            }\n          }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->observeNavState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$create$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    const-string v0, "observeNavState().switch\u2026  ).take(1)\n            }"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->storeChannels:Lcom/discord/stores/StoreChannels;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/simpleast/core/parser/Parser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method
