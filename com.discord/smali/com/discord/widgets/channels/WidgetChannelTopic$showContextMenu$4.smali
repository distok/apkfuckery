.class public final Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;
.super Ljava/lang/Object;
.source "WidgetChannelTopic.kt"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopic;->showContextMenu(ZJLjava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $channelTitle:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelTopic;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->$channelTitle:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 19

    move-object/from16 v0, p0

    sget-object v1, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    iget-object v2, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    const/4 v15, 0x1

    new-array v4, v15, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->$channelTitle:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const v5, 0x7f120f13

    invoke-virtual {v3, v5, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    new-array v5, v15, [Ljava/lang/Object;

    iget-object v7, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->$channelTitle:Ljava/lang/String;

    aput-object v7, v5, v6

    const v6, 0x7f120f10

    invoke-virtual {v4, v6, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.leave\u2026up_dm_body, channelTitle)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    const v6, 0x7f120f0f

    invoke-virtual {v5, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopic;

    const v7, 0x7f1203f1

    invoke-virtual {v6, v7}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0a06fc

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4$1;

    invoke-direct {v8, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;)V

    new-instance v9, Lkotlin/Pair;

    invoke-direct {v9, v7, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v9}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const v11, 0x7f040445

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x1dc0

    const/16 v17, 0x0

    const/16 v18, 0x1

    move/from16 v15, v16

    move-object/from16 v16, v17

    invoke-static/range {v1 .. v16}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return v18
.end method
