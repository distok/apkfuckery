.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;
.super Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GDM"
.end annotation


# instance fields
.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelId:J

.field private final developerModeEnabled:Z

.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;JZZZ)V
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p5, p6, v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;-><init>(ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-wide p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    iput-boolean p4, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    iput-boolean p5, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened:Z

    iput-boolean p6, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelChannel;JZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x4

    if-eqz p7, :cond_0

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p4

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;-><init>(Lcom/discord/models/domain/ModelChannel;JZZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;Lcom/discord/models/domain/ModelChannel;JZZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-wide p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p4, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    :cond_2
    move p8, p4

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened()Z

    move-result p5

    :cond_3
    move v2, p5

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab()Z

    move-result p6

    :cond_4
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move p6, p8

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->copy(Lcom/discord/models/domain/ModelChannel;JZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    return-wide v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened()Z

    move-result v0

    return v0
.end method

.method public final component5()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab()Z

    move-result v0

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;JZZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;
    .locals 8

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;-><init>(Lcom/discord/models/domain/ModelChannel;JZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    iget-wide v2, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab()Z

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    return-wide v0
.end method

.method public final getDeveloperModeEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    return v0
.end method

.method public isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab:Z

    return v0
.end method

.method public isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GDM(channel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", developerModeEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->developerModeEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isRightPanelOpened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isRightPanelOpened()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isOnHomeTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->isOnHomeTab()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
