.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;
.super Lx/m/c/k;
.source "WidgetChannelSidebarActions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->configureUI(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;

.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActions;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActions;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;->$viewState:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActions;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;->$viewState:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;->getChannelId()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->access$startPrivateCall(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;JZ)V

    return-void
.end method
