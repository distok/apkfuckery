.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NavState"
.end annotation


# instance fields
.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;ZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->copy(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    return v0
.end method

.method public final copy(ZZ)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;
    .locals 1

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;-><init>(ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    return v0
.end method

.method public final isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "NavState(isRightPanelOpened="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isOnHomeTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
