.class public final Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;
.super Ljava/lang/Object;
.source "WidgetTextChannelSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetTextChannelSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;


# instance fields
.field private final canManageChannel:Z

.field private final canManagePermissions:Z

.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final isPinsEnabled:Z

.field private final isPublicGuildRulesChannel:Z

.field private final isPublicGuildUpdatesChannel:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;ZZZZZ)V
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    iput-boolean p3, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    iput-boolean p4, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    iput-boolean p5, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    iput-boolean p6, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;Lcom/discord/models/domain/ModelChannel;ZZZZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->copy(Lcom/discord/models/domain/ModelChannel;ZZZZZ)Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;ZZZZZ)Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;
    .locals 8

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;-><init>(Lcom/discord/models/domain/ModelChannel;ZZZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanManageChannel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    return v0
.end method

.method public final getCanManagePermissions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    return v0
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    if-eqz v1, :cond_5

    goto :goto_1

    :cond_5
    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    return v0
.end method

.method public final isPinsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    return v0
.end method

.method public final isPublicGuildRulesChannel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    return v0
.end method

.method public final isPublicGuildUpdatesChannel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Model(channel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canManageChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManageChannel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", canManagePermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->canManagePermissions:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPinsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPinsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPublicGuildRulesChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildRulesChannel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPublicGuildUpdatesChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Model;->isPublicGuildUpdatesChannel:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
