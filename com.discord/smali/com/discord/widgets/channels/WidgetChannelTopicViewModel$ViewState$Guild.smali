.class public abstract Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;
.super Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Guild"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;
    }
.end annotation


# instance fields
.field private final channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

.field private final isOnHomeTab:Z

.field private final isRightPanelOpened:Z


# direct methods
.method private constructor <init>(Lcom/discord/utilities/channel/GuildChannelIconType;ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;-><init>(ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->isRightPanelOpened:Z

    iput-boolean p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->isOnHomeTab:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/channel/GuildChannelIconType;ZZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;-><init>(Lcom/discord/utilities/channel/GuildChannelIconType;ZZ)V

    return-void
.end method


# virtual methods
.method public getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->channelIconType:Lcom/discord/utilities/channel/GuildChannelIconType;

    return-object v0
.end method

.method public isOnHomeTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->isOnHomeTab:Z

    return v0
.end method

.method public isRightPanelOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->isRightPanelOpened:Z

    return v0
.end method
