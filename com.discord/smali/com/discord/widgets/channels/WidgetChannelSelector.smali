.class public final Lcom/discord/widgets/channels/WidgetChannelSelector;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetChannelSelector.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;,
        Lcom/discord/widgets/channels/WidgetChannelSelector$Model;,
        Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;,
        Lcom/discord/widgets/channels/WidgetChannelSelector$BaseFilterFunction;,
        Lcom/discord/widgets/channels/WidgetChannelSelector$TypeFilterFunction;,
        Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_FILTER_FUNCTION:Ljava/lang/String; = "INTENT_EXTRA_FILTER_FUNCTION"

.field private static final ARG_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field private static final ARG_INCLUDE_NO_CHANNEL:Ljava/lang/String; = "INTENT_EXTRA_INCLUDE_NO_CHANNEL"

.field private static final ARG_NO_CHANNEL_STRING_ID:Ljava/lang/String; = "INTENT_EXTRA_NO_CHANNEL_STRING_ID"

.field private static final ARG_REQUEST_CODE:Ljava/lang/String; = "INTENT_EXTRA_REQUEST_CODE"

.field public static final Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

.field private static final RESULT_EXTRA_CHANNEL_ID:Ljava/lang/String; = "INTENT_EXTRA_CHANNEL_ID"

.field private static final RESULT_EXTRA_CHANNEL_NAME:Ljava/lang/String; = "INTENT_EXTRA_CHANNEL_NAME"


# instance fields
.field private adapter:Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

.field private final list$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private requestCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/channels/WidgetChannelSelector;

    const-string v3, "list"

    const-string v4, "getList()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a01a7

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->list$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/channels/WidgetChannelSelector;)Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->adapter:Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "adapter"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$onChannelSelected(Lcom/discord/widgets/channels/WidgetChannelSelector;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSelector;->onChannelSelected(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public static final synthetic access$setAdapter$p(Lcom/discord/widgets/channels/WidgetChannelSelector;Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->adapter:Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

    return-void
.end method

.method private final getList()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->list$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelSelector;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final onChannelSelected(Lcom/discord/models/domain/ModelChannel;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    const-string v3, "INTENT_EXTRA_CHANNEL_ID"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_1
    const-string p1, "INTENT_EXTRA_CHANNEL_NAME"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getTargetFragment()Landroidx/fragment/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_2

    iget v1, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->requestCode:I

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2, v0}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    :cond_2
    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method


# virtual methods
.method public bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
    .locals 17

    move-object/from16 v0, p0

    const-string v1, "compositeSubscription"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/app/AppBottomSheet;->bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelSelector$Model;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion;

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "INTENT_EXTRA_INCLUDE_NO_CHANNEL"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "INTENT_EXTRA_FILTER_FUNCTION"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    instance-of v6, v5, Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;

    const/4 v7, 0x0

    if-nez v6, :cond_0

    move-object v5, v7

    :cond_0
    check-cast v5, Lcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;

    if-eqz v5, :cond_1

    goto :goto_0

    :cond_1
    new-instance v5, Lcom/discord/widgets/channels/WidgetChannelSelector$BaseFilterFunction;

    invoke-direct {v5}, Lcom/discord/widgets/channels/WidgetChannelSelector$BaseFilterFunction;-><init>()V

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/discord/widgets/channels/WidgetChannelSelector$Model$Companion;->get(JZLcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;)Lrx/Observable;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->adapter:Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

    if-eqz v2, :cond_2

    invoke-static {v1, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/channels/WidgetChannelSelector;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Lcom/discord/widgets/channels/WidgetChannelSelector$bindSubscriptions$1;

    invoke-direct {v14, v0}, Lcom/discord/widgets/channels/WidgetChannelSelector$bindSubscriptions$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelSelector;)V

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_2
    const-string v1, "adapter"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0195

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance p2, Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelSelector;->getList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "INTENT_EXTRA_NO_CHANNEL_STRING_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p2, v0, p0, v1}, Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/channels/WidgetChannelSelector;I)V

    invoke-virtual {p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->adapter:Lcom/discord/widgets/channels/WidgetChannelSelector$Adapter;

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "INTENT_EXTRA_REQUEST_CODE"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/discord/widgets/channels/WidgetChannelSelector;->requestCode:I

    return-void
.end method
