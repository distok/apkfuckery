.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;
.super Ljava/lang/Object;
.source "WidgetChannelSidebarActionsViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Boolean;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;->call(Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Boolean;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;",
            ">;"
        }
    .end annotation

    const-string v0, "isOpen"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->access$observeStoreState(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory$create$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;->access$observeStoreState(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method
