.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActions;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelSidebarActions.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final guildActionView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privateActionView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;

    const-string v3, "guildActionView"

    const-string v4, "getGuildActionView()Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;

    const-string v6, "privateActionView"

    const-string v7, "getPrivateActionView()Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0bb8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->guildActionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bb9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->privateActionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->configureUI(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$startPrivateCall(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->startPrivateCall(JZ)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    sget-object v2, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Uninitialized;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1, v3}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    goto/16 :goto_0

    :cond_0
    instance-of v2, v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;

    const/16 v4, 0x8

    const/4 v5, 0x1

    const-string v6, "requireContext()"

    if-eqz v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2, v5}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->getGuildActionView()Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->getPrivateActionView()Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    move-object v2, v1

    check-cast v2, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;->getChannelId()J

    move-result-wide v3

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->getPrivateActionView()Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;

    move-result-object v7

    new-instance v8, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$1;

    invoke-direct {v8, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V

    new-instance v9, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;

    invoke-direct {v9, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$2;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;)V

    new-instance v10, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$3;

    invoke-direct {v10, v0, v3, v4}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$3;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;J)V

    new-instance v11, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$4;

    invoke-direct {v11, v3, v4, v5}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$4;-><init>(JLandroid/content/Context;)V

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Private;->isMuted()Z

    move-result v12

    invoke-virtual/range {v7 .. v12}, Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;->a(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Z)V

    goto :goto_0

    :cond_2
    instance-of v2, v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;->getChannelId()J

    move-result-wide v7

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->getGuildActionView()Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;

    move-result-object v10

    new-instance v11, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$5;

    invoke-direct {v11, v1, v9}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$5;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState;Landroid/content/Context;)V

    new-instance v13, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$6;

    invoke-direct {v13, v9, v7, v8}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$6;-><init>(Landroid/content/Context;J)V

    new-instance v12, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$7;

    invoke-direct {v12, v0, v7, v8}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$7;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;J)V

    new-instance v14, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;

    invoke-direct {v14, v7, v8, v9}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$configureUI$8;-><init>(JLandroid/content/Context;)V

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;->isMuted()Z

    move-result v16

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;->getHasUnreadPins()Z

    move-result v15

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$ViewState$Guild;->getDisablePins()Z

    move-result v17

    invoke-virtual/range {v10 .. v17}, Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;->a(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZZZ)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {v1, v5}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->getPrivateActionView()Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->getGuildActionView()Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    :goto_0
    return-void
.end method

.method private final getGuildActionView()Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->guildActionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/channelsidebar/GuildChannelSideBarActionsView;

    return-object v0
.end method

.method private final getPrivateActionView()Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->privateActionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/channelsidebar/PrivateChannelSideBarActionsView;

    return-object v0
.end method

.method private final startPrivateCall(JZ)V
    .locals 4

    new-instance v0, Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;-><init>(Lcom/discord/app/AppPermissions$Requests;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V

    if-eqz p3, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVideoCall(J)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVoiceCall(J)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d019d

    return v0
.end method

.method public onViewBoundOrOnResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v9, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    const/4 v8, 0x0

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, p0, v9}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(this, \u2026onsViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;->viewModel:Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/channels/WidgetChannelSidebarActions;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActions$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelSidebarActions;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
