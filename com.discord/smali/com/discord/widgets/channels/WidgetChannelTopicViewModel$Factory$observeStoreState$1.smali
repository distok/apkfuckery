.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "WidgetChannelTopicViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->observeStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

.field public final synthetic this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened()Z

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab()Z

    move-result v1

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;-><init>(ZZ)V

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-static {v0, p1, v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->access$mapChannelToPrivateStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object v0

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getTopic()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getTopic()Ljava/lang/String;

    move-result-object v0

    const-string v1, "channel.topic"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-static {v0, p1, v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;->access$mapChannelToGuildStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;)Lrx/Observable;

    move-result-object v0

    goto :goto_2

    :cond_4
    :goto_1
    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isRightPanelOpened()Z

    move-result v1

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$observeStoreState$1;->$navState:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory$NavState;->isOnHomeTab()Z

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;-><init>(Lcom/discord/models/domain/ModelChannel;ZZ)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object v0, p1

    :goto_2
    return-object v0
.end method
