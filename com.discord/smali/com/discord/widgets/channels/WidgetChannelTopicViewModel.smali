.class public final Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;
.super Lf/a/b/l0;
.source "WidgetChannelTopicViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;,
        Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private previousChannelId:Ljava/lang/Long;

.field private revealedIndices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private storeChannels:Lcom/discord/stores/StoreChannels;

.field private topicParser:Lcom/discord/simpleast/core/parser/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation
.end field

.field private wasOnHomeTab:Z

.field private wasRightPanelOpened:Z


# direct methods
.method public constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/simpleast/core/parser/Parser;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannels"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topicParser"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;-><init>(ZZ)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->topicParser:Lcom/discord/simpleast/core/parser/Parser;

    sget-object p2, Lx/h/n;->d:Lx/h/n;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    const-wide/16 p2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->previousChannelId:Ljava/lang/Long;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    new-instance v6, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/simpleast/core/parser/Parser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    const/4 p5, 0x4

    and-int/2addr p4, p5

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    const/4 p4, 0x0

    const/4 v0, 0x0

    invoke-static {v0, p3, v0, p5, p4}, Lcom/discord/utilities/textprocessing/Parsers;->createParser$default(ZZZILjava/lang/Object;)Lcom/discord/simpleast/core/parser/Parser;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/simpleast/core/parser/Parser;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->handleStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V

    return-void
.end method

.method private final generateAST(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->topicParser:Lcom/discord/simpleast/core/parser/Parser;

    sget-object v1, Lcom/discord/utilities/textprocessing/MessageParseState;->Companion:Lcom/discord/utilities/textprocessing/MessageParseState$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final handleGuildStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    instance-of v2, v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;

    if-eqz v2, :cond_0

    new-instance v2, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    move-object v3, v1

    check-cast v3, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;

    invoke-virtual {v3}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-virtual {v3}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-static {v3}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->getChannelType(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;->isRightPanelOpened()Z

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;->isOnHomeTab()Z

    move-result v1

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;-><init>(Lcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)V

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_0
    instance-of v2, v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;

    if-eqz v2, :cond_7

    move-object v2, v1

    check-cast v2, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getTopic()Ljava/lang/String;

    move-result-object v5

    const-string v3, "channelTopic"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v5}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->generateAST(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v10

    new-instance v3, Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    const-wide/16 v12, -0x1

    iget-object v14, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1c

    const/16 v19, 0x0

    move-object v11, v3

    invoke-direct/range {v11 .. v19}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;-><init>(JLjava/util/Collection;Ljava/util/List;ZLjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v10}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->process(Ljava/util/Collection;)V

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getUsers()Ljava/util/Map;

    move-result-object v4

    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v11

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getMembers()Ljava/util/Map;

    move-result-object v9

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v9

    :goto_1
    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getChannels()Ljava/util/Map;

    move-result-object v4

    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v8

    invoke-static {v8}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v11, "name"

    invoke-static {v9, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-lez v9, :cond_5

    const/4 v9, 0x1

    goto :goto_4

    :cond_5
    const/4 v9, 0x0

    :goto_4
    if-eqz v9, :cond_4

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v8, v9, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getRoles()Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getAllowAnimatedEmojis()Z

    move-result v11

    invoke-virtual {v3}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->isLinkifyConflicting()Z

    move-result v3

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-static {v4}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->getChannelType(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v12

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;->isRightPanelOpened()Z

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;->isOnHomeTab()Z

    move-result v15

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    move-object v4, v1

    move-object v6, v8

    move-object v8, v9

    move v9, v11

    move v11, v3

    invoke-direct/range {v4 .. v15}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZ)V

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_5
    return-void

    :cond_7
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method private final handlePrivateStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V
    .locals 12

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/discord/models/domain/ModelUser;->getNickOrUsername(Lcom/discord/models/domain/ModelGuildMember$Computed;Lcom/discord/models/domain/ModelChannel;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v1, "storeState.channel.dmRec\u2026ate.channel\n            )"

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getGuildMembers()Ljava/util/Collection;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v6

    const-string/jumbo v9, "storeState.channel.dmRecipient"

    invoke-static {v6, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v3

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lx/h/f;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getDeveloperModeEnabled()Z

    move-result v9

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened()Z

    move-result v10

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab()Z

    move-result v11

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;

    move-object v4, p1

    invoke-direct/range {v4 .. v11}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;-><init>(Ljava/lang/String;Ljava/util/Set;JZZZ)V

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    move-object v1, p1

    check-cast v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;->getDeveloperModeEnabled()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened()Z

    move-result v6

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab()Z

    move-result v7

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;-><init>(Lcom/discord/models/domain/ModelChannel;JZZZ)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_4
    :goto_2
    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$Topic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->previousChannelId:Ljava/lang/Long;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    :goto_0
    sget-object v2, Lx/h/n;->d:Lx/h/n;

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    :goto_1
    iput-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild$DefaultTopic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$DM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$GDM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->previousChannelId:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened()Z

    move-result v0

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->wasRightPanelOpened:Z

    if-ne v0, v1, :cond_6

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab()Z

    move-result v0

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->wasOnHomeTab:Z

    if-eq v0, v1, :cond_8

    :cond_6
    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event$FocusFirstElement;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event$FocusFirstElement;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_7
    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened()Z

    move-result v0

    iput-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->wasRightPanelOpened:Z

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab()Z

    move-result v0

    iput-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->wasOnHomeTab:Z

    :cond_8
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$NoChannel;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isRightPanelOpened()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;->isOnHomeTab()Z

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;-><init>(ZZ)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_3

    :cond_9
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;

    if-eqz v0, :cond_a

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->handleGuildStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState$Guild;)V

    goto :goto_3

    :cond_a
    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->handlePrivateStoreState(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$StoreState;)V

    :goto_3
    return-void
.end method


# virtual methods
.method public final getStoreChannels()Lcom/discord/stores/StoreChannels;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-object v0
.end method

.method public final getTopicParser()Lcom/discord/simpleast/core/parser/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->topicParser:Lcom/discord/simpleast/core/parser/Parser;

    return-object v0
.end method

.method public final handleClosePrivateChannel(Landroid/content/Context;)Lkotlin/Unit;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->previousChannelId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Lcom/discord/utilities/channel/ChannelUtils;->delete(Landroid/content/Context;J)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final handleOnIndexClicked(Lcom/discord/utilities/textprocessing/node/SpoilerNode;)V
    .locals 18
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/textprocessing/node/SpoilerNode<",
            "*>;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string/jumbo v1, "spoilerNode"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    if-nez v3, :cond_0

    const/4 v1, 0x0

    :cond_0
    check-cast v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getRawTopicString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->generateAST(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v8

    iget-object v3, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/textprocessing/node/SpoilerNode;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v2}, Lx/h/f;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    new-instance v2, Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    const-wide/16 v10, -0x1

    iget-object v12, v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->revealedIndices:Ljava/util/Set;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1c

    const/16 v17, 0x0

    move-object v9, v2

    invoke-direct/range {v9 .. v17}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;-><init>(JLjava/util/Collection;Ljava/util/List;ZLjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v2, v8}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->process(Ljava/util/Collection;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->isLinkifyConflicting()Z

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x79f

    move-object v2, v1

    invoke-static/range {v2 .. v15}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->copy$default(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/List;ZLcom/discord/utilities/channel/GuildChannelIconType;Lcom/discord/models/domain/ModelChannel;ZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final listenForEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setStoreChannels(Lcom/discord/stores/StoreChannels;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-void
.end method

.method public final setTopicParser(Lcom/discord/simpleast/core/parser/Parser;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->topicParser:Lcom/discord/simpleast/core/parser/Parser;

    return-void
.end method
