.class public Lcom/discord/widgets/channels/WidgetCreateChannel;
.super Lcom/discord/app/AppFragment;
.source "WidgetCreateChannel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;,
        Lcom/discord/widgets/channels/WidgetCreateChannel$Model;
    }
.end annotation


# static fields
.field private static final INTENT_CATEGORY_ID:Ljava/lang/String; = "INTENT_CATEGORY_ID"

.field private static final INTENT_GUILD_ID:Ljava/lang/String; = "INTENT_GUILD_ID"

.field private static final INTENT_TYPE:Ljava/lang/String; = "INTENT_TYPE"


# instance fields
.field private categoryId:Ljava/lang/Long;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private channelNameLayout:Lcom/google/android/material/textfield/TextInputLayout;

.field private guildId:J

.field private privateChannelContainer:Landroid/view/View;

.field private privateInfo:Landroid/widget/TextView;

.field private privateSwitch:Landroidx/appcompat/widget/SwitchCompat;

.field private privateTitle:Landroid/widget/TextView;

.field private radioManager:Lcom/discord/views/RadioManager;

.field private roleHeader:Landroid/widget/TextView;

.field private rolesAdapter:Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;

.field private rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private textChannelContainer:Landroid/view/View;

.field private textRadio:Landroid/widget/RadioButton;

.field private type:I

.field private typeContainer:Landroid/view/View;

.field private voiceChannelContainer:Landroid/view/View;

.field private voiceRadio:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/channels/WidgetCreateChannel$Model;)V
    .locals 2

    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetCreateChannel$Model;->access$000(Lcom/discord/widgets/channels/WidgetCreateChannel$Model;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const v0, 0x7f120569

    goto :goto_0

    :cond_1
    const v0, 0x7f12056a

    :goto_0
    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesAdapter:Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;

    invoke-static {p1}, Lcom/discord/widgets/channels/WidgetCreateChannel$Model;->access$100(Lcom/discord/widgets/channels/WidgetCreateChannel$Model;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;->setData(Ljava/util/List;)V

    return-void

    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_3
    return-void
.end method

.method private createChannel(Ljava/lang/String;JILjava/lang/Long;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/Long;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p2, p3, p4, p6}, Lcom/discord/widgets/channels/WidgetCreateChannel;->getPermissionOverwrites(JILjava/util/Set;)Ljava/util/List;

    move-result-object v5

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p6

    new-instance v7, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;

    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move v1, p4

    move-object v3, p1

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p6, p2, p3, v7}, Lcom/discord/utilities/rest/RestAPI;->createGuildChannel(JLcom/discord/restapi/RestAPIParams$CreateGuildChannel;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lf/a/o/a/k1;

    invoke-direct {p2, p0}, Lf/a/o/a/k1;-><init>(Lcom/discord/widgets/channels/WidgetCreateChannel;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p2, p3}, Lf/a/b/r;->j(Lrx/functions/Action1;Landroid/content/Context;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/channels/WidgetCreateChannel;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetCreateChannel;->onChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public static synthetic g(Lcom/discord/widgets/channels/WidgetCreateChannel;Lcom/discord/widgets/channels/WidgetCreateChannel$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetCreateChannel;->configureUI(Lcom/discord/widgets/channels/WidgetCreateChannel$Model;)V

    return-void
.end method

.method private getChannelType()I
    .locals 2

    iget v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceRadio:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private getCheckedRoles()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateSwitch:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesAdapter:Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;->access$200(Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    iget-wide v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0
.end method

.method private getPermissionOverwrites(JILjava/util/Set;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;",
            ">;"
        }
    .end annotation

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    const-wide/32 v0, 0x100000

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x400

    :goto_0
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p1, p2, v4, v5}, Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;->createForRole(JLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    invoke-static {v4, v5, p2, p4}, Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;->createForRole(JLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object p3
.end method

.method private onChannelCreated(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/discord/utilities/channel/ChannelSelector;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(Lcom/discord/models/domain/ModelChannel;)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->hideKeyboard()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_1
    return-void
.end method

.method public static show(Landroid/content/Context;J)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/discord/widgets/channels/WidgetCreateChannel;->show(Landroid/content/Context;JI)V

    return-void
.end method

.method public static show(Landroid/content/Context;JI)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/discord/widgets/channels/WidgetCreateChannel;->show(Landroid/content/Context;JILjava/lang/Long;)V

    return-void
.end method

.method public static show(Landroid/content/Context;JILjava/lang/Long;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p1, "INTENT_TYPE"

    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p4, :cond_0

    const-string p1, "INTENT_CATEGORY_ID"

    invoke-virtual {v0, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    const-class p1, Lcom/discord/widgets/channels/WidgetCreateChannel;

    invoke-static {p0, p1, v0}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01e2

    return v0
.end method

.method public synthetic h(Landroid/view/MenuItem;Landroid/content/Context;)V
    .locals 7

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x7f0a06a6

    if-ne p1, p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->channelNameLayout:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->guildId:J

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetCreateChannel;->getChannelType()I

    move-result v4

    iget-object v5, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->categoryId:Ljava/lang/Long;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetCreateChannel;->getCheckedRoles()Ljava/util/Set;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetCreateChannel;->createChannel(Ljava/lang/String;JILjava/lang/Long;Ljava/util/Set;)V

    :cond_0
    return-void
.end method

.method public synthetic i(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->radioManager:Lcom/discord/views/RadioManager;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->textRadio:Landroid/widget/RadioButton;

    invoke-virtual {p1, v0}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    return-void
.end method

.method public synthetic j(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->radioManager:Lcom/discord/views/RadioManager;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceRadio:Landroid/widget/RadioButton;

    invoke-virtual {p1, v0}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    return-void
.end method

.method public synthetic k(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateSwitch:Landroidx/appcompat/widget/SwitchCompat;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->roleHeader:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateSwitch:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->roleHeader:Landroid/widget/TextView;

    invoke-static {v0, p1}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, p1}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a030a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->channelNameLayout:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f0a030f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a0312

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->textRadio:Landroid/widget/RadioButton;

    const v0, 0x7f0a0315

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceRadio:Landroid/widget/RadioButton;

    const v0, 0x7f0a030d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateSwitch:Landroidx/appcompat/widget/SwitchCompat;

    const v0, 0x7f0a030e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateTitle:Landroid/widget/TextView;

    const v0, 0x7f0a030c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateInfo:Landroid/widget/TextView;

    const v0, 0x7f0a0311

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->textChannelContainer:Landroid/view/View;

    const v0, 0x7f0a0314

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceChannelContainer:Landroid/view/View;

    const v0, 0x7f0a030b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateChannelContainer:Landroid/view/View;

    const v0, 0x7f0a0310

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->roleHeader:Landroid/widget/TextView;

    const v0, 0x7f0a0313

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->typeContainer:Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled(Z)Landroidx/appcompat/widget/Toolbar;

    new-instance p1, Lf/a/o/a/g1;

    invoke-direct {p1, p0}, Lf/a/o/a/g1;-><init>(Lcom/discord/widgets/channels/WidgetCreateChannel;)V

    const v0, 0x7f0e0004

    invoke-virtual {p0, v0, p1}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;)Landroidx/appcompat/widget/Toolbar;

    new-instance p1, Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p1, v0}, Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-static {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesAdapter:Lcom/discord/widgets/channels/WidgetCreateChannel$RolesAdapter;

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->rolesRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 6

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->guildId:J

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_TYPE"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_CATEGORY_ID"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->categoryId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v5, v0, v2

    if-gez v5, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->categoryId:Ljava/lang/Long;

    :cond_0
    iget-wide v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->guildId:J

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->categoryId:Ljava/lang/Long;

    invoke-static {v0, v1, v2}, Lcom/discord/widgets/channels/WidgetCreateChannel$Model;->get(JLjava/lang/Long;)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/a/m1;

    invoke-direct {v1, p0}, Lf/a/o/a/m1;-><init>(Lcom/discord/widgets/channels/WidgetCreateChannel;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    new-instance v0, Lcom/discord/views/RadioManager;

    const/4 v1, 0x2

    new-array v2, v1, [Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->textRadio:Landroid/widget/RadioButton;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceRadio:Landroid/widget/RadioButton;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->radioManager:Lcom/discord/views/RadioManager;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->textChannelContainer:Landroid/view/View;

    new-instance v2, Lf/a/o/a/l1;

    invoke-direct {v2, p0}, Lf/a/o/a/l1;-><init>(Lcom/discord/widgets/channels/WidgetCreateChannel;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceChannelContainer:Landroid/view/View;

    new-instance v2, Lf/a/o/a/h1;

    invoke-direct {v2, p0}, Lf/a/o/a/h1;-><init>(Lcom/discord/widgets/channels/WidgetCreateChannel;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->radioManager:Lcom/discord/views/RadioManager;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->textRadio:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    goto :goto_0

    :cond_1
    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->radioManager:Lcom/discord/views/RadioManager;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->voiceRadio:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateChannelContainer:Landroid/view/View;

    new-instance v1, Lf/a/o/a/n1;

    invoke-direct {v1, p0}, Lf/a/o/a/n1;-><init>(Lcom/discord/widgets/channels/WidgetCreateChannel;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->typeContainer:Landroid/view/View;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    const/4 v4, 0x1

    :cond_3
    invoke-static {v0, v4}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    if-ne v1, v2, :cond_4

    const v1, 0x7f121471

    goto :goto_1

    :cond_4
    const v1, 0x7f121473

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->privateInfo:Landroid/widget/TextView;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    if-ne v1, v2, :cond_5

    const v1, 0x7f121472

    goto :goto_2

    :cond_5
    const v1, 0x7f12148c

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->roleHeader:Landroid/widget/TextView;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    if-ne v1, v2, :cond_6

    const v1, 0x7f1207b5

    goto :goto_3

    :cond_6
    const v1, 0x7f1207b8

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->channelNameLayout:Lcom/google/android/material/textfield/TextInputLayout;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetCreateChannel;->type:I

    if-ne v1, v2, :cond_7

    const v1, 0x7f120407

    goto :goto_4

    :cond_7
    const v1, 0x7f1207b6

    :goto_4
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method
