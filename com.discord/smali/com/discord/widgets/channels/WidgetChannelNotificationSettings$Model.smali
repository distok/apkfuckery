.class public final Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;
.super Ljava/lang/Object;
.source "WidgetChannelNotificationSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;

.field public static final RADIO_DIMMED_ALPHA:F = 0.3f

.field public static final RADIO_OPAQUE_ALPHA:F = 1.0f


# instance fields
.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelIsMuted:Z

.field private final channelMuteEndTime:Ljava/lang/String;

.field private final isAboveNotifyAllSize:Z

.field private final isGuildMuted:Z

.field private final notificationSetting:I

.field private final notificationSettingIsInherited:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;ZLjava/lang/String;ZIZZ)V
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    iput-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    iput p5, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    iput-boolean p6, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    iput-boolean p7, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;Lcom/discord/models/domain/ModelChannel;ZLjava/lang/String;ZIZZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move p4, p9

    move-object p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->copy(Lcom/discord/models/domain/ModelChannel;ZLjava/lang/String;ZIZZ)Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;ZLjava/lang/String;ZIZZ)Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;
    .locals 9

    const-string v0, "channel"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;

    move-object v1, v0

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;-><init>(Lcom/discord/models/domain/ModelChannel;ZLjava/lang/String;ZIZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    iget v1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    iget-boolean v1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getChannelIsMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    return v0
.end method

.method public final getChannelMuteEndTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public final getNotificationSetting()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    return v0
.end method

.method public final getNotificationSettingIsInherited()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    if-eqz v1, :cond_5

    goto :goto_1

    :cond_5
    move v3, v1

    :goto_1
    add-int/2addr v0, v3

    return v0
.end method

.method public final isAboveNotifyAllSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    return v0
.end method

.method public final isGuildMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Model(channel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelIsMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelIsMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", channelMuteEndTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->channelMuteEndTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isGuildMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isGuildMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", notificationSetting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSetting:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", notificationSettingIsInherited="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->notificationSettingIsInherited:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAboveNotifyAllSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Model;->isAboveNotifyAllSize:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
