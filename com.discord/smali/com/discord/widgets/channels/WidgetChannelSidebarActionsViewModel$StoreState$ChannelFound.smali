.class public final Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;
.super Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;
.source "WidgetChannelSidebarActionsViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChannelFound"
.end annotation


# instance fields
.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final disablePins:Z

.field private final guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelNotificationSettings;Z)V
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    iput-boolean p3, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelNotificationSettings;ZILjava/lang/Object;)Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->copy(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelNotificationSettings;Z)Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelNotificationSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelNotificationSettings;Z)Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;
    .locals 1

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelNotificationSettings;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    iget-object v1, p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    iget-boolean p1, p1, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getDisablePins()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    return v0
.end method

.method public final getGuildNotificationSettings()Lcom/discord/models/domain/ModelNotificationSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelNotificationSettings;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ChannelFound(channel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildNotificationSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->guildNotificationSettings:Lcom/discord/models/domain/ModelNotificationSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", disablePins="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/channels/WidgetChannelSidebarActionsViewModel$StoreState$ChannelFound;->disablePins:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
