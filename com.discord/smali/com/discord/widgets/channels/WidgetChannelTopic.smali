.class public final Lcom/discord/widgets/channels/WidgetChannelTopic;
.super Lcom/discord/app/AppFragment;
.source "WidgetChannelTopic.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final channelIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelTopicEllipsis$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelTopicTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final moreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const-string v3, "moreButton"

    const-string v4, "getMoreButton()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const-string v6, "channelTopicTitle"

    const-string v7, "getChannelTopicTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const-string v6, "channelTopicEllipsis"

    const-string v7, "getChannelTopicEllipsis()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const-string v6, "channelName"

    const-string v7, "getChannelName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const-string v6, "channelIcon"

    const-string v7, "getChannelIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/channels/WidgetChannelTopic;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a01d3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->moreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01d5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelTopicTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01d2

    invoke-static {p0, v0}, Ly/a/g0;->e(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelTopicEllipsis$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01d4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a01d1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureEllipsis(Lcom/discord/widgets/channels/WidgetChannelTopic;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->configureEllipsis()V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/channels/WidgetChannelTopic;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopic;->configureUI(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getChannelTopicTitle$p(Lcom/discord/widgets/channels/WidgetChannelTopic;)Landroid/widget/TextView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/channels/WidgetChannelTopic;)Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->viewModel:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/channels/WidgetChannelTopic;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopic;->handleEvent(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$onClickMore(Lcom/discord/widgets/channels/WidgetChannelTopic;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopic;->onClickMore(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$onToggleTopicExpansionState(Lcom/discord/widgets/channels/WidgetChannelTopic;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->onToggleTopicExpansionState()V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/channels/WidgetChannelTopic;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->viewModel:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    return-void
.end method

.method private final configureEllipsis()V
    .locals 3

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicEllipsis()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopic$configureEllipsis$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$configureEllipsis$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicEllipsis()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getMaxLines()I

    move-result v1

    const/16 v2, 0x28

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_2
    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V
    .locals 10

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$NoChannel;

    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, v3}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/discord/utilities/channel/GuildChannelIconType$Text;->INSTANCE:Lcom/discord/utilities/channel/GuildChannelIconType$Text;

    invoke-static {v0}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->mapGuildChannelTypeToIcon(Lcom/discord/utilities/channel/GuildChannelIconType;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->setChannelIcon(I)V

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x7

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0, v2}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild;->getChannelIconType()Lcom/discord/utilities/channel/GuildChannelIconType;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->mapGuildChannelTypeToIcon(Lcom/discord/utilities/channel/GuildChannelIconType;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->setChannelIcon(I)V

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getRenderedTopicForDefaultTopic(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, "null cannot be cast to non-null type com.discord.widgets.channels.WidgetChannelTopicViewModel.ViewState.Guild.Topic"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getRenderedTopicForTopic(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    move-result-object v0

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0, v2}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0802da

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->setChannelIcon(I)V

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getRenderedTopicForDM(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    move-result-object v0

    goto :goto_0

    :cond_6
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0, v2}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_7
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/discord/widgets/channels/WidgetChannelTopic;->setChannelIconForGDM(Lcom/discord/models/domain/ModelChannel;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getRenderedTopicForGDM(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    move-result-object v0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->getTopic()Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_8

    const/4 v5, 0x1

    goto :goto_1

    :cond_8
    const/4 v5, 0x0

    :goto_1
    if-ne v5, v2, :cond_9

    goto :goto_2

    :cond_9
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_a

    const/4 v1, 0x0

    :cond_a
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/channels/WidgetChannelTopic$configureUI$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$configureUI$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->getAutoLinkMask()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->getTopic()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/channels/WidgetChannelTopic$configureUI$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$configureUI$2;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelName()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;->getChannelName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopic$configureUI$3;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/channels/WidgetChannelTopic$configureUI$3;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getChannelIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopic;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getChannelName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopic;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChannelTopicEllipsis()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelTopicEllipsis$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopic;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelTopicTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->channelTopicTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopic;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMoreButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->moreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelTopic;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getRenderedTopicForDM(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    .locals 11

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;->getRecipientNicknames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f1200ef

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "requireContext().getString(R.string.aka)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;->getRecipientNicknames()Ljava/util/Set;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x3e

    const-string v4, ", "

    invoke-static/range {v3 .. v10}, Lx/h/f;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/text/SpannableStringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v3, v2, v1, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    move-object v6, v3

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;->getRecipientName()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final getRenderedTopicForDefaultTopic(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    .locals 4

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    sget-object v1, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$DefaultTopic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "requireContext()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    invoke-direct {v0, p1, v1, v3}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method private final getRenderedTopicForGDM(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    .locals 7

    new-instance v6, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v6
.end method

.method private final getRenderedTopicForTopic(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;)Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;
    .locals 20

    new-instance v13, Lcom/discord/utilities/textprocessing/MessageRenderContext;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "channelTopicTitle.context"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getAllowAnimatedEmojis()Z

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getUserNames()Ljava/util/Map;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannelNames()Ljava/util/Map;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getRoles()Ljava/util/Map;

    move-result-object v7

    sget-object v10, Lcom/discord/widgets/channels/WidgetChannelTopic$getRenderedTopicForTopic$renderContext$1;->INSTANCE:Lcom/discord/widgets/channels/WidgetChannelTopic$getRenderedTopicForTopic$renderContext$1;

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$getRenderedTopicForTopic$renderContext$2;

    move-object/from16 v12, p0

    iget-object v2, v12, Lcom/discord/widgets/channels/WidgetChannelTopic;->viewModel:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    if-eqz v2, :cond_2

    invoke-direct {v0, v2}, Lcom/discord/widgets/channels/WidgetChannelTopic$getRenderedTopicForTopic$renderContext$2;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x36c0

    const/16 v17, 0x0

    const-wide/16 v2, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/16 v18, 0x0

    move/from16 v12, v18

    move-object/from16 v18, v0

    move-object v0, v13

    move-object/from16 v19, v13

    move-object/from16 v13, v18

    invoke-direct/range {v0 .. v17}, Lcom/discord/utilities/textprocessing/MessageRenderContext;-><init>(Landroid/content/Context;JZLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getAst()Ljava/util/List;

    move-result-object v0

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/discord/utilities/textprocessing/AstRenderer;->render(Ljava/util/Collection;Ljava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v2, 0xc8

    const/4 v3, 0x0

    if-gt v1, v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->isLinkifyConflicting()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0xf

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x0

    :goto_1
    new-instance v2, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;

    sget-object v4, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$Guild$Topic;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "requireContext()"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6, v3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelTopic$RenderedTopic;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    return-object v2

    :cond_2
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final handleEvent(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event;)V
    .locals 1

    instance-of p1, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Event$FocusFirstElement;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelName()Landroid/widget/TextView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    :cond_0
    return-void
.end method

.method private final onClickMore(Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState;)V
    .locals 9

    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->getChannelId()J

    move-result-wide v3

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "requireContext()"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v5, v6}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$GDM;->getDeveloperModeEnabled()Z

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/channels/WidgetChannelTopic;->showContextMenu(ZJLjava/lang/String;Z)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;->getChannelId()J

    move-result-wide v3

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$ViewState$DM;->getDeveloperModeEnabled()Z

    move-result v6

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/channels/WidgetChannelTopic;->showContextMenu$default(Lcom/discord/widgets/channels/WidgetChannelTopic;ZJLjava/lang/String;ZILjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final onToggleTopicExpansionState()V
    .locals 3

    new-instance v0, Lcom/discord/widgets/channels/WidgetChannelTopic$onToggleTopicExpansionState$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$onToggleTopicExpansionState$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getMaxLines()I

    move-result v1

    const/16 v2, 0x28

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v2}, Lcom/discord/widgets/channels/WidgetChannelTopic$onToggleTopicExpansionState$1;->invoke(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/discord/widgets/channels/WidgetChannelTopic$onToggleTopicExpansionState$1;->invoke(I)V

    :goto_0
    return-void
.end method

.method private final setChannelIcon(I)V
    .locals 4
    .param p1    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelIcon()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const/4 v2, 0x4

    invoke-static {v2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v2

    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    return-void
.end method

.method private final setChannelIconForGDM(Lcom/discord/models/domain/ModelChannel;)V
    .locals 7

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelIcon()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p1, v1, v2, v1}, Lcom/discord/utilities/icon/IconUtils;->getForChannel$default(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f070067

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x18

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelIcon()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const/16 v2, 0x8

    invoke-static {v2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v2

    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    return-void
.end method

.method private final showContextMenu(ZJLjava/lang/String;Z)V
    .locals 4

    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getMoreButton()Landroid/widget/ImageView;

    move-result-object v2

    const v3, 0x800053

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    const v1, 0x7f0e0013

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0a069c

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$1;

    invoke-direct {v2, p0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;J)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0a069a

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const-string v2, "copyChannelIdAction"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    new-instance p5, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$2;

    invoke-direct {p5, p0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$2;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;J)V

    invoke-interface {v1, p5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object p5

    const v1, 0x7f0a069b

    invoke-interface {p5, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p5

    const-string v1, "customizeGroupAction"

    invoke-static {p5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p5, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    new-instance v1, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$3;

    invoke-direct {v1, p0, p2, p3}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$3;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;J)V

    invoke-interface {p5, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object p2

    const p3, 0x7f0a0699

    invoke-interface {p2, p3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p2

    const/4 p3, 0x1

    if-ne p1, p3, :cond_0

    const p1, 0x7f120f0f

    invoke-interface {p2, p1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;

    invoke-direct {p1, p0, p4}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$4;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    const p1, 0x7f12049a

    invoke-interface {p2, p1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$5;

    invoke-direct {p1, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$showContextMenu$5;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    invoke-interface {p2, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :goto_0
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method

.method public static synthetic showContextMenu$default(Lcom/discord/widgets/channels/WidgetChannelTopic;ZJLjava/lang/String;ZILjava/lang/Object;)V
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    const/4 p4, 0x0

    :cond_0
    move-object v4, p4

    and-int/lit8 p4, p6, 0x8

    if-eqz p4, :cond_1

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move v5, p5

    :goto_0
    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/WidgetChannelTopic;->showContextMenu(ZJLjava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d019e

    return v0
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v11, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7f

    const/4 v10, 0x0

    move-object v1, v11

    invoke-direct/range {v1 .. v10}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel$Factory;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, p0, v11}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(this, \u2026picViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    iput-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->viewModel:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;->listenForEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/channels/WidgetChannelTopic$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/channels/WidgetChannelTopic;->viewModel:Lcom/discord/widgets/channels/WidgetChannelTopicViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/channels/WidgetChannelTopic;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/channels/WidgetChannelTopic$onViewBoundOrOnResume$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/channels/WidgetChannelTopic$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/channels/WidgetChannelTopic;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/channels/WidgetChannelTopic;->getChannelTopicTitle()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
