.class public final Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item$1;
.super Ljava/lang/Object;
.source "WidgetDebugging.kt"

# interfaces
.implements Lrx/functions/Action3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;-><init>(ILcom/discord/widgets/debugging/WidgetDebugging$Adapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action3<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        "Lcom/discord/app/AppLog$LoggedItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item$1;->this$0:Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/app/AppLog$LoggedItem;)V
    .locals 1

    const-string/jumbo p2, "view"

    const-string/jumbo p3, "view.context"

    invoke-static {p1, p2, p3}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item$1;->this$0:Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;

    invoke-static {p2}, Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;->access$getMessage$p(Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item;)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "message.text"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x0

    const/4 v0, 0x4

    invoke-static {p1, p2, p3, v0}, Lf/a/b/p;->c(Landroid/content/Context;Ljava/lang/CharSequence;II)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/app/AppLog$LoggedItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/debugging/WidgetDebugging$Adapter$Item$1;->call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/app/AppLog$LoggedItem;)V

    return-void
.end method
