.class public final Lcom/discord/widgets/debugging/WidgetDebugging$Model;
.super Ljava/lang/Object;
.source "WidgetDebugging.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/debugging/WidgetDebugging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation


# instance fields
.field private final isFiltered:Z

.field private final logs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/app/AppLog$LoggedItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/app/AppLog$LoggedItem;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "logs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    iput-boolean p2, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/debugging/WidgetDebugging$Model;Ljava/util/List;ZILjava/lang/Object;)Lcom/discord/widgets/debugging/WidgetDebugging$Model;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->copy(Ljava/util/List;Z)Lcom/discord/widgets/debugging/WidgetDebugging$Model;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/app/AppLog$LoggedItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    return v0
.end method

.method public final copy(Ljava/util/List;Z)Lcom/discord/widgets/debugging/WidgetDebugging$Model;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/app/AppLog$LoggedItem;",
            ">;Z)",
            "Lcom/discord/widgets/debugging/WidgetDebugging$Model;"
        }
    .end annotation

    const-string v0, "logs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/debugging/WidgetDebugging$Model;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/debugging/WidgetDebugging$Model;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/debugging/WidgetDebugging$Model;

    iget-object v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    iget-boolean p1, p1, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLogs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/app/AppLog$LoggedItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isFiltered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Model(logs="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->logs:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isFiltered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/debugging/WidgetDebugging$Model;->isFiltered:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
