.class public final Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;
.super Ljava/lang/Object;
.source "ManageReactionsModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->get()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $getCanManageMessagesObs$1:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;

.field public final synthetic $reactionsObs:Lrx/Observable;

.field public final synthetic this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;Lrx/Observable;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    iput-object p2, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->$getCanManageMessagesObs$1:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;

    iput-object p3, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->$reactionsObs:Lrx/Observable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->call(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->$getCanManageMessagesObs$1:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;

    const-string v1, "me"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;->invoke(Z)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->$reactionsObs:Lrx/Observable;

    sget-object v2, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$1;->INSTANCE:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$1;

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2;-><init>(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
