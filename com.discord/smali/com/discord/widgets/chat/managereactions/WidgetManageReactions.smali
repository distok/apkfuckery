.class public final Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;
.super Lcom/discord/app/AppFragment;
.source "WidgetManageReactions.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;

.field private static final EXTRA_EMOJI_KEY:Ljava/lang/String; = "com.discord.intent.extra.EXTRA_EMOJI_KEY"


# instance fields
.field private emojisAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;

.field private final emojisRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

.field private resultsAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter;

.field private final usersRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;

    const-string v3, "emojisRecycler"

    const-string v4, "getEmojisRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;

    const-string/jumbo v6, "usersRecycler"

    const-string v7, "getUsersRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->Companion:Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0651

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->emojisRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0657

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->usersRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->configureUI(Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;)V

    return-void
.end method

.method public static final synthetic access$getModelProvider$p(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;)Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "modelProvider"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setModelProvider$p(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;)V
    .locals 3

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->emojisAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;->getReactionItems()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->resultsAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;->getUserItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void

    :cond_2
    const-string p1, "resultsAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string p1, "emojisAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public static final create(JJLandroid/content/Context;Lcom/discord/models/domain/ModelMessageReaction;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->Companion:Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;

    move-wide v1, p0

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;->create(JJLandroid/content/Context;Lcom/discord/models/domain/ModelMessageReaction;)V

    return-void
.end method

.method private final getEmojisRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->emojisRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getUsersRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->usersRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0232

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const p1, 0x7f1214e4

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 9

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "com.discord.intent.extra.EXTRA_MESSAGE_ID"

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "com.discord.intent.extra.EXTRA_EMOJI_KEY"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance p1, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;-><init>(JJLjava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    :cond_0
    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->getEmojisRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;

    iput-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->emojisAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;

    new-instance v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->getUsersRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->resultsAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter;

    iget-object p1, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->emojisAdapter:Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    new-instance v1, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$onViewBound$2;

    iget-object v2, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    if-eqz v2, :cond_1

    invoke-direct {v1, v2}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$onViewBound$2;-><init>(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;)V

    invoke-virtual {p1, v1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter;->setOnEmojiSelectedListener(Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_1
    const-string p1, "modelProvider"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_2
    const-string p1, "emojisAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->modelProvider:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->get()Lrx/Observable;

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "modelProvider"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
