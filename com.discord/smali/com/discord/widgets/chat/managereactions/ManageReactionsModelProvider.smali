.class public final Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;
.super Ljava/lang/Object;
.source "ManageReactionsModel.kt"


# instance fields
.field private final channelId:J

.field private final messageId:J

.field private final targetedEmojiKeySubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->channelId:J

    iput-wide p3, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->messageId:J

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {p5}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->targetedEmojiKeySubject:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public synthetic constructor <init>(JJLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;-><init>(JJLjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$createModel(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;Ljava/util/List;Lcom/discord/stores/StoreMessageReactions$EmojiResults;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZJ)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->createModel(Ljava/util/List;Lcom/discord/stores/StoreMessageReactions$EmojiResults;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZJ)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTargetedEmojiKeySubject$p(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;)Lrx/subjects/SerializedSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->targetedEmojiKeySubject:Lrx/subjects/SerializedSubject;

    return-object p0
.end method

.method public static final synthetic access$getUsersForReaction(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;Lcom/discord/models/domain/ModelMessageReaction$Emoji;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->getUsersForReaction(Lcom/discord/models/domain/ModelMessageReaction$Emoji;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final createModel(Ljava/util/List;Lcom/discord/stores/StoreMessageReactions$EmojiResults;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZJ)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;",
            "Lcom/discord/stores/StoreMessageReactions$EmojiResults;",
            "Lcom/discord/models/domain/ModelMessageReaction$Emoji;",
            "ZJ)",
            "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelMessageReaction;

    new-instance v5, Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter$ReactionEmojiItem;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v6

    move-object/from16 v7, p3

    invoke-static {v6, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    invoke-direct {v5, v4, v6}, Lcom/discord/widgets/chat/managereactions/ManageReactionsEmojisAdapter$ReactionEmojiItem;-><init>(Lcom/discord/models/domain/ModelMessageReaction;Z)V

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    instance-of v3, v1, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;

    if-eqz v3, :cond_4

    check-cast v1, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;->getUsers()Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    const-string v4, "results.users.values"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/discord/models/domain/ModelUser;

    new-instance v5, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter$ReactionUserItem;

    const-string/jumbo v6, "user"

    invoke-static {v7, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v8, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->channelId:J

    iget-wide v10, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->messageId:J

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v12

    if-nez p4, :cond_2

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v13

    cmp-long v6, v13, p5

    if-nez v6, :cond_1

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    const/4 v13, 0x0

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v6, 0x1

    const/4 v13, 0x1

    :goto_3
    move-object v6, v5

    invoke-direct/range {v6 .. v13}, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter$ReactionUserItem;-><init>(Lcom/discord/models/domain/ModelUser;JJLcom/discord/models/domain/ModelMessageReaction$Emoji;Z)V

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v4}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    goto :goto_4

    :cond_4
    instance-of v3, v1, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Loading;

    if-eqz v3, :cond_5

    new-instance v1, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter$LoadingItem;

    invoke-direct {v1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter$LoadingItem;-><init>()V

    invoke-static {v1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto :goto_4

    :cond_5
    instance-of v3, v1, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;

    if-eqz v3, :cond_6

    new-instance v3, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter$ErrorItem;

    check-cast v1, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;->getChannelId()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;->getMessageId()J

    move-result-wide v6

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    move-object/from16 p1, v3

    move-wide/from16 p2, v4

    move-wide/from16 p4, v6

    move-object/from16 p6, v1

    invoke-direct/range {p1 .. p6}, Lcom/discord/widgets/chat/managereactions/ManageReactionsResultsAdapter$ErrorItem;-><init>(JJLcom/discord/models/domain/ModelMessageReaction$Emoji;)V

    invoke-static {v3}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :goto_4
    new-instance v3, Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;

    invoke-direct {v3, v2, v1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v3

    :cond_6
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method private final getUsersForReaction(Lcom/discord/models/domain/ModelMessageReaction$Emoji;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelMessageReaction$Emoji;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreMessageReactions$EmojiResults;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageReactions()Lcom/discord/stores/StoreMessageReactions;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->channelId:J

    iget-wide v4, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->messageId:J

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreMessageReactions;->getMessageReactions(JJLcom/discord/models/domain/ModelMessageReaction$Emoji;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final get()Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;-><init>(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;)V

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v2

    iget-wide v3, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->channelId:J

    iget-wide v5, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->messageId:J

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/discord/stores/StoreMessages;->observeMessagesForChannel(JJ)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$reactionsObs$1;->INSTANCE:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$reactionsObs$1;

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$reactionsObs$2;->INSTANCE:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$reactionsObs$2;

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    const-string v3, "StoreStream\n        .get\u2026ion -> reaction.count } }"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    new-instance v3, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;

    invoke-direct {v3, p0, v0, v2}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;-><init>(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$1;Lrx/Observable;)V

    invoke-virtual {v1, v3}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n        .get\u2026              }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n        .get\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->channelId:J

    return-wide v0
.end method

.method public final getMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->messageId:J

    return-wide v0
.end method

.method public final onEmojiTargeted(Ljava/lang/String;)V
    .locals 1

    const-string v0, "emojiKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->targetedEmojiKeySubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method
