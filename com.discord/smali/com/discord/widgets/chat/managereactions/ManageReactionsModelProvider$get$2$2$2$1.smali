.class public final Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;
.super Ljava/lang/Object;
.source "ManageReactionsModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;->call(Lcom/discord/models/domain/ModelMessageReaction$Emoji;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreMessageReactions$EmojiResults;",
        "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $targetedEmoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

.field public final synthetic this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;Lcom/discord/models/domain/ModelMessageReaction$Emoji;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;

    iput-object p2, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->$targetedEmoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StoreMessageReactions$EmojiResults;)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;

    iget-object v1, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2;

    iget-object v1, v1, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;

    iget-object v2, v1, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    iget-object v3, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;->$reactions:Ljava/util/List;

    const-string v0, "reactions"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "users"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->$targetedEmoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;

    iget-object v0, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;->$canManageMessages:Ljava/lang/Boolean;

    const-string v1, "canManageMessages"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget-object v0, p0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;

    iget-object v0, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2;->this$0:Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2;

    iget-object v0, v0, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2;->$me:Lcom/discord/models/domain/ModelUser;

    const-string v1, "me"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    move-object v4, p1

    invoke-static/range {v2 .. v8}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;->access$createModel(Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;Ljava/util/List;Lcom/discord/stores/StoreMessageReactions$EmojiResults;Lcom/discord/models/domain/ModelMessageReaction$Emoji;ZJ)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreMessageReactions$EmojiResults;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider$get$2$2$2$1;->call(Lcom/discord/stores/StoreMessageReactions$EmojiResults;)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;

    move-result-object p1

    return-object p1
.end method
