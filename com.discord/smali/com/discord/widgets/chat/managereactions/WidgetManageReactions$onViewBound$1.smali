.class public final synthetic Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$onViewBound$1;
.super Lx/m/c/m;
.source "WidgetManageReactions.kt"


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;)V
    .locals 6

    const-class v2, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;

    const-string v3, "modelProvider"

    const-string v4, "getModelProvider()Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lx/m/c/m;-><init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;

    invoke-static {v0}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->access$getModelProvider$p(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;)Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;

    check-cast p1, Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->access$setModelProvider$p(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;)V

    return-void
.end method
