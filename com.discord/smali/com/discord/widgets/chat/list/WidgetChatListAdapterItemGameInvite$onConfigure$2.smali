.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;
.super Lx/m/c/k;
.source "WidgetChatListAdapterItemGameInvite.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;->invoke(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;

    invoke-static {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->access$getGameInviteView$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->access$getAdapter$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getClock()Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->bind(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/utilities/time/Clock;)V

    return-void
.end method
