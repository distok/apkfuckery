.class public final Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;
.super Ljava/lang/Object;
.source "PublishActionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final followerStats:Lcom/discord/models/domain/ModelChannelFollowerStats;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannelFollowerStats;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;->followerStats:Lcom/discord/models/domain/ModelChannelFollowerStats;

    return-void
.end method


# virtual methods
.method public final getFollowerStats()Lcom/discord/models/domain/ModelChannelFollowerStats;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;->followerStats:Lcom/discord/models/domain/ModelChannelFollowerStats;

    return-object v0
.end method
