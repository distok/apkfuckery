.class public final Lcom/discord/widgets/chat/list/InlineMediaView;
.super Landroidx/cardview/widget/CardView;
.source "InlineMediaView.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lcom/discord/app/AppComponent;


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

.field private compositeSubscription:Lrx/subscriptions/CompositeSubscription;

.field private embed:Lcom/discord/models/domain/ModelMessageEmbed;

.field private featureTag:Ljava/lang/String;

.field private final imagePreview$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final loadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final playButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;

.field private final unsubscribeSignal:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final volumeToggle$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/list/InlineMediaView;

    const-string v3, "imagePreview"

    const-string v4, "getImagePreview()Lcom/facebook/drawee/view/SimpleDraweeView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/InlineMediaView;

    const-string v6, "playButton"

    const-string v7, "getPlayButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/InlineMediaView;

    const-string v6, "playerView"

    const-string v7, "getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/InlineMediaView;

    const-string v6, "loadingIndicator"

    const-string v7, "getLoadingIndicator()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/InlineMediaView;

    const-string/jumbo v6, "volumeToggle"

    const-string v7, "getVolumeToggle()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/list/InlineMediaView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;)V

    const p1, 0x7f0a05aa

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->imagePreview$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ac

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ad

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ab

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->loadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ae

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->volumeToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    const-string p1, ""

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->featureTag:Ljava/lang/String;

    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0d006d

    invoke-static {p1, v0, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0, p0}, Landroid/widget/FrameLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    const-string v0, "PublishSubject.create()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->unsubscribeSignal:Lrx/subjects/Subject;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p1, 0x7f0a05aa

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->imagePreview$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ac

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ad

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ab

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->loadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ae

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->volumeToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    const-string p1, ""

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->featureTag:Ljava/lang/String;

    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d006d

    invoke-static {p1, p2, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0, p0}, Landroid/widget/FrameLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    const-string p2, "PublishSubject.create()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->unsubscribeSignal:Lrx/subjects/Subject;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroidx/cardview/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p1, 0x7f0a05aa

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->imagePreview$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ac

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ad

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ab

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->loadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a05ae

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->volumeToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    const-string p1, ""

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->featureTag:Ljava/lang/String;

    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d006d

    invoke-static {p1, p2, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0, p0}, Landroid/widget/FrameLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    const-string p2, "PublishSubject.create()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->unsubscribeSignal:Lrx/subjects/Subject;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/InlineMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/InlineMediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$getAppMediaPlayer$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Lcom/discord/player/AppMediaPlayer;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    return-object p0
.end method

.method public static final synthetic access$getCompositeSubscription$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method

.method public static final synthetic access$getLoadingIndicator$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getLoadingIndicator()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPlayButton$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/widget/ImageView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayButton()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPlayerView$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Lcom/google/android/exoplayer2/ui/PlayerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getVolumeToggle$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/widget/ImageView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getVolumeToggle()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handlePlayerEvent(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/AppMediaPlayer$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->handlePlayerEvent(Lcom/discord/player/AppMediaPlayer$Event;)V

    return-void
.end method

.method public static final synthetic access$setAppMediaPlayer$p(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/AppMediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    return-void
.end method

.method public static final synthetic access$setCompositeSubscription$p(Lcom/discord/widgets/chat/list/InlineMediaView;Lrx/subscriptions/CompositeSubscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method private final clearPlayerAndSubscriptions()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->releasePlayer()V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final getImagePreview()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->imagePreview$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/InlineMediaView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getLoadingIndicator()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->loadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/InlineMediaView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPlayButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/InlineMediaView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/InlineMediaView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/PlayerView;

    return-object v0
.end method

.method private final getVolumeToggle()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->volumeToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/InlineMediaView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final handlePlayerEvent(Lcom/discord/player/AppMediaPlayer$Event;)V
    .locals 2

    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$a;->a:Lcom/discord/player/AppMediaPlayer$Event$a;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getImagePreview()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getLoadingIndicator()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$f;->a:Lcom/discord/player/AppMediaPlayer$Event$f;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->embed:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getVolumeToggle()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->isVideo()Z

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x0

    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$e;->a:Lcom/discord/player/AppMediaPlayer$Event$e;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->resetCurrentEmbed()V

    :cond_4
    :goto_1
    return-void
.end method

.method private final releasePlayer()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/player/AppMediaPlayer;->c()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    return-void
.end method

.method private final resetCurrentEmbed()V
    .locals 7

    iget-object v1, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->embed:Lcom/discord/models/domain/ModelMessageEmbed;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->featureTag:Ljava/lang/String;

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed$default(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final resetViews()V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->embed:Lcom/discord/models/domain/ModelMessageEmbed;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getImagePreview()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getVolumeToggle()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayButton()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->isVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getLoadingIndicator()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getVolumeToggle()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public static synthetic updateUIWithAttachment$default(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/models/domain/ModelMessageAttachment;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    move-object p3, v0

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithAttachment(Lcom/discord/models/domain/ModelMessageAttachment;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic updateUIWithEmbed$default(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    move-object p3, v0

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method private final updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iput-object v1, v0, Lcom/discord/widgets/chat/list/InlineMediaView;->embed:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->resetViews()V

    iget-object v2, v0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v2}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v3, v4, :cond_1

    :cond_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->requestLayout()V

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getImagePreview()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v3

    sget-object v2, Lcom/discord/utilities/embed/EmbedResourceUtils;->INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v4

    const-string v5, "embed.image"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "embed.image.proxyUrl"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Lcom/discord/utilities/embed/EmbedResourceUtils;->getPreviewUrls(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xfc

    const/4 v12, 0x0

    invoke-static/range {v3 .. v12}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getVideo()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getUrl()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_4
    move-object v4, v3

    :goto_0
    if-eqz v4, :cond_7

    iget-object v2, v0, Lcom/discord/widgets/chat/list/InlineMediaView;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v2, :cond_5

    goto :goto_1

    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v5, "context"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lf/a/g/i;->a(Landroid/content/Context;)Lcom/discord/player/AppMediaPlayer;

    move-result-object v2

    :goto_1
    iput-object v2, v0, Lcom/discord/widgets/chat/list/InlineMediaView;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/exoplayer2/ui/PlayerView;->setVisibility(I)V

    move-object/from16 v5, p4

    invoke-static {v1, v4, v5}, Ls/a/b/b/a;->n(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/player/MediaSource;

    move-result-object v4

    new-instance v5, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v5}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v5, v0, Lcom/discord/widgets/chat/list/InlineMediaView;->compositeSubscription:Lrx/subscriptions/CompositeSubscription;

    iget-object v5, v2, Lcom/discord/player/AppMediaPlayer;->d:Lrx/subjects/BehaviorSubject;

    const/4 v6, 0x2

    invoke-static {v5, v0, v3, v6, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v7

    const-class v8, Lcom/discord/widgets/chat/list/InlineMediaView;

    const/4 v5, 0x0

    new-instance v10, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$1;

    invoke-direct {v10, v0}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$1;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v13, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;

    invoke-direct {v13, v0, v2}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/AppMediaPlayer;)V

    const/16 v18, 0x1a

    const/16 v19, 0x0

    move-object v9, v5

    move-object/from16 v11, v16

    move-object/from16 v12, v17

    move/from16 v14, v18

    move-object/from16 v15, v19

    invoke-static/range {v7 .. v15}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v7, v2, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    invoke-static {v7, v0, v3, v6, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v9

    const-class v10, Lcom/discord/widgets/chat/list/InlineMediaView;

    new-instance v12, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$3;

    invoke-direct {v12, v0}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$3;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView;)V

    new-instance v15, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$4;

    invoke-direct {v15, v0}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$4;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView;)V

    move-object v11, v5

    move-object/from16 v13, v16

    move-object/from16 v14, v17

    move/from16 v16, v18

    move-object/from16 v17, v19

    invoke-static/range {v9 .. v17}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessageEmbed;->isGifv()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v7, 0x1

    const/4 v8, 0x1

    const-wide/16 v9, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object v11

    const/4 v12, 0x0

    const/16 v13, 0x28

    move-object v5, v2

    move-object v6, v4

    invoke-static/range {v5 .. v13}, Lcom/discord/player/AppMediaPlayer;->b(Lcom/discord/player/AppMediaPlayer;Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;I)V

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/discord/player/AppMediaPlayer;->d(F)V

    goto :goto_2

    :cond_6
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v2, v1}, Lcom/discord/player/AppMediaPlayer;->d(F)V

    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->getPlayButton()Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;

    invoke-direct {v2, v0, v4}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/MediaSource;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    return-void
.end method

.method public static synthetic updateUIWithValidatedEmbed$default(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    move-object p3, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getUnsubscribeSignal()Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/Subject<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->unsubscribeSignal:Lrx/subjects/Subject;

    return-object v0
.end method

.method public final onPause()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->clearPlayerAndSubscriptions()V

    return-void
.end method

.method public final onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->resetCurrentEmbed()V

    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->resetCurrentEmbed()V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->clearPlayerAndSubscriptions()V

    return-void
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    const-string v0, "changedView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onVisibilityChanged(Landroid/view/View;I)V

    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->resetCurrentEmbed()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/InlineMediaView;->clearPlayerAndSubscriptions()V

    :goto_0
    return-void
.end method

.method public final updateUIWithAttachment(Lcom/discord/models/domain/ModelMessageAttachment;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 4

    const-string v0, "attachment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getType()Lcom/discord/models/domain/ModelMessageAttachment$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/ModelMessageAttachment$Type;->IMAGE:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageAttachment;->getType()Lcom/discord/models/domain/ModelMessageAttachment$Type;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/ModelMessageAttachment$Type;->VIDEO:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getShowAttachmentMediaInline()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-nez v2, :cond_3

    return-void

    :cond_3
    sget-object v0, Lcom/discord/utilities/embed/EmbedResourceUtils;->INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/embed/EmbedResourceUtils;->createAttachmentEmbed(Lcom/discord/models/domain/ModelMessageAttachment;)Lcom/discord/models/domain/ModelMessageEmbed;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": attachment"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->featureTag:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method

.method public final updateUIWithEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 3

    const-string v0, "embed"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getVideo()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getInlineEmbedMedia()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    if-nez v1, :cond_3

    return-void

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": embed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView;->featureTag:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method
