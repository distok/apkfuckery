.class public final Lcom/discord/widgets/chat/list/PublishActionDialog;
.super Lcom/discord/app/AppDialog;
.source "PublishActionDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_THEME_ID:Ljava/lang/String; = "theme_id"

.field public static final Companion:Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;


# instance fields
.field private final body$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final header$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final headerContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final ok$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onSuccess:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const-string v3, "headerContainer"

    const-string v4, "getHeaderContainer()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const-string v6, "header"

    const-string v7, "getHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const-string v6, "body"

    const-string v7, "getBody()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const-string v6, "ok"

    const-string v7, "getOk()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const-string v6, "cancel"

    const-string v7, "getCancel()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/list/PublishActionDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/list/PublishActionDialog;->Companion:Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a06fa

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->headerContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06f9

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06f6

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->body$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06fc

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->ok$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06f7

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getOnSuccess$p(Lcom/discord/widgets/chat/list/PublishActionDialog;)Lkotlin/jvm/functions/Function0;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->onSuccess:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/chat/list/PublishActionDialog;)Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->viewModel:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/chat/list/PublishActionDialog;Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/PublishActionDialog;->handleEvent(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setOnSuccess$p(Lcom/discord/widgets/chat/list/PublishActionDialog;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->onSuccess:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/chat/list/PublishActionDialog;Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->viewModel:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/discord/widgets/chat/list/PublishActionDialog;Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/PublishActionDialog;->updateView(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;)V

    return-void
.end method

.method private final getBody()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->body$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/PublishActionDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCancel()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/PublishActionDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/PublishActionDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getHeaderContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->headerContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/PublishActionDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getOk()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->ok$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/PublishActionDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event;)V
    .locals 3

    instance-of v0, p1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event$Success;

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const p1, 0x7f12103f

    invoke-static {p0, p1, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    goto :goto_0

    :cond_0
    instance-of p1, p1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event$Failure;

    if-eqz p1, :cond_1

    const p1, 0x7f1214ad

    invoke-static {p0, p1, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void
.end method

.method private final renderHasFollowers(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;)V
    .locals 9

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getBody()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;->getFollowerStats()Lcom/discord/models/domain/ModelChannelFollowerStats;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannelFollowerStats;->getGuildsFollowing()Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const p1, 0x7f1214a9

    invoke-virtual {p0, p1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string p1, "getString(R.string.publi\u2026ildsFollowing.toString())"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3c

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final renderLoading()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getBody()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f120f2b

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final renderNoFollowers()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getBody()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1214a8

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final show(Landroidx/fragment/app/FragmentManager;JJLkotlin/jvm/functions/Function0;Ljava/lang/Integer;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "JJ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/chat/list/PublishActionDialog;->Companion:Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/discord/widgets/chat/list/PublishActionDialog$Companion;->show(Landroidx/fragment/app/FragmentManager;JJLkotlin/jvm/functions/Function0;Ljava/lang/Integer;)V

    return-void
.end method

.method private final updateView(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$Loading;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->renderLoading()V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedNoFollowers;->INSTANCE:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedNoFollowers;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->renderNoFollowers()V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/PublishActionDialog;->renderHasFollowers(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;)V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d00db

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "com.discord.intent.extra.EXTRA_MESSAGE_ID"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v2, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;-><init>(JJ)V

    invoke-direct {p1, p0, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026logViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->viewModel:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    if-eqz v2, :cond_0

    const v4, 0x7f040206

    const-string/jumbo v5, "theme_id"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    :cond_0
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p0, v3, v0}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    :cond_1
    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getHeader()Landroid/widget/TextView;

    move-result-object p1

    const v0, 0x7f1210e3

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getHeaderContainer()Landroid/view/ViewGroup;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getBody()Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextAlignment(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getBody()Landroid/widget/TextView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/notice/WidgetNoticeDialog$ActionLinkMovementMethod;

    new-instance v1, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$1;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialog;)V

    invoke-direct {v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$ActionLinkMovementMethod;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getOk()Landroid/widget/Button;

    move-result-object p1

    const v0, 0x7f1210e2

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getOk()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$2;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getCancel()Landroid/widget/Button;

    move-result-object p1

    const v0, 0x7f1203f1

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialog;->getCancel()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$3;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppDialog;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->viewModel:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialog;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialog;->viewModel:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/list/PublishActionDialog;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialog;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
