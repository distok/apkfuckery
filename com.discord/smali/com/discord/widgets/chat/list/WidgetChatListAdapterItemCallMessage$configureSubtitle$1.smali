.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;
.super Lx/m/c/k;
.source "WidgetChatListAdapterItemCallMessage.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->configureSubtitle(Lcom/discord/widgets/chat/list/entries/MessageEntry;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/res/Resources;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $callJoinedTimestampMs:J

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    iput-wide p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->$callJoinedTimestampMs:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->invoke(Ljava/lang/Long;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Long;)V
    .locals 8

    sget-object v0, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->$callJoinedTimestampMs:J

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->access$getClock$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v3

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyString$default(Lcom/discord/utilities/time/TimeUtils;JJLjava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->access$getMinWidthPxForTime(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->access$getSubtitleText$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->access$getUnjoinedCallDuration$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->access$getUnjoinedCallDuration$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->access$getUnjoinedCallDuration$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingStart()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    return-void
.end method
