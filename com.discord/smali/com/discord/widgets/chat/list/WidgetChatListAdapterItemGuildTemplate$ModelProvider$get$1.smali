.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemGuildTemplate.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider;->get(Lcom/discord/widgets/chat/list/entries/GuildTemplateEntry;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider$get$1;->call(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model;",
            ">;"
        }
    .end annotation

    instance-of v0, p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;

    if-eqz v0, :cond_1

    :goto_0
    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Loading;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Loading;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    instance-of v0, p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;

    check-cast p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;->getGuildTemplate()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;-><init>(Lcom/discord/models/domain/ModelGuildTemplate;)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object v0, p1

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Invalid;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Invalid;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_1
    return-object v0
.end method
