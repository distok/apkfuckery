.class public final Lcom/discord/widgets/chat/list/ViewReplySpline;
.super Landroid/view/View;
.source "ViewReplySpline.kt"


# instance fields
.field private final arcPercent:F

.field private final arcRect:Landroid/graphics/RectF;

.field private arcRectLocal:Landroid/graphics/RectF;

.field private final arcRectX:F

.field private final arcRectY:F

.field private final endX:F

.field private final endY:F

.field private final halfStrokeWidth:F

.field private final paint:Landroid/graphics/Paint;

.field private final path:Landroid/graphics/Path;

.field private final pathRect:Landroid/graphics/RectF;

.field private pathRectLocal:Landroid/graphics/RectF;

.field private spineEndPadding:I

.field private spineStartPadding:I

.field private final startX:F

.field private final startY:F

.field private final strokeWidth:F

.field private transformMatrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->paint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->path:Landroid/graphics/Path;

    const/high16 v1, 0x40400000    # 3.0f

    iput v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->halfStrokeWidth:F

    const/4 v2, 0x2

    int-to-float v2, v2

    mul-float v1, v1, v2

    iput v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->strokeWidth:F

    const/high16 v3, 0x3e800000    # 0.25f

    iput v3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcPercent:F

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->startY:F

    iput v4, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->endX:F

    iget v5, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->startX:F

    sub-float v5, v4, v5

    mul-float v5, v5, v3

    mul-float v5, v5, v2

    iput v5, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRectX:F

    iget v6, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->endY:F

    sub-float v6, v4, v6

    mul-float v6, v6, v3

    mul-float v6, v6, v2

    iput v6, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRectY:F

    new-instance v2, Landroid/graphics/RectF;

    iget v3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->startX:F

    iget v7, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->endY:F

    invoke-direct {v2, v3, v7, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->pathRect:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    iget v3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->startX:F

    iget v4, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->endY:F

    add-float/2addr v5, v3

    add-float/2addr v6, v4

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRect:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->pathRectLocal:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRectLocal:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->transformMatrix:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const v1, 0x7f04012c

    invoke-static {p0, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    if-eqz p2, :cond_2

    sget-object v0, Lcom/discord/R$a;->ViewReplySpline:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026* defStyleRes */0\n      )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    int-to-float v1, v1

    cmpl-float v2, p2, v1

    if-lez v2, :cond_0

    invoke-static {p2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(F)I

    move-result p2

    iput p2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->spineStartPadding:I

    :cond_0
    cmpl-float p2, v0, v1

    if-lez p2, :cond_1

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(F)I

    move-result p2

    iput p2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->spineEndPadding:I

    :cond_1
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_2
    return-void
.end method


# virtual methods
.method public final createPath()V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->pathRectLocal:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRectLocal:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000    # 180.0f

    const/high16 v3, 0x42b40000    # 90.0f

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->pathRectLocal:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    :cond_0
    iget-object p3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->transformMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p3}, Landroid/graphics/Matrix;->reset()V

    iget-object p3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->transformMatrix:Landroid/graphics/Matrix;

    iget p4, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->halfStrokeWidth:F

    invoke-virtual {p3, p4, p4}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object p3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->transformMatrix:Landroid/graphics/Matrix;

    int-to-float p1, p1

    iget p4, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->strokeWidth:F

    sub-float/2addr p1, p4

    iget v0, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->spineEndPadding:I

    int-to-float v0, v0

    sub-float/2addr p1, v0

    int-to-float p2, p2

    sub-float/2addr p2, p4

    iget p4, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->spineStartPadding:I

    int-to-float p4, p4

    sub-float/2addr p2, p4

    invoke-virtual {p3, p1, p2}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object p1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->transformMatrix:Landroid/graphics/Matrix;

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->pathRectLocal:Landroid/graphics/RectF;

    iget-object p3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->pathRect:Landroid/graphics/RectF;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    iget-object p1, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->transformMatrix:Landroid/graphics/Matrix;

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRectLocal:Landroid/graphics/RectF;

    iget-object p3, p0, Lcom/discord/widgets/chat/list/ViewReplySpline;->arcRect:Landroid/graphics/RectF;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/ViewReplySpline;->createPath()V

    :cond_1
    return-void
.end method
