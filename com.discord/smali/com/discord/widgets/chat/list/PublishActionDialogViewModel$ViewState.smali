.class public abstract Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;
.super Ljava/lang/Object;
.source "PublishActionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$Loading;,
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedNoFollowers;,
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;-><init>()V

    return-void
.end method
