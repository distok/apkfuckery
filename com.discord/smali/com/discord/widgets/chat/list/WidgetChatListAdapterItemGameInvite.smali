.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemGameInvite.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final gameInviteView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private subscription:Lrx/Subscription;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;

    const-string v3, "gameInviteView"

    const-string v4, "getGameInviteView()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01be

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const p1, 0x7f0a05ea

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->gameInviteView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    return-object p0
.end method

.method public static final synthetic access$getGameInviteView$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->getGameInviteView()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->subscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->subscription:Lrx/Subscription;

    return-void
.end method

.method private final getGameInviteView()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->gameInviteView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    return-object v0
.end method


# virtual methods
.method public getSubscription()Lrx/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->subscription:Lrx/Subscription;

    return-object v0
.end method

.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 9

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    check-cast p2, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->getGameInviteView()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;Lcom/discord/widgets/chat/list/entries/GameInviteEntry;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->setOnActionButtonClick(Lkotlin/jvm/functions/Function2;)V

    sget-object p1, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->Companion:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->get(Landroid/content/Context;Lcom/discord/widgets/chat/list/entries/GameInviteEntry;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    new-instance v4, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$2;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)V

    const-class p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string p1, "javaClass.simpleName"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$3;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite$onConfigure$3;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;)V

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x31

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method
