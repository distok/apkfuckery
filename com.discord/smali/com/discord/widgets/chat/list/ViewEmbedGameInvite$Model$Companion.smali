.class public final Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;
.super Ljava/lang/Object;
.source "ViewEmbedGameInvite.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$create(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;Lcom/discord/widgets/chat/list/entries/GameInviteEntry;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->create(Lcom/discord/widgets/chat/list/entries/GameInviteEntry;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createForShare(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->createForShare(Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object p0

    return-object p0
.end method

.method private final create(Lcom/discord/widgets/chat/list/entries/GameInviteEntry;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/list/entries/GameInviteEntry;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelApplication;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Landroid/content/pm/PackageInfo;",
            ">;)",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;"
        }
    .end annotation

    move-object v0, p0

    new-instance v12, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getAuthorId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getMessageId()J

    move-result-wide v1

    const/16 v5, 0x16

    ushr-long/2addr v1, v5

    const-wide v5, 0x14aa2cab000L

    add-long/2addr v5, v1

    if-eqz p3, :cond_0

    move-object/from16 v7, p3

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    move-object v7, v1

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v8

    move-object/from16 v9, p4

    move-object/from16 v1, p5

    invoke-direct {p0, v1, v9}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->createPartyUsers(Ljava/util/Map;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    move-object/from16 v2, p6

    invoke-direct {p0, v1, v2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->isGameInstalled(Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Z

    move-result v11

    move-object v1, v12

    move-object v2, p2

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;-><init>(Lcom/discord/models/domain/ModelUser;JJLcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/List;Z)V

    return-object v12
.end method

.method private final createForShare(Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "J",
            "Lcom/discord/models/domain/ModelMessage$Activity;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Lcom/discord/models/domain/ModelApplication;",
            "Ljava/util/List<",
            "+",
            "Landroid/content/pm/PackageInfo;",
            ">;)",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v7, p7

    if-eqz v7, :cond_0

    new-instance v12, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    move-object/from16 v9, p5

    move-object/from16 v1, p6

    invoke-direct {p0, v1, v9}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->createPartyUsers(Ljava/util/Map;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/util/ArrayList;

    move-result-object v10

    move-object/from16 v1, p8

    invoke-direct {p0, v7, v1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->isGameInstalled(Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Z

    move-result v11

    move-object v1, v12

    move-object v2, p1

    move-wide v5, p2

    move-object/from16 v7, p7

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;-><init>(Lcom/discord/models/domain/ModelUser;JJLcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/List;Z)V

    return-object v12

    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private final createPartyUsers(Ljava/util/Map;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/discord/widgets/channels/list/items/CollapsedUser;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/discord/models/domain/ModelUser;

    new-instance v2, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    move-object v3, v2

    invoke-direct/range {v3 .. v9}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivityParty;->getMaxSize()J

    move-result-wide v3

    goto :goto_1

    :cond_1
    move-wide v3, v1

    :goto_1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    const-wide/16 v5, 0x4

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    invoke-static {p1, v7, v8}, Lx/p/e;->until(IJ)Lkotlin/ranges/LongRange;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    move-object p2, p1

    check-cast p2, Lx/h/p;

    invoke-virtual {p2}, Lx/h/p;->nextLong()J

    move-result-wide v7

    const-wide/16 v9, 0x3

    cmp-long p2, v7, v9

    if-nez p2, :cond_2

    sub-long v7, v3, v5

    goto :goto_3

    :cond_2
    move-wide v7, v1

    :goto_3
    sget-object p2, Lcom/discord/widgets/channels/list/items/CollapsedUser;->Companion:Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;

    invoke-virtual {p2, v7, v8}, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;->createEmptyUser(J)Lcom/discord/widgets/channels/list/items/CollapsedUser;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    return-object v0
.end method

.method private final isGameInstalled(Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelApplication;",
            "Ljava/util/List<",
            "+",
            "Landroid/content/pm/PackageInfo;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplication;->getGPlayPackageNames()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_4

    instance-of v0, p2, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :cond_3
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_1

    :cond_4
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_1
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :cond_5
    return v1
.end method


# virtual methods
.method public final get(Landroid/content/Context;Lcom/discord/widgets/chat/list/entries/GameInviteEntry;)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/discord/widgets/chat/list/entries/GameInviteEntry;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, p2}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplication()Lcom/discord/stores/StoreApplication;

    move-result-object v3

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/discord/stores/StoreApplication;->get(Ljava/lang/Long;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v4

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getAuthorId()J

    move-result-wide v5

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v7

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/discord/stores/StoreUserPresence;->observeApplicationActivity(JJ)Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGameParty()Lcom/discord/stores/StoreGameParty;

    move-result-object v5

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/GameInviteEntry;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelMessage$Activity;->getPartyId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Lcom/discord/stores/StoreGameParty;->getUsersForPartyId(Ljava/lang/String;)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAndroidPackages()Lcom/discord/stores/StoreAndroidPackages;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreAndroidPackages;->getInstalledPackages(Landroid/content/Context;)Lrx/Observable;

    move-result-object v6

    new-instance p1, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$get$1;

    sget-object p2, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->Companion:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;

    invoke-direct {p1, p2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$get$1;-><init>(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;)V

    new-instance v7, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$sam$rx_functions_Func6$0;

    invoke-direct {v7, p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$sam$rx_functions_Func6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    invoke-static/range {v1 .. v7}, Lrx/Observable;->f(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable\n            .\u2026ion::create\n            )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable\n            .\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getForShare(Landroid/content/Context;Lcom/discord/utilities/time/Clock;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)Lrx/Observable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/discord/utilities/time/Clock;",
            "Landroid/net/Uri;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Observable.just(null)"

    const/4 v1, 0x0

    if-eqz p3, :cond_4

    const-string v2, "application_id"

    invoke-virtual {p3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lx/s/l;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    const-string v3, "party_id"

    invoke-virtual {p3, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "type"

    invoke-virtual {p3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v4}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v1

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/send/activity"

    invoke-static {v5, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_2

    goto/16 :goto_2

    :cond_2
    new-instance p3, Lcom/discord/models/domain/ModelMessage$Activity;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p3, v0, v3}, Lcom/discord/models/domain/ModelMessage$Activity;-><init>(ILjava/lang/String;)V

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p4}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    sget-object p4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v1

    new-instance v4, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;

    invoke-direct {v4, v2, v3}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/Observable;->m(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v6

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v3

    invoke-interface {p2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    new-instance v4, Lg0/l/e/j;

    invoke-direct {v4, p2}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, p3}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getGameParty()Lcom/discord/stores/StoreGameParty;

    move-result-object p2

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelMessage$Activity;->getPartyId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/discord/stores/StoreGameParty;->getUsersForPartyId(Ljava/lang/String;)Lrx/Observable;

    move-result-object v7

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getApplication()Lcom/discord/stores/StoreApplication;

    move-result-object p2

    invoke-virtual {p2, v2}, Lcom/discord/stores/StoreApplication;->get(Ljava/lang/Long;)Lrx/Observable;

    move-result-object v8

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getAndroidPackages()Lcom/discord/stores/StoreAndroidPackages;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreAndroidPackages;->getInstalledPackages(Landroid/content/Context;)Lrx/Observable;

    move-result-object v9

    new-instance p1, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$1;

    sget-object p2, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->Companion:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;

    invoke-direct {p1, p2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$1;-><init>(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;)V

    new-instance v10, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$sam$rx_functions_Func7$0;

    invoke-direct {v10, p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$sam$rx_functions_Func7$0;-><init>(Lkotlin/jvm/functions/Function7;)V

    invoke-static/range {v3 .. v10}, Lrx/Observable;->e(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable\n            .\u2026ateForShare\n            )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable\n            .\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_3
    :goto_2
    sget-object p1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Malformed Share URI: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    invoke-static {p1, p2, v1, p3, v1}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_4
    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
