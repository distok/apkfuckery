.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemSticker.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;->$data:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->access$getAdapter$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;->$data:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    check-cast v0, Lcom/discord/widgets/chat/list/entries/StickerEntry;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/entries/StickerEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;->$data:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    check-cast v1, Lcom/discord/widgets/chat/list/entries/StickerEntry;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/StickerEntry;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V

    return-void
.end method
