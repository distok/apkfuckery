.class public final Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;
.super Ljava/lang/Object;
.source "PublishActionDialogViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final channelId:J

.field private final messageId:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->messageId:J

    iput-wide p3, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->channelId:J

    return-void
.end method

.method private final observeChannelFollowerStatsStoreState(JLcom/discord/stores/StoreChannelFollowerStats;)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/stores/StoreChannelFollowerStats;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p3, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats;->observeChannelFollowerStats(J)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory$observeChannelFollowerStatsStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory$observeChannelFollowerStatsStoreState$1;

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string/jumbo p2, "storeChannelFollowerStat\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getChannelFollowerStats()Lcom/discord/stores/StoreChannelFollowerStats;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->channelId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreChannelFollowerStats;->fetchChannelFollowerStats(J)V

    new-instance v0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    iget-wide v3, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->messageId:J

    iget-wide v5, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->channelId:J

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v7

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->channelId:J

    invoke-direct {p0, v1, v2, p1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->observeChannelFollowerStatsStoreState(JLcom/discord/stores/StoreChannelFollowerStats;)Lrx/Observable;

    move-result-object v8

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;-><init>(JJLcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->channelId:J

    return-wide v0
.end method

.method public final getMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;->messageId:J

    return-wide v0
.end method
