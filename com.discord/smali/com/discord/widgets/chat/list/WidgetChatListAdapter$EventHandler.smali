.class public interface abstract Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;
.super Ljava/lang/Object;
.source "WidgetChatListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EventHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler$DefaultImpls;
    }
.end annotation


# virtual methods
.method public abstract onCallMessageClicked(JLcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;)V
.end method

.method public abstract onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V
.end method

.method public abstract onListClicked()V
.end method

.method public abstract onMessageAuthorAvatarClicked(Lcom/discord/models/domain/ModelMessage;J)V
.end method

.method public abstract onMessageAuthorLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/Long;)V
.end method

.method public abstract onMessageAuthorNameClicked(Lcom/discord/models/domain/ModelMessage;J)V
.end method

.method public abstract onMessageBlockedGroupClicked(Lcom/discord/models/domain/ModelMessage;)V
.end method

.method public abstract onMessageClicked(Lcom/discord/models/domain/ModelMessage;)V
.end method

.method public abstract onMessageLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;)V
.end method

.method public abstract onOldestMessageId(JJ)V
.end method

.method public abstract onOpenPinsClicked(Lcom/discord/models/domain/ModelMessage;)V
.end method

.method public abstract onQuickAddReactionClicked(JJJJZ)V
.end method

.method public abstract onQuickDownloadClicked(Landroid/net/Uri;Ljava/lang/String;)Z
.end method

.method public abstract onReactionClicked(JJJJZLcom/discord/models/domain/ModelMessageReaction;)V
.end method

.method public abstract onReactionLongClicked(JJJZLcom/discord/models/domain/ModelMessageReaction;)V
.end method

.method public abstract onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V
.end method

.method public abstract onUrlLongClicked(Ljava/lang/String;)V
.end method

.method public abstract onUserActivityAction(JJJILcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelApplication;)V
.end method

.method public abstract onUserMentionClicked(JJJ)V
.end method
