.class public final Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;
.super Ljava/lang/Object;
.source "MessageEntry.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/entries/MessageEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReplyData"
.end annotation


# instance fields
.field private final isRepliedUserBlocked:Z

.field private final messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

.field private final messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)V
    .locals 1

    const-string v0, "messageState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    iput-boolean p3, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;-><init>(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;ZILjava/lang/Object;)Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->copy(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreMessageReplies$MessageState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/chat/list/entries/MessageEntry;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    return v0
.end method

.method public final copy(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;
    .locals 1

    const-string v0, "messageState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;-><init>(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    iget-object v1, p1, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    iget-object v1, p1, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    iget-boolean p1, p1, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessageEntry()Lcom/discord/widgets/chat/list/entries/MessageEntry;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    return-object v0
.end method

.method public final getMessageState()Lcom/discord/stores/StoreMessageReplies$MessageState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isRepliedUserBlocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ReplyData(messageState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageState:Lcom/discord/stores/StoreMessageReplies$MessageState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", messageEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isRepliedUserBlocked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
