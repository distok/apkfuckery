.class public final Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;
.super Ljava/lang/Object;
.source "ListenTogetherEntry.kt"

# interfaces
.implements Lcom/discord/widgets/chat/list/entries/ChatListEntry;


# instance fields
.field private final activity:Lcom/discord/models/domain/ModelMessage$Activity;

.field private final messageId:J

.field private final userId:J


# direct methods
.method public constructor <init>(JJLcom/discord/models/domain/ModelMessage$Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    iput-wide p3, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    iput-object p5, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;JJLcom/discord/models/domain/ModelMessage$Activity;ILjava/lang/Object;)Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget-wide p3, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    :cond_1
    move-wide v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget-object p5, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    :cond_2
    move-object v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->copy(JJLcom/discord/models/domain/ModelMessage$Activity;)Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    return-wide v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelMessage$Activity;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    return-object v0
.end method

.method public final copy(JJLcom/discord/models/domain/ModelMessage$Activity;)Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;
    .locals 7

    const-string v0, "activity"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;

    move-object v1, v0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;-><init>(JJLcom/discord/models/domain/ModelMessage$Activity;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    iget-wide v2, p1, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    iget-wide v2, p1, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    iget-object p1, p1, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActivity()Lcom/discord/models/domain/ModelMessage$Activity;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 3

    const-string v0, "23 -- "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public final getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage$Activity;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public isInExpandedBlockedMessageChunk()Z
    .locals 1

    invoke-static {p0}, Lcom/discord/widgets/chat/list/entries/ChatListEntry$DefaultImpls;->isInExpandedBlockedMessageChunk(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ListenTogetherEntry(userId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->userId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", messageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->messageId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->activity:Lcom/discord/models/domain/ModelMessage$Activity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
