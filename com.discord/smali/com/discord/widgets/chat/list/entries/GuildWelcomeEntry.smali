.class public final Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;
.super Ljava/lang/Object;
.source "GuildWelcomeEntry.kt"

# interfaces
.implements Lcom/discord/widgets/chat/list/entries/ChatListEntry;


# instance fields
.field private final canInvite:Z

.field private final guildHasIcon:Z

.field private final guildId:J

.field private final guildName:Ljava/lang/String;

.field private final isOwner:Z

.field private final key:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZZZJLjava/lang/String;)V
    .locals 1

    const-string v0, "guildName"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    iput-boolean p2, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    iput-boolean p3, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    iput-wide p4, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    iput-object p6, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->getType()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->key:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;ZZZJLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-wide p4, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    :cond_3
    move-wide v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p6, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    :cond_4
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p8

    move p5, v0

    move-wide p6, v1

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->copy(ZZZJLjava/lang/String;)Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    return v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    return-wide v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZZZJLjava/lang/String;)Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;
    .locals 8

    const-string v0, "guildName"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move-wide v5, p4

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;-><init>(ZZZJLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanInvite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    return v0
.end method

.method public final getGuildHasIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    return v0
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    return-wide v0
.end method

.method public final getGuildName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    const/16 v0, 0x19

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public isInExpandedBlockedMessageChunk()Z
    .locals 1

    invoke-static {p0}, Lcom/discord/widgets/chat/list/entries/ChatListEntry$DefaultImpls;->isInExpandedBlockedMessageChunk(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Z

    move-result v0

    return v0
.end method

.method public final isOwner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GuildWelcomeEntry(isOwner="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->isOwner:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", guildHasIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildHasIcon:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", canInvite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->canInvite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;->guildName:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
