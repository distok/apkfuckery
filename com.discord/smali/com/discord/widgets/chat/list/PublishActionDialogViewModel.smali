.class public final Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;
.super Lf/a/b/l0;
.source "PublishActionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;,
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;,
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event;,
        Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final channelId:J

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final messageId:J

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;


# direct methods
.method public constructor <init>(JJLcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "restAPI"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeObservable"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-wide p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->messageId:J

    iput-wide p3, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->channelId:J

    iput-object p5, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    new-instance v6, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$1;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$emitFailureEvent(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->emitFailureEvent()V

    return-void
.end method

.method public static final synthetic access$emitSuccessActionEvent(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->emitSuccessActionEvent()V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->handleStoreState(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;)V

    return-void
.end method

.method private final emitFailureEvent()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event$Failure;

    const v2, 0x7f1205df

    invoke-direct {v1, v2}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event$Failure;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitSuccessActionEvent()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event$Success;

    const v2, 0x7f12103f

    invoke-direct {v1, v2}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event$Success;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;)V
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$StoreState;->getFollowerStats()Lcom/discord/models/domain/ModelChannelFollowerStats;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannelFollowerStats;->getGuildsFollowing()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannelFollowerStats;->getGuildsFollowing()Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    if-lez v0, :cond_2

    new-instance v0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedHasFollowers;-><init>(Lcom/discord/models/domain/ModelChannelFollowerStats;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    sget-object p1, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedNoFollowers;->INSTANCE:Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$ViewState$LoadedNoFollowers;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final publishMessage()V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->channelId:J

    iget-wide v3, p0, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->messageId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/utilities/rest/RestAPI;->crosspostMessage(JLjava/lang/Long;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    const-string v1, "restAPI\n        .crosspo\u2026ormers.restSubscribeOn())"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    new-instance v9, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$publishMessage$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$publishMessage$1;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;)V

    new-instance v7, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$publishMessage$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel$publishMessage$2;-><init>(Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
