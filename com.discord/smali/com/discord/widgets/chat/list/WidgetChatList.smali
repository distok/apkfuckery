.class public Lcom/discord/widgets/chat/list/WidgetChatList;
.super Lcom/discord/app/AppFragment;
.source "WidgetChatList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;
    }
.end annotation


# static fields
.field public static final synthetic d:I


# instance fields
.field private _flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

.field private adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

.field private chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private final userReactionHandler:Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->userReactionHandler:Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->_flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    return-void
.end method

.method public static synthetic access$000(Lcom/discord/widgets/chat/list/WidgetChatList;)Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->userReactionHandler:Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;

    return-object p0
.end method

.method public static synthetic access$100(Lcom/discord/widgets/chat/list/WidgetChatList;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatList;->getFlexInputViewModel()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    move-result-object p0

    return-object p0
.end method

.method private configureUI(Lcom/discord/widgets/chat/list/model/WidgetChatListModel;)V
    .locals 2
    .param p1    # Lcom/discord/widgets/chat/list/model/WidgetChatListModel;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->setData(Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;)V

    :cond_2
    return-void
.end method

.method private createAdapter(Lcom/discord/stores/StoreChat;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
    .locals 3

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Lcom/discord/widgets/chat/list/WidgetChatList$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatList$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList;Lcom/discord/stores/StoreChat;)V

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-direct {v0, v1, p0, v2, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissions$Requests;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;Lcom/discord/utilities/time/Clock;)V

    return-object v0
.end method

.method public static synthetic f(Lcom/discord/widgets/chat/list/WidgetChatList;Lcom/discord/widgets/chat/list/model/WidgetChatListModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatList;->configureUI(Lcom/discord/widgets/chat/list/model/WidgetChatListModel;)V

    return-void
.end method

.method public static synthetic g(Lcom/discord/widgets/chat/list/WidgetChatList;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatList;->scrollTo(J)V

    return-void
.end method

.method private getFlexInputViewModel()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->_flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-nez v0, :cond_0

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;

    invoke-direct {v2}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;-><init>()V

    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->_flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->_flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    return-object v0
.end method

.method private scrollTo(J)V
    .locals 2

    sget-object v0, Lf/a/o/b/b/p;->d:Lf/a/o/b/b/p;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1, p2, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->scrollToMessageId(JLrx/functions/Action0;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lf/a/o/b/b/p;->call()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01b4

    return v0
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->dispose()V

    :cond_1
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->onPause()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->disposeHandlers()V

    :cond_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a0282

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/list/WidgetChatList;->createAdapter(Lcom/discord/stores/StoreChat;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->chatListRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->setRecycler(Landroidx/recyclerview/widget/RecyclerView;)V

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-static {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setReverseLayout(Z)V

    :cond_1
    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 3

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->setHandlers()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->onResume()V

    :cond_0
    invoke-static {}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->get()Lrx/Observable;

    move-result-object v0

    const-string v1, "observable"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    const-string v2, "appComponent"

    invoke-static {p0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "it"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/b/b/h;

    invoke-direct {v1, p0}, Lf/a/o/b/b/h;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesLoader;->getScrollTo()Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/b/b/o;

    invoke-direct {v1, p0}, Lf/a/o/b/b/o;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
