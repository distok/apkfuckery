.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$observeState$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemCallMessage.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->observeState(Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;",
        "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/entries/MessageEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$observeState$1;->$messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;)",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;

    const-string/jumbo v1, "voiceParticipants"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$observeState$1;->$messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-direct {v0, p1, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;-><init>(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$observeState$1;->call(Ljava/util/Map;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;

    move-result-object p1

    return-object p1
.end method
