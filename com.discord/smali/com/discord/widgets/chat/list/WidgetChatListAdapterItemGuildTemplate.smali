.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemGuildTemplate.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private item:Lcom/discord/widgets/chat/list/entries/GuildTemplateEntry;

.field private final openButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private subscription:Lrx/Subscription;

.field private final templateNameText$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;

    const/4 v1, 0x3

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string/jumbo v3, "templateNameText"

    const-string v4, "getTemplateNameText()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/s;

    const-string/jumbo v6, "subText"

    const-string v7, "getSubText()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Lx/m/c/s;

    const-string v6, "openButton"

    const-string v7, "getOpenButton()Landroid/widget/Button;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01c0

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const p1, 0x7f0a0603

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->templateNameText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0604

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->subText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0602

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->openButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureInvalidUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->configureInvalidUI()V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model;)V

    return-void
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->subscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$openGuildTemplate(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;Landroid/content/Context;Lcom/discord/models/domain/ModelGuildTemplate;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->openGuildTemplate(Landroid/content/Context;Lcom/discord/models/domain/ModelGuildTemplate;)V

    return-void
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->subscription:Lrx/Subscription;

    return-void
.end method

.method private final configureInvalidUI()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getTemplateNameText()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f120c41

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getSubText()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f120c40

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getSubText()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getOpenButton()Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureLoadingUI()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getTemplateNameText()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f120c89

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getSubText()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getOpenButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureResolvedUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;)V
    .locals 6

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;->component1()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getTemplateNameText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildTemplate;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getSubText()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildTemplate;->getUsageCount()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildTemplate;->getUsageCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const v4, 0x7f10009b

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getSubText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getOpenButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->getOpenButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$configureResolvedUI$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$configureResolvedUI$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;Lcom/discord/models/domain/ModelGuildTemplate;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Loading;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->configureLoadingUI()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->configureResolvedUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Resolved;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$Model$Invalid;

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->configureInvalidUI()V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getOpenButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->openButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getSubText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->subText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTemplateNameText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->templateNameText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final openGuildTemplate(Landroid/content/Context;Lcom/discord/models/domain/ModelGuildTemplate;)V
    .locals 2

    sget-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->Companion:Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildTemplate;->getCode()Ljava/lang/String;

    move-result-object p2

    const-string v1, "Guild Template Embed"

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getSubscription()Lrx/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->subscription:Lrx/Subscription;

    return-object v0
.end method

.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 9

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    check-cast p2, Lcom/discord/widgets/chat/list/entries/GuildTemplateEntry;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->item:Lcom/discord/widgets/chat/list/entries/GuildTemplateEntry;

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider;

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$ModelProvider;->get(Lcom/discord/widgets/chat/list/entries/GuildTemplateEntry;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;

    const/4 v2, 0x0

    new-instance v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$onConfigure$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$onConfigure$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;)V

    new-instance v4, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$onConfigure$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$onConfigure$2;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;)V

    const/4 v5, 0x0

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$onConfigure$3;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate$onConfigure$3;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;)V

    const/16 v7, 0x12

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string p1, "item"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method
