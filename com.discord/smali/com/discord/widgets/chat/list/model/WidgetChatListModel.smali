.class public Lcom/discord/widgets/chat/list/model/WidgetChatListModel;
.super Ljava/lang/Object;
.source "WidgetChatListModel.java"

# interfaces
.implements Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;,
        Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;
    }
.end annotation


# static fields
.field private static final MESSAGE_CONCAT_TIMESTAMP_DELTA_THRESHOLD:J = 0x668a0L


# instance fields
.field private final channelId:J

.field private final channelNames:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final guild:Lcom/discord/models/domain/ModelGuild;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final guildId:J

.field private final isLoadingMessages:Z

.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final myRoleIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final newMessagesMarkerMessageId:J

.field public final newestKnownMessageId:J

.field private final oldestMessageId:J

.field private final shouldShowGuildGate:Z

.field private final userId:J


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;ZLjava/util/Map;JLjava/util/Map;Lcom/discord/models/domain/ModelGuild;Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;)V
    .locals 2
    .param p1    # Lcom/discord/models/domain/ModelChannel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p8    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p9    # Lcom/discord/models/domain/ModelGuild;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p6, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->userId:J

    iput-object p5, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->channelNames:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->channelId:J

    iput-object p9, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->guildId:J

    iput-boolean p4, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages:Z

    invoke-static {p3}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;->access$000(Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;)J

    move-result-wide p4

    iput-wide p4, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->oldestMessageId:J

    invoke-static {p3}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;->access$100(Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;)J

    move-result-wide p4

    iput-wide p4, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->newestKnownMessageId:J

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    invoke-interface {p8, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    if-eqz p4, :cond_0

    new-instance p4, Ljava/util/HashSet;

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    invoke-interface {p8, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-virtual {p5}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object p5

    invoke-direct {p4, p5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p4

    :goto_0
    iput-object p4, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->myRoleIds:Ljava/util/Set;

    invoke-static {p3}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;->access$200(Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;)J

    move-result-wide p4

    iput-wide p4, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->newMessagesMarkerMessageId:J

    invoke-direct {p0, p2, p3, p10}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->computeList(Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->list:Ljava/util/List;

    sget-object p3, Lcom/discord/utilities/guilds/GuildGatingUtils;->INSTANCE:Lcom/discord/utilities/guilds/GuildGatingUtils;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p4

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p6

    invoke-static {}, Lcom/discord/stores/StoreStream;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object p7

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p8

    invoke-virtual/range {p3 .. p8}, Lcom/discord/utilities/guilds/GuildGatingUtils;->shouldShowGuildGate(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->shouldShowGuildGate:Z

    return-void
.end method

.method public static a(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 10

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;->get(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;->get(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object v2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v3

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/discord/stores/StoreMessagesLoader;->getMessagesLoadedState(J)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lf/a/o/b/b/e0/j;->d:Lf/a/o/b/b/e0/j;

    invoke-virtual {v3, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v3

    invoke-static {}, Lcom/discord/stores/StoreStream;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreChannels;->observeNames()Lrx/Observable;

    move-result-object v4

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v5

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v6

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v6

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v7

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v7

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChatListState(J)Lrx/Observable;

    move-result-object v8

    sget-object v9, Lf/a/o/b/b/e0/a;->a:Lf/a/o/b/b/e0/a;

    invoke-static/range {v0 .. v9}, Lrx/Observable;->c(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic access$400(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 0

    invoke-static {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->canAddReactions(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private static canAddReactions(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isSystemDM()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    return-object v0

    :cond_1
    invoke-static {}, Lcom/discord/stores/StoreStream;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object p0

    sget-object v0, Lf/a/o/b/b/e0/b;->d:Lf/a/o/b/b/e0/b;

    invoke-virtual {p0, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private computeList(Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;)Ljava/util/List;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;->access$300(Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v1, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->DETACHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    if-ne p3, v1, :cond_0

    new-instance p3, Lcom/discord/widgets/chat/list/entries/LoadingEntry;

    invoke-direct {p3}, Lcom/discord/widgets/chat/list/entries/LoadingEntry;-><init>()V

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p3, Lcom/discord/widgets/chat/list/entries/SpacerEntry;

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->channelId:J

    invoke-direct {p3, v1, v2}, Lcom/discord/widgets/chat/list/entries/SpacerEntry;-><init>(J)V

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-static {p2}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;->access$300(Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Messages;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;->getItem()Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static get()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModel;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/o/b/b/e0/d;->d:Lf/a/o/b/b/e0/d;

    sget-object v2, Lf/a/o/b/b/e0/h;->d:Lf/a/o/b/b/e0/h;

    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, Lf/a/b/r;->o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private static getChatListState(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StoreMessages;->observeIsDetached(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/b/b/e0/c;

    invoke-direct {v1, p0, p1}, Lf/a/o/b/b/e0/c;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getUserId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getUserId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_3

    return v2

    :cond_3
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_4

    return v2

    :cond_4
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    if-nez v1, :cond_5

    if-eqz v3, :cond_6

    goto :goto_0

    :cond_5
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelGuild;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_0
    return v2

    :cond_6
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuildId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuildId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_7

    return v2

    :cond_7
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelNames()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelNames()Ljava/util/Map;

    move-result-object v3

    if-nez v1, :cond_8

    if-eqz v3, :cond_9

    goto :goto_1

    :cond_8
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    :goto_1
    return v2

    :cond_9
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getOldestMessageId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getOldestMessageId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_a

    return v2

    :cond_a
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages()Z

    move-result v3

    if-eq v1, v3, :cond_b

    return v2

    :cond_b
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getMyRoleIds()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getMyRoleIds()Ljava/util/Set;

    move-result-object v3

    if-nez v1, :cond_c

    if-eqz v3, :cond_d

    goto :goto_2

    :cond_c
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    :goto_2
    return v2

    :cond_d
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewMessagesMarkerMessageId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewMessagesMarkerMessageId()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_e

    return v2

    :cond_e
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getList()Ljava/util/List;

    move-result-object v3

    if-nez v1, :cond_f

    if-eqz v3, :cond_10

    goto :goto_3

    :cond_f
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    :goto_3
    return v2

    :cond_10
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getShouldShowGuildGate()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getShouldShowGuildGate()Z

    move-result v3

    if-eq v1, v3, :cond_11

    return v2

    :cond_11
    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewestKnownMessageId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewestKnownMessageId()J

    move-result-wide v5

    cmp-long p1, v3, v5

    if-eqz p1, :cond_12

    return v2

    :cond_12
    return v0
.end method

.method public getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->channelId:J

    return-wide v0
.end method

.method public getChannelNames()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->channelNames:Ljava/util/Map;

    return-object v0
.end method

.method public getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->guildId:J

    return-wide v0
.end method

.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->list:Ljava/util/List;

    return-object v0
.end method

.method public getMyRoleIds()Ljava/util/Set;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->myRoleIds:Ljava/util/Set;

    return-object v0
.end method

.method public getNewMessagesMarkerMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->newMessagesMarkerMessageId:J

    return-wide v0
.end method

.method public getNewestKnownMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->newestKnownMessageId:J

    return-wide v0
.end method

.method public getOldestMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->oldestMessageId:J

    return-wide v0
.end method

.method public getShouldShowGuildGate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->shouldShowGuildGate:Z

    return v0
.end method

.method public getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->userId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 11

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getUserId()J

    move-result-wide v0

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    const/16 v0, 0x3b

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelId()J

    move-result-wide v3

    mul-int/lit8 v1, v1, 0x3b

    ushr-long v5, v3, v2

    xor-long/2addr v3, v5

    long-to-int v4, v3

    add-int/2addr v1, v4

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    mul-int/lit8 v1, v1, 0x3b

    const/16 v4, 0x2b

    if-nez v3, :cond_0

    const/16 v3, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v3

    :goto_0
    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuildId()J

    move-result-wide v5

    mul-int/lit8 v1, v1, 0x3b

    ushr-long v7, v5, v2

    xor-long/2addr v5, v7

    long-to-int v3, v5

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelNames()Ljava/util/Map;

    move-result-object v3

    mul-int/lit8 v1, v1, 0x3b

    if-nez v3, :cond_1

    const/16 v3, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_1
    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getOldestMessageId()J

    move-result-wide v5

    mul-int/lit8 v1, v1, 0x3b

    ushr-long v7, v5, v2

    xor-long/2addr v5, v7

    long-to-int v3, v5

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x3b

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages()Z

    move-result v3

    const/16 v5, 0x4f

    const/16 v6, 0x61

    if-eqz v3, :cond_2

    const/16 v3, 0x4f

    goto :goto_2

    :cond_2
    const/16 v3, 0x61

    :goto_2
    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getMyRoleIds()Ljava/util/Set;

    move-result-object v3

    mul-int/lit8 v1, v1, 0x3b

    if-nez v3, :cond_3

    const/16 v3, 0x2b

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_3
    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewMessagesMarkerMessageId()J

    move-result-wide v7

    mul-int/lit8 v1, v1, 0x3b

    ushr-long v9, v7, v2

    xor-long/2addr v7, v9

    long-to-int v3, v7

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getList()Ljava/util/List;

    move-result-object v3

    mul-int/lit8 v1, v1, 0x3b

    if-nez v3, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_4
    add-int/2addr v1, v4

    mul-int/lit8 v1, v1, 0x3b

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getShouldShowGuildGate()Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_5

    :cond_5
    const/16 v5, 0x61

    :goto_5
    add-int/2addr v1, v5

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewestKnownMessageId()J

    move-result-wide v3

    mul-int/lit8 v1, v1, 0x3b

    ushr-long v5, v3, v2

    xor-long v2, v5, v3

    long-to-int v0, v2

    add-int/2addr v1, v0

    return v1
.end method

.method public isLoadingMessages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages:Z

    return v0
.end method

.method public isSpoilerClickAllowed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "WidgetChatListModel(userId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getUserId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getGuildId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", channelNames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getChannelNames()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", oldestMessageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getOldestMessageId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", isLoadingMessages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", myRoleIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getMyRoleIds()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newMessagesMarkerMessageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewMessagesMarkerMessageId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowGuildGate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getShouldShowGuildGate()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", newestKnownMessageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->getNewestKnownMessageId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
