.class public final Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;
.super Ljava/lang/Object;
.source "WidgetChatListModelTop.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion;->getWelcomeEntry(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Ljava/lang/Long;)Lcom/discord/widgets/chat/list/entries/ChatListEntry;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    iget-object v3, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v9, v3, v7

    if-nez v9, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->hasIcon()Z

    move-result v4

    if-ne v4, v5, :cond_2

    const/4 v9, 0x1

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    :goto_2
    const-wide/32 v4, 0x10000

    invoke-static {v4, v5, v2}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v14

    const-wide/16 v4, 0x1

    invoke-static {v4, v5, v2}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v10

    const-wide/16 v4, 0x10

    invoke-static {v4, v5, v2}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v15

    if-eqz v1, :cond_3

    const-string v2, "me"

    move-object/from16 v4, p2

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/discord/models/domain/ModelGuild;->isOwner(J)Z

    move-result v6

    move v8, v6

    goto :goto_3

    :cond_3
    const/4 v8, 0x0

    :goto_3
    iget-object v2, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyChannel()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    if-eqz v3, :cond_6

    if-eqz v14, :cond_6

    if-nez v10, :cond_4

    if-nez v9, :cond_6

    :cond_4
    new-instance v2, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    goto :goto_4

    :cond_5
    const-string v1, ""

    :goto_4
    move-object v13, v1

    move-object v7, v2

    invoke-direct/range {v7 .. v13}, Lcom/discord/widgets/chat/list/entries/GuildWelcomeEntry;-><init>(ZZZJLjava/lang/String;)V

    goto :goto_6

    :cond_6
    iget-object v1, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v1

    const-string v2, "channel.name"

    if-eqz v1, :cond_7

    new-instance v1, Lcom/discord/widgets/chat/list/entries/StartOfPrivateChatEntry;

    iget-object v3, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    iget-object v3, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v7

    iget-object v2, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    const/4 v3, 0x2

    const/4 v8, 0x0

    invoke-static {v2, v8, v3, v8}, Lcom/discord/utilities/icon/IconUtils;->getForChannel$default(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iget-object v2, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isSystemDM()Z

    move-result v9

    move-object v3, v1

    invoke-direct/range {v3 .. v9}, Lcom/discord/widgets/chat/list/entries/StartOfPrivateChatEntry;-><init>(JLjava/lang/String;ILjava/lang/String;Z)V

    goto :goto_5

    :cond_7
    new-instance v1, Lcom/discord/widgets/chat/list/entries/StartOfChatEntry;

    iget-object v3, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v11

    iget-object v3, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v10, v1

    invoke-direct/range {v10 .. v15}, Lcom/discord/widgets/chat/list/entries/StartOfChatEntry;-><init>(JLjava/lang/String;ZZ)V

    :goto_5
    move-object v2, v1

    :goto_6
    return-object v2
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Lcom/discord/models/domain/ModelUser;

    check-cast p3, Ljava/lang/Long;

    check-cast p4, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion$getWelcomeEntry$2;->call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Ljava/lang/Long;)Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    move-result-object p1

    return-object p1
.end method
