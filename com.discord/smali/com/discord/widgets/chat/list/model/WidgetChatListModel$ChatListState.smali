.class public final enum Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;
.super Ljava/lang/Enum;
.source "WidgetChatListModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/model/WidgetChatListModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChatListState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

.field public static final enum ATTACHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

.field public static final enum DETACHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

.field public static final enum DETACHED_UNTOUCHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    const-string v1, "DETACHED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->DETACHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    new-instance v1, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    const-string v3, "DETACHED_UNTOUCHED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->DETACHED_UNTOUCHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    new-instance v3, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    const-string v5, "ATTACHED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->ATTACHED:Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->$VALUES:[Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;
    .locals 1

    const-class v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->$VALUES:[Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    invoke-virtual {v0}, [Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;

    return-object v0
.end method
