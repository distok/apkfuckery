.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemCallMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation


# instance fields
.field private final messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

.field private final voiceParticipants:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Lcom/discord/widgets/chat/list/entries/MessageEntry;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "voiceParticipants"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageEntry"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;ILjava/lang/Object;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->copy(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/chat/list/entries/MessageEntry;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    return-object v0
.end method

.method public final copy(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Lcom/discord/widgets/chat/list/entries/MessageEntry;",
            ")",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;"
        }
    .end annotation

    const-string/jumbo v0, "voiceParticipants"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageEntry"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;-><init>(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    iget-object p1, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessageEntry()Lcom/discord/widgets/chat/list/entries/MessageEntry;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    return-object v0
.end method

.method public final getVoiceParticipants()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "State(voiceParticipants="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->voiceParticipants:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", messageEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->messageEntry:Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
