.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetChatListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EmptyData;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;

.field private static final NEW_MESSAGES_MAX_SCROLLBACK_COUNT:I = 0x1e

.field private static final NEW_MESSAGES_MIN_SCROLLBACK_COUNT:I = 0xa


# instance fields
.field private final appPermissionsRequests:Lcom/discord/app/AppPermissions$Requests;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

.field private final eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

.field private final handlerOfScrolls:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;

.field private final handlerOfTouches:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;

.field private final handlerOfUpdates:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;

.field private isTouchedSinceLastJump:Z

.field private mentionMeMessageLevelHighlighting:Z

.field private scrollToWithHighlight:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissions$Requests;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissions$Requests;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;Lcom/discord/utilities/time/Clock;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissions$Requests;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPermissionsRequests"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->appPermissionsRequests:Lcom/discord/app/AppPermissions$Requests;

    iput-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    iput-object p4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->clock:Lcom/discord/utilities/time/Clock;

    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EmptyData;

    invoke-direct {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EmptyData;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->mentionMeMessageLevelHighlighting:Z

    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfTouches:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;

    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfScrolls:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;

    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfUpdates:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissions$Requests;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;Lcom/discord/utilities/time/Clock;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p4

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissions$Requests;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;Lcom/discord/utilities/time/Clock;)V

    return-void
.end method

.method public static final synthetic access$getHandlerOfUpdates$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfUpdates:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfUpdates;

    return-object p0
.end method

.method public static final synthetic access$getScrollToWithHighlight$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->scrollToWithHighlight:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;

    return-object p0
.end method

.method public static final synthetic access$isTouchedSinceLastJump$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->isTouchedSinceLastJump:Z

    return p0
.end method

.method public static final synthetic access$publishInteractionState(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->publishInteractionState()V

    return-void
.end method

.method public static final synthetic access$setScrollToWithHighlight$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->scrollToWithHighlight:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;

    return-void
.end method

.method public static final synthetic access$setTouchedSinceLastJump$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->isTouchedSinceLastJump:Z

    return-void
.end method

.method private final publishInteractionState()V
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    instance-of v1, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    if-eqz v0, :cond_1

    iget-wide v0, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->newestKnownMessageId:J

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    :goto_0
    move-wide v5, v0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    new-instance v1, Lcom/discord/stores/StoreChat$InteractionState;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelId()J

    move-result-wide v3

    iget-boolean v7, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->isTouchedSinceLastJump:Z

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;

    move-result-object v8

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/discord/stores/StoreChat$InteractionState;-><init>(JJZLandroidx/recyclerview/widget/LinearLayoutManager;)V

    invoke-interface {v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V

    return-void
.end method


# virtual methods
.method public final disposeHandlers()V
    .locals 9

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$disposeHandlers$1;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$disposeHandlers$1;

    invoke-virtual {p0, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setOnUpdated(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple$OnUpdated;)V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfScrolls:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    new-instance v8, Lcom/discord/stores/StoreChat$InteractionState;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelId()J

    move-result-wide v2

    iget-boolean v6, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->isTouchedSinceLastJump:Z

    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/discord/stores/StoreChat$InteractionState;-><init>(JJZLandroidx/recyclerview/widget/LinearLayoutManager;)V

    invoke-interface {v0, v8}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V

    return-void
.end method

.method public final getAppPermissionsRequests()Lcom/discord/app/AppPermissions$Requests;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->appPermissionsRequests:Lcom/discord/app/AppPermissions$Requests;

    return-object v0
.end method

.method public final getClock()Lcom/discord/utilities/time/Clock;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->clock:Lcom/discord/utilities/time/Clock;

    return-object v0
.end method

.method public final getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    return-object v0
.end method

.method public final getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    return-object v0
.end method

.method public final getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    return-object v0
.end method

.method public final getMentionMeMessageLevelHighlighting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->mentionMeMessageLevelHighlighting:Z

    return v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapter;",
            "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :pswitch_1
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_2
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildTemplate;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_3
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemPrivateChannelStart;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemPrivateChannelStart;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_4
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_5
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_6
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildWelcome;

    const/4 p2, 0x2

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0, p2, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGuildWelcome;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Lcom/discord/utilities/time/Clock;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    :pswitch_7
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_8
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_9
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGameInvite;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_a
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_b
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;

    const p2, 0x7f0d01bd

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_c
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_d
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMentionFooter;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMentionFooter;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_e
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessageHeader;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessageHeader;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_f
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmptyPins;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmptyPins;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto/16 :goto_0

    :pswitch_10
    new-instance p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    const p2, 0x7f0d01b9

    invoke-direct {p1, p2, p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    goto/16 :goto_0

    :pswitch_11
    new-instance p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    const p2, 0x7f0d01cc

    invoke-direct {p1, p2, p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    goto/16 :goto_0

    :pswitch_12
    new-instance p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    const p2, 0x7f0d01cb

    invoke-direct {p1, p2, p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    goto :goto_0

    :pswitch_13
    new-instance p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    const p2, 0x7f0d01cd

    invoke-direct {p1, p2, p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    goto :goto_0

    :pswitch_14
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSearchResultCount;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSearchResultCount;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_15
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemBlocked;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemBlocked;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_16
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemTimestamp;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemTimestamp;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_17
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemNewMessages;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemNewMessages;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_18
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListItem;

    const p2, 0x7f0d01ce

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_19
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_1a
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSystemMessage;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSystemMessage;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_1b
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_1c
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_1d
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListItem;

    const p2, 0x7f0d01c4

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_1e
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;

    const p2, 0x7f0d01c7

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    goto :goto_0

    :pswitch_1f
    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;

    const p2, 0x7f0d01d2

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1f
    .end packed-switch
.end method

.method public final onPause()V
    .locals 4

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v2

    instance-of v3, v2, Lcom/discord/widgets/chat/list/FragmentLifecycleListener;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/discord/widgets/chat/list/FragmentLifecycleListener;

    invoke-interface {v2}, Lcom/discord/widgets/chat/list/FragmentLifecycleListener;->onPause()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final onQuickAddReactionClicked(J)V
    .locals 10

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getGuildId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelId()J

    move-result-wide v5

    iget-object v7, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v7}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getShouldShowGuildGate()Z

    move-result v9

    move-wide v7, p1

    invoke-interface/range {v0 .. v9}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onQuickAddReactionClicked(JJJJZ)V

    return-void
.end method

.method public final onReactionClicked(JLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 12

    const-string v0, "reaction"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getGuildId()J

    move-result-wide v2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelId()J

    move-result-wide v6

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getShouldShowGuildGate()Z

    move-result v10

    move-wide v8, p1

    move-object v11, p3

    invoke-interface/range {v1 .. v11}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onReactionClicked(JJJJZLcom/discord/models/domain/ModelMessageReaction;)V

    return-void
.end method

.method public final onReactionLongClicked(JLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 10

    const-string v0, "reaction"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getGuildId()J

    move-result-wide v2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelId()J

    move-result-wide v4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getShouldShowGuildGate()Z

    move-result v8

    move-wide v6, p1

    move-object v9, p3

    invoke-interface/range {v1 .. v9}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onReactionLongClicked(JJJZLcom/discord/models/domain/ModelMessageReaction;)V

    return-void
.end method

.method public final onResume()V
    .locals 4

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v2

    instance-of v3, v2, Lcom/discord/widgets/chat/list/FragmentLifecycleListener;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/discord/widgets/chat/list/FragmentLifecycleListener;

    invoke-interface {v2}, Lcom/discord/widgets/chat/list/FragmentLifecycleListener;->onResume()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "sticker"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    invoke-interface {v0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V

    return-void
.end method

.method public final onUserActivityAction(JJILcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelApplication;)V
    .locals 12

    move-object v0, p0

    const-string v1, "activity"

    move-object/from16 v10, p6

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "application"

    move-object/from16 v11, p7

    invoke-static {v11, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->eventHandler:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    iget-object v1, v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelId()J

    move-result-wide v5

    move-wide v3, p1

    move-wide v7, p3

    move/from16 v9, p5

    invoke-interface/range {v2 .. v11}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onUserActivityAction(JJJILcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelApplication;)V

    return-void
.end method

.method public final scrollToMessageId(JLrx/functions/Action0;)V
    .locals 6

    const-string v0, "onCompleted"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->scrollToWithHighlight:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->getMessageId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    cmp-long v0, p1, v4

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {p3}, Lrx/functions/Action0;->call()V

    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->isTouchedSinceLastJump:Z

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->scrollToWithHighlight:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->cancel()V

    :cond_2
    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$scrollToMessageId$1;

    invoke-direct {v1, p0, p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$scrollToMessageId$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Lrx/functions/Action0;)V

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;JLkotlin/jvm/functions/Function0;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->scrollToWithHighlight:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;

    return-void
.end method

.method public final setData(Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->data:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    invoke-interface {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getList()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method

.method public final setHandlers()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$setHandlers$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$setHandlers$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    invoke-virtual {p0, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setOnUpdated(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple$OnUpdated;)V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfTouches:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->handlerOfScrolls:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfScrolls;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void
.end method

.method public final setMentionMeMessageLevelHighlighting(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->mentionMeMessageLevelHighlighting:Z

    return-void
.end method
