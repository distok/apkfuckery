.class public Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemMessage.java"


# static fields
.field private static final MAX_REPLY_AST_NODES:I = 0x32

.field public static final synthetic a:I


# instance fields
.field private failedUploadList:Lcom/discord/views/FailedUploadList;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private itemAlertText:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private itemAvatar:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private itemName:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private itemTag:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private itemText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

.field private itemTimestamp:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyAvatar:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyHolder:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyIcon:Landroid/widget/ImageView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyLeadingViewsHolder:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyLinkItem:Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyName:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private userSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public constructor <init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 4
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a024e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a024f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAvatar:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0256

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemName:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0258

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0259

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTimestamp:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0243

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/FailedUploadList;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->failedUploadList:Lcom/discord/views/FailedUploadList;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a023a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAlertText:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0250

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyHolder:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0253

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLinkItem:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0254

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyName:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0257

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0248

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLeadingViewsHolder:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0252

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyIcon:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0251

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyAvatar:Landroid/widget/ImageView;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->userSettings:Lcom/discord/stores/StoreUserSettings;

    new-instance p1, Lf/a/o/b/b/q;

    invoke-direct {p1, p2}, Lf/a/o/b/b/q;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemName:Landroid/widget/TextView;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p0, p1, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    new-instance p1, Lf/a/o/b/b/z;

    invoke-direct {p1, p2}, Lf/a/o/b/b/z;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    new-array v1, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAvatar:Landroid/widget/ImageView;

    aput-object v2, v1, v3

    invoke-virtual {p0, p1, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    new-instance p1, Lf/a/o/b/b/v;

    invoke-direct {p1, p2}, Lf/a/o/b/b/v;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    new-array v1, v3, [Landroid/view/View;

    invoke-virtual {p0, p1, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    new-instance p1, Lf/a/o/b/b/w;

    invoke-direct {p1, p0, p2}, Lf/a/o/b/b/w;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    new-array v1, v3, [Landroid/view/View;

    invoke-virtual {p0, p1, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnLongClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    new-instance p1, Lf/a/o/b/b/u;

    invoke-direct {p1, p2}, Lf/a/o/b/b/u;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const/4 p2, 0x2

    new-array p2, p2, [Landroid/view/View;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemName:Landroid/widget/TextView;

    aput-object v1, p2, v3

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAvatar:Landroid/widget/ImageView;

    aput-object v1, p2, v0

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnLongClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    return-void
.end method

.method public static synthetic a(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object p1

    invoke-static {p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->extractMessage(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/models/domain/ModelMessage;

    move-result-object p2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object p0

    invoke-interface {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getGuildId()J

    move-result-wide v0

    invoke-interface {p1, p2, v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onMessageAuthorNameClicked(Lcom/discord/models/domain/ModelMessage;J)V

    return-void
.end method

.method public static synthetic b(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object p1

    invoke-static {p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->extractMessage(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/models/domain/ModelMessage;

    move-result-object p2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object p0

    invoke-interface {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getGuildId()J

    move-result-wide v0

    invoke-interface {p1, p2, v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onMessageAuthorAvatarClicked(Lcom/discord/models/domain/ModelMessage;J)V

    return-void
.end method

.method public static synthetic c(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object p0

    invoke-static {p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->extractMessage(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/models/domain/ModelMessage;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onMessageClicked(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private configureItemTag(Lcom/discord/models/domain/ModelMessage;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-static {p1}, Lcom/discord/utilities/guilds/PublicGuildUtils;->isPublicGuildSystemMessage(Lcom/discord/models/domain/ModelMessage;)Z

    move-result p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isBot()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    const v1, 0x7f120384

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :cond_3
    :goto_2
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    const v1, 0x7f1217ce

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isVerifiedBot()Z

    move-result p1

    if-eqz p1, :cond_4

    const p1, 0x7f080487

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    :goto_4
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_5
    return-void
.end method

.method public static synthetic e(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object p1

    invoke-static {p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->extractMessage(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/models/domain/ModelMessage;

    move-result-object p2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object p0

    invoke-interface {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getGuildId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p1, p2, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onMessageAuthorLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/Long;)V

    return-void
.end method

.method private static extractMessage(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/models/domain/ModelMessage;
    .locals 0

    check-cast p0, Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object p0

    return-object p0
.end method

.method private getAuthorTextColor(Lcom/discord/models/domain/ModelGuildMember$Computed;)I
    .locals 2
    .param p1    # Lcom/discord/models/domain/ModelGuildMember$Computed;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040153

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getColor(Lcom/discord/models/domain/ModelGuildMember$Computed;I)I

    move-result p1

    return p1
.end method

.method private getSpoilerClickHandler(Lcom/discord/models/domain/ModelMessage;)Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelMessage;",
            ")",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/textprocessing/node/SpoilerNode<",
            "*>;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->isSpoilerClickAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Lf/a/o/b/b/a0;

    invoke-direct {v0, p0, p1}, Lf/a/o/b/b/a0;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;Lcom/discord/models/domain/ModelMessage;)V

    return-object v0
.end method

.method private shouldLinkify(Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xc8

    const/4 v3, 0x1

    if-ge v1, v2, :cond_1

    return v3

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2e

    if-ne v4, v5, :cond_2

    add-int/lit8 v2, v2, 0x1

    const/16 v4, 0x32

    if-lt v2, v4, :cond_2

    return v0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return v3
.end method


# virtual methods
.method public configureReplyAvatar(Lcom/discord/models/domain/ModelUser;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyAvatar:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-nez p1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyAvatar:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyAvatar:Landroid/widget/ImageView;

    const v1, 0x7f070069

    invoke-static {v0, p1, v1}, Lcom/discord/utilities/icon/IconUtils;->setIcon(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelUser;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public configureReplyContentWithResourceId(I)V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLeadingViewsHolder:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLeadingViewsHolder:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    new-instance v2, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance p1, Landroid/text/style/StyleSpan;

    const/4 v3, 0x2

    invoke-direct {p1, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    new-instance v3, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-direct {v3, v0, v1}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v0

    const/16 v4, 0x21

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v0

    invoke-virtual {v2, p1, v1, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    const v0, 0x3f23d70a    # 0.64f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyLayoutDirection()V

    :cond_0
    return-void
.end method

.method public configureReplyLayoutDirection()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyHolder:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    if-eqz v0, :cond_0

    invoke-static {}, Landroidx/core/text/BidiFormatter;->getInstance()Landroidx/core/text/BidiFormatter;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/text/BidiFormatter;->isRtl(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyHolder:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutDirection(I)V

    :cond_0
    return-void
.end method

.method public configureReplyNames()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyNames(Ljava/lang/String;IZ)V

    return-void
.end method

.method public configureReplyNames(Ljava/lang/String;IZ)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyName:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    const-string p3, "@"

    goto :goto_0

    :cond_0
    const-string p3, ""

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyName:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyName:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyName:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyName:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_1
    return-void
.end method

.method public configureReplyPreview(Lcom/discord/widgets/chat/list/entries/MessageEntry;)V
    .locals 16
    .param p1    # Lcom/discord/widgets/chat/list/entries/MessageEntry;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v7, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getReplyData()Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;

    move-result-object v1

    if-eqz v1, :cond_b

    const/16 v2, 0x13

    if-eq v0, v2, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v0, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyHolder:Landroid/view/View;

    const/4 v8, 0x0

    if-eqz v0, :cond_1

    iget-object v2, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLinkItem:Landroid/view/View;

    if-eqz v2, :cond_1

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLinkItem:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->getMessageEntry()Lcom/discord/widgets/chat/list/entries/MessageEntry;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->getMessageState()Lcom/discord/stores/StoreMessageReplies$MessageState;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;->isRepliedUserBlocked()Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v7, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyAvatar(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyNames()V

    const v0, 0x7f12151a

    invoke-virtual {v7, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyContentWithResourceId(I)V

    goto/16 :goto_1

    :cond_2
    instance-of v1, v2, Lcom/discord/stores/StoreMessageReplies$MessageState$Unloaded;

    if-eqz v1, :cond_3

    invoke-virtual {v7, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyAvatar(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyNames()V

    const v0, 0x7f12151c

    invoke-virtual {v7, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyContentWithResourceId(I)V

    goto/16 :goto_1

    :cond_3
    instance-of v1, v2, Lcom/discord/stores/StoreMessageReplies$MessageState$Deleted;

    if-eqz v1, :cond_4

    invoke-virtual {v7, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyAvatar(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyNames()V

    const v0, 0x7f12151b

    invoke-virtual {v7, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyContentWithResourceId(I)V

    goto/16 :goto_1

    :cond_4
    instance-of v1, v2, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    if-eqz v1, :cond_a

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v4

    new-instance v1, Lf/a/o/b/b/r;

    invoke-direct {v1, v4}, Lf/a/o/b/b/r;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    const/4 v2, 0x1

    new-array v5, v2, [Landroid/view/View;

    iget-object v6, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyHolder:Landroid/view/View;

    aput-object v6, v5, v8

    invoke-virtual {v7, v1, v5}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyAvatar(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getNickOrUsernames()Ljava/util/Map;

    move-result-object v5

    invoke-static {v1, v5}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v5

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v9

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v11

    cmp-long v6, v11, v9

    if-nez v6, :cond_6

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getAuthor()Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-result-object v1

    invoke-direct {v7, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getAuthorTextColor(Lcom/discord/models/domain/ModelGuildMember$Computed;)I

    move-result v1

    invoke-virtual {v7, v5, v1, v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyNames(Ljava/lang/String;IZ)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    const-string v1, ""

    :cond_8
    move-object v9, v1

    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    if-eqz v1, :cond_a

    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLeadingViewsHolder:Landroid/view/View;

    if-eqz v1, :cond_a

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessage;->hasEmbeds()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    const v0, 0x7f12151d

    invoke-virtual {v7, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyContentWithResourceId(I)V

    goto :goto_1

    :cond_9
    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v7, v10, v0, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getMessageRenderContext(Landroid/content/Context;Lcom/discord/widgets/chat/list/entries/MessageEntry;Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/textprocessing/MessageRenderContext;

    move-result-object v12

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    new-instance v5, Lcom/discord/stores/StoreMessageState$State;

    invoke-direct {v5}, Lcom/discord/stores/StoreMessageState$State;-><init>()V

    const/4 v6, 0x0

    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p0

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v11

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getMessagePreprocessor(JLcom/discord/models/domain/ModelMessage;Lcom/discord/stores/StoreMessageState$State;ZLjava/lang/Integer;)Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    move-result-object v13

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, " "

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    sget-object v14, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->REPLY:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lcom/discord/utilities/textprocessing/DiscordParser;->parseChannelMessage(Landroid/content/Context;Ljava/lang/String;Lcom/discord/utilities/textprocessing/MessageRenderContext;Lcom/discord/utilities/textprocessing/MessagePreprocessor;Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLeadingViewsHolder:Landroid/view/View;

    invoke-virtual {v1, v8, v8}, Landroid/view/View;->measure(II)V

    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLeadingViewsHolder:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    new-instance v2, Landroid/text/style/LeadingMarginSpan$Standard;

    invoke-direct {v2, v1, v8}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v3, 0x21

    invoke-virtual {v0, v2, v8, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyLayoutDirection()V

    :cond_a
    :goto_1
    return-void

    :cond_b
    :goto_2
    iget-object v0, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyHolder:Landroid/view/View;

    if-eqz v0, :cond_c

    iget-object v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLinkItem:Landroid/view/View;

    if-eqz v1, :cond_c

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->replyLinkItem:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_c
    return-void
.end method

.method public synthetic d(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object p1

    invoke-static {p4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->extractMessage(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/models/domain/ModelMessage;

    move-result-object p2

    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {p3}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p3

    iget-object p4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {p4}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    move-result-object p4

    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result p4

    add-int/lit8 p4, p4, -0x1

    const/4 v0, 0x0

    invoke-static {p4, v0}, Ljava/lang/Math;->max(II)I

    move-result p4

    invoke-interface {p3, v0, p4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onMessageLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getMessagePreprocessor(JLcom/discord/models/domain/ModelMessage;Lcom/discord/stores/StoreMessageState$State;)Lcom/discord/utilities/textprocessing/MessagePreprocessor;
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getMessagePreprocessor(JLcom/discord/models/domain/ModelMessage;Lcom/discord/stores/StoreMessageState$State;ZLjava/lang/Integer;)Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    move-result-object p1

    return-object p1
.end method

.method public getMessagePreprocessor(JLcom/discord/models/domain/ModelMessage;Lcom/discord/stores/StoreMessageState$State;ZLjava/lang/Integer;)Lcom/discord/utilities/textprocessing/MessagePreprocessor;
    .locals 7
    .param p6    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getInlineEmbedMedia()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelMessage;->getEmbeds()Ljava/util/List;

    move-result-object p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    move-object v4, p3

    new-instance p3, Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    move-object v0, p3

    move-wide v1, p1

    move-object v3, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;-><init>(JLcom/discord/stores/StoreMessageState$State;Ljava/util/List;ZLjava/lang/Integer;)V

    return-object p3
.end method

.method public getMessageRenderContext(Landroid/content/Context;Lcom/discord/widgets/chat/list/entries/MessageEntry;Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/textprocessing/MessageRenderContext;
    .locals 18
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/discord/widgets/chat/list/entries/MessageEntry;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/jvm/functions/Function1;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/discord/widgets/chat/list/entries/MessageEntry;",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/textprocessing/node/SpoilerNode<",
            "*>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    new-instance v17, Lcom/discord/utilities/textprocessing/MessageRenderContext;

    iget-object v1, v0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v3

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getAnimateEmojis()Z

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getNickOrUsernames()Ljava/util/Map;

    move-result-object v6

    iget-object v1, v0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getChannelNames()Ljava/util/Map;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getRoles()Ljava/util/Map;

    move-result-object v8

    sget-object v10, Lf/a/o/b/b/x;->d:Lf/a/o/b/b/x;

    new-instance v11, Lf/a/o/b/b/y;

    invoke-direct {v11, v0}, Lf/a/o/b/b/y;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;)V

    const v1, 0x7f0405b0

    invoke-static {v2, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v12

    const v1, 0x7f0405b1

    invoke-static {v2, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v13

    new-instance v15, Lf/a/o/b/b/s;

    invoke-direct {v15, v0}, Lf/a/o/b/b/s;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;)V

    sget-object v16, Lf/a/o/b/b/t;->d:Lf/a/o/b/b/t;

    const v9, 0x7f040177

    move-object/from16 v1, v17

    move-object/from16 v14, p3

    invoke-direct/range {v1 .. v16}, Lcom/discord/utilities/textprocessing/MessageRenderContext;-><init>(Landroid/content/Context;JZLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v17
.end method

.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 6
    .param p2    # Lcom/discord/widgets/chat/list/entries/ChatListEntry;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    check-cast p2, Lcom/discord/widgets/chat/list/entries/MessageEntry;

    iget-object p1, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureItemTag(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemName:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getNickOrUsernames()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemName:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getAuthor()Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getAuthorTextColor(Lcom/discord/models/domain/ModelGuildMember$Computed;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTimestamp:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/discord/utilities/time/TimeUtils;->toReadableTimeString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {p0, v0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->processMessageText(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/widgets/chat/list/entries/MessageEntry;)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/discord/widgets/chat/list/ChatListItemMessageAccessibilityDelegate;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemText:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iget-object v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemName:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTag:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemTimestamp:Landroid/widget/TextView;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/discord/widgets/chat/list/ChatListItemMessageAccessibilityDelegate;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    invoke-virtual {p0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->configureReplyPreview(Lcom/discord/widgets/chat/list/entries/MessageEntry;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAvatar:Landroid/widget/ImageView;

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    const v1, 0x7f07006b

    invoke-static {p2, v0, v1}, Lcom/discord/utilities/icon/IconUtils;->setIcon(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelUser;I)V

    :cond_3
    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->failedUploadList:Lcom/discord/views/FailedUploadList;

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p2, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isFailed()Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->failedUploadList:Lcom/discord/views/FailedUploadList;

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->failedUploadList:Lcom/discord/views/FailedUploadList;

    iget-object v2, p1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    invoke-virtual {p2, v2}, Lcom/discord/views/FailedUploadList;->setUp(Ljava/util/List;)V

    goto :goto_0

    :cond_4
    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->failedUploadList:Lcom/discord/views/FailedUploadList;

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_5
    :goto_0
    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAlertText:Landroid/widget/TextView;

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isFailed()Z

    move-result p2

    if-eqz p2, :cond_8

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAlertText:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result p1

    const/4 p2, -0x3

    if-ne p1, p2, :cond_6

    const/4 v1, 0x1

    :cond_6
    if-eqz v1, :cond_7

    const p1, 0x7f120df8

    goto :goto_1

    :cond_7
    const p1, 0x7f121651

    :goto_1
    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAlertText:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_8
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->itemAlertText:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_9
    :goto_2
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method

.method public processMessageText(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/widgets/chat/list/entries/MessageEntry;)V
    .locals 11
    .param p1    # Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/discord/widgets/chat/list/entries/MessageEntry;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->isWebhook()Z

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->getEditedTimestamp()J

    move-result-wide v0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->isSourceDeleted()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1216cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessageState()Lcom/discord/stores/StoreMessageState$State;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v7, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getMessagePreprocessor(JLcom/discord/models/domain/ModelMessage;Lcom/discord/stores/StoreMessageState$State;)Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    move-result-object v10

    invoke-direct {p0, v7}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getSpoilerClickHandler(Lcom/discord/models/domain/ModelMessage;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-virtual {p0, v6, p2, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->getMessageRenderContext(Landroid/content/Context;Lcom/discord/widgets/chat/list/entries/MessageEntry;Lkotlin/jvm/functions/Function1;)Lcom/discord/utilities/textprocessing/MessageRenderContext;

    move-result-object v2

    sget-object v4, Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;->ALLOW_MASKED_LINKS:Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;

    move-object v0, v6

    move-object v3, v10

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/textprocessing/DiscordParser;->parseChannelMessage(Landroid/content/Context;Ljava/lang/String;Lcom/discord/utilities/textprocessing/MessageRenderContext;Lcom/discord/utilities/textprocessing/MessagePreprocessor;Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->isLinkifyConflicting()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;->shouldLinkify(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x6

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    invoke-static {p1, v1}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getMentionMeMessageLevelHighlighting()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->isMentionEveryone()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v1, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v1

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    cmp-long v10, v4, v1

    if-nez v10, :cond_4

    const/4 v0, 0x1

    :cond_5
    if-nez v0, :cond_7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getMyRoleIds()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_4

    :cond_7
    move v8, v0

    goto :goto_4

    :cond_8
    const/4 v8, 0x0

    :goto_4
    if-eqz v8, :cond_9

    const v0, 0x7f0405ad

    invoke-static {v6, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto :goto_5

    :cond_9
    invoke-virtual {p1, v9}, Landroidx/appcompat/widget/AppCompatTextView;->setBackgroundResource(I)V

    :goto_5
    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result p2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_a

    const/high16 p2, 0x3f000000    # 0.5f

    goto :goto_6

    :cond_a
    const/high16 p2, 0x3f800000    # 1.0f

    :goto_6
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    return-void
.end method
