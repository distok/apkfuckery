.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemSticker.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final stickerView$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;

    const-string/jumbo v3, "stickerView"

    const-string v4, "getStickerView()Lcom/discord/views/sticker/StickerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01d0

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const p1, 0x7f0a0249

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->stickerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    return-object p0
.end method

.method private final getStickerView()Lcom/discord/views/sticker/StickerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->stickerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    return-object v0
.end method


# virtual methods
.method public getSubscription()Lrx/Subscription;
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/views/sticker/StickerView;->getSubscription()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    move-object p1, p2

    check-cast p1, Lcom/discord/widgets/chat/list/entries/StickerEntry;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/StickerEntry;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object p1

    sget-object v1, Lcom/discord/views/sticker/StickerView;->j:[Lkotlin/reflect/KProperty;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker$onConfigure$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    invoke-virtual {p1, v0}, Lcom/discord/views/sticker/StickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemSticker;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method
