.class public Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;
.super Ljava/lang/Object;
.source "WidgetChatList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UserReactionHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;
    }
.end annotation


# static fields
.field private static final REQUEST_RATE_LIMIT_MILLIS:J = 0xfaL


# instance fields
.field private final commitReactionAdd:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final commitReactionRemove:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final requestStream:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;",
            "Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatList;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatList;)V
    .locals 5

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->requestStream:Lrx/subjects/Subject;

    sget-object v0, Lf/a/o/b/b/i;->d:Lf/a/o/b/b/i;

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->commitReactionAdd:Lrx/functions/Action1;

    sget-object v0, Lf/a/o/b/b/m;->d:Lf/a/o/b/b/m;

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->commitReactionRemove:Lrx/functions/Action1;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v1

    new-instance v2, Lg0/l/a/o2;

    const-wide/16 v3, 0xfa

    invoke-direct {v2, v3, v4, v0, v1}, Lg0/l/a/o2;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    new-instance v0, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v0, p1, v2}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/o/b/b/l;

    invoke-direct {v0, p0}, Lf/a/o/b/b/l;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public static synthetic a(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->requestReactionUpdate(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)V

    return-void
.end method

.method public static synthetic access$200(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;Lcom/discord/models/domain/emoji/Emoji;JJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->addNewReaction(Lcom/discord/models/domain/emoji/Emoji;JJ)V

    return-void
.end method

.method private addNewReaction(Lcom/discord/models/domain/emoji/Emoji;JJ)V
    .locals 6

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->getReactionKey()Ljava/lang/String;

    move-result-object v5

    move-wide v1, p2

    move-wide v3, p4

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/rest/RestAPI;->addReaction(JJLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {p2}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lf/a/o/b/b/n;->d:Lf/a/o/b/b/n;

    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {p2, p3}, Lf/a/b/r;->m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method private requestReactionUpdate(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)V
    .locals 14

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;->access$300(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)Lcom/discord/models/domain/ModelMessageReaction;

    move-result-object v0

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;->access$400(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)J

    move-result-wide v9

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;->access$500(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)J

    move-result-wide v11

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;->access$600(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->isCustom()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->getName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    new-instance v13, Lcom/discord/models/domain/ModelMessageReaction$Update;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v8

    move-object v1, v13

    move-wide v4, v9

    move-wide v6, v11

    invoke-direct/range {v1 .. v8}, Lcom/discord/models/domain/ModelMessageReaction$Update;-><init>(JJJLcom/discord/models/domain/ModelMessageReaction$Emoji;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->isMe()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    move-wide v2, v9

    move-wide v4, v11

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/rest/RestAPI;->removeSelfReaction(JJLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->commitReactionRemove:Lrx/functions/Action1;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->commitReactionAdd:Lrx/functions/Action1;

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    move-wide v2, v9

    move-wide v4, v11

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/rest/RestAPI;->addReaction(JJLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->commitReactionAdd:Lrx/functions/Action1;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->commitReactionRemove:Lrx/functions/Action1;

    :goto_1
    invoke-interface {v1, v13}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {v1}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lf/a/o/b/b/j;

    invoke-direct {v1, v0}, Lf/a/o/b/b/j;-><init>(Lcom/discord/models/domain/ModelMessageReaction;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Lf/a/o/b/b/k;

    invoke-direct {v3, v2, v13}, Lf/a/o/b/b/k;-><init>(Lrx/functions/Action1;Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    invoke-static {v1, v0, v3}, Lf/a/b/r;->k(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method


# virtual methods
.method public toggleReaction(JJLjava/lang/Long;Lcom/discord/models/domain/ModelMessageReaction;)V
    .locals 10

    new-instance v9, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v0, v9

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler$UpdateRequest;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;JJJLcom/discord/models/domain/ModelMessageReaction;)V

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->requestStream:Lrx/subjects/Subject;

    invoke-interface {v1, v9}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method
