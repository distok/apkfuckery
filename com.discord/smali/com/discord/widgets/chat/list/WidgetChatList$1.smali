.class public Lcom/discord/widgets/chat/list/WidgetChatList$1;
.super Ljava/lang/Object;
.source "WidgetChatList.java"

# interfaces
.implements Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatList;->createAdapter(Lcom/discord/stores/StoreChat;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

.field public final synthetic val$chat:Lcom/discord/stores/StoreChat;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatList;Lcom/discord/stores/StoreChat;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->val$chat:Lcom/discord/stores/StoreChat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallMessageClicked(JLcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->ACTIVE_JOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    if-ne p3, v0, :cond_0

    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->launch(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->ACTIVE_UNJOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    if-ne p3, v0, :cond_1

    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    new-instance v0, Lf/a/o/b/b/e;

    invoke-direct {v0, p0, p1, p2}, Lf/a/o/b/b/e;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList$1;J)V

    invoke-virtual {p3, v0}, Lcom/discord/app/AppFragment;->requestMicrophone(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_1
    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->show(JLandroidx/fragment/app/FragmentManager;)V

    :goto_0
    return-void
.end method

.method public onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V
    .locals 1
    .param p1    # Lcom/discord/stores/StoreChat$InteractionState;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->val$chat:Lcom/discord/stores/StoreChat;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChat;->setInteractionState(Lcom/discord/stores/StoreChat$InteractionState;)V

    return-void
.end method

.method public onListClicked()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Lcom/discord/app/AppFragment;->hideKeyboard()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {v0}, Lcom/discord/widgets/chat/list/WidgetChatList;->access$100(Lcom/discord/widgets/chat/list/WidgetChatList;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideExpressionTray()Z

    return-void
.end method

.method public onMessageAuthorAvatarClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 4
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1}, Lcom/discord/utilities/guilds/PublicGuildUtils;->isPublicGuildSystemMessage(Lcom/discord/models/domain/ModelMessage;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isCrosspost()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {v0, v1, p1, v2, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/WidgetChatList$1;->onMessageAuthorNameClicked(Lcom/discord/models/domain/ModelMessage;J)V

    :goto_1
    return-void
.end method

.method public onMessageAuthorLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/Long;)V
    .locals 4
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isWebhook()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f121965

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-static {p1, p2, v0, v1, v2}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-static {v0, v1, p1, v2, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V

    return-void
.end method

.method public onMessageAuthorNameClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 6
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1}, Lcom/discord/utilities/guilds/PublicGuildUtils;->isPublicGuildSystemMessage(Lcom/discord/models/domain/ModelMessage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/widgets/guilds/profile/WidgetPublicAnnouncementProfileSheet;->show(Landroidx/fragment/app/FragmentManager;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isCrosspost()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getGuildId()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getGuildId()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getChannelId()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->show(Landroidx/fragment/app/FragmentManager;ZJJ)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/discord/stores/StoreStream;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreChat;->appendMention(Lcom/discord/models/domain/ModelUser;J)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onMessageBlockedGroupClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 3
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->val$chat:Lcom/discord/stores/StoreChat;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChat;->toggleBlockedMessageGroup(J)V

    return-void
.end method

.method public onMessageClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onMessageLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;)V
    .locals 6
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->showForChat(Landroidx/fragment/app/FragmentManager;JJLjava/lang/CharSequence;)V

    return-void
.end method

.method public onOldestMessageId(JJ)V
    .locals 0

    return-void
.end method

.method public onOpenPinsClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 3
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;->show(Landroid/content/Context;J)V

    return-void
.end method

.method public onQuickAddReactionClicked(JJJJZ)V
    .locals 6

    if-eqz p9, :cond_0

    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3, p1, p2}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->create(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    new-instance p2, Lf/a/o/b/b/b;

    move-object v0, p2

    move-object v1, p0

    move-wide v2, p5

    move-wide v4, p7

    invoke-direct/range {v0 .. v5}, Lf/a/o/b/b/b;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList$1;JJ)V

    sget-object p3, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;->CHAT:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    sget-object p4, Lf/a/o/b/b/f;->d:Lf/a/o/b/b/f;

    invoke-static {p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->launchBottomSheet(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public onQuickDownloadClicked(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    new-instance v3, Lf/a/o/b/b/g;

    invoke-direct {v3, v0, p1, p2, v1}, Lf/a/o/b/b/g;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v2, v3}, Lcom/discord/app/AppFragment;->requestMediaDownload(Lkotlin/jvm/functions/Function0;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onReactionClicked(JJJJZLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 9
    .param p10    # Lcom/discord/models/domain/ModelMessageReaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object v0, p0

    if-eqz p9, :cond_0

    iget-object v1, v0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    move-wide v2, p1

    invoke-static {v1, p1, p2}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->create(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatList;->access$000(Lcom/discord/widgets/chat/list/WidgetChatList;)Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;

    move-result-object v2

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-wide v3, p3

    move-wide v5, p5

    move-object/from16 v8, p10

    invoke-virtual/range {v2 .. v8}, Lcom/discord/widgets/chat/list/WidgetChatList$UserReactionHandler;->toggleReaction(JJLjava/lang/Long;Lcom/discord/models/domain/ModelMessageReaction;)V

    :goto_0
    return-void
.end method

.method public onReactionLongClicked(JJJZLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 6
    .param p8    # Lcom/discord/models/domain/ModelMessageReaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    if-eqz p7, :cond_0

    iget-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3, p1, p2}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->create(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    move-wide v0, p3

    move-wide v2, p5

    move-object v5, p8

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->create(JJLandroid/content/Context;Lcom/discord/models/domain/ModelMessageReaction;)V

    :goto_0
    return-void
.end method

.method public onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 3
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {v0}, Lcom/discord/widgets/chat/list/WidgetChatList;->access$100(Lcom/discord/widgets/chat/list/WidgetChatList;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    invoke-static {v0, p2, v1, v2}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;J)V

    :cond_0
    return-void
.end method

.method public onUrlLongClicked(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/WidgetUrlActions;->launch(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onUserActivityAction(JJJILcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelApplication;)V
    .locals 10

    move-object v0, p0

    invoke-virtual/range {p8 .. p8}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual/range {p8 .. p8}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v7

    if-eqz v1, :cond_1

    if-eqz v7, :cond_1

    const/4 v2, 0x1

    move/from16 v3, p7

    if-eq v3, v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-wide v3, p1

    invoke-virtual/range {v2 .. v9}, Lcom/discord/utilities/rest/RestAPI;->userActivityActionJoin(JJLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-static {v2}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/o/b/b/a;

    move-object/from16 v3, p9

    invoke-direct {v2, p0, v3}, Lf/a/o/b/b/a;-><init>(Lcom/discord/widgets/chat/list/WidgetChatList$1;Lcom/discord/models/domain/ModelApplication;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    :cond_1
    :goto_0
    return-void
.end method

.method public onUserMentionClicked(JJJ)V
    .locals 0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    iget-object p4, p0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {p4}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p4

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    invoke-static {p1, p2, p3, p4, p5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V

    return-void
.end method
