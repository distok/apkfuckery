.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemInvite.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;->configureInvalidUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $isInviter:Z

.field public final synthetic $model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;ZLcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$isInviter:Z

    iput-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;

    iput-object p4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 10

    iget-boolean p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$isInviter:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "model?.channel?.guildId \u2026return@setOnClickListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sget-object v1, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare;->Companion:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$context:Landroid/content/Context;

    const-string p1, "context"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    const-string v7, "Invite Button Embed"

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;->launch$default(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;Landroid/content/Context;JLjava/lang/Long;ZLjava/lang/String;ILjava/lang/Object;)V

    goto :goto_1

    :cond_0
    return-void

    :cond_1
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->$model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$Model$Invalid;->getAuthorUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite$configureInvalidUI$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;->access$getItem$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemInvite;)Lcom/discord/widgets/chat/list/entries/InviteEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/InviteEntry;->getGuildId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/stores/StoreChat;->appendMention(Lcom/discord/models/domain/ModelUser;J)V

    :goto_1
    return-void
.end method
