.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;
.super Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;
.source "WidgetChatListAdapterItemUploadProgress.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Many"
.end annotation


# instance fields
.field private final numFiles:I

.field private final progress:I

.field private final sizeBytes:J


# direct methods
.method public constructor <init>(IJI)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    iput-wide p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    iput p4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;IJIILjava/lang/Object;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-wide p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget p4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->copy(IJI)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    return v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    return-wide v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    return v0
.end method

.method public final copy(IJI)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;-><init>(IJI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    iget v1, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    iget-wide v2, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    iget p1, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNumFiles()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    return v0
.end method

.method public final getProgress()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    return v0
.end method

.method public final getSizeBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Many(numFiles="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->numFiles:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", sizeBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->sizeBytes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->progress:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
