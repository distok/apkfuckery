.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemCallMessage.kt"

# interfaces
.implements Lcom/discord/widgets/chat/list/FragmentLifecycleListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final callParticipantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private chatListEntry:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private ongoingCallDurationSubscription:Lrx/Subscription;

.field private stateSubscription:Lrx/Subscription;

.field private final statusIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subtitleText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final titleText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unjoinedCallDuration$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unjoinedOngoingCallSubtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final usersAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    const/4 v1, 0x6

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string/jumbo v3, "statusIcon"

    const-string v4, "getStatusIcon()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/s;

    const-string/jumbo v6, "titleText"

    const-string v7, "getTitleText()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Lx/m/c/s;

    const-string/jumbo v6, "subtitleText"

    const-string v7, "getSubtitleText()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x3

    new-instance v4, Lx/m/c/s;

    const-string/jumbo v6, "unjoinedOngoingCallSubtitle"

    const-string v7, "getUnjoinedOngoingCallSubtitle()Landroid/view/View;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x4

    new-instance v4, Lx/m/c/s;

    const-string/jumbo v6, "unjoinedCallDuration"

    const-string v7, "getUnjoinedCallDuration()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x5

    new-instance v4, Lx/m/c/s;

    const-string v6, "callParticipantsRecycler"

    const-string v7, "getCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 2

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01b8

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const p1, 0x7f0a023c

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->statusIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a023f

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->titleText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a023e

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->subtitleText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0240

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->unjoinedOngoingCallSubtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a025a

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->unjoinedCallDuration$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a023d

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->callParticipantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->clock:Lcom/discord/utilities/time/Clock;

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->usersAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    return-void
.end method

.method public static final synthetic access$clearSubscriptions(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->clearSubscriptions()V

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Lcom/discord/utilities/time/Clock;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$getMinWidthPxForTime(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Ljava/lang/String;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getMinWidthPxForTime(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getOngoingCallDurationSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->ongoingCallDurationSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getStateSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->stateSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getSubtitleText$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Landroid/widget/TextView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getUnjoinedCallDuration$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)Landroid/widget/TextView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getUnjoinedCallDuration()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleState(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->handleState(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;)V

    return-void
.end method

.method public static final synthetic access$onItemClick(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->onItemClick(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;J)V

    return-void
.end method

.method public static final synthetic access$resetCurrentChatListEntry(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->resetCurrentChatListEntry()V

    return-void
.end method

.method public static final synthetic access$setOngoingCallDurationSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->ongoingCallDurationSubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setStateSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->stateSubscription:Lrx/Subscription;

    return-void
.end method

.method private final clearSubscriptions()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->ongoingCallDurationSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->stateSubscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    return-void
.end method

.method private final configure(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 9

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->chatListEntry:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.chat.list.entries.MessageEntry"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->clearSubscriptions()V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->observeState(Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    const/4 v2, 0x0

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configure$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configure$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configure$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configure$2;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)V

    const/16 v7, 0x1a

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final configureSubtitle(Lcom/discord/widgets/chat/list/entries/MessageEntry;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/res/Resources;Landroid/content/Context;)V
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->ongoingCallDurationSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getCallDuration()J

    move-result-wide v0

    invoke-static {p4, v0, v1}, Lcom/discord/utilities/duration/DurationUtilsKt;->humanizeDuration(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "subtitleText.context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getTimestamp()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/discord/utilities/time/TimeUtils;->toReadableTimeString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getUnjoinedOngoingCallSubtitle()Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->ACTIVE_UNJOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne p2, v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    const/16 v5, 0x8

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getUnjoinedOngoingCallSubtitle()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    xor-int/2addr v2, v3

    if-eqz v2, :cond_4

    const/4 v5, 0x0

    :cond_4
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    if-eqz p2, :cond_7

    if-eq p2, v3, :cond_7

    const p1, 0x7f1203b3

    const/4 v1, 0x2

    if-eq p2, v1, :cond_6

    const/4 v2, 0x3

    if-eq p2, v2, :cond_5

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object p2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p4, v1, v4

    aput-object v0, v1, v3

    invoke-virtual {p3, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object p2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p4, v1, v4

    aput-object v0, v1, v3

    invoke-virtual {p3, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getTimestampMilliseconds()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_8

    const-string p2, "message.timestampMilliseconds ?: return"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    const-wide/16 p3, 0x0

    const-wide/16 v0, 0x1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p3, p4, v0, v1, v2}, Lrx/Observable;->A(JJLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p3

    const-string p4, "Observable\n            .\u20260L, 1L, TimeUnit.SECONDS)"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;

    const/4 v2, 0x0

    new-instance v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;

    invoke-direct {v6, p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;J)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$configureSubtitle$2;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;)V

    const/16 v7, 0x1a

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_8
    :goto_3
    return-void
.end method

.method private final createCallParticipantUsers(Ljava/util/Map;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Lcom/discord/models/domain/ModelUser;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/channels/list/items/CollapsedUser;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->INACTIVE_JOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    if-eq p2, v0, :cond_6

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->INACTIVE_UNJOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    if-ne p2, v0, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    const/4 v0, 0x0

    if-eqz p3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    move-object v1, p3

    check-cast v1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v0, :cond_1

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v1, p1, -0x3

    const/4 v2, 0x3

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {v0, p1}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lx/h/o;

    invoke-virtual {v0}, Lx/h/o;->nextInt()I

    move-result v0

    new-instance v9, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    if-lez v1, :cond_5

    sget-object p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;->Companion:Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;

    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;->createEmptyUser(J)Lcom/discord/widgets/channels/list/items/CollapsedUser;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    return-object p3

    :cond_6
    :goto_2
    new-instance p1, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getCallDrawable(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const v0, 0x7f080285

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    const/4 v3, 0x1

    if-eq p1, v3, :cond_2

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x3

    if-ne p1, v3, :cond_0

    invoke-static {p2, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_3

    const v0, 0x7f040158

    invoke-static {p2, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p2

    invoke-static {p1, p2, v1}, Lcom/discord/utilities/color/ColorCompatKt;->setTint(Landroid/graphics/drawable/Drawable;IZ)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const p1, 0x7f080286

    invoke-static {p2, p1}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_3

    const v0, 0x7f06022b

    invoke-static {p2, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-static {p1, p2, v1}, Lcom/discord/utilities/color/ColorCompatKt;->setTint(Landroid/graphics/drawable/Drawable;IZ)V

    :goto_0
    move-object v2, p1

    goto :goto_1

    :cond_2
    invoke-static {p2, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_3

    const v0, 0x7f0601f3

    invoke-static {p2, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    invoke-static {p1, p2, v1}, Lcom/discord/utilities/color/ColorCompatKt;->setTint(Landroid/graphics/drawable/Drawable;IZ)V

    goto :goto_0

    :cond_3
    :goto_1
    return-object v2
.end method

.method private final getCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->callParticipantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getCallStatus(Lcom/discord/models/domain/ModelMessage;Ljava/util/Map;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelMessage;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;)",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getCall()Lcom/discord/models/domain/ModelMessage$Call;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getUserId()J

    move-result-wide v0

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v6}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v6

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance p2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {p2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const-string v2, "call"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage$Call;->getParticipants()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage$Call;->getEndedTimestamp()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    if-eqz p2, :cond_5

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->ACTIVE_JOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    goto :goto_4

    :cond_5
    if-eqz v4, :cond_6

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->ACTIVE_UNJOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    goto :goto_4

    :cond_6
    if-eqz v2, :cond_7

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->INACTIVE_JOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    goto :goto_4

    :cond_7
    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;->INACTIVE_UNJOINED:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    :goto_4
    return-object p1
.end method

.method private final getMinWidthPxForTime(Ljava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    const/16 v4, 0x3a

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_1

    add-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p1, v2

    const v0, 0x40f3cf3d

    const v1, 0x40430c31

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(F)I

    move-result v0

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(F)I

    move-result v1

    mul-int p1, p1, v0

    mul-int v2, v2, v1

    add-int/2addr v2, p1

    return v2
.end method

.method private final getStatusIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->statusIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getSubtitleText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->subtitleText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTitleString(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const p1, 0x7f1203b2

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getString(R.string.call_ended)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const p1, 0x7f121064

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getString(R.string.missed_call)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const p1, 0x7f1211f2

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getString(R.string.ongoing_call)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final getTitleText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->titleText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUnjoinedCallDuration()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->unjoinedCallDuration$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUnjoinedOngoingCallSubtitle()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->unjoinedOngoingCallSubtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final handleState(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;)V
    .locals 6

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->getMessageEntry()Lcom/discord/widgets/chat/list/entries/MessageEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;->getVoiceParticipants()Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getCallStatus(Lcom/discord/models/domain/ModelMessage;Ljava/util/Map;)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    const-string v4, "message.author"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->createCallParticipantUsers(Ljava/util/Map;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Lcom/discord/models/domain/ModelUser;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string/jumbo v4, "subtitleText.resources"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getSubtitleText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "subtitleText.context"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->configureSubtitle(Lcom/discord/widgets/chat/list/entries/MessageEntry;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/res/Resources;Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getStatusIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getStatusIcon()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "statusIcon.context"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getCallDrawable(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getTitleText()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getTitleText()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string/jumbo v4, "titleText.resources"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->getTitleString(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$handleState$1;

    invoke-direct {v3, p0, v2, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$handleState$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->usersAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-static {p1}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method

.method private final observeState(Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/list/entries/MessageEntry;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$State;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceParticipants()Lcom/discord/stores/StoreVoiceParticipants;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreVoiceParticipants;->get(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$observeState$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$observeState$1;-><init>(Lcom/discord/widgets/chat/list/entries/MessageEntry;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "StoreStream.getVoicePart\u2026, messageEntry)\n        }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onItemClick(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object v0

    invoke-interface {v0, p2, p3, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;->onCallMessageClicked(JLcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;)V

    return-void
.end method

.method private final resetCurrentChatListEntry()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->chatListEntry:Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->configure(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    invoke-direct {p0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->configure(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->clearSubscriptions()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage;->resetCurrentChatListEntry()V

    return-void
.end method
