.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model$Companion;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemListenTogether.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$create(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model$Companion;Lcom/discord/models/domain/ModelPresence;Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;Z)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model$Companion;->create(Lcom/discord/models/domain/ModelPresence;Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;Z)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;

    move-result-object p0

    return-object p0
.end method

.method private final create(Lcom/discord/models/domain/ModelPresence;Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;Z)Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelPresence;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;",
            "Z)",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;"
        }
    .end annotation

    move-object/from16 v0, p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/discord/models/domain/ModelUser;

    new-instance v3, Lcom/discord/widgets/channels/list/items/CollapsedUser;

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x0

    move-object v4, v3

    invoke-direct/range {v4 .. v10}, Lcom/discord/widgets/channels/list/items/CollapsedUser;-><init>(Lcom/discord/models/domain/ModelUser;ZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelPresence;->getListeningActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    const-wide/16 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivityParty;->getMaxSize()J

    move-result-wide v5

    goto :goto_2

    :cond_2
    move-wide v5, v3

    :goto_2
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->size()I

    move-result v2

    const-wide/16 v7, 0x6

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    invoke-static {v2, v9, v10}, Lx/p/e;->until(IJ)Lkotlin/ranges/LongRange;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    move-object v9, v2

    check-cast v9, Lx/h/p;

    invoke-virtual {v9}, Lx/h/p;->nextLong()J

    move-result-wide v9

    sget-object v11, Lcom/discord/widgets/channels/list/items/CollapsedUser;->Companion:Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;

    const-wide/16 v12, 0x5

    cmp-long v14, v9, v12

    if-nez v14, :cond_3

    sub-long v9, v5, v7

    goto :goto_4

    :cond_3
    move-wide v9, v3

    :goto_4
    invoke-virtual {v11, v9, v10}, Lcom/discord/widgets/channels/list/items/CollapsedUser$Companion;->createEmptyUser(J)Lcom/discord/widgets/channels/list/items/CollapsedUser;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    new-instance v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;

    move-object/from16 v3, p3

    move/from16 v4, p4

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;-><init>(Lcom/discord/models/domain/ModelPresence;Ljava/util/List;Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;Z)V

    return-object v2
.end method


# virtual methods
.method public final get(Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;",
            ">;"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->getUserId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreUserPresence;->observePresenceForUser(J)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGameParty()Lcom/discord/stores/StoreGameParty;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage$Activity;->getPartyId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/stores/StoreGameParty;->getUsersForPartyId(Ljava/lang/String;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v0

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model$Companion$get$1;

    invoke-direct {v3, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model$Companion$get$1;-><init>(Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;)V

    invoke-static {v1, v2, v0, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n             \u2026user?.id)\n              }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n             \u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
