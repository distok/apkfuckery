.class public final Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;
.super Landroid/widget/LinearLayout;
.source "ViewEmbedGameInvite.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;,
        Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Companion;

.field private static final EMBED_LIFETIME_MILLIS:J = 0x6ddd00L

.field private static final MAX_USERS_SHOWN:J = 0x4L


# instance fields
.field private final actionBtn:Lcom/google/android/material/button/MaterialButton;

.field private final applicationNameTv:Landroid/widget/TextView;

.field private final avatarIv:Lcom/facebook/drawee/view/SimpleDraweeView;

.field private final avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

.field private final coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

.field private final headerTv:Landroid/widget/TextView;

.field private onActionButtonClick:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/view/View;",
            "-",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final subtextTv:Landroid/widget/TextView;

.field private final userAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

.field private final usersRv:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->Companion:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0d011f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const p1, 0x7f0a05f1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_game_invite_header)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->headerTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f3

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_game_invite_subtext)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->subtextTv:Landroid/widget/TextView;

    const p1, 0x7f0a05ec

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_g\u2026vite_application_name_tv)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->applicationNameTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_game_invite_cover_iv)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ed

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_game_invite_avatar_iv)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ee

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_g\u2026_invite_avatar_status_iv)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05f2

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById(R.id.item_game_invite_recycler)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->usersRv:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a05eb

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.item_game_invite_action_btn)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    iput-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-direct {v1, p1}, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->userAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d011f

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const p1, 0x7f0a05f1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_header)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->headerTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f3

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_subtext)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->subtextTv:Landroid/widget/TextView;

    const p1, 0x7f0a05ec

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_g\u2026vite_application_name_tv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->applicationNameTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_cover_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ed

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_avatar_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ee

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_g\u2026_invite_avatar_status_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05f2

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_recycler)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->usersRv:Landroidx/recyclerview/widget/RecyclerView;

    const p2, 0x7f0a05eb

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "findViewById(R.id.item_game_invite_action_btn)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/google/android/material/button/MaterialButton;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    sget-object p2, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-direct {v0, p1}, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p2, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->userAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d011f

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const p1, 0x7f0a05f1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_header)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->headerTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f3

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_subtext)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->subtextTv:Landroid/widget/TextView;

    const p1, 0x7f0a05ec

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_g\u2026vite_application_name_tv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->applicationNameTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_cover_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ed

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_avatar_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ee

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_g\u2026_invite_avatar_status_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05f2

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_recycler)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->usersRv:Landroidx/recyclerview/widget/RecyclerView;

    const p2, 0x7f0a05eb

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.item_game_invite_action_btn)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/google/android/material/button/MaterialButton;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    sget-object p2, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance p3, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-direct {p3, p1}, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p2, p3}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->userAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d011f

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const p1, 0x7f0a05f1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_header)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->headerTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f3

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_subtext)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->subtextTv:Landroid/widget/TextView;

    const p1, 0x7f0a05ec

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_g\u2026vite_application_name_tv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->applicationNameTv:Landroid/widget/TextView;

    const p1, 0x7f0a05f0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_cover_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ed

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_avatar_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05ee

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_g\u2026_invite_avatar_status_iv)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p1, 0x7f0a05f2

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.item_game_invite_recycler)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->usersRv:Landroidx/recyclerview/widget/RecyclerView;

    const p2, 0x7f0a05eb

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "findViewById(R.id.item_game_invite_action_btn)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/google/android/material/button/MaterialButton;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    sget-object p2, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance p3, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-direct {p3, p1}, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p2, p3}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->userAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    return-void
.end method

.method private final configureActivityImages(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Z)V
    .locals 20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x0

    if-eqz v5, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v3

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon$default(JLjava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v10

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7c

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v10

    :goto_1
    iget-object v2, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getSmallImage()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_2
    move-object v3, v10

    :goto_2
    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    :goto_3
    const/16 v6, 0x8

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    goto :goto_4

    :cond_4
    const/16 v3, 0x8

    :goto_4
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v11, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getSmallImage()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_5

    sget-object v12, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const-string v2, "it"

    invoke-static {v14, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    invoke-static/range {v12 .. v17}, Lcom/discord/utilities/icon/IconUtils;->getAssetImage$default(Lcom/discord/utilities/icon/IconUtils;Ljava/lang/Long;Ljava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    goto :goto_5

    :cond_5
    move-object v12, v10

    :goto_5
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x7c

    const/16 v19, 0x0

    invoke-static/range {v11 .. v19}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object v2, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->avatarStatusIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getSmallText()Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    :cond_6
    move-object v3, v10

    :goto_6
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_7

    iget-object v1, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x7c

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void

    :cond_7
    iget-object v2, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getLargeImage()Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    :cond_8
    move-object v2, v10

    :goto_7
    if-eqz v2, :cond_9

    iget-object v3, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getLargeText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    sget-object v1, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    invoke-static {v4}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v4

    invoke-virtual {v1, v3, v2, v4}, Lcom/discord/utilities/icon/IconUtils;->getAssetImage(Ljava/lang/Long;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    iget-object v5, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x7c

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    goto :goto_8

    :cond_9
    iget-object v1, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    iget-object v1, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getCoverImage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v2

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon$default(JLjava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    :cond_a
    move-object v2, v10

    iget-object v1, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->coverIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7c

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :goto_8
    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/utilities/time/Clock;)V
    .locals 6

    invoke-interface {p2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->isDeadInvite(J)Z

    move-result p2

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->headerTv:Landroid/widget/TextView;

    const v1, 0x7f120e1b

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getMessageActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage$Activity;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const v1, 0x7f120e1f

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->applicationNameTv:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-wide/16 v0, 0x0

    if-eqz p2, :cond_3

    :cond_2
    move-wide v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivityParty;->getOpenSlots()J

    move-result-wide v2

    :goto_1
    const/4 v4, 0x0

    cmp-long v5, v2, v0

    if-gtz v5, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->subtextTv:Landroid/widget/TextView;

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_5
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v2

    :cond_6
    :goto_3
    invoke-static {v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->configureActivityImages(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Z)V

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->onConfigureActionButton(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;ZZ)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->usersRv:Landroidx/recyclerview/widget/RecyclerView;

    xor-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_7

    goto :goto_4

    :cond_7
    const/16 v4, 0x8

    :goto_4
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    if-nez p2, :cond_8

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->userAdapter:Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getUsers()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    :cond_8
    return-void
.end method

.method private final onConfigureActionButton(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;ZZ)V
    .locals 8

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f120e8f

    if-nez p2, :cond_6

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getCanJoin()Z

    move-result p2

    if-nez p2, :cond_0

    goto/16 :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->isGameInstalled()Z

    move-result p2

    const/4 v2, 0x1

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->isInParty()Z

    move-result v3

    if-nez v3, :cond_1

    if-nez p3, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getCreatorId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-eqz v7, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {p2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    if-eqz p3, :cond_2

    const v0, 0x7f120e1a

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->isInParty()Z

    move-result p3

    if-eqz p3, :cond_3

    const v0, 0x7f120e26

    :cond_3
    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/Button;->setText(I)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    new-instance p3, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$onConfigureActionButton$1;

    invoke-direct {p3, p0, p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$onConfigureActionButton$1;-><init>(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getGPlayPackageNames()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v2

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    invoke-virtual {p2, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    const p3, 0x7f121923

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getGPlayPackageNames()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    new-instance p3, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$onConfigureActionButton$2;

    invoke-direct {p3, p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$onConfigureActionButton$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_5
    iget-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->actionBtn:Lcom/google/android/material/button/MaterialButton;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final bind(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string v0, "model"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->configureUI(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/utilities/time/Clock;)V

    return-void
.end method

.method public final getOnActionButtonClick()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Landroid/view/View;",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->onActionButtonClick:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final setOnActionButtonClick(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/view/View;",
            "-",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->onActionButtonClick:Lkotlin/jvm/functions/Function2;

    return-void
.end method
