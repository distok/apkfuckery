.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;
.super Ljava/lang/Object;
.source "WidgetChatListAdapter.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "HandlerOfTouches"
.end annotation


# instance fields
.field private final tapGestureDetector:Landroid/view/GestureDetector;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches$tapGestureDetector$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches$tapGestureDetector$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;->tapGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 p1, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->access$isTouchedSinceLastJump$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->access$setTouchedSinceLastJump$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->access$publishInteractionState(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$HandlerOfTouches;->tapGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method
