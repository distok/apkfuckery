.class public final Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;
.super Ljava/lang/Object;
.source "InlineMediaView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $mediaSource:Lcom/discord/player/MediaSource;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/InlineMediaView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/MediaSource;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->$mediaSource:Lcom/discord/player/MediaSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 10

    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getPlayerView$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getAppMediaPlayer$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Lcom/discord/player/AppMediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->$mediaSource:Lcom/discord/player/MediaSource;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getPlayerView$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x28

    invoke-static/range {v1 .. v9}, Lcom/discord/player/AppMediaPlayer;->b(Lcom/discord/player/AppMediaPlayer;Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;I)V

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getPlayButton$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/widget/ImageView;

    move-result-object p1

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$5;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getLoadingIndicator$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
