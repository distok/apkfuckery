.class public final Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$4;
.super Lx/m/c/k;
.source "InlineMediaView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/player/AppMediaPlayer$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/InlineMediaView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/InlineMediaView;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$4;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/player/AppMediaPlayer$Event;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$4;->invoke(Lcom/discord/player/AppMediaPlayer$Event;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/player/AppMediaPlayer$Event;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$4;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$handlePlayerEvent(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/AppMediaPlayer$Event;)V

    return-void
.end method
