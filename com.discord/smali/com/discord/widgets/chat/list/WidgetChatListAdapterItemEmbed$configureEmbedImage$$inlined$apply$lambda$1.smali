.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemEmbed.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage(Lcom/discord/models/domain/ModelMessageEmbed;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

.field public final synthetic $this_apply:Lcom/discord/models/domain/ModelMessageEmbed;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelMessageEmbed;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/models/domain/ModelMessageEmbed;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/models/domain/ModelMessageEmbed;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;

    iput-object p3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->getExternallyOpen()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    const-string/jumbo v2, "view"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1, v2, v1}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageEmbed;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/utilities/uri/UriHandler;->handleOrUntrusted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/media/WidgetMedia;->Companion:Lcom/discord/widgets/media/WidgetMedia$Companion;

    invoke-static {p1, v2, v1}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageEmbed;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/discord/widgets/media/WidgetMedia$Companion;->launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/ModelMessageEmbed;)V

    :goto_0
    return-void
.end method
