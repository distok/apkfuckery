.class public final Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;
.super Ljava/lang/Object;
.source "ViewEmbedGameInvite.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->getForShare(Landroid/content/Context;Lcom/discord/utilities/time/Clock;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/models/domain/activity/ModelActivity;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $applicationId:Ljava/lang/Long;

.field public final synthetic $partyId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;->$applicationId:Ljava/lang/Long;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;->$partyId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;->call(Ljava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Long;)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v0

    const-string v1, "authorId"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;->$applicationId:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreUserPresence;->observeApplicationActivity(JJ)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1$1;-><init>(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
