.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemEmbed.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureInlineEmbed(Lcom/discord/models/domain/ModelMessageEmbed;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

.field public final synthetic $this_apply:Lcom/discord/widgets/chat/list/InlineMediaView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/models/domain/ModelMessageEmbed;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/widgets/chat/list/InlineMediaView;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    sget-object p1, Lcom/discord/widgets/media/WidgetMedia;->Companion:Lcom/discord/widgets/media/WidgetMedia$Companion;

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "this.context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageEmbed;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;->$embed$inlined:Lcom/discord/models/domain/ModelMessageEmbed;

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/widgets/media/WidgetMedia$Companion;->launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/ModelMessageEmbed;)V

    return-void
.end method
