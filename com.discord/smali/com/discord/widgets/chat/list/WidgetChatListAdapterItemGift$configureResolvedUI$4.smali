.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$configureResolvedUI$4;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemGift.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift;->configureResolvedUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Model$Resolved;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Model$Resolved;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Model$Resolved;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$configureResolvedUI$4;->$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$configureResolvedUI$4;->$model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Model$Resolved;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    sget-object v0, Lcom/discord/utilities/uri/UriHandler;->INSTANCE:Lcom/discord/utilities/uri/UriHandler;

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$configureResolvedUI$4;->$context:Landroid/content/Context;

    const-string p1, "context"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Companion;

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$configureResolvedUI$4;->$model:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Model$Resolved;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Model$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGift;->getSkuId()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Companion;->access$getDiscordStoreURL(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemGift$Companion;J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/uri/UriHandler;->handle$default(Lcom/discord/utilities/uri/UriHandler;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method
