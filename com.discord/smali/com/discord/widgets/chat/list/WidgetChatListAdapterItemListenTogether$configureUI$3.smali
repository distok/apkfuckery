.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$configureUI$3;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemListenTogether.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether;->configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $listeningActivity:Lcom/discord/models/domain/activity/ModelActivity;

.field public final synthetic $this_configureUI:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;Lcom/discord/models/domain/activity/ModelActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$configureUI$3;->$this_configureUI:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$configureUI$3;->$listeningActivity:Lcom/discord/models/domain/activity/ModelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    sget-object v0, Lcom/discord/utilities/integrations/SpotifyHelper;->INSTANCE:Lcom/discord/utilities/integrations/SpotifyHelper;

    const-string v1, "it"

    const-string v2, "it.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$configureUI$3;->$listeningActivity:Lcom/discord/models/domain/activity/ModelActivity;

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$configureUI$3;->$this_configureUI:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;->getItem()Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ListenTogetherEntry;->getUserId()J

    move-result-wide v3

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$configureUI$3;->$this_configureUI:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemListenTogether$Model;->isMe()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/integrations/SpotifyHelper;->launchAlbum(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;JZ)V

    return-void
.end method
