.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemUploadProgress.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$ModelProvider;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Companion;

.field private static final MODEL_THROTTLE_MS:J = 0x64L


# instance fields
.field private final cancelButtonCentered:Landroid/view/View;

.field private final cancelButtonTop:Landroid/view/View;

.field private subscription:Lrx/Subscription;

.field private final uploadProgressView1:Lcom/discord/views/UploadProgressView;

.field private final uploadProgressView2:Lcom/discord/views/UploadProgressView;

.field private final uploadProgressView3:Lcom/discord/views/UploadProgressView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01d4

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0ae2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.upload_progress_1)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/UploadProgressView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView1:Lcom/discord/views/UploadProgressView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0ae3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.upload_progress_2)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/UploadProgressView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView2:Lcom/discord/views/UploadProgressView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0ae4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.upload_progress_3)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/UploadProgressView;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView3:Lcom/discord/views/UploadProgressView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0800

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.progress_cancel_top)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonTop:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a07ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026progress_cancel_centered)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonCentered:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V

    return-void
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->subscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->subscription:Lrx/Subscription;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V
    .locals 7

    instance-of v0, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-nez v0, :cond_0

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView1:Lcom/discord/views/UploadProgressView;

    invoke-direct {p0, v4, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->setUploadState(Lcom/discord/views/UploadProgressView;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView2:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView3:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView1:Lcom/discord/views/UploadProgressView;

    move-object v5, p1

    check-cast v5, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;

    invoke-virtual {v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;->getUploads()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;

    invoke-direct {p0, v4, v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->setUploadState(Lcom/discord/views/UploadProgressView;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView2:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;->getUploads()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;

    invoke-direct {p0, v4, v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->setUploadState(Lcom/discord/views/UploadProgressView;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView2:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;->getUploads()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v6, 0x3

    if-ne v4, v6, :cond_1

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView3:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView3:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;->getUploads()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;

    invoke-direct {p0, v4, v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->setUploadState(Lcom/discord/views/UploadProgressView;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->uploadProgressView3:Lcom/discord/views/UploadProgressView;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    instance-of v4, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;

    if-nez v4, :cond_9

    instance-of v4, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;

    const/16 v5, 0x64

    if-eqz v4, :cond_2

    move-object v4, p1

    check-cast v4, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getProgress()I

    move-result v4

    if-lt v4, v5, :cond_9

    :cond_2
    if-eqz v0, :cond_7

    move-object v4, p1

    check-cast v4, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Few;->getUploads()Ljava/util/List;

    move-result-object v4

    instance-of v6, v4, Ljava/util/Collection;

    if-eqz v6, :cond_4

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getProgress()I

    move-result v6

    if-ge v6, v5, :cond_6

    const/4 v6, 0x1

    goto :goto_1

    :cond_6
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_5

    const/4 v4, 0x1

    :goto_2
    if-nez v4, :cond_9

    :cond_7
    instance-of v4, p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    if-eqz v4, :cond_8

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->getProgress()I

    move-result p1

    if-ge p1, v5, :cond_8

    goto :goto_3

    :cond_8
    const/4 p1, 0x0

    goto :goto_4

    :cond_9
    :goto_3
    const/4 p1, 0x1

    :goto_4
    if-eqz p1, :cond_c

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonTop:Landroid/view/View;

    if-eqz v0, :cond_a

    const/4 v4, 0x0

    goto :goto_5

    :cond_a
    const/16 v4, 0x8

    :goto_5
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonCentered:Landroid/view/View;

    xor-int/2addr v0, v1

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    :cond_b
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :cond_c
    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonTop:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonCentered:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_6
    return-void
.end method

.method private final setUploadState(Lcom/discord/views/UploadProgressView;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model;)V
    .locals 12

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$None;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$None;

    invoke-static {p2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    const v2, 0x7f040315

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "context"

    if-eqz v0, :cond_0

    const p2, 0x7f12190e

    invoke-static {p1, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p2

    sget v0, Lcom/discord/views/UploadProgressView;->j:I

    invoke-virtual {p1, p2, v4, v3}, Lcom/discord/views/UploadProgressView;->a(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v2, v4, v1, v3}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/UploadProgressView;->setIcon(I)V

    goto/16 :goto_3

    :cond_0
    instance-of v0, p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;

    const/4 v6, 0x1

    const v7, 0x7f10010f

    const-string v8, "resources"

    const/4 v9, -0x1

    if-eqz v0, :cond_3

    check-cast p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;->getNumFiles()I

    move-result v10

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;->getNumFiles()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v6, v4

    invoke-static {v0, v8, v7, v10, v6}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget v6, Lcom/discord/views/UploadProgressView;->j:I

    invoke-virtual {p1, v0, v9, v3}, Lcom/discord/views/UploadProgressView;->a(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;->getMimeType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;->getMimeType()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/discord/utilities/file/FileUtilsKt;->getIconForFiletype(Landroid/content/Context;Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/UploadProgressView;->setIcon(I)V

    goto/16 :goto_3

    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v2, v4, v1, v3}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/UploadProgressView;->setIcon(I)V

    goto/16 :goto_3

    :cond_3
    instance-of v0, p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;

    if-eqz v0, :cond_5

    check-cast p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getProgress()I

    move-result v1

    if-ne v1, v9, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getProgress()I

    move-result v9

    :goto_1
    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getSizeBytes()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/discord/utilities/file/FileUtilsKt;->getSizeSubtitle(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v9, v1}, Lcom/discord/views/UploadProgressView;->a(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Single;->getMimeType()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/discord/utilities/file/FileUtilsKt;->getIconForFiletype(Landroid/content/Context;Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/UploadProgressView;->setIcon(I)V

    goto :goto_3

    :cond_5
    instance-of v0, p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->getNumFiles()I

    move-result v10

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->getNumFiles()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v6, v4

    invoke-static {v0, v8, v7, v10, v6}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->getProgress()I

    move-result v6

    if-ne v6, v9, :cond_6

    goto :goto_2

    :cond_6
    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->getProgress()I

    move-result v9

    :goto_2
    invoke-virtual {p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$Model$Many;->getSizeBytes()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/discord/utilities/file/FileUtilsKt;->getSizeSubtitle(J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, v9, p2}, Lcom/discord/views/UploadProgressView;->a(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v2, v4, v1, v3}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/UploadProgressView;->setIcon(I)V

    :cond_7
    :goto_3
    return-void
.end method


# virtual methods
.method public getSubscription()Lrx/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->subscription:Lrx/Subscription;

    return-object v0
.end method

.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 10

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    move-object p1, p2

    check-cast p1, Lcom/discord/widgets/chat/list/entries/UploadProgressEntry;

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$ModelProvider;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$ModelProvider;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/UploadProgressEntry;->getMessageNonce()Ljava/lang/String;

    move-result-object p1

    const-wide/16 v1, 0x64

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$ModelProvider;->get(Ljava/lang/String;J)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->H()Lrx/Observable;

    move-result-object p1

    const-string v0, "ModelProvider.get(data.m\u2026  .onBackpressureLatest()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;

    new-instance v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;)V

    new-instance v4, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$2;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;)V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1a

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$cancel$1;

    invoke-direct {p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$cancel$1;-><init>(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonTop:Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$3;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->cancelButtonCentered:Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$4;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress$onConfigure$4;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemUploadProgress;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method
