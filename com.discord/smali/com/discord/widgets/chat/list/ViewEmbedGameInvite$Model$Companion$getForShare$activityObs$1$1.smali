.class public final Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1$1;
.super Ljava/lang/Object;
.source "ViewEmbedGameInvite.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;->call(Ljava/lang/Long;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/activity/ModelActivity;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1$1;->this$0:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/Boolean;
    .locals 1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityParty;->getId()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1$1;->this$0:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;

    iget-object v0, v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1;->$partyId:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1$1;->call(Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
