.class public final Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$2;
.super Ljava/lang/Object;
.source "PublishActionDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/PublishActionDialog;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/PublishActionDialog;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/PublishActionDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$2;->this$0:Lcom/discord/widgets/chat/list/PublishActionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$2;->this$0:Lcom/discord/widgets/chat/list/PublishActionDialog;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/PublishActionDialog;->access$getViewModel$p(Lcom/discord/widgets/chat/list/PublishActionDialog;)Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/PublishActionDialogViewModel;->publishMessage()V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/PublishActionDialog$onViewBound$2;->this$0:Lcom/discord/widgets/chat/list/PublishActionDialog;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/PublishActionDialog;->access$getOnSuccess$p(Lcom/discord/widgets/chat/list/PublishActionDialog;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_0
    return-void
.end method
