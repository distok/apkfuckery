.class public Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemReactions.java"


# static fields
.field private static final REACTION_LIMIT:I = 0x14


# instance fields
.field private final quickAddReactionView:Landroid/view/View;

.field private reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 1

    const v0, 0x7f0d01c9

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0281

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/flexbox/FlexboxLayout;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->createQuickAddReactionView()Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->quickAddReactionView:Landroid/view/View;

    return-void
.end method

.method private createQuickAddReactionView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d00dc

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private displayReactions(Ljava/util/Collection;JZZ)V
    .locals 7
    .param p1    # Ljava/util/Collection;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/discord/models/domain/ModelMessageReaction;",
            ">;JZZ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->removeQuickAddReactionView()V

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelMessageReaction;

    if-ge v3, v0, :cond_2

    iget-object v5, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lf/a/n/a0;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    new-instance v5, Lf/a/n/a0;

    iget-object v6, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lf/a/n/a0;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_2
    new-instance v6, Lf/a/o/b/b/c0;

    invoke-direct {v6, p0, p2, p3, v4}, Lf/a/o/b/b/c0;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;JLcom/discord/models/domain/ModelMessageReaction;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lf/a/o/b/b/d0;

    invoke-direct {v6, p0, p2, p3, v4}, Lf/a/o/b/b/d0;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;JLcom/discord/models/domain/ModelMessageReaction;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v5, v4, p2, p3, p5}, Lf/a/n/a0;->a(Lcom/discord/models/domain/ModelMessageReaction;JZ)V

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    const/16 p5, 0x14

    if-ge p1, p5, :cond_4

    if-eqz p4, :cond_4

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->quickAddReactionView:Landroid/view/View;

    new-instance p4, Lf/a/o/b/b/b0;

    invoke-direct {p4, p0, p2, p3}, Lf/a/o/b/b/b0;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;J)V

    invoke-virtual {p1, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    iget-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->quickAddReactionView:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    return-void
.end method

.method private removeQuickAddReactionView()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->quickAddReactionView:Landroid/view/View;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->reactionsContainer:Lcom/google/android/flexbox/FlexboxLayout;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeViewAt(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 0
    .param p2    # Lcom/discord/widgets/chat/list/entries/ChatListEntry;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ReactionsEntry;

    invoke-virtual {p0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->processReactions(Lcom/discord/widgets/chat/list/entries/ReactionsEntry;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method

.method public processReactions(Lcom/discord/widgets/chat/list/entries/ReactionsEntry;)V
    .locals 7

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ReactionsEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getReactions()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ReactionsEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ReactionsEntry;->getCanAddReactions()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/entries/ReactionsEntry;->getAnimateEmojis()Z

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;->displayReactions(Ljava/util/Collection;JZZ)V

    return-void
.end method
