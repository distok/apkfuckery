.class public final Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetChatListActionsEmojisAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/chat/list/actions/EmojiItem;",
        ">;"
    }
.end annotation


# instance fields
.field private onClickEmoji:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/emoji/Emoji;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClickMoreEmojis:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object p1, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter$onClickEmoji$1;->INSTANCE:Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter$onClickEmoji$1;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onClickEmoji:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter$onClickMoreEmojis$1;->INSTANCE:Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter$onClickMoreEmojis$1;

    iput-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onClickMoreEmojis:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method private final getEmojiItems(Ljava/util/List;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/emoji/Emoji;",
            ">;I)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/list/actions/EmojiItem;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-gtz p2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p2, p2, -0x1

    invoke-static {p1, p2}, Lx/h/f;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/emoji/Emoji;

    new-instance v1, Lcom/discord/widgets/chat/list/actions/EmojiItem$EmojiData;

    invoke-direct {v1, v0}, Lcom/discord/widgets/chat/list/actions/EmojiItem$EmojiData;-><init>(Lcom/discord/models/domain/emoji/Emoji;)V

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lx/h/f;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;->INSTANCE:Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;

    move-object v0, p1

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p1

    :cond_2
    :goto_1
    sget-object p1, Lx/h/l;->d:Lx/h/l;

    return-object p1
.end method


# virtual methods
.method public final getOnClickEmoji()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/emoji/Emoji;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onClickEmoji:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClickMoreEmojis()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onClickMoreEmojis:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/widgets/chat/list/actions/EmojiItem;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;-><init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/chat/list/actions/EmojiViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/list/actions/EmojiViewHolder;-><init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;)V

    :goto_0
    return-object p1
.end method

.method public final setData(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/emoji/Emoji;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "emojis"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->getEmojiItems(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method

.method public final setOnClickEmoji(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/emoji/Emoji;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onClickEmoji:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnClickMoreEmojis(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;->onClickMoreEmojis:Lkotlin/jvm/functions/Function0;

    return-void
.end method
