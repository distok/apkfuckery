.class public abstract Lcom/discord/widgets/chat/list/actions/EmojiItem;
.super Ljava/lang/Object;
.source "WidgetChatListActionsEmojisAdapter.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/actions/EmojiItem$EmojiData;,
        Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;,
        Lcom/discord/widgets/chat/list/actions/EmojiItem$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/list/actions/EmojiItem$Companion;

.field public static final TYPE_EMOJI:I = 0x0

.field public static final TYPE_MORE_EMOJIS:I = 0x1


# instance fields
.field private final _type:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/list/actions/EmojiItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/list/actions/EmojiItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/list/actions/EmojiItem;->Companion:Lcom/discord/widgets/chat/list/actions/EmojiItem$Companion;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/widgets/chat/list/actions/EmojiItem;->_type:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/actions/EmojiItem;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/list/actions/EmojiItem;->_type:I

    return v0
.end method
