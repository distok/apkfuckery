.class public final Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$configureUI$3;
.super Ljava/lang/Object;
.source "WidgetChatListActions.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->configureUI(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$configureUI$3;->this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    sget-object v0, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions;->Companion:Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;

    iget-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$configureUI$3;->this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->access$getChannelId$p(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;)J

    move-result-wide v1

    iget-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$configureUI$3;->this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->access$getMessageId$p(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;)J

    move-result-wide v3

    iget-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$configureUI$3;->this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    const-string p1, "requireContext()"

    invoke-static {v5, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;->create$default(Lcom/discord/widgets/chat/managereactions/WidgetManageReactions$Companion;JJLandroid/content/Context;Lcom/discord/models/domain/ModelMessageReaction;ILjava/lang/Object;)V

    return-void
.end method
