.class public final Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetChatListActionsEmojisAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;",
        "Lcom/discord/widgets/chat/list/actions/EmojiItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d0121

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;)Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;

    return-object p0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/chat/list/actions/EmojiItem;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance p2, Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder$onConfigure$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder$onConfigure$1;-><init>(Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/actions/EmojiItem;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;->onConfigure(ILcom/discord/widgets/chat/list/actions/EmojiItem;)V

    return-void
.end method
