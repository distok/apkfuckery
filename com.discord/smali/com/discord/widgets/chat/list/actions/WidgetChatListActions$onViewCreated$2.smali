.class public final Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2;
.super Lx/m/c/k;
.source "WidgetChatListActions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2;->this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2;->this$0:Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2$1;-><init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2;)V

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;->CHAT:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    new-instance v3, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2$2;-><init>(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$onViewCreated$2;)V

    invoke-static {v0, v1, v2, v3}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->launchBottomSheet(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
