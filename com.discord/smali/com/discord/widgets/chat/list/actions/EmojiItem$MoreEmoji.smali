.class public final Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;
.super Lcom/discord/widgets/chat/list/actions/EmojiItem;
.source "WidgetChatListActionsEmojisAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/actions/EmojiItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoreEmoji"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;

    invoke-direct {v0}, Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;->INSTANCE:Lcom/discord/widgets/chat/list/actions/EmojiItem$MoreEmoji;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/chat/list/actions/EmojiItem;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/list/actions/EmojiItem;->getType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
