.class public final Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;
.super Lx/m/c/k;
.source "InlineMediaView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithValidatedEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Float;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $player:Lcom/discord/player/AppMediaPlayer;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/InlineMediaView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/player/AppMediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->$player:Lcom/discord/player/AppMediaPlayer;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->invoke(F)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(F)V
    .locals 3

    const/4 v0, 0x0

    int-to-float v1, v0

    cmpl-float p1, p1, v1

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    const p1, 0x7f08049b

    goto :goto_0

    :cond_1
    const p1, 0x7f080499

    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {v1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getVolumeToggle$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {p1}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getVolumeToggle$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/widget/ImageView;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2$1;

    invoke-direct {v1, p0, v0}, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2$1;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;Z)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v0, :cond_2

    const p1, 0x7f121a33

    goto :goto_1

    :cond_2
    const p1, 0x7f121a34

    :goto_1
    iget-object v0, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {v0}, Lcom/discord/widgets/chat/list/InlineMediaView;->access$getVolumeToggle$p(Lcom/discord/widgets/chat/list/InlineMediaView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/list/InlineMediaView$updateUIWithValidatedEmbed$2;->this$0:Lcom/discord/widgets/chat/list/InlineMediaView;

    invoke-static {v1, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
