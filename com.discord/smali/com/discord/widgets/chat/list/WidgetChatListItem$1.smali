.class public final Lcom/discord/widgets/chat/list/WidgetChatListItem$1;
.super Ljava/lang/Object;
.source "WidgetChatListItem.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

.field private final tapGestureDetector:Landroid/view/GestureDetector;

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListItem;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListItem;Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapter;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListItem$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListItem;

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListItem$1;->$adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p2, Landroid/view/GestureDetector;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListItem$1$tapGestureDetector$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/list/WidgetChatListItem$1$tapGestureDetector$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListItem$1;)V

    invoke-direct {p2, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListItem$1;->tapGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListItem$1;->tapGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method
