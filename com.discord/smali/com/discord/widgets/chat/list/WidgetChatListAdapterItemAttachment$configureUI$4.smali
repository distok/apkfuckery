.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$configureUI$4;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemAttachment.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment;->configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $attachment:Lcom/discord/models/domain/ModelMessageAttachment;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelMessageAttachment;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$configureUI$4;->$attachment:Lcom/discord/models/domain/ModelMessageAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$Companion;

    const-string v1, "it"

    const-string v2, "it.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$configureUI$4;->$attachment:Lcom/discord/models/domain/ModelMessageAttachment;

    invoke-static {v0, p1, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$Companion;->access$navigateToAttachment(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemAttachment$Companion;Landroid/content/Context;Lcom/discord/models/domain/ModelMessageAttachment;)V

    return-void
.end method
