.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;
.super Lcom/discord/widgets/chat/list/WidgetChatListItem;
.source "WidgetChatListAdapterItemEmbed.kt"

# interfaces
.implements Lcom/discord/widgets/chat/list/FragmentLifecycleListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;,
        Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

.field private static final EMBED_TYPE_DESC:Ljava/lang/String; = "desc"

.field private static final EMBED_TYPE_FIELD_NAME:Ljava/lang/String; = "f_name"

.field private static final EMBED_TYPE_FIELD_VALUE:Ljava/lang/String; = "f_value"

.field private static final EMBED_TYPE_TITLE:Ljava/lang/String; = "title"

.field private static final MAX_IMAGE_VIEW_HEIGHT_PX:I

.field private static final UI_THREAD_TITLES_PARSER:Lcom/discord/simpleast/core/parser/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation
.end field

.field private static final UI_THREAD_VALUES_PARSER:Lcom/discord/simpleast/core/parser/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final containerCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedAuthorIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedAuthorText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedFields$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedFooterIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedFooterText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedImage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedImageContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedImageIcons$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedImageThumbnail$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedInlineMedia$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedProvider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedThumbnailMaxSize:I

.field private final embedTinyIconSize:I

.field private final embedTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final maxEmbedImageWidth:I

.field private final shouldRenderEmbeds:Z

.field private final spoilerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private subscription:Lrx/Subscription;

.field private final userSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    const-class v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;

    const/16 v1, 0x11

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string v3, "containerCard"

    const-string v4, "getContainerCard()Lcom/google/android/material/card/MaterialCardView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    new-instance v2, Lx/m/c/s;

    const-string v4, "embedProvider"

    const-string v6, "getEmbedProvider()Landroid/widget/TextView;"

    invoke-direct {v2, v0, v4, v6, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/4 v2, 0x2

    new-instance v6, Lx/m/c/s;

    const-string v7, "embedTitle"

    const-string v8, "getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;"

    invoke-direct {v6, v0, v7, v8, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v6, v1, v2

    const/4 v2, 0x3

    new-instance v6, Lx/m/c/s;

    const-string v7, "embedAuthorIcon"

    const-string v8, "getEmbedAuthorIcon()Landroid/widget/ImageView;"

    invoke-direct {v6, v0, v7, v8, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v6, v1, v2

    new-instance v2, Lx/m/c/s;

    const-string v6, "embedAuthorText"

    const-string v7, "getEmbedAuthorText()Landroid/widget/TextView;"

    invoke-direct {v2, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x4

    aput-object v2, v1, v6

    const/4 v2, 0x5

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedDescription"

    const-string v9, "getEmbedDescription()Lcom/discord/utilities/view/text/LinkifiedTextView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/4 v2, 0x6

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedFields"

    const-string v9, "getEmbedFields()Landroid/view/ViewGroup;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/4 v2, 0x7

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedContent"

    const-string v9, "getEmbedContent()Landroidx/constraintlayout/widget/ConstraintLayout;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0x8

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedImageContainer"

    const-string v9, "getEmbedImageContainer()Landroid/view/ViewGroup;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0x9

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedImage"

    const-string v9, "getEmbedImage()Landroid/widget/ImageView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0xa

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedImageIcons"

    const-string v9, "getEmbedImageIcons()Landroid/widget/ImageView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0xb

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedInlineMedia"

    const-string v9, "getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0xc

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedImageThumbnail"

    const-string v9, "getEmbedImageThumbnail()Landroid/widget/ImageView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0xd

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedDivider"

    const-string v9, "getEmbedDivider()Landroid/view/View;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0xe

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedFooterIcon"

    const-string v9, "getEmbedFooterIcon()Landroid/widget/ImageView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0xf

    new-instance v7, Lx/m/c/s;

    const-string v8, "embedFooterText"

    const-string v9, "getEmbedFooterText()Landroid/widget/TextView;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    const/16 v2, 0x10

    new-instance v7, Lx/m/c/s;

    const-string/jumbo v8, "spoilerView"

    const-string v9, "getSpoilerView()Landroid/widget/FrameLayout;"

    invoke-direct {v7, v0, v8, v9, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v7, v1, v2

    sput-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    const/16 v2, 0x168

    invoke-static {v2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v2

    sput v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->MAX_IMAGE_VIEW_HEIGHT_PX:I

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->createTitlesParser()Lcom/discord/simpleast/core/parser/Parser;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_TITLES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    invoke-static {v4, v4, v5, v6, v1}, Lcom/discord/utilities/textprocessing/Parsers;->createParser$default(ZZZILjava/lang/Object;)Lcom/discord/simpleast/core/parser/Parser;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_VALUES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;)V
    .locals 3

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01ba

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListItem;-><init>(ILcom/discord/widgets/chat/list/WidgetChatListAdapter;)V

    const p1, 0x7f0a026c

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->containerCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0278

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedProvider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a027a

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0268

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedAuthorIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0269

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedAuthorText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a026e

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0272

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedFields$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a026d

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a039e

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImageContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0275

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0276

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImageIcons$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a039f

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedInlineMedia$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0277

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImageThumbnail$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a026f

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0273

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedFooterIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0274

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedFooterText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0279

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->spoilerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedProvider()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;->invoke(Landroid/widget/TextView;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;->invoke(Landroid/widget/TextView;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;->invoke(Landroid/widget/TextView;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDescription()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;->invoke(Landroid/widget/TextView;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$1;->invoke(Landroid/widget/TextView;)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->userSettings:Lcom/discord/stores/StoreUserSettings;

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v1, "itemView"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0700cd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedTinyIconSize:I

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0700cc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedThumbnailMaxSize:I

    sget-object v0, Lcom/discord/utilities/embed/EmbedResourceUtils;->INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

    iget-object v2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "itemView.context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/embed/EmbedResourceUtils;->computeMaximumImageWidthPx(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->maxEmbedImageWidth:I

    invoke-virtual {p1}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    return-void
.end method

.method public static final synthetic access$configureEmbedDescription(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedDescription(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    return-void
.end method

.method public static final synthetic access$configureEmbedFields(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedFields(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    return-void
.end method

.method public static final synthetic access$configureEmbedTitle(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedTitle(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    return-object p0
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->subscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getUI_THREAD_TITLES_PARSER$cp()Lcom/discord/simpleast/core/parser/Parser;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_TITLES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    return-object v0
.end method

.method public static final synthetic access$getUI_THREAD_VALUES_PARSER$cp()Lcom/discord/simpleast/core/parser/Parser;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_VALUES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    return-object v0
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->subscription:Lrx/Subscription;

    return-void
.end method

.method private final configureEmbedAuthor(Lcom/discord/models/domain/ModelMessageEmbed$Item;)V
    .locals 11

    const/4 v0, 0x0

    const/16 v1, 0x8

    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorText()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorText()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->access$bindUrlOnClick(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorText()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorText()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyIconUrl()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorIcon()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorIcon()Landroid/widget/ImageView;

    move-result-object v3

    iget v6, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedTinyIconSize:I

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyIconUrl()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    move-object v2, p0

    move v4, v6

    move v5, v6

    invoke-static/range {v2 .. v10}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Landroid/widget/ImageView;IIILjava/lang/String;IILjava/lang/Object;)V

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedAuthorIcon()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method private final configureEmbedDescription(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 24

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getParsedDescription()Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-eqz v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbedIndex()I

    move-result v3

    new-instance v13, Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getMyId()J

    move-result-wide v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getMessageState()Lcom/discord/stores/StoreMessageState$State;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/discord/stores/StoreMessageState$State;->getVisibleSpoilerEmbedMap()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    const-string v8, "desc"

    invoke-static {v7, v4, v3, v8}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->access$getEmbedFieldVisibleIndices(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;Ljava/util/Map;ILjava/lang/String;)Ljava/util/List;

    move-result-object v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move-object v7, v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1c

    const/4 v12, 0x0

    move-object v4, v13

    invoke-direct/range {v4 .. v12}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;-><init>(JLjava/util/Collection;Ljava/util/List;ZLjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getParsedDescription()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v13, v4}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->process(Ljava/util/Collection;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDescription()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getParsedDescription()Ljava/util/Collection;

    move-result-object v5

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    new-instance v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedDescription$1;

    move-object/from16 v19, v6

    invoke-direct {v6, v1, v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedDescription$1;-><init>(JI)V

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x37ff

    const/16 v23, 0x0

    move-object/from16 v6, p2

    invoke-static/range {v6 .. v23}, Lcom/discord/utilities/textprocessing/MessageRenderContext;->copy$default(Lcom/discord/utilities/textprocessing/MessageRenderContext;Landroid/content/Context;JZLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/utilities/textprocessing/MessageRenderContext;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/discord/utilities/textprocessing/AstRenderer;->render(Ljava/util/Collection;Ljava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDescription()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDescription()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private final configureEmbedDivider(Ljava/lang/Integer;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDivider()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    const/16 v1, 0xff

    invoke-static {p1, v1}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDivider()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f040136

    invoke-static {p1, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private final configureEmbedFields(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 11

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getParsedFields()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    if-eqz v0, :cond_4

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-nez v1, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v9

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbedIndex()I

    move-result v7

    invoke-virtual {p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getMessageState()Lcom/discord/stores/StoreMessageState$State;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/stores/StoreMessageState$State;->getVisibleSpoilerEmbedMap()Ljava/util/Map;

    move-result-object v3

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    move-object v6, v3

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedFields$1;

    move-object v4, v3

    move-object v5, p1

    move-object v8, p2

    invoke-direct/range {v4 .. v10}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedFields$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Ljava/util/Map;ILcom/discord/utilities/textprocessing/MessageRenderContext;J)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    const/4 p2, 0x0

    :goto_1
    if-ge p2, p1, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge p2, v4, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    goto :goto_2

    :cond_2
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0d01bb

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    :goto_2
    const-string v5, "f_name:"

    invoke-static {v5, p2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;->getParsedName()Ljava/util/Collection;

    move-result-object v6

    const v7, 0x7f0a0270

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {v3, v6, v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedFields$1;->invoke(Ljava/util/Collection;Ljava/lang/String;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "f_value:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;->getParsedValue()Ljava/util/Collection;

    move-result-object v6

    const v7, 0x7f0a0271

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {v3, v6, v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedFields$1;->invoke(Ljava/util/Collection;Ljava/lang/String;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_3
    return-void

    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFields()Landroid/view/ViewGroup;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private final configureEmbedImage(Landroid/widget/ImageView;IIILjava/lang/String;I)V
    .locals 12

    move-object/from16 v0, p5

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    move-object v2, p1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v1, Lcom/discord/utilities/embed/EmbedResourceUtils;->INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const-string v3, "embedInlineMedia.resources"

    invoke-static {v8, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v1

    move v4, p3

    move/from16 v5, p4

    move v6, p2

    move v7, p2

    move/from16 v9, p6

    invoke-virtual/range {v3 .. v9}, Lcom/discord/utilities/embed/EmbedResourceUtils;->calculateScaledSize(IIIILandroid/content/res/Resources;I)Lkotlin/Pair;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-virtual {v3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v6, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v6, v4, :cond_1

    iget v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v6, v3, :cond_2

    :cond_1
    iput v4, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p1}, Landroid/widget/ImageView;->requestLayout()V

    :cond_2
    invoke-virtual {v1, v0, v4, v3}, Lcom/discord/utilities/embed/EmbedResourceUtils;->getPreviewUrls(Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfc

    const/4 v11, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v11}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void
.end method

.method private final configureEmbedImage(Landroid/widget/ImageView;ILcom/discord/models/domain/ModelMessageEmbed$Item;I)V
    .locals 7

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getWidth()I

    move-result v3

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getHeight()I

    move-result v4

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyUrl()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage(Landroid/widget/ImageView;IIILjava/lang/String;I)V

    return-void
.end method

.method private final configureEmbedImage(Lcom/discord/models/domain/ModelMessageEmbed;)V
    .locals 8

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    const/16 v1, 0x8

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderMedia()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v2

    const-string v3, "image"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getWidth()I

    move-result v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v2

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getHeight()I

    move-result v2

    if-lez v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImage()Landroid/widget/ImageView;

    move-result-object v2

    iget v6, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->maxEmbedImageWidth:I

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v7

    invoke-static {v7, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->maxEmbedImageWidth:I

    div-int/lit8 v3, v3, 0x2

    invoke-direct {p0, v2, v6, v7, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage(Landroid/widget/ImageView;ILcom/discord/models/domain/ModelMessageEmbed$Item;I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageContainer()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageContainer()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageIcons()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->isPlayable()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    const/4 v1, 0x0

    :cond_5
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImage()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;

    invoke-direct {v1, p1, p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1;-><init>(Lcom/discord/models/domain/ModelMessageEmbed;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/models/domain/ModelMessageEmbed;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static synthetic configureEmbedImage$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Landroid/widget/ImageView;IIILjava/lang/String;IILjava/lang/Object;)V
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    move v6, p6

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage(Landroid/widget/ImageView;IIILjava/lang/String;I)V

    return-void
.end method

.method public static synthetic configureEmbedImage$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Landroid/widget/ImageView;ILcom/discord/models/domain/ModelMessageEmbed$Item;IILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage(Landroid/widget/ImageView;ILcom/discord/models/domain/ModelMessageEmbed$Item;I)V

    return-void
.end method

.method private final configureEmbedProvider(Lcom/discord/models/domain/ModelMessageEmbed;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getProvider()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedProvider()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->isAttachment()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/discord/utilities/textprocessing/MessageRenderContext;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "providerName"

    invoke-static {v1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {p1, v3, v4}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedProvider()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->access$bindUrlOnClick(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedProvider()Landroid/widget/TextView;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedProvider()Landroid/widget/TextView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final configureEmbedThumbnail(Lcom/discord/models/domain/ModelMessageEmbed$Item;)V
    .locals 8

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-eqz v0, :cond_0

    iget v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedThumbnailMaxSize:I

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageThumbnail()Landroid/widget/ImageView;

    move-result-object v2

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, p1

    invoke-static/range {v1 .. v7}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Landroid/widget/ImageView;ILcom/discord/models/domain/ModelMessageEmbed$Item;IILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final configureEmbedTitle(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V
    .locals 27

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbed()Lcom/discord/models/domain/ModelMessageEmbed;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v7, p0

    if-eqz v2, :cond_2

    iget-boolean v1, v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    if-eqz v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbedIndex()I

    move-result v8

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_TITLES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    sget-object v3, Lcom/discord/utilities/textprocessing/MessageParseState;->Companion:Lcom/discord/utilities/textprocessing/MessageParseState$Companion;

    invoke-virtual {v3}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/discord/utilities/textprocessing/MessagePreprocessor;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getMyId()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getMessageState()Lcom/discord/stores/StoreMessageState$State;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/discord/stores/StoreMessageState$State;->getVisibleSpoilerEmbedMap()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v4, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    const-string/jumbo v5, "title"

    invoke-static {v4, v3, v8, v5}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->access$getEmbedFieldVisibleIndices(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;Ljava/util/Map;ILjava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    move-object v12, v3

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1c

    const/16 v17, 0x0

    move-object v9, v2

    invoke-direct/range {v9 .. v17}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;-><init>(JLjava/util/Collection;Ljava/util/List;ZLjava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v2, v1}, Lcom/discord/utilities/textprocessing/MessagePreprocessor;->process(Ljava/util/Collection;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v2

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedTitle$1;

    move-object/from16 v22, v3

    move-object/from16 v4, p1

    invoke-direct {v3, v4, v8}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureEmbedTitle$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;I)V

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x37ff

    const/16 v26, 0x0

    move-object/from16 v9, p2

    invoke-static/range {v9 .. v26}, Lcom/discord/utilities/textprocessing/MessageRenderContext;->copy$default(Lcom/discord/utilities/textprocessing/MessageRenderContext;Landroid/content/Context;JZLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/utilities/textprocessing/MessageRenderContext;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/discord/utilities/textprocessing/AstRenderer;->render(Ljava/util/Collection;Ljava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v2

    const v3, 0x7f040177

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    goto :goto_1

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v2

    const v3, 0x7f04048e

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->access$bindUrlOnClick(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method private final configureFooter(Lcom/discord/models/domain/ModelMessageEmbed$Item;Ljava/lang/String;)V
    .locals 12

    iget-boolean v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderEmbeds:Z

    const/16 v9, 0x8

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    goto :goto_4

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyIconUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterIcon()Landroid/widget/ImageView;

    move-result-object v1

    iget v4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedTinyIconSize:I

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getProxyIconUrl()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p0

    move v2, v4

    move v3, v4

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage$default(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Landroid/widget/ImageView;IIILjava/lang/String;IILjava/lang/Object;)V

    goto :goto_5

    :cond_6
    :goto_4
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    const/4 v0, 0x0

    if-eqz p2, :cond_7

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterText()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "embedFooterText.context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/time/TimeUtils;->parseUTCDate(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/discord/utilities/time/TimeUtils;->toReadableTimeString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_6

    :cond_7
    move-object v1, v0

    :goto_6
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedFooterText()Landroid/widget/TextView;

    move-result-object v2

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getText()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    :cond_8
    move-object v3, v0

    :goto_7
    if-eqz v3, :cond_9

    if-eqz v1, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " | "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    :cond_9
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getText()Ljava/lang/String;

    move-result-object v3

    goto :goto_8

    :cond_a
    move-object v3, v0

    :goto_8
    if-eqz v3, :cond_b

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    :cond_b
    if-eqz v1, :cond_c

    move-object v0, v1

    :cond_c
    :goto_9
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, "text"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_d

    goto :goto_a

    :cond_d
    const/4 v10, 0x0

    :goto_a
    if-eqz v10, :cond_e

    const/4 v9, 0x0

    :cond_e
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureInlineEmbed(Lcom/discord/models/domain/ModelMessageEmbed;)V
    .locals 13

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->shouldRenderMedia()Z

    move-result v0

    const/16 v1, 0x8

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getVideo()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getImage()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getWidth()I

    move-result v2

    const/4 v3, 0x0

    if-gtz v2, :cond_3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getHeight()I

    move-result v2

    if-lez v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_4

    sget-object v4, Lcom/discord/utilities/embed/EmbedResourceUtils;->INSTANCE:Lcom/discord/utilities/embed/EmbedResourceUtils;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed$Item;->getHeight()I

    move-result v6

    iget v7, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->maxEmbedImageWidth:I

    sget v8, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->MAX_IMAGE_VIEW_HEIGHT_PX:I

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const-string v0, "embedInlineMedia.resources"

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/16 v11, 0x20

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/embed/EmbedResourceUtils;->calculateScaledSize$default(Lcom/discord/utilities/embed/EmbedResourceUtils;IIIILandroid/content/res/Resources;IILjava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, p1, v1, v0}, Lcom/discord/widgets/chat/list/InlineMediaView;->updateUIWithEmbed(Lcom/discord/models/domain/ModelMessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;

    invoke-direct {v1, v0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureInlineEmbed$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/chat/list/InlineMediaView;Lcom/discord/models/domain/ModelMessageEmbed;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;)V
    .locals 22

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->getEmbedEntry()Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbed()Lcom/discord/models/domain/ModelMessageEmbed;

    move-result-object v3

    iget-object v4, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v5, "itemView"

    const-string v6, "itemView.context"

    invoke-static {v4, v5, v6}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v4

    iget-object v6, v1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getEventHandler()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->createRenderContext(Landroid/content/Context;Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;)Lcom/discord/utilities/textprocessing/MessageRenderContext;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->isSpoilerHidden()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getSpoilerView()Landroid/widget/FrameLayout;

    move-result-object v7

    const-wide/16 v8, 0x32

    const/4 v10, 0x0

    sget-object v11, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureUI$1;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureUI$1;

    const/4 v12, 0x0

    const/16 v13, 0xa

    const/4 v14, 0x0

    invoke-static/range {v7 .. v14}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeIn$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getSpoilerView()Landroid/widget/FrameLayout;

    move-result-object v15

    const-wide/16 v16, 0xc8

    sget-object v18, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureUI$2;->INSTANCE:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureUI$2;

    const/16 v19, 0x0

    const/16 v20, 0x4

    const/16 v21, 0x0

    invoke-static/range {v15 .. v21}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeOut$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getSpoilerView()Landroid/widget/FrameLayout;

    move-result-object v6

    new-instance v7, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureUI$3;

    invoke-direct {v7, v1, v0, v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$configureUI$3;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/widgets/chat/list/entries/EmbedEntry;)V

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v2, 0x8

    :try_start_0
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessageEmbed;->isInlineEmbed()Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedContent()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDivider()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageThumbnail()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {v1, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureInlineEmbed(Lcom/discord/models/domain/ModelMessageEmbed;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v3

    const v4, 0x7f06026f

    invoke-static {v3, v4}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/material/card/MaterialCardView;->setCardBackgroundColor(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/material/card/MaterialCardView;->setStrokeWidth(I)V

    goto/16 :goto_2

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedContent()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedDivider()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedImageThumbnail()Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;->isSpoilerHidden()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessageEmbed;->getColor()Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    invoke-direct {v1, v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedDivider(Ljava/lang/Integer;)V

    invoke-direct {v1, v3, v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedProvider(Lcom/discord/models/domain/ModelMessageEmbed;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessageEmbed;->getAuthor()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v6

    invoke-direct {v1, v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedAuthor(Lcom/discord/models/domain/ModelMessageEmbed$Item;)V

    invoke-direct {v1, v0, v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedTitle(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    invoke-direct {v1, v0, v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedDescription(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessageEmbed;->getThumbnail()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v6

    invoke-direct {v1, v6}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedThumbnail(Lcom/discord/models/domain/ModelMessageEmbed$Item;)V

    invoke-direct {v1, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedImage(Lcom/discord/models/domain/ModelMessageEmbed;)V

    invoke-direct {v1, v0, v4}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureEmbedFields(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessageEmbed;->getFooter()Lcom/discord/models/domain/ModelMessageEmbed$Item;

    move-result-object v0

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelMessageEmbed;->getTimestamp()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureFooter(Lcom/discord/models/domain/ModelMessageEmbed$Item;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v3

    const v4, 0x7f04013b

    invoke-static {v3, v4}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/material/card/MaterialCardView;->setCardBackgroundColor(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getContainerCard()Lcom/google/android/material/card/MaterialCardView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/material/card/MaterialCardView;->setStrokeWidth(I)V

    :goto_2
    iget-object v0, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    move-object v10, v0

    iget-object v0, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v8, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const-string v9, "Unable to render embed."

    invoke-static/range {v8 .. v13}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_3
    return-void
.end method

.method private final getContainerCard()Lcom/google/android/material/card/MaterialCardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->containerCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/card/MaterialCardView;

    return-object v0
.end method

.method private final getEmbedAuthorIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedAuthorIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getEmbedAuthorText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedAuthorText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmbedContent()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method private final getEmbedDescription()Lcom/discord/utilities/view/text/LinkifiedTextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/text/LinkifiedTextView;

    return-object v0
.end method

.method private final getEmbedDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getEmbedFields()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedFields$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getEmbedFooterIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedFooterIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getEmbedFooterText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedFooterText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmbedImage()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getEmbedImageContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImageContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getEmbedImageIcons()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImageIcons$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getEmbedImageThumbnail()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedImageThumbnail$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedInlineMedia$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/list/InlineMediaView;

    return-object v0
.end method

.method private final getEmbedProvider()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedProvider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmbedTitle()Lcom/discord/utilities/view/text/LinkifiedTextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->embedTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/text/LinkifiedTextView;

    return-object v0
.end method

.method private final getSpoilerView()Landroid/widget/FrameLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->spoilerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private final parseFields(Ljava/util/List;Lcom/discord/simpleast/core/parser/Parser;Lcom/discord/simpleast/core/parser/Parser;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessageEmbed$Field;",
            ">;",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            "Lcom/discord/simpleast/core/node/Node<",
            "Lcom/discord/utilities/textprocessing/MessageRenderContext;",
            ">;",
            "Lcom/discord/utilities/textprocessing/MessageParseState;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelMessageEmbed$Field;

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed$Field;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v4, "it.name"

    invoke-static {v5, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v10, Lcom/discord/utilities/textprocessing/MessageParseState;->Companion:Lcom/discord/utilities/textprocessing/MessageParseState$Companion;

    invoke-virtual {v10}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v4 .. v9}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessageEmbed$Field;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v2, "it.value"

    invoke-static {v12, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x4

    const/16 v16, 0x0

    move-object/from16 v11, p3

    invoke-static/range {v11 .. v16}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model$ParsedField;-><init>(Ljava/util/Collection;Ljava/util/Collection;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method private final shouldRenderMedia()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getInlineEmbedMedia()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public getSubscription()Lrx/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->subscription:Lrx/Subscription;

    return-object v0
.end method

.method public onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
    .locals 12

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListItem;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    move-object v2, p2

    check-cast v2, Lcom/discord/widgets/chat/list/entries/EmbedEntry;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$onConfigure$1;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbed()Lcom/discord/models/domain/ModelMessageEmbed;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getDescription()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_VALUES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    sget-object p1, Lcom/discord/utilities/textprocessing/MessageParseState;->Companion:Lcom/discord/utilities/textprocessing/MessageParseState$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v3, p1

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/entries/EmbedEntry;->getEmbed()Lcom/discord/models/domain/ModelMessageEmbed;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageEmbed;->getFields()Ljava/util/List;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_TITLES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    sget-object v0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->UI_THREAD_VALUES_PARSER:Lcom/discord/simpleast/core/parser/Parser;

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->parseFields(Ljava/util/List;Lcom/discord/simpleast/core/parser/Parser;Lcom/discord/simpleast/core/parser/Parser;)Ljava/util/List;

    move-result-object v4

    new-instance p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/16 v10, 0x78

    const/4 v11, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;-><init>(Lcom/discord/widgets/chat/list/entries/EmbedEntry;Ljava/util/Collection;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->configureUI(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;)V

    sget-object p2, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;

    invoke-static {p2, p1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;->access$getModel(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Companion;Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$Model;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    const-string p2, "getModel(initialModel)\n        .take(1)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;

    const/4 v2, 0x0

    new-instance v3, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$onConfigure$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$onConfigure$2;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;)V

    const/4 v4, 0x0

    new-instance v6, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$onConfigure$3;

    invoke-direct {v6, p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed$onConfigure$3;-><init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;)V

    const/16 v7, 0x1a

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/InlineMediaView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemEmbed;->getEmbedInlineMedia()Lcom/discord/widgets/chat/list/InlineMediaView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/InlineMediaView;->onResume()V

    return-void
.end method
