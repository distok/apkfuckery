.class public final synthetic Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$1;
.super Lx/m/c/i;
.source "ViewEmbedGameInvite.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function7;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->getForShare(Landroid/content/Context;Lcom/discord/utilities/time/Clock;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function7<",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelMessage$Activity;",
        "Lcom/discord/models/domain/activity/ModelActivity;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;",
        "Lcom/discord/models/domain/ModelApplication;",
        "Ljava/util/List<",
        "+",
        "Landroid/content/pm/PackageInfo;",
        ">;",
        "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;)V
    .locals 7

    const-class v3, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;

    const/4 v1, 0x7

    const-string v4, "createForShare"

    const-string v5, "createForShare(Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "J",
            "Lcom/discord/models/domain/ModelMessage$Activity;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Lcom/discord/models/domain/ModelApplication;",
            "Ljava/util/List<",
            "+",
            "Landroid/content/pm/PackageInfo;",
            ">;)",
            "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;"
        }
    .end annotation

    const-string v0, "p1"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p5"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p7"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v1, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;

    move-wide v3, p2

    move-object v6, p5

    move-object/from16 v8, p7

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;->access$createForShare(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    move-object v1, p1

    check-cast v1, Lcom/discord/models/domain/ModelUser;

    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    move-object v4, p3

    check-cast v4, Lcom/discord/models/domain/ModelMessage$Activity;

    move-object v5, p4

    check-cast v5, Lcom/discord/models/domain/activity/ModelActivity;

    move-object v6, p5

    check-cast v6, Ljava/util/Map;

    move-object v7, p6

    check-cast v7, Lcom/discord/models/domain/ModelApplication;

    move-object/from16 v8, p7

    check-cast v8, Ljava/util/List;

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion$getForShare$1;->invoke(Lcom/discord/models/domain/ModelUser;JLcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/Map;Lcom/discord/models/domain/ModelApplication;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object v0

    return-object v0
.end method
