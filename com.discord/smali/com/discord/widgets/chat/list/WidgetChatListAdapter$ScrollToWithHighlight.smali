.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;
.super Ljava/lang/Object;
.source "WidgetChatListAdapter.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScrollToWithHighlight"
.end annotation


# instance fields
.field private final adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

.field private attempts:I

.field private final messageId:J

.field private final onCompleted:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapter;JLkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/list/WidgetChatListAdapter;",
            "J",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCompleted"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    iput-wide p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->messageId:J

    iput-object p4, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->onCompleted:Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private final animateHighlight(Landroid/view/View;)V
    .locals 4

    const v0, 0x7f08011c

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.graphics.drawable.TransitionDrawable"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    new-instance v1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight$animateHighlight$1;

    invoke-direct {v1, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight$animateHighlight$1;-><init>(Landroid/graphics/drawable/TransitionDrawable;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private final getNewMessageEntryIndex(Ljava/util/List;)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/list/entries/ChatListEntry;",
            ">;)I"
        }
    .end annotation

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->messageId:J

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    cmp-long v6, v0, v2

    if-nez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    const-wide/16 v7, 0x1

    cmp-long v9, v0, v7

    if-nez v9, :cond_1

    return v5

    :cond_1
    const/4 v7, 0x0

    cmp-long v8, v0, v2

    if-nez v8, :cond_5

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getNewMessagesMarkerMessageId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    cmp-long v1, v8, v2

    if-lez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    move-object v0, v7

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_3

    :cond_4
    return v5

    :cond_5
    :goto_3
    const/4 v8, -0x1

    cmp-long v9, v0, v2

    if-gtz v9, :cond_6

    return v8

    :cond_6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    instance-of v10, v9, Lcom/discord/widgets/chat/list/entries/MessageEntry;

    if-eqz v10, :cond_7

    check-cast v9, Lcom/discord/widgets/chat/list/entries/MessageEntry;

    invoke-virtual {v9}, Lcom/discord/widgets/chat/list/entries/MessageEntry;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v9

    cmp-long v11, v9, v0

    if-gtz v11, :cond_7

    const/4 v9, 0x1

    goto :goto_5

    :cond_7
    const/4 v9, 0x0

    :goto_5
    if-eqz v9, :cond_8

    move v8, v3

    goto :goto_6

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    :goto_6
    if-gez v8, :cond_a

    if-eqz v6, :cond_a

    sget-object v2, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->Companion:Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;

    invoke-static {p1}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;->access$findBestNewMessagesPosition(Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Companion;I)I

    move-result v8

    :cond_a
    invoke-static {v8, v5}, Lx/p/e;->downTo(II)Lkotlin/ranges/IntProgression;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/chat/list/entries/ChatListEntry;

    instance-of v9, v6, Lcom/discord/widgets/chat/list/entries/NewMessagesEntry;

    if-eqz v9, :cond_c

    check-cast v6, Lcom/discord/widgets/chat/list/entries/NewMessagesEntry;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/list/entries/NewMessagesEntry;->getMessageId()J

    move-result-wide v9

    cmp-long v6, v9, v0

    if-nez v6, :cond_c

    const/4 v6, 0x1

    goto :goto_7

    :cond_c
    const/4 v6, 0x0

    :goto_7
    if-eqz v6, :cond_b

    move-object v7, v3

    :cond_d
    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_e

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v8

    :cond_e
    return v8
.end method

.method private final scheduleRetry()V
    .locals 3

    iget v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->attempts:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->attempts:I

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, p0, v1, v2}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->onCompleted:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_0
    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final getAdapter()Lcom/discord/widgets/chat/list/WidgetChatListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    return-object v0
.end method

.method public final getMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->messageId:J

    return-wide v0
.end method

.method public final getOnCompleted()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->onCompleted:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/model/WidgetChatListModel;->isLoadingMessages()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->scheduleRetry()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->getNewMessageEntryIndex(Ljava/util/List;)I

    move-result v0

    if-gez v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->scheduleRetry()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    float-to-int v1, v1

    iget-object v3, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3, v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    invoke-virtual {v3, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v2

    :cond_3
    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->adapter:Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->getData()Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$Data;->getList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/widgets/chat/list/entries/NewMessagesEntry;

    if-nez v0, :cond_4

    invoke-direct {p0, v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->animateHighlight(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->onCompleted:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter$ScrollToWithHighlight;->scheduleRetry()V

    :goto_0
    return-void
.end method
