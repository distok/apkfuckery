.class public final Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetChatListAdapterItemStart.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;->onConfigure(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart$onConfigure$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;

    iput-wide p2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart$onConfigure$1;->$channelId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    sget-object p1, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->Companion:Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;

    iget-wide v0, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart$onConfigure$1;->$channelId:J

    iget-object v2, p0, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart$onConfigure$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;

    invoke-static {v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;->access$getAdapter$p(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemStart;)Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/widgets/channels/WidgetTextChannelSettings$Companion;->launch(JLandroid/content/Context;)V

    return-void
.end method
