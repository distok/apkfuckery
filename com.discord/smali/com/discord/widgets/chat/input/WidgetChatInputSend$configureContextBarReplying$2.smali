.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$2;
.super Ljava/lang/Object;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureContextBarReplying(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $model:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$2;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getPendingReplies()Lcom/discord/stores/StorePendingReplies;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$2;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getChannelId()Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v1, "model.messageReference.channelId!!"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StorePendingReplies;->onDeletePendingReply(J)V

    return-void
.end method
