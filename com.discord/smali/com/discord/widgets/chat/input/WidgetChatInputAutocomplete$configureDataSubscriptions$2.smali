.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;
.super Ljava/lang/Object;
.source "WidgetChatInputAutocomplete.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(ZZ)Ljava/lang/Boolean;
    .locals 0

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;->call(ZZ)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
