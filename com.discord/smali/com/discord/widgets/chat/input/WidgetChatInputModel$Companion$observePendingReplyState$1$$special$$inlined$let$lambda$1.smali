.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$$special$$inlined$let$lambda$1;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;->call(Lcom/discord/stores/StorePendingReplies$PendingReply;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $originalAuthorId$inlined:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$$special$$inlined$let$lambda$1;->$originalAuthorId$inlined:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;)Lcom/discord/models/domain/ModelGuildMember$Computed;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;)",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;"
        }
    .end annotation

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$$special$$inlined$let$lambda$1;->$originalAuthorId$inlined:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildMember$Computed;

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$$special$$inlined$let$lambda$1;->call(Ljava/util/Map;)Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-result-object p1

    return-object p1
.end method
