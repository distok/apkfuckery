.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->observePendingReplyState(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StorePendingReplies$PendingReply;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StorePendingReplies$PendingReply;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;->call(Lcom/discord/stores/StorePendingReplies$PendingReply;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/stores/StorePendingReplies$PendingReply;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePendingReplies$PendingReply;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Hide;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Hide;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getOriginalMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->isWebhook()Z

    move-result v0

    const-string v1, "pendingReply.originalMessage.author"

    if-eqz v0, :cond_1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getShouldMention()Z

    move-result v4

    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getShowMentionToggle()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getOriginalMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v6

    invoke-static {v6, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;-><init>(Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object v0, p1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getOriginalMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_2

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v3

    const-string v4, "guildId"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$$special$$inlined$let$lambda$1;

    invoke-direct {v3, v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$$special$$inlined$let$lambda$1;-><init>(J)V

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    new-instance v3, Lg0/l/e/j;

    invoke-direct {v3, v2}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object v2, v3

    :goto_0
    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/discord/stores/StoreUser;->observeUser(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;-><init>(Lcom/discord/stores/StorePendingReplies$PendingReply;)V

    invoke-static {v0, v2, v1}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    :goto_1
    return-object v0
.end method
