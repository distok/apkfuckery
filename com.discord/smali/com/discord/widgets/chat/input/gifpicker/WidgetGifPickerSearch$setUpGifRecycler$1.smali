.class public final Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;
.super Lx/m/c/k;
.source "WidgetGifPickerSearch.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->setUpGifRecycler(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;->this$0:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;->invoke(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V
    .locals 1

    const-string v0, "gifItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;->this$0:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->access$getGifPickerViewModel$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;->this$0:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->access$getOnGifSelected$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
