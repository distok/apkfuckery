.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;
.super Ljava/lang/Object;
.source "GifSearchViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final searchSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeGifPicker:Lcom/discord/stores/StoreGifPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGifPicker;Lcom/discord/stores/StoreAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/discord/stores/StoreGifPicker;",
            "Lcom/discord/stores/StoreAnalytics;",
            ")V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGifPicker"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAnalytics"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGifPicker;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const-string p2, ""

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    const-string p6, "BehaviorSubject.create(\"\")"

    invoke-static {p2, p6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGifPicker()Lcom/discord/stores/StoreGifPicker;

    move-result-object p3

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    sget-object p4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p4

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;-><init>(Landroid/content/Context;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGifPicker;Lcom/discord/stores/StoreAnalytics;)V

    return-void
.end method

.method public static final synthetic access$getStoreAnalytics$p(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)Lcom/discord/stores/StoreAnalytics;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getStoreGifPicker$p(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)Lcom/discord/stores/StoreGifPicker;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    return-object p0
.end method

.method public static final synthetic access$observeQueryState(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;Ljava/util/List;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->observeQueryState(Ljava/util/List;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final observeQueryState(Ljava/util/List;)Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Lg0/p/a;->c()Lrx/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Lg0/p/a;->c()Lrx/Scheduler;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-direct {v1, v4, v5, v2, v3}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    new-instance v2, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v0, v1}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "searchSubject\n          \u2026          }\n            }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifPicker;->observeGifTrendingSearchTerms()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeStoreState$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "storeGifPicker.observeGi\u2026eryState(terms)\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "modelClass"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    new-instance v16, Lcom/discord/widgets/chat/MessageManager;

    iget-object v6, v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->context:Landroid/content/Context;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xfe

    const/4 v15, 0x0

    move-object/from16 v5, v16

    invoke-direct/range {v5 .. v15}, Lcom/discord/widgets/chat/MessageManager;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v6, 0x0

    const/16 v7, 0x8

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v1
.end method
