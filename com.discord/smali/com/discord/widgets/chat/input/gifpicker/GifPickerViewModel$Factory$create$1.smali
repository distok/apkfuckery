.class public final Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;
.super Ljava/lang/Object;
.source "GifPickerViewModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;->create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
        ">;",
        "Ljava/lang/String;",
        "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;

    const-string v1, "gifCategories"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "trendingGifCategoryPreviewUrl"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;-><init>(Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;->call(Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
