.class public final Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;
.super Ljava/lang/Object;
.source "GifCategoryViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final gifCategoryItem:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

.field private final storeGifPicker:Lcom/discord/stores/StoreGifPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lcom/discord/stores/StoreGifPicker;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gifCategoryItem"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGifPicker"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->gifCategoryItem:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lcom/discord/stores/StoreGifPicker;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGifPicker()Lcom/discord/stores/StoreGifPicker;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;-><init>(Landroid/content/Context;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lcom/discord/stores/StoreGifPicker;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->gifCategoryItem:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    instance-of v1, v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;->getGifCategory()Lcom/discord/models/gifpicker/domain/ModelGifCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/gifpicker/domain/ModelGifCategory;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreGifPicker;->observeGifsForSearchQuery(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v0, v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifPicker;->observeTrendingCategoryGifs()Lrx/Observable;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory$observeStoreState$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "gifsObservable.map { gifs -> StoreState(gifs) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    new-instance v11, Lcom/discord/widgets/chat/MessageManager;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->context:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xfe

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/discord/widgets/chat/MessageManager;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v3

    const/4 v5, 0x4

    move-object v1, p1

    move-object v2, v11

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;-><init>(Lcom/discord/widgets/chat/MessageManager;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method
