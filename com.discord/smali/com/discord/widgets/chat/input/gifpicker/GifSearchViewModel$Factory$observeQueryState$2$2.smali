.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;
.super Ljava/lang/Object;
.source "GifSearchViewModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->call(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/gifpicker/dto/ModelGif;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $query:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;->$query:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;

    const-string v1, "gifResults"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "searchTerms"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;

    iget-object v1, v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->$trendingSearchTerms:Ljava/util/List;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;->$query:Ljava/lang/String;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;->call(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;

    move-result-object p1

    return-object p1
.end method
