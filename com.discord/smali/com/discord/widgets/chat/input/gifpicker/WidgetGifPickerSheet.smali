.class public final Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetGifPickerSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;


# instance fields
.field private container:Landroid/view/View;

.field private gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

.field private onCancel:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onGifSelected:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->Companion:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->container:Landroid/view/View;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "container"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getOnGifSelected$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;)Lkotlin/jvm/functions/Function0;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$setContainer$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->container:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setOnGifSelected$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic setOnCancel$default(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->setOnCancel(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01fc

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->clearSearchBar()V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->onCancel:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void

    :cond_1
    const-string p1, "gifPickerFragment"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    invoke-super {p0, p1}, Lcom/discord/app/AppBottomSheet;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$onCreateDialog$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$onCreateDialog$1;-><init>(Landroid/app/Dialog;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    return-object p1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->container:Landroid/view/View;

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    invoke-direct {p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const/4 p2, 0x0

    const-string v0, "gifPickerFragment"

    if-eqz p1, :cond_2

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$onViewCreated$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;)V

    invoke-virtual {p1, v1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->setOnGifSelected(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    const v1, 0x7f0a0496

    iget-object v2, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    if-eqz v2, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v1, v2, p2}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$onViewCreated$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;)V

    invoke-virtual {p1, p2}, Landroidx/fragment/app/FragmentTransaction;->runOnCommit(Ljava/lang/Runnable;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void

    :cond_0
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_1
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_2
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2
.end method

.method public final setOnCancel(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->onCancel:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setOnGifSelected(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-void
.end method
