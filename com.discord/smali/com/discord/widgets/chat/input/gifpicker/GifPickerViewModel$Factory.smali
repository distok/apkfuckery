.class public final Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;
.super Ljava/lang/Object;
.source "GifPickerViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final storeGifPicker:Lcom/discord/stores/StoreGifPicker;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;-><init>(Lcom/discord/stores/StoreGifPicker;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreGifPicker;)V
    .locals 1

    const-string/jumbo v0, "storeGifPicker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreGifPicker;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGifPicker()Lcom/discord/stores/StoreGifPicker;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifPicker;->observeGifCategories()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;->storeGifPicker:Lcom/discord/stores/StoreGifPicker;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifPicker;->observeTrendingGifCategoryPreviewUrl()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory$create$1;

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026Url\n          )\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;-><init>(Lrx/Observable;)V

    return-object p1
.end method
