.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;
.super Lf/a/b/l0;
.source "GifSearchViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final messageManager:Lcom/discord/widgets/chat/MessageManager;

.field private final searchSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreAnalytics;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/discord/widgets/chat/MessageManager;",
            "Lcom/discord/stores/StoreAnalytics;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageManager"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAnalytics"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->storeStateObservable:Lrx/Observable;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    const/4 p2, 0x2

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    new-instance v7, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    sget-object p4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p4}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p4

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreAnalytics;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;)V
    .locals 9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;->getTrendingSearchTerms()Ljava/util/List;

    move-result-object v1

    instance-of v2, p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;

    const v3, 0x7f1208f0

    if-eqz v2, :cond_7

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;->getGifs()Ljava/util/List;

    move-result-object v1

    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v1, v5}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/gifpicker/dto/ModelGif;

    new-instance v6, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;->getSearchQuery()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;-><init>(Lcom/discord/models/gifpicker/dto/ModelGif;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;->getSuggested()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-lez v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;->getSuggested()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    if-lez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    iget-object v7, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v7}, Lrx/subjects/BehaviorSubject;->i0()Ljava/lang/Object;

    move-result-object v7

    const-string v8, "searchSubject.value"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-lez v7, :cond_3

    goto :goto_3

    :cond_3
    const/4 v5, 0x0

    :goto_3
    if-eqz v4, :cond_4

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;

    const v3, 0x7f1208f2

    invoke-direct {p1, v1, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;-><init>(Ljava/util/List;I)V

    goto :goto_4

    :cond_4
    if-eqz v5, :cond_5

    if-nez p1, :cond_5

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;

    const v3, 0x7f121100

    invoke-direct {p1, v1, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;-><init>(Ljava/util/List;I)V

    goto :goto_4

    :cond_5
    if-eqz v5, :cond_6

    if-eqz p1, :cond_6

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;

    const v3, 0x7f1210ff

    invoke-direct {p1, v1, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;-><init>(Ljava/util/List;I)V

    goto :goto_4

    :cond_6
    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;

    invoke-direct {p1, v1, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;-><init>(Ljava/util/List;I)V

    :goto_4
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    instance-of p1, p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$TrendingSearchTermsResults;

    if-eqz p1, :cond_8

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;

    invoke-direct {p1, v1, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;-><init>(Ljava/util/List;I)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    :goto_5
    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    invoke-direct {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;-><init>(Ljava/util/List;)V

    if-eqz v2, :cond_9

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    sget-object v1, Lcom/discord/utilities/analytics/SearchType;->GIF:Lcom/discord/utilities/analytics/SearchType;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;->getGifCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreAnalytics;->trackSearchResultViewed(Lcom/discord/utilities/analytics/SearchType;I)V

    :cond_9
    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V
    .locals 18

    move-object/from16 v0, p0

    const-string v1, "gifItem"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    if-nez v3, :cond_0

    const/4 v1, 0x0

    :cond_0
    check-cast v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    sget-object v4, Lcom/discord/utilities/analytics/SearchType;->GIF:Lcom/discord/utilities/analytics/SearchType;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;->getGifCount()I

    move-result v1

    sget-object v5, Lcom/discord/utilities/analytics/SourceObject;->GIF_PICKER:Lcom/discord/utilities/analytics/SourceObject;

    invoke-virtual {v3, v4, v1, v5}, Lcom/discord/stores/StoreAnalytics;->trackSearchResultSelected(Lcom/discord/utilities/analytics/SearchType;ILcom/discord/utilities/analytics/SourceObject;)V

    :cond_1
    iget-object v6, v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;->getGif()Lcom/discord/models/gifpicker/dto/ModelGif;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/gifpicker/dto/ModelGif;->getTenorGifUrl()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1fe

    const/16 v17, 0x0

    invoke-static/range {v6 .. v17}, Lcom/discord/widgets/chat/MessageManager;->sendMessage$default(Lcom/discord/widgets/chat/MessageManager;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Z

    return-void
.end method

.method public final setSearchText(Ljava/lang/String;)V
    .locals 2

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->i0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
