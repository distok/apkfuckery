.class public final Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;
.super Ljava/lang/Object;
.source "GifCategoryViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final gifCount$delegate:Lkotlin/Lazy;

.field private final gifItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "gifItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState$gifCount$2;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState$gifCount$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifCount$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->copy(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            ">;)",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;"
        }
    .end annotation

    const-string v0, "gifItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGifCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifCount$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getGifItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(gifItems="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->gifItems:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
