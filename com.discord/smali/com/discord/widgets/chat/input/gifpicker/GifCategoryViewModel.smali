.class public final Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;
.super Lf/a/b/l0;
.source "GifCategoryViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final messageManager:Lcom/discord/widgets/chat/MessageManager;

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/MessageManager;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/MessageManager;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreAnalytics;",
            ")V"
        }
    .end annotation

    const-string v0, "messageManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAnalytics"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->storeStateObservable:Lrx/Observable;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    sget-object p1, Lcom/discord/utilities/analytics/SearchType;->GIF:Lcom/discord/utilities/analytics/SearchType;

    invoke-virtual {p3, p1}, Lcom/discord/stores/StoreAnalytics;->trackSearchStarted(Lcom/discord/utilities/analytics/SearchType;)V

    const/4 p1, 0x0

    const/4 p3, 0x1

    invoke-static {p2, p1, p3, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    new-instance v7, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/MessageManager;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;-><init>(Lcom/discord/widgets/chat/MessageManager;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;->getGifs()Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/gifpicker/dto/ModelGif;

    new-instance v2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v2, v1, v4, v3, v4}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;-><init>(Lcom/discord/models/gifpicker/dto/ModelGif;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;

    invoke-direct {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;-><init>(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    sget-object v1, Lcom/discord/utilities/analytics/SearchType;->GIF:Lcom/discord/utilities/analytics/SearchType;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->getGifCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreAnalytics;->trackSearchResultViewed(Lcom/discord/utilities/analytics/SearchType;I)V

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V
    .locals 18

    move-object/from16 v0, p0

    const-string v1, "gifItem"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;

    if-eqz v1, :cond_0

    iget-object v3, v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    sget-object v4, Lcom/discord/utilities/analytics/SearchType;->GIF:Lcom/discord/utilities/analytics/SearchType;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->getGifCount()I

    move-result v1

    sget-object v5, Lcom/discord/utilities/analytics/SourceObject;->GIF_PICKER:Lcom/discord/utilities/analytics/SourceObject;

    invoke-virtual {v3, v4, v1, v5}, Lcom/discord/stores/StoreAnalytics;->trackSearchResultSelected(Lcom/discord/utilities/analytics/SearchType;ILcom/discord/utilities/analytics/SourceObject;)V

    :cond_0
    iget-object v6, v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;->getGif()Lcom/discord/models/gifpicker/dto/ModelGif;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/gifpicker/dto/ModelGif;->getTenorGifUrl()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1fe

    const/16 v17, 0x0

    invoke-static/range {v6 .. v17}, Lcom/discord/widgets/chat/MessageManager;->sendMessage$default(Lcom/discord/widgets/chat/MessageManager;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Z

    return-void
.end method
