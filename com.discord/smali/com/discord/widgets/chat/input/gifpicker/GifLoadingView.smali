.class public final Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "GifLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifViewHolder;,
        Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;
    }
.end annotation


# instance fields
.field private isStaggered:Z

.field private final loadingGifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-direct {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->loadingGifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-direct {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->loadingGifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-direct {p0, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-direct {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->loadingGifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-direct {p0, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final initialize(Landroid/util/AttributeSet;)V
    .locals 3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/discord/R$a;->GifLoadingView:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string v0, "context.obtainStyledAttr\u2026ble.GifLoadingView, 0, 0)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->isStaggered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->loadingGifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public static synthetic updateView$default(Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;IIILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x4

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->updateView(II)V

    return-void
.end method


# virtual methods
.method public final updateView(II)V
    .locals 5

    new-instance v0, Landroidx/recyclerview/widget/StaggeredGridLayoutManager;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Landroidx/recyclerview/widget/StaggeredGridLayoutManager;-><init>(II)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    new-instance v0, Lcom/discord/utilities/recycler/GridColumnSpaceItemDecoration;

    const/16 v2, 0x8

    invoke-static {v2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v2

    invoke-direct {v0, v2, p1}, Lcom/discord/utilities/recycler/GridColumnSpaceItemDecoration;-><init>(II)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    mul-int p2, p2, p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->isStaggered:Z

    const/16 v3, 0x60

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v2, :cond_2

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sub-int/2addr p2, v1

    :goto_0
    if-ge v1, p2, :cond_1

    rem-int/lit8 v2, v1, 0x2

    if-nez v2, :cond_0

    if-ge v1, p1, :cond_0

    const/16 v2, 0x60

    goto :goto_1

    :cond_0
    const/16 v2, 0xa0

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2
    const/4 p1, 0x0

    :goto_2
    if-ge p1, p2, :cond_3

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_3
    :goto_3
    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->loadingGifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView$LoadingGifAdapter;->setData(Ljava/util/List;)V

    return-void
.end method
