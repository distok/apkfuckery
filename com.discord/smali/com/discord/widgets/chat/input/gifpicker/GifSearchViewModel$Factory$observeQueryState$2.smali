.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;
.super Ljava/lang/Object;
.source "GifSearchViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->observeQueryState(Ljava/util/List;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/String;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $trendingSearchTerms:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->$trendingSearchTerms:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->call(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/String;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;",
            ">;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$TrendingSearchTermsResults;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->$trendingSearchTerms:Ljava/util/List;

    invoke-direct {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$TrendingSearchTermsResults;-><init>(Ljava/util/List;)V

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->access$getStoreGifPicker$p(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)Lcom/discord/stores/StoreGifPicker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGifPicker;->observeGifsForSearchQuery(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;

    invoke-static {v1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->access$getStoreGifPicker$p(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)Lcom/discord/stores/StoreGifPicker;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/discord/stores/StoreGifPicker;->observeSuggestedSearchTerms(Ljava/lang/String;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    :goto_1
    return-object v0
.end method
