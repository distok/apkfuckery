.class public final Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;
.super Lcom/discord/app/AppFragment;
.source "WidgetGifPicker.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private categoryAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;

.field private final categoryRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifCategoriesLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onSelectGifCategory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    const-string v6, "categoryRecycler"

    const-string v7, "getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    const-string v6, "gifCategoriesLoadingView"

    const-string v7, "getGifCategoriesLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a048e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a048d

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->categoryRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a048f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->gifCategoriesLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getCategoryRecycler$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGifCategoriesLoadingView$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;)Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getGifCategoriesLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->handleViewState(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;)V

    return-void
.end method

.method private final getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->categoryRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getGifCategoriesLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->gifCategoriesLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    return-object v0
.end method

.method private final handleViewState(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;->isLoaded()Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getGifCategoriesLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;->isLoaded()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;->getGifCategoryItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->setItems(Ljava/util/List;)V

    return-void

    :cond_2
    const-string p1, "categoryAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final setUpCategoryRecycler()V
    .locals 10

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;

    iget-object v6, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->onSelectGifCategory:Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v0

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;-><init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance v0, Lcom/discord/utilities/recycler/GridColumnSpaceItemDecoration;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v1

    invoke-direct {v0, v1, v3}, Lcom/discord/utilities/recycler/GridColumnSpaceItemDecoration;-><init>(II)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    :cond_0
    const-string v0, "categoryAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setWindowInsetsListeners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$setWindowInsetsListeners$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$setWindowInsetsListeners$1;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getGifCategoriesLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$setWindowInsetsListeners$2;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$setWindowInsetsListeners$2;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getContainer()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$setWindowInsetsListeners$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$setWindowInsetsListeners$3;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01fa

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v2}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;-><init>(Lcom/discord/stores/StoreGifPicker;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026kerViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->viewModel:Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->setUpCategoryRecycler()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->setWindowInsetsListeners()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getGifCategoriesLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v0, v2}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->updateView$default(Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;IIILjava/lang/Object;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->viewModel:Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final scrollToTop()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "categoryAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final setOnSelectGifCategory(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onGifCategorySelected"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->onSelectGifCategory:Lkotlin/jvm/functions/Function1;

    return-void
.end method
