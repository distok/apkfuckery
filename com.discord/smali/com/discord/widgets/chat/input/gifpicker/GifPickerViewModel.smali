.class public final Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;
.super Lf/a/b/l0;
.source "GifPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;

    sget-object v1, Lx/h/l;->d:Lx/h/l;

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;->storeStateObservable:Lrx/Observable;

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;

    new-instance v8, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;->getTrendingGifCategoryPreviewUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;->getTrendingGifCategoryPreviewUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;->getGifCategories()Ljava/util/List;

    move-result-object p1

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/gifpicker/domain/ModelGifCategory;

    new-instance v3, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    invoke-direct {v3, v2}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;-><init>(Lcom/discord/models/gifpicker/domain/ModelGifCategory;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance p1, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;

    invoke-direct {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method
