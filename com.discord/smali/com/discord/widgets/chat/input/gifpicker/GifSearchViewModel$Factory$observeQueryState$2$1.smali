.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;
.super Ljava/lang/Object;
.source "GifSearchViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2;->call(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/gifpicker/dto/ModelGif;",
        ">;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreGifPicker;->Companion:Lcom/discord/stores/StoreGifPicker$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifPicker$Companion;->getSearchResultsLoadingList()Ljava/util/List;

    move-result-object v0

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$2$1;->call(Ljava/util/List;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
