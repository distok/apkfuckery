.class public abstract Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;
.super Ljava/lang/Object;
.source "GifSearchViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;,
        Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;-><init>()V

    return-void
.end method
