.class public abstract Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;
.super Ljava/lang/Object;
.source "GifAdapterItem.kt"

# interfaces
.implements Lcom/discord/utilities/recycler/DiffKeyProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;,
        Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;,
        Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;
    }
.end annotation


# instance fields
.field private final type:Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;


# direct methods
.method private constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;->type:Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;)V

    return-void
.end method


# virtual methods
.method public final getType()Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;->type:Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;

    return-object v0
.end method
