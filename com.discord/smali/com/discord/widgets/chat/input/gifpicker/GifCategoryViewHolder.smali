.class public final Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "GifCategoryViewHolder.kt"


# instance fields
.field private final categoryIcon:Landroid/widget/ImageView;

.field private final categoryText:Landroid/widget/TextView;

.field private final previewImage:Lcom/facebook/drawee/view/SimpleDraweeView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a0488

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->previewImage:Lcom/facebook/drawee/view/SimpleDraweeView;

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;

    const v1, 0x3f666666    # 0.9f

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;-><init>(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0a0489

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryText:Landroid/widget/TextView;

    const v0, 0x7f0a0487

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryIcon:Landroid/widget/ImageView;

    return-void
.end method

.method private final setPreviewImage(Ljava/lang/String;)V
    .locals 11

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->previewImage:Lcom/facebook/drawee/view/SimpleDraweeView;

    const-string v1, "previewImage"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->previewImage:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x70

    const/4 v10, 0x0

    move-object v3, p1

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "gifCategoryItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    const-string v1, "categoryIcon"

    const-string v2, "categoryText"

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;->getGifCategory()Lcom/discord/models/gifpicker/domain/ModelGifCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/gifpicker/domain/ModelGifCategory;->getGifPreviewUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->setPreviewImage(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryText:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/gifpicker/domain/ModelGifCategory;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryIcon:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;->getGifPreviewUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->setPreviewImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryText:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v3, "itemView"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f1208f3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryIcon:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->categoryIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08024e

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder$configure$1;

    invoke-direct {v1, p2, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder$configure$1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
