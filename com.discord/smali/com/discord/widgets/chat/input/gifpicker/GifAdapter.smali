.class public final Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "GifAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

.field private static final DEFAULT_COLUMN_COUNT:I = 0x2

.field private static final MIN_COLUMN_WIDTH_DP:I = 0xa4


# instance fields
.field private final appComponent:Lcom/discord/app/AppComponent;

.field private final columnWidthPx:I

.field private final diffCreator:Lcom/discord/utilities/recycler/DiffCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;",
            ">;"
        }
    .end annotation
.end field

.field private final onSelectGif:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSelectSuggestedTerm:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppComponent;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;",
            "Lkotlin/Unit;",
            ">;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;",
            ">;)V"
        }
    .end annotation

    const-string v0, "appComponent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diffCreator"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->appComponent:Lcom/discord/app/AppComponent;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onSelectGif:Lkotlin/jvm/functions/Function1;

    iput p3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->columnWidthPx:I

    iput-object p4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onSelectSuggestedTerm:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p2

    :goto_0
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_2

    new-instance p5, Lcom/discord/utilities/recycler/DiffCreator;

    invoke-direct {p5}, Lcom/discord/utilities/recycler/DiffCreator;-><init>()V

    :cond_2
    move-object v6, p5

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;-><init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;)V

    return-void
.end method

.method public static final synthetic access$getItems$p(Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$setItems$p(Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final clearItems()V
    .locals 1

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-virtual {p0, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->setItems(Ljava/util/List;)V

    return-void
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;->getType()Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onBindViewHolder(Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;I)V
    .locals 2

    const-string v0, "holder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;

    instance-of v0, p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$Gif;

    check-cast p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;

    iget v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->columnWidthPx:I

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onSelectGif:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$Gif;->configure(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;ILkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_0
    instance-of v0, p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;

    check-cast p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onSelectSuggestedTerm:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->configure(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_1
    instance-of v0, p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;

    check-cast p2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onSelectSuggestedTerm:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->configure(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;Lkotlin/jvm/functions/Function1;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;
    .locals 4

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;->Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType$Companion;

    invoke-virtual {v1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType$Companion;->fromInt(I)Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$ViewType;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    const-string v1, "inflater.inflate(\n      \u2026        false\n          )"

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    const/4 v3, 0x1

    if-eq p2, v3, :cond_1

    const/4 v3, 0x2

    if-ne p2, v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    new-instance p2, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;

    const v3, 0x7f0d0062

    invoke-virtual {v0, v3, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;-><init>(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    new-instance p2, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$Gif;

    const v3, 0x7f0d0060

    invoke-virtual {v0, v3, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$Gif;-><init>(Landroid/view/View;)V

    :goto_1
    return-object p2
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    new-instance v3, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$setItems$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$setItems$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->items:Ljava/util/List;

    iget-object v6, p0, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->appComponent:Lcom/discord/app/AppComponent;

    move-object v2, p0

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/recycler/DiffCreator;->dispatchDiffUpdatesAsync(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;Lcom/discord/app/AppComponent;)V

    return-void
.end method
