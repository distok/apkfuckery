.class public final Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;
.super Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;
.source "GifViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SuggestedTerms"
.end annotation


# instance fields
.field private final gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

.field private final gifSearchEmptyStateIconText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    const v0, 0x7f0a0498

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    const v0, 0x7f0a0499

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateIconText:Landroid/widget/TextView;

    return-void
.end method

.method private final configureSearchTerms(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;Lkotlin/jvm/functions/Function1;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;I)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateIconText:Landroid/widget/TextView;

    const-string v1, "gifSearchEmptyStateIconText"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;->getTitleResId()I

    move-result v2

    invoke-static {v0, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;->getTerms()Ljava/util/List;

    move-result-object p1

    iget-object v2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateIconText:Landroid/widget/TextView;

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    const-string v1, "gifSearchEmptyStateFlexbox"

    if-lez v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    iget-object v4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-lt v3, v4, :cond_0

    iget-object v4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v6, 0x7f0d0061

    iget-object v7, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6, v7, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iget-object v6, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-virtual {v6, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_0
    iget-object v4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v6, "null cannot be cast to non-null type android.widget.TextView"

    invoke-static {v4, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v6, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms$configureSearchTerms$1;

    invoke-direct {v6, p2, v5}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms$configureSearchTerms$1;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    const/4 p2, 0x1

    if-ge v0, p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    sub-int/2addr p1, p2

    if-lt p1, v0, :cond_2

    :goto_1
    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateFlexbox:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    if-eq p1, v0, :cond_2

    add-int/lit8 p1, p1, -0x1

    goto :goto_1

    :cond_2
    new-instance p1, Landroidx/recyclerview/widget/StaggeredGridLayoutManager$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, p3}, Landroidx/recyclerview/widget/StaggeredGridLayoutManager$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/StaggeredGridLayoutManager$LayoutParams;->setFullSpan(Z)V

    iget-object p2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string p3, "itemView"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;Lkotlin/jvm/functions/Function1;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsEmptyResults;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "termsItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateIconText:Landroid/widget/TextView;

    const-string v0, "gifSearchEmptyStateIconText"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v8, "itemView"

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0405b5

    const/4 v9, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v2, v9, v3, v4}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/view/View;IIILjava/lang/Object;)I

    move-result v3

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->configureSearchTerms(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;Lkotlin/jvm/functions/Function1;I)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result p2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    invoke-virtual {p1, p2, v9, v0, v9}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method public final configure(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem$SuggestedTermsNonEmptyResults;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "termsItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->gifSearchEmptyStateIconText:Landroid/widget/TextView;

    const-string v0, "gifSearchEmptyStateIconText"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;ILjava/lang/Object;)V

    const/4 v0, -0x2

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifViewHolder$SuggestedTerms;->configureSearchTerms(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$SuggestedTermsItem;Lkotlin/jvm/functions/Function1;I)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p2, 0x20

    invoke-static {p2}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result p2

    const/16 v0, 0x28

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p1, v1, p2, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method
