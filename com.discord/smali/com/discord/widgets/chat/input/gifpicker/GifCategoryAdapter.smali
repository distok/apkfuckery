.class public final Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "GifCategoryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final appComponent:Lcom/discord/app/AppComponent;

.field private final diffCreator:Lcom/discord/utilities/recycler/DiffCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final onSelectGifCategory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppComponent;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;",
            ">;)V"
        }
    .end annotation

    const-string v0, "appComponent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diffCreator"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->appComponent:Lcom/discord/app/AppComponent;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->onSelectGifCategory:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->items:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    new-instance p3, Lcom/discord/utilities/recycler/DiffCreator;

    invoke-direct {p3}, Lcom/discord/utilities/recycler/DiffCreator;-><init>()V

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;-><init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;)V

    return-void
.end method

.method public static final synthetic access$getItems$p(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->items:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$setItems$p(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->onBindViewHolder(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->onSelectGifCategory:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;->configure(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;
    .locals 2

    const-string p2, "parent"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0d005f

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;-><init>(Landroid/view/View;)V

    return-object p2
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    new-instance v3, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter$setItems$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter$setItems$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->items:Ljava/util/List;

    iget-object v6, p0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryAdapter;->appComponent:Lcom/discord/app/AppComponent;

    move-object v2, p0

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/recycler/DiffCreator;->dispatchDiffUpdatesAsync(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;Lcom/discord/app/AppComponent;)V

    return-void
.end method
