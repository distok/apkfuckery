.class public abstract Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;
.super Ljava/lang/Object;
.source "GifCategoryItem.kt"

# interfaces
.implements Lcom/discord/utilities/recycler/DiffKeyProvider;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;,
        Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;-><init>()V

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
