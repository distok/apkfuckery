.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$1;
.super Ljava/lang/Object;
.source "GifSearchViewModel.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->observeQueryState(Ljava/util/List;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$1;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory$observeQueryState$1;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;->access$getStoreAnalytics$p(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;)Lcom/discord/stores/StoreAnalytics;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/analytics/SearchType;->GIF:Lcom/discord/utilities/analytics/SearchType;

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAnalytics;->trackSearchStarted(Lcom/discord/utilities/analytics/SearchType;)V

    return-void
.end method
