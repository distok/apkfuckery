.class public final Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded$gifCount$2;
.super Lx/m/c/k;
.source "GifSearchViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;-><init>(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded$gifCount$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded$gifCount$2;->this$0:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;->getAdapterItems()Ljava/util/List;

    move-result-object v0

    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;

    instance-of v1, v1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;

    if-eqz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-ltz v2, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Lx/h/f;->throwCountOverflow()V

    const/4 v0, 0x0

    throw v0

    :cond_3
    :goto_1
    return v2
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded$gifCount$2;->invoke()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
