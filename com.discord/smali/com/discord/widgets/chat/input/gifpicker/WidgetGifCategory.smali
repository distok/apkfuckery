.class public final Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;
.super Lcom/discord/app/AppFragment;
.source "WidgetGifCategory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final ARG_GIF_CATEGORY_ITEM:Ljava/lang/String; = "GIF_CATEGORY_ITEM"

.field public static final Companion:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$Companion;


# instance fields
.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

.field private gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

.field private gifCategoryViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

.field private final gifRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifsLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onGifSelected:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final title$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    const-string/jumbo v6, "title"

    const-string v7, "getTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    const-string v6, "gifRecycler"

    const-string v7, "getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    const-string v6, "gifsLoadingView"

    const-string v7, "getGifsLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->Companion:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0485

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a048b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->title$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0486

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a048a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifsLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getGifRecycler$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->handleBack()V

    return-void
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->handleViewState(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$selectGif(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V

    return-void
.end method

.method private final getContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getGifCategory()Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GIF_CATEGORY_ITEM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.widgets.chat.input.gifpicker.GifCategoryItem"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    return-object v0
.end method

.method private final getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getGifsLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifsLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    return-object v0
.end method

.method private final getTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->title$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleBack()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->clickBack()V

    return-void

    :cond_0
    const-string v0, "expressionTrayViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final handleViewState(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifsLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;->getGifItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->setItems(Ljava/util/List;)V

    return-void

    :cond_0
    const-string p1, "gifAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifCategoryViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;->selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->onGifSelected:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void

    :cond_1
    const-string p1, "gifCategoryViewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final setUpBackBehavior()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setUpBackBehavior$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setUpBackBehavior$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/discord/app/AppFragment;->setOnBackPressed(Lrx/functions/Func0;I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setUpBackBehavior$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setUpBackBehavior$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setUpGifRecycler(I)V
    .locals 11

    new-instance v0, Landroidx/recyclerview/widget/StaggeredGridLayoutManager;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Landroidx/recyclerview/widget/StaggeredGridLayoutManager;-><init>(II)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    new-instance v10, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    sget-object v2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-virtual {v2, v3, p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;->calculateColumnWidth(Landroidx/recyclerview/widget/RecyclerView;II)I

    move-result v5

    new-instance v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setUpGifRecycler$1;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setUpGifRecycler$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x18

    const/4 v9, 0x0

    move-object v2, v10

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;-><init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v10, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance v1, Ll;

    invoke-direct {v1, v0, p1}, Ll;-><init>(II)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    :cond_0
    const-string p1, "gifAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setUpTitle()V
    .locals 3

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifCategory()Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getTitle()Landroid/widget/TextView;

    move-result-object v1

    instance-of v2, v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Standard;->getGifCategory()Lcom/discord/models/gifpicker/domain/ModelGifCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/gifpicker/domain/ModelGifCategory;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v0, v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem$Trending;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f1208f3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method private final setWindowInsetsListeners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setWindowInsetsListeners$1;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setWindowInsetsListeners$1;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getContainer()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setWindowInsetsListeners$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$setWindowInsetsListeners$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01f9

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v7, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreStickers;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, v0, v7}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026rayViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v6, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "requireContext()"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifCategory()Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    move-result-object v2

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Factory;-><init>(Landroid/content/Context;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lcom/discord/stores/StoreGifPicker;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, p0, v6}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026oryViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifCategoryViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;->calculateColumnCount(Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->setUpTitle()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->setUpBackBehavior()V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->setUpGifRecycler(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->getGifsLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->updateView$default(Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;IIILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->setWindowInsetsListeners()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->gifCategoryViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "gifCategoryViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final setOnGifSelected(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onSelected"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-void
.end method
