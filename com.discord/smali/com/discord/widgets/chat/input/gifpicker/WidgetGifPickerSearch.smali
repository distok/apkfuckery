.class public final Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;
.super Lcom/discord/app/AppFragment;
.source "WidgetGifPickerSearch.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

.field private final gifLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private gifPickerViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

.field private final gifRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifSearchInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifSearchInputClear$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifSearchViewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onGifSelected:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const-string v3, "gifSearchViewFlipper"

    const-string v4, "getGifSearchViewFlipper()Lcom/discord/app/AppViewFlipper;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const-string v6, "gifLoadingView"

    const-string v7, "getGifLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const-string v6, "gifRecycler"

    const-string v7, "getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const-string v6, "gifSearchInput"

    const-string v7, "getGifSearchInput()Lcom/google/android/material/textfield/TextInputEditText;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const-string v6, "gifSearchInputClear"

    const-string v7, "getGifSearchInputClear()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a049b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifSearchViewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0495

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0493

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0494

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifSearchInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0492

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifSearchInputClear$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getGifPickerViewModel$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifPickerViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "gifPickerViewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getOnGifSelected$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)Lkotlin/jvm/functions/Function0;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$handleInputChanged(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->handleInputChanged(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->handleViewState(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$setGifPickerViewModel$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifPickerViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    return-void
.end method

.method public static final synthetic access$setOnGifSelected$p(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method private final getGifLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifLoadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    return-object v0
.end method

.method private final getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getGifSearchInput()Lcom/google/android/material/textfield/TextInputEditText;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifSearchInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputEditText;

    return-object v0
.end method

.method private final getGifSearchInputClear()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifSearchInputClear$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGifSearchViewFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifSearchViewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final handleInputChanged(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifPickerViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;->setSearchText(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInputClear()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_0

    const v1, 0x7f080412

    goto :goto_0

    :cond_0
    const v1, 0x7f0802b8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInputClear()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040178

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040179

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInputClear()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_2

    const p1, 0x7f121615

    goto :goto_2

    :cond_2
    const p1, 0x7f121539

    :goto_2
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    const-string p1, "gifPickerViewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final handleViewState(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;)V
    .locals 3

    instance-of v0, p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    const/4 v1, 0x0

    const-string v2, "gifAdapter"

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;->getAdapterItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->setItems(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchViewFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    sget-object v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;->INSTANCE:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->clearItems()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchViewFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    :goto_0
    return-void
.end method

.method private final setUpGifRecycler(I)V
    .locals 11

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Landroidx/recyclerview/widget/StaggeredGridLayoutManager;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Landroidx/recyclerview/widget/StaggeredGridLayoutManager;-><init>(II)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    new-instance v10, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    new-instance v4, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)V

    sget-object v2, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-virtual {v2, v3, p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;->calculateColumnWidth(Landroidx/recyclerview/widget/RecyclerView;II)I

    move-result v5

    new-instance v6, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$2;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v2

    invoke-direct {v6, v2}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setUpGifRecycler$2;-><init>(Lcom/google/android/material/textfield/TextInputEditText;)V

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x0

    move-object v2, v10

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;-><init>(Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v10, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifAdapter:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance v1, Ll;

    invoke-direct {v1, v0, p1}, Ll;-><init>(II)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    :cond_0
    const-string p1, "gifAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setupSearchBar()V
    .locals 3

    sget-object v0, Lcom/discord/utilities/view/text/TextWatcher;->Companion:Lcom/discord/utilities/view/text/TextWatcher$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setupSearchBar$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setupSearchBar$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/discord/utilities/view/text/TextWatcher$Companion;->addBindedTextWatcher(Landroidx/fragment/app/Fragment;Landroid/widget/TextView;Lrx/functions/Action1;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInputClear()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setupSearchBar$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$setupSearchBar$2;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final clearSearchBar()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01fb

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v7, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "requireContext()"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Factory;-><init>(Landroid/content/Context;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGifPicker;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, p0, v7}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026rchViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifPickerViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object p1, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;->Companion:Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/input/gifpicker/GifAdapter$Companion;->calculateColumnCount(Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->setUpGifRecycler(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->setupSearchBar()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifLoadingView()Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;->updateView$default(Lcom/discord/widgets/chat/input/gifpicker/GifLoadingView;IIILjava/lang/Object;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 11

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->getGifSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->gifPickerViewModel:Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "gifPickerViewModel\n     \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$onViewBoundOrOnResume$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "gifPickerViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final setOnGifSelected(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onSelected"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;->onGifSelected:Lkotlin/jvm/functions/Function0;

    return-void
.end method
