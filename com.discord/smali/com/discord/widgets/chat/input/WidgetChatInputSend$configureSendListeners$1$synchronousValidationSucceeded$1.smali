.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$1;
.super Lx/m/c/k;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke(Ljava/util/List;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/MessageManager$MessageSendResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $messageSendResultHandler:Lkotlin/jvm/functions/Function2;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function2;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$1;->$messageSendResultHandler:Lkotlin/jvm/functions/Function2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/MessageManager$MessageSendResult;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$1;->invoke(Lcom/discord/widgets/chat/MessageManager$MessageSendResult;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/MessageManager$MessageSendResult;)V
    .locals 2

    const-string v0, "messageSendResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$1;->$messageSendResultHandler:Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/MessageManager$MessageSendResult;->getMessageResult()Lcom/discord/utilities/messagesend/MessageResult;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/MessageManager$MessageSendResult;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
