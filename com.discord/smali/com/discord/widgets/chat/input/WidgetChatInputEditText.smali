.class public final Lcom/discord/widgets/chat/input/WidgetChatInputEditText;
.super Ljava/lang/Object;
.source "WidgetChatInputEditText.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;


# instance fields
.field private channelId:J

.field private final chatInputCommands:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

.field private final editText:Lcom/lytefast/flexinput/widget/FlexEditText;

.field private final emptyTextSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lastTypingEmissionMillis:J

.field private onSendListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/lytefast/flexinput/widget/FlexEditText;Landroidx/recyclerview/widget/RecyclerView;Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 2

    const-string v0, "editText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mentionsRecycler"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commandsRoot"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create(true)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->emptyTextSubject:Lrx/subjects/Subject;

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;-><init>(Lcom/lytefast/flexinput/widget/FlexEditText;Landroidx/recyclerview/widget/RecyclerView;Landroidx/constraintlayout/widget/ConstraintLayout;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->chatInputCommands:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setOnSelectionChangedListener()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setOnTextChangedListener()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setSoftwareKeyboardSendBehavior()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setHardwareKeyboardSendBehavior()V

    return-void
.end method

.method public static final synthetic access$getChatInputCommands$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->chatInputCommands:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    return-object p0
.end method

.method public static final synthetic access$getEmptyTextSubject$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)Lrx/subjects/Subject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->emptyTextSubject:Lrx/subjects/Subject;

    return-object p0
.end method

.method public static final synthetic access$getLastTypingEmissionMillis$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->lastTypingEmissionMillis:J

    return-wide v0
.end method

.method public static final synthetic access$setLastTypingEmissionMillis$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->lastTypingEmissionMillis:J

    return-void
.end method

.method private final setHardwareKeyboardSendBehavior()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setHardwareKeyboardSendBehavior$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setHardwareKeyboardSendBehavior$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method private final setOnSelectionChangedListener()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnSelectionChangedListener$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnSelectionChangedListener$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/widget/FlexEditText;->setOnSelectionChangedListener(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private final setOnTextChangedListener()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final setSoftwareKeyboardSendBehavior()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setSoftwareKeyboardSendBehavior$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setSoftwareKeyboardSendBehavior$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method


# virtual methods
.method public final clearLastTypingEmission()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->lastTypingEmissionMillis:J

    return-void
.end method

.method public final configureMentionsDataSubscriptions(Lcom/discord/app/AppFragment;)V
    .locals 2

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->chatInputCommands:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->emptyTextSubject:Lrx/subjects/Subject;

    invoke-virtual {v0, p1, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V

    return-void
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->channelId:J

    return-wide v0
.end method

.method public final getMatchedContentWithMetaData()Lcom/discord/models/domain/ModelMessage$Content;
    .locals 8

    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;->toStringSafe(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-gt v4, v1, :cond_5

    if-nez v5, :cond_0

    move v6, v4

    goto :goto_1

    :cond_0
    move v6, v1

    :goto_1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    invoke-static {v6, v7}, Lx/m/c/j;->compare(II)I

    move-result v6

    if-gtz v6, :cond_1

    const/4 v6, 0x1

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    :goto_2
    if-nez v5, :cond_3

    if-nez v6, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    if-nez v6, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_5
    :goto_3
    add-int/2addr v1, v2

    invoke-interface {v0, v4, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->chatInputCommands:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->replaceMatches(Ljava/lang/String;)Lcom/discord/models/domain/ModelMessage$Content;

    move-result-object v0

    goto :goto_4

    :cond_6
    new-instance v1, Lcom/discord/models/domain/ModelMessage$Content;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/discord/models/domain/ModelMessage$Content;-><init>(Ljava/lang/String;Ljava/util/List;)V

    move-object v0, v1

    :goto_4
    return-object v0
.end method

.method public final getOnSendListener()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->onSendListener:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final saveText()V
    .locals 4

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->channelId:J

    iget-object v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v3}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/stores/StoreChat;->setTextChannelInput(JLjava/lang/CharSequence;)V

    return-void
.end method

.method public final setChannelId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->channelId:J

    return-void
.end method

.method public final setChannelId(JZ)V
    .locals 0

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->saveText()V

    :cond_0
    iput-wide p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->channelId:J

    return-void
.end method

.method public final setOnSendListener(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->onSendListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method
