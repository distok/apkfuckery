.class public final Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;
.super Lx/m/c/k;
.source "WidgetChatInput.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInput;->showExpressionButtonSparkle(Lcom/discord/tooltips/TooltipManager$Tooltip;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $tooltip:Lcom/discord/tooltips/TooltipManager$Tooltip;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/tooltips/TooltipManager$Tooltip;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->$tooltip:Lcom/discord/tooltips/TooltipManager$Tooltip;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 13

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getFlexInputFragment$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object v0

    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v12, Lcom/discord/tooltips/SparkleView;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v2}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getContainer$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "container.context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-direct {v12, v2, v1, v3}, Lcom/discord/tooltips/SparkleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v1}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getTooltipManager$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/discord/tooltips/TooltipManager;

    move-result-object v1

    iget-object v4, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->$tooltip:Lcom/discord/tooltips/TooltipManager$Tooltip;

    sget-object v5, Lcom/discord/floating_view_manager/FloatingViewGravity;->CENTER:Lcom/discord/floating_view_manager/FloatingViewGravity;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-virtual {v2}, Lcom/discord/app/AppFragment;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1$1;

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v9

    const-string/jumbo v2, "unsubscribeSignal.map { Unit }"

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v10, 0x70

    const/4 v11, 0x0

    move-object v2, v0

    move-object v3, v12

    invoke-static/range {v1 .. v11}, Lcom/discord/tooltips/TooltipManager;->e(Lcom/discord/tooltips/TooltipManager;Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;ILjava/lang/Object;)V

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1$2;

    invoke-direct {v1, p0, v0}, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;Landroid/view/View;)V

    invoke-virtual {v12, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const-string v0, "expressionBtn"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
