.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;
.super Ljava/lang/Object;
.source "WidgetEmojiAdapter.kt"

# interfaces
.implements Lrx/functions/Action3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action3<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $adapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;->$adapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 0

    const-string p1, "null cannot be cast to non-null type com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter.EmojiItem"

    invoke-static {p3, p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p3, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;

    invoke-virtual {p3}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->isUsable()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->isAvailable()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreEmoji;->onEmojiUsed(Lcom/discord/models/domain/emoji/Emoji;)V

    :cond_0
    iget-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;->$adapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    invoke-static {p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->access$getOnEmojiSelectedListener$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;->onEmojiSelected(Lcom/discord/models/domain/emoji/Emoji;)V

    :try_start_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->access$getDraweeView$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;)Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->performHapticFeedback(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;->call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
