.class public Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;
.super Lf/a/b/l0;
.source "EmojiPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;,
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;,
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final locale:Ljava/util/Locale;

.field private final searchSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCategoryItemIdSubject"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->locale:Ljava/util/Locale;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    const-string p3, "PublishSubject.create()"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v7, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$1;-><init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;)V
    .locals 17
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    instance-of v2, v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    if-eqz v2, :cond_d

    check-cast v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getEmojiSet()Lcom/discord/models/domain/emoji/EmojiSet;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getSelectedCategoryItemId()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getAllowEmojisToAnimate()Z

    move-result v5

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->locale:Ljava/util/Locale;

    const-string v8, "null cannot be cast to non-null type java.lang.String"

    invoke-static {v6, v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getAllGuilds()Ljava/util/LinkedHashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v9

    const-string/jumbo v10, "storeState.allGuilds.values"

    invoke-static {v9, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v9}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v9

    sget-object v10, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;

    iget-object v11, v2, Lcom/discord/models/domain/emoji/EmojiSet;->recentEmojis:Ljava/util/List;

    invoke-static {v10, v11, v6, v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;->access$buildEmojiListItems(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;Ljava/util/List;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    move-result v12

    const/4 v13, 0x1

    xor-int/2addr v12, v13

    const/4 v14, 0x0

    if-eqz v12, :cond_1

    new-instance v12, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;

    sget-object v15, Lcom/discord/models/domain/emoji/EmojiCategory;->RECENT:Lcom/discord/models/domain/emoji/EmojiCategory;

    invoke-direct {v12, v15}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;-><init>(Lcom/discord/models/domain/emoji/EmojiCategory;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    sget-object v11, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    invoke-virtual {v11, v15}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;->mapEmojiCategoryToItemId(Lcom/discord/models/domain/emoji/EmojiCategory;)J

    move-result-wide v11

    cmp-long v16, v11, v3

    if-nez v16, :cond_0

    const/4 v11, 0x1

    goto :goto_0

    :cond_0
    const/4 v11, 0x0

    :goto_0
    new-instance v12, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    new-instance v13, Lkotlin/Pair;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-direct {v13, v0, v14}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v12, v15, v13, v11}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;-><init>(Lcom/discord/models/domain/emoji/EmojiCategory;Lkotlin/Pair;Z)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getEmojiContext()Lcom/discord/stores/StoreEmoji$EmojiContext;

    move-result-object v0

    instance-of v11, v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;

    if-eqz v11, :cond_2

    check-cast v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;

    invoke-virtual {v0}, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;->getGuildId()J

    move-result-wide v11

    goto :goto_1

    :cond_2
    const-wide/16 v11, 0x0

    :goto_1
    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getAllGuilds()Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuild;

    iget-object v13, v2, Lcom/discord/models/domain/emoji/EmojiSet;->customEmojis:Ljava/util/Map;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v13, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    if-eqz v0, :cond_4

    if-eqz v11, :cond_4

    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    move-result v11

    const/4 v12, 0x1

    xor-int/2addr v11, v12

    if-eqz v11, :cond_4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-static {v10, v0, v2, v6, v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;->access$buildGuildEmojiListItems(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/emoji/EmojiSet;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v10

    invoke-direct {v12, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v7, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    sget-object v10, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    invoke-virtual {v10, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;->mapGuildToItemId(Lcom/discord/models/domain/ModelGuild;)J

    move-result-wide v12

    cmp-long v10, v12, v3

    if-nez v10, :cond_3

    const/4 v10, 0x1

    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    :goto_2
    new-instance v12, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    new-instance v13, Lkotlin/Pair;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-direct {v13, v11, v14}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v12, v0, v13, v10}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;-><init>(Lcom/discord/models/domain/ModelGuild;Lkotlin/Pair;Z)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v9, v0}, Lx/h/f;->minus(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/models/domain/ModelGuild;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    sget-object v11, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;

    invoke-static {v11, v9, v2, v6, v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;->access$buildGuildEmojiListItems(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/emoji/EmojiSet;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    move-result v12

    const/4 v13, 0x1

    xor-int/2addr v12, v13

    if-eqz v12, :cond_5

    invoke-interface {v7, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    sget-object v11, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    const-string v12, "guild"

    invoke-static {v9, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;->mapGuildToItemId(Lcom/discord/models/domain/ModelGuild;)J

    move-result-wide v11

    cmp-long v13, v11, v3

    if-nez v13, :cond_6

    const/4 v11, 0x1

    goto :goto_4

    :cond_6
    const/4 v11, 0x0

    :goto_4
    new-instance v12, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    new-instance v13, Lkotlin/Pair;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-direct {v13, v10, v14}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v12, v9, v13, v11}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;-><init>(Lcom/discord/models/domain/ModelGuild;Lkotlin/Pair;Z)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-object v0, v2, Lcom/discord/models/domain/emoji/EmojiSet;->unicodeEmojis:Ljava/util/Map;

    invoke-static {}, Lcom/discord/models/domain/emoji/EmojiCategory;->values()[Lcom/discord/models/domain/emoji/EmojiCategory;

    move-result-object v2

    const/16 v9, 0xa

    const/4 v10, 0x0

    :goto_5
    if-ge v10, v9, :cond_b

    aget-object v11, v2, v10

    invoke-interface {v0, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_8

    goto :goto_7

    :cond_8
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v12

    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    sget-object v14, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;

    invoke-static {v14, v13, v6, v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;->access$buildEmojiListItems(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;Ljava/util/List;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->isEmpty()Z

    move-result v14

    const/4 v15, 0x1

    xor-int/2addr v14, v15

    if-eqz v14, :cond_a

    new-instance v14, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;

    invoke-direct {v14, v11}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;-><init>(Lcom/discord/models/domain/emoji/EmojiCategory;)V

    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    sget-object v13, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    invoke-virtual {v13, v11}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;->mapEmojiCategoryToItemId(Lcom/discord/models/domain/emoji/EmojiCategory;)J

    move-result-wide v13

    cmp-long v15, v13, v3

    if-nez v15, :cond_9

    const/4 v13, 0x1

    goto :goto_6

    :cond_9
    const/4 v13, 0x0

    :goto_6
    new-instance v14, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    new-instance v15, Lkotlin/Pair;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v15, v12, v9}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v14, v11, v15, v13}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;-><init>(Lcom/discord/models/domain/emoji/EmojiCategory;Lkotlin/Pair;Z)V

    invoke-interface {v8, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    :goto_7
    add-int/lit8 v10, v10, 0x1

    const/16 v9, 0xa

    goto :goto_5

    :cond_b
    invoke-interface {v7}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_c

    const-string v0, ""

    invoke-static {v6, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    if-eqz v0, :cond_c

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;-><init>(Ljava/lang/String;)V

    move-object/from16 v2, p0

    invoke-virtual {v2, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_8

    :cond_c
    move-object/from16 v2, p0

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v7, v8}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v2, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_8

    :cond_d
    move-object v2, v0

    :goto_8
    return-void
.end method


# virtual methods
.method public final observeEvents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public final onClickUnicodeEmojiCategories()V
    .locals 3

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;->getFirstUnicodeEmojiCategoryItem()Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->getStableId()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->setSelectedCategoryItemId(J)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;->getCategoryRange()Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-direct {v2, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;-><init>(I)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final setSearchText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final setSelectedCategoryItemId(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
