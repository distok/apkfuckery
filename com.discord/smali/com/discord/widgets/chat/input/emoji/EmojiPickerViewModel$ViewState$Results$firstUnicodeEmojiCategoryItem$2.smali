.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2;
.super Lx/m/c/k;
.source "EmojiPickerViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;->getCategoryItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->isUnicodeEmojiCategory()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    instance-of v0, v1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    check-cast v2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    return-object v2
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2;->invoke()Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    move-result-object v0

    return-object v0
.end method
