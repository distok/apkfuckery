.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;
.super Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;
.source "WidgetEmojiAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;,
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;,
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;,
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;,
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$Companion;

.field private static final DEFAULT_NUM_COLUMNS:I = 0x8

.field private static final ITEM_TYPE_EMOJI:I = 0x1

.field private static final MAX_EMOJI_SIZE_PX:I = 0x40


# instance fields
.field private final emojiSizePx:I

.field private final layoutManager:Landroidx/recyclerview/widget/GridLayoutManager;

.field private final numColumns:I

.field private final onEmojiSelectedListener:Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;)V
    .locals 2

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEmojiSelectedListener"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->onEmojiSelectedListener:Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "recycler.context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v1, 0x7f07007e

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->emojiSizePx:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    const/16 v1, 0x8

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;->calculateNumOfColumns(Landroidx/recyclerview/widget/RecyclerView;FI)I

    move-result p2

    iput p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->numColumns:I

    new-instance p2, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->getNumColumns()I

    move-result v1

    invoke-direct {p2, v0, v1}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->layoutManager:Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-virtual {p1, p0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public static final synthetic access$getEmojiSizePx$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)I
    .locals 0

    iget p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->emojiSizePx:I

    return p0
.end method

.method public static final synthetic access$getOnEmojiSelectedListener$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->onEmojiSelectedListener:Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;

    return-object p0
.end method


# virtual methods
.method public createStickyHeaderViewHolder(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V

    return-object v0
.end method

.method public bridge synthetic createStickyHeaderViewHolder(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->createStickyHeaderViewHolder(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->layoutManager:Landroidx/recyclerview/widget/GridLayoutManager;

    return-object v0
.end method

.method public getNumColumns()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->numColumns:I

    return v0
.end method

.method public isHeader(I)Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getInternalData()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "this.internalData"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;

    return p1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V

    :goto_0
    return-object p1
.end method
