.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;
.super Ljava/lang/Object;
.source "EmojiPickerNavigator.kt"


# static fields
.field public static final ARG_EMOJI_CONTEXT_TYPE:Ljava/lang/String; = "EMOJI_CONTEXT_TYPE"

.field public static final ARG_MODE:Ljava/lang/String; = "MODE"

.field public static final INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;

.field public static final INTENT_EXTRA_EMOJI_CONTEXT_TYPE:Ljava/lang/String; = "INTENT_EXTRA_EMOJI_CONTEXT_TYPE"

.field public static final RESULT_METADATA_PAYLOAD:Ljava/lang/String; = "RESULT_METADATA_PAYLOAD"


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final launchBottomSheet(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;",
            "Lcom/discord/widgets/chat/input/emoji/EmojiContextType;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "fragmentManager"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic launchBottomSheet$default(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x8

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->launchBottomSheet(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
