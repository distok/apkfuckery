.class public abstract Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;
.super Ljava/lang/Object;
.source "EmojiPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;,
        Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;
    }
.end annotation


# instance fields
.field private final searchQuery:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;->searchQuery:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getSearchQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;->searchQuery:Ljava/lang/String;

    return-object v0
.end method
