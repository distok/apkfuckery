.class public final synthetic Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/discord/models/domain/emoji/EmojiCategory;->values()[Lcom/discord/models/domain/emoji/EmojiCategory;

    const/16 v0, 0xa

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/models/domain/emoji/EmojiCategory;->PEOPLE:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v2, 0x2

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v4, Lcom/discord/models/domain/emoji/EmojiCategory;->NATURE:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v4, 0x3

    aput v2, v1, v4

    sget-object v2, Lcom/discord/models/domain/emoji/EmojiCategory;->FOOD:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v2, 0x4

    aput v4, v1, v2

    sget-object v4, Lcom/discord/models/domain/emoji/EmojiCategory;->ACTIVITY:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v4, 0x5

    aput v2, v1, v4

    sget-object v2, Lcom/discord/models/domain/emoji/EmojiCategory;->TRAVEL:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v2, 0x6

    aput v4, v1, v2

    sget-object v4, Lcom/discord/models/domain/emoji/EmojiCategory;->OBJECTS:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v4, 0x7

    aput v2, v1, v4

    sget-object v2, Lcom/discord/models/domain/emoji/EmojiCategory;->SYMBOLS:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/16 v2, 0x8

    aput v4, v1, v2

    sget-object v4, Lcom/discord/models/domain/emoji/EmojiCategory;->FLAGS:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/16 v4, 0x9

    aput v2, v1, v4

    sget-object v2, Lcom/discord/models/domain/emoji/EmojiCategory;->RECENT:Lcom/discord/models/domain/emoji/EmojiCategory;

    const/4 v2, 0x0

    aput v4, v1, v2

    sget-object v2, Lcom/discord/models/domain/emoji/EmojiCategory;->CUSTOM:Lcom/discord/models/domain/emoji/EmojiCategory;

    aput v0, v1, v3

    return-void
.end method
