.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetEmojiAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmojiViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# instance fields
.field private final draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V
    .locals 2

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d004d

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0a03a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.emoji_item_draweeview)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V

    const/4 p1, 0x0

    new-array v1, p1, [Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    sget-object v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$2;->INSTANCE:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder$2;

    new-array p1, p1, [Landroid/view/View;

    invoke-virtual {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnLongClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$getDraweeView$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;)Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object p0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 11

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    instance-of p1, p2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;

    if-nez p1, :cond_0

    const/4 p2, 0x0

    :cond_0
    check-cast p2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;->access$getEmojiSizePx$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)I

    move-result v0

    invoke-static {v0}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v0

    const/16 v1, 0x40

    if-le v0, v1, :cond_1

    const/16 v0, 0x40

    :cond_1
    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;->getAllowEmojisToAnimate()Z

    move-result p2

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/discord/models/domain/emoji/Emoji;->getImageUri(ZILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x6c

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->isUsable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/16 v0, 0xff

    goto :goto_1

    :cond_3
    :goto_0
    const/16 v0, 0x64

    :goto_1
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageAlpha(I)V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->draweeView:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->getFirstName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_4
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
