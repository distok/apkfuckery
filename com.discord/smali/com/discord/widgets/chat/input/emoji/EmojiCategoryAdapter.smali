.class public final Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "EmojiCategoryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final appComponent:Lcom/discord/app/AppComponent;

.field private final diffCreator:Lcom/discord/utilities/recycler/DiffCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final onCategoryClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSelectedItemAdapterPositionUpdated:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;",
            ">;",
            "Lcom/discord/app/AppComponent;",
            ")V"
        }
    .end annotation

    const-string v0, "onCategoryClicked"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSelectedItemAdapterPositionUpdated"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diffCreator"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appComponent"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onCategoryClicked:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onSelectedItemAdapterPositionUpdated:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->appComponent:Lcom/discord/app/AppComponent;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    new-instance p3, Lcom/discord/utilities/recycler/DiffCreator;

    invoke-direct {p3}, Lcom/discord/utilities/recycler/DiffCreator;-><init>()V

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;)V

    return-void
.end method

.method public static final synthetic access$getItems$p(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getOnSelectedItemAdapterPositionUpdated$p(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onSelectedItemAdapterPositionUpdated:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$setItems$p(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getItemAtPosition(I)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    return-object p1
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->getStableId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    instance-of v0, p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    instance-of p1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    :goto_0
    return p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onBindViewHolder(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    instance-of v0, p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;

    check-cast p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onCategoryClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->configure(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_0
    instance-of v0, p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;

    check-cast p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onCategoryClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;->configure(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;Lkotlin/jvm/functions/Function1;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;
    .locals 4

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const-string v1, "itemView"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p2, v3, :cond_0

    const p2, 0x7f0d004c

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const p2, 0x7f0d004b

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;-><init>(Landroid/view/View;)V

    :goto_0
    return-object p2
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    new-instance v3, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter$setItems$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter$setItems$1;-><init>(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->items:Ljava/util/List;

    iget-object v6, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->appComponent:Lcom/discord/app/AppComponent;

    move-object v2, p0

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/recycler/DiffCreator;->dispatchDiffUpdatesAsync(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;Lcom/discord/app/AppComponent;)V

    return-void
.end method
