.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;
.super Ljava/lang/Object;
.source "EmojiPickerViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->getEmojiContextObservable(Lcom/discord/widgets/chat/input/emoji/EmojiContextType;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/stores/StoreEmoji$EmojiContext;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;->INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/stores/StoreEmoji$EmojiContext;
    .locals 5

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "selectedChannel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;-><init>(JJ)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;->call(Lcom/discord/models/domain/ModelChannel;)Lcom/discord/stores/StoreEmoji$EmojiContext;

    move-result-object p1

    return-object p1
.end method
