.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder$Companion;
.super Ljava/lang/Object;
.source "WidgetEmojiAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCategoryString(Lcom/discord/models/domain/emoji/EmojiCategory;)I
    .locals 1
    .annotation build Landroidx/annotation/StringRes;
    .end annotation

    const-string v0, "emojiCategory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    const p1, 0x7f1216f4

    goto :goto_0

    :pswitch_0
    const p1, 0x7f120690

    goto :goto_0

    :pswitch_1
    const p1, 0x7f120689

    goto :goto_0

    :pswitch_2
    const p1, 0x7f12068b

    goto :goto_0

    :pswitch_3
    const p1, 0x7f120691

    goto :goto_0

    :pswitch_4
    const p1, 0x7f12068e

    goto :goto_0

    :pswitch_5
    const p1, 0x7f120692

    goto :goto_0

    :pswitch_6
    const p1, 0x7f120688

    goto :goto_0

    :pswitch_7
    const p1, 0x7f12068c

    goto :goto_0

    :pswitch_8
    const p1, 0x7f12068d

    goto :goto_0

    :pswitch_9
    const p1, 0x7f12068f

    :goto_0
    return p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
