.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;
.super Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;
.source "EmojiPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Emoji"
.end annotation


# instance fields
.field private final allGuilds:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation
.end field

.field private final allowEmojisToAnimate:Z

.field private final emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

.field private final emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

.field private final searchInputStringUpper:Ljava/lang/String;

.field private final selectedCategoryItemId:J


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/emoji/EmojiSet;",
            "Lcom/discord/stores/StoreEmoji$EmojiContext;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Ljava/lang/String;",
            "ZJ)V"
        }
    .end annotation

    const-string v0, "emojiSet"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emojiContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allGuilds"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchInputStringUpper"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    iput-wide p6, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJILjava/lang/Object;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-wide p6, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    :cond_5
    move-wide v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move-wide p8, v3

    invoke-virtual/range {p2 .. p9}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->copy(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJ)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/emoji/EmojiSet;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreEmoji$EmojiContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    return-object v0
.end method

.method public final component3()Ljava/util/LinkedHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    return v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    return-wide v0
.end method

.method public final copy(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJ)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/emoji/EmojiSet;",
            "Lcom/discord/stores/StoreEmoji$EmojiContext;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Ljava/lang/String;",
            "ZJ)",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;"
        }
    .end annotation

    const-string v0, "emojiSet"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emojiContext"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allGuilds"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchInputStringUpper"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    move-object v1, v0

    move v6, p5

    move-wide v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;-><init>(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    iget-wide v2, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllGuilds()Ljava/util/LinkedHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public final getAllowEmojisToAnimate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    return v0
.end method

.method public final getEmojiContext()Lcom/discord/stores/StoreEmoji$EmojiContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    return-object v0
.end method

.method public final getEmojiSet()Lcom/discord/models/domain/emoji/EmojiSet;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    return-object v0
.end method

.method public final getSearchInputStringUpper()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelectedCategoryItemId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/emoji/EmojiSet;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Emoji(emojiSet="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiSet:Lcom/discord/models/domain/emoji/EmojiSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emojiContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allGuilds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allGuilds:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchInputStringUpper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->searchInputStringUpper:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", allowEmojisToAnimate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->allowEmojisToAnimate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", selectedCategoryItemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;->selectedCategoryItemId:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
