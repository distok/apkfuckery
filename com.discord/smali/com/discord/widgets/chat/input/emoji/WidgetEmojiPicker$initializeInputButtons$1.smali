.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$1;
.super Ljava/lang/Object;
.source "WidgetEmojiPicker.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->initializeInputButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->access$setNextCategoryScrollSmooth$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->access$getViewModel$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->onClickUnicodeEmojiCategories()V

    return-void
.end method
