.class public final Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;
.super Ljava/lang/Object;
.source "EmojiCategoryViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCategoryIconResId(Lcom/discord/models/domain/emoji/EmojiCategory;)I
    .locals 1
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    const-string v0, "emojiCategory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const v0, 0x7f0802e9

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0802ef

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0802ea

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0802f0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0802ed

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0802f1

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0802e8

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0802eb

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0802ec

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0802ee

    :goto_0
    :pswitch_9
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
