.class public abstract Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;
.super Ljava/lang/Object;
.source "EmojiCategoryItem.kt"

# interfaces
.implements Lcom/discord/utilities/recycler/DiffKeyProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;,
        Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;,
        Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

.field public static final TYPE_GUILD:I = 0x1

.field public static final TYPE_STANDARD:I


# instance fields
.field private final categoryRange:Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final isSelected:Z

.field private final stableId:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    return-void
.end method

.method private constructor <init>(JLkotlin/Pair;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->stableId:J

    iput-object p3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->categoryRange:Lkotlin/Pair;

    iput-boolean p4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->isSelected:Z

    return-void
.end method

.method public synthetic constructor <init>(JLkotlin/Pair;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;-><init>(JLkotlin/Pair;Z)V

    return-void
.end method


# virtual methods
.method public getCategoryRange()Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->categoryRange:Lkotlin/Pair;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->stableId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getStableId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->stableId:J

    return-wide v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->isSelected:Z

    return v0
.end method

.method public final isUnicodeEmojiCategory()Z
    .locals 2

    instance-of v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;->getEmojiCategory()Lcom/discord/models/domain/emoji/EmojiCategory;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/emoji/EmojiCategory;->RECENT:Lcom/discord/models/domain/emoji/EmojiCategory;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
