.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerSheetViewModel;
.super Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;
.source "EmojiPickerSheetViewModel.kt"


# direct methods
.method public constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCategoryItemIdSubject"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;)V

    return-void
.end method
