.class public abstract Lcom/discord/widgets/chat/input/emoji/EmojiPicker;
.super Lcom/discord/app/AppFragment;
.source "EmojiPicker.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract setListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V
.end method

.method public abstract setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V
.end method

.method public setOnEmojiSearchOpenedListener(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
