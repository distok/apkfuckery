.class public final Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;
.super Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;
.source "EmojiCategoryViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Standard"
.end annotation


# instance fields
.field private final iconView:Landroid/widget/ImageView;

.field private final selectionOverline:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    const v0, 0x7f0a03a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    const v0, 0x7f0a03ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->selectionOverline:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;Lkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "standardCategoryItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCategoryClicked"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;->getEmojiCategory()Lcom/discord/models/domain/emoji/EmojiCategory;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;->getCategoryIconResId(Lcom/discord/models/domain/emoji/EmojiCategory;)I

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    iget-object v2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v3, "itemView"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;->isSelected()Z

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    const-string v2, "iconView"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->selectionOverline:Landroid/view/View;

    const-string v3, "selectionOverline"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f040155

    invoke-static {v1, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f040158

    invoke-static {v1, v3}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    :goto_1
    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v1}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;->iconView:Landroid/widget/ImageView;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    const/high16 v0, 0x3f000000    # 0.5f

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard$configure$1;

    invoke-direct {v1, p2, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard$configure$1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
