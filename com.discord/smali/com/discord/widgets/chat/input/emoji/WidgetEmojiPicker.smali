.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;
.super Lcom/discord/widgets/chat/input/emoji/EmojiPicker;
.source "WidgetEmojiPicker.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$Companion;

.field private static final EMOJI_PICKER_VIEW_FLIPPER_EMPTY_STATE:I = 0x1

.field private static final EMOJI_PICKER_VIEW_FLIPPER_RESULTS:I


# instance fields
.field private backspaceButton:Landroid/widget/ImageView;

.field private categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

.field private categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private container:Landroid/view/View;

.field private emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

.field private emojiAppBar:Lcom/google/android/material/appbar/AppBarLayout;

.field private emojiBottomBar:Landroid/view/View;

.field private emojiBottomBarDivider:Landroid/view/View;

.field private final emojiCategoryScrollSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

.field private emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

.field private emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private emojiSearchBar:Landroid/view/View;

.field private emojiToolbar:Landroidx/appcompat/widget/Toolbar;

.field private emojiViewFlipper:Lcom/discord/app/AppViewFlipper;

.field private flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

.field private inlineSearchButton:Landroid/view/View;

.field private isNextCategoryScrollSmooth:Z

.field private metadata:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation
.end field

.field private onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

.field private onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private previousViewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

.field private restoredSearchQueryFromViewModel:Z

.field private searchClearButton:Landroid/widget/ImageView;

.field private searchInput:Lcom/google/android/material/textfield/TextInputEditText;

.field private unicodeEmojiCategoriesShortcut:Landroid/widget/ImageView;

.field private viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/EmojiPicker;-><init>()V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiCategoryScrollSubject:Lrx/subjects/PublishSubject;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->isNextCategoryScrollSmooth:Z

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->configureUI(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$configureUnicodeCategoriesShortcutButton(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->configureUnicodeCategoriesShortcutButton(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getAdditionalBottomPaddingPx(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)I
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getAdditionalBottomPaddingPx()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getBackspaceButton$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->backspaceButton:Landroid/widget/ImageView;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "backspaceButton"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getEmojiCategoryScrollSubject$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiCategoryScrollSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$getEmojiPickerMode$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    return-object p0
.end method

.method public static final synthetic access$getOnBackspacePressedListener$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lcom/discord/widgets/chat/input/OnBackspacePressedListener;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    return-object p0
.end method

.method public static final synthetic access$getOnEmojiSearchOpenedListener$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lkotlin/jvm/functions/Function0;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$getPreviousViewState$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->previousViewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

    return-object p0
.end method

.method public static final synthetic access$getSearchInput$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lcom/google/android/material/textfield/TextInputEditText;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "searchInput"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->handleEvent(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$handleInputChanged(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->handleInputChanged(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleNewEmojiRecyclerScrollPosition(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;ILjava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->handleNewEmojiRecyclerScrollPosition(ILjava/util/List;)V

    return-void
.end method

.method public static final synthetic access$isNextCategoryScrollSmooth$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->isNextCategoryScrollSmooth:Z

    return p0
.end method

.method public static final synthetic access$launchBottomSheet(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->launchBottomSheet()V

    return-void
.end method

.method public static final synthetic access$onCategoryClicked(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onCategoryClicked(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;)V

    return-void
.end method

.method public static final synthetic access$onSelectedCategoryAdapterPositionUpdated(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onSelectedCategoryAdapterPositionUpdated(I)V

    return-void
.end method

.method public static final synthetic access$setBackspaceButton$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->backspaceButton:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setEmojiPickerMode$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    return-void
.end method

.method public static final synthetic access$setNextCategoryScrollSmooth$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->isNextCategoryScrollSmooth:Z

    return-void
.end method

.method public static final synthetic access$setOnBackspacePressedListener$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    return-void
.end method

.method public static final synthetic access$setOnEmojiSearchOpenedListener$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$setPreviousViewState$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->previousViewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

    return-void
.end method

.method public static final synthetic access$setSearchInput$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/google/android/material/textfield/TextInputEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->restoredSearchQueryFromViewModel:Z

    const/4 v3, 0x1

    if-nez v2, :cond_2

    if-eqz v1, :cond_2

    iput-boolean v3, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->restoredSearchQueryFromViewModel:Z

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    const-string p1, "searchInput"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_1
    if-nez p1, :cond_3

    return-void

    :cond_3
    instance-of v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;

    const-string v2, "emojiBottomBarDivider"

    const-string v4, "emojiBottomBar"

    const-string v5, "emojiViewFlipper"

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiViewFlipper:Lcom/discord/app/AppViewFlipper;

    if-eqz v1, :cond_6

    invoke-virtual {v1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiBottomBar:Landroid/view/View;

    if-eqz v1, :cond_5

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiBottomBarDivider:Landroid/view/View;

    if-eqz v1, :cond_4

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_7
    instance-of v1, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiBottomBar:Landroid/view/View;

    if-eqz v1, :cond_d

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiBottomBarDivider:Landroid/view/View;

    if-eqz v1, :cond_c

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiViewFlipper:Lcom/discord/app/AppViewFlipper;

    if-eqz v1, :cond_b

    invoke-virtual {v1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    const-string v2, "emojiAdapter"

    if-eqz v1, :cond_a

    move-object v3, p1

    check-cast v3, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;->getEmojiItems()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setData(Ljava/util/List;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    if-eqz v1, :cond_9

    new-instance v2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V

    invoke-virtual {v1, v2}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setOnScrollPositionListener(Lkotlin/jvm/functions/Function1;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    if-eqz v1, :cond_8

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;->getCategoryItems()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->setItems(Ljava/util/List;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->configureUnicodeCategoriesShortcutButton(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V

    goto :goto_2

    :cond_8
    const-string p1, "categoryAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_9
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_a
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_b
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_c
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_d
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_e
    :goto_2
    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->previousViewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

    return-void
.end method

.method private final configureUnicodeCategoriesShortcutButton(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
    .locals 6

    instance-of v0, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;->getFirstUnicodeEmojiCategoryItem()Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;

    move-result-object p1

    const-string/jumbo v0, "unicodeEmojiCategoriesShortcut"

    if-eqz p1, :cond_2

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Standard;->getEmojiCategory()Lcom/discord/models/domain/emoji/EmojiCategory;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;->getCategoryIconResId(Lcom/discord/models/domain/emoji/EmojiCategory;)I

    move-result v2

    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->unicodeEmojiCategoriesShortcut:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    const-string v3, "categoryLayoutManager"

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v2

    iget-object v4, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v4, :cond_e

    invoke-virtual {v4}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v3

    const/4 v4, -0x1

    if-eq v2, v4, :cond_d

    if-ne v3, v4, :cond_3

    goto :goto_5

    :cond_3
    iget-object v4, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    const-string v5, "categoryAdapter"

    if-eqz v4, :cond_c

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->getItemCount()I

    move-result v4

    if-lt v3, v4, :cond_4

    return-void

    :cond_4
    iget-object v4, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    if-eqz v4, :cond_b

    invoke-virtual {v4, v2}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->getItemAtPosition(I)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    move-result-object v2

    iget-object v4, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    if-eqz v4, :cond_a

    invoke-virtual {v4, v3}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->getItemAtPosition(I)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->isUnicodeEmojiCategory()Z

    move-result v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v2, :cond_6

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->isUnicodeEmojiCategory()Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    :goto_1
    const/4 v2, 0x1

    :goto_2
    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->unicodeEmojiCategoriesShortcut:Landroid/widget/ImageView;

    if-eqz v3, :cond_9

    if-nez v2, :cond_7

    if-eqz p1, :cond_7

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_8

    goto :goto_4

    :cond_8
    const/16 v5, 0x8

    :goto_4
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_9
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_a
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_b
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_c
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_d
    :goto_5
    return-void

    :cond_e
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_f
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_10
    return-void
.end method

.method private final getAdditionalBottomPaddingPx()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getEmojiContextType()Lcom/discord/widgets/chat/input/emoji/EmojiContextType;
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "EMOJI_CONTEXT_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_EMOJI_CONTEXT_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.widgets.chat.input.emoji.EmojiContextType"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    :goto_1
    return-object v0
.end method

.method private final getMode()Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "MODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    check-cast v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    :goto_2
    return-object v1
.end method

.method private final handleEvent(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;->getPosition()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->scrollToPosition(I)V

    goto :goto_0

    :cond_0
    const-string p1, "emojiAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleInputChanged(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->setSearchText(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchClearButton:Landroid/widget/ImageView;

    const-string v2, "searchClearButton"

    if-eqz v0, :cond_5

    if-eqz p1, :cond_0

    const v3, 0x7f080412

    goto :goto_0

    :cond_0
    const v3, 0x7f0802b8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchClearButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040178

    invoke-static {v3, v4}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040179

    invoke-static {v3, v4}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchClearButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    const p1, 0x7f121603

    goto :goto_2

    :cond_2
    const p1, 0x7f121539

    :goto_2
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_6
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final handleNewEmojiRecyclerScrollPosition(ILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->getCategoryRange()Lkotlin/Pair;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {v1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-le v2, p1, :cond_1

    goto :goto_0

    :cond_1
    if-le v1, p1, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->getStableId()J

    move-result-wide p1

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->selectCategoryByItemId(J)V

    :cond_2
    return-void
.end method

.method private final initializeInputButtons()V
    .locals 13

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    sget-object v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->backspaceButton:Landroid/widget/ImageView;

    const-string v3, "backspaceButton"

    const/4 v4, 0x0

    if-eqz v1, :cond_4

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->unicodeEmojiCategoriesShortcut:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->backspaceButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    new-instance v1, Lcom/discord/utilities/press/RepeatingOnTouchListener;

    const-wide/16 v6, 0xfa

    const-wide/16 v8, 0x32

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v11, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$2;

    invoke-direct {v11, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$2;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    new-instance v12, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$3;

    invoke-direct {v12, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeInputButtons$3;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    move-object v5, v1

    invoke-direct/range {v5 .. v12}, Lcom/discord/utilities/press/RepeatingOnTouchListener;-><init>(JJLjava/util/concurrent/TimeUnit;Lrx/functions/Action0;Lrx/functions/Action0;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_2
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_3
    const-string/jumbo v0, "unicodeEmojiCategoriesShortcut"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_4
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4
.end method

.method private final initializeSearchBar()V
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAppBar:Lcom/google/android/material/appbar/AppBarLayout;

    const/4 v1, 0x0

    if-eqz v0, :cond_e

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    sget-object v3, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/16 v6, 0x8

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->inlineSearchButton:Landroid/view/View;

    const-string v2, "inlineSearchButton"

    if-eqz v0, :cond_d

    iget-object v7, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-ne v7, v3, :cond_2

    const/4 v7, 0x1

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    :goto_2
    if-eqz v7, :cond_3

    const/4 v7, 0x0

    goto :goto_3

    :cond_3
    const/16 v7, 0x8

    :goto_3
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    const-string v7, "searchInput"

    if-eqz v0, :cond_c

    iget-object v8, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-eq v8, v3, :cond_4

    goto :goto_4

    :cond_4
    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_5

    goto :goto_5

    :cond_5
    const/16 v5, 0x8

    :goto_5
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->inlineSearchButton:Landroid/view/View;

    if-eqz v0, :cond_b

    new-instance v2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeSearchBar$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeSearchBar$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchClearButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_a

    new-instance v2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeSearchBar$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeSearchBar$2;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/discord/utilities/view/text/TextWatcher;->Companion:Lcom/discord/utilities/view/text/TextWatcher$Companion;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    if-eqz v2, :cond_9

    new-instance v3, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeSearchBar$3;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$initializeSearchBar$3;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    invoke-virtual {v0, p0, v2, v3}, Lcom/discord/utilities/view/text/TextWatcher$Companion;->addBindedTextWatcher(Landroidx/fragment/app/Fragment;Landroid/widget/TextView;Lrx/functions/Action1;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->BOTTOM_SHEET:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    if-eqz v0, :cond_6

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->showKeyboard(Landroid/view/View;)V

    goto :goto_6

    :cond_6
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_7
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_8
    :goto_6
    return-void

    :cond_9
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_a
    const-string v0, "searchClearButton"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_b
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_c
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_d
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_e
    const-string v0, "emojiAppBar"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final launchBottomSheet()V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getEmojiContextType()Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->launchBottomSheet$default(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method private final onCategoryClicked(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;)V
    .locals 3

    instance-of v0, p1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    move-object v1, p1

    check-cast v1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->emojiCategorySelected(J)V

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->getStableId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->selectCategoryByItemId(J)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->getCategoryRange()Lkotlin/Pair;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->scrollToPosition(I)V

    return-void

    :cond_1
    const-string p1, "emojiAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final onSelectedCategoryAdapterPositionUpdated(I)V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    const-string v1, "categoryLayoutManager"

    const/4 v2, 0x0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v1

    sub-int v3, v1, v0

    new-instance v4, Lkotlin/ranges/IntRange;

    invoke-direct {v4, v0, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-virtual {v4, p1}, Lkotlin/ranges/IntRange;->contains(I)Z

    move-result v1

    if-nez v1, :cond_5

    if-ge p1, v0, :cond_0

    sub-int/2addr p1, v3

    goto :goto_0

    :cond_0
    add-int/2addr p1, v3

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;->getItemCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->isNextCategoryScrollSmooth:Z

    const-string v3, "categoryRecycler"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    goto :goto_1

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    iput-boolean v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->isNextCategoryScrollSmooth:Z

    goto :goto_1

    :cond_3
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    const-string p1, "categoryAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    :goto_1
    return-void

    :cond_6
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_7
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final selectCategoryByItemId(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->setSelectedCategoryItemId(J)V

    return-void

    :cond_0
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final setUpCategoryRecycler()V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "categoryRecycler"

    const/4 v2, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    new-instance v4, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setUpCategoryRecycler$1;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setUpCategoryRecycler$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    new-instance v5, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setUpCategoryRecycler$2;

    invoke-direct {v5, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setUpCategoryRecycler$2;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    const/4 v6, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, v0

    move-object v7, p0

    invoke-direct/range {v3 .. v9}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    const-string v3, "categoryAdapter"

    if-eqz v0, :cond_7

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    if-eqz v4, :cond_5

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance v0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;

    iget-object v6, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v6, :cond_4

    iget-object v7, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;

    if-eqz v7, :cond_3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v11}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$Adapter;IZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {v3, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setUpCategoryRecycler$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setUpCategoryRecycler$3;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    const-string v0, "categoryLayoutManager"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_6
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_7
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_8
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final setUpEmojiRecycler()V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "emojiRecycler"

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v3, :cond_4

    invoke-direct {v0, v3, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    new-instance v3, Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    if-eqz v0, :cond_3

    invoke-direct {v3, v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;-><init>(Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    invoke-virtual {v3, v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->blockClicks(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    const-string v0, "emojiAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final setWindowInsetsListeners()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->container:Landroid/view/View;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setWindowInsetsListeners$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$setWindowInsetsListeners$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void

    :cond_0
    const-string v0, "container"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final subscribeToCategoryRecyclerScrolls()V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiCategoryScrollSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0xfa

    invoke-direct {v1, v3, v4, v2}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;)V

    new-instance v2, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v0, v1}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    const-string v1, "emojiCategoryScrollSubje\u2026, TimeUnit.MILLISECONDS))"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$subscribeToCategoryRecyclerScrolls$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$subscribeToCategoryRecyclerScrolls$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final clearSearchInput()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->setSearchText(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01e7

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 17

    move-object/from16 v0, p0

    invoke-super/range {p0 .. p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "RESULT_METADATA_PAYLOAD"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iput-object v1, v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->metadata:Ljava/util/HashMap;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getMode()Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;

    invoke-direct {v3}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v2, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-virtual {v1, v2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v1

    const-string v2, "ViewModelProvider(\n     \u2026putViewModel::class.java)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    iput-object v1, v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getMode()Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-ne v1, v2, :cond_0

    const-class v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerInlineViewModel;

    goto :goto_0

    :cond_0
    const-class v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerSheetViewModel;

    :goto_0
    new-instance v2, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    new-instance v15, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getMode()Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getEmojiContextType()Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    move-result-object v6

    new-instance v4, Lcom/discord/utilities/locale/LocaleManager;

    invoke-direct {v4}, Lcom/discord/utilities/locale/LocaleManager;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/discord/utilities/locale/LocaleManager;->getPrimaryLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1f8

    const/16 v16, 0x0

    move-object v4, v15

    move-object v0, v15

    move-object/from16 v15, v16

    invoke-direct/range {v4 .. v15}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Ljava/util/Locale;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreChannelsSelected;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v2, v3, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    invoke-virtual {v2, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    return-void
.end method

.method public onEmojiSelected(Lcom/discord/models/domain/emoji/Emoji;)V
    .locals 14

    const-string v0, "emoji"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->isUsable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    instance-of v0, p1, Lcom/discord/models/domain/emoji/ModelEmojiCustom;

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/models/domain/emoji/ModelEmojiCustom;

    invoke-virtual {p1}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->isAnimated()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    sget-object v3, Lf/a/a/e/c;->n:Lf/a/a/e/c$b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v4

    const-string p1, "parentFragmentManager"

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_1

    const/4 v2, 0x2

    const/4 v5, 0x2

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    :goto_0
    if-eqz v1, :cond_2

    const p1, 0x7f12144b

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const p1, 0x7f121453

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    move-object v6, p1

    if-eqz v1, :cond_3

    const p1, 0x7f12144c

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    const p1, 0x7f121454

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    move-object v7, p1

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x100

    const-string v9, "Emoji Picker Popout"

    invoke-static/range {v3 .. v13}, Lf/a/a/e/c$b;->a(Lf/a/a/e/c$b;Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;I)V

    return-void

    :cond_4
    invoke-interface {p1}, Lcom/discord/models/domain/emoji/Emoji;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f120693

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {p1, v0, v1, v2, v3}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    return-void

    :cond_5
    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    if-eqz v0, :cond_6

    invoke-interface {v0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;->onEmojiPicked(Lcom/discord/models/domain/emoji/Emoji;)V

    :cond_6
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a03ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.emoji_picker_container)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->container:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getAdditionalBottomPaddingPx()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/view/View;->setPadding(IIII)V

    const v0, 0x7f0a03a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_app_bar)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/appbar/AppBarLayout;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAppBar:Lcom/google/android/material/appbar/AppBarLayout;

    const v0, 0x7f0a03a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_picker_bottom_bar)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiBottomBar:Landroid/view/View;

    const v0, 0x7f0a03a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.e\u2026icker_bottom_bar_divider)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiBottomBarDivider:Landroid/view/View;

    const v0, 0x7f0a03b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_toolbar)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiToolbar:Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f0a0220

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.c\u2026moji_picker_view_flipper)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiViewFlipper:Lcom/discord/app/AppViewFlipper;

    const v0, 0x7f0a021f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView"

    invoke-static {v0, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a03ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.e\u2026de_emoji_shortcut_button)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->unicodeEmojiCategoriesShortcut:Landroid/widget/ImageView;

    const v0, 0x7f0a03a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.e\u2026ji_picker_backspace_icon)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->backspaceButton:Landroid/widget/ImageView;

    const v0, 0x7f0a03ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_search_bar)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiSearchBar:Landroid/view/View;

    const v0, 0x7f0a03af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_search_input)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/textfield/TextInputEditText;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchInput:Lcom/google/android/material/textfield/TextInputEditText;

    const v0, 0x7f0a03a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_inline_search_button)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->inlineSearchButton:Landroid/view/View;

    const v0, 0x7f0a03ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v2, "view.findViewById(R.id.emoji_search_clear)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->searchClearButton:Landroid/widget/ImageView;

    const v0, 0x7f0a03aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.e\u2026picker_category_recycler)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->categoryRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->getMode()Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->setWindowInsetsListeners()V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->initializeInputButtons()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->initializeSearchBar()V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiToolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    const-string v1, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout.LayoutParams"

    invoke-static {p1, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/material/appbar/AppBarLayout$LayoutParams;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-ne v1, v0, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/material/appbar/AppBarLayout$LayoutParams;->setScrollFlags(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->setUpEmojiRecycler()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->setUpCategoryRecycler()V

    return-void

    :cond_2
    const-string p1, "emojiToolbar"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string p1, "container"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->viewModel:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->subscribeToCategoryRecyclerScrolls()V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final scrollToTop()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiAdapter:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItemCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_0

    :cond_0
    const-string v0, "emojiRecycler"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "emojiAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public setListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    return-void
.end method

.method public setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    return-void
.end method

.method public setOnEmojiSearchOpenedListener(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method
