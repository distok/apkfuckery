.class public final Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;
.super Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;
.source "EmojiCategoryViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Guild"
.end annotation


# instance fields
.field private final guildAvatar:Lcom/discord/widgets/chat/input/emoji/GuildAvatar;

.field private final selectionOverline:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    const v0, 0x7f0a03a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/emoji/GuildAvatar;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;->guildAvatar:Lcom/discord/widgets/chat/input/emoji/GuildAvatar;

    const v0, 0x7f0a03ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;->selectionOverline:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "guildCategoryItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCategoryClicked"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;->guildAvatar:Lcom/discord/widgets/chat/input/emoji/GuildAvatar;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/emoji/GuildAvatar;->updateView(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;->selectionOverline:Landroid/view/View;

    const-string v1, "selectionOverline"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild$configure$1;

    invoke-direct {v1, p2, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild$configure$1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Guild;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
