.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;
.super Lx/m/c/k;
.source "WidgetEmojiPicker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->configureUI(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;->$viewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;->this$0:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$configureUI$1;->$viewState:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;

    check-cast v1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;->getCategoryItems()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->access$handleNewEmojiRecyclerScrollPosition(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;ILjava/util/List;)V

    return-void
.end method
