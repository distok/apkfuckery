.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;
.super Ljava/lang/Object;
.source "EmojiPickerViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final emojiContextType:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

.field private final emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

.field private final locale:Ljava/util/Locale;

.field private final searchSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeEmoji:Lcom/discord/stores/StoreEmoji;

.field private final storeGuildsSorted:Lcom/discord/stores/StoreGuildsSorted;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Ljava/util/Locale;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreChannelsSelected;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;",
            "Lcom/discord/widgets/chat/input/emoji/EmojiContextType;",
            "Ljava/util/Locale;",
            "Lcom/discord/stores/StoreEmoji;",
            "Lcom/discord/stores/StoreGuildsSorted;",
            "Lcom/discord/stores/StoreUserSettings;",
            "Lcom/discord/stores/StoreChannelsSelected;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emojiPickerMode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeEmoji"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuildsSorted"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCategoryItemIdSubject"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->emojiContextType:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->locale:Ljava/util/Locale;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeEmoji:Lcom/discord/stores/StoreEmoji;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeGuildsSorted:Lcom/discord/stores/StoreGuildsSorted;

    iput-object p6, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object p7, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p8, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iput-object p9, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Ljava/util/Locale;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreChannelsSelected;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p10

    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object v1

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object/from16 v6, p4

    :goto_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGuildsSorted()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v1

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object/from16 v7, p5

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    move-object v8, v1

    goto :goto_2

    :cond_2
    move-object/from16 v8, p6

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p7

    :goto_3
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_4

    const-string v1, ""

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    const-string v2, "BehaviorSubject.create(\"\")"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v10, v1

    goto :goto_4

    :cond_4
    move-object/from16 v10, p8

    :goto_4
    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_5

    sget-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;->Companion:Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;

    sget-object v1, Lcom/discord/models/domain/emoji/EmojiCategory;->RECENT:Lcom/discord/models/domain/emoji/EmojiCategory;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;->mapEmojiCategoryToItemId(Lcom/discord/models/domain/emoji/EmojiCategory;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create(\n\u2026ry.RECENT\n        )\n    )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v11, v0

    goto :goto_5

    :cond_5
    move-object/from16 v11, p9

    :goto_5
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v2 .. v11}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Ljava/util/Locale;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreChannelsSelected;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;)V

    return-void
.end method

.method public static final synthetic access$getSearchSubject$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getSelectedCategoryItemIdSubject$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getStoreEmoji$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lcom/discord/stores/StoreEmoji;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeEmoji:Lcom/discord/stores/StoreEmoji;

    return-object p0
.end method

.method public static final synthetic access$getStoreGuildsSorted$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lcom/discord/stores/StoreGuildsSorted;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeGuildsSorted:Lcom/discord/stores/StoreGuildsSorted;

    return-object p0
.end method

.method public static final synthetic access$getStoreUserSettings$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lcom/discord/stores/StoreUserSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    return-object p0
.end method

.method private final getEmojiContextObservable(Lcom/discord/widgets/chat/input/emoji/EmojiContextType;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/emoji/EmojiContextType;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreEmoji$EmojiContext;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;->INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$getEmojiContextObservable$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "storeChannelsSelected.ob\u2026      }\n                }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/discord/stores/StoreEmoji$EmojiContext$Global;->INSTANCE:Lcom/discord/stores/StoreEmoji$EmojiContext$Global;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(\n       \u2026ontext.Global\n          )"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p1, v0

    :goto_1
    return-object p1
.end method

.method private final observeStoreState(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreEmoji$EmojiContext;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "emojiContextObservable.s\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->emojiPickerMode:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    sget-object v0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    if-ne p1, v0, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerInlineViewModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->emojiContextType:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->getEmojiContextObservable(Lcom/discord/widgets/chat/input/emoji/EmojiContextType;)Lrx/Observable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->observeStoreState(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->locale:Ljava/util/Locale;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerInlineViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerSheetViewModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->emojiContextType:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->getEmojiContextObservable(Lcom/discord/widgets/chat/input/emoji/EmojiContextType;)Lrx/Observable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->observeStoreState(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->selectedCategoryItemIdSubject:Lrx/subjects/BehaviorSubject;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->locale:Ljava/util/Locale;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerSheetViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;)V

    :goto_0
    return-object p1
.end method
