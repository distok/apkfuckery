.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "EmojiPickerViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->observeStoreState(Lrx/Observable;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreEmoji$EmojiContext;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreEmoji$EmojiContext;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->call(Lcom/discord/stores/StoreEmoji$EmojiContext;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/stores/StoreEmoji$EmojiContext;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreEmoji$EmojiContext;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Uninitialized;->INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Uninitialized;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->access$getStoreEmoji$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lcom/discord/stores/StoreEmoji;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, v1}, Lcom/discord/stores/StoreEmoji;->getEmojiSet(Lcom/discord/stores/StoreEmoji$EmojiContext;ZZ)Lrx/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->access$getStoreGuildsSorted$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildsSorted;->getFlat()Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->access$getSearchSubject$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lrx/subjects/BehaviorSubject;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->access$getStoreUserSettings$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getAllowAnimatedEmojisObservable()Lrx/Observable;

    move-result-object v5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;->access$getSelectedCategoryItemIdSubject$p(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory;)Lrx/subjects/BehaviorSubject;

    move-result-object v6

    new-instance v7, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1$1;

    invoke-direct {v7, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1$1;-><init>(Lcom/discord/stores/StoreEmoji$EmojiContext;)V

    invoke-static/range {v2 .. v7}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
