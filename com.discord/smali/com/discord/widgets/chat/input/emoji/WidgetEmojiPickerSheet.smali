.class public final Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;
.super Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;
.source "WidgetEmojiPickerSheet.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;
    }
.end annotation


# static fields
.field private static final ARG_EMOJI_CONTEXT_TYPE:Ljava/lang/String; = "ARG_EMOJI_CONTEXT_TYPE"

.field public static final Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;


# instance fields
.field private container:Landroid/view/View;

.field private emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

.field private emojiPickerListenerDelegate:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->Companion:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;-><init>()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->container:Landroid/view/View;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "container"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setContainer$p(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->container:Landroid/view/View;

    return-void
.end method

.method private final cancelDialog()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->clearSearchInput()V

    return-void

    :cond_0
    const-string v0, "emojiPickerFragment"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01e8

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;->onCancel(Landroid/content/DialogInterface;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->cancelDialog()V

    return-void
.end method

.method public onEmojiPicked(Lcom/discord/models/domain/emoji/Emoji;)V
    .locals 1

    const-string v0, "emoji"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->emojiPickerListenerDelegate:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;->onEmojiPicked(Lcom/discord/models/domain/emoji/Emoji;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->cancelDialog()V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->container:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    const-string v0, "ARG_EMOJI_CONTEXT_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    instance-of v0, p1, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    if-nez v0, :cond_1

    move-object p1, p2

    :cond_1
    check-cast p1, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    const-string v1, "emojiPickerFragment"

    if-eqz v0, :cond_5

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->BOTTOM_SHEET:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    const-string v4, "MODE"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v3, "EMOJI_CONTEXT_TYPE"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    if-eqz p1, :cond_4

    invoke-virtual {p1, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->setListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    const v0, 0x7f0a03b0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    if-eqz v2, :cond_3

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, v2, p2}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$onViewCreated$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;)V

    invoke-virtual {p1, p2}, Landroidx/fragment/app/FragmentTransaction;->runOnCommit(Ljava/lang/Runnable;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_4
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_5
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2
.end method

.method public final setEmojiPickerListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPickerSheet;->emojiPickerListenerDelegate:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    return-void
.end method
