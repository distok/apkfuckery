.class public final Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1$1;
.super Ljava/lang/Object;
.source "EmojiPickerViewModel.kt"

# interfaces
.implements Lrx/functions/Func5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1;->call(Lcom/discord/stores/StoreEmoji$EmojiContext;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func5<",
        "Lcom/discord/models/domain/emoji/EmojiSet;",
        "Ljava/util/LinkedHashMap<",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelGuild;",
        ">;",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreEmoji$EmojiContext;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1$1;->$emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/emoji/EmojiSet;Ljava/util/LinkedHashMap;Ljava/lang/String;ZLjava/lang/Long;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/emoji/EmojiSet;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Long;",
            ")",
            "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;"
        }
    .end annotation

    const-string v0, "allGuilds"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchInputString"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Uninitialized;->INSTANCE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Uninitialized;

    goto :goto_0

    :cond_0
    new-instance v8, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1$1;->$emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    const-string v0, "selectedCategoryItemId"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v0, v8

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;-><init>(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJ)V

    move-object p1, v8

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    move-object v1, p1

    check-cast v1, Lcom/discord/models/domain/emoji/EmojiSet;

    move-object v2, p2

    check-cast v2, Ljava/util/LinkedHashMap;

    move-object v3, p3

    check-cast v3, Ljava/lang/String;

    check-cast p4, Ljava/lang/Boolean;

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move-object v5, p5

    check-cast v5, Ljava/lang/Long;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Factory$observeStoreState$1$1;->call(Lcom/discord/models/domain/emoji/EmojiSet;Ljava/util/LinkedHashMap;Ljava/lang/String;ZLjava/lang/Long;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
