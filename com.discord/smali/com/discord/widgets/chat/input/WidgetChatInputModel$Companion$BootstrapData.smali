.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BootstrapData"
.end annotation


# instance fields
.field private final me:Lcom/discord/models/domain/ModelUser$Me;

.field private final selectedChannel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->copy(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelUser$Me;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMe()Lcom/discord/models/domain/ModelUser$Me;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    return-object v0
.end method

.method public final getSelectedChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser$Me;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "BootstrapData(selectedChannel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", me="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->me:Lcom/discord/models/domain/ModelUser$Me;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
