.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;
.super Ljava/lang/Object;
.source "WidgetChatInputAutocomplete.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$removeSpans(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;Landroid/text/Spannable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;->removeSpans(Landroid/text/Spannable;)V

    return-void
.end method

.method private final removeSpans(Landroid/text/Spannable;)V
    .locals 5

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    instance-of v4, v3, Landroid/text/style/CharacterStyle;

    if-eqz v4, :cond_0

    invoke-interface {p1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
