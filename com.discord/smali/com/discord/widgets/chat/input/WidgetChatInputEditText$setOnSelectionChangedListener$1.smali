.class public final Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnSelectionChangedListener$1;
.super Lx/m/c/k;
.source "WidgetChatInputEditText.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setOnSelectionChangedListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnSelectionChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnSelectionChangedListener$1;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnSelectionChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->access$getChatInputCommands$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onSelectionChanged(II)V

    return-void
.end method
