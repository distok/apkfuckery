.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;
.super Ljava/lang/Object;
.source "WidgetChatInputAutocomplete.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/chat/input/ChatInputCommandsModel;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;->call(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getToken()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
