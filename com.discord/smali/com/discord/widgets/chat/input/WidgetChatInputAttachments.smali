.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;
.super Ljava/lang/Object;
.source "WidgetChatInputAttachments.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$PermissionsEmptyListAdapter;,
        Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$DiscordFilesFragment;,
        Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$DiscordPhotosFragment;
    }
.end annotation


# instance fields
.field private final flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V
    .locals 1

    const-string v0, "flexInputFragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    return-void
.end method

.method public static final synthetic access$createAndConfigureExpressionFragment(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Landroidx/fragment/app/FragmentManager;Landroid/widget/TextView;)Landroidx/fragment/app/Fragment;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->createAndConfigureExpressionFragment(Landroidx/fragment/app/FragmentManager;Landroid/widget/TextView;)Landroidx/fragment/app/Fragment;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createPreviewAdapter(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Landroid/content/Context;)Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->createPreviewAdapter(Landroid/content/Context;)Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFlexInputFragment$p(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;)Lcom/lytefast/flexinput/fragment/FlexInputFragment;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    return-object p0
.end method

.method public static final synthetic access$setAttachmentFromPicker(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Landroid/content/Context;Landroidx/core/view/inputmethod/InputContentInfoCompat;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->setAttachmentFromPicker(Landroid/content/Context;Landroidx/core/view/inputmethod/InputContentInfoCompat;)V

    return-void
.end method

.method private final createAndConfigureExpressionFragment(Landroidx/fragment/app/FragmentManager;Landroid/widget/TextView;)Landroidx/fragment/app/Fragment;
    .locals 3

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$emojiPickerListener$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$emojiPickerListener$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;)V

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$stickerPickerListener$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$stickerPickerListener$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;)V

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$onBackspacePressedListener$1;

    invoke-direct {v2, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$onBackspacePressedListener$1;-><init>(Landroid/widget/TextView;)V

    const p2, 0x7f0a0444

    invoke-virtual {p1, p2}, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;

    move-result-object p1

    instance-of p2, p1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-direct {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;-><init>()V

    :goto_0
    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setEmojiPickerListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V

    invoke-virtual {p1, v1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setStickerPickerListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V

    invoke-virtual {p1, v2}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V

    new-instance p2, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createAndConfigureExpressionFragment$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setOnEmojiSearchOpenedListener(Lkotlin/jvm/functions/Function0;)V

    return-object p1
.end method

.method private final createPreviewAdapter(Landroid/content/Context;)Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "+",
            "Ljava/lang/Object;",
            ">;>(",
            "Landroid/content/Context;",
            ")",
            "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createPreviewAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$createPreviewAdapter$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method private final setAttachmentFromPicker(Landroid/content/Context;Landroidx/core/view/inputmethod/InputContentInfoCompat;)V
    .locals 3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f120260

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "context.getString(R.stri\u2026achment_filename_unknown)"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/lytefast/flexinput/model/Attachment;->Companion:Lcom/lytefast/flexinput/model/Attachment$Companion;

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v0, v2, p1}, Lcom/lytefast/flexinput/model/Attachment$Companion;->b(Landroidx/core/view/inputmethod/InputContentInfoCompat;Landroid/content/ContentResolver;ZLjava/lang/String;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/SourcedAttachment;

    const-string v0, "keyboard"

    invoke-direct {p2, p1, v0}, Lcom/discord/widgets/chat/input/SourcedAttachment;-><init>(Lcom/lytefast/flexinput/model/Attachment;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-virtual {p1, p2}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->e(Lcom/lytefast/flexinput/model/Attachment;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final addExternalAttachment(Lcom/lytefast/flexinput/model/Attachment;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "attachment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$addExternalAttachment$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$addExternalAttachment$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/lytefast/flexinput/model/Attachment;)V

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final configureFlexInputFragment(Lcom/discord/app/AppFragment;)V
    .locals 2

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/discord/app/AppFragment;)V

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k(Lkotlin/jvm/functions/Function0;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;)V

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/discord/app/AppFragment;->setOnBackPressed(Lrx/functions/Func0;I)V

    return-void
.end method

.method public final setInputListener(Lcom/lytefast/flexinput/InputListener;)V
    .locals 2

    const-string v0, "inputListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->v:Lcom/lytefast/flexinput/InputListener;

    return-void
.end method

.method public final setViewModel(Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->flexInputFragment:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iput-object p1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    return-void
.end method
