.class public final Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "SelectedApplicationCommandAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;",
        ">;"
    }
.end annotation


# instance fields
.field private currentCommand:Lcom/discord/stores/ModelApplicationCommand;

.field private highlightedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

.field private final onClickApplicationCommandOption:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final paramPositions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private scroller:Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickApplicationCommandOption"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;Z)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->onClickApplicationCommandOption:Lkotlin/jvm/functions/Function1;

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter$scroller$1;

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter$scroller$1;-><init>(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->scroller:Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;

    return-void
.end method

.method public static final synthetic access$getScroller$p(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;)Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->scroller:Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;

    return-object p0
.end method

.method public static final synthetic access$setScroller$p(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->scroller:Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;

    return-void
.end method

.method private final configureVerified()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-virtual {p0, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->currentCommand:Lcom/discord/stores/ModelApplicationCommand;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->highlightedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    return-void
.end method

.method public final clearParamOptionHighlight()V
    .locals 3

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItemCount()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->highlightedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItem(I)Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->setHighlighted(Z)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    :cond_2
    return-void
.end method

.method public final getOnClickApplicationCommandOption()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->onClickApplicationCommandOption:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final highlightOption(Lcom/discord/stores/ModelApplicationCommandOption;)V
    .locals 3

    const-string v0, "commandOption"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->highlightedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->clearParamOptionHighlight()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->highlightedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItemCount()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->highlightedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {p0, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItem(I)Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->setHighlighted(Z)V

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter$highlightOption$$inlined$let$lambda$1;

    invoke-direct {v2, v0, p0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter$highlightOption$$inlined$let$lambda$1;-><init>(ILcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;Lcom/discord/stores/ModelApplicationCommandOption;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;",
            "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    const/4 p1, 0x1

    if-eq p2, p1, :cond_1

    const/4 p1, 0x2

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandSectionHeadingAdapterItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandSectionHeadingAdapterItem;-><init>(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;-><init>(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandTitleAdapterItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandTitleAdapterItem;-><init>(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;)V

    :goto_0
    return-object p1
.end method

.method public final onParamClicked(Lcom/discord/stores/ModelApplicationCommandOption;)V
    .locals 1

    const-string v0, "option"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->onClickApplicationCommandOption:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final setApplicationCommand(Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/models/slashcommands/ModelApplication;)V
    .locals 26

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "command"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "application"

    move-object/from16 v3, p2

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->currentCommand:Lcom/discord/stores/ModelApplicationCommand;

    invoke-static {v2, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iput-object v1, v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->currentCommand:Lcom/discord/stores/ModelApplicationCommand;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/ModelApplicationCommand;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/slashcommands/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object v11

    const/4 v15, 0x0

    if-eqz v11, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/ModelApplicationCommand;->getApplicationId()J

    move-result-wide v9

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    invoke-static/range {v9 .. v14}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon$default(JLjava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    goto :goto_0

    :cond_1
    move-object v9, v15

    :goto_0
    const/4 v10, 0x0

    const/16 v11, 0x4f

    const/4 v12, 0x0

    new-instance v13, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    move-object v3, v13

    invoke-direct/range {v3 .. v12}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {v6}, Lcom/discord/stores/ModelApplicationCommandOption;->getRequired()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v7, v5, 0x1

    if-ltz v5, :cond_4

    move-object/from16 v17, v6

    check-cast v17, Lcom/discord/stores/ModelApplicationCommandOption;

    iget-object v5, v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x7e

    const/16 v25, 0x0

    move-object/from16 v16, v5

    invoke-direct/range {v16 .. v25}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v7

    goto :goto_2

    :cond_4
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    throw v15

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v3

    instance-of v5, v3, Ljava/util/Collection;

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    goto :goto_4

    :cond_6
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getRequired()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_7

    add-int/lit8 v4, v4, 0x1

    if-ltz v4, :cond_8

    goto :goto_3

    :cond_8
    invoke-static {}, Lx/h/f;->throwCountOverflow()V

    throw v15

    :cond_9
    :goto_4
    if-eqz v4, :cond_a

    new-instance v3, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f1204d2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x3f

    const/4 v14, 0x0

    move-object v5, v3

    invoke-direct/range {v5 .. v14}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getRequired()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_b

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_c
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/discord/stores/ModelApplicationCommandOption;

    iget-object v3, v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x7e

    const/4 v13, 0x0

    move-object v4, v3

    invoke-direct/range {v4 .. v13}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_d
    invoke-virtual {v0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method

.method public final setVerified(Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V
    .locals 6

    const-string/jumbo v0, "verifiedInputs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItemCount()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->getVerifiedInputs()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/ModelApplicationCommandOption;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->paramPositions:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->getVerifiedInputs()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {p0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItem(I)Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v3, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v4, v3}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->setCompleted(Z)V

    invoke-virtual {p0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItem(I)Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->getShowOptionErrorSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->setError(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->configureVerified()V

    return-void
.end method
