.class public final Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;
.super Ljava/lang/Object;
.source "WidgetChatInputApplicationCommands.kt"


# instance fields
.field private final showOptionErrorSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            ">;"
        }
    .end annotation
.end field

.field private final verifiedInputs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;-><init>(Ljava/util/Map;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "verifiedInputs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showOptionErrorSet"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Map;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    sget-object p2, Lx/h/n;->d:Lx/h/n;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;-><init>(Ljava/util/Map;Ljava/util/Set;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;Ljava/util/Map;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->copy(Ljava/util/Map;Ljava/util/Set;)Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    return-object v0
.end method

.method public final component2()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    return-object v0
.end method

.method public final copy(Ljava/util/Map;Ljava/util/Set;)Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            ">;)",
            "Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;"
        }
    .end annotation

    const-string/jumbo v0, "verifiedInputs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showOptionErrorSet"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;-><init>(Ljava/util/Map;Ljava/util/Set;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getShowOptionErrorSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    return-object v0
.end method

.method public final getVerifiedInputs()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "CommandInputVerification(verifiedInputs="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->verifiedInputs:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showOptionErrorSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->showOptionErrorSet:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
