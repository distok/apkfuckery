.class public final Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;
.super Ljava/lang/Object;
.source "WidgetChatInputApplicationCommands.kt"


# instance fields
.field private final applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

.field private final validInputs:Z

.field private final values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/ModelApplicationCommand;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandValue;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "applicationCommand"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "values"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;-><init>(Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;ZILjava/lang/Object;)Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->copy(Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;Z)Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/ModelApplicationCommand;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    return v0
.end method

.method public final copy(Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;Z)Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/ModelApplicationCommand;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandValue;",
            ">;Z)",
            "Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;"
        }
    .end annotation

    const-string v0, "applicationCommand"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "values"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;-><init>(Lcom/discord/stores/ModelApplicationCommand;Ljava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    iget-boolean p1, p1, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplicationCommand()Lcom/discord/stores/ModelApplicationCommand;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-object v0
.end method

.method public final getValidInputs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    return v0
.end method

.method public final getValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommand;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ApplicationCommandData(applicationCommand="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->applicationCommand:Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", validInputs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/ApplicationCommandData;->validInputs:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
