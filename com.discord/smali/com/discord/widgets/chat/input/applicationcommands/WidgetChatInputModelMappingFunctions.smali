.class public final Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;
.super Ljava/lang/Object;
.source "WidgetChatInputModelMappingFunctions.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

.field private static final TAG_SYMBOLS_PATTERN:Ljava/util/regex/Pattern;

.field private static final TAG_SYMBOLS_PERF_LIMIT:I = 0x7d0


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    const-string v0, "^(.*\\s)?[@#:].*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "Pattern.compile(\"^(.*\\\\s)?[@#:].*\")"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->TAG_SYMBOLS_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getCommandOptionInputValues(Ljava/lang/String;Lcom/discord/stores/ModelApplicationCommand;)Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;
    .locals 8

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-static {p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommandsKt;->findOptionRanges(Ljava/lang/String;Lcom/discord/stores/ModelApplicationCommand;)Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v4

    iget v4, v4, Lkotlin/ranges/IntProgression;->d:I

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v3

    iget v3, v3, Lkotlin/ranges/IntProgression;->e:I

    const-string v5, "null cannot be cast to non-null type java.lang.String"

    invoke-static {p1, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/discord/models/slashcommands/CommandChoices;

    invoke-virtual {v7}, Lcom/discord/models/slashcommands/CommandChoices;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_2
    move-object v6, v5

    :goto_1
    check-cast v6, Lcom/discord/models/slashcommands/CommandChoices;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/discord/models/slashcommands/CommandChoices;->getValue()Ljava/lang/String;

    move-result-object v5

    :cond_3
    if-eqz v5, :cond_5

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/4 v6, 0x1

    if-lez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    :goto_2
    if-ne v4, v6, :cond_5

    move-object v3, v5

    :cond_5
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_6
    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    invoke-direct {p1, v0, p2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    return-object p1
.end method

.method private final getSelectedOption(ILjava/util/Map;)Lcom/discord/stores/ModelApplicationCommandOption;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;",
            ">;)",
            "Lcom/discord/stores/ModelApplicationCommandOption;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getParam()Lkotlin/ranges/IntRange;

    move-result-object v3

    iget v3, v3, Lkotlin/ranges/IntProgression;->d:I

    if-ge v3, p1, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v2

    iget v2, v2, Lkotlin/ranges/IntProgression;->e:I

    if-lt v2, p1, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public final addAutocompleteToken(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 7

    const-string v0, "modelChat"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->getMentionAutoCompleteToken(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object v6

    new-instance v0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInputMentionsMap()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v5

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;)V

    return-object v0
.end method

.method public final createCommandContextFromInputAndSelectedCommand(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/stores/ModelApplicationCommand;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 10

    const-string v0, "input"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {v0, v1, v1, v2, v1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;-><init>(Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->getCommandOptionInputValues(Ljava/lang/String;Lcom/discord/stores/ModelApplicationCommand;)Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/InputModel;->getSelected()Lkotlin/ranges/IntRange;

    move-result-object p1

    iget p1, p1, Lkotlin/ranges/IntProgression;->d:I

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getInputRanges()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->getSelectedOption(ILjava/util/Map;)Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object p1

    move-object v5, p1

    move-object v6, v0

    goto :goto_0

    :cond_0
    move-object v6, v0

    move-object v5, v1

    :goto_0
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreApplicationCommands;->getApplicationMap()Ljava/util/Map;

    move-result-object p1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/stores/ModelApplicationCommand;->getApplicationId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Lcom/discord/models/slashcommands/ModelApplication;

    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;-><init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public final filterMentionsForCommandContext(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 13

    const-string v0, "modelChat"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->isCommand()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommandOption()Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v3

    const-string v4, "WidgetChatInputCommandsM\u2026FromCommandOption(option)"

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v2

    if-ne v3, v2, :cond_0

    new-instance v1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInputMentionsMap()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    move-result-object v8

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createFromCommandOption(Lcom/discord/stores/ModelApplicationCommandOption;)Ljava/util/Collection;

    move-result-object v9

    invoke-static {v9, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/16 v11, 0x10

    const/4 v12, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v12}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    const/4 v5, 0x4

    if-eq v3, v5, :cond_b

    const/4 v6, 0x5

    if-eq v3, v6, :cond_8

    const/4 v6, 0x6

    if-eq v3, v6, :cond_4

    const/4 v6, 0x7

    if-eq v3, v6, :cond_1

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createFromCommandOption(Lcom/discord/stores/ModelApplicationCommandOption;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v0, v1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_6

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v7

    if-ne v7, v5, :cond_3

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_2

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    const/4 v5, 0x1

    goto :goto_3

    :cond_6
    const/4 v5, 0x0

    :goto_3
    if-eqz v5, :cond_5

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    move-object v5, v3

    goto :goto_7

    :cond_8
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v5

    if-nez v5, :cond_a

    const/4 v5, 0x1

    goto :goto_5

    :cond_a
    const/4 v5, 0x0

    :goto_5
    if-eqz v5, :cond_9

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_b
    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createFromCommandOption(Lcom/discord/stores/ModelApplicationCommandOption;)Ljava/util/Collection;

    move-result-object v0

    :goto_6
    move-object v5, v0

    :goto_7
    new-instance v0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInputMentionsMap()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    move-result-object v4

    const-string p1, "mentions"

    invoke-static {v5, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0

    :cond_c
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/MentionToken;->getLeadingIdentifier()Ljava/lang/Character;

    move-result-object v0

    goto :goto_8

    :cond_d
    const/4 v0, 0x0

    :goto_8
    const/16 v3, 0x2f

    if-nez v0, :cond_e

    goto/16 :goto_b

    :cond_e
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v0, v3, :cond_15

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/MentionToken;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_f

    const/4 v1, 0x1

    :cond_f
    if-eqz v1, :cond_15

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, Lcom/discord/models/slashcommands/ModelApplicationComparator;->Companion:Lcom/discord/models/slashcommands/ModelApplicationComparator$Companion;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_10
    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v5

    const-string v6, "command.application"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v8

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    :cond_11
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-eqz v5, :cond_10

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_12
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_13
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v3}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-eqz v4, :cond_13

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v2

    if-eqz v5, :cond_13

    invoke-static {v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createSlashCommandApplication(Lcom/discord/models/slashcommands/ModelApplication;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v3

    const-string v5, "WidgetChatInputCommandsM\u2026shCommandApplication(app)"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_a

    :cond_14
    new-instance v0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInputMentionsMap()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    move-result-object v6

    const/4 v8, 0x0

    const/16 v9, 0x10

    const/4 v10, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v10}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0

    :cond_15
    :goto_b
    return-object p1
.end method

.method public final filterMentionsFromToken(Lcom/discord/widgets/chat/input/MentionToken;Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/MentionToken;",
            "Lcom/discord/widgets/chat/input/InputModel;",
            "Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;)",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "commandContext"

    invoke-static {p3, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "mentions"

    invoke-static {p4, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getFormattedToken()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getLeadingIdentifier()Ljava/lang/Character;

    move-result-object p2

    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->hasSelectedOptionWithChoices()Z

    move-result p2

    if-eqz p2, :cond_2

    return-object p4

    :cond_2
    if-eqz v0, :cond_3

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    return-object p1

    :cond_3
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_4
    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_7

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {p4}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getLeadingIdentifier()Ljava/lang/Character;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getLeadingIdentifier()Ljava/lang/Character;

    move-result-object v3

    if-nez v3, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    if-eq v2, v3, :cond_6

    goto :goto_2

    :cond_6
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "Locale.getDefault()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getFormattedToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "null cannot be cast to non-null type java.lang.String"

    invoke-static {v4, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {v0, v3, v1, v2}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    return-object p2
.end method

.method public final getMentionAutoCompleteToken(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/MentionToken;
    .locals 7

    const-string v0, "chatInputCommandsModel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/InputModel;->getSelected()Lkotlin/ranges/IntRange;

    move-result-object v1

    iget v1, v1, Lkotlin/ranges/IntProgression;->d:I

    invoke-static {v0, v1}, Lcom/discord/widgets/chat/input/MentionUtilsKt;->getSelectedToken(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommandOption()Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    if-eqz v0, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lx/s/r;->removePrefix(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v3

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->hasSelectedFreeformInputOption()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getInputRanges()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v3

    :goto_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getValues()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/InputModel;->getSelected()Lkotlin/ranges/IntRange;

    move-result-object v5

    iget v5, v5, Lkotlin/ranges/IntProgression;->d:I

    iget v6, v4, Lkotlin/ranges/IntProgression;->d:I

    if-le v5, v6, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/InputModel;->getSelected()Lkotlin/ranges/IntRange;

    move-result-object p1

    iget p1, p1, Lkotlin/ranges/IntProgression;->d:I

    iget v0, v4, Lkotlin/ranges/IntProgression;->d:I

    sub-int/2addr p1, v0

    invoke-virtual {v1, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string p1, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    new-instance p1, Lcom/discord/widgets/chat/input/MentionToken;

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const-string v0, ""

    :goto_2
    invoke-direct {p1, v3, v0}, Lcom/discord/widgets/chat/input/MentionToken;-><init>(Ljava/lang/Character;Ljava/lang/String;)V

    return-object p1

    :cond_4
    if-eqz v0, :cond_5

    invoke-static {v0}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_5

    invoke-static {}, Lcom/discord/widgets/chat/input/MentionUtilsKt;->getDEFAULT_LEADING_IDENTIFIERS()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    new-instance p1, Lcom/discord/widgets/chat/input/MentionToken;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-direct {p1, v1, v0}, Lcom/discord/widgets/chat/input/MentionToken;-><init>(Ljava/lang/Character;Ljava/lang/String;)V

    return-object p1

    :cond_5
    return-object v3
.end method

.method public final getSelectedCommand(Ljava/util/List;Ljava/lang/String;Lcom/discord/widgets/chat/input/InputModel;)Lcom/discord/stores/ModelApplicationCommand;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/discord/widgets/chat/input/InputModel;",
            ")",
            "Lcom/discord/stores/ModelApplicationCommand;"
        }
    .end annotation

    const-string v0, "commands"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prefix"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {v4}, Lcom/discord/stores/ModelApplicationCommand;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_2
    move-object v0, v3

    :goto_1
    move-object p1, v0

    check-cast p1, Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {p3}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    move-result-object p1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    invoke-static {p1, p2, v2, p3}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result p1

    if-eqz p1, :cond_3

    move-object v3, v0

    :cond_3
    check-cast v3, Lcom/discord/stores/ModelApplicationCommand;

    :cond_4
    return-object v3
.end method

.method public final isBoolean(Ljava/lang/String;)Z
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "Locale.getDefault()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string/jumbo v0, "true"

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "false"

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    return p1
.end method

.method public final mapInputToMentions(Ljava/lang/String;Ljava/util/Collection;Z)Lcom/discord/widgets/chat/input/ChatInputMentionsMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;Z)",
            "Lcom/discord/widgets/chat/input/ChatInputMentionsMap;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mentionModels"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v2

    const/16 v3, 0x7d0

    if-le v2, v3, :cond_0

    if-nez p3, :cond_0

    sget-object p3, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->TAG_SYMBOLS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/regex/Matcher;->matches()Z

    move-result p3

    if-nez p3, :cond_0

    new-instance p2, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    sget-object p3, Lx/h/m;->d:Lx/h/m;

    invoke-direct {p2, p1, p3}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object p2

    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {p3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getDisplayTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "commandModel.displayTag"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {p1, v2, v3, v3, v4}, Lx/s/r;->indexOf$default(Ljava/lang/CharSequence;Ljava/lang/String;IZI)I

    move-result v5

    :goto_1
    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v5

    new-instance v7, Lkotlin/ranges/IntRange;

    invoke-direct {v7, v5, v6}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    invoke-static {p1, v2, v5, v3, v4}, Lx/s/r;->indexOf$default(Ljava/lang/CharSequence;Ljava/lang/String;IZI)I

    move-result v5

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p3

    goto :goto_0

    :cond_3
    new-instance p2, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    invoke-direct {p2, p1, v1}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object p2
.end method

.method public final verifyCommandInput(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;)Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;
    .locals 11

    const-string v0, "commandContext"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mentionsMap"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_c

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getValues()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v2}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-eqz v6, :cond_1

    invoke-static {v6}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_1
    move-object v6, v3

    :goto_1
    if-eqz v6, :cond_a

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;->getMentions()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v9}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getDisplayTag()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    goto :goto_2

    :cond_3
    move-object v8, v3

    :goto_2
    check-cast v8, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-eqz v7, :cond_7

    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    xor-int/2addr v7, v10

    if-ne v7, v10, :cond_7

    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v7

    instance-of v8, v7, Ljava/util/Collection;

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    goto/16 :goto_4

    :cond_4
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/slashcommands/CommandChoices;

    invoke-virtual {v8}, Lcom/discord/models/slashcommands/CommandChoices;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_6
    :goto_3
    :pswitch_0
    const/4 v9, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_1
    if-eqz v8, :cond_9

    invoke-virtual {v8}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_9

    goto :goto_3

    :pswitch_2
    if-eqz v8, :cond_9

    invoke-virtual {v8}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_9

    goto :goto_3

    :pswitch_3
    if-eqz v8, :cond_8

    invoke-virtual {v8}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v6

    if-eqz v6, :cond_6

    :cond_8
    if-eqz v8, :cond_9

    invoke-virtual {v8}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v6

    if-ne v6, v10, :cond_9

    goto :goto_3

    :pswitch_4
    sget-object v7, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    invoke-virtual {v7, v6}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->isBoolean(Ljava/lang/String;)Z

    move-result v9

    goto :goto_4

    :pswitch_5
    invoke-static {v6}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_9

    goto :goto_3

    :pswitch_6
    invoke-static {v6}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    goto :goto_3

    :cond_9
    :goto_4
    :pswitch_7
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v9, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommandOption()Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v6

    invoke-static {v6, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v10

    if-eqz v6, :cond_0

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommandOption;->getRequired()Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v6, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_b
    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;-><init>(Ljava/util/Map;Ljava/util/Set;)V

    return-object p1

    :cond_c
    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    const/4 p2, 0x3

    invoke-direct {p1, v3, v3, p2, v3}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;-><init>(Ljava/util/Map;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
