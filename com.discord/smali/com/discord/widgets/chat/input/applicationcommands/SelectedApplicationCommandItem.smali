.class public final Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;
.super Ljava/lang/Object;
.source "SelectedApplicationCommandAdapter.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem$Companion;

.field public static final TYPE_PARAM:I = 0x1

.field public static final TYPE_SECTION_HEADING:I = 0x2

.field public static final TYPE_TITLE:I


# instance fields
.field private final avatar:Ljava/lang/String;

.field private completed:Z

.field private error:Z

.field private final heading:Ljava/lang/String;

.field private highlighted:Z

.field private final option:Lcom/discord/stores/ModelApplicationCommandOption;

.field private final title:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->Companion:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->option:Lcom/discord/stores/ModelApplicationCommandOption;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->highlighted:Z

    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->error:Z

    iput-boolean p4, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->completed:Z

    iput-object p5, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->title:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->avatar:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->heading:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p9, p8, 0x1

    const/4 v0, 0x0

    if-eqz p9, :cond_0

    move-object p9, v0

    goto :goto_0

    :cond_0
    move-object p9, p1

    :goto_0
    and-int/lit8 p1, p8, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    move v2, p2

    :goto_1
    and-int/lit8 p1, p8, 0x4

    if-eqz p1, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    move v3, p3

    :goto_2
    and-int/lit8 p1, p8, 0x8

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move v1, p4

    :goto_3
    and-int/lit8 p1, p8, 0x10

    if-eqz p1, :cond_4

    move-object v4, v0

    goto :goto_4

    :cond_4
    move-object v4, p5

    :goto_4
    and-int/lit8 p1, p8, 0x20

    if-eqz p1, :cond_5

    move-object v5, v0

    goto :goto_5

    :cond_5
    move-object v5, p6

    :goto_5
    and-int/lit8 p1, p8, 0x40

    if-eqz p1, :cond_6

    move-object p8, v0

    goto :goto_6

    :cond_6
    move-object p8, p7

    :goto_6
    move-object p1, p0

    move-object p2, p9

    move p3, v2

    move p4, v3

    move p5, v1

    move-object p6, v4

    move-object p7, v5

    invoke-direct/range {p1 .. p8}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getAvatar()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->avatar:Ljava/lang/String;

    return-object v0
.end method

.method public final getCompleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->completed:Z

    return v0
.end method

.method public final getError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->error:Z

    return v0
.end method

.method public final getHeading()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->heading:Ljava/lang/String;

    return-object v0
.end method

.method public final getHighlighted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->highlighted:Z

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->getType()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->heading:Ljava/lang/String;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, "heading"

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->option:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    const-string v0, "option"

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->title:Ljava/lang/String;

    if-eqz v0, :cond_5

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "title"

    :goto_0
    return-object v0
.end method

.method public final getOption()Lcom/discord/stores/ModelApplicationCommandOption;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->option:Lcom/discord/stores/ModelApplicationCommandOption;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->option:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->title:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    :goto_0
    return v0
.end method

.method public final setCompleted(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->completed:Z

    return-void
.end method

.method public final setError(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->error:Z

    return-void
.end method

.method public final setHighlighted(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->highlighted:Z

    return-void
.end method
