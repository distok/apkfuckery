.class public final Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;
.super Ljava/lang/Object;
.source "WidgetChatInputApplicationCommands.kt"


# instance fields
.field private final applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

.field private final applicationCommandsRoot:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final descriptionText:Landroid/widget/TextView;

.field private final editText:Lcom/lytefast/flexinput/widget/FlexEditText;

.field private guildId:Ljava/lang/Long;

.field private input:Lcom/discord/widgets/chat/input/InputModel;

.field private lastModelChat:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

.field private lastQuery:Ljava/lang/String;

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private selectedCommand:Lcom/discord/stores/ModelApplicationCommand;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/widget/FlexEditText;Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 3

    const-string v0, "editText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applicationCommandsRoot"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsRoot:Landroidx/constraintlayout/widget/ConstraintLayout;

    const p1, 0x7f0a0216

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "applicationCommandsRoot.\u2026cation_commands_recycler)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a0215

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "applicationCommandsRoot.\u2026mands_option_description)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->descriptionText:Landroid/widget/TextView;

    new-instance p2, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$applicationCommandsParamAdapter$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$applicationCommandsParamAdapter$1;-><init>(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;)V

    invoke-direct {p2, p1, v0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public static final synthetic access$queryCommands(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/widgets/chat/input/InputModel;J)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->queryCommands(Lcom/discord/widgets/chat/input/InputModel;J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final appendParam(Lcom/discord/stores/ModelApplicationCommandOption;)V
    .locals 8

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v0, v2, v3, v4}, Lx/s/r;->endsWith$default(Ljava/lang/CharSequence;CZI)Z

    move-result v2

    const-string v3, ""

    if-nez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    const-string v2, " "

    goto :goto_0

    :cond_0
    move-object v2, v3

    :goto_0
    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x3a

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v4, 0x5

    const-string v6, "@"

    if-eq p1, v4, :cond_2

    const/4 v4, 0x6

    if-eq p1, v4, :cond_1

    const/4 v4, 0x7

    if-eq p1, v4, :cond_2

    goto :goto_1

    :cond_1
    const-string v3, "#"

    goto :goto_1

    :cond_2
    move-object v3, v6

    :goto_1
    invoke-static {v2, v3}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    const-string v4, "editText.editableText"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2, v3, p1, v6, v7}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;->insert(Landroid/text/Editable;Ljava/lang/CharSequence;II)V

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v1, v5, v3}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applyParamSpan(Landroid/text/Spannable;IIZ)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, v0

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method private final handleCommandToken(Ljava/lang/String;J)V
    .locals 3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationCommands;->clearQueryCommands()V

    iput-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastQuery:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object p1

    invoke-virtual {p1, p2, p3, v1}, Lcom/discord/stores/StoreApplicationCommands;->requestInitialApplicationCommands(JLjava/lang/Long;)V

    iput-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastQuery:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastQuery:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3, v1}, Lcom/discord/stores/StoreApplicationCommands;->requestApplicationCommandsQuery(JLjava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastQuery:Ljava/lang/String;

    :cond_2
    :goto_0
    return-void
.end method

.method private final observeGuildId(Lcom/discord/app/AppFragment;)V
    .locals 12

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSelected;->observeSelectedGuildId()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    new-instance v9, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;-><init>(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final onApplicationOptionClicked(Lcom/discord/stores/ModelApplicationCommandOption;Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastModelChat:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getInputRanges()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x5

    const/4 v1, 0x1

    if-eq p1, p2, :cond_0

    const/4 p2, 0x6

    if-eq p1, p2, :cond_0

    const/4 p2, 0x7

    if-eq p1, p2, :cond_0

    const/4 v1, 0x0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    iget p2, v0, Lkotlin/ranges/IntProgression;->d:I

    add-int/2addr p2, v1

    iget v0, v0, Lkotlin/ranges/IntProgression;->e:I

    invoke-virtual {p1, p2, v0}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->appendParam(Lcom/discord/stores/ModelApplicationCommandOption;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public static synthetic onApplicationOptionClicked$default(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/stores/ModelApplicationCommandOption;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->onApplicationOptionClicked(Lcom/discord/stores/ModelApplicationCommandOption;Z)V

    return-void
.end method

.method private final queryCommands(Lcom/discord/widgets/chat/input/InputModel;J)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^(/[^\\s]*)+"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lkotlin/text/Regex;->find(Ljava/lang/CharSequence;I)Lkotlin/text/MatchResult;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lx/s/e;

    invoke-virtual {p1}, Lx/s/e;->getGroupValues()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->handleCommandToken(Ljava/lang/String;J)V

    return-object p1
.end method

.method private final showApplicationCommandsUi(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsRoot:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public static synthetic showNextOption$default(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->showNextOption(Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Z)V

    return-void
.end method


# virtual methods
.method public final applyParamSpan(Landroid/text/Spannable;IIZ)V
    .locals 8

    const-string/jumbo v0, "spannable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    const v1, 0x7f04012e

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v5

    if-eqz p4, :cond_0

    iget-object p4, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    const v0, 0x7f040153

    invoke-static {p4, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p4

    goto :goto_0

    :cond_0
    iget-object p4, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    const v0, 0x7f060238

    invoke-static {p4, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result p4

    :goto_0
    move v6, p4

    new-instance p4, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v3

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v4

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    int-to-float v7, v0

    move-object v2, p4

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;-><init>(IIIIF)V

    const/16 v0, 0x21

    invoke-interface {p1, p4, p2, p3, v0}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method public final configureObservables(Lcom/discord/app/AppFragment;)V
    .locals 1

    const-string v0, "appFragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->observeGuildId(Lcom/discord/app/AppFragment;)V

    return-void
.end method

.method public final configureUi(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)V
    .locals 7

    const-string v0, "chatInputModel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->isCommand()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->showApplicationCommandsUi(Z)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastModelChat:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v3

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastModelChat:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->descriptionText:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v4

    const-string v5, "applicationCommandsRoot.resources"

    if-eqz v4, :cond_1

    iget-object v6, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsRoot:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v6}, Lcom/discord/stores/StoreApplicationCommandsKt;->getDescriptionText(Lcom/discord/stores/ModelApplicationCommand;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v2

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->setApplicationCommand(Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/models/slashcommands/ModelApplication;)V

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommandOption()Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->descriptionText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsRoot:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/discord/stores/StoreApplicationCommandsKt;->getDescriptionText(Lcom/discord/stores/ModelApplicationCommandOption;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    invoke-virtual {v2, v3}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->highlightOption(Lcom/discord/stores/ModelApplicationCommandOption;)V

    goto :goto_2

    :cond_2
    iget-object v3, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->descriptionText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    if-eqz v4, :cond_3

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsRoot:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v2}, Lcom/discord/stores/StoreApplicationCommandsKt;->getDescriptionText(Lcom/discord/stores/ModelApplicationCommand;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->clearParamOptionHighlight()V

    :goto_2
    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getInputVerification()Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->setVerified(Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applicationCommandsParamAdapter:Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;->clear()V

    :goto_3
    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->showNextOption$default(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;ZILjava/lang/Object;)V

    :cond_5
    return-void
.end method

.method public final getCommandContext(Lcom/discord/widgets/chat/input/InputModel;Ljava/util/List;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/InputModel;",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)",
            "Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queryCommands"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->input:Lcom/discord/widgets/chat/input/InputModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->guildId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->queryCommands(Lcom/discord/widgets/chat/input/InputModel;J)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    invoke-virtual {v1, p2, v0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->getSelectedCommand(Ljava/util/List;Ljava/lang/String;Lcom/discord/widgets/chat/input/InputModel;)Lcom/discord/stores/ModelApplicationCommand;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {v1, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->createCommandContextFromInputAndSelectedCommand(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/stores/ModelApplicationCommand;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;-><init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getInput()Lcom/discord/widgets/chat/input/InputModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->input:Lcom/discord/widgets/chat/input/InputModel;

    return-object v0
.end method

.method public final getLastModelChat()Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastModelChat:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    return-object v0
.end method

.method public final getLastQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastQuery:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-object v0
.end method

.method public final selectApplicationCommand(Lcom/discord/stores/ModelApplicationCommand;)V
    .locals 1

    const-string v0, "command"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-void
.end method

.method public final setGuildId(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->guildId:Ljava/lang/Long;

    return-void
.end method

.method public final setInput(Lcom/discord/widgets/chat/input/InputModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->input:Lcom/discord/widgets/chat/input/InputModel;

    return-void
.end method

.method public final setLastModelChat(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastModelChat:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    return-void
.end method

.method public final setLastQuery(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->lastQuery:Ljava/lang/String;

    return-void
.end method

.method public final setSelectedCommand(Lcom/discord/stores/ModelApplicationCommand;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-void
.end method

.method public final showNextOption(Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Z)V
    .locals 3

    const-string v0, "command"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-static {v1, p2}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/stores/ModelApplicationCommandOption;

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Lcom/discord/stores/ModelApplicationCommandOption;->getRequired()Z

    move-result p3

    if-eqz p3, :cond_1

    :cond_0
    const/4 p3, 0x2

    invoke-static {p0, p2, v0, p3, v1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->onApplicationOptionClicked$default(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/stores/ModelApplicationCommandOption;ZILjava/lang/Object;)V

    :cond_1
    sget-object v1, Lkotlin/Unit;->a:Lkotlin/Unit;

    :cond_2
    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->onApplicationOptionClicked(Lcom/discord/stores/ModelApplicationCommandOption;Z)V

    :goto_0
    return-void
.end method
