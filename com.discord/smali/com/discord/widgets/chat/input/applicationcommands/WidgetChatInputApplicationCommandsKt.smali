.class public final Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommandsKt;
.super Ljava/lang/Object;
.source "WidgetChatInputApplicationCommands.kt"


# direct methods
.method public static final findOptionRanges(Ljava/lang/String;Lcom/discord/stores/ModelApplicationCommand;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ")",
            "Ljava/util/Map<",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            "Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$findOptionRanges"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {v2}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommandsKt;->findStartOfParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v4}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommandsKt;->findValueRange(Ljava/lang/String;Lcom/discord/stores/ModelApplicationCommand;Ljava/lang/String;)Lkotlin/ranges/IntRange;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v5, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    new-instance v6, Lkotlin/ranges/IntRange;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v7, v4, Lkotlin/ranges/IntProgression;->d:I

    invoke-direct {v6, v3, v7}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-direct {v5, v6, v4}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;-><init>(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)V

    invoke-interface {v0, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static final findStartOfParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3a

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-static {p0, p1, v0, v0, v1}, Lx/s/r;->indexOf$default(Ljava/lang/CharSequence;Ljava/lang/String;IZI)I

    move-result p0

    const/4 p1, -0x1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    add-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static final findStartOfValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3a

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-static {p0, p1, v0, v0, v1}, Lx/s/r;->indexOf$default(Ljava/lang/CharSequence;Ljava/lang/String;IZI)I

    move-result p0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, p0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final findValueRange(Ljava/lang/String;Lcom/discord/stores/ModelApplicationCommand;Ljava/lang/String;)Lkotlin/ranges/IntRange;
    .locals 11

    const-string v0, "$this$findValueRange"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paramName"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommandsKt;->findStartOfValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_b

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, " (\\w*):"

    invoke-direct {v1, v2}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lkotlin/text/Regex;->findAll(Ljava/lang/CharSequence;I)Lkotlin/sequences/Sequence;

    move-result-object v1

    check-cast v1, Lx/r/i;

    new-instance v2, Lx/r/i$a;

    invoke-direct {v2, v1}, Lx/r/i$a;-><init>(Lx/r/i;)V

    :cond_0
    invoke-virtual {v2}, Lx/r/i$a;->hasNext()Z

    move-result v1

    const/4 v4, -0x1

    if-eqz v1, :cond_9

    invoke-virtual {v2}, Lx/r/i$a;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/text/MatchResult;

    invoke-interface {v1}, Lkotlin/text/MatchResult;->getGroups()Lx/s/d;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lx/s/c;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v6, :cond_3

    iget-object v10, v6, Lx/s/c;->a:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v10, v0

    :goto_0
    invoke-virtual {v9}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    goto :goto_1

    :cond_4
    move-object v8, v0

    :goto_1
    check-cast v8, Lcom/discord/stores/ModelApplicationCommandOption;

    goto :goto_2

    :cond_5
    move-object v8, v0

    :goto_2
    if-eqz v8, :cond_6

    const/4 v6, 0x1

    goto :goto_3

    :cond_6
    const/4 v6, 0x0

    :goto_3
    if-eqz v6, :cond_1

    goto :goto_4

    :cond_7
    move-object v5, v0

    :goto_4
    check-cast v5, Lx/s/c;

    if-eqz v5, :cond_8

    iget-object v1, v5, Lx/s/c;->b:Lkotlin/ranges/IntRange;

    if-eqz v1, :cond_8

    iget v1, v1, Lkotlin/ranges/IntProgression;->d:I

    goto :goto_5

    :cond_8
    const/4 v1, -0x1

    :goto_5
    if-eq v1, v4, :cond_0

    goto :goto_6

    :cond_9
    const/4 v1, -0x1

    :goto_6
    if-ne v1, v4, :cond_a

    new-instance p1, Lkotlin/ranges/IntRange;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    invoke-direct {p1, p2, p0}, Lkotlin/ranges/IntRange;-><init>(II)V

    goto :goto_7

    :cond_a
    new-instance p1, Lkotlin/ranges/IntRange;

    add-int/2addr v1, p2

    invoke-direct {p1, p2, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    :goto_7
    return-object p1

    :cond_b
    return-object v0
.end method
