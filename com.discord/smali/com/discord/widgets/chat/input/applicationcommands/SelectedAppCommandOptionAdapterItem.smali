.class public final Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "SelectedApplicationCommandAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;",
        "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final paramName$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;

    const-string v3, "paramName"

    const-string v4, "getParamName()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d0118

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    const p1, 0x7f0a009f

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->paramName$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;)Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;

    return-object p0
.end method

.method private final getParamName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->paramName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;)V
    .locals 3

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->getOption()Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->getParamName()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->getHighlighted()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080118

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->getCompleted()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f080117

    goto :goto_0

    :cond_1
    const v0, 0x7f080119

    :goto_0
    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->getHighlighted()Z

    move-result v1

    const v2, 0x7f040153

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->getParamName()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;->getError()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->getParamName()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f060238

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v1

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->getParamName()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->getParamName()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->getParamName()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem$onConfigure$$inlined$let$lambda$1;

    invoke-direct {v1, p1, p0, p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem$onConfigure$$inlined$let$lambda$1;-><init>(Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;->onConfigure(ILcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;)V

    return-void
.end method
