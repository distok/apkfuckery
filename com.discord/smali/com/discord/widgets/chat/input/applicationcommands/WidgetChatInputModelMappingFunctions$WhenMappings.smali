.class public final synthetic Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 10

    invoke-static {}, Lcom/discord/models/slashcommands/ApplicationCommandType;->values()[Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/16 v0, 0x9

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/models/slashcommands/ApplicationCommandType;->CHANNEL:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/4 v2, 0x6

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v4, Lcom/discord/models/slashcommands/ApplicationCommandType;->USER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/4 v4, 0x5

    const/4 v5, 0x2

    aput v5, v1, v4

    sget-object v6, Lcom/discord/models/slashcommands/ApplicationCommandType;->ROLE:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/4 v6, 0x7

    const/4 v7, 0x3

    aput v7, v1, v6

    sget-object v8, Lcom/discord/models/slashcommands/ApplicationCommandType;->BOOLEAN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/4 v8, 0x4

    aput v8, v1, v8

    invoke-static {}, Lcom/discord/models/slashcommands/ApplicationCommandType;->values()[Lcom/discord/models/slashcommands/ApplicationCommandType;

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v9, Lcom/discord/models/slashcommands/ApplicationCommandType;->SUBCOMMAND:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/4 v9, 0x0

    aput v3, v1, v9

    sget-object v9, Lcom/discord/models/slashcommands/ApplicationCommandType;->SUBCOMMAND_GROUP:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput v5, v1, v3

    sget-object v3, Lcom/discord/models/slashcommands/ApplicationCommandType;->STRING:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput v7, v1, v5

    sget-object v3, Lcom/discord/models/slashcommands/ApplicationCommandType;->INTEGER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput v8, v1, v7

    aput v4, v1, v8

    aput v2, v1, v4

    aput v6, v1, v2

    const/16 v2, 0x8

    aput v2, v1, v6

    sget-object v3, Lcom/discord/models/slashcommands/ApplicationCommandType;->UNKNOWN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    aput v0, v1, v2

    return-void
.end method
