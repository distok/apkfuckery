.class public final Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
.super Ljava/lang/Object;
.source "WidgetChatInputApplicationCommands.kt"


# instance fields
.field private final inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

.field private final optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

.field private final selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

.field private final selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

.field private final selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;-><init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V
    .locals 1

    const-string v0, "optionValues"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputVerification"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object p7, v0

    goto :goto_0

    :cond_0
    move-object p7, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    move-object v1, v0

    goto :goto_1

    :cond_1
    move-object v1, p2

    :goto_1
    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    move-object v2, v0

    goto :goto_2

    :cond_2
    move-object v2, p3

    :goto_2
    and-int/lit8 p1, p6, 0x8

    const/4 p2, 0x3

    if-eqz p1, :cond_3

    new-instance p4, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    invoke-direct {p4, v0, v0, p2, v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;-><init>(Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_3
    move-object v3, p4

    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_4

    new-instance p5, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    invoke-direct {p5, v0, v0, p2, v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;-><init>(Ljava/util/Map;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_4
    move-object p6, p5

    move-object p1, p0

    move-object p2, p7

    move-object p3, v1

    move-object p4, v2

    move-object p5, v3

    invoke-direct/range {p1 .. p6}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;-><init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->copy(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/slashcommands/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/ModelApplicationCommand;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-object v0
.end method

.method public final component3()Lcom/discord/stores/ModelApplicationCommandOption;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    return-object v0
.end method

.method public final component4()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    return-object v0
.end method

.method public final component5()Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 7

    const-string v0, "optionValues"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputVerification"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;-><init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInputVerification()Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    return-object v0
.end method

.method public final getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    return-object v0
.end method

.method public final getSelectedApplication()Lcom/discord/models/slashcommands/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    return-object v0
.end method

.method public final getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    return-object v0
.end method

.method public final getSelectedCommandOption()Lcom/discord/stores/ModelApplicationCommandOption;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    return-object v0
.end method

.method public final hasSelectedFreeformInputOption()Z
    .locals 5

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->isCommand()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v3

    :goto_0
    sget-object v4, Lcom/discord/models/slashcommands/ApplicationCommandType;->INTEGER:Lcom/discord/models/slashcommands/ApplicationCommandType;

    if-eq v0, v4, :cond_6

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v3

    :cond_2
    sget-object v0, Lcom/discord/models/slashcommands/ApplicationCommandType;->STRING:Lcom/discord/models/slashcommands/ApplicationCommandType;

    if-ne v3, v0, :cond_5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    :cond_6
    :goto_3
    return v1
.end method

.method public final hasSelectedOptionWithChoices()Z
    .locals 3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->isCommand()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommand;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/discord/models/slashcommands/ApplicationCommandType;->BOOLEAN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/slashcommands/ModelApplication;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/stores/ModelApplicationCommand;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/stores/ModelApplicationCommandOption;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isCommand()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "CommandContext(selectedApplication="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedApplication:Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedCommand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommand:Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedCommandOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->selectedCommandOption:Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->optionValues:Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", inputVerification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->inputVerification:Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
