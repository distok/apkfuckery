.class public final Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;
.super Ljava/lang/Object;
.source "WidgetChatInputApplicationCommands.kt"


# instance fields
.field private final param:Lkotlin/ranges/IntRange;

.field private final value:Lkotlin/ranges/IntRange;


# direct methods
.method public constructor <init>(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)V
    .locals 1

    const-string v0, "param"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->copy(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lkotlin/ranges/IntRange;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public final component2()Lkotlin/ranges/IntRange;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public final copy(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;
    .locals 1

    const-string v0, "param"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;-><init>(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getParam()Lkotlin/ranges/IntRange;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public final getValue()Lkotlin/ranges/IntRange;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/ranges/IntRange;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lkotlin/ranges/IntRange;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "OptionRange(param="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->param:Lkotlin/ranges/IntRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->value:Lkotlin/ranges/IntRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
