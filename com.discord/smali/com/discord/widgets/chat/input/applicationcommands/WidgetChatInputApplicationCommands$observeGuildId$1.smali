.class public final Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;
.super Lx/m/c/k;
.source "WidgetChatInputApplicationCommands.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->observeGuildId(Lcom/discord/app/AppFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;->this$0:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;->invoke(J)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;->this$0:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->setGuildId(Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;->this$0:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands$observeGuildId$1;->this$0:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-static {v1, v0, p1, p2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->access$queryCommands(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/widgets/chat/input/InputModel;J)Ljava/lang/String;

    :cond_0
    return-void
.end method
