.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->get(Landroid/content/Context;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/models/domain/ModelUser$Me;",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    check-cast p2, Lcom/discord/models/domain/ModelUser$Me;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;->call(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    move-result-object p1

    return-object p1
.end method
