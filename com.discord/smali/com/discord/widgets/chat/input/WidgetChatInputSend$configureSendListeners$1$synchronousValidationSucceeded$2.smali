.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;
.super Lx/m/c/k;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke(Ljava/util/List;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $attachments:Ljava/util/ArrayList;

.field public final synthetic $currentFileSizeMB:F

.field public final synthetic $hasImage:Z

.field public final synthetic $hasVideo:Z

.field public final synthetic $maxAttachmentSizeMB:F

.field public final synthetic $messageResendCompressedHandler:Lkotlin/jvm/functions/Function0;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;FFLjava/util/ArrayList;ZZLkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iput p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$currentFileSizeMB:F

    iput p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$maxAttachmentSizeMB:F

    iput-object p4, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$attachments:Ljava/util/ArrayList;

    iput-boolean p5, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$hasImage:Z

    iput-boolean p6, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$hasVideo:Z

    iput-object p7, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$messageResendCompressedHandler:Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->invoke(IZ)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(IZ)V
    .locals 10

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iget-object v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;

    if-eqz v1, :cond_0

    iget v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$currentFileSizeMB:F

    iget v4, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$maxAttachmentSizeMB:F

    iget-object v6, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$attachments:Ljava/util/ArrayList;

    iget-boolean v7, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$hasImage:Z

    iget-boolean v8, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$hasVideo:Z

    iget-object v9, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;->$messageResendCompressedHandler:Lkotlin/jvm/functions/Function0;

    move v2, p1

    move v5, p2

    invoke-interface/range {v1 .. v9}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;->onFilesTooLarge(IFFZLjava/util/List;ZZLkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method
