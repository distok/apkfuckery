.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;
.super Lx/m/c/k;
.source "WidgetChatInputAutocomplete.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/input/ChatInputCommandsModel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->invoke(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/MentionToken;->getToken()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->setLastToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->access$getApplicationCommands$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    move-result-object v0

    const-string v1, "model"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->configureUi(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v1, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInput()Lcom/discord/widgets/chat/input/InputModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getMentionModels()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->filterMentionsFromToken(Lcom/discord/widgets/chat/input/MentionToken;Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/MentionToken;->getLeadingIdentifier()Ljava/lang/Character;

    move-result-object p1

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result p1

    if-ne p1, v2, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/MentionToken;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->access$getMentionsAdapter$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    move-result-object p1

    invoke-static {v1}, Lx/h/f;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->setData(Ljava/util/List;Ljava/lang/Boolean;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->access$getMentionsAdapter$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_5
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->access$getMentionsAdapter$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    move-result-object p1

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->access$getMentionsAdapter$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void
.end method
