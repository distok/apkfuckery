.class public Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
.super Ljava/lang/Object;
.source "WidgetChatInputCommandsModel.java"

# interfaces
.implements Lcom/discord/widgets/chat/input/TagObject;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/widgets/chat/input/TagObject;",
        "Ljava/lang/Comparable<",
        "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPTY:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation
.end field

.field private static PARTITION_HUGE_GUILD_SIZE:I = 0xbb8

.field private static PARTITION_IDEAL_PARTITION_SIZE:I = 0x64

.field public static final TYPE_CHANNEL:I = 0x2

.field public static final TYPE_CUSTOM:I = 0x8

.field public static final TYPE_EMOJI:I = 0x3

.field public static final TYPE_MENTION:I = 0x4

.field public static final TYPE_SLASH:I = 0x5

.field public static final TYPE_SLASH_APPLICATION:I = 0x7

.field public static final TYPE_SLASH_BUILT_IN:I = 0x6

.field public static final TYPE_USER:I = 0x0

.field public static final TYPE_USER_WITH_NICKNAME:I = 0x1


# instance fields
.field private application:Lcom/discord/models/slashcommands/ModelApplication;

.field private channel:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

.field private command:Lcom/discord/stores/ModelApplicationCommand;

.field private emoji:Lcom/discord/models/domain/emoji/Emoji;

.field private icon:Ljava/lang/String;

.field private nick:Ljava/lang/String;

.field private presence:Lcom/discord/models/domain/ModelPresence;

.field private role:Lcom/discord/models/domain/ModelGuildRole;

.field private slashAction:Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private slashDisplay:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private slashOutput:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private tag:Ljava/lang/String;

.field private tagRegex:Ljava/util/regex/Pattern;

.field private type:I

.field private user:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->EMPTY:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private compareSlashCommand(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_0
    invoke-virtual {v0}, Lcom/discord/models/slashcommands/ModelApplication;->getBuiltIn()Z

    move-result v0

    iget-object v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v1}, Lcom/discord/models/slashcommands/ModelApplication;->getBuiltIn()Z

    move-result v1

    sub-int/2addr v0, v1

    if-eqz v0, :cond_1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v0}, Lcom/discord/models/slashcommands/ModelApplication;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v1}, Lcom/discord/models/slashcommands/ModelApplication;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method private static createBuiltInSlashCommands()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/a/o/b/a/j;->a:Lf/a/o/b/a/j;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "shrug"

    const-string/jumbo v3, "\u00af\\\\_(\u30c4)\\_/\u00af"

    const-string/jumbo v4, "\u00af\\_(\u30c4)_/\u00af"

    invoke-static {v2, v3, v4, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createSlashCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v2, "tableflip"

    const-string v3, "(\u256f\u00b0\u25a1\u00b0\uff09\u256f\ufe35 \u253b\u2501\u253b"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createSlashCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v2, "unflip"

    const-string/jumbo v3, "\u252c\u2500\u252c \u30ce( \u309c-\u309c\u30ce)"

    invoke-static {v2, v3, v4, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createSlashCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method private static createChannel(Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    invoke-static {p1, p0}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    new-instance p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->channel:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    return-object v0
.end method

.method private static createChannelCommands(Landroid/content/Context;J)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreChannels;->observeChannelsForGuild(J)Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lcom/discord/stores/StoreStream;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StorePermissions;->observePermissionsForAllChannels()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lf/a/o/b/a/f;

    invoke-direct {v3, p0}, Lf/a/o/b/a/f;-><init>(Landroid/content/Context;)V

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1f4

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static createCommandsForEmoji(Lcom/discord/models/domain/emoji/EmojiSet;)Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/emoji/EmojiSet;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/discord/models/domain/emoji/EmojiSet;->unicodeEmojis:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/emoji/Emoji;

    invoke-interface {v3}, Lcom/discord/models/domain/emoji/Emoji;->getNames()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createEmoji(Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object p0, p0, Lcom/discord/models/domain/emoji/EmojiSet;->customEmojis:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/emoji/Emoji;

    invoke-interface {v2}, Lcom/discord/models/domain/emoji/Emoji;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Lcom/discord/models/domain/emoji/Emoji;->getFirstName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createEmoji(Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    return-object v0
.end method

.method private static createCommandsForMentions(JLjava/util/Map;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Collection;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPermissionOverwrite;",
            ">;JJ",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;)",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->size()I

    move-result v0

    sget v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->PARTITION_HUGE_GUILD_SIZE:I

    if-le v0, v1, :cond_0

    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->size()I

    move-result v0

    sget v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->PARTITION_IDEAL_PARTITION_SIZE:I

    div-int/2addr v0, v1

    new-instance v1, Lf/a/o/b/a/l;

    invoke-direct {v1, v0}, Lf/a/o/b/a/l;-><init>(I)V

    invoke-static {v0, v1}, Lcom/discord/utilities/collections/ShallowPartitionCollection;->withArrayListParitions(ILkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/ShallowPartitionCollection;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    const-wide/16 v1, 0x400

    move-wide/from16 v3, p0

    move-object/from16 v5, p7

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/permissions/PermissionUtils;->canEveryone(JJLjava/util/Map;Ljava/util/Map;)Z

    move-result v1

    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-nez v1, :cond_1

    const-wide/16 v6, 0x400

    move-wide/from16 v8, v17

    move-wide/from16 v10, p0

    move-wide/from16 v12, p5

    move-object v14, v5

    move-object/from16 v15, p7

    move-object/from16 v16, p2

    invoke-static/range {v6 .. v16}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JJJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v6

    if-nez v6, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v6, p9

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelUser;

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v4

    :cond_3
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v7, p10

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelPresence;

    invoke-static {v3, v4, v5}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v2, p8

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/discord/models/domain/ModelGuildMember$Computed;

    const/4 v1, 0x0

    if-eqz v13, :cond_5

    const-wide/32 v5, 0x20000

    move-wide/from16 v7, p3

    move-wide/from16 v9, p0

    move-wide/from16 v11, p5

    move-object/from16 v14, p7

    move-object/from16 v15, p2

    invoke-static/range {v5 .. v15}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JJJJLcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v2, "@here"

    invoke-static {v2, v4}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createMention(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const-string v2, "@everyone"

    invoke-static {v2, v4}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createMention(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->isMentionable()Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz v1, :cond_6

    :cond_7
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v4

    cmp-long v6, v4, p0

    if-eqz v6, :cond_6

    const/16 v4, 0x40

    invoke-static {v4}, Lf/e/c/a/a;->E(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createMention(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    return-object v0
.end method

.method private static createCommandsForMentions(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelChannel$RecipientNick;",
            ">;)",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelUser;

    invoke-static {v1, p2}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelChannel$RecipientNick;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel$RecipientNick;->getNick()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-static {v1, p1}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelPresence;

    invoke-static {v1, v2, v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string p0, "@here"

    invoke-static {p0, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createMention(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string p0, "@everyone"

    invoke-static {p0, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createMention(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static createCommandsForSlash(Ljava/util/List;Ljava/util/List;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v2}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getApplicationId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/slashcommands/ModelApplication;

    invoke-static {v2, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createSlashCommand(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method private static createEmoji(Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    const/4 v1, 0x3

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    invoke-interface {p0, p1}, Lcom/discord/models/domain/emoji/Emoji;->getCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-interface {p0, p1}, Lcom/discord/models/domain/emoji/Emoji;->getRegex(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tagRegex:Ljava/util/regex/Pattern;

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    return-object v0
.end method

.method private static createEmojiCommands(JJ)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-wide v1, p0

    move-wide v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/discord/stores/StoreEmoji;->getEmojiSet(JJZZ)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lf/a/o/b/a/a;->d:Lf/a/o/b/a/a;

    invoke-virtual {p0, p1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static createFromCommandOption(Lcom/discord/stores/ModelApplicationCommandOption;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/ModelApplicationCommandOption;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v1

    sget-object v2, Lcom/discord/models/slashcommands/ApplicationCommandType;->BOOLEAN:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const/16 v3, 0x8

    if-ne v1, v2, :cond_0

    new-instance p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    iput v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    const-string/jumbo v1, "true"

    iput-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    iput v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    const-string v1, "false"

    iput-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/slashcommands/CommandChoices;

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    iput v3, v2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    invoke-virtual {v1}, Lcom/discord/models/slashcommands/CommandChoices;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    :goto_1
    return-object v0
.end method

.method private static createMention(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    const/4 v1, 0x4

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    iput-object p1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->role:Lcom/discord/models/domain/ModelGuildRole;

    return-object v0
.end method

.method private static createMentionCommands(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    sget-object p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->EMPTY:Ljava/util/List;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getRecipients()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/o/b/a/b;->d:Lf/a/o/b/a/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/b/a/q;

    invoke-direct {v1, p0}, Lf/a/o/b/a/q;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lf/a/o/b/a/n;->d:Lf/a/o/b/a/n;

    invoke-virtual {v0, v2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lf/a/o/b/a/c;->d:Lf/a/o/b/a/c;

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/discord/stores/StoreGuilds;->observeRoles(J)Lrx/Observable;

    move-result-object v3

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v0

    new-instance v4, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x5

    invoke-direct {v4, v5, v6, v10}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;)V

    new-instance v7, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v7, v0, v4}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v7}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v4

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeAllUsers()Lrx/Observable;

    move-result-object v0

    new-instance v7, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    invoke-direct {v7, v5, v6, v10}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;)V

    new-instance v5, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v5, v0, v7}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v5}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v5

    invoke-static {}, Lcom/discord/stores/StoreStream;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserPresence;->observeAllPresences()Lrx/Observable;

    move-result-object v0

    new-instance v6, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    const-wide/16 v7, 0xa

    invoke-direct {v6, v7, v8, v10}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;)V

    new-instance v7, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v7, v0, v6}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v7}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v6

    new-instance v7, Lf/a/o/b/a/h;

    invoke-direct {v7, p0}, Lf/a/o/b/a/h;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    const-wide/16 v8, 0x2

    invoke-static/range {v1 .. v10}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private static createSlashCommand(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 4
    .param p0    # Lcom/discord/models/slashcommands/ModelApplication;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    const/4 v1, 0x5

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    const/16 v1, 0x2f

    invoke-static {v1}, Lf/e/c/a/a;->E(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-static {v1}, Lf/e/c/a/a;->E(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tagRegex:Ljava/util/regex/Pattern;

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    iput-object p1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->command:Lcom/discord/stores/ModelApplicationCommand;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object p0

    const/16 p1, 0xa0

    invoke-static {v1, v2, p0, p1}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon(JLjava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->icon:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method private static createSlashCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p0

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tagRegex:Ljava/util/regex/Pattern;

    iput-object p1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashOutput:Ljava/lang/String;

    if-eqz p2, :cond_0

    move-object p1, p2

    :cond_0
    iput-object p1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashDisplay:Ljava/lang/String;

    iput-object p3, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashAction:Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    const/4 p0, 0x5

    iput p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    return-object v0
.end method

.method public static createSlashCommandApplication(Lcom/discord/models/slashcommands/ModelApplication;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 4
    .param p0    # Lcom/discord/models/slashcommands/ModelApplication;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    const/4 v1, 0x7

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object p0

    const/16 v3, 0xa0

    invoke-static {v1, v2, p0, v3}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon(JLjava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->icon:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method private static createSlashCommands(Z)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;>;"
        }
    .end annotation

    if-nez p0, :cond_0

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    return-object v0

    :cond_0
    invoke-static {}, Lcom/discord/stores/StoreStream;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object p0

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationCommands;->observeGuildApplications()Lrx/Observable;

    move-result-object p0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationCommands;->observeQueryCommands()Lrx/Observable;

    move-result-object v0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationCommands;->observeDiscoverCommands()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lf/a/o/b/a/p;->a:Lf/a/o/b/a/p;

    invoke-static {p0, v0, v1, v2}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p0

    const-string v0, "observable"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private static createUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->user:Lcom/discord/models/domain/ModelUser;

    iput-object p1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->nick:Ljava/lang/String;

    iput-object p2, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->presence:Lcom/discord/models/domain/ModelPresence;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->getMention()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    goto :goto_1

    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelUser;->getMention()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0xa

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    :goto_1
    return-object v0
.end method

.method public static synthetic f(Ljava/lang/String;Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)Ljava/lang/String;
    .locals 2

    iget-object v0, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x20

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p0, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashOutput:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic g(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyChannel()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, p2}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createChannel(Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v1, "2020-11_android_app_slash_commands"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {}, Lcom/discord/stores/StoreStream;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/o/b/a/m;->d:Lf/a/o/b/a/m;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/b/a/o;

    invoke-direct {v1, p0, v2}, Lf/a/o/b/a/o;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic h(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Collection;
    .locals 12

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    invoke-static/range {v1 .. v11}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createCommandsForMentions(JLjava/util/Map;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic i(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/Collection;
    .locals 1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createCommandsForSlash(Ljava/util/List;Ljava/util/List;)Ljava/util/Collection;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createCommandsForSlash(Ljava/util/List;Ljava/util/List;)Ljava/util/Collection;

    move-result-object p0

    return-object p0
.end method

.method private isSlashCommand()Z
    .locals 2

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public static synthetic j(Landroid/content/Context;ZLcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 5

    invoke-static {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createMentionCommands(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createChannelCommands(Landroid/content/Context;J)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createEmojiCommands(JJ)Lrx/Observable;

    move-result-object p2

    invoke-static {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createSlashCommands(Z)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/o/b/a/g;

    invoke-direct {v2, p1}, Lf/a/o/b/a/g;-><init>(Z)V

    invoke-static {v0, p0, p2, v1, v2}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic k(ZLjava/util/Collection;Ljava/util/List;Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, p3}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    if-eqz p0, :cond_0

    invoke-virtual {v0, p4}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createBuiltInSlashCommands()Ljava/util/Collection;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    :goto_0
    return-object v0
.end method

.method public static synthetic l(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Collection;
    .locals 0

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getNicks()Ljava/util/Map;

    move-result-object p0

    invoke-static {p1, p2, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->createCommandsForMentions(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Collection;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic m(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)I
    .locals 2

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    iget v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    sub-int/2addr p1, p0

    return p1
.end method

.method public static replaceMatches(Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/domain/ModelMessage$Content;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;)",
            "Lcom/discord/models/domain/ModelMessage$Content;"
        }
    .end annotation

    sget-object v0, Lf/a/o/b/a/k;->d:Lf/a/o/b/a/k;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getDisplayTag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    if-eqz v3, :cond_3

    iget-object v2, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tagRegex:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    invoke-direct {v3, p0}, Ljava/lang/StringBuffer;-><init>(I)V

    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    if-eqz p0, :cond_2

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    const/4 v4, 0x2

    :goto_2
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    invoke-interface {v4}, Lcom/discord/models/domain/emoji/Emoji;->getMessageContentReplacement()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_3
    iget-object v3, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->channel:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    const/16 v4, 0x3e

    if-eqz v3, :cond_4

    const-string v3, "<#"

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->channel:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;->getId()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_4
    iget-object v3, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->user:Lcom/discord/models/domain/ModelUser;

    if-eqz v3, :cond_6

    const-string v3, "<@"

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_5
    iget-object v1, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->user:Lcom/discord/models/domain/ModelUser;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_6
    iget-object v3, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->role:Lcom/discord/models/domain/ModelGuildRole;

    if-eqz v3, :cond_7

    const-string v3, "<@&"

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->role:Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    :cond_7
    iget-object v2, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashOutput:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-ne v2, v3, :cond_0

    iget-object v2, v1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashAction:Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    if-eqz v2, :cond_0

    invoke-interface {v2, p0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;->call(Ljava/lang/String;Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    :cond_8
    new-instance p1, Lcom/discord/models/domain/ModelMessage$Content;

    invoke-direct {p1, p0, v0}, Lcom/discord/models/domain/ModelMessage$Content;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1
.end method


# virtual methods
.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    return p1
.end method

.method public compareTo(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)I
    .locals 2
    .param p1    # Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->isSlashCommand()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->isSlashCommand()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->compareSlashCommand(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)I

    move-result p1

    return p1

    :cond_0
    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    iget v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->compareTo(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    if-nez v1, :cond_3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelUser;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    return v2

    :cond_4
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v3

    if-nez v1, :cond_5

    if-eqz v3, :cond_6

    goto :goto_1

    :cond_5
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelPresence;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_1
    return v2

    :cond_6
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getChannel()Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getChannel()Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    move-result-object v3

    if-nez v1, :cond_7

    if-eqz v3, :cond_8

    goto :goto_2

    :cond_7
    invoke-virtual {v1, v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    :goto_2
    return v2

    :cond_8
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v3

    if-nez v1, :cond_9

    if-eqz v3, :cond_a

    goto :goto_3

    :cond_9
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelGuildRole;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    :goto_3
    return v2

    :cond_a
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v3

    if-nez v1, :cond_b

    if-eqz v3, :cond_c

    goto :goto_4

    :cond_b
    invoke-virtual {v1, v3}, Lcom/discord/models/slashcommands/ModelApplication;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    :goto_4
    return v2

    :cond_c
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v3

    if-nez v1, :cond_d

    if-eqz v3, :cond_e

    goto :goto_5

    :cond_d
    invoke-virtual {v1, v3}, Lcom/discord/stores/ModelApplicationCommand;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    :goto_5
    return v2

    :cond_e
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object v3

    if-nez v1, :cond_f

    if-eqz v3, :cond_10

    goto :goto_6

    :cond_f
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    :goto_6
    return v2

    :cond_10
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getNick()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getNick()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_11

    if-eqz v3, :cond_12

    goto :goto_7

    :cond_11
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    :goto_7
    return v2

    :cond_12
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashOutput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashOutput()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_13

    if-eqz v3, :cond_14

    goto :goto_8

    :cond_13
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    :goto_8
    return v2

    :cond_14
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashDisplay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashDisplay()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_15

    if-eqz v3, :cond_16

    goto :goto_9

    :cond_15
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    :goto_9
    return v2

    :cond_16
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashAction()Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashAction()Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    move-result-object v3

    if-nez v1, :cond_17

    if-eqz v3, :cond_18

    goto :goto_a

    :cond_17
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    :goto_a
    return v2

    :cond_18
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTag()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_19

    if-eqz v3, :cond_1a

    goto :goto_b

    :cond_19
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    :goto_b
    return v2

    :cond_1a
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTagRegex()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTagRegex()Ljava/util/regex/Pattern;

    move-result-object v3

    if-nez v1, :cond_1b

    if-eqz v3, :cond_1c

    goto :goto_c

    :cond_1b
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    :goto_c
    return v2

    :cond_1c
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getIcon()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_1d

    if-eqz v3, :cond_1e

    goto :goto_d

    :cond_1d
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    :goto_d
    return v2

    :cond_1e
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result p1

    if-eq v1, p1, :cond_1f

    return v2

    :cond_1f
    return v0
.end method

.method public getApplication()Lcom/discord/models/slashcommands/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->application:Lcom/discord/models/slashcommands/ModelApplication;

    return-object v0
.end method

.method public getChannel()Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->channel:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    return-object v0
.end method

.method public getCommand()Lcom/discord/stores/ModelApplicationCommand;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->command:Lcom/discord/stores/ModelApplicationCommand;

    return-object v0
.end method

.method public getDisplayTag()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getNick()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public getEmoji()Lcom/discord/models/domain/emoji/Emoji;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNick()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->nick:Ljava/lang/String;

    return-object v0
.end method

.method public getPresence()Lcom/discord/models/domain/ModelPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->presence:Lcom/discord/models/domain/ModelPresence;

    return-object v0
.end method

.method public getRole()Lcom/discord/models/domain/ModelGuildRole;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->role:Lcom/discord/models/domain/ModelGuildRole;

    return-object v0
.end method

.method public getSlashAction()Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashAction:Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    return-object v0
.end method

.method public getSlashDisplay()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashDisplay:Ljava/lang/String;

    return-object v0
.end method

.method public getSlashOutput()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->slashOutput:Ljava/lang/String;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public getTagRegex()Ljava/util/regex/Pattern;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->tagRegex:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->type:I

    return v0
.end method

.method public getUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    const/16 v1, 0x2b

    if-nez v0, :cond_0

    const/16 v0, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    :goto_0
    const/16 v2, 0x3b

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_1

    const/16 v3, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPresence;->hashCode()I

    move-result v3

    :goto_1
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getChannel()Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_2

    const/16 v3, 0x2b

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;->hashCode()I

    move-result v3

    :goto_2
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_3

    const/16 v3, 0x2b

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->hashCode()I

    move-result v3

    :goto_3
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_4

    const/16 v3, 0x2b

    goto :goto_4

    :cond_4
    invoke-virtual {v3}, Lcom/discord/models/slashcommands/ModelApplication;->hashCode()I

    move-result v3

    :goto_4
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_5

    const/16 v3, 0x2b

    goto :goto_5

    :cond_5
    invoke-virtual {v3}, Lcom/discord/stores/ModelApplicationCommand;->hashCode()I

    move-result v3

    :goto_5
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_6

    const/16 v3, 0x2b

    goto :goto_6

    :cond_6
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_6
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getNick()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_7

    const/16 v3, 0x2b

    goto :goto_7

    :cond_7
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_7
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashOutput()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_8

    const/16 v3, 0x2b

    goto :goto_8

    :cond_8
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_8
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashDisplay()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_9

    const/16 v3, 0x2b

    goto :goto_9

    :cond_9
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_9
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashAction()Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_a

    const/16 v3, 0x2b

    goto :goto_a

    :cond_a
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_a
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTag()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_b

    const/16 v3, 0x2b

    goto :goto_b

    :cond_b
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_b
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTagRegex()Ljava/util/regex/Pattern;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_c

    const/16 v3, 0x2b

    goto :goto_c

    :cond_c
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_c
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getIcon()Ljava/lang/String;

    move-result-object v3

    mul-int/lit8 v0, v0, 0x3b

    if-nez v3, :cond_d

    goto :goto_d

    :cond_d
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3b

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "WidgetChatInputCommandsModel(user="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", presence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getChannel()Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel$Channel;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", command="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emoji="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nick="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getNick()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", slashOutput="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashOutput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", slashDisplay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashDisplay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", slashAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getSlashAction()Lcom/discord/widgets/chat/input/WidgetChatInputSlashAction;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tagRegex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getTagRegex()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
