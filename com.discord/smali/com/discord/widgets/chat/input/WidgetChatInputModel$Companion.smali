.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$computeVerificationLevelTriggered(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildMember$Computed;IJ)I
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->computeVerificationLevelTriggered(Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildMember$Computed;IJ)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getHint(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;ZZ)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->getHint(Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;ZZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$observePendingReplyState(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;J)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->observePendingReplyState(J)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final computeVerificationLevelTriggered(Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildMember$Computed;IJ)I
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lcom/discord/models/domain/ModelGuild;->isOwner(J)Z

    move-result p2

    if-ne p2, v1, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object p3

    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    xor-int/2addr p3, v1

    if-ne p3, v1, :cond_1

    const/4 p3, 0x1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Me;->hasPhone()Z

    move-result v2

    if-ne v2, v1, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    if-nez p2, :cond_9

    if-nez p3, :cond_9

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_3
    if-lt p4, v5, :cond_4

    const/4 v0, 0x4

    goto :goto_3

    :cond_4
    if-lt p4, v4, :cond_5

    invoke-static {p5, p6}, Lcom/discord/models/domain/ModelGuildMember;->isGuildMemberOldEnough(J)Z

    move-result p2

    if-nez p2, :cond_5

    const/4 v0, 0x3

    goto :goto_3

    :cond_5
    if-lt p4, v3, :cond_7

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->isAccountOldEnough()Z

    move-result p2

    if-eq p2, v1, :cond_7

    :cond_6
    const/4 v0, 0x2

    goto :goto_3

    :cond_7
    if-lt p4, v1, :cond_9

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Me;->isVerified()Z

    move-result p1

    if-eq p1, v1, :cond_9

    :cond_8
    const/4 v0, 0x1

    :cond_9
    :goto_3
    return v0
.end method

.method private final getHint(Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;ZZ)Ljava/lang/String;
    .locals 7

    if-eqz p3, :cond_0

    const p2, 0x7f120644

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026erification_text_blocked)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-nez p4, :cond_1

    const p2, 0x7f12111a

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026s_permission_placeholder)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const p3, 0x7f121823

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    const/4 v0, 0x0

    sget-object v1, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils$Companion;Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, v0

    invoke-virtual {p1, p3, p4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026.getDisplayName(context))"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final observePendingReplyState(J)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPendingReplies()Lcom/discord/stores/StorePendingReplies;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StorePendingReplies;->observePendingReply(J)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;

    invoke-virtual {p1, p2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "StoreStream\n            \u2026          }\n            }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final get(Landroid/content/Context;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreUser;->observeMe(Z)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$1;

    invoke-static {v1, v0, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;

    invoke-direct {v1, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable.combineLatest\u2026anged()\n        }\n      }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getVerificationLevelTriggered(J)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeJoinedAt(J)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeVerificationLevel(J)Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x5dc

    invoke-direct {p2, v5, v6, v1}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;)V

    new-instance v1, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v1, p1, p2}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreUser;->observeMe(Z)Lrx/Observable;

    move-result-object v6

    sget-object v7, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;

    invoke-static/range {v2 .. v7}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable\n          .co\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
