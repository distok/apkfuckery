.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->get(Landroid/content/Context;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;->call(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;)Lrx/Observable;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel;",
            ">;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->getSelectedChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/discord/models/domain/ModelUser$Me;->EMPTY:Lcom/discord/models/domain/ModelUser$Me;

    const-string v3, "ModelUser.Me.EMPTY"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_1
    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreChat;->getEditingMessage()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isDM()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    const-string v6, "channel.dmRecipient"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    goto :goto_1

    :cond_2
    const-wide/16 v5, 0x0

    :goto_1
    invoke-virtual {v3, v5, v6}, Lcom/discord/stores/StoreUserRelationships;->observe(J)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v6

    sget-object v3, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v7

    const-string v8, "channel.guildId"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v3, v9, v10}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->getVerificationLevelTriggered(J)Lrx/Observable;

    move-result-object v7

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getLurking()Lcom/discord/stores/StoreLurking;

    move-result-object v9

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v10

    invoke-static {v10, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Lcom/discord/stores/StoreLurking;->isLurkingObs(J)Lrx/Observable;

    move-result-object v9

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v10

    invoke-virtual {v10}, Lcom/discord/stores/StoreChat;->getExternalMessageText()Lrx/Observable;

    move-result-object v10

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getSlowMode()Lcom/discord/stores/StoreSlowMode;

    move-result-object v11

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/discord/stores/StoreSlowMode;->getCooldownSecs(Ljava/lang/Long;)Lrx/Observable;

    move-result-object v11

    sget-object v12, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$1;

    invoke-virtual {v11, v12}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v11

    invoke-virtual {v11}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v11

    const-string v12, "StoreStream\n            \u2026  .distinctUntilChanged()"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v13

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lcom/discord/stores/StoreGuilds;->observeFromChannelId(J)Lrx/Observable;

    move-result-object v13

    invoke-virtual {v13}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v13

    invoke-static {v13, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v12, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x7

    const/16 v19, 0x0

    move-object v14, v12

    invoke-direct/range {v14 .. v19}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v12}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->observeCanShowStickerPickerNfx()Lrx/Observable;

    move-result-object v12

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v14

    invoke-static {v3, v14, v15}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->access$observePendingReplyState(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;J)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v3

    const-string v14, "observePendingReplyState\u2026d).distinctUntilChanged()"

    invoke-static {v3, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v2

    const/4 v14, 0x1

    invoke-virtual {v2, v14}, Lcom/discord/stores/StoreStickers;->observeNewStickerPacks(Z)Lrx/Observable;

    move-result-object v14

    sget-object v15, Lcom/discord/utilities/guilds/GuildGatingUtils;->INSTANCE:Lcom/discord/utilities/guilds/GuildGatingUtils;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0xe

    const/16 v22, 0x0

    invoke-static/range {v15 .. v22}, Lcom/discord/utilities/guilds/GuildGatingUtils;->observeShouldShowGuildGate$default(Lcom/discord/utilities/guilds/GuildGatingUtils;JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v15

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;

    move-object/from16 v8, p0

    invoke-direct {v2, v8, v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)V

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v13

    move-object v13, v3

    move-object/from16 v16, v2

    invoke-static/range {v4 .. v16}, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function12;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    const-wide/16 v2, 0x15e

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;)V

    new-instance v2, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v0, v1}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n         \u2026, TimeUnit.MILLISECONDS))"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    :goto_2
    return-object v1
.end method
