.class public final Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;
.super Ljava/lang/Object;
.source "WidgetChatInput.kt"

# interfaces
.implements Landroidx/core/view/OnApplyWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInput;->setWindowInsetsListeners(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $shouldApplyWindowInsets:Z

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;->$shouldApplyWindowInsets:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 2

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "insets"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;->$shouldApplyWindowInsets:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetBottom()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    new-instance v1, Landroidx/core/view/WindowInsetsCompat$Builder;

    invoke-direct {v1}, Landroidx/core/view/WindowInsetsCompat$Builder;-><init>()V

    invoke-static {v0, v0, v0, p1}, Landroidx/core/graphics/Insets;->of(IIII)Landroidx/core/graphics/Insets;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroidx/core/view/WindowInsetsCompat$Builder;->setSystemWindowInsets(Landroidx/core/graphics/Insets;)Landroidx/core/view/WindowInsetsCompat$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/core/view/WindowInsetsCompat$Builder;->build()Landroidx/core/view/WindowInsetsCompat;

    move-result-object p1

    const-string v0, "WindowInsetsCompat.Build\u2026        )\n      ).build()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatInputWrap$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatGuard$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatGatingGuard$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    invoke-virtual {p2}, Landroidx/core/view/WindowInsetsCompat;->consumeSystemWindowInsets()Landroidx/core/view/WindowInsetsCompat;

    move-result-object p1

    return-object p1
.end method
