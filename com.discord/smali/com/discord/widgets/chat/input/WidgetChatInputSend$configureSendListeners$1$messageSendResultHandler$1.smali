.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;
.super Lx/m/c/k;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke(Ljava/util/List;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/discord/utilities/messagesend/MessageResult;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $attachments:Ljava/util/ArrayList;

.field public final synthetic $hasImage:Z

.field public final synthetic $hasVideo:Z

.field public final synthetic $messageResendCompressedHandler:Lkotlin/jvm/functions/Function0;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;Ljava/util/ArrayList;ZZLkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$attachments:Ljava/util/ArrayList;

    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$hasImage:Z

    iput-boolean p4, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$hasVideo:Z

    iput-object p5, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$messageResendCompressedHandler:Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/messagesend/MessageResult;

    check-cast p2, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->invoke(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/domain/ModelGuild;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/domain/ModelGuild;)V
    .locals 3

    const-string v0, "messageResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    check-cast p1, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;

    invoke-virtual {p1}, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;->getError()Lcom/discord/utilities/error/Error;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iget-object v1, v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;

    invoke-direct {v2, p0, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;Lcom/discord/models/domain/ModelGuild;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/utilities/rest/SendUtils;->handleSendError(Lcom/discord/utilities/error/Error;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method
