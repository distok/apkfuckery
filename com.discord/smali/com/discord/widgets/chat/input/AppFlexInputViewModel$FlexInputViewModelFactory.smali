.class public final Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;
.super Ljava/lang/Object;
.source "AppFlexInputViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/AppFlexInputViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FlexInputViewModelFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final observeStores()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1;->INSTANCE:Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n          .g\u2026            }\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    new-instance v10, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;-><init>(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;->observeStores()Lrx/Observable;

    move-result-object v3

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, v10

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;-><init>(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method
