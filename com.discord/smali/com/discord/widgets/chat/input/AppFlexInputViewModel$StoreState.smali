.class public final Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;
.super Ljava/lang/Object;
.source "AppFlexInputViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/AppFlexInputViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final channelPermission:Ljava/lang/Long;

.field private final leftPanelState:Lcom/discord/panels/PanelState;

.field private final notice:Lcom/discord/stores/StoreNotices$Notice;

.field private final rightPanelState:Lcom/discord/panels/PanelState;

.field private final selectedChannel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)V
    .locals 1

    const-string v0, "leftPanelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rightPanelState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->copy(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/panels/PanelState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    return-object v0
.end method

.method public final component2()Lcom/discord/panels/PanelState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component4()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    return-object v0
.end method

.method public final component5()Lcom/discord/stores/StoreNotices$Notice;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    return-object v0
.end method

.method public final copy(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;
    .locals 7

    const-string v0, "leftPanelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rightPanelState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;-><init>(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelPermission()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    return-object v0
.end method

.method public final getLeftPanelState()Lcom/discord/panels/PanelState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    return-object v0
.end method

.method public final getNotice()Lcom/discord/stores/StoreNotices$Notice;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    return-object v0
.end method

.method public final getRightPanelState()Lcom/discord/panels/PanelState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    return-object v0
.end method

.method public final getSelectedChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(leftPanelState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rightPanelState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->rightPanelState:Lcom/discord/panels/PanelState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelPermission="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->channelPermission:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", notice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->notice:Lcom/discord/stores/StoreNotices$Notice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
