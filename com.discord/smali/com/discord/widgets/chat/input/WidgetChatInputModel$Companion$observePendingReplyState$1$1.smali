.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1;->call(Lcom/discord/stores/StorePendingReplies$PendingReply;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePendingReplies$PendingReply;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;
    .locals 7

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Hide;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Hide;

    goto :goto_0

    :cond_0
    new-instance v6, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    invoke-virtual {v0}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    invoke-virtual {v0}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getShouldMention()Z

    move-result v2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    invoke-virtual {v0}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getShowMentionToggle()Z

    move-result v3

    move-object v0, v6

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;-><init>(Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)V

    move-object p1, v6

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$observePendingReplyState$1$1;->call(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    move-result-object p1

    return-object p1
.end method
