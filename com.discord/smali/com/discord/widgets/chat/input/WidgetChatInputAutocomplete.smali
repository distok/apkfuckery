.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;
.super Ljava/lang/Object;
.source "WidgetChatInputAutocomplete.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;


# instance fields
.field private final applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

.field private commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

.field private currentInput:Lcom/discord/widgets/chat/input/InputModel;

.field private final editText:Lcom/lytefast/flexinput/widget/FlexEditText;

.field private final inputMentionModelSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/chat/input/ChatInputCommandsModel;",
            ">;"
        }
    .end annotation
.end field

.field private lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

.field private lastToken:Ljava/lang/String;

.field private mentions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation
.end field

.field private mentionsAdapter:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

.field private queryCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/lytefast/flexinput/widget/FlexEditText;Landroidx/recyclerview/widget/RecyclerView;Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 8

    const-string v0, "editText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mentionsRecycler"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commandsRoot"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance v0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-direct {v0, p1, p3}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;-><init>(Lcom/lytefast/flexinput/widget/FlexEditText;Landroidx/constraintlayout/widget/ConstraintLayout;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->queryCommands:Ljava/util/List;

    new-instance p3, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p3

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;-><init>(Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentions:Ljava/util/List;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->inputMentionModelSubject:Lrx/subjects/BehaviorSubject;

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object p1

    const-string p3, "2020-11_android_app_slash_commands"

    const/4 v0, 0x1

    invoke-virtual {p1, p3, v0}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result p1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance p3, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)V

    invoke-direct {p3, p2, v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;ZLrx/functions/Action1;)V

    invoke-virtual {p1, p3}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentionsAdapter:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    return-void
.end method

.method public static final synthetic access$getApplicationCommands$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    return-object p0
.end method

.method public static final synthetic access$getMentionsAdapter$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentionsAdapter:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    return-object p0
.end method

.method public static final synthetic access$onClickMentionItem(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onClickMentionItem(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)V

    return-void
.end method

.method public static final synthetic access$onGuildCommandsUpdated(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onGuildCommandsUpdated(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$onMentionsDataUpdated(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onMentionsDataUpdated(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$setMentionsAdapter$p(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentionsAdapter:Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;

    return-void
.end method

.method private final applySpansToInput(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V
    .locals 7

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;->getInput()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    sget-object v2, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;

    const-string v3, "editable"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;->access$removeSpans(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$Companion;Landroid/text/Spannable;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getInputRanges()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/stores/ModelApplicationCommandOption;

    invoke-virtual {p3}, Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;->getShowOptionErrorSet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getInputRanges()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getParam()Lkotlin/ranges/IntRange;

    move-result-object v6

    iget v6, v6, Lkotlin/ranges/IntProgression;->d:I

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getParam()Lkotlin/ranges/IntRange;

    move-result-object v3

    iget v3, v3, Lkotlin/ranges/IntProgression;->e:I

    invoke-virtual {v5, v0, v6, v3, v4}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->applyParamSpan(Landroid/text/Spannable;IIZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;->getMentions()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {p3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result p3

    const/4 v2, 0x5

    if-eq p3, v2, :cond_3

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lkotlin/ranges/IntRange;

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iget v3, p3, Lkotlin/ranges/IntProgression;->d:I

    iget p3, p3, Lkotlin/ranges/IntProgression;->e:I

    const/16 v4, 0x21

    invoke-interface {v0, v2, v3, p3, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method private final onClickMentionItem(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)V
    .locals 9

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v0

    const-string v1, " "

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v2

    const-string/jumbo v3, "tag.command"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->selectApplicationCommand(Lcom/discord/stores/ModelApplicationCommand;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x2f

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/ModelApplicationCommand;->getName()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommand()Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_1
    move-object v4, v2

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getSelectedCommandOption()Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v0

    move-object v5, v0

    goto :goto_1

    :cond_2
    move-object v5, v2

    :goto_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->getOptionValues()Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;->getInputRanges()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;

    :cond_3
    if-eqz v5, :cond_4

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v3

    iget v3, v3, Lkotlin/ranges/IntProgression;->d:I

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/OptionRange;->getValue()Lkotlin/ranges/IntRange;

    move-result-object v2

    iget v2, v2, Lkotlin/ranges/IntProgression;->e:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getDisplayTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v2, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getDisplayTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tag.displayTag"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;->insertMention(Lcom/lytefast/flexinput/widget/FlexEditText;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    iget-object v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->showNextOption$default(Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;ZILjava/lang/Object;)V

    :cond_5
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object p1

    const-string/jumbo v1, "tag.emoji"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmoji;->onEmojiUsed(Lcom/discord/models/domain/emoji/Emoji;)V

    :cond_6
    return-void
.end method

.method private final onGuildCommandsUpdated(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->queryCommands:Ljava/util/List;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-virtual {v1, v0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->getCommandContext(Lcom/discord/widgets/chat/input/InputModel;Ljava/util/List;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onModelUpdated()V

    :cond_0
    return-void
.end method

.method private final onInputChanged(Lcom/discord/widgets/chat/input/InputModel;)V
    .locals 2

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->queryCommands:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->getCommandContext(Lcom/discord/widgets/chat/input/InputModel;Ljava/util/List;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onModelUpdated()V

    return-void
.end method

.method private final onMentionsDataUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentions:Ljava/util/List;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onModelUpdated()V

    return-void
.end method

.method private final onModelUpdated()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    if-eqz v2, :cond_0

    sget-object v1, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentions:Ljava/util/List;

    iget-object v5, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-virtual {v5}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->isCommand()Z

    move-result v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->mapInputToMentions(Ljava/lang/String;Ljava/util/Collection;Z)Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    move-result-object v9

    iget-object v3, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-virtual {v1, v3, v9}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->verifyCommandInput(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;)Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;

    move-result-object v8

    new-instance v7, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    iget-object v10, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0xf

    const/16 v17, 0x0

    move-object v15, v8

    invoke-static/range {v10 .. v17}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->copy$default(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/models/slashcommands/ModelApplication;Lcom/discord/stores/ModelApplicationCommand;Lcom/discord/stores/ModelApplicationCommandOption;Lcom/discord/widgets/chat/input/applicationcommands/CommandOptionValues;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    move-result-object v3

    iget-object v5, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentions:Ljava/util/List;

    const/4 v6, 0x0

    const/16 v10, 0x10

    move-object v1, v7

    move-object v4, v9

    move-object v12, v7

    move v7, v10

    move-object v10, v8

    move-object v8, v11

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-direct {v0, v1, v9, v10}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applySpansToInput(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Lcom/discord/widgets/chat/input/applicationcommands/CommandInputVerification;)V

    iput-object v12, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    iget-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->inputMentionModelSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1, v12}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppFragment;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emptyTextSubject"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->applicationCommands:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputApplicationCommands;->configureObservables(Lcom/discord/app/AppFragment;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationCommands;->observeQueryCommands()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    new-instance v9, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;->observeIsExpressionTrayActive()Lrx/Observable;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$2;

    invoke-static {v0, p2, v3}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p2

    invoke-virtual {p2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p2

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->get(Landroid/content/Context;)Lrx/Observable;

    move-result-object v3

    const-string v4, "WidgetChatInputCommandsModel.get(editText.context)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "defaultValue"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lf/a/b/f0;->d:Lf/a/b/f0;

    new-instance v5, Lf/a/b/g0;

    invoke-direct {v5, v3}, Lf/a/b/g0;-><init>(Lrx/Observable;)V

    invoke-static {v4, v0, v5}, Lf/a/b/r;->o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p2

    const-string v0, "Observable\n        .comb\u2026)\n            )\n        )"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$3;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$3;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->inputMentionModelSubject:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;

    invoke-virtual {p2, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$5;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$5;

    invoke-virtual {p2, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    const-string v0, "inputMentionModelSubject\u2026utocompleteToken(model) }"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    invoke-static {p2, p1, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    new-instance v9, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$6;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)V

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->inputMentionModelSubject:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$7;

    invoke-virtual {p2, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    const-string v0, "inputMentionModelSubject\u2026utoCompleteToken?.token }"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreGuilds$Actions;->requestMembers(Lcom/discord/app/AppComponent;Lrx/Observable;Z)V

    return-void
.end method

.method public final getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    return-object v0
.end method

.method public final getCurrentInput()Lcom/discord/widgets/chat/input/InputModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    return-object v0
.end method

.method public final getEditText()Lcom/lytefast/flexinput/widget/FlexEditText;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    return-object v0
.end method

.method public final getLastChatInputModel()Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    return-object v0
.end method

.method public final getLastToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getMentions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentions:Ljava/util/List;

    return-object v0
.end method

.method public final getQueryCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->queryCommands:Ljava/util/List;

    return-object v0
.end method

.method public final onSelectionChanged(II)V
    .locals 3

    new-instance v0, Lcom/discord/widgets/chat/input/InputModel;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/InputModel;->getContent()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    new-instance v2, Lkotlin/ranges/IntRange;

    invoke-direct {v2, p1, p2}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/chat/input/InputModel;-><init>(Ljava/lang/String;Lkotlin/ranges/IntRange;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onInputChanged(Lcom/discord/widgets/chat/input/InputModel;)V

    return-void
.end method

.method public final onTextChanged(Ljava/lang/String;)V
    .locals 4

    const-string v0, "content"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/InputModel;->getSelected()Lkotlin/ranges/IntRange;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/ranges/IntRange;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    :goto_0
    iget v1, v0, Lkotlin/ranges/IntProgression;->d:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v0, v0, Lkotlin/ranges/IntProgression;->e:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v2, Lcom/discord/widgets/chat/input/InputModel;

    new-instance v3, Lkotlin/ranges/IntRange;

    invoke-direct {v3, v1, v0}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-direct {v2, p1, v3}, Lcom/discord/widgets/chat/input/InputModel;-><init>(Ljava/lang/String;Lkotlin/ranges/IntRange;)V

    invoke-direct {p0, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onInputChanged(Lcom/discord/widgets/chat/input/InputModel;)V

    return-void
.end method

.method public final replaceMatches(Ljava/lang/String;)Lcom/discord/models/domain/ModelMessage$Content;
    .locals 1

    const-string v0, "content"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->getInputMentionsMap()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;->getMentions()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    :goto_1
    invoke-static {p1, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->replaceMatches(Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/domain/ModelMessage$Content;

    move-result-object p1

    const-string v0, "WidgetChatInputCommandsM\u2026.toList().orEmpty()\n    )"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final setCommandContext(Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    return-void
.end method

.method public final setCurrentInput(Lcom/discord/widgets/chat/input/InputModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->currentInput:Lcom/discord/widgets/chat/input/InputModel;

    return-void
.end method

.method public final setLastChatInputModel(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastChatInputModel:Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    return-void
.end method

.method public final setLastToken(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->lastToken:Ljava/lang/String;

    return-void
.end method

.method public final setMentions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->mentions:Ljava/util/List;

    return-void
.end method

.method public final setQueryCommands(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->queryCommands:Ljava/util/List;

    return-void
.end method
