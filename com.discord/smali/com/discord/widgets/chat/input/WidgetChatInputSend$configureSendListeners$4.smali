.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;
.super Ljava/lang/Object;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lcom/lytefast/flexinput/InputListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureSendListeners(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

.field public final synthetic $sendMessage$1:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

.field public final synthetic $sendMessageError$2:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$sendMessageError$2:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$sendMessage$1:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSend(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;)Z"
        }
    .end annotation

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isOnCooldown()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$sendMessageError$2:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;

    const p2, 0x7f12046e

    invoke-virtual {p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;->invoke(I)Z

    move-result p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result p1

    const-string v0, "list"

    if-eqz p1, :cond_1

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$sendMessageError$2:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;

    const p2, 0x7f120666

    invoke-virtual {p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;->invoke(I)Z

    move-result p1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getAbleToSendMessage()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$sendMessage$1:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke$default(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;Ljava/util/List;ZILjava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;->$sendMessageError$2:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;

    const p2, 0x7f12111a

    invoke-virtual {p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;->invoke(I)Z

    move-result p1

    :goto_0
    return p1
.end method
