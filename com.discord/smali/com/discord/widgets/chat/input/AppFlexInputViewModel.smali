.class public final Lcom/discord/widgets/chat/input/AppFlexInputViewModel;
.super Lf/a/b/l0;
.source "AppFlexInputViewModel.kt"

# interfaces
.implements Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/lytefast/flexinput/viewmodel/FlexInputState;",
        ">;",
        "Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;"
    }
.end annotation


# instance fields
.field private eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lf/b/a/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lytefast/flexinput/viewmodel/FlexInputState;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreAnalytics;",
            ")V"
        }
    .end annotation

    const-string v0, "initialViewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeObservable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAnalytics"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string/jumbo p2, "storeObservable\n        \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    new-instance v6, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$1;-><init>(Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;-><init>(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;)V
    .locals 11

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getSelectedChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getChannelPermission()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getLeftPanelState()Lcom/discord/panels/PanelState;

    move-result-object v3

    sget-object v4, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    invoke-static {v3, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const/4 v5, 0x1

    xor-int/2addr v3, v5

    const/4 v6, 0x0

    if-nez v3, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getRightPanelState()Lcom/discord/panels/PanelState;

    move-result-object v3

    invoke-static {v3, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v5

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getNotice()Lcom/discord/stores/StoreNotices$Notice;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getNotice()Lcom/discord/stores/StoreNotices$Notice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreNotices$Notice;->isInAppNotification()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;->getNotice()Lcom/discord/stores/StoreNotices$Notice;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreNotices$Notice;->isPopup()Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    if-eqz v0, :cond_3

    invoke-static {v0, v2}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccessWrite(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;)Z

    move-result v4

    move v7, v4

    goto :goto_3

    :cond_3
    const/4 v7, 0x0

    :goto_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-nez v0, :cond_4

    const-wide/32 v8, 0x8000

    invoke-static {v8, v9, v2}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    :goto_4
    iget-boolean v2, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    if-nez p1, :cond_7

    if-eqz v3, :cond_6

    goto :goto_5

    :cond_6
    move v8, v2

    goto :goto_6

    :cond_7
    :goto_5
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    const/4 v8, 0x0

    :goto_6
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x8f

    move v6, v7

    move v7, v0

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final showKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lf/b/a/f/a$b;->a:Lf/b/a/f/a$b;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final clean()V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    if-eqz v1, :cond_0

    sget-object v4, Lx/h/l;->d:Lx/h/l;

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf0

    const-string v2, ""

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public hideExpressionTray()Z
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    if-nez v2, :cond_0

    return v0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xbf

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final hideKeyboard()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lf/b/a/f/a$a;->a:Lf/b/a/f/a$a;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public observeEvents()Lrx/Observable;
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lf/b/a/f/a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public observeState()Lrx/Observable;
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/lytefast/flexinput/viewmodel/FlexInputState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onAttachmentsUpdated(Ljava/util/List;)V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "+",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "attachments"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfb

    move-object v4, p1

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public onCameraButtonClicked()V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    if-eqz v1, :cond_1

    iget-boolean v0, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lf/b/a/f/a$d;

    const v2, 0x7f1203f2

    invoke-direct {v1, v2}, Lf/b/a/f/a$d;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xb7

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public onContentDialogDismissed()V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboard()V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf7

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public onContentDialogPageChanged(I)V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf7

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    const-string v0, "camera"

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAnalytics;->trackChatInputComponentViewed(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    const-string v0, "files"

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAnalytics;->trackChatInputComponentViewed(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    const-string v0, "media picker"

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAnalytics;->trackChatInputComponentViewed(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onExpandButtonClicked()V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfd

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public onExpressionTrayButtonClicked()V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    iget-boolean v0, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboard()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-boolean v0, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    xor-int/lit8 v8, v0, 0x1

    const/4 v9, 0x0

    const/16 v10, 0xbf

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public onFlexInputFragmentPause()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    return-void
.end method

.method public onGalleryButtonClicked()V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    iget-boolean v0, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lf/b/a/f/a$d;

    const v2, 0x7f1203f2

    invoke-direct {v1, v2}, Lf/b/a/f/a$d;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xb7

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public onInputTextAppended(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "appendText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    iget-object v1, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0, v1}, Lf/h/a/f/f/n/g;->U(Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    return-void
.end method

.method public onInputTextChanged(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "inputText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    iget-object v0, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboard()V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    :cond_2
    :goto_0
    iget-boolean p2, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-lez p2, :cond_3

    const/4 p2, 0x1

    goto :goto_1

    :cond_3
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_4

    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    iget-boolean p2, v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    move v3, p2

    :goto_2
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfc

    move-object v2, p1

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public onInputTextClicked()Z
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboard()V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xbf

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    const/4 v0, 0x0

    return v0
.end method

.method public onSendButtonClicked(Lcom/lytefast/flexinput/InputListener;)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v1, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    invoke-interface {p1, v1, v0}, Lcom/lytefast/flexinput/InputListener;->onSend(Ljava/lang/String;Ljava/util/List;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->clean()V

    :cond_1
    return-void
.end method

.method public onShowDialog()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideKeyboard()V

    return-void
.end method

.method public onToolTipButtonLongPressed(Landroid/view/View;)Z
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "button"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lf/b/a/f/a$c;

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lf/b/a/f/a$c;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1
.end method

.method public setShowExpressionTrayButtonBadge(Z)V
    .locals 11

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x7f

    move v9, p1

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public final showKeyboardAndHideExpressionTray()V
    .locals 11

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xbf

    invoke-static/range {v1 .. v10}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboard()V

    return-void
.end method
