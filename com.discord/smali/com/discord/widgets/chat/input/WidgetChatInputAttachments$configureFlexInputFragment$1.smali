.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;
.super Lx/m/c/k;
.source "WidgetChatInputAttachments.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->configureFlexInputFragment(Lcom/discord/app/AppFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $fragment:Lcom/discord/app/AppFragment;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/discord/app/AppFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->$fragment:Lcom/discord/app/AppFragment;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->access$getFlexInputFragment$p(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;)Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Lf/b/a/c/d$a;

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$1$1;

    const v3, 0x7f08035c

    const v4, 0x7f120262

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$1$1;-><init>(II)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$1$2;

    const v4, 0x7f080309

    const v5, 0x7f120261

    invoke-direct {v2, v4, v5}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$1$2;-><init>(II)V

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$1$3;

    const v4, 0x7f080243

    const v5, 0x7f1203c8

    invoke-direct {v2, v4, v5}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$1$3;-><init>(II)V

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "pageSuppliers"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->y:[Lf/b/a/c/d$a;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->$fragment:Lcom/discord/app/AppFragment;

    invoke-virtual {v1}, Lcom/discord/app/AppFragment;->getFileManager()Lcom/lytefast/flexinput/managers/FileManager;

    move-result-object v1

    const-string v2, "<set-?>"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->w:Lcom/lytefast/flexinput/managers/FileManager;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$$special$$inlined$apply$lambda$1;

    invoke-direct {v1, v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$$special$$inlined$apply$lambda$1;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;)V

    iput-object v1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->u:Lf/b/a/d/a;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->l()Lcom/lytefast/flexinput/widget/FlexEditText;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$$special$$inlined$apply$lambda$2;

    invoke-direct {v2, v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1$$special$$inlined$apply$lambda$2;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;)V

    invoke-virtual {v1, v2}, Lcom/lytefast/flexinput/widget/FlexEditText;->setInputContentHandler(Lkotlin/jvm/functions/Function1;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->$fragment:Lcom/discord/app/AppFragment;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->access$createPreviewAdapter(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Landroid/content/Context;)Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    move-result-object v1

    const-string v2, "previewAdapter"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v1, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;->a:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->initFrom(Lcom/lytefast/flexinput/utils/SelectionAggregator;)Lcom/lytefast/flexinput/utils/SelectionAggregator;

    iput-object v1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->x:Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    iget-object v2, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$configureFlexInputFragment$1;->$fragment:Lcom/discord/app/AppFragment;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v5, "fragment.childFragmentManager"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->l()Lcom/lytefast/flexinput/widget/FlexEditText;

    move-result-object v5

    invoke-static {v1, v2, v5}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->access$createAndConfigureExpressionFragment(Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Landroidx/fragment/app/FragmentManager;Landroid/widget/TextView;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v2

    sget v5, Lcom/lytefast/flexinput/R$e;->flex_input_expression_tray_container:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v1, v6}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "expressionBtn"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_2
    const-string v0, "attachmentPreviewList"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4
.end method
