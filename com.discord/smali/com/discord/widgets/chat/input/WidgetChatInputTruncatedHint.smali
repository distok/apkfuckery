.class public final Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;
.super Ljava/lang/Object;
.source "WidgetChatInputTruncatedHint.kt"


# instance fields
.field private final editText:Lcom/lytefast/flexinput/widget/FlexEditText;

.field private hint:Ljava/lang/CharSequence;

.field private hintIsTruncated:Z

.field private previousMaxLines:I


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/widget/FlexEditText;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getMaxLines()I

    move-result v0

    iput v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->previousMaxLines:I

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)V

    invoke-virtual {p1, v0}, Lcom/lytefast/flexinput/widget/FlexEditText;->setOnMaxLinesChangedListener(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$getHintIsTruncated$p(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->hintIsTruncated:Z

    return p0
.end method

.method public static final synthetic access$getPreviousMaxLines$p(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)I
    .locals 0

    iget p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->previousMaxLines:I

    return p0
.end method

.method public static final synthetic access$setHintIsTruncated$p(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->hintIsTruncated:Z

    return-void
.end method

.method public static final synthetic access$setPreviousMaxLines$p(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->previousMaxLines:I

    return-void
.end method

.method public static final synthetic access$syncHint(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->syncHint()V

    return-void
.end method

.method private final syncHint()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->hintIsTruncated:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/widget/FlexEditText;->setMaxLines(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->hint:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    iget v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->previousMaxLines:I

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/widget/FlexEditText;->setMaxLines(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public final addBindedTextWatcher(Lcom/discord/app/AppFragment;)V
    .locals 2

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$addBindedTextWatcher$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$addBindedTextWatcher$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)V

    invoke-static {v0, p1, v1}, Lcom/discord/utilities/view/text/TextWatcherKt;->addBindedTextWatcher(Landroid/widget/TextView;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final getEditText()Lcom/lytefast/flexinput/widget/FlexEditText;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->editText:Lcom/lytefast/flexinput/widget/FlexEditText;

    return-object v0
.end method

.method public final setHint(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "hint"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->hint:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->syncHint()V

    return-void
.end method
