.class public final Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;
.super Lcom/discord/utilities/view/text/TextWatcher;
.source "WidgetChatInputEditText.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setOnTextChangedListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field private empty:Z

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/view/text/TextWatcher;-><init>(Lkotlin/jvm/functions/Function4;Lkotlin/jvm/functions/Function4;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->empty:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    const-string v0, "s"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/utilities/view/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->saveText()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->access$getChatInputCommands$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->onTextChanged(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->empty:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->empty:Z

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->access$getEmptyTextSubject$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)Lrx/subjects/Subject;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->access$getLastTypingEmissionMillis$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)J

    move-result-wide v0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const/16 v2, -0x2710

    int-to-long v2, v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->access$setLastTypingEmissionMillis$p(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;J)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUsersTyping()Lcom/discord/stores/StoreUserTyping;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputEditText$setOnTextChangedListener$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->getChannelId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreUserTyping;->setUserTyping(J)V

    :cond_1
    return-void
.end method
