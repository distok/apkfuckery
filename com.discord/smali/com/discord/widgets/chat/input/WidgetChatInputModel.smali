.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;,
        Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;


# instance fields
.field private final ableToSendMessage:Z

.field private final canShowStickerPickerNfx:Z

.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelId:J

.field private final editingMessage:Lcom/discord/stores/StoreChat$EditingMessage;

.field private final externalText:Ljava/lang/String;

.field private final inputHint:Ljava/lang/String;

.field private final isGuildGatingEnabled:Z

.field private final isLurking:Z

.field private final isOnCooldown:Z

.field private final isSystemDM:Z

.field private final maxFileSizeMB:I

.field private final me:Lcom/discord/models/domain/ModelUser;

.field private final pendingReplyState:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

.field private final shouldBadgeChatInput:Z

.field private final shouldShowFollow:Z

.field private final verificationLevelTriggered:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;JLcom/discord/models/domain/ModelUser;Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/String;ZIZZLjava/lang/String;ZIZZLcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;ZZ)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p6

    move-object/from16 v3, p11

    move-object/from16 v4, p16

    const-string v5, "channel"

    invoke-static {p1, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "inputHint"

    invoke-static {p6, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "externalText"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "pendingReplyState"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->channel:Lcom/discord/models/domain/ModelChannel;

    move-wide v5, p2

    iput-wide v5, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->channelId:J

    move-object v1, p4

    iput-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->me:Lcom/discord/models/domain/ModelUser;

    move-object v1, p5

    iput-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->editingMessage:Lcom/discord/stores/StoreChat$EditingMessage;

    iput-object v2, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->inputHint:Ljava/lang/String;

    move v1, p7

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->ableToSendMessage:Z

    move v1, p8

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->verificationLevelTriggered:I

    move/from16 v1, p9

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isLurking:Z

    move/from16 v1, p10

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isSystemDM:Z

    iput-object v3, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->externalText:Ljava/lang/String;

    move/from16 v1, p12

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isOnCooldown:Z

    move/from16 v1, p13

    iput v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->maxFileSizeMB:I

    move/from16 v1, p14

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->shouldShowFollow:Z

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->canShowStickerPickerNfx:Z

    iput-object v4, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->pendingReplyState:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    move/from16 v1, p17

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->shouldBadgeChatInput:Z

    move/from16 v1, p18

    iput-boolean v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isGuildGatingEnabled:Z

    return-void
.end method


# virtual methods
.method public final getAbleToSendMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->ableToSendMessage:Z

    return v0
.end method

.method public final getCanShowStickerPickerNfx()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->canShowStickerPickerNfx:Z

    return v0
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->channelId:J

    return-wide v0
.end method

.method public final getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->editingMessage:Lcom/discord/stores/StoreChat$EditingMessage;

    return-object v0
.end method

.method public final getExternalText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->externalText:Ljava/lang/String;

    return-object v0
.end method

.method public final getInputHint()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->inputHint:Ljava/lang/String;

    return-object v0
.end method

.method public final getMaxFileSizeMB()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->maxFileSizeMB:I

    return v0
.end method

.method public final getMe()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->me:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getPendingReplyState()Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->pendingReplyState:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    return-object v0
.end method

.method public final getShouldBadgeChatInput()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->shouldBadgeChatInput:Z

    return v0
.end method

.method public final getShouldShowFollow()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->shouldShowFollow:Z

    return v0
.end method

.method public final getVerificationActionCallback()Landroid/view/View$OnClickListener;
    .locals 2

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->verificationLevelTriggered:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$getVerificationActionCallback$2;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$getVerificationActionCallback$2;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$getVerificationActionCallback$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$getVerificationActionCallback$1;

    :goto_0
    return-object v0
.end method

.method public final getVerificationActionText(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->verificationLevelTriggered:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const v0, 0x7f121a27

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const v0, 0x7f121a1e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getVerificationText(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->verificationLevelTriggered:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const v0, 0x7f120cbf

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const v0, 0x7f120cbd

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "10"

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    const v0, 0x7f120cbc

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "5"

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    const v0, 0x7f120cbe

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final isEditing()Z
    .locals 5

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->ableToSendMessage:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->editingMessage:Lcom/discord/stores/StoreChat$EditingMessage;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreChat$EditingMessage;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isGuildGatingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isGuildGatingEnabled:Z

    return v0
.end method

.method public final isInputShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isSystemDM:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isLurking:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isVerificationLevelTriggered()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->shouldShowFollow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isGuildGatingEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isLurking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isLurking:Z

    return v0
.end method

.method public final isOnCooldown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isOnCooldown:Z

    return v0
.end method

.method public final isReplying()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->pendingReplyState:Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    instance-of v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    return v0
.end method

.method public final isSystemDM()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isSystemDM:Z

    return v0
.end method

.method public final isVerificationLevelTriggered()Z
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->verificationLevelTriggered:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
