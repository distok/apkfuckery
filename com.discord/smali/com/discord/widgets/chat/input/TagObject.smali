.class public interface abstract Lcom/discord/widgets/chat/input/TagObject;
.super Ljava/lang/Object;
.source "WidgetChatInputEditText.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# virtual methods
.method public abstract getDisplayTag()Ljava/lang/String;
.end method

.method public abstract getTag()Ljava/lang/String;
.end method

.method public abstract getTagRegex()Ljava/util/regex/Pattern;
.end method
