.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;
.super Ljava/lang/Object;
.source "WidgetChatInputAutocomplete.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/chat/input/ChatInputCommandsModel;",
        "Lcom/discord/widgets/chat/input/ChatInputCommandsModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 2

    sget-object v0, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->INSTANCE:Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;

    const-string v1, "model"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/applicationcommands/WidgetChatInputModelMappingFunctions;->filterMentionsForCommandContext(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$4;->call(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    move-result-object p1

    return-object p1
.end method
