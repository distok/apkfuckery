.class public final Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;
.super Lx/m/c/k;
.source "WidgetChatInputTruncatedHint.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;-><init>(Lcom/lytefast/flexinput/widget/FlexEditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->access$getHintIsTruncated$p(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->access$setPreviousMaxLines$p(Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;I)V

    :cond_0
    return-void
.end method
