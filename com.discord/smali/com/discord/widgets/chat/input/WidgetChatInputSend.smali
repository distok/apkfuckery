.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend;
.super Ljava/lang/Object;
.source "WidgetChatInputSend.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputSend;

.field private static final chatStore:Lcom/discord/stores/StoreChat;

.field private static wasEditing:Z

.field private static wasReplying:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputSend;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->chatStore:Lcom/discord/stores/StoreChat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$clearInput(Lcom/discord/widgets/chat/input/WidgetChatInputSend;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Ljava/lang/Boolean;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->clearInput(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Ljava/lang/Boolean;)Z

    move-result p0

    return p0
.end method

.method private final clearInput(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Ljava/lang/Boolean;)Z
    .locals 1

    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {p2, v0, p3}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->onInputTextChanged(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->clean()V

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->clearLastTypingEmission()V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreChat;->setEditingMessage(Lcom/discord/stores/StoreChat$EditingMessage;)V

    const/4 p1, 0x1

    return p1
.end method

.method public static synthetic clearInput$default(Lcom/discord/widgets/chat/input/WidgetChatInputSend;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Ljava/lang/Boolean;ILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->clearInput(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Ljava/lang/Boolean;)Z

    move-result p0

    return p0
.end method

.method private final configureContextBarEditing(Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setClickable(Z)V

    const p3, 0x7f120665

    invoke-virtual {p4, p3}, Landroid/widget/TextView;->setText(I)V

    new-instance p3, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarEditing$1;

    invoke-direct {p3, p2, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarEditing$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V

    invoke-virtual {p5, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 p1, 0x8

    invoke-virtual {p6, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureContextBarReplying(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 15

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p6

    move-object/from16 v12, p8

    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getRepliedAuthorGuildMember()Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getRepliedAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const v1, 0x7f040153

    invoke-static {v8, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getRepliedAuthorGuildMember()Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getColor(Lcom/discord/models/domain/ModelGuildMember$Computed;I)I

    move-result v1

    const v2, 0x7f121071

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v13

    invoke-virtual {v8, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "context.getString(R.stri\u2026_replying_to, authorName)"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x38

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v14

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object/from16 v1, p4

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$1;

    invoke-direct {v0, v9}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;)V

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$2;

    invoke-direct {v0, v9}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;)V

    move-object/from16 v1, p5

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getShowMentionToggle()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 v13, 0x8

    :goto_1
    invoke-virtual {v11, v13}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$3;

    invoke-direct {v0, v9}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureContextBarReplying$3;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;)V

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getShouldMention()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f06004a

    invoke-static {v8, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    goto :goto_2

    :cond_2
    const v0, 0x7f040178

    invoke-static {v8, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    :goto_2
    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->getShouldMention()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f121518

    goto :goto_3

    :cond_3
    const v0, 0x7f121516

    :goto_3
    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public static final configureSendListeners(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V
    .locals 17

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v0, p4

    const-string v1, "context"

    invoke-static {v11, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "chatInputWidget"

    invoke-static {v13, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "model"

    invoke-static {v14, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "listener"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v12, :cond_1

    new-instance v15, Ljava/lang/ref/WeakReference;

    invoke-direct {v15, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v16, Lcom/discord/widgets/chat/MessageManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xfe

    const/4 v10, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/discord/widgets/chat/MessageManager;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    new-instance v7, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    move-object v0, v7

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object v4, v15

    move-object/from16 v5, v16

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;-><init>(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Ljava/lang/ref/WeakReference;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;

    invoke-direct {v0, v11}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getChannelId()J

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->getChannelId()J

    move-result-wide v3

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getChannelId()J

    move-result-wide v5

    cmp-long v8, v3, v5

    if-eqz v8, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v12, v1, v2, v3}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setChannelId(JZ)V

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$3;

    invoke-direct {v1, v7}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$3;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;)V

    invoke-virtual {v12, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->setOnSendListener(Lkotlin/jvm/functions/Function0;)V

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputSend;

    move-object/from16 v2, p5

    invoke-direct {v1, v14, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->setText(Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;)V

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;

    invoke-direct {v1, v14, v0, v7}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$4;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$2;Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;)V

    invoke-virtual {v13, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->setInputListener(Lcom/lytefast/flexinput/InputListener;)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result v0

    sput-boolean v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->wasEditing:Z

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isReplying()Z

    move-result v0

    sput-boolean v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->wasReplying:Z

    :cond_1
    return-void
.end method

.method private final setText(Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;)V
    .locals 8

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getAbleToSendMessage()Z

    move-result v1

    const-string v2, ""

    const/4 v3, 0x0

    if-nez v1, :cond_0

    if-eqz p2, :cond_9

    const/4 p1, 0x2

    invoke-static {p2, v2, v3, p1, v3}, Lf/h/a/f/f/n/g;->U(Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    goto/16 :goto_3

    :cond_0
    sget-boolean v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->wasEditing:Z

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->chatStore:Lcom/discord/stores/StoreChat;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/stores/StoreChat$EditingMessage;->copy$default(Lcom/discord/stores/StoreChat$EditingMessage;Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;ZILjava/lang/Object;)Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreChat;->setEditingMessage(Lcom/discord/stores/StoreChat$EditingMessage;)V

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreChat$EditingMessage;->getContent()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1, v0}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onInputTextChanged(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_3

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getExternalText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-lez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getExternalText()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onInputTextAppended(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    sget-boolean v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->wasEditing:Z

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result v1

    if-nez v1, :cond_6

    :cond_4
    sget-boolean v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->wasReplying:Z

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isReplying()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :cond_6
    :goto_1
    if-eqz p2, :cond_9

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->chatStore:Lcom/discord/stores/StoreChat;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getChannelId()J

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lcom/discord/stores/StoreChat;->getTextChannelInput(J)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    move-object v2, p1

    :cond_7
    if-eqz v4, :cond_8

    goto :goto_2

    :cond_8
    move-object v0, v3

    :goto_2
    invoke-interface {p2, v2, v0}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onInputTextChanged(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_9
    :goto_3
    return-void
.end method


# virtual methods
.method public final configureContextBar(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 9

    move-object v3, p5

    const-string v0, "context"

    move-object v1, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    move-object v2, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chatInputContextBar"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chatInputContextText"

    move-object v4, p6

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chatInputContextCancel"

    move-object/from16 v5, p7

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chatInputContextReplyMentionButton"

    move-object/from16 v6, p8

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chatInputContextReplyMentionButtonImage"

    move-object/from16 v7, p9

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chatInputContextReplyMentionButtonText"

    move-object/from16 v8, p10

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_2

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureContextBarEditing(Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getPendingReplyState()Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getPendingReplyState()Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p5

    move-object v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureContextBarReplying(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p5, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void
.end method
