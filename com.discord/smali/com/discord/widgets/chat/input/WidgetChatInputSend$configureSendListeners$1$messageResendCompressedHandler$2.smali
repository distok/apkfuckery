.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;
.super Lx/m/c/k;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke(Ljava/util/List;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $attachments:Ljava/util/ArrayList;

.field public final synthetic $compressedImages:Z

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;ZLjava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->$compressedImages:Z

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->$attachments:Ljava/util/ArrayList;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->$compressedImages:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iget-object v1, v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;->$attachments:Ljava/util/ArrayList;

    new-instance v3, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/utilities/rest/SendUtils;->compressImageAttachments(Landroid/content/Context;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    :cond_0
    return-void
.end method
