.class public final Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "ExpressionTrayViewModel.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Ljava/util/Set;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;

    const-string v1, "canShowStickerPickerNfx"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string v1, "lastSelectedTab"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result p3

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;-><init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;I)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    check-cast p2, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    check-cast p3, Ljava/util/Set;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;->call(Ljava/lang/Boolean;Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Ljava/util/Set;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
