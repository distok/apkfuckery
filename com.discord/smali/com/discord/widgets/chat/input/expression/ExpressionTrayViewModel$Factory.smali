.class public final Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;
.super Ljava/lang/Object;
.source "ExpressionTrayViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final stickerPickerNfxManager:Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

.field private final storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

.field private final storeStickers:Lcom/discord/stores/StoreStickers;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreStickers;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreStickers;)V
    .locals 1

    const-string/jumbo v0, "stickerPickerNfxManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeExpressionPickerNavigation"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStickers"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->stickerPickerNfxManager:Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreStickers;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getExpressionPickerNavigation()Lcom/discord/stores/StoreExpressionPickerNavigation;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object p3

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreStickers;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->stickerPickerNfxManager:Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->observeCanShowStickerPickerNfx()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    invoke-virtual {v1}, Lcom/discord/stores/StoreExpressionPickerNavigation;->observeSelectedTab()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/discord/stores/StoreStickers;->observeNewStickerPacks$default(Lcom/discord/stores/StoreStickers;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory$observeStoreState$1;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ize\n          )\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreExpressionPickerNavigation;->observeExpressionPickerEvents()Lrx/Observable;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/Observable;Lrx/Observable;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method
