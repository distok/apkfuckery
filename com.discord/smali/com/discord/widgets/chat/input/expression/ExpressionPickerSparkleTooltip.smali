.class public final Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;
.super Lcom/discord/tooltips/TooltipManager$Tooltip;
.source "ExpressionPickerSparkleTooltip.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "CACHE_KEY_EXPRESSION_SPARKLE_STICKER_TOOLTIP_ACKNOWLEDGED"

    const-string v1, "EXPRESSION_PICKER_SPARKLE_STICKER"

    invoke-direct {p0, v0, v1}, Lcom/discord/tooltips/TooltipManager$Tooltip;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
