.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;
.super Lx/m/c/k;
.source "WidgetExpressionTray.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->subscribeToScrollPosition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;->invoke(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Boolean;)V
    .locals 1

    const-string v0, "isAtInitialScrollPosition"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$getWasActive$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$showStickerPickerTooltip(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$hideStickerPickerTooltip(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    :goto_0
    return-void
.end method
