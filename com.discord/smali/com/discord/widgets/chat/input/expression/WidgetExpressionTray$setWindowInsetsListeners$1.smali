.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;
.super Ljava/lang/Object;
.source "WidgetExpressionTray.kt"

# interfaces
.implements Landroidx/core/view/OnApplyWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setWindowInsetsListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "insets"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$getEmojiPickerContent$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;

    move-result-object p1

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$getGifPickerContent$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;

    move-result-object p1

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$getStickerPickerContent$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;

    move-result-object p1

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    move-result-object p1

    return-object p1
.end method
