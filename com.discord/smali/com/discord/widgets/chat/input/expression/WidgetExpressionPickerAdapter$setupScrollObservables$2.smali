.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$2;
.super Lx/m/c/k;
.source "WidgetExpressionPickerAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setupScrollObservables()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$2;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$2;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$2;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->getOnScrollPositionListener()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$2;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
