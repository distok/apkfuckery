.class public final Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;
.super Lf/a/b/l0;
.source "ExpressionTrayViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;,
        Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

.field private wasActive:Z


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/Observable;Lrx/Observable;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreAnalytics;",
            "Lcom/discord/stores/StoreExpressionPickerNavigation;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;",
            ">;",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    const-string/jumbo v5, "storeAnalytics"

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v5, "storeExpressionPickerNavigation"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v5, "storeStateObservable"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "expressionPickerNavigationObservable"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    sget-object v8, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->EMOJI:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v6, v5

    invoke-direct/range {v6 .. v13}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;-><init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, v5}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    iput-object v2, v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v3, v0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    const-class v6, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    new-instance v11, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$1;

    invoke-direct {v11, v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$1;-><init>(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v12, 0x1e

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-static {v4, v0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v14

    const-class v15, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    new-instance v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$2;

    invoke-direct {v1, v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$2;-><init>(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x1e

    const/16 v22, 0x0

    move-object/from16 v20, v1

    invoke-static/range {v14 .. v22}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/Observable;Lrx/Observable;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getExpressionPickerNavigation()Lcom/discord/stores/StoreExpressionPickerNavigation;

    move-result-object p2

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/Observable;Lrx/Observable;)V

    return-void
.end method

.method public static final synthetic access$handleExpressionPickerEvents(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->handleExpressionPickerEvents(Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;)V

    return-void
.end method

.method private final getChatInputComponentType(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const-string/jumbo p1, "sticker"

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const-string p1, "GIF"

    goto :goto_0

    :cond_2
    const-string p1, "emoji"

    :goto_0
    return-object p1
.end method

.method private final handleExpressionPickerEvents(Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;)V
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$CloseExpressionPicker;->INSTANCE:Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$CloseExpressionPicker;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->hideExpressionPicker()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$OpenStickerPicker;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$OpenStickerPicker;

    invoke-virtual {p1}, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$OpenStickerPicker;->getStickerPackId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$OpenStickerPicker;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->showStickerPickerSheet(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;)V
    .locals 9

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->getCanShowStickerPickerNfx()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->getNewStickerPackCount()I

    move-result v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->updateViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    return-void
.end method

.method private final hideExpressionPicker()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$HideExpressionTray;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$HideExpressionTray;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final showEmojiPickerSheet()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowEmojiPickerSheet;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowEmojiPickerSheet;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final showGifPickerSheet()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowGifPickerSheet;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowGifPickerSheet;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final showStickerPickerSheet(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;-><init>(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic showStickerPickerSheet$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    move-object p2, v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->showStickerPickerSheet(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    return-void
.end method


# virtual methods
.method public final canShowStickerPickerNfx()Z
    .locals 1

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getCanShowStickerPickerNfx()Z

    move-result v0

    return v0
.end method

.method public final clickBack()V
    .locals 9

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getExpressionDetailPage()Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->updateViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    :cond_0
    return-void
.end method

.method public final clickSearch()V
    .locals 2

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {p0, v1, v1, v0, v1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->showStickerPickerSheet$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->showGifPickerSheet()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->showEmojiPickerSheet()V

    :goto_0
    return-void
.end method

.method public final handleIsActive(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->wasActive:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->getChatInputComponentType(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreAnalytics;->trackChatInputComponentViewed(Ljava/lang/String;)V

    :cond_0
    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->wasActive:Z

    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final selectGifCategory(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;)V
    .locals 9

    const-string v0, "gifCategoryItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    new-instance v4, Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage$GifCategoryPage;

    invoke-direct {v4, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage$GifCategoryPage;-><init>(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->updateViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    return-void
.end method

.method public final selectTab(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V
    .locals 1

    const-string v0, "expressionTrayTab"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreExpressionPickerNavigation;->onSelectTab(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V

    return-void
.end method

.method public final showStickersSearchBar(Z)V
    .locals 9

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    move v5, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->updateViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    return-void
.end method

.method public updateViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V
    .locals 3

    const-string/jumbo v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getShowLandingPage()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->wasActive:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-direct {p0, v1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->getChatInputComponentType(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreAnalytics;->trackChatInputComponentViewed(Ljava/lang/String;)V

    :cond_1
    invoke-super {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic updateViewState(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->updateViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    return-void
.end method
