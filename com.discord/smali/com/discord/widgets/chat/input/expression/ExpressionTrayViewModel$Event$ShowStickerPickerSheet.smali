.class public final Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;
.super Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;
.source "ExpressionTrayViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowStickerPickerSheet"
.end annotation


# instance fields
.field private final stickerPackId:Ljava/lang/Long;

.field private final stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->copy(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-object v0
.end method

.method public final copy(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;-><init>(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getStickerPackId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ShowStickerPickerSheet(stickerPackId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPackId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerPickerScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
