.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$1;
.super Ljava/lang/Object;
.source "WidgetExpressionPickerAdapter.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setupScrollObservables()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lrx/Emitter<",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Emitter;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$1;->call(Lrx/Emitter;)V

    return-void
.end method

.method public final call(Lrx/Emitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$1;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$1$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$setupScrollObservables$1$1;-><init>(Lrx/Emitter;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void
.end method
