.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;
.super Ljava/lang/Object;
.source "WidgetExpressionTray.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExpressionTabViews"
.end annotation


# instance fields
.field private final button:Landroid/widget/TextView;

.field private final card:Landroidx/cardview/widget/CardView;

.field private final content:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 1

    const-string v0, "button"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "content"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->button:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->card:Landroidx/cardview/widget/CardView;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->content:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final getButton()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->button:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getCard()Landroidx/cardview/widget/CardView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->card:Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method public final getContent()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->content:Landroid/view/View;

    return-object v0
.end method
