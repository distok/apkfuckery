.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;
.super Lcom/discord/app/AppFragment;
.source "WidgetExpressionTray.kt"

# interfaces
.implements Lf/b/a/e/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;,
        Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;

.field private static final isExpressionTrayActiveSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

.field private final detailPage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojiButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojiCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojiPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

.field private emojiPickerInitialized:Z

.field private emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

.field private expressionTabToViewsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
            "Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;",
            ">;"
        }
    .end annotation
.end field

.field private expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

.field private flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

.field private final gifButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gifPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

.field private gifPickerInitialized:Z

.field private final isAtInitialScrollPositionSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final landingPage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final landingPageContentContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

.field private onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final searchBar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerNewText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerPickerFeatureFlag:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

.field private stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

.field private stickerPickerInitialized:Z

.field private stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

.field private final toolbarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;

.field private wasActive:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x11

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "landingPage"

    const-string v7, "getLandingPage()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "landingPageContentContainer"

    const-string v7, "getLandingPageContentContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "detailPage"

    const-string v7, "getDetailPage()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string/jumbo v6, "toolbarLayout"

    const-string v7, "getToolbarLayout()Lcom/google/android/material/appbar/AppBarLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "emojiCard"

    const-string v7, "getEmojiCard()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "emojiButton"

    const-string v7, "getEmojiButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "gifCard"

    const-string v7, "getGifCard()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "gifButton"

    const-string v7, "getGifButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string/jumbo v6, "stickerCard"

    const-string v7, "getStickerCard()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string/jumbo v6, "stickerButton"

    const-string v7, "getStickerButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string/jumbo v6, "stickerNewText"

    const-string v7, "getStickerNewText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "searchButton"

    const-string v7, "getSearchButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "searchBar"

    const-string v7, "getSearchBar()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "emojiPickerContent"

    const-string v7, "getEmojiPickerContent()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string v6, "gifPickerContent"

    const-string v7, "getGifPickerContent()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const-string/jumbo v6, "stickerPickerContent"

    const-string v7, "getStickerPickerContent()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create(false)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isExpressionTrayActiveSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a03ef

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->landingPage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->landingPageContentContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->detailPage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0402

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->toolbarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03ff

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03fd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03fe

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerNewText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03fa

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->searchButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->searchBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03f7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0400

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "logger"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v1, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    move-object v7, v1

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string v0, "floatingViewManager"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/tooltips/TooltipManager;

    :cond_2
    if-nez v2, :cond_3

    new-instance v2, Lcom/discord/tooltips/TooltipManager;

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/a/l/a;

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Set;

    const/4 v6, 0x0

    const/4 v8, 0x4

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    :cond_3
    iput-object v2, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    new-instance v0, Lcom/discord/tooltips/DefaultTooltipCreator;

    invoke-direct {v0, v2}, Lcom/discord/tooltips/DefaultTooltipCreator;-><init>(Lcom/discord/tooltips/TooltipManager;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isAtInitialScrollPositionSubject:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerFeatureFlag:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    return-void
.end method

.method public static final synthetic access$getEmojiPickerContent$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiPickerContent()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getExpressionTrayViewModel$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "expressionTrayViewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getFlexInputViewModel$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "flexInputViewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getGifPickerContent$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifPickerContent()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getLandingPageContentContainer$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getLandingPageContentContainer()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStickerPickerContent$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerPickerContent()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getToolbarLayout$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Lcom/google/android/material/appbar/AppBarLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getToolbarLayout()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getWasActive$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->wasActive:Z

    return p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->handleEvent(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->handleViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$hideStickerPickerTooltip(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->hideStickerPickerTooltip()V

    return-void
.end method

.method public static final synthetic access$isAtInitialScrollPositionSubject$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isAtInitialScrollPositionSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$isExpressionTrayActiveSubject$cp()Lrx/subjects/BehaviorSubject;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isExpressionTrayActiveSubject:Lrx/subjects/BehaviorSubject;

    return-object v0
.end method

.method public static final synthetic access$onGifSearchSheetCanceled(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onGifSearchSheetCanceled()V

    return-void
.end method

.method public static final synthetic access$onGifSelected(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onGifSelected()V

    return-void
.end method

.method public static final synthetic access$setExpressionTrayViewModel$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    return-void
.end method

.method public static final synthetic access$setFlexInputViewModel$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    return-void
.end method

.method public static final synthetic access$setWasActive$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->wasActive:Z

    return-void
.end method

.method public static final synthetic access$showStickerPickerTooltip(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->showStickerPickerTooltip()V

    return-void
.end method

.method public static final synthetic access$showStickersSearchBar(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->showStickersSearchBar(Z)V

    return-void
.end method

.method public static final synthetic access$trackExpressionPickerTabClicked(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->trackExpressionPickerTabClicked(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V

    return-void
.end method

.method private final configureDetailPage(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V
    .locals 4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getExpressionDetailPage()Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage$GifCategoryPage;

    const v1, 0x7f0a03f1

    if-eqz v0, :cond_0

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage$GifCategoryPage;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage$GifCategoryPage;->getGifCategoryItem()Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;

    move-result-object p1

    const-string v3, "GIF_CATEGORY_ITEM"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    new-instance p1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$configureDetailPage$gifCategoryFragment$1$2;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$configureDetailPage$gifCategoryFragment$1$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;->setOnGifSelected(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    const-class v2, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v0, v2}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    :cond_1
    :goto_0
    return-void
.end method

.method private final configureLandingPage(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V
    .locals 12

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v3, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    invoke-virtual {v0, v3}, Lcom/discord/tooltips/TooltipManager;->a(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setUpStickerPicker()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setUpGifPicker()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setUpEmojiPicker()V

    :goto_0
    const v0, 0x7f04012c

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v0

    const v3, 0x7f06026f

    invoke-static {p0, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v3

    const v4, 0x7f060292

    invoke-static {p0, v4}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v4

    const v5, 0x7f040157

    invoke-static {p0, v5}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v5

    iget-object v6, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTabToViewsMap:Ljava/util/Map;

    if-eqz v6, :cond_b

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;

    invoke-virtual {v7}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->getButton()Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v7}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->getCard()Landroidx/cardview/widget/CardView;

    move-result-object v10

    invoke-virtual {v7}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;->getContent()Landroid/view/View;

    move-result-object v7

    const/4 v11, 0x0

    if-ne v8, p1, :cond_3

    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    :goto_2
    if-eqz v8, :cond_4

    goto :goto_3

    :cond_4
    const/16 v11, 0x8

    :goto_3
    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    if-eqz v8, :cond_5

    move v7, v0

    goto :goto_4

    :cond_5
    move v7, v3

    :goto_4
    invoke-virtual {v10, v7}, Landroidx/cardview/widget/CardView;->setCardBackgroundColor(I)V

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setSelected(Z)V

    if-eqz v8, :cond_6

    move v7, v4

    goto :goto_5

    :cond_6
    move v7, v5

    :goto_5
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_7
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_a

    if-eq p1, v2, :cond_9

    if-ne p1, v1, :cond_8

    const p1, 0x7f121612

    goto :goto_6

    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_9
    const p1, 0x7f121637

    goto :goto_6

    :cond_a
    const p1, 0x7f121610

    :goto_6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(searchTextStringRes)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getSearchButton()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getSearchBar()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_b
    const-string p1, "expressionTabToViewsMap"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final fetchDataForTrayOpen()V
    .locals 2

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStickers;->fetchStickerStoreDirectory(Z)V

    :cond_0
    return-void
.end method

.method private final getContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getDetailPage()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->detailPage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getEmojiButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmojiCard()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getEmojiPickerContent()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getGifButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGifCard()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getGifPickerContent()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getLandingPage()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->landingPage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getLandingPageContentContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->landingPageContentContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getSearchBar()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->searchBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSearchButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->searchButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getStickerButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getStickerCard()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getStickerNewText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerNewText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getStickerPickerContent()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getToolbarLayout()Lcom/google/android/material/appbar/AppBarLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->toolbarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/appbar/AppBarLayout;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V
    .locals 14

    sget-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$HideExpressionTray;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$HideExpressionTray;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideExpressionTray()Z

    goto/16 :goto_0

    :cond_0
    const-string p1, "flexInputViewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    sget-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowEmojiPickerSheet;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowEmojiPickerSheet;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "parentFragmentManager"

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_2
    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v0, "emoji search"

    invoke-virtual {p1, v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->chatInputComponentViewed(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    sget-object v5, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;->CHAT:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->launchBottomSheet$default(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    sget-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowGifPickerSheet;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowGifPickerSheet;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v0, "gif search"

    invoke-virtual {p1, v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->chatInputComponentViewed(Ljava/lang/String;)V

    sget-object p1, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet;->Companion:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    new-instance v2, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string/jumbo v3, "sticker search"

    invoke-virtual {v0, v3}, Lcom/discord/utilities/analytics/AnalyticsTracker;->chatInputComponentViewed(Ljava/lang/String;)V

    sget-object v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v5

    invoke-static {v5, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->getStickerPackId()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v8

    new-instance v9, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$pickerSheet$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$pickerSheet$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual/range {v4 .. v9}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lkotlin/jvm/functions/Function0;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    move-result-object v2

    sget-object v3, Lkotlin/Unit;->a:Lkotlin/Unit;

    new-instance v4, Lg0/l/e/j;

    invoke-direct {v4, v3}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-wide/16 v5, 0x1f4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5, v6, v3}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v3

    const-string v4, "Observable.just(Unit)\n  \u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-static {v3, p0, v1, v4, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    const-class v6, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    new-instance v11, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;

    invoke-direct {v11, v2, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V

    const/16 v12, 0x1e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz p1, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->getStickerPackId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollToPack(Ljava/lang/Long;)V

    goto :goto_0

    :cond_5
    const-string/jumbo p1, "stickerPickerFragment"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_6
    :goto_0
    return-void
.end method

.method private final handleViewState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->configureLandingPage(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->configureDetailPage(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getLandingPage()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getShowLandingPage()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getDetailPage()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getShowLandingPage()Z

    move-result v1

    const/4 v4, 0x1

    xor-int/2addr v1, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getSearchBar()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getShowSearchBar()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getCanShowStickerPickerNfx()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->wasActive:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isAtInitialScrollPositionSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "isAtInitialScrollPositionSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->i0()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "isAtInitialScrollPositionSubject.value"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->showStickerPickerTooltip()V

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    invoke-virtual {v0, v1}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerNewText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getNewStickerPackCount()I

    move-result v1

    if-lez v1, :cond_4

    goto :goto_4

    :cond_4
    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_5

    const/4 v2, 0x0

    :cond_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerNewText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getNewStickerPackCount()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final hideStickerPickerTooltip()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    invoke-virtual {v0, v1}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    return-void
.end method

.method private final initializeExpressionTabToViewsMap()V
    .locals 7

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/Pair;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->EMOJI:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    new-instance v2, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiButton()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiCard()Landroidx/cardview/widget/CardView;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiPickerContent()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;-><init>(Landroid/widget/TextView;Landroidx/cardview/widget/CardView;Landroid/view/View;)V

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->GIF:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    new-instance v3, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifButton()Landroid/widget/TextView;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifCard()Landroidx/cardview/widget/CardView;

    move-result-object v5

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifPickerContent()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;-><init>(Landroid/widget/TextView;Landroidx/cardview/widget/CardView;Landroid/view/View;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->STICKER:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    new-instance v3, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerButton()Landroid/widget/TextView;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerCard()Landroidx/cardview/widget/CardView;

    move-result-object v5

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerPickerContent()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$ExpressionTabViews;-><init>(Landroid/widget/TextView;Landroidx/cardview/widget/CardView;Landroid/view/View;)V

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v0, v1

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTabToViewsMap:Ljava/util/Map;

    return-void
.end method

.method public static final observeIsExpressionTrayActive()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;->observeIsExpressionTrayActive()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final onGifSearchSheetCanceled()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboardAndHideExpressionTray()V

    return-void

    :cond_0
    const-string v0, "flexInputViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final onGifSelected()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->showKeyboardAndHideExpressionTray()V

    return-void

    :cond_0
    const-string v0, "flexInputViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final setUpEmojiPicker()V
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerInitialized:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerInitialized:Z

    new-instance v0, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;-><init>()V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->setListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;->INLINE:Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;

    const-string v3, "MODE"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;->CHAT:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    const-string v3, "EMOJI_CONTEXT_TYPE"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0a03f4

    const-class v3, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    return-void
.end method

.method private final setUpGifPicker()V
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerInitialized:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerInitialized:Z

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;-><init>()V

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpGifPicker$1;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v2, :cond_1

    invoke-direct {v1, v2}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpGifPicker$1;-><init>(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->setOnSelectGifCategory(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0a03f7

    const-class v3, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    return-void

    :cond_1
    const-string v0, "expressionTrayViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final setUpStickerPicker()V
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerInitialized:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerInitialized:Z

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;-><init>()V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpStickerPicker$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpStickerPicker$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setShowSearchBar(Lkotlin/jvm/functions/Function1;)V

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpStickerPicker$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpStickerPicker$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setScrollExpressionPickerToTop(Lkotlin/jvm/functions/Function0;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    const-string v3, "MODE"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0a0400

    const-class v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    iput-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    return-void
.end method

.method private final setUpTabs()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpTabs$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpTabs$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpTabs$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpTabs$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpTabs$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setUpTabs$3;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerFeatureFlag:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final setWindowInsetsListeners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiPickerContent()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifPickerContent()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerPickerContent()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getDetailPage()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getLandingPageContentContainer()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getLandingPage()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$setWindowInsetsListeners$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private final showStickerPickerTooltip()V
    .locals 10

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->canShowStickerPickerNfx()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerButton()Landroid/widget/TextView;

    move-result-object v2

    sget-object v4, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f120002

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "resources.getString(R.string._new)"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/discord/floating_view_manager/FloatingViewGravity;->TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;

    const/4 v6, 0x0

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v7

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v8, Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v9, "tooltip"

    invoke-static {v8, v9}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/discord/tooltips/TooltipManager;->c:Ljava/util/Set;

    invoke-virtual {v8}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getTooltipName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v0

    sget-object v9, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$showStickerPickerTooltip$1;->INSTANCE:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$showStickerPickerTooltip$1;

    invoke-virtual {v0, v9}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v9

    const-string/jumbo v0, "this.unsubscribeSignal.map { }"

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {v1 .. v9}, Lcom/discord/tooltips/DefaultTooltipCreator;->a(Landroid/view/View;Ljava/lang/String;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "expressionTrayViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final showStickersSearchBar(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->showStickersSearchBar(Z)V

    return-void

    :cond_0
    const-string p1, "expressionTrayViewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final subscribeToScrollPosition()V
    .locals 11

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isAtInitialScrollPositionSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "isAtInitialScrollPositio\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    new-instance v8, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$subscribeToScrollPosition$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final trackExpressionPickerOpened()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerOpened$1;->INSTANCE:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerOpened$1;

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v1, "expressionTrayViewModel"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    throw v1
.end method

.method private final trackExpressionPickerTabClicked(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerTabClicked$1;

    move-object/from16 v1, p1

    invoke-direct {v14, v1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerTabClicked$1;-><init>(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v1, "expressionTrayViewModel"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    throw v1
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01ed

    return v0
.end method

.method public isShown(Z)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    const-string v1, "expressionTrayViewModel"

    const/4 v2, 0x0

    if-eqz v0, :cond_c

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->handleIsActive(Z)V

    sget-object v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->isExpressionTrayActiveSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const-string/jumbo v0, "stickerPickerFragment"

    if-eqz p1, :cond_6

    iget-boolean v3, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->wasActive:Z

    if-nez v3, :cond_6

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getToolbarLayout()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/material/appbar/AppBarLayout;->setExpanded(Z)V

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v1

    invoke-static {v1, v2, v3, v3, v2}, Lcom/discord/stores/StoreStickers;->cacheViewedPurchaseableStickerPacks$default(Lcom/discord/stores/StoreStickers;Ljava/util/Set;ZILjava/lang/Object;)V

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerInitialized:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerFragment:Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;->scrollToTop()V

    goto :goto_0

    :cond_0
    const-string p1, "emojiPickerFragment"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerInitialized:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->gifPickerFragment:Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;->scrollToTop()V

    goto :goto_1

    :cond_2
    const-string p1, "gifPickerFragment"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    :goto_1
    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerInitialized:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollToTop()V

    goto :goto_2

    :cond_4
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->showStickerPickerTooltip()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->fetchDataForTrayOpen()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->trackExpressionPickerOpened()V

    goto :goto_3

    :cond_6
    if-nez p1, :cond_8

    iget-boolean v3, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->wasActive:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->clickBack()V

    goto :goto_3

    :cond_7
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_8
    :goto_3
    if-nez p1, :cond_9

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->hideStickerPickerTooltip()V

    :cond_9
    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerInitialized:Z

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz v1, :cond_a

    invoke-virtual {v1, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->isShown(Z)V

    goto :goto_4

    :cond_a
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_b
    :goto_4
    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->wasActive:Z

    return-void

    :cond_c
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v7, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreStickers;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, v0, v7}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026rayViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;

    invoke-direct {v1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;-><init>()V

    invoke-direct {p1, v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026putViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setUpTabs()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->initializeExpressionTabToViewsMap()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getSearchBar()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->setWindowInsetsListeners()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getToolbarLayout()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    invoke-virtual {p1, v0}, Lcom/google/android/material/appbar/AppBarLayout;->addOnOffsetChangedListener(Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getEmojiCard()Landroidx/cardview/widget/CardView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;

    const v1, 0x3f666666    # 0.9f

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;-><init>(F)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getGifCard()Landroidx/cardview/widget/CardView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;-><init>(F)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->getStickerCard()Landroidx/cardview/widget/CardView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/gifpicker/ViewScalingOnTouchListener;-><init>(F)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    const/4 v1, 0x0

    const-string v2, "expressionTrayViewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->expressionTrayViewModel:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->subscribeToScrollPosition()V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final setEmojiPickerListener(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V
    .locals 1

    const-string v0, "emojiPickerListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->emojiPickerListener:Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;

    return-void
.end method

.method public final setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    return-void
.end method

.method public final setOnEmojiSearchOpenedListener(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onEmojiSearchOpenedListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setStickerPickerListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V
    .locals 1

    const-string/jumbo v0, "stickerPickerListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    return-void
.end method
