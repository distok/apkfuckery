.class public final enum Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
.super Ljava/lang/Enum;
.source "ExpressionTrayTab.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

.field public static final enum EMOJI:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

.field public static final enum GIF:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

.field public static final enum STICKER:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    new-instance v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const-string v2, "EMOJI"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->EMOJI:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const-string v2, "GIF"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->GIF:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const-string v2, "STICKER"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->STICKER:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->$VALUES:[Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
    .locals 1

    const-class v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->$VALUES:[Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-virtual {v0}, [Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-object v0
.end method
