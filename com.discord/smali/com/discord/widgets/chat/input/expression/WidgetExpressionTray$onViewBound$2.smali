.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$2;
.super Ljava/lang/Object;
.source "WidgetExpressionTray.kt"

# interfaces
.implements Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$2;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onOffsetChanged(Lcom/google/android/material/appbar/AppBarLayout;I)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$onViewBound$2;->this$0:Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->access$isAtInitialScrollPositionSubject$p(Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
