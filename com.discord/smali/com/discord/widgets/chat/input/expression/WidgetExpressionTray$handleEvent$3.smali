.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;
.super Lx/m/c/k;
.source "WidgetExpressionTray.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->handleEvent(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $event:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;

.field public final synthetic $pickerSheet:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;->$pickerSheet:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;->$event:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;->$pickerSheet:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$handleEvent$3;->$event:Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;

    check-cast v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPickerSheet;->getStickerPackId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->scrollToPack(Ljava/lang/Long;)V

    return-void
.end method
