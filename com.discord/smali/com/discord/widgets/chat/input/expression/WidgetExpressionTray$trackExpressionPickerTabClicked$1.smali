.class public final Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerTabClicked$1;
.super Lx/m/c/k;
.source "WidgetExpressionTray.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;->trackExpressionPickerTabClicked(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $clickedTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerTabClicked$1;->$clickedTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerTabClicked$1;->invoke(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$trackExpressionPickerTabClicked$1;->$clickedTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "sticker"

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const-string v0, "gif"

    goto :goto_0

    :cond_2
    const-string v0, "emoji"

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->STICKER:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    if-ne v2, v3, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->getNewStickerPackCount()I

    move-result p1

    if-lez p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {p1, v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->expressionPickerTabClicked(Ljava/lang/String;Z)V

    return-void
.end method
