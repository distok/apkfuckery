.class public final Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;
.super Ljava/lang/Object;
.source "ExpressionTrayViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final canShowStickerPickerNfx:Z

.field private final newStickerPackCount:I

.field private final selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;


# direct methods
.method public constructor <init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;I)V
    .locals 1

    const-string v0, "selectedExpressionTab"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    iput-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    iput p3, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;IILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->copy(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;I)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    return v0
.end method

.method public final component2()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    return v0
.end method

.method public final copy(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;I)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;
    .locals 1

    const-string v0, "selectedExpressionTab"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;-><init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    iget p1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanShowStickerPickerNfx()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    return v0
.end method

.method public final getNewStickerPackCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    return v0
.end method

.method public final getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StoreState(canShowStickerPickerNfx="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->canShowStickerPickerNfx:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", selectedExpressionTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newStickerPackCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;->newStickerPackCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
