.class public final Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;
.super Ljava/lang/Object;
.source "ExpressionTrayViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final canShowStickerPickerNfx:Z

.field private final expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

.field private final newStickerPackCount:I

.field private final selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

.field private final showLandingPage:Z

.field private final showSearchBar:Z

.field private final showStickersSearchBar:Z


# direct methods
.method public constructor <init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZI)V
    .locals 1

    const-string v0, "selectedExpressionTab"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    iput-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    iput-boolean p4, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    iput p5, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    const/4 p1, 0x1

    if-nez p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showLandingPage:Z

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    if-eqz p2, :cond_2

    if-eq p2, p1, :cond_2

    const/4 p1, 0x2

    if-ne p2, p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    const/4 p4, 0x1

    :goto_1
    iput-boolean p4, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showSearchBar:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    :cond_0
    move-object v3, p3

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;-><init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZI)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZIILjava/lang/Object;)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    :cond_4
    move v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->copy(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZI)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    return v0
.end method

.method public final component2()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-object v0
.end method

.method public final component3()Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    return v0
.end method

.method public final copy(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZI)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;
    .locals 7

    const-string v0, "selectedExpressionTab"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;-><init>(ZLcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    iget p1, p1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanShowStickerPickerNfx()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    return v0
.end method

.method public final getExpressionDetailPage()Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    return-object v0
.end method

.method public final getNewStickerPackCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    return v0
.end method

.method public final getSelectedExpressionTab()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    return-object v0
.end method

.method public final getShowLandingPage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showLandingPage:Z

    return v0
.end method

.method public final getShowSearchBar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showSearchBar:Z

    return v0
.end method

.method public final getShowStickersSearchBar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(canShowStickerPickerNfx="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->canShowStickerPickerNfx:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", selectedExpressionTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->selectedExpressionTab:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", expressionDetailPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->expressionDetailPage:Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showStickersSearchBar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->showStickersSearchBar:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", newStickerPackCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;->newStickerPackCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
