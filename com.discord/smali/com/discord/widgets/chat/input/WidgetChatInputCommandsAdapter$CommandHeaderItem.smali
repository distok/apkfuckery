.class public Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetChatInputCommandsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommandHeaderItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;",
        "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final itemAvatar:Landroid/widget/ImageView;

.field private final itemName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(ILcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0214

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->itemAvatar:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0217

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->itemName:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->itemAvatar:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getIcon()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->itemAvatar:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getIcon()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/icon/IconUtils;->setIcon(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->itemAvatar:Landroid/widget/ImageView;

    const v0, 0x7f0800a8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->itemName:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/models/slashcommands/ModelApplication;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;->onConfigure(ILcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;)V

    return-void
.end method
