.class public final Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$1;
.super Lx/m/c/k;
.source "WidgetChatInputAutocomplete.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->configureDataSubscriptions(Lcom/discord/app/AppFragment;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/stores/ModelApplicationCommand;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$1;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)V"
        }
    .end annotation

    const-string v0, "guildCommands"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete$configureDataSubscriptions$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;->access$onGuildCommandsUpdated(Lcom/discord/widgets/chat/input/WidgetChatInputAutocomplete;Ljava/util/List;)V

    return-void
.end method
