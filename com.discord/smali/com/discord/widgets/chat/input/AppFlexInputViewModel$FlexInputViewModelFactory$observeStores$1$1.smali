.class public final Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1$1;
.super Ljava/lang/Object;
.source "AppFlexInputViewModel.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/panels/PanelState;",
        "Lcom/discord/panels/PanelState;",
        "Ljava/lang/Long;",
        "Lcom/discord/stores/StoreNotices$Notice;",
        "Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;
    .locals 7

    new-instance v6, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;

    const-string v0, "leftPanelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rightPanelState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;-><init>(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)V

    return-object v6
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/panels/PanelState;

    check-cast p2, Lcom/discord/panels/PanelState;

    check-cast p3, Ljava/lang/Long;

    check-cast p4, Lcom/discord/stores/StoreNotices$Notice;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory$observeStores$1$1;->call(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;)Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
