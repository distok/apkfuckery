.class public final Lcom/discord/widgets/chat/input/MentionToken;
.super Ljava/lang/Object;
.source "WidgetChatInputEditText.kt"


# instance fields
.field private final formattedToken:Ljava/lang/String;

.field private final leadingIdentifier:Ljava/lang/Character;

.field private final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Character;Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "token"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const-string v1, "$this$replaceFirst"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "oldValue"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newValue"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p2, p1, v2, v2, v1}, Lx/s/r;->indexOf$default(Ljava/lang/CharSequence;Ljava/lang/String;IZI)I

    move-result v1

    if-gez v1, :cond_0

    move-object p1, p2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, v1

    invoke-static {p2, v1, p1, v0}, Lx/s/r;->replaceRange(Ljava/lang/CharSequence;IILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_1

    move-object p2, p1

    :cond_1
    iput-object p2, p0, Lcom/discord/widgets/chat/input/MentionToken;->formattedToken:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/MentionToken;Ljava/lang/Character;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/MentionToken;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/MentionToken;->copy(Ljava/lang/Character;Ljava/lang/String;)Lcom/discord/widgets/chat/input/MentionToken;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Character;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/Character;Ljava/lang/String;)Lcom/discord/widgets/chat/input/MentionToken;
    .locals 1

    const-string/jumbo v0, "token"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/MentionToken;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/MentionToken;-><init>(Ljava/lang/Character;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/MentionToken;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/MentionToken;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFormattedToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->formattedToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getLeadingIdentifier()Ljava/lang/Character;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "MentionToken(leadingIdentifier="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/MentionToken;->leadingIdentifier:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/MentionToken;->token:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
