.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;
.super Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;
.source "WidgetChatInputModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Replying"
.end annotation


# instance fields
.field private final messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

.field private final repliedAuthor:Lcom/discord/models/domain/ModelUser;

.field private final repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

.field private final shouldMention:Z

.field private final showMentionToggle:Z


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)V
    .locals 1

    const-string v0, "messageReference"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "repliedAuthor"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    iput-object p4, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->copy(Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelMessage$MessageReference;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    return v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component5()Lcom/discord/models/domain/ModelGuildMember$Computed;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;
    .locals 7

    const-string v0, "messageReference"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "repliedAuthor"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;-><init>(Lcom/discord/models/domain/ModelMessage$MessageReference;ZZLcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    return-object v0
.end method

.method public final getRepliedAuthor()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getRepliedAuthorGuildMember()Lcom/discord/models/domain/ModelGuildMember$Computed;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    return-object v0
.end method

.method public final getShouldMention()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    return v0
.end method

.method public final getShowMentionToggle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage$MessageReference;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    move v3, v2

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Replying(messageReference="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldMention="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->shouldMention:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showMentionToggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->showMentionToggle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", repliedAuthor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthor:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", repliedAuthorGuildMember="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState$Replying;->repliedAuthorGuildMember:Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
