.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;
.super Lx/m/c/k;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureSendListeners(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/util/List<",
        "+",
        "Lcom/lytefast/flexinput/model/Attachment<",
        "*>;>;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $chatInput:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

.field public final synthetic $messageManager:Lcom/discord/widgets/chat/MessageManager;

.field public final synthetic $model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

.field public final synthetic $weakListener:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Ljava/lang/ref/WeakReference;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$chatInput:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$weakListener:Ljava/lang/ref/WeakReference;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$messageManager:Lcom/discord/widgets/chat/MessageManager;

    iput-object p6, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method

.method public static synthetic invoke$default(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;Ljava/util/List;ZILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke(Ljava/util/List;Z)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->invoke(Ljava/util/List;Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/util/List;Z)Z
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;Z)Z"
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v0, p1

    move/from16 v1, p2

    const-string v2, "attachmentsRaw"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v3

    const-string v4, "2020-09_mobile_image_compression"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v3

    const/4 v9, 0x0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-nez v1, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getAutoImageCompressionEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    sget-object v1, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    iget-object v2, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$1;

    invoke-direct {v0, v8}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;)V

    invoke-virtual {v1, v2, v3, v0}, Lcom/discord/utilities/rest/SendUtils;->compressImageAttachments(Landroid/content/Context;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    return v9

    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$chatInput:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->getMatchedContentWithMetaData()Lcom/discord/models/domain/ModelMessage$Content;

    move-result-object v7

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const-string v2, "attachment"

    if-eqz v0, :cond_4

    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lytefast/flexinput/model/Attachment;

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isImage(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v10, 0x1

    :goto_2
    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    const/4 v11, 0x0

    goto :goto_3

    :cond_7
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lytefast/flexinput/model/Attachment;

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isVideo(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v11, 0x1

    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v6, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lytefast/flexinput/model/Attachment;

    invoke-virtual {v3}, Lcom/lytefast/flexinput/model/Attachment;->getUri()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v12, "context.contentResolver"

    invoke-static {v4, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/discord/utilities/rest/SendUtilsKt;->computeFileSizeMegabytes(Landroid/net/Uri;Landroid/content/ContentResolver;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    invoke-static {v0}, Lx/h/f;->sumOfFloat(Ljava/lang/Iterable;)F

    move-result v12

    invoke-static {v0}, Lx/h/f;->maxOrNull(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    move v13, v0

    goto :goto_5

    :cond_a
    const/4 v0, 0x0

    const/4 v13, 0x0

    :goto_5
    if-nez v1, :cond_f

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    const/4 v5, 0x0

    goto :goto_6

    :cond_c
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lytefast/flexinput/model/Attachment;

    const-string v3, "it"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isImage(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z

    move-result v2

    if-eqz v2, :cond_d

    :goto_6
    if-nez v5, :cond_e

    goto :goto_7

    :cond_e
    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;

    invoke-direct {v0, v8, v1, v6}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageResendCompressedHandler$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;ZLjava/util/ArrayList;)V

    goto :goto_8

    :cond_f
    :goto_7
    const/4 v0, 0x0

    :goto_8
    move-object v14, v0

    new-instance v15, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object v2, v6

    move v3, v10

    move v4, v11

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;Ljava/util/ArrayList;ZZLkotlin/jvm/functions/Function0;)V

    new-instance v5, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$onMessageTooLong$1;

    invoke-direct {v5, v8}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$onMessageTooLong$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;)V

    iget-object v0, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result v0

    const-string v1, "messageData.content"

    if-eqz v0, :cond_10

    iget-object v0, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$messageManager:Lcom/discord/widgets/chat/MessageManager;

    iget-object v2, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreChat$EditingMessage;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v17

    iget-object v2, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getEditingMessage()Lcom/discord/stores/StoreChat$EditingMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreChat$EditingMessage;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v19

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage$Content;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v16, v0

    move-object/from16 v21, v2

    move-object/from16 v22, v5

    invoke-virtual/range {v16 .. v22}, Lcom/discord/widgets/chat/MessageManager;->editMessage(JJLjava/lang/String;Lkotlin/jvm/functions/Function2;)Z

    move-result v0

    goto :goto_9

    :cond_10
    iget-object v4, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$messageManager:Lcom/discord/widgets/chat/MessageManager;

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage$Content;->getContent()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelMessage$Content;->getMentions()Ljava/util/List;

    move-result-object v18

    new-instance v7, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;

    iget-object v0, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getMaxFileSizeMB()I

    move-result v0

    invoke-direct {v7, v12, v0, v6}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;-><init>(FILjava/util/List;)V

    iget-object v0, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getChannelId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$1;

    invoke-direct {v2, v15}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    new-instance v24, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object v15, v2

    move v2, v12

    move-object v12, v3

    move v3, v13

    move-object v13, v4

    move-object v4, v6

    move-object/from16 v23, v5

    move v5, v10

    move v6, v11

    move-object v10, v7

    move-object v7, v14

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$synchronousValidationSucceeded$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;FFLjava/util/ArrayList;ZZLkotlin/jvm/functions/Function0;)V

    const/16 v26, 0x30

    const/16 v27, 0x0

    move-object/from16 v16, v13

    move-object/from16 v17, v12

    move-object/from16 v19, v10

    move-object/from16 v25, v15

    invoke-static/range {v16 .. v27}, Lcom/discord/widgets/chat/MessageManager;->sendMessage$default(Lcom/discord/widgets/chat/MessageManager;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Z

    move-result v0

    :goto_9
    if-nez v0, :cond_11

    return v9

    :cond_11
    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputSend;

    iget-object v2, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$chatInput:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    iget-object v3, v8, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->clearInput$default(Lcom/discord/widgets/chat/input/WidgetChatInputSend;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Ljava/lang/Boolean;ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method
