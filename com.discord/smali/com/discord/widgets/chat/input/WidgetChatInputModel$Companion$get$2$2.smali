.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;
.super Lx/m/c/k;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function12;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;->call(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$BootstrapData;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function12<",
        "Lcom/discord/stores/StoreChat$EditingMessage;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/chat/input/WidgetChatInputModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $me:Lcom/discord/models/domain/ModelUser$Me;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$Me;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$me:Lcom/discord/models/domain/ModelUser$Me;

    const/16 p1, 0xc

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/Integer;Ljava/lang/Long;IZLjava/lang/String;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;Ljava/util/Set;Z)Lcom/discord/widgets/chat/input/WidgetChatInputModel;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreChat$EditingMessage;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "IZ",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Z",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Lcom/discord/widgets/chat/input/WidgetChatInputModel;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "externalText"

    move-object/from16 v13, p6

    invoke-static {v13, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newStickerPacks"

    move-object/from16 v2, p11

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    iget-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isSystemDM()Z

    move-result v12

    iget-object v1, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    move-object/from16 v3, p3

    invoke-static {v1, v3}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccessWrite(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;)Z

    move-result v1

    const/4 v3, 0x2

    move-object/from16 v6, p2

    invoke-static {v6, v3}, Lcom/discord/models/domain/ModelUserRelationship;->isType(Ljava/lang/Integer;I)Z

    move-result v3

    iget-object v6, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$me:Lcom/discord/models/domain/ModelUser$Me;

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v6, :cond_0

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    if-nez p5, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    :goto_0
    iget-object v6, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v6

    const/4 v10, 0x5

    if-ne v6, v10, :cond_1

    if-nez v9, :cond_1

    const/16 v16, 0x1

    goto :goto_1

    :cond_1
    const/16 v16, 0x0

    :goto_1
    sget-object v6, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    iget-object v10, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;

    iget-object v10, v10, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2;->$context:Landroid/content/Context;

    iget-object v11, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v6, v10, v11, v3, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->access$getHint(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;ZZ)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$me:Lcom/discord/models/domain/ModelUser$Me;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getMaxFileSizeMB()I

    move-result v3

    goto :goto_2

    :cond_2
    const/16 v3, 0x8

    :goto_2
    if-eqz p8, :cond_3

    invoke-virtual/range {p8 .. p8}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB()I

    move-result v6

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v15

    invoke-interface/range {p11 .. p11}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v7

    if-eqz v2, :cond_4

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v19, 0x1

    goto :goto_4

    :cond_4
    const/16 v19, 0x0

    :goto_4
    new-instance v21, Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    move-object/from16 v2, v21

    iget-object v3, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v6, v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->$me:Lcom/discord/models/domain/ModelUser$Me;

    const-string v7, "isOnCooldown"

    move-object/from16 v8, p7

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    const-string v7, "pendingReplyState"

    move-object/from16 v11, p10

    invoke-static {v11, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v7, p1

    move-object v8, v1

    move/from16 v10, p4

    move/from16 v11, p5

    move-object/from16 v13, p6

    move/from16 v17, p9

    move-object/from16 v18, p10

    move/from16 v20, p12

    invoke-direct/range {v2 .. v20}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;-><init>(Lcom/discord/models/domain/ModelChannel;JLcom/discord/models/domain/ModelUser;Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/String;ZIZZLjava/lang/String;ZIZZLcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;ZZ)V

    return-object v21
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    move-object v1, p1

    check-cast v1, Lcom/discord/stores/StoreChat$EditingMessage;

    move-object v2, p2

    check-cast v2, Ljava/lang/Integer;

    move-object/from16 v3, p3

    check-cast v3, Ljava/lang/Long;

    move-object/from16 v0, p4

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v4

    move-object/from16 v0, p5

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object/from16 v6, p6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v7, p7

    check-cast v7, Ljava/lang/Boolean;

    move-object/from16 v8, p8

    check-cast v8, Lcom/discord/models/domain/ModelGuild;

    move-object/from16 v0, p9

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    move-object/from16 v10, p10

    check-cast v10, Lcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;

    move-object/from16 v11, p11

    check-cast v11, Ljava/util/Set;

    move-object/from16 v0, p12

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    move-object v0, p0

    invoke-virtual/range {v0 .. v12}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$get$2$2;->invoke(Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/Integer;Ljava/lang/Long;IZLjava/lang/String;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/widgets/chat/input/WidgetChatInputModel$PendingReplyState;Ljava/util/Set;Z)Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    move-result-object v0

    return-object v0
.end method
