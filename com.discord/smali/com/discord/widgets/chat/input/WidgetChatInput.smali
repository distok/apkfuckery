.class public final Lcom/discord/widgets/chat/input/WidgetChatInput;
.super Lcom/discord/app/AppFragment;
.source "WidgetChatInput.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private chatAttachments:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

.field private final chatGatingGuard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatGatingGuardAction$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatGuard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatGuardAction$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatGuardActionSecondary$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatGuardSubtext$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatGuardText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputApplicationCommandsRoot$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputContextBar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputContextCancel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputContextReplyMentionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputContextReplyMentionButtonImage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputContextReplyMentionButtonText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputContextText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private chatInputEditTextHolder:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

.field private final chatInputMentionsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private chatInputTruncatedHint:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

.field private final chatInputWidget$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInputWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flexInputFragment$delegate:Lkotlin/Lazy;

.field private flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

.field private final inlineVoiceVisibilityObserver:Lcom/discord/utilities/views/ViewVisibilityObserver;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;

.field private wasEditing:Z

.field private wasReplying:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x12

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputWrap"

    const-string v7, "getChatInputWrap()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputWidget"

    const-string v7, "getChatInputWidget()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputMentionsRecycler"

    const-string v7, "getChatInputMentionsRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputContextBar"

    const-string v7, "getChatInputContextBar()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputContextText"

    const-string v7, "getChatInputContextText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputContextCancel"

    const-string v7, "getChatInputContextCancel()Landroid/widget/ImageButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputContextReplyMentionButton"

    const-string v7, "getChatInputContextReplyMentionButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputContextReplyMentionButtonImage"

    const-string v7, "getChatInputContextReplyMentionButtonImage()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputContextReplyMentionButtonText"

    const-string v7, "getChatInputContextReplyMentionButtonText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGuard"

    const-string v7, "getChatGuard()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGuardText"

    const-string v7, "getChatGuardText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGuardSubtext"

    const-string v7, "getChatGuardSubtext()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatInputApplicationCommandsRoot"

    const-string v7, "getChatInputApplicationCommandsRoot()Landroidx/constraintlayout/widget/ConstraintLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGuardAction"

    const-string v7, "getChatGuardAction()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGuardActionSecondary"

    const-string v7, "getChatGuardActionSecondary()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGatingGuard"

    const-string v7, "getChatGatingGuard()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x11

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const-string v6, "chatGatingGuardAction"

    const-string v7, "getChatGatingGuardAction()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0218

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0239

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0238

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputWidget$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a022e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputMentionsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0219

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a021b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a021a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextCancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a021c

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextReplyMentionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a021d

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextReplyMentionButtonImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a021e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextReplyMentionButtonText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0221

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0225

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0224

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardSubtext$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00a0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputApplicationCommandsRoot$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0222

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardAction$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0223

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardActionSecondary$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;->INSTANCE:Lcom/discord/utilities/views/ViewVisibilityObserverProvider;

    const-string v1, "INLINE_VOICE_FEATURE"

    invoke-virtual {v0, v1}, Lcom/discord/utilities/views/ViewVisibilityObserverProvider;->get(Ljava/lang/String;)Lcom/discord/utilities/views/ViewVisibilityObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->inlineVoiceVisibilityObserver:Lcom/discord/utilities/views/ViewVisibilityObserver;

    const v0, 0x7f0a0210

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGatingGuard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0211

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGatingGuardAction$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInput$flexInputFragment$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInput$flexInputFragment$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputFragment$delegate:Lkotlin/Lazy;

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "logger"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v1, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    move-object v7, v1

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string v0, "floatingViewManager"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/tooltips/TooltipManager;

    :cond_2
    if-nez v2, :cond_3

    new-instance v2, Lcom/discord/tooltips/TooltipManager;

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/a/l/a;

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Set;

    const/4 v6, 0x0

    const/4 v8, 0x4

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    :cond_3
    iput-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInput;->configureUI(Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V

    return-void
.end method

.method public static final synthetic access$getChatGatingGuard$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGatingGuard()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChatGuard$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuard()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChatInputApplicationCommandsRoot$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputApplicationCommandsRoot()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChatInputEditTextHolder$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/discord/widgets/chat/input/WidgetChatInputEditText;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputEditTextHolder:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    return-object p0
.end method

.method public static final synthetic access$getChatInputMentionsRecycler$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputMentionsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChatInputTruncatedHint$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputTruncatedHint:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    return-object p0
.end method

.method public static final synthetic access$getChatInputWrap$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputWrap()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getContainer$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getContainer()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFlexInputFragment$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/lytefast/flexinput/fragment/FlexInputFragment;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getFlexInputFragment()Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTooltipManager$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/discord/tooltips/TooltipManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    return-object p0
.end method

.method public static final synthetic access$setChatInputEditTextHolder$p(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputEditTextHolder:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    return-void
.end method

.method public static final synthetic access$setChatInputTruncatedHint$p(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputTruncatedHint:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    return-void
.end method

.method public static final synthetic access$setWindowInsetsListeners(Lcom/discord/widgets/chat/input/WidgetChatInput;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInput;->setWindowInsetsListeners(Z)V

    return-void
.end method

.method public static final synthetic access$showFollowSheet(Lcom/discord/widgets/chat/input/WidgetChatInput;JJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/WidgetChatInput;->showFollowSheet(JJ)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V
    .locals 20

    move-object/from16 v6, p0

    move-object/from16 v15, p1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputWrap()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v15, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isInputShowing()Z

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x8

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGatingGuard()Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v15, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isGuildGatingEnabled()Z

    move-result v4

    if-ne v4, v2, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    const/4 v4, 0x0

    goto :goto_3

    :cond_3
    const/16 v4, 0x8

    :goto_3
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuard()Landroid/view/View;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGatingGuard()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_4

    :cond_4
    const/4 v4, 0x0

    :goto_4
    if-nez v4, :cond_8

    if-eqz v15, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isLurking()Z

    move-result v4

    if-eq v4, v2, :cond_9

    :cond_5
    if-eqz v15, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isVerificationLevelTriggered()Z

    move-result v4

    if-eq v4, v2, :cond_9

    :cond_6
    if-eqz v15, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isSystemDM()Z

    move-result v4

    if-eq v4, v2, :cond_9

    :cond_7
    if-eqz v15, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getShouldShowFollow()Z

    move-result v4

    if-ne v4, v2, :cond_8

    goto :goto_5

    :cond_8
    const/4 v2, 0x0

    :cond_9
    :goto_5
    if-eqz v2, :cond_a

    const/4 v2, 0x0

    goto :goto_6

    :cond_a
    const/16 v2, 0x8

    :goto_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz v15, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isEditing()Z

    move-result v0

    goto :goto_7

    :cond_b
    const/4 v0, 0x0

    :goto_7
    if-eqz v15, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isReplying()Z

    move-result v1

    :cond_c
    iget-boolean v2, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->wasEditing:Z

    const-string v18, "flexInputViewModel"

    const/16 v19, 0x0

    if-nez v2, :cond_d

    if-nez v0, :cond_e

    :cond_d
    iget-boolean v2, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->wasReplying:Z

    if-nez v2, :cond_10

    if-eqz v1, :cond_10

    :cond_e
    iget-object v2, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->hideExpressionTray()Z

    goto :goto_8

    :cond_f
    invoke-static/range {v18 .. v18}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v19

    :cond_10
    :goto_8
    iput-boolean v0, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->wasEditing:Z

    iput-boolean v1, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->wasReplying:Z

    if-nez v15, :cond_11

    return-void

    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isGuildGatingEnabled()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGatingGuardAction()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$1;

    invoke-direct {v1, v6, v15}, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_9

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isSystemDM()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardText()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1217cb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardSubtext()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1217cc

    invoke-virtual {v6, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardActionSecondary()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->isLurking()Z

    move-result v0

    const v1, 0x7f1208e4

    const v2, 0x7f120761

    if-eqz v0, :cond_15

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$lurkGuild$1;

    invoke-direct {v0, v15}, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$lurkGuild$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getShouldShowFollow()Z

    move-result v4

    const v5, 0x7f120f71

    if-eqz v4, :cond_14

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardText()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v6, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;

    invoke-direct {v2, v6, v15}, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardActionSecondary()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v6, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardActionSecondary()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInput$sam$android_view_View_OnClickListener$0;

    invoke-direct {v2, v0}, Lcom/discord/widgets/chat/input/WidgetChatInput$sam$android_view_View_OnClickListener$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_9

    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardText()Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f120f72

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v6, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInput$sam$android_view_View_OnClickListener$0;

    invoke-direct {v2, v0}, Lcom/discord/widgets/chat/input/WidgetChatInput$sam$android_view_View_OnClickListener$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardActionSecondary()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    :cond_15
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getShouldShowFollow()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v6, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$3;

    invoke-direct {v1, v6, v15}, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$3;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardActionSecondary()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardText()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardText()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "chatGuardText.context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getVerificationText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "chatGuardAction.context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getVerificationActionText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardAction()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getVerificationActionCallback()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuardActionSecondary()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_9
    iget-object v0, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputTruncatedHint:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    if-eqz v0, :cond_17

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getInputHint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->setHint(Ljava/lang/CharSequence;)V

    :cond_17
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v7, "requireContext()"

    invoke-static {v0, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputEditTextHolder:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    iget-object v2, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatAttachments:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    if-eqz v2, :cond_1c

    iget-object v5, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v5, :cond_1b

    move-object/from16 v3, p1

    move-object/from16 v4, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureSendListeners(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V

    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputSend;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v10, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v10, :cond_1a

    iget-object v11, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputEditTextHolder:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputContextBar()Landroid/view/View;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputContextText()Landroid/widget/TextView;

    move-result-object v13

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputContextCancel()Landroid/widget/ImageButton;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputContextReplyMentionButton()Landroid/view/View;

    move-result-object v1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputContextReplyMentionButtonImage()Landroid/widget/ImageView;

    move-result-object v16

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputContextReplyMentionButtonText()Landroid/widget/TextView;

    move-result-object v17

    move-object v7, v0

    move-object/from16 v9, p1

    move-object v15, v1

    invoke-virtual/range {v7 .. v17}, Lcom/discord/widgets/chat/input/WidgetChatInputSend;->configureContextBar(Landroid/content/Context;Lcom/discord/widgets/chat/input/WidgetChatInputModel;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getAbleToSendMessage()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getCanShowStickerPickerNfx()Z

    move-result v0

    if-eqz v0, :cond_18

    sget-object v0, Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;

    invoke-direct {v6, v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->showExpressionButtonSparkle(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    goto :goto_a

    :cond_18
    iget-object v0, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/expression/ExpressionPickerSparkleTooltip;

    invoke-virtual {v0, v1}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    :goto_a
    iget-object v0, v6, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v0, :cond_19

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getShouldBadgeChatInput()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;->setShowExpressionTrayButtonBadge(Z)V

    return-void

    :cond_19
    invoke-static/range {v18 .. v18}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v19

    :cond_1a
    invoke-static/range {v18 .. v18}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v19

    :cond_1b
    invoke-static/range {v18 .. v18}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v19

    :cond_1c
    const-string v0, "chatAttachments"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v19
.end method

.method private final getChatGatingGuard()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGatingGuard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChatGatingGuardAction()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGatingGuardAction$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChatGuard()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChatGuardAction()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardAction$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChatGuardActionSecondary()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardActionSecondary$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChatGuardSubtext()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardSubtext$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChatGuardText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatGuardText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChatInputApplicationCommandsRoot()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputApplicationCommandsRoot$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method private final getChatInputContextBar()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChatInputContextCancel()Landroid/widget/ImageButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextCancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    return-object v0
.end method

.method private final getChatInputContextReplyMentionButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextReplyMentionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChatInputContextReplyMentionButtonImage()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextReplyMentionButtonImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getChatInputContextReplyMentionButtonText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextReplyMentionButtonText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChatInputContextText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputContextText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getChatInputMentionsRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputMentionsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getChatInputWidget()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputWidget$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getChatInputWrap()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getFlexInputFragment()Lcom/lytefast/flexinput/fragment/FlexInputFragment;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputFragment$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    return-object v0
.end method

.method private final populateDirectShareData()V
    .locals 5

    sget-object v0, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/intent/IntentUtils;->getDirectShareId(Landroid/content/Intent;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/discord/utilities/ShareUtils;->INSTANCE:Lcom/discord/utilities/ShareUtils;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/discord/utilities/ShareUtils;->getSharedContent(Landroid/content/Intent;Z)Lcom/discord/utilities/ShareUtils$SharedContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/utilities/ShareUtils$SharedContent;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {v3}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :cond_1
    :goto_0
    if-nez v4, :cond_2

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/utilities/ShareUtils$SharedContent;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v1, v4}, Lcom/discord/stores/StoreChat;->setTextChannelInput(JLjava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/discord/utilities/ShareUtils$SharedContent;->getUris()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatAttachments:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    if-eqz v3, :cond_3

    sget-object v4, Lcom/lytefast/flexinput/model/Attachment;->Companion:Lcom/lytefast/flexinput/model/Attachment$Companion;

    invoke-virtual {v4, v2, v0}, Lcom/lytefast/flexinput/model/Attachment$Companion;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->addExternalAttachment(Lcom/lytefast/flexinput/model/Attachment;)V

    goto :goto_1

    :cond_3
    const-string v0, "chatAttachments"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0

    :cond_4
    return-void
.end method

.method private final setWindowInsetsListeners(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputWrap()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputWidget()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGuard()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$1;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatGatingGuard()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$2;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$2;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getContainer()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInput$setWindowInsetsListeners$3;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Z)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestApplyInsets()V

    return-void
.end method

.method public static synthetic setWindowInsetsListeners$default(Lcom/discord/widgets/chat/input/WidgetChatInput;ZILjava/lang/Object;)V
    .locals 0

    const/4 p3, 0x1

    and-int/2addr p2, p3

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInput;->setWindowInsetsListeners(Z)V

    return-void
.end method

.method private final showExpressionButtonSparkle(Lcom/discord/tooltips/TooltipManager$Tooltip;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getFlexInputFragment()Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInput$showExpressionButtonSparkle$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final showFollowSheet(JJ)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->Companion:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;JJ)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01ab

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;

    invoke-direct {v1}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;-><init>()V

    invoke-direct {p1, v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026putViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    return-void
.end method

.method public onDestroyView()V
    .locals 3

    invoke-static {}, Lf/a/f/b$b;->a()Lf/a/f/b;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputWrap()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    iget-object v2, v0, Lf/a/f/b;->d:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/a/f/b;->a()V

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onFilesTooLarge(IFFZLjava/util/List;ZZLkotlin/jvm/functions/Function0;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFFZ",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "attachments"

    move-object/from16 v8, p5

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v0, "context ?: return"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    const-string v0, "parentFragmentManager"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    move v4, p2

    move v5, p1

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move-object/from16 v11, p8

    invoke-virtual/range {v1 .. v11}, Lcom/discord/utilities/rest/SendUtils;->tryShowFilesTooLargeDialog(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;FIFZLjava/util/List;ZZLkotlin/jvm/functions/Function0;)Z

    :cond_0
    return-void
.end method

.method public onMessageTooLong(II)V
    .locals 17
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringFormatMatches"
        }
    .end annotation

    move-object/from16 v0, p0

    sget-object v1, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f121047

    invoke-virtual {v0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    const v5, 0x7f121046

    invoke-virtual {v0, v5, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(\n            R\u2026xCharacterCount\n        )"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    invoke-static/range {v1 .. v16}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    new-instance p1, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getFlexInputFragment()Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatAttachments:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->configureFlexInputFragment(Lcom/discord/app/AppFragment;)V

    sget-object p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object p1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->fetchExperiment(Lcom/discord/stores/StoreExperiments;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getFlexInputFragment()Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;)V

    invoke-virtual {p1, v1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->getChatInputWrap()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-static {}, Lf/a/f/b$b;->a()Lf/a/f/b;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v1, v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->setWindowInsetsListeners$default(Lcom/discord/widgets/chat/input/WidgetChatInput;ZILjava/lang/Object;)V

    return-void

    :cond_0
    const-string p1, "chatAttachments"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->populateDirectShareData()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputEditTextHolder:Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->configureMentionsDataSubscriptions(Lcom/discord/app/AppFragment;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatInputTruncatedHint:Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->addBindedTextWatcher(Lcom/discord/app/AppFragment;)V

    :cond_1
    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->get(Landroid/content/Context;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->chatAttachments:Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;->setViewModel(Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput;->inlineVoiceVisibilityObserver:Lcom/discord/utilities/views/ViewVisibilityObserver;

    invoke-virtual {v0}, Lcom/discord/utilities/views/ViewVisibilityObserver;->observeIsVisible()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/WidgetChatInput;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/chat/input/WidgetChatInput;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_2
    const-string v0, "flexInputViewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    const-string v0, "chatAttachments"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
