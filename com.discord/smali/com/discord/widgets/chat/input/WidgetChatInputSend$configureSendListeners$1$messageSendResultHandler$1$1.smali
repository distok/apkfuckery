.class public final Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;
.super Lx/m/c/k;
.source "WidgetChatInputSend.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->invoke(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/domain/ModelGuild;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guild:Lcom/discord/models/domain/ModelGuild;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->$guild:Lcom/discord/models/domain/ModelGuild;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->$guild:Lcom/discord/models/domain/ModelGuild;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;

    iget-object v2, v2, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iget-object v2, v2, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getMaxFileSizeMB()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;

    iget-object v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iget-object v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$weakListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;

    if-eqz v3, :cond_3

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    const v6, 0x7f7fffff    # Float.MAX_VALUE

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;

    iget-object v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;

    iget-object v0, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v1

    move v7, v1

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    :goto_2
    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;

    iget-object v8, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$attachments:Ljava/util/ArrayList;

    iget-boolean v9, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$hasImage:Z

    iget-boolean v10, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$hasVideo:Z

    iget-object v11, v0, Lcom/discord/widgets/chat/input/WidgetChatInputSend$configureSendListeners$1$messageSendResultHandler$1;->$messageResendCompressedHandler:Lkotlin/jvm/functions/Function0;

    invoke-interface/range {v3 .. v11}, Lcom/discord/widgets/chat/input/WidgetChatInputSend$Listener;->onFilesTooLarge(IFFZLjava/util/List;ZZLkotlin/jvm/functions/Function0;)V

    :cond_3
    return-void
.end method
