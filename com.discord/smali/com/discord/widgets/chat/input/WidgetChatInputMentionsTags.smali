.class public final Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;
.super Ljava/lang/Object;
.source "WidgetChatInputMentionsTags.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final token:Ljava/lang/String;

.field private final tokenIdentifier:C


# direct methods
.method public constructor <init>(Ljava/util/List;CLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;C",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "tags"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "token"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    iput-char p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;Ljava/util/List;CLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-char p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->copy(Ljava/util/List;CLjava/lang/String;)Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    return-object v0
.end method

.method public final component2()C
    .locals 1

    iget-char v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/util/List;CLjava/lang/String;)Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;C",
            "Ljava/lang/String;",
            ")",
            "Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags<",
            "TT;>;"
        }
    .end annotation

    const-string/jumbo v0, "tags"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "token"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;-><init>(Ljava/util/List;CLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-char v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    iget-char v1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final getTokenIdentifier()C
    .locals 1

    iget-char v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-char v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "WidgetChatInputMentionsTags(tags="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tags:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tokenIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-char v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->tokenIdentifier:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputMentionsTags;->token:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
