.class public Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetChatInputCommandsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$Item;,
        Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMMAND_HEADER_HEIGHT:I = 0x20

.field private static final CONTAINER_ROW_MAX_VISIBLE_COUNT:I = 0x4

.field private static final MENTION_CONTAINER_MAX_SIZE:I = 0xb0

.field private static final MENTION_CONTAINER_MAX_SIZE_OLD:I = 0x80

.field private static final MENTION_ROW_HEIGHT_DEFAULT:I = 0x2c

.field private static final MENTION_ROW_HEIGHT_OLD:I = 0x20


# instance fields
.field public final onClickAction:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation
.end field

.field private showAvatar:Ljava/lang/Boolean;

.field public slashCommandsEnabled:Z


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;ZLrx/functions/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Z",
            "Lrx/functions/Action1<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object p3, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->onClickAction:Lrx/functions/Action1;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->slashCommandsEnabled:Z

    return-void
.end method

.method public static synthetic access$100(Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;)Ljava/lang/Boolean;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->showAvatar:Ljava/lang/Boolean;

    return-object p0
.end method

.method private getDataHeight(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;)I"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;->getType()I

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x20

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v1, 0x2c

    goto :goto_0

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    iget-boolean p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->slashCommandsEnabled:Z

    if-eqz p1, :cond_1

    const/4 p1, 0x7

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;

    const p2, 0x7f0d01ae

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$CommandHeaderItem;-><init>(ILcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;)V

    return-object p1

    :cond_0
    const p1, 0x7f0d01ad

    goto :goto_0

    :cond_1
    const p1, 0x7f0d01b1

    :goto_0
    new-instance p2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$Item;

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->slashCommandsEnabled:Z

    invoke-direct {p2, p1, p0, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$Item;-><init>(ILcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;Z)V

    return-object p2
.end method

.method public setData(Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->slashCommandsEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->getDataHeight(Ljava/util/List;)I

    move-result v1

    const/16 v2, 0xb0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x20

    const/16 v2, 0x80

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_0
    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->requestLayout()V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter;->showAvatar:Ljava/lang/Boolean;

    invoke-super {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method
