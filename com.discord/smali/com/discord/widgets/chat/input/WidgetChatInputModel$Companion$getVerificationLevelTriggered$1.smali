.class public final Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;
.super Ljava/lang/Object;
.source "WidgetChatInputModel.kt"

# interfaces
.implements Lrx/functions/Func5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->getVerificationLevelTriggered(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func5<",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Ljava/lang/Integer;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Lcom/discord/models/domain/ModelUser$Me;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;->INSTANCE:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Integer;Ljava/util/Map;Lcom/discord/models/domain/ModelUser$Me;)Ljava/lang/Integer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Lcom/discord/models/domain/ModelUser$Me;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->Companion:Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;

    if-eqz p5, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/discord/models/domain/ModelGuildMember$Computed;

    goto :goto_0

    :cond_0
    const/4 p4, 0x0

    :goto_0
    move-object v3, p4

    const-string/jumbo p4, "verificationLevel"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const-string p3, "joinedAt"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object v1, p5

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;->access$computeVerificationLevelTriggered(Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion;Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildMember$Computed;IJ)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    check-cast p2, Lcom/discord/models/domain/ModelGuild;

    check-cast p3, Ljava/lang/Integer;

    check-cast p4, Ljava/util/Map;

    check-cast p5, Lcom/discord/models/domain/ModelUser$Me;

    invoke-virtual/range {p0 .. p5}, Lcom/discord/widgets/chat/input/WidgetChatInputModel$Companion$getVerificationLevelTriggered$1;->call(Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Integer;Ljava/util/Map;Lcom/discord/models/domain/ModelUser$Me;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
