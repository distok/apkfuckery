.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;
.super Lcom/discord/tooltips/TooltipManager$Tooltip;
.source "StickerPackStoreTooltip.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "CACHE_KEY_STICKER_PACK_STORE_TOOLTIP_ACKNOWLEDGED"

    const-string v1, "STICKER_PACK_STORE"

    invoke-direct {p0, v0, v1}, Lcom/discord/tooltips/TooltipManager$Tooltip;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
