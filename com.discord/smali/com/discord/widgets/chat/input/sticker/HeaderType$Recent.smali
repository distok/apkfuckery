.class public final Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;
.super Lcom/discord/widgets/chat/input/sticker/HeaderType;
.source "StickerAdapterItems.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/HeaderType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Recent"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/sticker/HeaderType;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    const-string v0, "recents"

    return-object v0
.end method
