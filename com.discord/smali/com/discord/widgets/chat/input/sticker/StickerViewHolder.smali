.class public final Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "StickerAdapterViewHolders.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final stickerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final type:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;

    const-string/jumbo v3, "stickerView"

    const-string v4, "getStickerView()Lcom/discord/views/sticker/StickerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(ILcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d00ef

    invoke-direct {p0, v0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iput p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->type:I

    const p1, 0x7f0a0a45

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->stickerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    return-object p0
.end method

.method public static final synthetic access$getStickerView$p(Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;)Lcom/discord/views/sticker/StickerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object p0

    return-object p0
.end method

.method private final getStickerView()Lcom/discord/views/sticker/StickerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->stickerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    return-object v0
.end method


# virtual methods
.method public final cancelLoading()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/views/sticker/StickerView;->e()V

    return-void
.end method

.method public getSubscription()Lrx/Subscription;
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/views/sticker/StickerView;->getSubscription()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->type:I

    return v0
.end method

.method public onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    instance-of p1, p2, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->getStickerView()Lcom/discord/views/sticker/StickerView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder$onConfigure$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    invoke-virtual {p1, v0}, Lcom/discord/views/sticker/StickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
