.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;
.super Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;
.source "WidgetStickerAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$Companion;

.field public static final DEFAULT_NUM_COLUMNS:I = 0x4

.field public static final ITEM_TYPE_OWNED_HEADER:I = 0x0

.field public static final ITEM_TYPE_STICKER:I = 0x1

.field public static final ITEM_TYPE_STORE_BUY_BUTTON:I = 0x3

.field public static final ITEM_TYPE_STORE_HEADER:I = 0x2


# instance fields
.field private final layoutManager:Landroidx/recyclerview/widget/GridLayoutManager;

.field private final numColumns:I

.field private final onBuyButtonPurchased:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onStickerHeaderItemsClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onStickerItemSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/sticker/StickerItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/sticker/StickerItem;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "-",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "-",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onStickerItemSelected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Ljava/util/Set;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onStickerItemSelected:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onBuyButtonPurchased:Lkotlin/jvm/functions/Function3;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onStickerHeaderItemsClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p3, "recycler.context"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget-object p3, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;

    const p4, 0x7f07007f

    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    const/4 p4, 0x4

    invoke-virtual {p3, p1, p2, p4}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;->calculateNumOfColumns(Landroidx/recyclerview/widget/RecyclerView;FI)I

    move-result p2

    iput p2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->numColumns:I

    new-instance p2, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->getNumColumns()I

    move-result p4

    invoke-direct {p2, p3, p4}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->layoutManager:Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;

    move-result-object p2

    new-instance p3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$1;

    invoke-direct {p3, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V

    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-virtual {p1, p0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createStickyHeaderViewHolder(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->createStickyHeaderViewHolder(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public createStickyHeaderViewHolder(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V

    return-object v0
.end method

.method public getLayoutManager()Landroidx/recyclerview/widget/GridLayoutManager;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->layoutManager:Landroidx/recyclerview/widget/GridLayoutManager;

    return-object v0
.end method

.method public getNumColumns()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->numColumns:I

    return v0
.end method

.method public final getOnBuyButtonPurchased()Lkotlin/jvm/functions/Function3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function3<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onBuyButtonPurchased:Lkotlin/jvm/functions/Function3;

    return-object v0
.end method

.method public final getOnStickerHeaderItemsClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onStickerHeaderItemsClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnStickerItemSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/sticker/StickerItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onStickerItemSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public isHeader(I)Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getInternalData()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "this.internalData"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    instance-of p1, p1, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;

    return p1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_3

    const/4 p1, 0x1

    if-eq p2, p1, :cond_2

    const/4 p1, 0x2

    if-eq p2, p1, :cond_1

    const/4 p1, 0x3

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;

    invoke-direct {p1, p2, p0}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;-><init>(ILcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V

    goto :goto_0

    :cond_3
    new-instance p1, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V

    :goto_0
    return-object p1
.end method
