.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "StickerPackStoreSheetViewModel.kt"

# interfaces
.implements Lrx/functions/Func6;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func6<",
        "Lcom/discord/stores/StoreStickers$StickerPackState;",
        "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/lang/Integer;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;I",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;"
        }
    .end annotation

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownedStickerPackState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasingPacks"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewedPurchaseableStickerPacks"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUser"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;-><init>(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    move-object v1, p1

    check-cast v1, Lcom/discord/stores/StoreStickers$StickerPackState;

    move-object v2, p2

    check-cast v2, Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-object v3, p3

    check-cast v3, Ljava/util/Set;

    move-object v4, p4

    check-cast v4, Ljava/util/Map;

    check-cast p5, Ljava/lang/Number;

    invoke-virtual {p5}, Ljava/lang/Number;->intValue()I

    move-result v5

    move-object v6, p6

    check-cast v6, Lcom/discord/models/domain/ModelUser;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;->call(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
