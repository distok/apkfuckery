.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;
.super Lcom/discord/tooltips/TooltipManager$Tooltip;
.source "StickerPickerTooltip.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "CACHE_KEY_STICKER_PICKER_TOOLTIP_ACKNOWLEDGED"

    const-string v1, "STICKER_PICKER"

    invoke-direct {p0, v0, v1}, Lcom/discord/tooltips/TooltipManager$Tooltip;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
