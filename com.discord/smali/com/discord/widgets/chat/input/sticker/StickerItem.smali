.class public final Lcom/discord/widgets/chat/input/sticker/StickerItem;
.super Ljava/lang/Object;
.source "StickerAdapterItems.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;
    }
.end annotation


# instance fields
.field private final mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

.field private final sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field private final stickerAnimationSettings:I


# direct methods
.method public constructor <init>(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)V
    .locals 1

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iput p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerItem;Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerItem;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->copy(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)Lcom/discord/widgets/chat/input/sticker/StickerItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    return v0
.end method

.method public final component3()Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)Lcom/discord/widgets/chat/input/sticker/StickerItem;
    .locals 1

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/StickerItem;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    iget v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getKey()Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, "sticker:"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMode()Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    return-object v0
.end method

.method public final getSticker()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    return-object v0
.end method

.method public final getStickerAnimationSettings()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    return v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelSticker;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StickerItem(sticker="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerAnimationSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->stickerAnimationSettings:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerItem;->mode:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
