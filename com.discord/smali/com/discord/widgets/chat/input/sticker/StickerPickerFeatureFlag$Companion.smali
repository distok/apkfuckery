.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;
.super Ljava/lang/Object;
.source "StickerPickerFeatureFlag.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;-><init>()V

    return-void
.end method

.method public static synthetic getINSTANCE$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;
    .locals 2

    invoke-static {}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->access$getINSTANCE$cp()Lkotlin/Lazy;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    return-object v0
.end method
