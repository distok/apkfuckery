.class public final Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;
.super Ljava/lang/Object;
.source "StickerAdapterViewHolders.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;->$data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->access$getAdapter$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;->getOnBuyButtonPurchased()Lkotlin/jvm/functions/Function3;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;->$data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;->$data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    check-cast v2, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
