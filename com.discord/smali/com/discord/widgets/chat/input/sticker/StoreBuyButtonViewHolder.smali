.class public final Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "StickerAdapterViewHolders.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;",
        "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;"
    }
.end annotation


# instance fields
.field private final buy:Lcom/discord/views/LoadingButton;

.field private final buyPremium:Lcom/discord/views/LoadingButton;

.field private final divider:Landroid/view/View;

.field private subscription:Lrx/Subscription;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d00f0

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a56

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026store_buy_button_premium)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/LoadingButton;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a54

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026sticker_store_buy_button)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/LoadingButton;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a55

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026store_buy_button_divider)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->divider:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    return-object p0
.end method

.method public static final synthetic access$getBuy$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/views/LoadingButton;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    return-object p0
.end method

.method public static final synthetic access$getBuyPremium$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/views/LoadingButton;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    return-object p0
.end method

.method public static final synthetic access$getSubscription$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->subscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$setSubscription$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->subscription:Lrx/Subscription;

    return-void
.end method


# virtual methods
.method public bind(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method

.method public getItemView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v1, "itemView"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getSubscription()Lrx/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->subscription:Lrx/Subscription;

    return-object v0
.end method

.method public onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 13

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    instance-of v0, p2, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    move-object v1, p2

    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v2

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v3

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0, v3, v4}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v3

    const-string v4, "context"

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v5

    invoke-virtual {v0, p1, v4, v5}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v6

    invoke-virtual {v0, p1, v5, v6}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPremiumPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v5

    sget-object v6, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-ne v5, v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    iget-object v6, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    invoke-virtual {v6, v0}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getOwned()Z

    move-result v6

    if-eqz v6, :cond_1

    const v4, 0x7f121713

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_1
    invoke-virtual {v0, v4}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getOwned()Z

    move-result v0

    xor-int/2addr v0, v7

    const/16 v4, 0x8

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getOwned()Z

    move-result v0

    if-nez v0, :cond_4

    if-nez v5, :cond_3

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_5

    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    const/16 v0, 0x8

    :goto_4
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getOwned()Z

    move-result v0

    xor-int/2addr v0, v7

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {p1, v0, v2, v5, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getOwned()Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$2;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$2;-><init>(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    invoke-virtual {p1, v7}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    invoke-virtual {p1, v7}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    goto :goto_5

    :cond_6
    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    invoke-virtual {p1, v8}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    invoke-virtual {p1, v8}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buy:Lcom/discord/views/LoadingButton;

    invoke-virtual {p1, v6}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->buyPremium:Lcom/discord/views/LoadingButton;

    invoke-virtual {p1, v6}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_5
    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getShowDivider()Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_6

    :cond_7
    const/16 v8, 0x8

    :goto_6
    invoke-virtual {p1, v8}, Landroid/view/View;->setVisibility(I)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreStickers;->observePurchasingStickerPacks()Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$3;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$3;-><init>(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;

    invoke-direct {v10, p0, p2, v3}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;-><init>(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;Z)V

    const/16 v11, 0x1a

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_8
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
