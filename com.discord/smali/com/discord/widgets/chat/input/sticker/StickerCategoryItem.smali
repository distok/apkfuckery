.class public abstract Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;
.super Ljava/lang/Object;
.source "StickerCategoryItem.kt"

# interfaces
.implements Lcom/discord/utilities/recycler/DiffKeyProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;,
        Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;,
        Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$Companion;

.field public static final TYPE_PACK:I = 0x1

.field public static final TYPE_RECENT:I


# instance fields
.field private final isSelected:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$Companion;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;->isSelected:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;->isSelected:Z

    return v0
.end method
