.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;
.super Lx/m/c/k;
.source "StickerPickerViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function10;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function10<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/sticker/dto/ModelStickerPack;",
        ">;",
        "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/sticker/dto/ModelSticker;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
        "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
            ")",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;"
        }
    .end annotation

    move-object/from16 v0, p10

    const-string/jumbo v1, "stickerPackStoreDirectory"

    move-object v3, p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "ownedStickerPackState"

    move-object v4, p2

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "frequentlyUsedStickers"

    move-object/from16 v5, p3

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "viewedPurchaseableStickerPackIds"

    move-object/from16 v6, p4

    invoke-static {v6, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newStickerPacks"

    move-object/from16 v2, p5

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "searchInputString"

    move-object/from16 v7, p6

    invoke-static {v7, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "meUser"

    move-object/from16 v10, p9

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "selectedExpressionPickerTab"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    invoke-interface/range {p5 .. p5}, Ljava/util/Set;->size()I

    move-result v11

    sget-object v2, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->STICKER:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    const/4 v12, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v12, 0x0

    :goto_0
    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v2 .. v12}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;-><init>(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;IZ)V

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    move-object v1, p1

    check-cast v1, Ljava/util/List;

    move-object v2, p2

    check-cast v2, Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-object v3, p3

    check-cast v3, Ljava/util/List;

    move-object v4, p4

    check-cast v4, Ljava/util/Map;

    move-object/from16 v5, p5

    check-cast v5, Ljava/util/Set;

    move-object/from16 v6, p6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, p7

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v7

    move-object/from16 v8, p8

    check-cast v8, Ljava/lang/Long;

    move-object/from16 v9, p9

    check-cast v9, Lcom/discord/models/domain/ModelUser;

    move-object/from16 v10, p10

    check-cast v10, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    move-object v0, p0

    invoke-virtual/range {v0 .. v10}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;->invoke(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;

    move-result-object v0

    return-object v0
.end method
