.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;
.super Lf/a/b/l0;
.source "StickerPackStoreSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;,
        Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;


# instance fields
.field private final analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final stickerPackId:J

.field private final stickersStore:Lcom/discord/stores/StoreStickers;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lrx/Observable;JLcom/discord/stores/StoreStickers;Lcom/discord/utilities/time/Clock;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;",
            ">;J",
            "Lcom/discord/stores/StoreStickers;",
            "Lcom/discord/utilities/time/Clock;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    const-string/jumbo v4, "storeStateObservable"

    move-object v5, p1

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "stickersStore"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "clock"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "analytics"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    move-wide/from16 v6, p2

    iput-wide v6, v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickerPackId:J

    iput-object v1, v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    iput-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    iput-object v3, v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->fetchStickersData()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->trackStickerPackStoreSheetViewed()V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, p0, v4, v2, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    const-class v6, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    new-instance v11, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$1;

    invoke-direct {v11, p0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v12, 0x1e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lrx/Observable;JLcom/discord/stores/StoreStickers;Lcom/discord/utilities/time/Clock;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x8

    if-eqz p7, :cond_0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p5

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;-><init>(Lrx/Observable;JLcom/discord/stores/StoreStickers;Lcom/discord/utilities/time/Clock;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V

    return-void
.end method

.method private final handleLoadedStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V
    .locals 9

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getStickerAnimationSettings()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getStickerPack()Lcom/discord/stores/StoreStickers$StickerPackState;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type com.discord.stores.StoreStickers.StickerPackState.Loaded"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v3, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    instance-of v3, v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    const/4 v6, 0x0

    if-eqz v3, :cond_0

    check-cast v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v0

    iget-wide v7, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickerPackId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getHasAccess()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v8, 0x1

    goto :goto_0

    :cond_0
    const/4 v8, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getPurchasingPacks()Ljava/util/Set;

    move-result-object v0

    iget-wide v6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickerPackId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getViewedPurchaseableStickerPacks()Ljava/util/Map;

    move-result-object p1

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-static {v0, v4, v1, p1, v3}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;->access$buildStoreStickerListItems(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;Lcom/discord/models/sticker/dto/ModelStickerPack;ILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v5, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_1
    move-object v7, v0

    const-string v0, "meUser.premiumTier ?: Mo\u2026tionPlan.PremiumTier.NONE"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)V

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->getStickerPack()Lcom/discord/stores/StoreStickers$StickerPackState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->handleLoadedStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V

    :cond_0
    return-void
.end method

.method private final trackStickerPackStoreSheetViewed()V
    .locals 13

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->getType()Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const-string v0, "Sticker Pack Detail Sheet"

    goto :goto_0

    :cond_0
    const-string v0, "Sticker Pack Detail Sheet (Sticker Upsell Popout)"

    :goto_0
    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->getLocation()Ljava/lang/String;

    move-result-object v3

    new-instance v12, Lcom/discord/utilities/analytics/Traits$Location;

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->getLocation()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1d

    const/4 v11, 0x0

    move-object v4, v12

    invoke-direct/range {v4 .. v11}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v1, v2, v0, v3, v12}, Lcom/discord/utilities/analytics/AnalyticsTracker;->stickerPackViewAllViewed(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;)V

    return-void
.end method


# virtual methods
.method public final fetchStickersData()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->fetchOwnedStickerPacks()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    iget-wide v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->stickerPackId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreStickers;->fetchStickerPack(J)V

    return-void
.end method
