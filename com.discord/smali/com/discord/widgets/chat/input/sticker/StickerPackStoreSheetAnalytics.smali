.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;
.super Ljava/lang/Object;
.source "StickerPackStoreSheetViewModel.kt"


# instance fields
.field private final location:Ljava/lang/String;

.field private final section:Ljava/lang/String;

.field private final sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field private final type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;


# direct methods
.method public constructor <init>(Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "section"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->copy(Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;
    .locals 1

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "section"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    return-object v0
.end method

.method public final getSection()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    return-object v0
.end method

.method public final getSticker()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    return-object v0
.end method

.method public final getType()Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelSticker;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StickerPackStoreSheetAnalytics(sticker="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->type:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->location:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", section="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;->section:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
