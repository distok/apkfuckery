.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$observeExperiment$1;
.super Ljava/lang/Object;
.source "StickerPickerFeatureFlag.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->observeExperiment(Lcom/discord/stores/StoreExperiments;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/models/experiments/domain/Experiment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$observeExperiment$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$observeExperiment$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    const-string v1, "experiment"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->access$writeExperimentToCache(Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/models/experiments/domain/Experiment;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$observeExperiment$1;->call(Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method
