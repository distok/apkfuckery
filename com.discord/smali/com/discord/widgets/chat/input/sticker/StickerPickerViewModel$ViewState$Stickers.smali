.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;
.super Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;
.source "StickerPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Stickers"
.end annotation


# instance fields
.field private final categoryItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final isStickersSelectedTab:Z

.field private final newStickerPackCount:I

.field private final searchQuery:Ljava/lang/String;

.field private final stickerItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation
.end field

.field private final stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
            "IZ)V"
        }
    .end annotation

    const-string v0, "searchQuery"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerItems"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryItems"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerPickerScreen"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->searchQuery:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iput p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    iput-boolean p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getSearchQuery()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->copy(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZ)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getSearchQuery()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-object v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZ)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
            "IZ)",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;"
        }
    .end annotation

    const-string v0, "searchQuery"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerItems"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryItems"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerPickerScreen"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getSearchQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    iget v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    iget-boolean p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCategoryItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    return-object v0
.end method

.method public final getNewStickerPackCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    return v0
.end method

.method public getSearchQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->searchQuery:Ljava/lang/String;

    return-object v0
.end method

.method public final getStickerItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    return-object v0
.end method

.method public final getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getSearchQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isStickersSelectedTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Stickers(searchQuery="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", categoryItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->categoryItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerPickerScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->stickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newStickerPackCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->newStickerPackCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isStickersSelectedTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
