.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;
.super Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;
.source "WidgetStickerPickerSheet.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;


# instance fields
.field private container:Landroid/view/View;

.field private stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

.field private stickerPickerListenerDelegate:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;-><init>()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->container:Landroid/view/View;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "container"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setContainer$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->container:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02b6

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;->onCancel(Landroid/content/DialogInterface;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->clearSearchInput()V

    return-void

    :cond_0
    const-string/jumbo p1, "stickerPickerFragment"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public onStickerPicked(Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 1

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerListenerDelegate:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;->onStickerPicked(Lcom/discord/models/sticker/dto/ModelSticker;)V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->container:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "com.discord.intent.EXTRA_STICKER_PACK_ID"

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "com.discord.intent.EXTRA_STICKER_PICKER_SCREEN"

    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    new-instance v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-direct {v3}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;-><init>()V

    iput-object v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string/jumbo v4, "stickerPickerFragment"

    if-eqz v3, :cond_7

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v6, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->BOTTOM_SHEET:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    const-string v7, "MODE"

    invoke-virtual {v5, v7, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    sget-object v6, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;->STICKER_SEARCH_VIEW_ALL:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    const-string v7, "VIEW_TYPE"

    invoke-virtual {v5, v7, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, p2, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v5, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_3
    invoke-virtual {v3, v5}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz p1, :cond_6

    invoke-virtual {p1, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    const p2, 0x7f0a0a4f

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz v1, :cond_5

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v1, v0}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$onViewCreated$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;)V

    invoke-virtual {p1, p2}, Landroidx/fragment/app/FragmentTransaction;->runOnCommit(Ljava/lang/Runnable;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void

    :cond_4
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_7
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public final scrollToPack(Ljava/lang/Long;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerFragment:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollToPack(Ljava/lang/Long;)V

    return-void

    :cond_0
    const-string/jumbo p1, "stickerPickerFragment"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public final setStickerPickerListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->stickerPickerListenerDelegate:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    return-void
.end method
