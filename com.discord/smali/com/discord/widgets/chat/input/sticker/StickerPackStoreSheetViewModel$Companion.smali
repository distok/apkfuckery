.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;
.super Ljava/lang/Object;
.source "StickerPackStoreSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$buildStoreStickerListItems(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;Lcom/discord/models/sticker/dto/ModelStickerPack;ILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;->buildStoreStickerListItems(Lcom/discord/models/sticker/dto/ModelStickerPack;ILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final buildStoreStickerListItems(Lcom/discord/models/sticker/dto/ModelStickerPack;ILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "I",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/discord/utilities/time/Clock;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    return-object p1

    :cond_0
    invoke-interface {p4}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    sub-long v9, v1, v3

    sget-object v5, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v6

    move-object v8, p3

    invoke-virtual/range {v5 .. v10}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackNew(JLjava/util/Map;J)Z

    move-result p3

    new-instance p4, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    invoke-direct {p4, p1, p3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Z)V

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/models/sticker/dto/ModelSticker;

    new-instance p4, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    invoke-direct {p4, p3, p2, v1}, Lcom/discord/widgets/chat/input/sticker/StickerItem;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)V

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
