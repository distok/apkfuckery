.class public final Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;
.super Lx/m/c/k;
.source "StickerCategoryAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->setItems(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->access$setItems$p(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;Ljava/util/List;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_1
    if-eq v0, v2, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->access$getOnSelectedItemAdapterPositionUpdated$p(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method
