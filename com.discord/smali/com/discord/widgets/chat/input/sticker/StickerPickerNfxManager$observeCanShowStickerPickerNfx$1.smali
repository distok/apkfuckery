.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager$observeCanShowStickerPickerNfx$1;
.super Ljava/lang/Object;
.source "StickerPickerNfxManager.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->observeCanShowStickerPickerNfx()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Ljava/lang/Boolean;",
        "Lcom/discord/models/domain/ModelUser$Me;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager$observeCanShowStickerPickerNfx$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;Lcom/discord/models/domain/ModelUser$Me;)Ljava/lang/Boolean;
    .locals 1

    const-string v0, "isHomeInitialized"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager$observeCanShowStickerPickerNfx$1;->this$0:Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;

    invoke-static {p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->access$canShowStickerPickerNfx(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/models/domain/ModelUser;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    check-cast p2, Lcom/discord/models/domain/ModelUser$Me;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager$observeCanShowStickerPickerNfx$1;->call(Ljava/lang/Boolean;Lcom/discord/models/domain/ModelUser$Me;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
