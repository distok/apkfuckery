.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;
.super Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;
.source "StickerPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final frequentlyUsedStickers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation
.end field

.field private final isStickersSelectedTab:Z

.field private final meUser:Lcom/discord/models/domain/ModelUser;

.field private final newStickerPackCount:I

.field private final ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

.field private final searchInputStringUpper:Ljava/lang/String;

.field private final selectedPackId:Ljava/lang/Long;

.field private final stickerAnimationSettings:I

.field private final stickerPackStoreDirectory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation
.end field

.field private final viewedPurchaseableStickerPackIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "IZ)V"
        }
    .end annotation

    const-string/jumbo v0, "stickerPackStoreDirectory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownedStickerPackState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "frequentlyUsedStickers"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewedPurchaseableStickerPackIds"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchInputStringUpper"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUser"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    iput p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    iput-object p7, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    iput-object p8, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    iput p9, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    iput-boolean p10, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;IZILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-boolean v1, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    goto :goto_9

    :cond_9
    move/from16 v1, p10

    :goto_9
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->copy(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;IZ)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    return v0
.end method

.method public final component2()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    return v0
.end method

.method public final component7()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component8()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    return v0
.end method

.method public final copy(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;IZ)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "IZ)",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;"
        }
    .end annotation

    const-string/jumbo v0, "stickerPackStoreDirectory"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownedStickerPackState"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "frequentlyUsedStickers"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewedPurchaseableStickerPackIds"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchInputStringUpper"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUser"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    move-object v1, v0

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;-><init>(Ljava/util/List;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/domain/ModelUser;IZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    iget v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    iget v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    iget-boolean p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFrequentlyUsedStickers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    return-object v0
.end method

.method public final getMeUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getNewStickerPackCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    return v0
.end method

.method public final getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-object v0
.end method

.method public final getSearchInputStringUpper()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelectedPackId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getStickerAnimationSettings()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    return v0
.end method

.method public final getStickerPackStoreDirectory()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    return-object v0
.end method

.method public final getViewedPurchaseableStickerPackIds()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public final isStickersSelectedTab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(stickerPackStoreDirectory="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerPackStoreDirectory:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ownedStickerPackState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", frequentlyUsedStickers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->frequentlyUsedStickers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", viewedPurchaseableStickerPackIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->viewedPurchaseableStickerPackIds:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchInputStringUpper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->searchInputStringUpper:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerAnimationSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->stickerAnimationSettings:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", selectedPackId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->selectedPackId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", meUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newStickerPackCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->newStickerPackCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isStickersSelectedTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
