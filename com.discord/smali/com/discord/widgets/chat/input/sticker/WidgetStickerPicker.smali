.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;
.super Lcom/discord/widgets/chat/input/sticker/StickerPicker;
.source "WidgetStickerPicker.kt"

# interfaces
.implements Lf/b/a/e/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final ARG_MODE:Ljava/lang/String; = "MODE"

.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$Companion;

.field private static final STICKER_PICKER_VIEW_FLIPPER_EMPTY_STATE:I = 0x1

.field private static final STICKER_PICKER_VIEW_FLIPPER_RESULTS:I = 0x0

.field public static final VIEW_TYPE:Ljava/lang/String; = "VIEW_TYPE"


# instance fields
.field private autoscrollToPackId:Ljava/lang/Long;

.field private categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

.field private categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private final categoryRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

.field private final emptyImage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptyLink$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptySubtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptyTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

.field private final floatingViewManager:Lcom/discord/floating_view_manager/FloatingViewManager;

.field private onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

.field private onStickerSearchOpenedListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private previousViewState:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;

.field private restoredSearchQueryFromViewModel:Z

.field private scrollExpressionPickerToTop:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final searchClearButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final shopHighlight$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final shopIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final shopIconContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private showSearchBar:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private sparkleView:Lcom/discord/tooltips/SparkleView;

.field private stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

.field private final stickerAppBar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerCategoryScrollSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

.field private stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

.field private final stickerRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerToolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerViewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final storeTooltipScrollListener:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;

.field private viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

.field private wasActive:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x10

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string/jumbo v6, "stickerAppBar"

    const-string v7, "getStickerAppBar()Lcom/google/android/material/appbar/AppBarLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "emptyImage"

    const-string v7, "getEmptyImage()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "emptyTitle"

    const-string v7, "getEmptyTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "emptySubtitle"

    const-string v7, "getEmptySubtitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "emptyLink"

    const-string v7, "getEmptyLink()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string/jumbo v6, "stickerToolbar"

    const-string v7, "getStickerToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string/jumbo v6, "stickerViewFlipper"

    const-string v7, "getStickerViewFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string/jumbo v6, "stickerRecycler"

    const-string v7, "getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "shopIconContainer"

    const-string v7, "getShopIconContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "shopIcon"

    const-string v7, "getShopIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "shopHighlight"

    const-string v7, "getShopHighlight()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "searchInput"

    const-string v7, "getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "searchContainer"

    const-string v7, "getSearchContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "searchClearButton"

    const-string v7, "getSearchClearButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const-string v6, "categoryRecycler"

    const-string v7, "getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPicker;-><init>()V

    const v0, 0x7f0a0a44

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a32

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAppBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0230

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptyImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0233

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptyTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0232

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptySubtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0231

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptyLink$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a5f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerToolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0235

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerViewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0234

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a48

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->shopIconContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a47

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->shopIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a46

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->shopHighlight$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a4b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->searchInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a49

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->searchContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a4a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->searchClearButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a43

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$scrollExpressionPickerToTop$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$scrollExpressionPickerToTop$1;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollExpressionPickerToTop:Lkotlin/jvm/functions/Function0;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerCategoryScrollSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "logger"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_0

    :cond_0
    move-object v2, v3

    :goto_0
    if-nez v2, :cond_1

    new-instance v2, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v2, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    move-object v8, v2

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string v0, "floatingViewManager"

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/tooltips/TooltipManager;

    goto :goto_1

    :cond_2
    move-object v0, v3

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Lcom/discord/tooltips/TooltipManager;

    sget-object v2, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {v2}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lf/a/l/a;

    sget-object v2, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {v2}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Ljava/util/Set;

    const/4 v7, 0x0

    const/4 v9, 0x4

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    :cond_3
    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    new-instance v2, Lcom/discord/tooltips/DefaultTooltipCreator;

    invoke-direct {v2, v0}, Lcom/discord/tooltips/DefaultTooltipCreator;-><init>(Lcom/discord/tooltips/TooltipManager;)V

    iput-object v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/floating_view_manager/FloatingViewManager;

    :cond_4
    if-nez v3, :cond_5

    new-instance v3, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v3, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_5
    iput-object v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->floatingViewManager:Lcom/discord/floating_view_manager/FloatingViewManager;

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->storeTooltipScrollListener:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->configureUI(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getAdditionalBottomPaddingPx(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)I
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getAdditionalBottomPaddingPx()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getSearchInput$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Lcom/google/android/material/textfield/TextInputEditText;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStickerAdapter$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "stickerAdapter"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getStickerCategoryScrollSubject$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerCategoryScrollSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$getStickerPickerMode$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    return-object p0
.end method

.method public static final synthetic access$getStickerRecycler$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTooltipManager$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Lcom/discord/tooltips/TooltipManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    return-object p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->handleEvent(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$handleInputChanged(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->handleInputChanged(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleNewStickerRecyclerScrollPosition(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;ILjava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->handleNewStickerRecyclerScrollPosition(ILjava/util/List;)V

    return-void
.end method

.method public static final synthetic access$launchBottomSheet(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->launchBottomSheet()V

    return-void
.end method

.method public static final synthetic access$onBuyButtonClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onBuyButtonClicked(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-void
.end method

.method public static final synthetic access$onPackClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onPackClicked(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V

    return-void
.end method

.method public static final synthetic access$onRecentClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onRecentClicked()V

    return-void
.end method

.method public static final synthetic access$onSelectedCategoryAdapterPositionUpdated(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onSelectedCategoryAdapterPositionUpdated(I)V

    return-void
.end method

.method public static final synthetic access$onStickerHeaderItemsClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onStickerHeaderItemsClicked(Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V

    return-void
.end method

.method public static final synthetic access$onStickerItemSelected(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StickerItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onStickerItemSelected(Lcom/discord/widgets/chat/input/sticker/StickerItem;)V

    return-void
.end method

.method public static final synthetic access$onStoreClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onStoreClicked()V

    return-void
.end method

.method public static final synthetic access$setStickerAdapter$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    return-void
.end method

.method public static final synthetic access$setStickerPickerMode$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    return-void
.end method

.method public static final synthetic access$showStickerPackStoreTooltip(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showStickerPackStoreTooltip()V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;)V
    .locals 12

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->restoredSearchQueryFromViewModel:Z

    const/4 v3, 0x1

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->restoredSearchQueryFromViewModel:Z

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-nez p1, :cond_2

    return-void

    :cond_2
    instance-of v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$NoOwnedPacks;

    const-string v2, "categoryAdapter"

    const/4 v4, 0x0

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerViewFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showSearchBar:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_3

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v1, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Unit;

    :cond_3
    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollExpressionPickerToTop:Lkotlin/jvm/functions/Function0;

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyImage()Landroid/widget/ImageView;

    move-result-object v1

    const v3, 0x7f080409

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyTitle()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f121711

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptySubtitle()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f121710

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyLink()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyLink()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    const-string v3, "requireContext()"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f12170f

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v6, "getString(R.string.stick\u2026ty_state_subtitle_browse)"

    invoke-static {v3, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "["

    const-string v7, ""

    const/4 v8, 0x4

    invoke-static {v3, v6, v7, v4, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v3

    const-string v6, "]"

    invoke-static {v3, v6, v7, v4, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v3

    const-string v6, "(onBrowseStickerPacks)"

    invoke-static {v3, v6, v7, v4, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1c

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyLink()Landroid/widget/TextView;

    move-result-object v1

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$configureUI$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$configureUI$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    if-eqz v1, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$NoOwnedPacks;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$NoOwnedPacks;->getCategoryItems()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->setItems(Ljava/util/List;)V

    goto/16 :goto_a

    :cond_4
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_5
    instance-of v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;

    const/16 v5, 0x8

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerViewFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showSearchBar:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_6

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v1, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Unit;

    :cond_6
    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollExpressionPickerToTop:Lkotlin/jvm/functions/Function0;

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyImage()Landroid/widget/ImageView;

    move-result-object v1

    const v3, 0x7f080541

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyTitle()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptySubtitle()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f12111b

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getEmptyLink()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    if-eqz v1, :cond_7

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;->getCategoryItems()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->setItems(Ljava/util/List;)V

    goto/16 :goto_a

    :cond_7
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_8
    instance-of v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-eqz v1, :cond_1c

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerViewFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showSearchBar:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_a

    move-object v6, p1

    check-cast v6, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v6

    sget-object v7, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    if-ne v6, v7, :cond_9

    const/4 v6, 0x1

    goto :goto_1

    :cond_9
    const/4 v6, 0x0

    :goto_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v1, v6}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Unit;

    :cond_a
    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->previousViewState:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;

    instance-of v6, v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-nez v6, :cond_b

    move-object v1, v0

    :cond_b
    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v1

    goto :goto_2

    :cond_c
    move-object v1, v0

    :goto_2
    move-object v6, p1

    check-cast v6, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v7

    if-eq v1, v7, :cond_d

    const/4 v1, 0x1

    goto :goto_3

    :cond_d
    const/4 v1, 0x0

    :goto_3
    iget-object v7, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    const-string/jumbo v8, "stickerAdapter"

    if-eqz v7, :cond_1b

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerItems()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setData(Ljava/util/List;)V

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getNewStickerPackCount()I

    move-result v7

    if-lez v7, :cond_e

    const/4 v7, 0x1

    goto :goto_4

    :cond_e
    const/4 v7, 0x0

    :goto_4
    if-eqz v7, :cond_f

    const v9, 0x7f080445

    goto :goto_5

    :cond_f
    const v9, 0x7f080444

    :goto_5
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getShopIcon()Landroid/widget/ImageView;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    if-eqz v7, :cond_10

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->isStickersSelectedTab()Z

    move-result v9

    if-eqz v9, :cond_10

    const/4 v9, 0x1

    goto :goto_6

    :cond_10
    const/4 v9, 0x0

    :goto_6
    invoke-direct {p0, v9}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showShopIconSparkles(Z)V

    if-eqz v1, :cond_11

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerAppBar()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/material/appbar/AppBarLayout;->setExpanded(Z)V

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v1

    sget-object v9, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    if-ne v1, v9, :cond_11

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollToTop()V

    :cond_11
    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_17

    if-eq v1, v3, :cond_12

    goto/16 :goto_9

    :cond_12
    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v7, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;

    invoke-virtual {v1, v7}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getShopHighlight()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchContainer()Landroid/view/View;

    move-result-object v1

    iget-object v7, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    sget-object v9, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-eq v7, v9, :cond_13

    goto :goto_7

    :cond_13
    const/4 v3, 0x0

    :goto_7
    if-eqz v3, :cond_14

    goto :goto_8

    :cond_14
    const/16 v4, 0x8

    :goto_8
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    if-eqz v1, :cond_16

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$configureUI$2;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$configureUI$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;)V

    invoke-virtual {v1, v3}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setOnScrollPositionListener(Lkotlin/jvm/functions/Function1;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    if-eqz v1, :cond_15

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->storeTooltipScrollListener:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    goto :goto_9

    :cond_15
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_16
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_17
    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->cacheViewedPurchaseableStickerPacks()V

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v1, v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->expressionPickerStickerShopViewed(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showStickerPackStoreTooltip()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getShopHighlight()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchContainer()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    if-eqz v1, :cond_19

    sget-object v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$configureUI$3;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$configureUI$3;

    invoke-virtual {v1, v3}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setOnScrollPositionListener(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->storeTooltipScrollListener:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    :goto_9
    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    if-eqz v1, :cond_18

    invoke-virtual {v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getCategoryItems()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->setItems(Ljava/util/List;)V

    goto :goto_a

    :cond_18
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_19
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1a
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1b
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1c
    :goto_a
    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->previousViewState:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;

    return-void
.end method

.method private final getAdditionalBottomPaddingPx()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getEmptyImage()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptyImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getEmptyLink()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptyLink$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmptySubtitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptySubtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmptyTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->emptyTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMode()Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "MODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    :goto_2
    return-object v1
.end method

.method private final getSearchClearButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->searchClearButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getSearchContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->searchContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->searchInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputEditText;

    return-object v0
.end method

.method private final getShopHighlight()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->shopHighlight$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getShopIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->shopIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getShopIconContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->shopIconContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStickerAppBar()Lcom/google/android/material/appbar/AppBarLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAppBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/appbar/AppBarLayout;

    return-object v0
.end method

.method private final getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getStickerToolbar()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerToolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final getStickerViewFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerViewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getViewType()Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "VIEW_TYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;->STICKER_PICKER_VIEW_ALL:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    :goto_2
    return-object v1
.end method

.method private final handleEvent(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;->getPosition()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->scrollToPosition(I)V

    goto :goto_0

    :cond_0
    const-string/jumbo p1, "stickerAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleInputChanged(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->setSearchText(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchClearButton()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_0

    const v1, 0x7f080412

    goto :goto_0

    :cond_0
    const v1, 0x7f0802b8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchClearButton()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040178

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040179

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchClearButton()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_2

    const p1, 0x7f121612

    goto :goto_2

    :cond_2
    const p1, 0x7f121539

    :goto_2
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final handleNewStickerRecyclerScrollPosition(ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;

    instance-of v1, v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getCategoryRange()Lkotlin/Pair;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    if-le v3, p1, :cond_1

    goto :goto_0

    :cond_1
    if-le v2, p1, :cond_0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->autoscrollToPackId:Ljava/lang/Long;

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v3

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->autoscrollToPackId:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->selectPackById(J)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private final initializeInputButtons()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getShopIconContainer()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$initializeInputButtons$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$initializeInputButtons$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final initializeSearchBar()V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerAppBar()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/16 v5, 0x8

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchContainer()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-eq v1, v2, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchContainer()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/4 v3, 0x0

    :goto_4
    if-eqz v3, :cond_5

    goto :goto_5

    :cond_5
    const/16 v4, 0x8

    :goto_5
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchClearButton()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$initializeSearchBar$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$initializeSearchBar$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/discord/utilities/view/text/TextWatcher;->Companion:Lcom/discord/utilities/view/text/TextWatcher$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$initializeSearchBar$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$initializeSearchBar$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/discord/utilities/view/text/TextWatcher$Companion;->addBindedTextWatcher(Landroidx/fragment/app/Fragment;Landroid/widget/TextView;Lrx/functions/Action1;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->BOTTOM_SHEET:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-ne v0, v1, :cond_6

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getSearchInput()Lcom/google/android/material/textfield/TextInputEditText;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->showKeyboard(Landroid/view/View;)V

    :cond_6
    return-void
.end method

.method private final launchBottomSheet()V
    .locals 8

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;->show$default(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    return-void
.end method

.method private final onBuyButtonClicked(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 14

    move-object v0, p0

    :try_start_0
    sget-object v1, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    const-string v4, "parentFragmentManager"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v13, Lcom/discord/utilities/analytics/Traits$Location;

    const/4 v6, 0x0

    sget-object v4, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->EXPRESSION_PICKER:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v4}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1d

    const/4 v12, 0x0

    move-object v5, v13

    invoke-direct/range {v5 .. v12}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object v7, v13

    invoke-virtual/range {v1 .. v7}, Lcom/discord/utilities/dsti/StickerUtils;->claimOrPurchaseStickerPack(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/utilities/analytics/Traits$Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const v1, 0x7f1206e8

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-static {p0, v1, v2, v3}, Lf/a/b/p;->l(Landroidx/fragment/app/Fragment;Ljava/lang/CharSequence;II)V

    :goto_0
    return-void
.end method

.method private final onPackClicked(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V
    .locals 12

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->stickerPackCategorySelected(J)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->autoscrollToPackId:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->selectPackById(J)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getCategoryRange()Lkotlin/Pair;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-wide/16 v2, 0xc8

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.just(Unit)\n  \u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$onPackClicked$1;

    invoke-direct {v9, p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$onPackClicked$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lkotlin/Pair;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final onRecentClicked()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->selectPackById(J)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->scrollToPosition(I)V

    return-void

    :cond_0
    const-string/jumbo v0, "stickerAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final onSelectedCategoryAdapterPositionUpdated(I)V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    const-string v1, "categoryLayoutManager"

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v1

    sub-int v3, v1, v0

    new-instance v4, Lkotlin/ranges/IntRange;

    invoke-direct {v4, v0, v1}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-virtual {v4, p1}, Lkotlin/ranges/IntRange;->contains(I)Z

    move-result v1

    if-nez v1, :cond_2

    if-ge p1, v0, :cond_0

    sub-int/2addr p1, v3

    goto :goto_0

    :cond_0
    add-int/2addr p1, v3

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_1

    :cond_1
    const-string p1, "categoryAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final onStickerHeaderItemsClicked(Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog;->Companion:Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "childFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog$Companion;->show$default(Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog$Companion;Landroidx/fragment/app/FragmentManager;JLjava/lang/Integer;ILjava/lang/Object;)V

    return-void
.end method

.method private final onStickerItemSelected(Lcom/discord/widgets/chat/input/sticker/StickerItem;)V
    .locals 8

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->getMode()Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;

    invoke-virtual {v0, v1}, Lcom/discord/tooltips/TooltipManager;->a(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    const-string v0, "childFragmentManager"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v4

    sget-object v7, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->EXPRESSION_PICKER:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getViewType()Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    move-result-object v5

    const-string v6, "Expression Picker"

    invoke-virtual/range {v2 .. v7}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Lcom/discord/widgets/stickers/StickerPurchaseLocation;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;->onStickerPicked(Lcom/discord/models/sticker/dto/ModelSticker;)V

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->onStickerSelected(Lcom/discord/models/sticker/dto/ModelSticker;)V

    :goto_0
    return-void

    :cond_3
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final onStoreClicked()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->gc()V

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->selectPackById(J)V

    return-void
.end method

.method private final selectPackById(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->setSelectedPackId(J)V

    return-void

    :cond_0
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final setUpCategoryRecycler()V
    .locals 11

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    new-instance v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    new-instance v5, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$3;

    invoke-direct {v5, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$3;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    const/4 v6, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v2, v0

    move-object v7, p0

    invoke-direct/range {v2 .. v9}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    const-string v2, "categoryAdapter"

    if-eqz v0, :cond_3

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance v0, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    iget-object v6, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryAdapter:Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;

    if-eqz v6, :cond_1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v10}, Lcom/discord/utilities/recycler/SelfHealingLinearLayoutManager;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$Adapter;IZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->categoryLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getCategoryRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$4;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpCategoryRecycler$4;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void

    :cond_0
    const-string v0, "categoryLayoutManager"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setUpStickerRecycler()V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    new-instance v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    new-instance v5, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$3;

    invoke-direct {v5, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$3;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerAdapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    new-instance v2, Lcom/discord/utilities/views/StickyHeaderItemDecoration;

    if-eqz v0, :cond_0

    invoke-direct {v2, v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;-><init>(Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/discord/utilities/views/StickyHeaderItemDecoration;->blockClicks(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setRecyclerListener(Landroidx/recyclerview/widget/RecyclerView$RecyclerListener;)V

    return-void

    :cond_0
    const-string/jumbo v0, "stickerAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setWindowInsetsListeners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getContainer()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setWindowInsetsListeners$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setWindowInsetsListeners$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method

.method private final showShopIconSparkles(Z)V
    .locals 9

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->sparkleView:Lcom/discord/tooltips/SparkleView;

    if-nez p1, :cond_0

    new-instance p1, Lcom/discord/tooltips/SparkleView;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "container.context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f080580

    invoke-direct {p1, v0, v1}, Lcom/discord/tooltips/SparkleView;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->sparkleView:Lcom/discord/tooltips/SparkleView;

    :cond_0
    iget-object v4, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->sparkleView:Lcom/discord/tooltips/SparkleView;

    if-eqz v4, :cond_2

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->floatingViewManager:Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getShopIcon()Landroid/widget/ImageView;

    move-result-object v3

    sget-object v5, Lcom/discord/floating_view_manager/FloatingViewGravity;->CENTER:Lcom/discord/floating_view_manager/FloatingViewGravity;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$showShopIconSparkles$1$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$showShopIconSparkles$1$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v8

    const-string/jumbo p1, "unsubscribeSignal.map { }"

    invoke-static {v8, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {v2 .. v8}, Lcom/discord/floating_view_manager/FloatingViewManager;->c(Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;IILrx/Observable;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->sparkleView:Lcom/discord/tooltips/SparkleView;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->floatingViewManager:Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getId()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/floating_view_manager/FloatingViewManager;->b(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final showStickerPackStoreTooltip()V
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    sget-object v3, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f12171f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "resources.getString(R.st\u2026_view_all_tooltip_mobile)"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/floating_view_manager/FloatingViewGravity;->TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;

    const/16 v5, 0x3c

    invoke-static {v5}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v5

    sget-object v7, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$showStickerPackStoreTooltip$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$showStickerPackStoreTooltip$1;

    invoke-virtual {v5, v7}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v8

    const-string/jumbo v5, "this.unsubscribeSignal.map { }"

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v8}, Lcom/discord/tooltips/DefaultTooltipCreator;->a(Landroid/view/View;Ljava/lang/String;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V

    return-void
.end method


# virtual methods
.method public final clearSearchInput()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->setSearchText(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02b5

    return v0
.end method

.method public isShown(Z)V
    .locals 7

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->wasActive:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerAppBar()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/material/appbar/AppBarLayout;->setExpanded(Z)V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollToTop()V

    :cond_0
    if-nez p1, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_1

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->setSelectedPackId(J)V

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->clearSearchInput()V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_0
    .catch Lkotlin/UninitializedPropertyAccessException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    throw p1

    :catch_0
    move-exception v0

    move-object v3, v0

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v2, "isShown called before WidgetStickerPicker viewmodel was initialized"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :cond_2
    :goto_0
    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->wasActive:Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 28

    move-object/from16 v0, p0

    invoke-super/range {p0 .. p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/System;->gc()V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getMode()Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;

    invoke-direct {v3}, Lcom/discord/widgets/chat/input/AppFlexInputViewModel$FlexInputViewModelFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v2, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-virtual {v1, v2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v1

    const-string v2, "ViewModelProvider(\n     \u2026putViewModel::class.java)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    iput-object v1, v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->flexInputViewModel:Lcom/discord/widgets/chat/input/AppFlexInputViewModel;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getMode()Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-ne v1, v2, :cond_0

    const-class v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerInlineViewModel;

    goto :goto_0

    :cond_0
    const-class v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerSheetViewModel;

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const-string v4, "com.discord.intent.EXTRA_STICKER_PICKER_SCREEN"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    instance-of v4, v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    if-nez v4, :cond_2

    move-object v2, v3

    :cond_2
    check-cast v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_3

    const-string v3, "com.discord.intent.EXTRA_STICKER_PACK_ID"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :cond_3
    new-instance v4, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v5

    new-instance v15, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getMode()Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    move-result-object v7

    new-instance v6, Lcom/discord/utilities/locale/LocaleManager;

    invoke-direct {v6}, Lcom/discord/utilities/locale/LocaleManager;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/discord/utilities/locale/LocaleManager;->getPrimaryLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v8

    new-instance v9, Lcom/discord/widgets/chat/MessageManager;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    const-string v10, "requireContext()"

    invoke-static {v6, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0xfe

    const/16 v26, 0x0

    move-object/from16 v16, v9

    move-object/from16 v17, v6

    invoke-direct/range {v16 .. v26}, Lcom/discord/widgets/chat/MessageManager;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    if-eqz v2, :cond_4

    goto :goto_2

    :cond_4
    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    :goto_2
    move-object/from16 v17, v2

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_3

    :cond_5
    const-wide/16 v2, -0x1

    :goto_3
    const/16 v18, 0x0

    const/16 v19, 0x4f8

    const/16 v20, 0x0

    move-object v6, v15

    move-object/from16 v27, v15

    move-wide v15, v2

    invoke-direct/range {v6 .. v20}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/subjects/BehaviorSubject;JLcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lrx/subjects/BehaviorSubject;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v2, v27

    invoke-direct {v4, v5, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    invoke-virtual {v4, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    iput-object v1, v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getContainer()Landroid/view/View;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getAdditionalBottomPaddingPx()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->setPadding(IIII)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getMode()Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setWindowInsetsListeners()V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->initializeInputButtons()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->initializeSearchBar()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerToolbar()Landroidx/appcompat/widget/Toolbar;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    const-string v1, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout.LayoutParams"

    invoke-static {p1, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/google/android/material/appbar/AppBarLayout$LayoutParams;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-ne v1, v0, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/material/appbar/AppBarLayout$LayoutParams;->setScrollFlags(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setUpStickerRecycler()V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setUpCategoryRecycler()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_2

    sget-object v3, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->Companion:Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07007f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    const/4 v6, 0x4

    invoke-virtual {v3, v4, v5, v6}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;->calculateNumOfColumns(Landroidx/recyclerview/widget/RecyclerView;FI)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->setStickerCountToDisplayForStore(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final scrollToPack(Ljava/lang/Long;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->scrollToPackId(Ljava/lang/Long;)V

    return-void

    :cond_0
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public final scrollToTop()V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->getStickerRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$scrollToTop$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$scrollToTop$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public setListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->stickerPickerListener:Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;

    return-void
.end method

.method public setOnBackspacePressedListener(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onBackspacePressedListener:Lcom/discord/widgets/chat/input/OnBackspacePressedListener;

    return-void
.end method

.method public setOnStickerSearchOpenedListener(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->onStickerSearchOpenedListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setScrollExpressionPickerToTop(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "scrollExpressionPickerToTop"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->scrollExpressionPickerToTop:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setShowSearchBar(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->showSearchBar:Lkotlin/jvm/functions/Function1;

    return-void
.end method
