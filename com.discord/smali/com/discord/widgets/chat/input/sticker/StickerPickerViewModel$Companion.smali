.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;
.super Ljava/lang/Object;
.source "StickerPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$buildOwnedStickerListItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/lang/String;ILjava/util/Locale;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;->buildOwnedStickerListItems(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/lang/String;ILjava/util/Locale;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$buildStoreStickerListItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;Lcom/discord/models/sticker/dto/ModelStickerPack;ILcom/discord/models/domain/ModelUser;ZZILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;->buildStoreStickerListItems(Lcom/discord/models/sticker/dto/ModelStickerPack;ILcom/discord/models/domain/ModelUser;ZZILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final buildOwnedStickerListItems(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/lang/String;ILjava/util/Locale;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "null cannot be cast to non-null type java.lang.String"

    invoke-static {v6, v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v6, p4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x2

    invoke-static {v6, p2, v5, v8}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getTags()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6, p4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, p2, v5, v8}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v5

    if-ne v5, v4, :cond_1

    :cond_2
    new-instance v4, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    sget-object v5, Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;->OWNED:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    invoke-direct {v4, v3, p3, v5}, Lcom/discord/widgets/chat/input/sticker/StickerItem;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-lez p2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_5

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    return-object v0

    :cond_5
    new-instance p2, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;

    new-instance p3, Lcom/discord/widgets/chat/input/sticker/HeaderType$OwnedPackItem;

    invoke-direct {p3, p1}, Lcom/discord/widgets/chat/input/sticker/HeaderType$OwnedPackItem;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-direct {p2, p3}, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;-><init>(Lcom/discord/widgets/chat/input/sticker/HeaderType;)V

    invoke-virtual {v1, v5, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-object v1
.end method

.method private final buildStoreStickerListItems(Lcom/discord/models/sticker/dto/ModelStickerPack;ILcom/discord/models/domain/ModelUser;ZZILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "I",
            "Lcom/discord/models/domain/ModelUser;",
            "ZZI",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/discord/utilities/time/Clock;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    move-object v0, p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    return-object v0

    :cond_0
    invoke-interface/range {p8 .. p8}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    sub-long v10, v2, v4

    sget-object v6, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v7

    move-object/from16 v9, p7

    invoke-virtual/range {v6 .. v11}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackNew(JLjava/util/Map;J)Z

    move-result v2

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    invoke-direct {v3, p1, v2}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v2

    move/from16 v3, p6

    invoke-static {v2, v3}, Lx/h/f;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/sticker/dto/ModelSticker;

    new-instance v4, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    sget-object v5, Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    move v6, p2

    invoke-direct {v4, v3, p2, v5}, Lcom/discord/widgets/chat/input/sticker/StickerItem;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelUser;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_1
    const-string v4, "meUser.premiumTier ?: Mo\u2026tionPlan.PremiumTier.NONE"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ZZ)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v1
.end method
