.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerInlineViewModel;
.super Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;
.source "StickerPickerInlineViewModel.kt"


# direct methods
.method public constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;J)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Locale;",
            "Lcom/discord/widgets/chat/MessageManager;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
            "J)V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedPackIdSubject"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageManager"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialStickerPickerScreen"

    move-object/from16 v8, p6

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v7

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xf00

    const/16 v16, 0x0

    move-object/from16 v1, p0

    move-wide/from16 v9, p7

    invoke-direct/range {v1 .. v16}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;JLcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
