.class public final Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;
.super Lx/m/c/k;
.source "StickerAdapterViewHolders.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

.field public final synthetic $isFreeForNonNitro:Z

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->$data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->$isFreeForNonNitro:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->invoke(Ljava/util/Set;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "packsBeingPurchased"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->$data:Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->access$getBuy$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/views/LoadingButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->access$getBuyPremium$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/views/LoadingButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->this$0:Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;->access$getBuyPremium$p(Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder;)Lcom/discord/views/LoadingButton;

    move-result-object p1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreBuyButtonViewHolder$onConfigure$4;->$isFreeForNonNitro:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/views/LoadingButton;->setIconVisibility(Z)V

    return-void
.end method
