.class public final enum Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
.super Ljava/lang/Enum;
.source "StickerPickerScreen.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

.field public static final enum OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

.field public static final enum STORE:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    const-string v2, "STORE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    const-string v2, "OWNED_PACKS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->$VALUES:[Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
    .locals 1

    const-class v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->$VALUES:[Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-virtual {v0}, [Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    return-object v0
.end method
