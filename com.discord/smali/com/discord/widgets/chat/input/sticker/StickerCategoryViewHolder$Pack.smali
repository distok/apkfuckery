.class public final Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;
.super Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;
.source "StickerCategoryViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pack"
.end annotation


# instance fields
.field private final selectionOverline:Landroid/view/View;

.field private final stickerView:Lcom/discord/views/sticker/StickerView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    const v0, 0x7f0a0a33

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->stickerView:Lcom/discord/views/sticker/StickerView;

    const v0, 0x7f0a03ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->selectionOverline:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "packItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPackClicked"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->stickerView:Lcom/discord/views/sticker/StickerView;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getCoverSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->stickerView:Lcom/discord/views/sticker/StickerView;

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack$configure$1;

    invoke-direct {v1, p2, p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack$configure$1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/sticker/StickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->stickerView:Lcom/discord/views/sticker/StickerView;

    invoke-virtual {p2}, Lcom/discord/views/sticker/StickerView;->g()V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->stickerView:Lcom/discord/views/sticker/StickerView;

    const-string/jumbo v0, "stickerView"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->selectionOverline:Landroid/view/View;

    const-string v0, "selectionOverline"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->isSelected()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
