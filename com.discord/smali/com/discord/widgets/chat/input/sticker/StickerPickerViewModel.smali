.class public Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;
.super Lf/a/b/l0;
.source "StickerPickerViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;,
        Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;,
        Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;,
        Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;,
        Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;

.field public static final RECENT_SELECTED_ID:J = -0x1L

.field public static final STORE_SELECTED_ID:J


# instance fields
.field private final channelSelectedStore:Lcom/discord/stores/StoreChannelsSelected;

.field private final channelStore:Lcom/discord/stores/StoreChannels;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final initialStickerPackId:J

.field private final initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

.field private final locale:Ljava/util/Locale;

.field private final messageManager:Lcom/discord/widgets/chat/MessageManager;

.field private final permissionStore:Lcom/discord/stores/StorePermissions;

.field private final searchSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedPackIdSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private stickerCountToDisplayForStore:I

.field private final stickersStore:Lcom/discord/stores/StoreStickers;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;JLcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Locale;",
            "Lcom/discord/widgets/chat/MessageManager;",
            "Lcom/discord/stores/StoreStickers;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
            "J",
            "Lcom/discord/utilities/time/Clock;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/stores/StoreChannelsSelected;",
            "Lcom/discord/stores/StorePermissions;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedPackIdSubject"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageManager"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickersStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialStickerPickerScreen"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelStore"

    invoke-static {p11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelSelectedStore"

    invoke-static {p12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionStore"

    invoke-static {p13, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->selectedPackIdSubject:Lrx/subjects/BehaviorSubject;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->locale:Ljava/util/Locale;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    iput-object p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    iput-object p7, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iput-wide p8, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->initialStickerPackId:J

    iput-object p10, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p11, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->channelStore:Lcom/discord/stores/StoreChannels;

    iput-object p12, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->channelSelectedStore:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p13, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->permissionStore:Lcom/discord/stores/StorePermissions;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    const-string p3, "PublishSubject.create()"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 p2, 0x4

    iput p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->stickerCountToDisplayForStore:I

    invoke-virtual {p6}, Lcom/discord/stores/StoreStickers;->fetchOwnedStickerPacks()V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p2

    const-string/jumbo p1, "storeStateObservable\n   \u2026  .distinctUntilChanged()"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    new-instance p8, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$1;

    invoke-direct {p8, p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;)V

    const/4 p4, 0x0

    const/4 p5, 0x0

    const/4 p6, 0x0

    const/4 p7, 0x0

    const/16 p9, 0x1e

    const/4 p10, 0x0

    invoke-static/range {p2 .. p10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;JLcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 16

    move/from16 v0, p14

    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v1

    move-object v12, v1

    goto :goto_0

    :cond_0
    move-object/from16 v12, p10

    :goto_0
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    move-object v13, v1

    goto :goto_1

    :cond_1
    move-object/from16 v13, p11

    :goto_1
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    move-object v14, v1

    goto :goto_2

    :cond_2
    move-object/from16 v14, p12

    :goto_2
    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    move-object v15, v0

    goto :goto_3

    :cond_3
    move-object/from16 v15, p13

    :goto_3
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-wide/from16 v10, p8

    invoke-direct/range {v2 .. v15}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;JLcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->handleStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;)V

    return-void
.end method

.method private final createCategoryItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;Ljava/util/List;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSelectedPackId()Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-nez p2, :cond_1

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    instance-of v9, v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-nez v9, :cond_0

    const/4 v9, 0x0

    goto :goto_0

    :cond_0
    move-object v9, v0

    :goto_0
    check-cast v9, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-eqz v9, :cond_8

    invoke-virtual {v9}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v9

    if-eqz v9, :cond_8

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    if-eqz v9, :cond_8

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    instance-of v11, v10, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    if-eqz v11, :cond_2

    check-cast v10, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    invoke-virtual {v10}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getFrequentlyUsedStickers()Ljava/util/List;

    move-result-object v11

    instance-of v12, v11, Ljava/util/Collection;

    if-eqz v12, :cond_4

    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_4

    :cond_3
    const/4 v11, 0x0

    goto :goto_4

    :cond_4
    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v12}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v12

    invoke-virtual {v10}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v14

    cmp-long v16, v12, v14

    if-nez v16, :cond_6

    const/4 v12, 0x1

    goto :goto_3

    :cond_6
    const/4 v12, 0x0

    :goto_3
    if-eqz v12, :cond_5

    const/4 v11, 0x1

    :goto_4
    if-eqz v11, :cond_7

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {v10}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getFrequentlyUsedStickers()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_9

    const/4 v9, 0x0

    goto :goto_5

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getFrequentlyUsedStickers()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/2addr v9, v7

    :goto_5
    add-int/2addr v9, v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getFrequentlyUsedStickers()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    xor-int/2addr v10, v7

    if-eqz v10, :cond_c

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    new-instance v6, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    if-nez v2, :cond_a

    goto :goto_6

    :cond_a
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v12, v10, v4

    if-nez v12, :cond_b

    const/4 v4, 0x1

    goto :goto_7

    :cond_b
    :goto_6
    const/4 v4, 0x0

    :goto_7
    new-instance v5, Lkotlin/Pair;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-direct {v5, v10, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v6, v4, v5}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;-><init>(ZLkotlin/Pair;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    instance-of v4, v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-eqz v4, :cond_10

    check-cast v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_d
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    invoke-virtual {v6}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getHasAccess()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_e
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_f
    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    if-eqz v5, :cond_f

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_10
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    :cond_11
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v5

    if-nez v2, :cond_12

    goto :goto_b

    :cond_12
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v12, v10, v5

    if-nez v12, :cond_13

    const/4 v5, 0x1

    goto :goto_c

    :cond_13
    :goto_b
    const/4 v5, 0x0

    :goto_c
    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v6, v7

    add-int/2addr v6, v9

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    new-instance v10, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    new-instance v11, Lkotlin/Pair;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-direct {v11, v9, v12}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v10, v4, v11, v5}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Lkotlin/Pair;Z)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    move v9, v6

    goto :goto_a

    :cond_15
    return-object v1
.end method

.method private final handleStoreState(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSelectedPackId()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSelectedPackId()Ljava/lang/Long;

    move-result-object v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-nez v0, :cond_2

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    goto :goto_1

    :cond_2
    :goto_0
    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    :goto_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSelectedPackId()Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_3

    iget-wide v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->initialStickerPackId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    goto :goto_3

    :cond_4
    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->handleStoreStateForOwnedPage(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;)V

    goto :goto_3

    :cond_5
    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->handleStoreStateForStorePage(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;)V

    :goto_3
    invoke-virtual {p0, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->scrollToPackId(Ljava/lang/Long;)V

    :cond_6
    return-void
.end method

.method private final handleStoreStateForOwnedPage(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;)V
    .locals 13

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getStickerAnimationSettings()I

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->locale:Ljava/util/Locale;

    const-string v4, "null cannot be cast to non-null type java.lang.String"

    invoke-static {v2, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getFrequentlyUsedStickers()Ljava/util/List;

    move-result-object v5

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    instance-of v6, v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-eqz v6, :cond_0

    check-cast v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    const/4 v7, 0x1

    xor-int/2addr v6, v7

    const/4 v9, 0x0

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v6}, Lcom/discord/models/sticker/dto/ModelSticker;->getName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->locale:Ljava/util/Locale;

    invoke-static {v10, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x2

    invoke-static {v10, v2, v9, v11}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v6}, Lcom/discord/models/sticker/dto/ModelSticker;->getTags()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    iget-object v12, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->locale:Ljava/util/Locale;

    invoke-virtual {v10, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10, v2, v9, v11}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v10

    if-ne v10, v7, :cond_1

    :cond_2
    new-instance v10, Lcom/discord/widgets/chat/input/sticker/StickerItem;

    sget-object v11, Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;->OWNED:Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;

    invoke-direct {v10, v6, v1, v11}, Lcom/discord/widgets/chat/input/sticker/StickerItem;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v8}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_4

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;

    sget-object v4, Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/HeaderType$Recent;

    invoke-direct {v3, v4}, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;-><init>(Lcom/discord/widgets/chat/input/sticker/HeaderType;)V

    invoke-interface {v8, v9, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_4
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getHasAccess()Z

    move-result v5

    if-nez v5, :cond_6

    goto :goto_2

    :cond_6
    sget-object v5, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-static {v4}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->locale:Ljava/util/Locale;

    invoke-static {v5, v4, v2, v1, v6}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;->access$buildOwnedStickerListItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/lang/String;ILjava/util/Locale;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v8, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    invoke-direct {p0, p1, v8}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->createCategoryItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_9

    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$NoOwnedPacks;

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-direct {p1, v1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$NoOwnedPacks;-><init>(Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_5

    :cond_9
    invoke-interface {v8}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_a

    goto :goto_4

    :cond_a
    const/4 v7, 0x0

    :goto_4
    if-eqz v7, :cond_b

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-direct {v0, p1, v1, v2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_5

    :cond_b
    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v7

    sget-object v10, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getNewStickerPackCount()I

    move-result v11

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab()Z

    move-result v12

    move-object v6, v0

    move-object v9, v1

    invoke-direct/range {v6 .. v12}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZ)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_5
    return-void
.end method

.method private final handleStoreStateForStorePage(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;)V
    .locals 19

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getStickerPackStoreDirectory()Ljava/util/List;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getStickerAnimationSettings()I

    move-result v12

    const/4 v3, 0x0

    move-object/from16 v13, p1

    invoke-direct {v0, v13, v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->createCategoryItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;Ljava/util/List;)Ljava/util/List;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->locale:Ljava/util/Locale;

    const-string v5, "null cannot be cast to non-null type java.lang.String"

    invoke-static {v3, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    const-string v3, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v15, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    instance-of v3, v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-eqz v3, :cond_0

    check-cast v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    :cond_0
    sget-object v1, Lx/h/m;->d:Lx/h/m;

    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v6}, Lcom/discord/models/sticker/dto/ModelStickerPack;->canBePurchased()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$handleStoreStateForStorePage$$inlined$sortedBy$1;

    invoke-direct {v4, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$handleStoreStateForStorePage$$inlined$sortedBy$1;-><init>(Ljava/util/Map;)V

    invoke-static {v3, v4}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/discord/models/sticker/dto/ModelStickerPack;

    sget-object v3, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v6

    invoke-static {v2}, Lx/h/f;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-static {v5, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v4

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    iget v9, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->stickerCountToDisplayForStore:I

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getViewedPurchaseableStickerPackIds()Ljava/util/Map;

    move-result-object v10

    iget-object v4, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->clock:Lcom/discord/utilities/time/Clock;

    move-object/from16 v17, v4

    move-object v4, v5

    move v5, v12

    move-object/from16 v18, v1

    move-object v1, v11

    move-object/from16 v11, v17

    invoke-static/range {v3 .. v11}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;->access$buildStoreStickerListItems(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;Lcom/discord/models/sticker/dto/ModelStickerPack;ILcom/discord/models/domain/ModelUser;ZZILjava/util/Map;Lcom/discord/utilities/time/Clock;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v11, v1

    move-object/from16 v1, v18

    goto :goto_2

    :cond_3
    move-object v1, v11

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v15}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_4

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_5

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-direct {v1, v2, v14, v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_4

    :cond_5
    new-instance v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getSearchInputStringUpper()Ljava/lang/String;

    move-result-object v5

    sget-object v8, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->STORE:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->getNewStickerPackCount()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;->isStickersSelectedTab()Z

    move-result v10

    move-object v4, v2

    move-object v6, v1

    move-object v7, v14

    invoke-direct/range {v4 .. v10}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;IZ)V

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_4
    return-void
.end method


# virtual methods
.method public final cacheViewedPurchaseableStickerPacks()V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3, v1}, Lcom/discord/stores/StoreStickers;->cacheViewedPurchaseableStickerPacks$default(Lcom/discord/stores/StoreStickers;Ljava/util/Set;ZILjava/lang/Object;)V

    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public final onStickerSelected(Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 13

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;

    instance-of v0, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->channelSelectedStore:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->getId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->channelStore:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v2, v0, v1}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->permissionStore:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v3}, Lcom/discord/stores/StorePermissions;->getPermissionsByChannel()Ljava/util/Map;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v2, v0}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccessWrite(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStickers;->onStickerUsed(Lcom/discord/models/sticker/dto/ModelSticker;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1ef

    const/4 v12, 0x0

    invoke-static/range {v1 .. v12}, Lcom/discord/widgets/chat/MessageManager;->sendMessage$default(Lcom/discord/widgets/chat/MessageManager;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Z

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final scrollToPackId(Ljava/lang/Long;)V
    .locals 9

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;

    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerPickerScreen()Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    const/4 v3, 0x1

    if-eq v1, v3, :cond_1

    goto/16 :goto_3

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    instance-of v5, v4, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;

    if-eqz v5, :cond_2

    check-cast v4, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;->getHeaderType()Lcom/discord/widgets/chat/input/sticker/HeaderType;

    move-result-object v5

    instance-of v5, v5, Lcom/discord/widgets/chat/input/sticker/HeaderType$OwnedPackItem;

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/sticker/OwnedHeaderItem;->getHeaderType()Lcom/discord/widgets/chat/input/sticker/HeaderType;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/chat/input/sticker/HeaderType$OwnedPackItem;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/sticker/HeaderType$OwnedPackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v8, v4, v6

    if-nez v8, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;->getStickerItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    instance-of v5, v4, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    if-eqz v5, :cond_4

    check-cast v4, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v8, v4, v6

    if-nez v8, :cond_4

    :goto_2
    move v2, v3

    goto :goto_3

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    :goto_3
    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;

    invoke-direct {v0, v2}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;-><init>(I)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_6
    return-void
.end method

.method public final setSearchText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->searchSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final setSelectedPackId(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->selectedPackIdSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final setStickerCountToDisplayForStore(I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;->stickerCountToDisplayForStore:I

    return-void
.end method
