.class public final synthetic Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$2;
.super Lx/m/c/i;
.source "WidgetStickerPicker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setUpStickerRecycler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function3<",
        "Lcom/discord/models/sticker/dto/ModelStickerPack;",
        "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
        "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V
    .locals 7

    const-class v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    const/4 v1, 0x3

    const-string v4, "onBuyButtonClicked"

    const-string v5, "onBuyButtonClicked(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/sticker/dto/ModelStickerPack;

    check-cast p2, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    check-cast p3, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$2;->invoke(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-static {v0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->access$onBuyButtonClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-void
.end method
