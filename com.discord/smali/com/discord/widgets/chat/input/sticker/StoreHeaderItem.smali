.class public final Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;
.super Ljava/lang/Object;
.source "StickerAdapterItems.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# instance fields
.field private final isNewPack:Z

.field private final pack:Lcom/discord/models/sticker/dto/ModelStickerPack;


# direct methods
.method public constructor <init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Z)V
    .locals 1

    const-string v0, "pack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iput-boolean p2, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;Lcom/discord/models/sticker/dto/ModelStickerPack;ZILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->copy(Lcom/discord/models/sticker/dto/ModelStickerPack;Z)Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/sticker/dto/ModelStickerPack;Z)Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;
    .locals 1

    const-string v0, "pack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    iget-boolean p1, p1, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getKey()Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, "store-header:"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isNewPack()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StoreHeaderItem(pack="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->pack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isNewPack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
