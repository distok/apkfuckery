.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "StickerPackStoreSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

.field private final stickerPackId:J

.field private final storeStickers:Lcom/discord/stores/StoreStickers;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;JLcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;)V
    .locals 1

    const-string/jumbo v0, "storeStickers"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    iput-wide p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->stickerPackId:J

    iput-object p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;JLcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object p1

    :cond_0
    move-object v1, p1

    and-int/lit8 p1, p7, 0x2

    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p2

    :cond_1
    move-object v2, p2

    and-int/lit8 p1, p7, 0x4

    if-eqz p1, :cond_2

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p3

    :cond_2
    move-object v3, p3

    move-object v0, p0

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;JLcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iget-wide v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->stickerPackId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreStickers;->observeStickerPack(J)Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observeOwnedStickerPacks()Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observePurchasingStickerPacks()Lrx/Observable;

    move-result-object v5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observeViewedPurchaseableStickerPacks()Lrx/Observable;

    move-result-object v6

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->observeStickerAnimationSettings()Lrx/Observable;

    move-result-object v7

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v8

    sget-object v9, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory$observeStoreState$1;

    invoke-static/range {v3 .. v9}, Lrx/Observable;->f(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026cks\n          )\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v2

    iget-object v5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iget-wide v3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->stickerPackId:J

    iget-object v7, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;->analytics:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    const/4 v6, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;-><init>(Lrx/Observable;JLcom/discord/stores/StoreStickers;Lcom/discord/utilities/time/Clock;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method
