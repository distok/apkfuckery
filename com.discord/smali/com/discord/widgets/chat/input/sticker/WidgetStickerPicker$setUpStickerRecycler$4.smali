.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;
.super Ljava/lang/Object;
.source "WidgetStickerPicker.kt"

# interfaces
.implements Landroidx/recyclerview/widget/RecyclerView$RecyclerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->setUpStickerRecycler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;

    invoke-direct {v0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;-><init>()V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$setUpStickerRecycler$4;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    const-string/jumbo v0, "viewHolder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;->cancelLoading()V

    :cond_0
    return-void
.end method
