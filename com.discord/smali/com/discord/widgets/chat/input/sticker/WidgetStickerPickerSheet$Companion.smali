.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;
.super Ljava/lang/Object;
.source "WidgetStickerPickerSheet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;-><init>()V

    return-void
.end method

.method public static synthetic show$default(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    move-object v6, v0

    goto :goto_2

    :cond_2
    move-object v6, p5

    :goto_2
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lkotlin/jvm/functions/Function0;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final show(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lkotlin/jvm/functions/Function0;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;",
            "Ljava/lang/Long;",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;"
        }
    .end annotation

    const-string v0, "fragmentManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->gc()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Number;->longValue()J

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string p3, "com.discord.intent.EXTRA_STICKER_PACK_ID"

    invoke-virtual {v0, p3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    if-eqz p4, :cond_1

    const-string p3, "com.discord.intent.EXTRA_STICKER_PICKER_SCREEN"

    invoke-virtual {v0, p3, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_1
    new-instance p3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    invoke-direct {p3}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;-><init>()V

    invoke-virtual {p3, p2}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;->setStickerPickerListener(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V

    invoke-virtual {p3, p5}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;->setOnCancel(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p3, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-class p2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/discord/app/AppBottomSheet;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-object p3
.end method
