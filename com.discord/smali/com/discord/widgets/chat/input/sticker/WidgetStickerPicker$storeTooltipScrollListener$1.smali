.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;
.super Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;
.source "WidgetStickerPicker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;->this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 0

    const-string p2, "recyclerView"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, -0x1

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->canScrollVertically(I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;->this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->access$getTooltipManager$p(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)Lcom/discord/tooltips/TooltipManager;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreTooltip;

    invoke-virtual {p1, p2}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$storeTooltipScrollListener$1;->this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;

    invoke-static {p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;->access$showStickerPackStoreTooltip(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;)V

    :goto_0
    return-void
.end method
