.class public final Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "StickerCategoryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final appComponent:Lcom/discord/app/AppComponent;

.field private final diffCreator:Lcom/discord/utilities/recycler/DiffCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final onPackClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRecentClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSelectedItemAdapterPositionUpdated:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/utilities/recycler/DiffCreator<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;",
            ">;",
            "Lcom/discord/app/AppComponent;",
            ")V"
        }
    .end annotation

    const-string v0, "onPackClicked"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRecentClicked"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSelectedItemAdapterPositionUpdated"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diffCreator"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appComponent"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onPackClicked:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onRecentClicked:Lkotlin/jvm/functions/Function0;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onSelectedItemAdapterPositionUpdated:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->appComponent:Lcom/discord/app/AppComponent;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    new-instance p4, Lcom/discord/utilities/recycler/DiffCreator;

    invoke-direct {p4}, Lcom/discord/utilities/recycler/DiffCreator;-><init>()V

    :cond_0
    move-object v4, p4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/recycler/DiffCreator;Lcom/discord/app/AppComponent;)V

    return-void
.end method

.method public static final synthetic access$getItems$p(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getOnSelectedItemAdapterPositionUpdated$p(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onSelectedItemAdapterPositionUpdated:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$setItems$p(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    instance-of p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    :goto_0
    return p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onBindViewHolder(Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;

    instance-of v0, p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Recent;

    check-cast p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onRecentClicked:Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Recent;->configure(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_0
    instance-of v0, p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;

    check-cast p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onPackClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;->configure(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;Lkotlin/jvm/functions/Function1;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;
    .locals 4

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const-string v1, "itemView"

    const/4 v2, 0x0

    if-eqz p2, :cond_1

    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    const p2, 0x7f0d00ea

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid Sticker Category Type: "

    invoke-static {v0, p2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const p2, 0x7f0d00eb

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Recent;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Recent;-><init>(Landroid/view/View;)V

    :goto_0
    return-object p2
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->diffCreator:Lcom/discord/utilities/recycler/DiffCreator;

    new-instance v3, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter$setItems$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;)V

    iget-object v4, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->items:Ljava/util/List;

    iget-object v6, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;->appComponent:Lcom/discord/app/AppComponent;

    move-object v2, p0

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/recycler/DiffCreator;->dispatchDiffUpdatesAsync(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;Lcom/discord/app/AppComponent;)V

    return-void
.end method
