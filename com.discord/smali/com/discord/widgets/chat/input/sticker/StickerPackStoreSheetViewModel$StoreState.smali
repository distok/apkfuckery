.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;
.super Ljava/lang/Object;
.source "StickerPackStoreSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final meUser:Lcom/discord/models/domain/ModelUser;

.field private final ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

.field private final purchasingPacks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final stickerAnimationSettings:I

.field private final stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

.field private final viewedPurchaseableStickerPacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;I",
            "Lcom/discord/models/domain/ModelUser;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownedStickerPackState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasingPacks"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewedPurchaseableStickerPacks"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUser"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    iput p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    iput-object p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->copy(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreStickers$StickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-object v0
.end method

.method public final component3()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    return-object v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    return v0
.end method

.method public final component6()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;I",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;"
        }
    .end annotation

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownedStickerPackState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasingPacks"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewedPurchaseableStickerPacks"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUser"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;-><init>(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Ljava/util/Map;ILcom/discord/models/domain/ModelUser;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    iget v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMeUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-object v0
.end method

.method public final getPurchasingPacks()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    return-object v0
.end method

.method public final getStickerAnimationSettings()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    return v0
.end method

.method public final getStickerPack()Lcom/discord/stores/StoreStickers$StickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    return-object v0
.end method

.method public final getViewedPurchaseableStickerPacks()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(stickerPack="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerPack:Lcom/discord/stores/StoreStickers$StickerPackState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ownedStickerPackState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchasingPacks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->purchasingPacks:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", viewedPurchaseableStickerPacks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->viewedPurchaseableStickerPacks:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerAnimationSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->stickerAnimationSettings:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", meUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
