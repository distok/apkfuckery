.class public final Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;
.super Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;
.source "StickerCategoryItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecentItem"
.end annotation


# instance fields
.field private final categoryRange:Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final isSelected:Z


# direct methods
.method public constructor <init>(ZLkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "categoryRange"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;-><init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected:Z

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;ZLkotlin/Pair;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected()Z

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->copy(ZLkotlin/Pair;)Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected()Z

    move-result v0

    return v0
.end method

.method public final component2()Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    return-object v0
.end method

.method public final copy(ZLkotlin/Pair;)Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;"
        }
    .end annotation

    const-string v0, "categoryRange"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;-><init>(ZLkotlin/Pair;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCategoryRange()Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    const-string v0, "recent"

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lkotlin/Pair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "RecentItem(isSelected="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->isSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", categoryRange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;->categoryRange:Lkotlin/Pair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
