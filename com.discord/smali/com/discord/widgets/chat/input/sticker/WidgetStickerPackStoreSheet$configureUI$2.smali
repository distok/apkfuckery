.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;
.super Ljava/lang/Object;
.source "WidgetStickerPackStoreSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->configureUI(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;->this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;->$viewState:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;->this$0:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;->$viewState:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;->$viewState:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->access$onBuyButtonClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-void
.end method
