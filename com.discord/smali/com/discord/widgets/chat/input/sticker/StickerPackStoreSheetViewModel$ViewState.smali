.class public final Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;
.super Ljava/lang/Object;
.source "StickerPackStoreSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final isBeingPurchased:Z

.field private final isPackOwned:Z

.field private final meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field private final stickerItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation
.end field

.field private final stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;


# direct methods
.method public constructor <init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;Z",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Z)V"
        }
    .end annotation

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerItems"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUserPremiumTier"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    iput-boolean p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iput-boolean p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ZILjava/lang/Object;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->copy(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    return v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;Z",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Z)",
            "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;"
        }
    .end annotation

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stickerItems"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUserPremiumTier"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    iget-boolean v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    iget-boolean p1, p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final getStickerItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    return-object v0
.end method

.method public final getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    move v3, v1

    :goto_2
    add-int/2addr v0, v3

    return v0
.end method

.method public final inPremiumTier2()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isBeingPurchased()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    return v0
.end method

.method public final isPackOwned()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(stickerPack="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stickerItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->stickerItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isBeingPurchased="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", meUserPremiumTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isPackOwned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
