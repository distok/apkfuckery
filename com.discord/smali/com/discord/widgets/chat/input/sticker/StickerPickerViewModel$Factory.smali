.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;
.super Ljava/lang/Object;
.source "StickerPickerViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final initialStickerPackId:J

.field private final initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

.field private final locale:Ljava/util/Locale;

.field private final messageManager:Lcom/discord/widgets/chat/MessageManager;

.field private final searchSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedPackIdSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

.field private final storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

.field private final storeStickers:Lcom/discord/stores/StoreStickers;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/subjects/BehaviorSubject;JLcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lrx/subjects/BehaviorSubject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;",
            "Ljava/util/Locale;",
            "Lcom/discord/widgets/chat/MessageManager;",
            "Lcom/discord/stores/StoreStickers;",
            "Lcom/discord/stores/StoreUserSettings;",
            "Lcom/discord/stores/StoreUser;",
            "Lcom/discord/stores/StoreExpressionPickerNavigation;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;J",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "stickerPickerMode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageManager"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStickers"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeExpressionPickerNavigation"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchSubject"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialStickerPickerScreen"

    invoke-static {p11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedPackIdSubject"

    invoke-static {p12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->locale:Ljava/util/Locale;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object p6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p7, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    iput-object p8, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iput-wide p9, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->initialStickerPackId:J

    iput-object p11, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iput-object p12, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->selectedPackIdSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/subjects/BehaviorSubject;JLcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lrx/subjects/BehaviorSubject;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 15

    move/from16 v0, p13

    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object v1

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object/from16 v6, p4

    :goto_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object/from16 v7, p5

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    move-object v8, v1

    goto :goto_2

    :cond_2
    move-object/from16 v8, p6

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExpressionPickerNavigation()Lcom/discord/stores/StoreExpressionPickerNavigation;

    move-result-object v1

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p7

    :goto_3
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_4

    const-string v1, ""

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    const-string v2, "BehaviorSubject.create(\"\")"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v10, v1

    goto :goto_4

    :cond_4
    move-object/from16 v10, p8

    :goto_4
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create(\n\u2026      null as Long?\n    )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v14, v0

    goto :goto_5

    :cond_5
    move-object/from16 v14, p12

    :goto_5
    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v11, p9

    move-object/from16 v13, p11

    invoke-direct/range {v2 .. v14}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExpressionPickerNavigation;Lrx/subjects/BehaviorSubject;JLcom/discord/widgets/chat/input/sticker/StickerPickerScreen;Lrx/subjects/BehaviorSubject;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observeStickerStoreDirectoryLayout()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observeOwnedStickerPacks()Lrx/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observeFrequentlyUsedStickers()Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->observeViewedPurchaseableStickerPacks()Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v0, v5, v6, v7}, Lcom/discord/stores/StoreStickers;->observeNewStickerPacks$default(Lcom/discord/stores/StoreStickers;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    iget-object v6, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->observeStickerAnimationSettings()Lrx/Observable;

    move-result-object v7

    const-string/jumbo v0, "storeUserSettings.observ\u2026tickerAnimationSettings()"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->selectedPackIdSubject:Lrx/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v9

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->storeExpressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreExpressionPickerNavigation;->observeSelectedTab()Lrx/Observable;

    move-result-object v10

    sget-object v11, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory$observeStoreState$1;

    invoke-static/range {v1 .. v11}, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function10;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "modelClass"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->stickerPickerMode:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;->INLINE:Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;

    if-ne v1, v2, :cond_0

    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerInlineViewModel;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v4

    iget-object v5, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iget-object v6, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->selectedPackIdSubject:Lrx/subjects/BehaviorSubject;

    iget-object v7, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->locale:Ljava/util/Locale;

    iget-object v8, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    iget-object v9, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iget-wide v10, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->initialStickerPackId:J

    move-object v3, v1

    invoke-direct/range {v3 .. v11}, Lcom/discord/widgets/chat/input/sticker/StickerPickerInlineViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;J)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerSheetViewModel;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v13

    iget-object v14, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->searchSubject:Lrx/subjects/BehaviorSubject;

    iget-object v15, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->selectedPackIdSubject:Lrx/subjects/BehaviorSubject;

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->locale:Ljava/util/Locale;

    iget-object v3, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->messageManager:Lcom/discord/widgets/chat/MessageManager;

    iget-object v4, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->initialStickerPickerScreen:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    iget-wide v5, v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Factory;->initialStickerPackId:J

    move-object v12, v1

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    move-wide/from16 v19, v5

    invoke-direct/range {v12 .. v20}, Lcom/discord/widgets/chat/input/sticker/StickerPickerSheetViewModel;-><init>(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;J)V

    :goto_0
    return-object v1
.end method
