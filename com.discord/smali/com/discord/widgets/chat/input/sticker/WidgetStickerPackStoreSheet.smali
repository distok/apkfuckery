.class public final Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetStickerPackStoreSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ANALYTICS_LOCATION:Ljava/lang/String; = "widget_sticker_pack_Store_sheet_analytics_location"

.field private static final ANALYTICS_LOCATION_SECTION:Ljava/lang/String; = "widget_sticker_pack_Store_sheet_analytics_location_section"

.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;

.field private static final VIEW_TYPE:Ljava/lang/String; = "widget_sticker_pack_store_sheet_view_type"


# instance fields
.field private adapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

.field private final buy$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyPremium$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final divider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    const-string v6, "recyclerView"

    const-string v7, "getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    const-string v6, "buy"

    const-string v7, "getBuy()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    const-string v6, "buyPremium"

    const-string v7, "getBuyPremium()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    const-string v6, "divider"

    const-string v7, "getDivider()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0a3e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a3f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a54

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->buy$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a56

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->buyPremium$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a55

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->divider$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->configureUI(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$onBuyButtonClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->onBuyButtonClicked(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-void
.end method

.method public static final synthetic access$onStickerHeaderItemsClicked(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->onStickerHeaderItemsClicked(Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V

    return-void
.end method

.method public static final synthetic access$onStickerItemSelected(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/widgets/chat/input/sticker/StickerItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->onStickerItemSelected(Lcom/discord/widgets/chat/input/sticker/StickerItem;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V
    .locals 8

    if-eqz p1, :cond_7

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/discord/app/AppBottomSheet;->setBottomSheetState(I)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->adapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerItems()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;->setData(Ljava/util/List;)V

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "requireContext()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v5

    invoke-virtual {v0, v2, v4, v5}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPremiumPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v5

    invoke-virtual {v0, v4, v3, v5}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v4

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    sget-object v6, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0, v5, v6}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f121713

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-virtual {v2, v3}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned()Z

    move-result v3

    const/4 v5, 0x1

    xor-int/2addr v3, v5

    const/16 v6, 0x8

    const/4 v7, 0x0

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->inPremiumTier2()Z

    move-result v3

    if-nez v3, :cond_2

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_4

    const/4 v6, 0x0

    :cond_4
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned()Z

    move-result v3

    xor-int/2addr v3, v5

    const/4 v4, 0x0

    const/4 v6, 0x2

    invoke-static {v2, v3, v4, v6, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isPackOwned()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$configureUI$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuy()Lcom/discord/views/LoadingButton;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;->isBeingPurchased()Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getBuyPremium()Lcom/discord/views/LoadingButton;

    move-result-object p1

    xor-int/2addr v0, v5

    invoke-virtual {p1, v0}, Lcom/discord/views/LoadingButton;->setIconVisibility(Z)V

    goto :goto_4

    :cond_6
    const-string p1, "adapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_7
    :goto_4
    return-void
.end method

.method private final getAdditionalBottomPaddingPx()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getBuy()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->buy$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getBuyPremium()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->buyPremium$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->divider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final onBuyButtonClicked(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 14

    move-object v0, p0

    :try_start_0
    sget-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->Companion:Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const-string/jumbo v4, "widget_sticker_pack_Store_sheet_analytics_location_section"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v3

    :goto_0
    instance-of v4, v2, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    if-nez v4, :cond_1

    move-object v2, v3

    :cond_1
    check-cast v2, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v1, v2}, Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;->getSimplifiedLocation(Lcom/discord/widgets/stickers/StickerPurchaseLocation;)Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    goto :goto_1

    :cond_2
    move-object v6, v3

    :goto_1
    sget-object v1, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v12

    const-string v4, "parentFragmentManager"

    invoke-static {v12, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v13, Lcom/discord/utilities/analytics/Traits$Location;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_3

    const-string/jumbo v5, "widget_sticker_pack_Store_sheet_analytics_location"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    goto :goto_2

    :cond_3
    move-object v5, v3

    :goto_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string/jumbo v7, "widget_sticker_pack_store_sheet_view_type"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    goto :goto_3

    :cond_4
    move-object v4, v3

    :goto_3
    instance-of v7, v4, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    if-nez v7, :cond_5

    move-object v4, v3

    :cond_5
    check-cast v4, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v3

    :cond_6
    move-object v7, v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x18

    const/4 v11, 0x0

    move-object v4, v13

    invoke-direct/range {v4 .. v11}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v7, v1

    move-object v8, v2

    move-object v9, v12

    move-object v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v7 .. v13}, Lcom/discord/utilities/dsti/StickerUtils;->claimOrPurchaseStickerPack(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/utilities/analytics/Traits$Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    const v1, 0x7f1206e8

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-static {p0, v1, v2, v3}, Lf/a/b/p;->l(Landroidx/fragment/app/Fragment;Ljava/lang/CharSequence;II)V

    :goto_4
    return-void
.end method

.method private final onStickerHeaderItemsClicked(Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V
    .locals 4

    sget-object v0, Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog;->Companion:Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "childFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v2

    const/16 p1, 0x50

    invoke-static {p1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog$Companion;->show(Landroidx/fragment/app/FragmentManager;JLjava/lang/Integer;)V

    return-void
.end method

.method private final onStickerItemSelected(Lcom/discord/widgets/chat/input/sticker/StickerItem;)V
    .locals 3

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerFullSizeDialog;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerFullSizeDialog$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "childFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerItem;->getSticker()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/discord/widgets/chat/input/sticker/StickerFullSizeDialog$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;)V

    return-void
.end method

.method public static final show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Lcom/discord/widgets/stickers/StickerPurchaseLocation;)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Lcom/discord/widgets/stickers/StickerPurchaseLocation;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02b4

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "com.discord.intent.extra.EXTRA_STICKER"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    instance-of v1, p1, Lcom/discord/models/sticker/dto/ModelSticker;

    if-nez v1, :cond_1

    move-object p1, v0

    :cond_1
    check-cast p1, Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz p1, :cond_5

    new-instance v7, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "widget_sticker_pack_store_sheet_view_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    :goto_1
    const-string v2, "null cannot be cast to non-null type com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewType"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string/jumbo v3, "widget_sticker_pack_Store_sheet_analytics_location"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_3
    move-object v2, v0

    :goto_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_4

    const-string/jumbo v0, "widget_sticker_pack_Store_sheet_analytics_location_section"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    :cond_4
    const-string v3, "null cannot be cast to non-null type com.discord.widgets.stickers.StickerPurchaseLocation"

    invoke-static {v0, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v0}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, p1, v1, v2, v0}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;-><init>(Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v10, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v5

    const/4 v8, 0x7

    const/4 v9, 0x0

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Factory;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;JLcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, p0, v10}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    :cond_5
    return-void
.end method

.method public onResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$onResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$onResume$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getContainer()Landroid/view/View;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getAdditionalBottomPaddingPx()I

    move-result p2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/view/View;->setPadding(IIII)V

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getDivider()Landroid/view/View;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    new-instance p1, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$onViewCreated$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;)V

    new-instance v4, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$onViewCreated$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;)V

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->adapter:Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    iget-object p1, p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->viewModel:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;->fetchStickersData()V

    return-void

    :cond_0
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method
