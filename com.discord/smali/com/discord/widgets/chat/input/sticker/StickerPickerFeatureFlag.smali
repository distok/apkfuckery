.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;
.super Ljava/lang/Object;
.source "StickerPickerFeatureFlag.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

.field private static final DEFAULT_BUCKET:I

.field private static final INSTANCE$delegate:Lkotlin/Lazy;


# instance fields
.field private experimentBucket:I

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;-><init>(Landroid/content/SharedPreferences;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 2

    const-string v0, "sharedPreferences"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v0, "CACHE_KEY_STICKER_PICKER_EXPERIMENT_BUCKET"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->experimentBucket:I

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/SharedPreferences;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    sget-object p1, Lcom/discord/utilities/cache/SharedPreferencesProvider;->INSTANCE:Lcom/discord/utilities/cache/SharedPreferencesProvider;

    invoke-virtual {p1}, Lcom/discord/utilities/cache/SharedPreferencesProvider;->get()Landroid/content/SharedPreferences;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;-><init>(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->INSTANCE$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$writeExperimentToCache(Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->writeExperimentToCache(Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method

.method public static final getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;
    .locals 1

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v0

    return-object v0
.end method

.method private final observeExperiment(Lcom/discord/stores/StoreExperiments;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreExperiments;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/experiments/domain/Experiment;",
            ">;"
        }
    .end annotation

    const-string v0, "2020-10_stickers_user_android"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "filter { it != null }.map { it!! }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$observeExperiment$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$observeExperiment$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "storeExperiments.observe\u2026che(experiment)\n        }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private final writeExperimentToCache(Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result p1

    const-string v1, "CACHE_KEY_STICKER_PICKER_EXPERIMENT_BUCKET"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public final clearCachedExperiment()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->experimentBucket:I

    iget-object v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_STICKER_PICKER_EXPERIMENT_BUCKET"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final fetchExperiment(Lcom/discord/stores/StoreExperiments;)V
    .locals 10

    const-string/jumbo v0, "storeExperiments"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->observeExperiment(Lcom/discord/stores/StoreExperiments;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    sget-object v7, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$fetchExperiment$1;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$fetchExperiment$1;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final isEnabled()Z
    .locals 2

    iget v0, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->experimentBucket:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
