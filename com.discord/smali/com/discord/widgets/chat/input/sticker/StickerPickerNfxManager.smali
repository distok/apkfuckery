.class public final Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;
.super Ljava/lang/Object;
.source "StickerPickerNfxManager.kt"


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;

.field private final userStore:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreUser;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;)V
    .locals 1

    const-string/jumbo v0, "userStore"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipManager"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->userStore:Lcom/discord/stores/StoreUser;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_5

    sget-object p3, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string p4, "logger"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p4, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 p5, 0x0

    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_0

    :cond_2
    move-object p4, p5

    :goto_0
    if-nez p4, :cond_3

    new-instance p4, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {p4, p3}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance p3, Ljava/lang/ref/WeakReference;

    invoke-direct {p3, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object p3, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_3
    move-object v4, p4

    sget-object p3, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string p3, "floatingViewManager"

    invoke-static {v4, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p3, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/tooltips/TooltipManager;

    goto :goto_1

    :cond_4
    move-object p3, p5

    :goto_1
    if-nez p3, :cond_5

    new-instance p3, Lcom/discord/tooltips/TooltipManager;

    sget-object p4, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {p4}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object p4

    move-object v1, p4

    check-cast v1, Lf/a/l/a;

    sget-object p4, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {p4}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object p4

    move-object v2, p4

    check-cast v2, Ljava/util/Set;

    const/4 v3, 0x0

    const/4 v5, 0x4

    move-object v0, p3

    invoke-direct/range {v0 .. v5}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance p4, Ljava/lang/ref/WeakReference;

    invoke-direct {p4, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object p4, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;)V

    return-void
.end method

.method public static final synthetic access$canShowStickerPickerNfx(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;Lcom/discord/models/domain/ModelUser;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->canShowStickerPickerNfx(Lcom/discord/models/domain/ModelUser;)Z

    move-result p0

    return p0
.end method

.method private final canShowStickerPickerNfx(Lcom/discord/models/domain/ModelUser;)Z
    .locals 7

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getRegistrationTimeMs()J

    move-result-wide v1

    const-wide v3, 0x174e2f5e580L

    const/4 p1, 0x1

    cmp-long v5, v1, v3

    if-gez v5, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    const-wide v4, 0x176bb3e7000L

    cmp-long v6, v2, v4

    if-ltz v6, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_3

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;->INSTANCE:Lcom/discord/widgets/chat/input/sticker/StickerPickerTooltip;

    invoke-virtual {v1, v2, p1}, Lcom/discord/tooltips/TooltipManager;->b(Lcom/discord/tooltips/TooltipManager$Tooltip;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method


# virtual methods
.method public final observeCanShowStickerPickerNfx()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/home/WidgetHomeModel;->Companion:Lcom/discord/widgets/home/WidgetHomeModel$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel$Companion;->getInitialized()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;->userStore:Lcom/discord/stores/StoreUser;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUser;->observeMe(Z)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager$observeCanShowStickerPickerNfx$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager$observeCanShowStickerPickerNfx$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StickerPickerNfxManager;)V

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026howStickerPickerNfx(me) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
