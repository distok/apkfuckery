.class public final Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "StickerAdapterViewHolders.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;",
        "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;"
    }
.end annotation


# instance fields
.field private final animatedIv:Landroid/view/View;

.field private final expiringIv:Landroid/view/View;

.field private final itemContainer:Landroid/view/View;

.field private final limitedChip:Lcom/google/android/material/chip/Chip;

.field private final newChip:Lcom/google/android/material/chip/Chip;

.field private final premiumIv:Landroid/view/View;

.field private final subtitleTv:Landroid/widget/TextView;

.field private final titleTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d00f1

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a5e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026_store_header_item_title)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->titleTv:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a5d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026ore_header_item_subtitle)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->subtitleTv:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a57

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026ore_header_item_animated)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->animatedIv:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a5c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026tore_header_item_premium)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->premiumIv:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a5a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026tore_header_item_limited)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/google/android/material/chip/Chip;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->limitedChip:Lcom/google/android/material/chip/Chip;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a5b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026er_store_header_item_new)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/google/android/material/chip/Chip;

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->newChip:Lcom/google/android/material/chip/Chip;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a59

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026ore_header_item_expiring)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->expiringIv:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0a58

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026re_header_item_container)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->itemContainer:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;

    return-object p0
.end method


# virtual methods
.method public bind(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method

.method public getItemView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v1, "itemView"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "data"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    instance-of v2, v1, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->titleTv:Landroid/widget/TextView;

    move-object v3, v1

    check-cast v3, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v4, "itemView"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v5, "itemView.context"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f10010c

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v8

    invoke-virtual {v8}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    aput-object v8, v7, v9

    invoke-virtual {v2, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "itemView.context.resourc\u2026ack.stickers.size\n      )"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->subtitleTv:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->animatedIv:Landroid/view/View;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isAnimatedPack()Z

    move-result v5

    const/16 v6, 0x8

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    const/16 v5, 0x8

    :goto_0
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->premiumIv:Landroid/view/View;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    const/16 v5, 0x8

    :goto_1
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->limitedChip:Lcom/google/android/material/chip/Chip;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isLimitedPack()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    const/16 v5, 0x8

    :goto_2
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->limitedChip:Lcom/google/android/material/chip/Chip;

    sget-object v10, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    iget-object v5, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v5, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "itemView.resources"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object v5

    invoke-virtual {v10, v4, v5}, Lcom/discord/utilities/dsti/StickerUtils;->getLimitedTimeLeftString(Landroid/content/res/Resources;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isAnimatedPack()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isLimitedPack()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->itemContainer:Landroid/view/View;

    new-instance v4, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder$onConfigure$1;

    invoke-direct {v4, v0, v1}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder$onConfigure$1;-><init>(Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->isNewPack()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->newChip:Lcom/google/android/material/chip/Chip;

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->expiringIv:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_5
    iget-object v1, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->newChip:Lcom/google/android/material/chip/Chip;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;->getPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object v11

    const-wide/32 v12, 0x5265c00

    const/4 v14, 0x0

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-static/range {v10 .. v16}, Lcom/discord/utilities/dsti/StickerUtils;->isStoreListingExpiringSoon$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLcom/discord/utilities/time/Clock;ILjava/lang/Object;)Z

    move-result v1

    iget-object v2, v0, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->expiringIv:Landroid/view/View;

    if-eqz v1, :cond_6

    goto :goto_3

    :cond_6
    const/16 v9, 0x8

    :goto_3
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    :goto_4
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
