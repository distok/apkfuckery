.class public final Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;
.super Lx/m/c/k;
.source "WidgetChatInput.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInput;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInput;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getFlexInputFragment$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->l()Lcom/lytefast/flexinput/widget/FlexEditText;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0405a7

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v2

    const-string v3, "2020-11_android_app_slash_commands"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v2

    if-ne v2, v4, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04013c

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    :cond_0
    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v2}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatInputMentionsRecycler$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    iget-object v3, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v3}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatInputMentionsRecycler$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v4}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatInputApplicationCommandsRoot$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;-><init>(Lcom/lytefast/flexinput/widget/FlexEditText;Landroidx/recyclerview/widget/RecyclerView;Landroidx/constraintlayout/widget/ConstraintLayout;)V

    invoke-static {v1, v2}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$setChatInputEditTextHolder$p(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputEditText;)V

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v1}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatInputEditTextHolder$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/discord/widgets/chat/input/WidgetChatInputEditText;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-virtual {v1, v2}, Lcom/discord/widgets/chat/input/WidgetChatInputEditText;->configureMentionsDataSubscriptions(Lcom/discord/app/AppFragment;)V

    :cond_1
    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    new-instance v2, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    invoke-direct {v2, v0}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;-><init>(Lcom/lytefast/flexinput/widget/FlexEditText;)V

    invoke-static {v1, v2}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$setChatInputTruncatedHint$p(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;)V

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-static {v0}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$getChatInputTruncatedHint$p(Lcom/discord/widgets/chat/input/WidgetChatInput;)Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$onViewBound$1;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;->addBindedTextWatcher(Lcom/discord/app/AppFragment;)V

    :cond_2
    return-void
.end method
