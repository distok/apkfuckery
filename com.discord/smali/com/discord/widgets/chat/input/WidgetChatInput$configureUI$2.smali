.class public final Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;
.super Ljava/lang/Object;
.source "WidgetChatInput.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/input/WidgetChatInput;->configureUI(Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

.field public final synthetic this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/WidgetChatInput;Lcom/discord/widgets/chat/input/WidgetChatInputModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;->this$0:Lcom/discord/widgets/chat/input/WidgetChatInput;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/widgets/chat/input/WidgetChatInput$configureUI$2;->$model:Lcom/discord/widgets/chat/input/WidgetChatInputModel;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/WidgetChatInputModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    const-string v3, "model.channel.guildId"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p1, v0, v1, v2, v3}, Lcom/discord/widgets/chat/input/WidgetChatInput;->access$showFollowSheet(Lcom/discord/widgets/chat/input/WidgetChatInput;JJ)V

    return-void
.end method
