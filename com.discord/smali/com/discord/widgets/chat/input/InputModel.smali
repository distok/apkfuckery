.class public final Lcom/discord/widgets/chat/input/InputModel;
.super Ljava/lang/Object;
.source "WidgetChatInputEditText.kt"


# instance fields
.field private final content:Ljava/lang/String;

.field private final selected:Lkotlin/ranges/IntRange;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lkotlin/ranges/IntRange;)V
    .locals 1

    const-string v0, "content"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/InputModel;Ljava/lang/String;Lkotlin/ranges/IntRange;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/InputModel;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/chat/input/InputModel;->copy(Ljava/lang/String;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/InputModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lkotlin/ranges/IntRange;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/InputModel;
    .locals 1

    const-string v0, "content"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/InputModel;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/chat/input/InputModel;-><init>(Ljava/lang/String;Lkotlin/ranges/IntRange;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/InputModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/InputModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelected()Lkotlin/ranges/IntRange;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lkotlin/ranges/IntRange;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "InputModel(content="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/InputModel;->content:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", selected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/InputModel;->selected:Lkotlin/ranges/IntRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
