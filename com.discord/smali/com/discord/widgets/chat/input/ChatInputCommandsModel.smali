.class public final Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
.super Ljava/lang/Object;
.source "WidgetChatInputEditText.kt"


# instance fields
.field private final autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

.field private final commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

.field private final input:Lcom/discord/widgets/chat/input/InputModel;

.field private final inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

.field private final mentionModels:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/InputModel;",
            "Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;",
            "Lcom/discord/widgets/chat/input/ChatInputMentionsMap;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;",
            "Lcom/discord/widgets/chat/input/MentionToken;",
            ")V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commandContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputMentionsMap"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mentionModels"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    iput-object p2, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    iput-object p3, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    iput-object p4, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    iput-object p5, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/chat/input/ChatInputCommandsModel;Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;ILjava/lang/Object;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->copy(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/chat/input/InputModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    return-object v0
.end method

.method public final component3()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    return-object v0
.end method

.method public final component4()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    return-object v0
.end method

.method public final component5()Lcom/discord/widgets/chat/input/MentionToken;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    return-object v0
.end method

.method public final copy(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;)Lcom/discord/widgets/chat/input/ChatInputCommandsModel;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/chat/input/InputModel;",
            "Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;",
            "Lcom/discord/widgets/chat/input/ChatInputMentionsMap;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;",
            "Lcom/discord/widgets/chat/input/MentionToken;",
            ")",
            "Lcom/discord/widgets/chat/input/ChatInputCommandsModel;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commandContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputMentionsMap"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mentionModels"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;-><init>(Lcom/discord/widgets/chat/input/InputModel;Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;Lcom/discord/widgets/chat/input/ChatInputMentionsMap;Ljava/util/Collection;Lcom/discord/widgets/chat/input/MentionToken;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    iget-object v1, p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    iget-object p1, p1, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoCompleteToken()Lcom/discord/widgets/chat/input/MentionToken;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    return-object v0
.end method

.method public final getCommandContext()Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    return-object v0
.end method

.method public final getInput()Lcom/discord/widgets/chat/input/InputModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    return-object v0
.end method

.method public final getInputMentionsMap()Lcom/discord/widgets/chat/input/ChatInputMentionsMap;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    return-object v0
.end method

.method public final getMentionModels()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/discord/widgets/chat/input/WidgetChatInputCommandsModel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/InputModel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/ChatInputMentionsMap;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/widgets/chat/input/MentionToken;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ChatInputCommandsModel(input="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->input:Lcom/discord/widgets/chat/input/InputModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", commandContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->commandContext:Lcom/discord/widgets/chat/input/applicationcommands/CommandContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", inputMentionsMap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->inputMentionsMap:Lcom/discord/widgets/chat/input/ChatInputMentionsMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mentionModels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->mentionModels:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", autoCompleteToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/chat/input/ChatInputCommandsModel;->autoCompleteToken:Lcom/discord/widgets/chat/input/MentionToken;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
