.class public final Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;
.super Lx/m/c/k;
.source "MessageManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/MessageManager;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/MessageManager$MessageSendResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/MessageManager;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/MessageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;->this$0:Lcom/discord/widgets/chat/MessageManager;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/MessageManager$MessageSendResult;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;->invoke(Lcom/discord/widgets/chat/MessageManager$MessageSendResult;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/MessageManager$MessageSendResult;)V
    .locals 7

    const-string v0, "messageSendResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/chat/MessageManager$MessageSendResult;->getMessageResult()Lcom/discord/utilities/messagesend/MessageResult;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    check-cast p1, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;

    invoke-virtual {p1}, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;->getError()Lcom/discord/utilities/error/Error;

    move-result-object v2

    iget-object p1, p0, Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;->this$0:Lcom/discord/widgets/chat/MessageManager;

    invoke-static {p1}, Lcom/discord/widgets/chat/MessageManager;->access$getContext$p(Lcom/discord/widgets/chat/MessageManager;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/rest/SendUtils;->handleSendError$default(Lcom/discord/utilities/rest/SendUtils;Lcom/discord/utilities/error/Error;Landroid/content/Context;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    instance-of p1, p1, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;->this$0:Lcom/discord/widgets/chat/MessageManager;

    invoke-static {p1}, Lcom/discord/widgets/chat/MessageManager;->access$getContext$p(Lcom/discord/widgets/chat/MessageManager;)Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f12046e

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {p1, v0, v1, v2, v3}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    :cond_1
    :goto_0
    return-void
.end method
