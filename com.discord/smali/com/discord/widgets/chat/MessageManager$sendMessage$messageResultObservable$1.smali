.class public final Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;
.super Ljava/lang/Object;
.source "MessageManager.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/MessageManager;->sendMessage(Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Integer;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/utilities/messagesend/MessageResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

.field public final synthetic $attachments:Ljava/util/List;

.field public final synthetic $content:Ljava/lang/String;

.field public final synthetic $me:Lcom/discord/models/domain/ModelUser$Me;

.field public final synthetic $mentions:Ljava/util/List;

.field public final synthetic $nonNullChannelId:J

.field public final synthetic $pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

.field public final synthetic $stickers:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/widgets/chat/MessageManager;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StorePendingReplies$PendingReply;JLcom/discord/models/domain/ModelUser$Me;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelAllowedMentions;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->this$0:Lcom/discord/widgets/chat/MessageManager;

    iput-object p2, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    iput-wide p3, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$nonNullChannelId:J

    iput-object p5, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$me:Lcom/discord/models/domain/ModelUser$Me;

    iput-object p6, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$content:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$mentions:Ljava/util/List;

    iput-object p8, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$attachments:Ljava/util/List;

    iput-object p9, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$stickers:Ljava/util/List;

    iput-object p10, p0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->call(Ljava/lang/Integer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Integer;)Lrx/Observable;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    new-instance v1, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long v2, v2, v4

    invoke-direct {v1, v2, v3}, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;-><init>(J)V

    new-instance v2, Lg0/l/e/j;

    invoke-direct {v2, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_1
    iget-object v1, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->this$0:Lcom/discord/widgets/chat/MessageManager;

    invoke-static {v1}, Lcom/discord/widgets/chat/MessageManager;->access$getStorePendingReplies$p(Lcom/discord/widgets/chat/MessageManager;)Lcom/discord/stores/StorePendingReplies;

    move-result-object v1

    iget-wide v2, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$nonNullChannelId:J

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StorePendingReplies;->onDeletePendingReply(J)V

    :cond_2
    iget-object v1, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->this$0:Lcom/discord/widgets/chat/MessageManager;

    invoke-static {v1}, Lcom/discord/widgets/chat/MessageManager;->access$getStoreMessages$p(Lcom/discord/widgets/chat/MessageManager;)Lcom/discord/stores/StoreMessages;

    move-result-object v2

    iget-wide v3, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$nonNullChannelId:J

    iget-object v5, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$me:Lcom/discord/models/domain/ModelUser$Me;

    iget-object v6, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$content:Ljava/lang/String;

    iget-object v7, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$mentions:Ljava/util/List;

    iget-object v8, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$attachments:Ljava/util/List;

    iget-object v9, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$stickers:Ljava/util/List;

    iget-object v1, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$pendingReply:Lcom/discord/stores/StorePendingReplies$PendingReply;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    move-object v10, v1

    iget-object v11, v0, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;->$allowedMentions:Lcom/discord/models/domain/ModelAllowedMentions;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x3f00

    const/16 v19, 0x0

    invoke-static/range {v2 .. v19}, Lcom/discord/stores/StoreMessages;->sendMessage$default(Lcom/discord/stores/StoreMessages;JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    :goto_2
    return-object v2
.end method
