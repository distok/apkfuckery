.class public final Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;
.super Ljava/lang/Object;
.source "MessageManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/MessageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttachmentsRequest"
.end annotation


# instance fields
.field private final attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final currentFileSizeMB:F

.field private final maxFileSizeMB:I


# direct methods
.method public constructor <init>(FILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FI",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->currentFileSizeMB:F

    iput p2, p0, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->maxFileSizeMB:I

    iput-object p3, p0, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->attachments:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(FILjava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;-><init>(FILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public final getAttachments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->attachments:Ljava/util/List;

    return-object v0
.end method

.method public final getCurrentFileSizeMB()F
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->currentFileSizeMB:F

    return v0
.end method

.method public final getMaxFileSizeMB()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->maxFileSizeMB:I

    return v0
.end method
