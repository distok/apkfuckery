.class public final Lcom/discord/widgets/chat/MessageManager;
.super Ljava/lang/Object;
.source "MessageManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/chat/MessageManager$MessageSendResult;,
        Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;,
        Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;,
        Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final defaultMessageResultHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/chat/MessageManager$MessageSendResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final logger:Lcom/discord/utilities/logging/Logger;

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storeMessages:Lcom/discord/stores/StoreMessages;

.field private final storePendingReplies:Lcom/discord/stores/StorePendingReplies;

.field private final storeSlowMode:Lcom/discord/stores/StoreSlowMode;

.field private final storeUser:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeMessages"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeSlowMode"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuilds"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storePendingReplies"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/chat/MessageManager;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/widgets/chat/MessageManager;->storeMessages:Lcom/discord/stores/StoreMessages;

    iput-object p3, p0, Lcom/discord/widgets/chat/MessageManager;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p4, p0, Lcom/discord/widgets/chat/MessageManager;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p5, p0, Lcom/discord/widgets/chat/MessageManager;->storeSlowMode:Lcom/discord/stores/StoreSlowMode;

    iput-object p6, p0, Lcom/discord/widgets/chat/MessageManager;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object p7, p0, Lcom/discord/widgets/chat/MessageManager;->storePendingReplies:Lcom/discord/stores/StorePendingReplies;

    iput-object p8, p0, Lcom/discord/widgets/chat/MessageManager;->logger:Lcom/discord/utilities/logging/Logger;

    new-instance p1, Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/chat/MessageManager$defaultMessageResultHandler$1;-><init>(Lcom/discord/widgets/chat/MessageManager;)V

    iput-object p1, p0, Lcom/discord/widgets/chat/MessageManager;->defaultMessageResultHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, p2

    :goto_0
    and-int/lit8 v2, v0, 0x4

    if-eqz v2, :cond_1

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, p3

    :goto_1
    and-int/lit8 v3, v0, 0x8

    if-eqz v3, :cond_2

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v3

    goto :goto_2

    :cond_2
    move-object v3, p4

    :goto_2
    and-int/lit8 v4, v0, 0x10

    if-eqz v4, :cond_3

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getSlowMode()Lcom/discord/stores/StoreSlowMode;

    move-result-object v4

    goto :goto_3

    :cond_3
    move-object v4, p5

    :goto_3
    and-int/lit8 v5, v0, 0x20

    if-eqz v5, :cond_4

    sget-object v5, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v5}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v5

    goto :goto_4

    :cond_4
    move-object v5, p6

    :goto_4
    and-int/lit8 v6, v0, 0x40

    if-eqz v6, :cond_5

    sget-object v6, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v6}, Lcom/discord/stores/StoreStream$Companion;->getPendingReplies()Lcom/discord/stores/StorePendingReplies;

    move-result-object v6

    goto :goto_5

    :cond_5
    move-object v6, p7

    :goto_5
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    goto :goto_6

    :cond_6
    move-object v0, p8

    :goto_6
    move-object p2, p0

    move-object p3, p1

    move-object p4, v1

    move-object p5, v2

    move-object p6, v3

    move-object p7, v4

    move-object p8, v5

    move-object/from16 p9, v6

    move-object/from16 p10, v0

    invoke-direct/range {p2 .. p10}, Lcom/discord/widgets/chat/MessageManager;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/utilities/logging/Logger;)V

    return-void
.end method

.method public static final synthetic access$getContext$p(Lcom/discord/widgets/chat/MessageManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/MessageManager;->context:Landroid/content/Context;

    return-object p0
.end method

.method public static final synthetic access$getStoreMessages$p(Lcom/discord/widgets/chat/MessageManager;)Lcom/discord/stores/StoreMessages;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/MessageManager;->storeMessages:Lcom/discord/stores/StoreMessages;

    return-object p0
.end method

.method public static final synthetic access$getStorePendingReplies$p(Lcom/discord/widgets/chat/MessageManager;)Lcom/discord/stores/StorePendingReplies;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/chat/MessageManager;->storePendingReplies:Lcom/discord/stores/StorePendingReplies;

    return-object p0
.end method

.method public static synthetic editMessage$default(Lcom/discord/widgets/chat/MessageManager;JJLjava/lang/String;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Z
    .locals 7

    and-int/lit8 p7, p7, 0x8

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/chat/MessageManager;->editMessage(JJLjava/lang/String;Lkotlin/jvm/functions/Function2;)Z

    move-result p0

    return p0
.end method

.method public static synthetic sendMessage$default(Lcom/discord/widgets/chat/MessageManager;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Z
    .locals 10

    move/from16 v0, p10

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    move-object v2, v3

    goto :goto_1

    :cond_1
    move-object v2, p2

    :goto_1
    and-int/lit8 v4, v0, 0x4

    if-eqz v4, :cond_2

    move-object v4, v3

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    move-object v5, v3

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v0, 0x10

    if-eqz v6, :cond_4

    sget-object v6, Lx/h/l;->d:Lx/h/l;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    const/4 v7, 0x1

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v0, 0x40

    if-eqz v8, :cond_6

    move-object v8, v3

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v0, 0x80

    if-eqz v9, :cond_7

    goto :goto_7

    :cond_7
    move-object/from16 v3, p8

    :goto_7
    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    move-object v0, p0

    iget-object v9, v0, Lcom/discord/widgets/chat/MessageManager;->defaultMessageResultHandler:Lkotlin/jvm/functions/Function1;

    goto :goto_8

    :cond_8
    move-object v0, p0

    move-object/from16 v9, p9

    :goto_8
    move-object p1, v1

    move-object p2, v2

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v3

    move-object/from16 p9, v9

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/chat/MessageManager;->sendMessage(Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    return v0
.end method

.method private final validateAttachments(Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;)Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;
    .locals 2

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$EmptyAttachments;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$EmptyAttachments;

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->getAttachments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->getCurrentFileSizeMB()F

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->getMaxFileSizeMB()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    new-instance v0, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;

    invoke-direct {v0, p1}, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;-><init>(Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;)V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$Success;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$Success;

    goto :goto_1

    :cond_3
    :goto_0
    sget-object v0, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$EmptyAttachments;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$EmptyAttachments;

    :goto_1
    return-object v0
.end method

.method private final validateMessageContent(Ljava/lang/String;Ljava/util/List;)Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;)",
            "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    sget-object p1, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$EmptyContent;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$EmptyContent;

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 p2, 0x7d0

    if-le p1, p2, :cond_4

    sget-object p1, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;

    goto :goto_2

    :cond_4
    sget-object p1, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;

    :goto_2
    return-object p1
.end method


# virtual methods
.method public final editMessage(JJLjava/lang/String;Lkotlin/jvm/functions/Function2;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "content"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-direct {p0, p5, v0}, Lcom/discord/widgets/chat/MessageManager;->validateMessageContent(Ljava/lang/String;Ljava/util/List;)Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    if-eqz p6, :cond_0

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/16 p2, 0x7d0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p6, p1, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return v2

    :cond_1
    sget-object p6, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$EmptyContent;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$EmptyContent;

    invoke-static {v0, p6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p6

    if-eqz p6, :cond_2

    return v2

    :cond_2
    sget-object p6, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;

    invoke-static {v0, p6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/discord/widgets/chat/MessageManager;->storeMessages:Lcom/discord/stores/StoreMessages;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreMessages;->editMessage(JJLjava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final sendMessage(Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)Z
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;Z",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/chat/MessageManager$MessageSendResult;",
            "Lkotlin/Unit;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v6, p1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v9, p5

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    move-object/from16 v12, p9

    const-string v4, "content"

    invoke-static {v6, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "stickers"

    invoke-static {v9, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "messageSendResultHandler"

    invoke-static {v12, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v11, Lcom/discord/widgets/chat/MessageManager;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v4}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v5

    const/4 v13, 0x0

    if-nez v5, :cond_0

    iget-object v0, v11, Lcom/discord/widgets/chat/MessageManager;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x6

    const/4 v4, 0x0

    const-string v5, "failed to send message because me was null"

    move-object/from16 p1, v0

    move-object/from16 p2, v5

    move-object/from16 p3, v1

    move-object/from16 p4, v2

    move/from16 p5, v3

    move-object/from16 p6, v4

    invoke-static/range {p1 .. p6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return v13

    :cond_0
    invoke-direct {v11, v6, v9}, Lcom/discord/widgets/chat/MessageManager;->validateMessageContent(Ljava/lang/String;Ljava/util/List;)Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;

    move-result-object v4

    sget-object v7, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;

    invoke-static {v4, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/16 v8, 0x7d0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/Unit;

    :cond_1
    invoke-direct {v11, v0}, Lcom/discord/widgets/chat/MessageManager;->validateAttachments(Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;)Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;

    move-result-object v2

    instance-of v7, v2, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;

    if-eqz v7, :cond_3

    check-cast v2, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;

    invoke-virtual {v2}, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;->getAttachmentsRequest()Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;

    move-result-object v0

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->getMaxFileSizeMB()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    :cond_2
    return v13

    :cond_3
    sget-object v3, Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;

    invoke-static {v4, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const/4 v14, 0x1

    xor-int/2addr v3, v14

    if-eqz v3, :cond_4

    sget-object v3, Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$Success;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$Success;

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v14

    if-eqz v2, :cond_4

    return v13

    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;->getAttachments()Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    goto :goto_0

    :cond_5
    const/4 v8, 0x0

    :goto_0
    if-eqz v1, :cond_6

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_1

    :cond_6
    iget-object v0, v11, Lcom/discord/widgets/chat/MessageManager;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->getId()J

    move-result-wide v2

    :goto_1
    move-wide v3, v2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x19

    if-lt v0, v2, :cond_8

    iget-object v0, v11, Lcom/discord/widgets/chat/MessageManager;->context:Landroid/content/Context;

    invoke-static {v0}, Landroidx/core/content/pm/ShortcutManagerCompat;->getDynamicShortcuts(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    const-string v2, "ShortcutManagerCompat.getDynamicShortcuts(context)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v0, v7}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroidx/core/content/pm/ShortcutInfoCompat;

    const-string v10, "it"

    invoke-static {v7, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Landroidx/core/content/pm/ShortcutInfoCompat;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    invoke-static {v2}, Lx/h/f;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v11, Lcom/discord/widgets/chat/MessageManager;->context:Landroid/content/Context;

    const-class v2, Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutManager;

    if-eqz v0, :cond_8

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/ShortcutManager;->reportShortcutUsed(Ljava/lang/String;)V

    :cond_8
    if-eqz p6, :cond_9

    iget-object v0, v11, Lcom/discord/widgets/chat/MessageManager;->storePendingReplies:Lcom/discord/stores/StorePendingReplies;

    invoke-virtual {v0, v3, v4}, Lcom/discord/stores/StorePendingReplies;->getPendingReply(J)Lcom/discord/stores/StorePendingReplies$PendingReply;

    move-result-object v0

    move-object v2, v0

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getShouldMention()Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Lcom/discord/models/domain/ModelAllowedMentions;

    invoke-static {}, Lcom/discord/models/domain/ModelAllowedMentions$Types;->values()[Lcom/discord/models/domain/ModelAllowedMentions$Types;

    move-result-object v7

    invoke-static {v7}, Lf/h/a/f/f/n/g;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    sget-object v20, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const/16 v21, 0x6

    const/16 v22, 0x0

    move-object/from16 v16, v0

    invoke-direct/range {v16 .. v22}, Lcom/discord/models/domain/ModelAllowedMentions;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v10, v0

    goto :goto_4

    :cond_a
    const/4 v10, 0x0

    :goto_4
    iget-object v0, v11, Lcom/discord/widgets/chat/MessageManager;->storeSlowMode:Lcom/discord/stores/StoreSlowMode;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreSlowMode;->getCooldownSecs(Ljava/lang/Long;)Lrx/Observable;

    move-result-object v7

    new-instance v1, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;

    move-object v0, v1

    move-object v13, v1

    move-object/from16 v1, p0

    move-wide/from16 p3, v3

    move-object/from16 v6, p1

    move-object v15, v7

    move-object/from16 v7, p2

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v10}, Lcom/discord/widgets/chat/MessageManager$sendMessage$messageResultObservable$1;-><init>(Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StorePendingReplies$PendingReply;JLcom/discord/models/domain/ModelUser$Me;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelAllowedMentions;)V

    invoke-virtual {v15, v13}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    iget-object v1, v11, Lcom/discord/widgets/chat/MessageManager;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    move-wide/from16 v2, p3

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeFromChannelId(J)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/chat/MessageManager$sendMessage$1;->INSTANCE:Lcom/discord/widgets/chat/MessageManager$sendMessage$1;

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0, v14}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026d)\n    }\n        .take(1)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v2, v14, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/chat/MessageManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/chat/MessageManager$sendMessage$2;

    invoke-direct {v6, v12}, Lcom/discord/widgets/chat/MessageManager$sendMessage$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    const/16 v7, 0x1e

    const/4 v8, 0x0

    move-object/from16 p1, v0

    move-object/from16 p2, v1

    move-object/from16 p3, v2

    move-object/from16 p4, v3

    move-object/from16 p5, v4

    move-object/from16 p6, v5

    move-object/from16 p7, v6

    move/from16 p8, v7

    move-object/from16 p9, v8

    invoke-static/range {p1 .. p9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return v14
.end method
