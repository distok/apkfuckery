.class public Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$PinsAdapterEventHandler;
.super Ljava/lang/Object;
.source "WidgetChannelPinnedMessages.java"

# interfaces
.implements Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PinsAdapterEventHandler"
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;


# direct methods
.method private constructor <init>(Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$PinsAdapterEventHandler;->this$0:Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$PinsAdapterEventHandler;-><init>(Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;)V

    return-void
.end method


# virtual methods
.method public onCallMessageClicked(JLcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;)V
    .locals 0
    .param p3    # Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V
    .locals 0
    .param p1    # Lcom/discord/stores/StoreChat$InteractionState;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onListClicked()V
    .locals 0

    return-void
.end method

.method public onMessageAuthorAvatarClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onMessageAuthorLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/Long;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public onMessageAuthorNameClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onMessageBlockedGroupClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onMessageClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 5
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreMessagesLoader;->jumpToMessage(JJ)V

    iget-object p1, p0, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$PinsAdapterEventHandler;->this$0:Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lf/a/b/m;->a(Landroid/content/Context;)V

    return-void
.end method

.method public onMessageLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;)V
    .locals 6
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    iget-object p1, p0, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$PinsAdapterEventHandler;->this$0:Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;->showForPin(Landroidx/fragment/app/FragmentManager;JJLjava/lang/CharSequence;)V

    return-void
.end method

.method public onOldestMessageId(JJ)V
    .locals 0

    return-void
.end method

.method public onOpenPinsClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onQuickAddReactionClicked(JJJJZ)V
    .locals 0

    return-void
.end method

.method public onQuickDownloadClicked(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 p1, 0x0

    return p1
.end method

.method public onReactionClicked(JJJJZLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 0
    .param p10    # Lcom/discord/models/domain/ModelMessageReaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onReactionLongClicked(JJJZLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 0
    .param p8    # Lcom/discord/models/domain/ModelMessageReaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 4
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/discord/models/sticker/dto/ModelSticker;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v2

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/discord/stores/StoreMessagesLoader;->jumpToMessage(JJ)V

    return-void
.end method

.method public onUrlLongClicked(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$PinsAdapterEventHandler;->this$0:Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/WidgetUrlActions;->launch(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onUserActivityAction(JJJILcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelApplication;)V
    .locals 0
    .param p8    # Lcom/discord/models/domain/activity/ModelActivity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p9    # Lcom/discord/models/domain/ModelApplication;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onUserMentionClicked(JJJ)V
    .locals 0

    return-void
.end method
