.class public final Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$onViewBoundOrOnResume$2;
.super Lx/m/c/k;
.source "WidgetChatOverlay.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$onViewBoundOrOnResume$2;->invoke(Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;

    invoke-static {v0}, Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;->access$getJumpToPresentFab$p(Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;->isViewingOldMessages()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
