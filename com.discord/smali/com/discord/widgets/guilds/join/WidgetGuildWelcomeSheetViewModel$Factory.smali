.class public final Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetGuildWelcomeSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;->guildId:J

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    invoke-virtual {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;->guildId:J

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreChannelsSelected;J)V

    return-object p1
.end method

.method public final observeStores()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;->guildId:J

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildWelcomeScreens()Lcom/discord/stores/StoreGuildWelcomeScreens;

    move-result-object v0

    iget-wide v2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;->guildId:J

    invoke-virtual {v0, v2, v3}, Lcom/discord/stores/StoreGuildWelcomeScreens;->observeGuildWelcomeScreen(J)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;

    invoke-static {v1, v0, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n            .\u2026          )\n            }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
