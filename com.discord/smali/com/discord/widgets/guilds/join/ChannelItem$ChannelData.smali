.class public final Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;
.super Lcom/discord/widgets/guilds/join/ChannelItem;
.source "WidgetGuildWelcomeSheetChannelAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/join/ChannelItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChannelData"
.end annotation


# instance fields
.field private final dismissSheet:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final goToChannel:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final guildId:J

.field private final index:I

.field private final welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuildWelcomeChannel;Lkotlin/jvm/functions/Function0;JLkotlin/jvm/functions/Function2;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;J",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;I)V"
        }
    .end annotation

    const-string/jumbo v0, "welcomeChannel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dismissSheet"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "goToChannel"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/guilds/join/ChannelItem;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    iput-object p2, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    iput-wide p3, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    iput-object p5, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    iput p6, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;Lcom/discord/models/domain/ModelGuildWelcomeChannel;Lkotlin/jvm/functions/Function0;JLkotlin/jvm/functions/Function2;IILjava/lang/Object;)Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    :cond_3
    move-object v2, p5

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget p6, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    :cond_4
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-wide p5, v0

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->copy(Lcom/discord/models/domain/ModelGuildWelcomeChannel;Lkotlin/jvm/functions/Function0;JLkotlin/jvm/functions/Function2;I)Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuildWelcomeChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    return-wide v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuildWelcomeChannel;Lkotlin/jvm/functions/Function0;JLkotlin/jvm/functions/Function2;I)Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;J",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;I)",
            "Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;"
        }
    .end annotation

    const-string/jumbo v0, "welcomeChannel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dismissSheet"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "goToChannel"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;-><init>(Lcom/discord/models/domain/ModelGuildWelcomeChannel;Lkotlin/jvm/functions/Function0;JLkotlin/jvm/functions/Function2;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    iget-object v1, p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    iget-object v1, p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    iget p1, p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDismissSheet()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getGoToChannel()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    return-wide v0
.end method

.method public final getIndex()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    return v0
.end method

.method public final getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ChannelData(welcomeChannel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->welcomeChannel:Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", dismissSheet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->dismissSheet:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", goToChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->goToChannel:Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->index:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
