.class public final Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;
.super Ljava/lang/Object;
.source "WidgetGuildWelcomeSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;ILjava/lang/Object;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->copy(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    iget-object p1, p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getGuildWelcomeScreen()Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(guild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildWelcomeScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->guildWelcomeScreen:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
