.class public final Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;
.super Ljava/lang/Object;
.source "WidgetGuildWelcomeSheetChannelAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/join/ChannelViewHolder;->bind(Lcom/discord/widgets/guilds/join/ChannelItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/guilds/join/ChannelItem;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/join/ChannelItem;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;->$data:Lcom/discord/widgets/guilds/join/ChannelItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;->$data:Lcom/discord/widgets/guilds/join/ChannelItem;

    check-cast p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getGoToChannel()Lkotlin/jvm/functions/Function2;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;->$data:Lcom/discord/widgets/guilds/join/ChannelItem;

    check-cast v0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getChannelId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;->$data:Lcom/discord/widgets/guilds/join/ChannelItem;

    check-cast v1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;->$data:Lcom/discord/widgets/guilds/join/ChannelItem;

    check-cast p1, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getDismissSheet()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
