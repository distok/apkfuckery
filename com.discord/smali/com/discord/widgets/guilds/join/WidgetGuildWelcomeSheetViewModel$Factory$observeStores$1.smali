.class public final Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;
.super Ljava/lang/Object;
.source "WidgetGuildWelcomeSheetViewModel.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;",
        "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory$observeStores$1;->call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
