.class public abstract Lcom/discord/widgets/guilds/join/ChannelItem;
.super Ljava/lang/Object;
.source "WidgetGuildWelcomeSheetChannelAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;,
        Lcom/discord/widgets/guilds/join/ChannelItem$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/guilds/join/ChannelItem$Companion;

.field public static final TYPE_CHANNEL:I


# instance fields
.field private final type:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/guilds/join/ChannelItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/join/ChannelItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/join/ChannelItem;->Companion:Lcom/discord/widgets/guilds/join/ChannelItem$Companion;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/widgets/guilds/join/ChannelItem;->type:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/join/ChannelItem;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/join/ChannelItem;->type:I

    return v0
.end method
