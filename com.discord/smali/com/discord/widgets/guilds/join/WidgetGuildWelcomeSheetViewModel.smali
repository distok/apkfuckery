.class public final Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;
.super Lf/a/b/l0;
.source "WidgetGuildWelcomeSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;,
        Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;,
        Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final guildId:J

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreChannelsSelected;J)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreChannelsSelected;",
            "J)V"
        }
    .end annotation

    const-string/jumbo v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;->storeObservable:Lrx/Observable;

    iput-object p2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-wide p3, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;->guildId:J

    const/4 p2, 0x0

    const/4 v0, 0x2

    invoke-static {p1, p0, p2, v0, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    new-instance v7, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$1;-><init>(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGuildWelcomeScreens()Lcom/discord/stores/StoreGuildWelcomeScreens;

    move-result-object p2

    invoke-virtual {p2, p3, p4}, Lcom/discord/stores/StoreGuildWelcomeScreens;->fetchIfNonexisting(J)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGuildWelcomeScreens()Lcom/discord/stores/StoreGuildWelcomeScreens;

    move-result-object p1

    invoke-virtual {p1, p3, p4}, Lcom/discord/stores/StoreGuildWelcomeScreens;->markWelcomeScreenShown(J)V

    const-string p1, "Guild Welcome Screen"

    const-string p2, ""

    invoke-static {p1, p2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->openModal(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;->handleStoreState(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;)V

    return-void
.end method

.method private final handleLoadedGuild(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildWelcomeScreen;)V
    .locals 9

    new-instance v8, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v0, "guild.name"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v4

    const-string v0, "guild.shortName"

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v5

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->getDescription()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :cond_0
    move-object v6, p1

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->getWelcomeChannels()Ljava/util/List;

    move-result-object p1

    :cond_1
    move-object v7, p1

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v8}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleLoadedWelcomeScreen(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildWelcomeScreen;)V
    .locals 9

    new-instance v8, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v0, "guild.name"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v4

    const-string v0, "guild.shortName"

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildWelcomeScreen;->getWelcomeChannels()Ljava/util/List;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v8}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;)V
    .locals 4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->component1()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$StoreState;->component2()Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    move-result-object p1

    instance-of v1, p1, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    check-cast v1, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;

    if-eqz v0, :cond_1

    sget-object v3, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Failure;->INSTANCE:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Failure;

    if-ne p1, v3, :cond_1

    sget-object p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_1
    sget-object v3, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Fetching;->INSTANCE:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Fetching;

    if-ne p1, v3, :cond_2

    sget-object p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;->getData()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object p1

    goto :goto_1

    :cond_3
    move-object p1, v2

    :goto_1
    if-eqz p1, :cond_4

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;->getData()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;->handleLoadedWelcomeScreen(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_6

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;->getData()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object v2

    :cond_5
    invoke-direct {p0, v0, v2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;->handleLoadedGuild(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;->getData()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object v2

    :cond_7
    if-nez v2, :cond_8

    sget-object p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_8
    sget-object p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final onClickChannel(JJILjava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJI",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "welcomeChannels"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel$default(Lcom/discord/utilities/channel/ChannelSelector;JJIILjava/lang/Object;)V

    invoke-interface {p6, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p6, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getEmojiId()Ljava/lang/Long;

    move-result-object p3

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    const/4 v6, 0x0

    :goto_0
    if-nez v5, :cond_1

    return-void

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getChannelId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p6

    invoke-virtual {v4, p6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getDescription()Ljava/lang/String;

    move-result-object p4

    const-string p6, "null cannot be cast to non-null type kotlin.String"

    invoke-static {p4, p6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, p5

    move-wide v1, p1

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/analytics/AnalyticsTracker;->welcomeScreenChannelSelected(IJLjava/util/List;Ljava/util/List;Ljava/lang/String;Z)V

    return-void
.end method
