.class public final Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetGuildWelcomeSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;

.field private static final LOADED_VIEW_INDEX:I = 0x1

.field private static final LOADING_VIEW_INDEX:I


# instance fields
.field private final channelsAdapter:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;

.field private final channelsRecyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildWelcomeSheetFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const-string v3, "guildWelcomeSheetFlipper"

    const-string v4, "getGuildWelcomeSheetFlipper()Lcom/discord/app/AppViewFlipper;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const-string v6, "guildIcon"

    const-string v7, "getGuildIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const-string v6, "guildIconName"

    const-string v7, "getGuildIconName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const-string v6, "guildName"

    const-string v7, "getGuildName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const-string v6, "guildDescription"

    const-string v7, "getGuildDescription()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const-string v6, "channelsRecyclerView"

    const-string v7, "getChannelsRecyclerView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->Companion:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a054a

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildWelcomeSheetFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a054b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a054d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a054e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0549

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0548

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->channelsRecyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->channelsAdapter:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;)Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->viewModel:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->handleViewState(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->viewModel:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    return-void
.end method

.method private final configureGuildDetails(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildName()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const p1, 0x7f121ab2

    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildDescription()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildDescription()Landroid/widget/TextView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final configureGuildIcon(JLjava/lang/String;Ljava/lang/String;)V
    .locals 14

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object/from16 v1, p3

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildIcon()Landroid/widget/ImageView;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildIcon()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1c

    const/4 v6, 0x0

    const-string v1, "asset://asset/images/default_icon_selected.jpg"

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;)V
    .locals 6

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;->component1()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;->component2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;->component3()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;->component4()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;->component5()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;->component6()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, v0, v1, v4, v3}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->configureGuildIcon(JLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2, v5}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->configureGuildDetails(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->configureWelcomeChannels(Ljava/util/List;J)V

    return-void
.end method

.method private final configureWelcomeChannels(Ljava/util/List;J)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildWelcomeChannel;",
            ">;J)V"
        }
    .end annotation

    new-instance v7, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$configureWelcomeChannels$dismissSheet$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$configureWelcomeChannels$dismissSheet$1;-><init>(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;)V

    new-instance v8, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$configureWelcomeChannels$goToChannel$1;

    invoke-direct {v8, p0, p2, p3, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$configureWelcomeChannels$goToChannel$1;-><init>(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;JLjava/util/List;)V

    new-instance v9, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v6, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v10, v6, 0x1

    if-ltz v6, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    new-instance v11, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    move-object v0, v11

    move-object v2, v7

    move-wide v3, p2

    move-object v5, v8

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;-><init>(Lcom/discord/models/domain/ModelGuildWelcomeChannel;Lkotlin/jvm/functions/Function0;JLkotlin/jvm/functions/Function2;I)V

    invoke-interface {v9, v11}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v6, v10

    goto :goto_0

    :cond_0
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 p1, 0x0

    throw p1

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->channelsAdapter:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;

    invoke-virtual {p1, v9}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;->setData(Ljava/util/List;)V

    return-void
.end method

.method private final getChannelsRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->channelsRecyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getGuildDescription()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGuildIconName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildWelcomeSheetFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->guildWelcomeSheetFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final handleViewState(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loading;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->showLoadingView()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->updateView(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Invalid;

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static final show(Landroidx/fragment/app/FragmentManager;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->Companion:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;J)V

    return-void
.end method

.method private final showLoadingView()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildWelcomeSheetFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method

.method private final updateView(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getGuildWelcomeSheetFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->configureUI(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$ViewState$Loaded;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d021a

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "com.discord.intent.extra.EXTRA_GUILD_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide p1

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel$Factory;-><init>(J)V

    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProvider(\n     \u2026del::class.java\n        )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->viewModel:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetViewModel;

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p1, p0, p2, v0, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$onViewCreated$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->getChannelsRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->channelsAdapter:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheetChannelAdapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_0
    const-string/jumbo p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2
.end method
