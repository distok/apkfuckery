.class public final Lcom/discord/widgets/guilds/join/ChannelViewHolder;
.super Lcom/discord/widgets/guilds/join/BaseChannelViewHolder;
.source "WidgetGuildWelcomeSheetChannelAdapter.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final descriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojiImageView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final nameView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unicodeEmojiView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final welcomeChannel$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;

    const/4 v1, 0x5

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string/jumbo v3, "welcomeChannel"

    const-string v4, "getWelcomeChannel()Landroidx/cardview/widget/CardView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/s;

    const-string v6, "emojiImageView"

    const-string v7, "getEmojiImageView()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Lx/m/c/s;

    const-string v6, "descriptionView"

    const-string v7, "getDescriptionView()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x3

    new-instance v4, Lx/m/c/s;

    const-string v6, "nameView"

    const-string v7, "getNameView()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x4

    new-instance v4, Lx/m/c/s;

    const-string/jumbo v6, "unicodeEmojiView"

    const-string v7, "getUnicodeEmojiView()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/join/BaseChannelViewHolder;-><init>(Landroid/view/View;)V

    const p1, 0x7f0a053f

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->welcomeChannel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0542

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->emojiImageView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0540

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->descriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0545

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->nameView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0547

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->unicodeEmojiView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method private final getDescriptionView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->descriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmojiImageView()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->emojiImageView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getNameView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->nameView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUnicodeEmojiView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->unicodeEmojiView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getWelcomeChannel()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->welcomeChannel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/discord/widgets/guilds/join/ChannelItem;)V
    .locals 12

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/widgets/guilds/join/BaseChannelViewHolder;->bind(Lcom/discord/widgets/guilds/join/ChannelItem;)V

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getEmojiImageView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getEmojiUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getDescriptionView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getNameView()Landroid/widget/TextView;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getChannelId()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getWelcomeChannel()Landroidx/cardview/widget/CardView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;

    invoke-direct {v2, p1}, Lcom/discord/widgets/guilds/join/ChannelViewHolder$bind$1;-><init>(Lcom/discord/widgets/guilds/join/ChannelItem;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 p1, 0x8

    const/4 v1, 0x0

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getEmojiImageView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7c

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getEmojiImageView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getUnicodeEmojiView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getEmojiName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getEmojiImageView()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getUnicodeEmojiView()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/ChannelViewHolder;->getUnicodeEmojiView()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/ChannelItem$ChannelData;->getWelcomeChannel()Lcom/discord/models/domain/ModelGuildWelcomeChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildWelcomeChannel;->getEmojiName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    return-void
.end method
