.class public final Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild$1;
.super Ljava/lang/Object;
.source "WidgetGuildSelector.kt"

# interfaces
.implements Lrx/functions/Action3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild;-><init>(ILcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action3<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        "Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $adapter:Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild$1;->$adapter:Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild$1;->$adapter:Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;

    invoke-static {p1}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;->access$getDialog$p(Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;)Lcom/discord/widgets/guilds/WidgetGuildSelector;

    move-result-object p1

    invoke-virtual {p3}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/discord/widgets/guilds/WidgetGuildSelector;->access$onGuildSelected(Lcom/discord/widgets/guilds/WidgetGuildSelector;Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild$1;->call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;)V

    return-void
.end method
