.class public final Lcom/discord/widgets/guilds/WidgetGuildSelector$BaseFilterFunction;
.super Ljava/lang/Object;
.source "WidgetGuildSelector.kt"

# interfaces
.implements Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/WidgetGuildSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BaseFilterFunction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public includeGuild(Lcom/discord/models/domain/ModelGuild;)Z
    .locals 1

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction$DefaultImpls;->includeGuild(Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;Lcom/discord/models/domain/ModelGuild;)Z

    move-result p1

    return p1
.end method
