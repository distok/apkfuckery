.class public final enum Lcom/discord/widgets/guilds/create/StockGuildTemplate;
.super Ljava/lang/Enum;
.source "StockGuildTemplate.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/guilds/create/StockGuildTemplate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum CLUB:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum CONTENT_CREATOR:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum CREATE:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum FRIEND_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum GAMING_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum LOCAL_COMMUNITY:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field public static final enum STUDY_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "FRIEND_GROUP"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->FRIEND_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "STUDY_GROUP"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->STUDY_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "GAMING_GROUP"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->GAMING_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "CONTENT_CREATOR"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CONTENT_CREATOR:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "CLUB"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CLUB:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "LOCAL_COMMUNITY"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->LOCAL_COMMUNITY:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    const-string v2, "CREATE"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CREATE:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->$VALUES:[Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/guilds/create/StockGuildTemplate;
    .locals 1

    const-class v0, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/guilds/create/StockGuildTemplate;
    .locals 1

    sget-object v0, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->$VALUES:[Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    invoke-virtual {v0}, [Lcom/discord/widgets/guilds/create/StockGuildTemplate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    return-object v0
.end method


# virtual methods
.method public final getChannels(Landroid/content/res/Resources;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "resources"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const-string v3, "resources.getString(R.st\u2026plate_name_announcements)"

    const v4, 0x7f120c5e

    const-string v7, "resources.getString(R.st\u2026e_name_welcome_and_rules)"

    const v8, 0x7f120c83

    const-string v9, "resources.getString(R.st\u2026ame_category_information)"

    const v10, 0x7f120c5f

    const-string v11, "resources.getString(R.st\u2026mplate_name_voice_lounge)"

    const-string v15, "resources.getString(R.st\u2026late_name_category_voice)"

    const-string v5, "resources.getString(R.st\u2026ld_template_name_general)"

    const-string v14, "resources.getString(R.st\u2026plate_name_category_text)"

    const/4 v6, 0x3

    const/4 v2, 0x1

    const/4 v12, 0x2

    const/4 v13, 0x0

    packed-switch v1, :pswitch_data_0

    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v9, v6, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v10, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v10, v8}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v10, v9, v13

    new-instance v7, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v4}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v7, v9, v2

    new-instance v3, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v4, 0x7f120c74

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "resources.getString(R.st\u2026_template_name_resources)"

    invoke-static {v4, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v3, v9, v12

    const-wide/16 v3, 0x64

    invoke-static {v1, v3, v4, v13, v9}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c60

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v6, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v6, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;

    const v7, 0x7f120c67

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v6, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;-><init>(Ljava/lang/String;)V

    aput-object v6, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c6c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "resources.getString(R.st\u2026plate_name_meeting_plans)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c72

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "resources.getString(R.st\u2026_template_name_off_topic)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v12

    const-wide/16 v5, 0xc8

    invoke-static {v3, v5, v6, v13, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v3}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c61

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7d

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v6, "resources.getString(R.st\u2026_name_voice_meeting_room)"

    invoke-static {v0, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v0}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    const-wide/16 v5, 0x12c

    invoke-static {v3, v5, v6, v12, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v9, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v10, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v10, v8}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v10, v9, v13

    new-instance v7, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v4}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v7, v9, v2

    const-wide/16 v3, 0x64

    invoke-static {v1, v3, v4, v13, v9}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c60

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v6, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v7, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;

    const v8, 0x7f120c67

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;-><init>(Ljava/lang/String;)V

    aput-object v7, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v7, 0x7f120c63

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resources.getString(R.st\u2026ild_template_name_events)"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v7, 0x7f120c6b

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resources.getString(R.st\u2026_name_ideas_and_feedback)"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v12

    const-wide/16 v7, 0xc8

    invoke-static {v3, v7, v8, v13, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v3}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c61

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v6, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c78

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "resources.getString(R.st\u2026_voice_community_hangout)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    new-instance v2, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v5, 0x7f120c80

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v5, "resources.getString(R.st\u2026e_name_voice_stream_room)"

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v2, v4, v12

    const-wide/16 v5, 0x12c

    invoke-static {v3, v5, v6, v12, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v6, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v9, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v9, v8}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v9, v6, v13

    new-instance v7, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v4}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v7, v6, v2

    const-wide/16 v3, 0x64

    invoke-static {v1, v3, v4, v13, v6}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c60

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v6, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;

    const v7, 0x7f120c67

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v6, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;-><init>(Ljava/lang/String;)V

    aput-object v6, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c6c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "resources.getString(R.st\u2026plate_name_meeting_plans)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    const-wide/16 v5, 0xc8

    invoke-static {v3, v5, v6, v13, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v3}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c61

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7d

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v6, "resources.getString(R.st\u2026_name_voice_meeting_room)"

    invoke-static {v0, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v0}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    const-wide/16 v5, 0x12c

    invoke-static {v3, v5, v6, v12, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v3, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v8}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v13

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v7, 0x7f120c71

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resources.getString(R.st\u2026ate_name_notes_resources)"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v2

    const-wide/16 v7, 0x64

    invoke-static {v1, v7, v8, v13, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c60

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x4

    new-array v4, v4, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v7, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;

    const v8, 0x7f120c67

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;-><init>(Ljava/lang/String;)V

    aput-object v7, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v7, 0x7f120c6a

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resources.getString(R.st\u2026plate_name_homework_help)"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v7, 0x7f120c75

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resources.getString(R.st\u2026te_name_session_planning)"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v12

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v7, 0x7f120c72

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "resources.getString(R.st\u2026_template_name_off_topic)"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v6

    const-wide/16 v7, 0xc8

    invoke-static {v3, v7, v8, v13, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v3}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c61

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v6, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c81

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v13

    invoke-virtual {v0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "resources.getString(R.st\u2026study_room, 1.toString())"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c81

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v13

    invoke-virtual {v0, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "resources.getString(R.st\u2026study_room, 2.toString())"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v0}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v12

    const-wide/16 v5, 0x12c

    invoke-static {v3, v5, v6, v12, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    const v1, 0x7f120c60

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v3, v6, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;

    const v6, 0x7f120c67

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v13

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v5, 0x7f120c64

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "resources.getString(R.st\u2026guild_template_name_game)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v5, 0x7f120c6f

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "resources.getString(R.st\u2026uild_template_name_music)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v12

    const-wide/16 v4, 0x64

    invoke-static {v1, v4, v5, v13, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c61

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c80

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v6, "resources.getString(R.st\u2026e_name_voice_stream_room)"

    invoke-static {v0, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v0}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    const-wide/16 v5, 0xc8

    invoke-static {v3, v5, v6, v12, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    const v1, 0x7f120c60

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v3, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;

    const v6, 0x7f120c67

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v13

    new-instance v4, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v5, 0x7f120c62

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "resources.getString(R.st\u2026ame_clips_and_highlights)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v2

    const-wide/16 v4, 0x64

    invoke-static {v1, v4, v5, v13, v3}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v1

    const v3, 0x7f120c61

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v12, [Lcom/discord/widgets/guilds/create/ChannelTemplate;

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c7b

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "resources.getString(R.st\u2026emplate_name_voice_lobby)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;

    const v6, 0x7f120c79

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v6, "resources.getString(R.st\u2026mplate_name_voice_gaming)"

    invoke-static {v0, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v0}, Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v2

    const-wide/16 v5, 0xc8

    invoke-static {v3, v5, v6, v12, v4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getSystemChannelId()J
    .locals 2

    const-wide/16 v0, 0xb

    return-wide v0
.end method
