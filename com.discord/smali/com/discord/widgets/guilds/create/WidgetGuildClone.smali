.class public final Lcom/discord/widgets/guilds/create/WidgetGuildClone;
.super Lcom/discord/widgets/guilds/create/WidgetGuildCreate;
.source "WidgetGuildClone.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;

.field private static final INTENT_GUILD_TEMPLATE_CODE:Ljava/lang/String; = "guild_template_code"

.field private static final VIEW_INDEX_INVALID_TEMPLATE:I = 0x1

.field private static final VIEW_INDEX_LOADING_TEMPLATE:I = 0x0

.field private static final VIEW_INDEX_READY:I = 0x2


# instance fields
.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildTemplateName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildTemplatePreviewChannels$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildTemplatePreviewRoles$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildTemplatePreviewRolesLayout$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/create/WidgetGuildClone;

    const-string v3, "flipper"

    const-string v4, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildClone;

    const-string v6, "guildTemplateName"

    const-string v7, "getGuildTemplateName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildClone;

    const-string v6, "guildTemplatePreviewRolesLayout"

    const-string v7, "getGuildTemplatePreviewRolesLayout()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildClone;

    const-string v6, "guildTemplatePreviewChannels"

    const-string v7, "getGuildTemplatePreviewChannels()Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildClone;

    const-string v6, "guildTemplatePreviewRoles"

    const-string v7, "getGuildTemplatePreviewRoles()Lcom/discord/widgets/roles/RolesListView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->Companion:Lcom/discord/widgets/guilds/create/WidgetGuildClone$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;-><init>()V

    const v0, 0x7f0a04cf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0530

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplateName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0533

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplatePreviewRolesLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0531

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplatePreviewChannels$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0532

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplatePreviewRoles$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getGuildTemplateName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplateName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildTemplatePreviewChannels()Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplatePreviewChannels$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView;

    return-object v0
.end method

.method private final getGuildTemplatePreviewRoles()Lcom/discord/widgets/roles/RolesListView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplatePreviewRoles$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/roles/RolesListView;

    return-object v0
.end method

.method private final getGuildTemplatePreviewRolesLayout()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->guildTemplatePreviewRolesLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method public configureUI(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;)V
    .locals 6

    const-string/jumbo v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->configureUI(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildTemplates()Lcom/discord/stores/StoreGuildTemplates;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildTemplates;->clearDynamicLinkGuildTemplateCode()V

    sget-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Uninitialized;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_3

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v0, :cond_9

    check-cast p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v0

    sget-object v2, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_3

    :cond_3
    sget-object v2, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_3

    :cond_4
    instance-of v0, v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;->getGuildTemplate()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildTemplate;->getSerializedSourceGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v2

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplateName()Landroid/widget/TextView;

    move-result-object v2

    const v4, 0x7f040154

    invoke-static {v2, v4}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplateName()Landroid/widget/TextView;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplateName()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildTemplate;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplatePreviewChannels()Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView;

    move-result-object p1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v2

    const-string v4, "guild.channels"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/discord/widgets/guilds/create/WidgetGuildClone$configureUI$$inlined$sortedBy$1;

    invoke-direct {v4}, Lcom/discord/widgets/guilds/create/WidgetGuildClone$configureUI$$inlined$sortedBy$1;-><init>()V

    invoke-static {v2, v4}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/discord/widgets/guilds/create/GuildTemplateChannelsView;->updateView(Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getRoles()Ljava/util/List;

    move-result-object p1

    const-string v0, "guild.roles"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildClone$configureUI$$inlined$sortedBy$2;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone$configureUI$$inlined$sortedBy$2;-><init>()V

    invoke-static {p1, v0}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/discord/models/domain/ModelGuildRole;

    const-string v5, "role"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuildRole;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "@everyone"

    invoke-static {v4, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v3

    if-eqz v4, :cond_6

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplatePreviewRolesLayout()Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v3

    if-eqz v2, :cond_8

    goto :goto_2

    :cond_8
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplatePreviewRoles()Lcom/discord/widgets/roles/RolesListView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildClone;->getGuildTemplatePreviewRoles()Lcom/discord/widgets/roles/RolesListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040495

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/roles/RolesListView;->updateView(Ljava/util/List;I)V

    :cond_9
    :goto_3
    return-void
.end method

.method public createViewModelFactory()Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;
    .locals 8

    const v0, 0x7f12057c

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "getString(R.string.creat\u2026fault_server_name_format)"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "guild_template_code"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_SOURCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    move-object v6, v0

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;-><init>(Ljava/lang/String;Lcom/discord/widgets/guilds/create/StockGuildTemplate;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0206

    return v0
.end method
