.class public final Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;
.super Lf/a/b/l0;
.source "WidgetGuildCreateViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;,
        Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;,
        Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;,
        Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticLocation:Ljava/lang/String;

.field private final customTitle:Ljava/lang/String;

.field private final defaultGuildNameFormat:Ljava/lang/String;

.field private didTrackCreateGuildViewed:Z

.field private final eventsSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final selectedGuildStore:Lcom/discord/stores/StoreGuildSelected;

.field private final showChannelPrompt:Z

.field private final stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/widgets/guilds/create/StockGuildTemplate;Lcom/discord/stores/StoreGuildSelected;Lrx/Observable;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/widgets/guilds/create/StockGuildTemplate;",
            "Lcom/discord/stores/StoreGuildSelected;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p7

    const-string v7, "defaultGuildNameFormat"

    invoke-static {p1, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "restAPI"

    invoke-static {p2, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v7, "stockGuildTemplate"

    invoke-static {p3, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "selectedGuildStore"

    invoke-static {v4, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v7, "storeObservable"

    invoke-static {v5, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "analyticLocation"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v7}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->defaultGuildNameFormat:Ljava/lang/String;

    iput-object v2, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object v3, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    iput-object v4, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->selectedGuildStore:Lcom/discord/stores/StoreGuildSelected;

    iput-object v5, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->storeObservable:Lrx/Observable;

    move/from16 v1, p6

    iput-boolean v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->showChannelPrompt:Z

    iput-object v6, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->analyticLocation:Ljava/lang/String;

    move-object/from16 v1, p8

    iput-object v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->customTitle:Ljava/lang/String;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->eventsSubject:Lrx/subjects/PublishSubject;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v5, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    new-instance v9, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$1;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleGuildCreateFailure(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->handleGuildCreateFailure()V

    return-void
.end method

.method public static final synthetic access$handleGuildCreateSuccess(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->handleGuildCreateSuccess(Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method private final emitEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->eventsSubject:Lrx/subjects/PublishSubject;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleGuildCreateFailure()V
    .locals 9
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->copy$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private final handleGuildCreateSuccess(Lcom/discord/models/domain/ModelGuild;)V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->selectedGuildStore:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildSelected;->set(J)V

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->showChannelPrompt:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchChannelPrompt;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchChannelPrompt;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->emitEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchInviteShareScreen;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchInviteShareScreen;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->emitEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final createGuild(Landroid/content/res/Resources;)V
    .locals 13
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildNameText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildIconUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy()Z

    move-result v4

    if-eqz v4, :cond_1

    return-void

    :cond_1
    invoke-static {v0}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$ShowToast;

    const v0, 0x7f121664

    invoke-direct {p1, v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$ShowToast;-><init>(I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->emitEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V

    return-void

    :cond_2
    invoke-virtual {v3}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v4

    instance-of v4, v4, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    if-eqz v4, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v4

    check-cast v4, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    invoke-virtual {v4}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;->getGuildTemplate()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuildTemplate;->getCode()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;

    invoke-direct {v5, v0, v1}, Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v4, v5}, Lcom/discord/utilities/rest/RestAPI;->createGuildFromTemplate(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    new-instance v5, Lcom/discord/restapi/RestAPIParams$CreateGuild;

    iget-object v6, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    invoke-virtual {v6, p1}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->getChannels(Landroid/content/res/Resources;)Ljava/util/List;

    move-result-object p1

    iget-object v6, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    invoke-virtual {v6}, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->getSystemChannelId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v5, v0, v1, p1, v6}, Lcom/discord/restapi/RestAPIParams$CreateGuild;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;)V

    invoke-virtual {v4, v5}, Lcom/discord/utilities/rest/RestAPI;->createGuild(Lcom/discord/restapi/RestAPIParams$CreateGuild;)Lrx/Observable;

    move-result-object p1

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p1, p0, v2, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$createGuild$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$createGuild$1;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;)V

    const/4 v9, 0x0

    new-instance v8, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$createGuild$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$createGuild$2;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/16 v9, 0x17

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->copy$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public final handleStoreState(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;)V
    .locals 8
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v2

    iget-object v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->defaultGuildNameFormat:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const-string v0, "java.lang.String.format(this, *args)"

    invoke-static {v4, v3, v1, v0}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->customTitle:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;-><init>(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {p0, v7}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->didTrackCreateGuildViewed:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$StoreState;->getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;->getGuildTemplate()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    iget-object v2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->analyticLocation:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->createGuildViewed(Lcom/discord/widgets/guilds/create/StockGuildTemplate;Lcom/discord/models/domain/ModelGuildTemplate;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->eventsSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventsSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final updateGuildIconUri(Ljava/lang/String;)V
    .locals 9
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    move-object v4, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->copy$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final updateGuildName(Ljava/lang/String;)V
    .locals 9
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "guildName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v3, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->copy$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
