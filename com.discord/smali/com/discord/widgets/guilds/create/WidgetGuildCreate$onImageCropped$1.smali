.class public final Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageCropped$1;
.super Ljava/lang/Object;
.source "WidgetGuildCreate.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->onImageCropped(Landroid/net/Uri;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageCropped$1;->this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageCropped$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageCropped$1;->this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    invoke-static {v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->access$getViewModel$p(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->updateGuildIconUri(Ljava/lang/String;)V

    return-void
.end method
