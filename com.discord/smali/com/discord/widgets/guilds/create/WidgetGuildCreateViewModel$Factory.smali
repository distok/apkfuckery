.class public final Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetGuildCreateViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final analyticLocation:Ljava/lang/String;

.field private final customTitle:Ljava/lang/String;

.field private final defaultGuildNameFormat:Ljava/lang/String;

.field private final guildTemplateCode:Ljava/lang/String;

.field private final showChannelPrompt:Z

.field private final stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/widgets/guilds/create/StockGuildTemplate;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "defaultGuildNameFormat"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticLocation"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->defaultGuildNameFormat:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    iput-object p3, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->guildTemplateCode:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->showChannelPrompt:Z

    iput-object p5, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->analyticLocation:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->customTitle:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    iget-object v2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->defaultGuildNameFormat:Ljava/lang/String;

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->stockGuildTemplate:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CREATE:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    :goto_0
    move-object v4, v0

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v1

    iget-object v6, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->guildTemplateCode:Ljava/lang/String;

    if-nez v6, :cond_1

    sget-object v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;

    new-instance v6, Lg0/l/e/j;

    invoke-direct {v6, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildTemplates()Lcom/discord/stores/StoreGuildTemplates;

    move-result-object v0

    iget-object v6, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->guildTemplateCode:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/discord/stores/StoreGuildTemplates;->observeGuildTemplate(Ljava/lang/String;)Lrx/Observable;

    move-result-object v6

    :goto_1
    sget-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory$create$1;->INSTANCE:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory$create$1;

    if-eqz v0, :cond_2

    new-instance v7, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$sam$rx_functions_Func2$0;

    invoke-direct {v7, v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$sam$rx_functions_Func2$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v7

    :cond_2
    check-cast v0, Lrx/functions/Func2;

    invoke-static {v1, v6, v0}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v6

    const-string v0, "Observable.combineLatest\u2026 ::StoreState\n          )"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->showChannelPrompt:Z

    iget-object v8, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->analyticLocation:Ljava/lang/String;

    iget-object v9, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;->customTitle:Ljava/lang/String;

    move-object v1, p1

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;-><init>(Ljava/lang/String;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/widgets/guilds/create/StockGuildTemplate;Lcom/discord/stores/StoreGuildSelected;Lrx/Observable;ZLjava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method
