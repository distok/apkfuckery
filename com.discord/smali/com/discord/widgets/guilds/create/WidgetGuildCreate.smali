.class public Lcom/discord/widgets/guilds/create/WidgetGuildCreate;
.super Lcom/discord/app/AppFragment;
.source "WidgetGuildCreate.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;

.field private static final EXTRA_CUSTOM_TITLE:Ljava/lang/String; = "com.discord.intent.extra.EXTRA_CUSTOM_TITLE"

.field private static final EXTRA_SHOW_CHANNEL_PROMPT:Ljava/lang/String; = "com.discord.intent.extra.EXTRA_SHOW_CHANNEL_PROMPT"

.field private static final EXTRA_TEMPLATE:Ljava/lang/String; = "com.discord.intent.extra.EXTRA_TEMPLATE"

.field private static final GUIDELINES_URL:Ljava/lang/String; = "https://discord.com/guidelines"


# instance fields
.field private currentImageUri:Ljava/lang/String;

.field private final guidelinesTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildCreateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildCreateIconUploader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final iconUploaderImage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final iconUploaderPlaceholderGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final screenTitleView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final serverNameView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v3, "screenTitleView"

    const-string v4, "getScreenTitleView()Lcom/discord/views/ScreenTitleView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v6, "guildCreateButton"

    const-string v7, "getGuildCreateButton()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v6, "serverNameView"

    const-string v7, "getServerNameView()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v6, "iconUploaderImage"

    const-string v7, "getIconUploaderImage()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v6, "iconUploaderPlaceholderGroup"

    const-string v7, "getIconUploaderPlaceholderGroup()Landroidx/constraintlayout/widget/Group;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v6, "guildCreateIconUploader"

    const-string v7, "getGuildCreateIconUploader()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    const-string v6, "guidelinesTextView"

    const-string v7, "getGuidelinesTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->Companion:Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a04d4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->screenTitleView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04ce

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->guildCreateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04d2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->serverNameView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a057e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->iconUploaderImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0580

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->iconUploaderPlaceholderGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04d1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->guildCreateIconUploader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04d0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->guidelinesTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getServerNameView$p(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getServerNameView()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->viewModel:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->handleEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->viewModel:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    return-void
.end method

.method private final getCommunityGuidelinesText()Ljava/lang/CharSequence;
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "https://discord.com/guidelines"

    aput-object v2, v0, v1

    const v1, 0x7f120580

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "getString(R.string.creat\u2026idelines, GUIDELINES_URL)"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "requireContext()"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private final getGuidelinesTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->guidelinesTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildCreateButton()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->guildCreateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getGuildCreateIconUploader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->guildCreateIconUploader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getIconUploaderImage()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->iconUploaderImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getIconUploaderPlaceholderGroup()Landroidx/constraintlayout/widget/Group;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->iconUploaderPlaceholderGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/Group;

    return-object v0
.end method

.method private final getScreenTitleView()Lcom/discord/views/ScreenTitleView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->screenTitleView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/ScreenTitleView;

    return-object v0
.end method

.method private final getServerNameView()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->serverNameView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event;)V
    .locals 9

    instance-of v0, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$ShowToast;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$ShowToast;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$ShowToast;->getStringResId()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchChannelPrompt;

    const-string v1, "requireContext()"

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->Companion:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchChannelPrompt;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchChannelPrompt;->getGuildId()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;->launch(Landroid/content/Context;J)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchInviteShareScreen;

    if-eqz v0, :cond_2

    sget-object v2, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare;->Companion:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchInviteShareScreen;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Event$LaunchInviteShareScreen;->getGuildId()J

    move-result-wide v4

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v8, "Guild Create"

    invoke-virtual/range {v2 .. v8}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;->launch(Landroid/content/Context;JLjava/lang/Long;ZLjava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public configureUI(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;)V
    .locals 11

    const-string/jumbo v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Uninitialized;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildNameText()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getServerNameView()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getServerNameView()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildNameText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->currentImageUri:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildIconUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getIconUploaderPlaceholderGroup()Landroidx/constraintlayout/widget/Group;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildIconUri()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getIconUploaderImage()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildIconUri()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f07006d

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getGuildIconUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->currentImageUri:Ljava/lang/String;

    :cond_4
    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getCustomTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getScreenTitleView()Lcom/discord/views/ScreenTitleView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->getCustomTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/views/ScreenTitleView;->setTitle(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getGuildCreateButton()Lcom/discord/views/LoadingButton;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    :cond_6
    :goto_2
    return-void
.end method

.method public createViewModelFactory()Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;
    .locals 8

    const v0, 0x7f12057c

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "getString(R.string.creat\u2026fault_server_name_format)"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_SOURCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    move-object v6, v0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_TEMPLATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    move-object v3, v0

    check-cast v3, Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v4, "com.discord.intent.extra.EXTRA_SHOW_CHANNEL_PROMPT"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_CUSTOM_TITLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;

    const/4 v4, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;-><init>(Ljava/lang/String;Lcom/discord/widgets/guilds/create/StockGuildTemplate;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0208

    return v0
.end method

.method public onImageChosen(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppFragment;->onImageChosen(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    const-string v0, "parentFragmentManager"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageChosen$1;

    invoke-direct {v5, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageChosen$1;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    sget-object v6, Lcom/discord/dialogs/ImageUploadDialog$PreviewType;->GUILD_AVATAR:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/images/MGImages;->prepareImageUpload(Landroid/net/Uri;Ljava/lang/String;Landroidx/fragment/app/FragmentManager;Lcom/miguelgaeta/media_picker/MediaPicker$Provider;Lrx/functions/Action1;Lcom/discord/dialogs/ImageUploadDialog$PreviewType;)V

    return-void
.end method

.method public onImageCropped(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppFragment;->onImageCropped(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageCropped$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onImageCropped$1;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    invoke-static {v0, p1, p2, v1}, Lcom/discord/utilities/images/MGImages;->requestDataUrl(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lrx/functions/Action1;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getGuildCreateIconUploader()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$1;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getGuidelinesTextView()Landroid/widget/TextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getCommunityGuidelinesText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getServerNameView()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$2;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    invoke-static {p1, p0, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->getGuildCreateButton()Lcom/discord/views/LoadingButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$3;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->createViewModelFactory()Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$Factory;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(this, \u2026ateViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    iput-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->viewModel:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->viewModel:Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
