.class public abstract Lcom/discord/widgets/guilds/create/ChannelTemplate;
.super Ljava/lang/Object;
.source "StockGuildTemplate.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/create/ChannelTemplate$NormalChannel;,
        Lcom/discord/widgets/guilds/create/ChannelTemplate$SystemChannel;
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/Long;

.field private final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/create/ChannelTemplate;->id:Ljava/lang/Long;

    iput-object p2, p0, Lcom/discord/widgets/guilds/create/ChannelTemplate;->name:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/guilds/create/ChannelTemplate;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/ChannelTemplate;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/ChannelTemplate;->name:Ljava/lang/String;

    return-object v0
.end method
