.class public final Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;
.super Ljava/lang/Object;
.source "StockGuildTemplate.kt"


# static fields
.field private static final SYSTEM_CHANNEL_ID:J = 0xbL


# direct methods
.method public static final varargs synthetic access$createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/discord/widgets/guilds/create/StockGuildTemplateKt;->createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static final varargs createCategorySection(Ljava/lang/String;JI[Lcom/discord/widgets/guilds/create/ChannelTemplate;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JI[",
            "Lcom/discord/widgets/guilds/create/ChannelTemplate;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p4

    new-instance v8, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v2, 0x4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v8

    move-object v4, p0

    invoke-direct/range {v1 .. v7}, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V

    invoke-static {v8}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    array-length v3, v0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v0, v4

    new-instance v13, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/create/ChannelTemplate;->getId()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/create/ChannelTemplate;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v6, v13

    move/from16 v7, p3

    invoke-direct/range {v6 .. v12}, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v2, v13}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1, v2}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
