.class public final Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$3;
.super Ljava/lang/Object;
.source "WidgetGuildCreate.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$3;->this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$3;->this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    invoke-static {p1}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->access$getViewModel$p(Lcom/discord/widgets/guilds/create/WidgetGuildCreate;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$onViewBound$3;->this$0:Lcom/discord/widgets/guilds/create/WidgetGuildCreate;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel;->createGuild(Landroid/content/res/Resources;)V

    return-void
.end method
