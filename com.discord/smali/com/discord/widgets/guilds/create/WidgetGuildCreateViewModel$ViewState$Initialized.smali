.class public final Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;
.super Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;
.source "WidgetGuildCreateViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Initialized"
.end annotation


# instance fields
.field private final customTitle:Ljava/lang/String;

.field private final guildIconUri:Ljava/lang/String;

.field private final guildNameText:Ljava/lang/String;

.field private final guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

.field private final isBusy:Z


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "guildTemplate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildNameText"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    iput-object p2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    iput-object p5, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->copy(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;
    .locals 7

    const-string v0, "guildTemplate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildNameText"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;-><init>(Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    iget-object v1, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    iget-boolean v1, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCustomTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildIconUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildNameText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildTemplate()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isBusy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Initialized(guildTemplate="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildTemplate:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildNameText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildNameText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", guildIconUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->guildIconUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isBusy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->isBusy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", customTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/create/WidgetGuildCreateViewModel$ViewState$Initialized;->customTitle:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
