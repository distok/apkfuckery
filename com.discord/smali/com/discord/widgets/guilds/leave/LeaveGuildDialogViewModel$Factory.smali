.class public final Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;
.super Ljava/lang/Object;
.source "LeaveGuildDialogViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J

.field private final guildStore:Lcom/discord/stores/StoreGuilds;

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;


# direct methods
.method public constructor <init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "guildStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->guildStore:Lcom/discord/stores/StoreGuilds;

    iput-object p4, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public synthetic constructor <init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p3

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    sget-object p4, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p4}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p4

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method private final observeStoreState(J)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->guildStore:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory$observeStoreState$1;

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "guildStore\n          .ob\u2026            }\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    iget-wide v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->guildId:J

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->observeStoreState(J)Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;-><init>(JLrx/Observable;Lcom/discord/utilities/rest/RestAPI;)V

    return-object p1
.end method
