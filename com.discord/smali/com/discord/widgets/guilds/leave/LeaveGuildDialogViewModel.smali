.class public final Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;
.super Lf/a/b/l0;
.source "LeaveGuildDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState;,
        Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;,
        Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event;,
        Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final guildId:J

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLrx/Observable;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;",
            ">;",
            "Lcom/discord/utilities/rest/RestAPI;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->storeStateObservable:Lrx/Observable;

    iput-object p4, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 p1, 0x2

    invoke-static {p3, p0, v0, p1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    new-instance v7, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$1;-><init>(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->handleStoreState(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;)V

    return-void
.end method

.method public static final synthetic access$onLeaveFailed(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->onLeaveFailed()V

    return-void
.end method

.method public static final synthetic access$onLeaveSucceeded(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->onLeaveSucceeded()V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;->isLoading()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    instance-of v1, p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState$Valid;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    check-cast p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState$Valid;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;-><init>(Lcom/discord/models/domain/ModelGuild;Z)V

    invoke-virtual {p0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Invalid;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private final onLeaveFailed()V
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;Lcom/discord/models/domain/ModelGuild;ZILjava/lang/Object;)Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private final onLeaveSucceeded()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event$Dismiss;->INSTANCE:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event$Dismiss;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->guildId:J

    return-wide v0
.end method

.method public final getRestAPI()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final getStoreStateObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->storeStateObservable:Lrx/Observable;

    return-object v0
.end method

.method public final leaveGuild()V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-static {v0, v2, v1, v1, v2}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;Lcom/discord/models/domain/ModelGuild;ZILjava/lang/Object;)Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v3, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->guildId:J

    invoke-virtual {v0, v3, v4}, Lcom/discord/utilities/rest/RestAPI;->leaveGuild(J)Lrx/Observable;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v9, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$leaveGuild$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$leaveGuild$1;-><init>(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;)V

    const/4 v8, 0x0

    new-instance v7, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$leaveGuild$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$leaveGuild$2;-><init>(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;)V

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
