.class public final Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;
.super Lcom/discord/app/AppDialog;
.source "WidgetLeaveGuildDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;


# instance fields
.field private final body$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final cancelBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final confirmBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final header$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;

    const-string v3, "header"

    const-string v4, "getHeader()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;

    const-string v6, "body"

    const-string v7, "getBody()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;

    const-string v6, "confirmBtn"

    const-string v7, "getConfirmBtn()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;

    const-string v6, "cancelBtn"

    const-string v7, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->Companion:Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a063e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a063d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->body$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a063c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->confirmBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a063b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->cancelBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->configureUI(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;)Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->viewModel:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->handleEvent(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->viewModel:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState;)V
    .locals 6

    instance-of v0, p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->getHeader()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f120f17

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    check-cast p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {p0, v1, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->getBody()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f120f16

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(\n             \u2026.guild.name\n            )"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lf/a/j/b/b/g;->b(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Valid;->isLoading()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$ViewState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->dismiss()V

    :cond_1
    :goto_0
    return-void
.end method

.method private final getBody()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->body$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCancelBtn()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->cancelBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getConfirmBtn()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->confirmBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event$Dismiss;->INSTANCE:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Event$Dismiss;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->dismiss()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d007a

    return v0
.end method

.method public onResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppDialog;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$1;-><init>(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->getCancelBtn()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$2;-><init>(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->viewModel:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$3;

    invoke-direct {v9, p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$3;-><init>(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->viewModel:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$4;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$onResume$4;-><init>(Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppDialog;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p2, "com.discord.intent.extra.EXTRA_GUILD_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance p2, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, p0, p2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p2, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    invoke-virtual {p1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProvider(this, \u2026ewModel::class.java\n    )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    iput-object p1, p0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->viewModel:Lcom/discord/widgets/guilds/leave/LeaveGuildDialogViewModel;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "missing guild id"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
