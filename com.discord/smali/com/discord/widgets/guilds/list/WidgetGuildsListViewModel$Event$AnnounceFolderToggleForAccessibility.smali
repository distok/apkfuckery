.class public final Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;
.super Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;
.source "WidgetGuildsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnnounceFolderToggleForAccessibility"
.end annotation


# instance fields
.field private final item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)V
    .locals 1

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;ILjava/lang/Object;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->copy(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    return-object v0
.end method

.method public final copy(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;
    .locals 1

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;

    invoke-direct {v0, p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;-><init>(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    iget-object p1, p1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getItem()Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "AnnounceFolderToggleForAccessibility(item="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;->item:Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
