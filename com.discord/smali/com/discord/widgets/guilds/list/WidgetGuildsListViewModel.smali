.class public final Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;
.super Lf/a/b/l0;
.source "WidgetGuildsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;,
        Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;,
        Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;,
        Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private currentTargetOperation:Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private wasLeftPanelOpened:Z

.field private wasOnHomeTab:Z


# direct methods
.method public constructor <init>(Lrx/Observable;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "storeObservable\n        \u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;

    new-instance v8, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$1;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$createGuildItem(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;Lcom/discord/models/domain/ModelGuild;JJLjava/util/Set;Ljava/util/Map;ILjava/util/Map;Ljava/util/Set;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;
    .locals 0

    invoke-direct/range {p0 .. p14}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->createGuildItem(Lcom/discord/models/domain/ModelGuild;JJLjava/util/Set;Ljava/util/Map;ILjava/util/Map;Ljava/util/Set;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->handleStoreState(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V

    return-void
.end method

.method private final createDirectMessageItems(Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/domain/ModelNotificationSettings;)Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ")",
            "Lkotlin/sequences/Sequence<",
            "Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {p1}, Lx/r/q;->filterNotNull(Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$createDirectMessageItems$1;

    invoke-direct {v0, p3, p2}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$createDirectMessageItems$1;-><init>(Lcom/discord/models/domain/ModelNotificationSettings;Ljava/util/Map;)V

    invoke-static {p1, v0}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {}, Lcom/discord/models/domain/ModelChannel;->getSortByNameAndType()Ljava/util/Comparator;

    move-result-object p3

    const-string v0, "ModelChannel.getSortByNameAndType()"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p3}, Lx/r/q;->sortedWith(Lkotlin/sequences/Sequence;Ljava/util/Comparator;)Lkotlin/sequences/Sequence;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$createDirectMessageItems$2;

    invoke-direct {p3, p2}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$createDirectMessageItems$2;-><init>(Ljava/util/Map;)V

    invoke-static {p1, p3}, Lx/r/q;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    return-object p1
.end method

.method private final createGuildItem(Lcom/discord/models/domain/ModelGuild;JJLjava/util/Set;Ljava/util/Map;ILjava/util/Map;Ljava/util/Set;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "JJ",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;I",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v3, p9

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v4, p7

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelNotificationSettings;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelNotificationSettings;->isMuted()Z

    move-result v6

    if-ne v6, v4, :cond_0

    const/4 v11, 0x0

    goto :goto_0

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v7, p6

    invoke-interface {v7, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    move v11, v6

    :goto_0
    cmp-long v6, v0, p2

    if-nez v6, :cond_1

    const/4 v12, 0x1

    goto :goto_1

    :cond_1
    const/4 v12, 0x0

    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v8, p4, v6

    if-lez v8, :cond_5

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_3

    :cond_2
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    cmp-long v8, p4, v6

    if-nez v8, :cond_4

    const/4 v6, 0x1

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_3

    const/4 v14, 0x1

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v14, 0x0

    :goto_4
    move-object/from16 p2, p0

    move-wide/from16 p3, v0

    move-object/from16 p5, v3

    move-object/from16 p6, p13

    move-object/from16 p7, p14

    invoke-direct/range {p2 .. p7}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->shouldDisplayVideoIconOnGuild(JLcom/discord/models/domain/ModelNotificationSettings;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v15

    new-instance v0, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v2, p10

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    const/16 v16, 0x0

    const/16 v18, 0x100

    const/16 v19, 0x0

    move-object v7, v0

    move-object/from16 v8, p1

    move/from16 v9, p8

    move-object/from16 v13, p11

    move-object/from16 v17, p12

    invoke-direct/range {v7 .. v19}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;-><init>(Lcom/discord/models/domain/ModelGuild;IZZZLjava/lang/Long;ZZZLjava/lang/Boolean;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final handleStoreState(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V
    .locals 44

    move-object/from16 v15, p0

    move-object/from16 v14, p1

    new-instance v13, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSortedGuilds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v0, Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedGuildId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    cmp-long v5, v1, v3

    if-gtz v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;-><init>(Z)V

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getPrivateChannels()Ljava/util/Map;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getMentionCounts()Ljava/util/Map;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getGuildSettings()Ljava/util/Map;

    move-result-object v2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelNotificationSettings;

    invoke-direct {v15, v0, v1, v2}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->createDirectMessageItems(Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/domain/ModelNotificationSettings;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-static {v13, v0}, Lx/h/f;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSortedGuilds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/StoreGuildsSorted$Entry;

    instance-of v2, v1, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getChannelIds()Ljava/util/Map;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getMentionCounts()Ljava/util/Map;

    move-result-object v5

    invoke-direct {v15, v1, v2, v4, v5}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->sumMentionCountsForGuild(JLjava/util/Map;Ljava/util/Map;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v10, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    instance-of v2, v1, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getGuilds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getChannelIds()Ljava/util/Map;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getMentionCounts()Ljava/util/Map;

    move-result-object v6

    invoke-direct {v15, v2, v3, v5, v6}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->sumMentionCountsForGuild(JLjava/util/Map;Ljava/util/Map;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v10, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSortedGuilds()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$$inlined$filterIsInstance$1;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$$inlined$filterIsInstance$1;

    invoke-static {v0, v1}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$lurkerGuildItems$1;

    invoke-direct {v1, v14}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$lurkerGuildItems$1;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V

    invoke-static {v0, v1}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$lurkerGuildItems$2;

    invoke-direct {v1, v15, v14, v10}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$lurkerGuildItems$2;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;Ljava/util/HashMap;)V

    invoke-static {v0, v1}, Lx/r/q;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v9

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSortedGuilds()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$2;

    invoke-direct {v1, v14}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$handleStoreState$2;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;)V

    invoke-static {v0, v1}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    check-cast v0, Lx/r/g;

    new-instance v7, Lx/r/g$a;

    invoke-direct {v7, v0}, Lx/r/g$a;-><init>(Lx/r/g;)V

    :goto_3
    invoke-virtual {v7}, Lx/r/g$a;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {v7}, Lx/r/g$a;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGuildsSorted$Entry;

    instance-of v1, v0, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;

    if-eqz v1, :cond_5

    check-cast v0, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedGuildId()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedVoiceChannelId()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getUnreadGuildIds()Ljava/util/Set;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getGuildSettings()Ljava/util/Map;

    move-result-object v17

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    goto :goto_4

    :cond_4
    move-object/from16 v0, v16

    :goto_4
    const-string v11, "mentionCountsByGuild[folder.guild.id] ?: 0"

    invoke-static {v0, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getChannelIds()Ljava/util/Map;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getLurkingGuildIds()Ljava/util/Set;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getAllApplicationStreamContexts()Ljava/util/Map;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getAllChannelPermissions()Ljava/util/Map;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v25, v7

    move-object/from16 v7, v17

    move-object/from16 v26, v8

    move v8, v11

    move-object v11, v9

    move-object/from16 v9, v19

    move-object/from16 v27, v10

    move-object/from16 v10, v20

    move-object v15, v11

    move-object/from16 v11, v21

    move-object/from16 v12, v22

    move-object/from16 v28, v13

    move-object/from16 v13, v23

    move-object/from16 v14, v24

    invoke-direct/range {v0 .. v14}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->createGuildItem(Lcom/discord/models/domain/ModelGuild;JJLjava/util/Set;Ljava/util/Map;ILjava/util/Map;Ljava/util/Set;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    move-result-object v0

    move-object/from16 v14, v26

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_13

    :cond_5
    move-object/from16 v25, v7

    move-object v14, v8

    move-object v15, v9

    move-object/from16 v27, v10

    move-object/from16 v28, v13

    instance-of v1, v0, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;

    if-eqz v1, :cond_17

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getOpenFolderIds()Ljava/util/Set;

    move-result-object v1

    move-object/from16 v17, v0

    check-cast v17, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v13, "mentionCountsByGuild[guild.id] ?: 0"

    if-nez v0, :cond_11

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getGuilds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v11, 0x0

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelGuild;

    if-nez v3, :cond_7

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedGuildId()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_6

    goto :goto_6

    :cond_6
    const/4 v3, 0x0

    goto :goto_7

    :cond_7
    :goto_6
    const/4 v3, 0x1

    :goto_7
    if-nez v4, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getChannelIds()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-eqz v4, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedVoiceChannelId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_8

    :cond_8
    const/4 v4, 0x0

    :goto_8
    if-eqz v4, :cond_9

    goto :goto_9

    :cond_9
    const/4 v4, 0x0

    goto :goto_a

    :cond_a
    :goto_9
    const/4 v4, 0x1

    :goto_a
    if-nez v11, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getUnreadGuildIds()Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getGuildSettings()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelNotificationSettings;

    if-eqz v6, :cond_b

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelNotificationSettings;->isMuted()Z

    move-result v6

    const/4 v12, 0x1

    if-eq v6, v12, :cond_d

    goto :goto_b

    :cond_b
    const/4 v12, 0x1

    :goto_b
    const/4 v6, 0x1

    goto :goto_c

    :cond_c
    const/4 v12, 0x1

    :cond_d
    const/4 v6, 0x0

    :goto_c
    move v11, v6

    goto :goto_d

    :cond_e
    const/4 v12, 0x1

    :goto_d
    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v10, v27

    invoke-virtual {v10, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_f

    goto :goto_e

    :cond_f
    move-object/from16 v5, v16

    :goto_e
    invoke-static {v5, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v2, v5

    move-object/from16 v27, v10

    goto/16 :goto_5

    :cond_10
    move-object/from16 v10, v27

    const/4 v12, 0x1

    move/from16 v38, v2

    move/from16 v36, v3

    move/from16 v37, v4

    move/from16 v39, v11

    goto :goto_f

    :cond_11
    move-object/from16 v10, v27

    const/4 v12, 0x1

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    :goto_f
    new-instance v1, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getId()J

    move-result-wide v30

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getColor()Ljava/lang/Integer;

    move-result-object v32

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getGuilds()Ljava/util/List;

    move-result-object v35

    const/16 v40, 0x0

    const/16 v41, 0x200

    const/16 v42, 0x0

    move-object/from16 v29, v1

    move/from16 v34, v0

    invoke-direct/range {v29 .. v42}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;-><init>(JLjava/lang/Integer;Ljava/lang/String;ZLjava/util/List;ZZIZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_16

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getGuilds()Ljava/util/List;

    move-result-object v0

    new-instance v11, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v11, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    const/4 v0, 0x0

    :goto_10
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v20, v0, 0x1

    if-ltz v0, :cond_14

    check-cast v1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedGuildId()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getSelectedVoiceChannelId()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getUnreadGuildIds()Ljava/util/Set;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getGuildSettings()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_12

    goto :goto_11

    :cond_12
    move-object/from16 v8, v16

    :goto_11
    invoke-static {v8, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getChannelIds()Ljava/util/Map;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getLurkingGuildIds()Ljava/util/Set;

    move-result-object v21

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getId()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v17 .. v17}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;->getGuilds()Ljava/util/List;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v12

    if-ne v12, v0, :cond_13

    const/4 v12, 0x1

    goto :goto_12

    :cond_13
    const/4 v12, 0x0

    :goto_12
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getAllApplicationStreamContexts()Ljava/util/Map;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getAllChannelPermissions()Ljava/util/Map;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v26, v10

    move-object/from16 v10, v21

    move-object/from16 v21, v15

    move-object v15, v11

    move-object/from16 v11, v22

    const/16 v22, 0x1

    move-object/from16 v27, v13

    move-object/from16 v13, v23

    move-object/from16 v43, v14

    move-object/from16 v14, v24

    invoke-direct/range {v0 .. v14}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->createGuildItem(Lcom/discord/models/domain/ModelGuild;JJLjava/util/Set;Ljava/util/Map;ILjava/util/Map;Ljava/util/Set;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v11, v15

    move/from16 v0, v20

    move-object/from16 v15, v21

    move-object/from16 v10, v26

    move-object/from16 v13, v27

    move-object/from16 v14, v43

    const/4 v12, 0x1

    goto/16 :goto_10

    :cond_14
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 v0, 0x0

    throw v0

    :cond_15
    move-object/from16 v26, v10

    move-object v0, v14

    move-object/from16 v21, v15

    const/16 v22, 0x1

    move-object v15, v11

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_15

    :cond_16
    move-object/from16 v26, v10

    move-object v0, v14

    move-object/from16 v21, v15

    goto :goto_14

    :cond_17
    :goto_13
    move-object v0, v14

    move-object/from16 v21, v15

    move-object/from16 v26, v27

    :goto_14
    const/16 v22, 0x1

    :goto_15
    move-object/from16 v15, p0

    move-object/from16 v14, p1

    move-object v8, v0

    move-object/from16 v9, v21

    move-object/from16 v7, v25

    move-object/from16 v10, v26

    move-object/from16 v13, v28

    const/4 v11, 0x0

    const/4 v12, 0x1

    goto/16 :goto_3

    :cond_18
    move-object v0, v8

    move-object v2, v9

    move-object v1, v13

    const/16 v22, 0x1

    invoke-static {v1, v2}, Lx/h/f;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    sget-object v2, Lcom/discord/widgets/guilds/list/GuildListItem$DividerItem;->INSTANCE:Lcom/discord/widgets/guilds/list/GuildListItem$DividerItem;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getUnavailableGuilds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_19

    new-instance v0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getUnavailableGuilds()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v0, v3}, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_19
    sget-object v0, Lcom/discord/widgets/guilds/list/GuildListItem$CreateItem;->INSTANCE:Lcom/discord/widgets/guilds/list/GuildListItem$CreateItem;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isNewUser()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/discord/widgets/guilds/list/GuildListItem$HelpItem;->INSTANCE:Lcom/discord/widgets/guilds/list/GuildListItem$HelpItem;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1a
    sget-object v0, Lcom/discord/widgets/guilds/list/GuildListItem$SpaceItem;->INSTANCE:Lcom/discord/widgets/guilds/list/GuildListItem$SpaceItem;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->getChannelIds()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;-><init>(Ljava/util/List;ZZ)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isLeftPanelOpened()Z

    move-result v1

    move-object/from16 v2, p0

    iget-boolean v3, v2, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->wasLeftPanelOpened:Z

    if-ne v1, v3, :cond_1b

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isOnHomeTab()Z

    move-result v1

    iget-boolean v3, v2, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->wasOnHomeTab:Z

    if-eq v1, v3, :cond_1d

    :cond_1b
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isLeftPanelOpened()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isOnHomeTab()Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, v2, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v3, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$FocusFirstElement;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$FocusFirstElement;

    iget-object v1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v3}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isLeftPanelOpened()Z

    move-result v1

    iput-boolean v1, v2, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->wasLeftPanelOpened:Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;->isOnHomeTab()Z

    move-result v1

    iput-boolean v1, v2, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->wasOnHomeTab:Z

    :cond_1d
    invoke-virtual {v2, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final move(IILjava/lang/Long;)V
    .locals 24

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v14, p3

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    if-nez v4, :cond_0

    const/4 v3, 0x0

    :cond_0
    move-object v15, v3

    check-cast v15, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    if-eqz v15, :cond_9

    invoke-virtual {v15}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/guilds/list/GuildListItem;

    const/4 v13, 0x2

    const/16 v16, 0x0

    const/16 v17, 0x1

    if-gt v1, v2, :cond_2

    sub-int v5, v1, v2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-lt v5, v13, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v5, 0x1

    :goto_1
    iget-object v6, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->currentTargetOperation:Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

    if-nez v6, :cond_4

    if-nez v5, :cond_4

    instance-of v5, v4, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v5, :cond_3

    move-object v5, v4

    check-cast v5, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5, v14}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v5, 0x1

    :goto_3
    if-nez v5, :cond_5

    return-void

    :cond_5
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v12}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->untargetCurrentTarget(Ljava/util/ArrayList;)V

    if-ge v1, v2, :cond_6

    add-int/lit8 v2, v2, -0x1

    :cond_6
    move v11, v2

    instance-of v2, v4, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v2, :cond_7

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v12, v11, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_4

    :cond_7
    instance-of v2, v4, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v2, :cond_8

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-object/from16 v18, v4

    check-cast v18, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3df

    const/16 v21, 0x0

    move-object/from16 v1, v18

    move-object/from16 v7, p3

    move/from16 v22, v11

    move-object/from16 v11, v19

    move-object/from16 v23, v12

    move/from16 v12, v20

    move-object/from16 v19, v15

    const/4 v15, 0x2

    move-object/from16 v13, v21

    invoke-static/range {v1 .. v13}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;Lcom/discord/models/domain/ModelGuild;IZZZLjava/lang/Long;ZZZLjava/lang/Boolean;ILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    move-result-object v1

    move/from16 v3, v22

    move-object/from16 v2, v23

    invoke-virtual {v2, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    new-array v1, v15, [Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v16

    aput-object v14, v1, v17

    invoke-static {v1}, Lx/h/f;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->rebuildFolders(Ljava/util/ArrayList;Ljava/util/Set;)V

    goto :goto_5

    :cond_8
    :goto_4
    move-object v2, v12

    move-object/from16 v19, v15

    :goto_5
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object/from16 v4, v19

    move-object v5, v2

    invoke-static/range {v4 .. v9}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;Ljava/util/List;ZZILjava/lang/Object;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_9
    return-void
.end method

.method private final performTargetOperation(Ljava/util/ArrayList;II)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/discord/widgets/guilds/list/GuildListItem;",
            ">;II)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual/range {p1 .. p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "editingList[fromPosition]"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/discord/widgets/guilds/list/GuildListItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "editingList[toPosition]"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v4, v2, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_6

    instance-of v7, v3, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v7, :cond_6

    new-instance v4, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    sget-object v7, Lx/o/c;->b:Lx/o/c$a;

    sget-object v7, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v7}, Lx/o/c;->nextLong()J

    move-result-wide v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/discord/models/domain/ModelGuild;

    check-cast v3, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v8

    aput-object v8, v7, v6

    check-cast v2, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v7}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_0

    :cond_0
    const/4 v15, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v15, 0x1

    :goto_1
    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isConnectedToVoice()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isConnectedToVoice()Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_2

    :cond_2
    const/16 v16, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/16 v16, 0x1

    :goto_3
    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isUnread()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isUnread()Z

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_4

    :cond_4
    const/16 v18, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/16 v18, 0x1

    :goto_5
    const/16 v19, 0x0

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getMentionCount()I

    move-result v3

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getMentionCount()I

    move-result v2

    add-int v17, v2, v3

    move-object v8, v4

    invoke-direct/range {v8 .. v19}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;-><init>(JLjava/lang/Integer;Ljava/lang/String;ZLjava/util/List;ZZIZZ)V

    invoke-virtual {v0, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "editingList.removeAt(fromPosition)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_c

    :cond_6
    if-eqz v4, :cond_d

    instance-of v4, v3, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v4, :cond_d

    move-object v7, v3

    check-cast v7, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v7}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getGuilds()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lx/h/f;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    check-cast v2, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v4

    invoke-static {v3, v4}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    invoke-virtual {v7}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->isAnyGuildSelected()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_7

    goto :goto_6

    :cond_7
    const/4 v14, 0x0

    goto :goto_7

    :cond_8
    :goto_6
    const/4 v14, 0x1

    :goto_7
    invoke-virtual {v7}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->isUnread()Z

    move-result v3

    if-nez v3, :cond_a

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isUnread()Z

    move-result v3

    if-eqz v3, :cond_9

    goto :goto_8

    :cond_9
    const/16 v17, 0x0

    goto :goto_9

    :cond_a
    :goto_8
    const/16 v17, 0x1

    :goto_9
    const/16 v16, 0x0

    invoke-virtual {v7}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->isAnyGuildConnectedToVoice()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isConnectedToVoice()Z

    move-result v2

    if-eqz v2, :cond_b

    goto :goto_a

    :cond_b
    const/4 v15, 0x0

    goto :goto_b

    :cond_c
    :goto_a
    const/4 v15, 0x1

    :goto_b
    const/16 v18, 0x0

    const/16 v19, 0x8f

    const/16 v20, 0x0

    invoke-static/range {v7 .. v20}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;JLjava/lang/Integer;Ljava/lang/String;ZLjava/util/List;ZZIZZILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_d
    :goto_c
    return-void
.end method

.method private final rebuildFolders(Ljava/util/ArrayList;Ljava/util/Set;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/discord/widgets/guilds/list/GuildListItem;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    instance-of v5, v4, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v5, :cond_1

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    if-eqz v6, :cond_4

    goto :goto_2

    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v9, v8, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v9, :cond_6

    check-cast v8, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-virtual {v8}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getFolderId()J

    move-result-wide v8

    cmp-long v10, v8, v3

    if-nez v10, :cond_6

    const/4 v8, 0x1

    goto :goto_5

    :cond_6
    const/4 v8, 0x0

    :goto_5
    if-eqz v8, :cond_7

    goto :goto_6

    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_8
    const/4 v7, -0x1

    :goto_6
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    const-string v6, "null cannot be cast to non-null type com.discord.widgets.guilds.list.GuildListItem.FolderItem"

    invoke-static {v5, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v8, v5

    check-cast v8, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    if-eqz v3, :cond_9

    goto :goto_7

    :cond_9
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :goto_7
    move-object v14, v3

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3ef

    const/16 v21, 0x0

    invoke-static/range {v8 .. v21}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;JLjava/lang/Integer;Ljava/lang/String;ZLjava/util/List;ZZIZZILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    move-result-object v3

    invoke-virtual {v0, v7, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_a
    return-void
.end method

.method private final shouldDisplayVideoIconOnGuild(JLcom/discord/models/domain/ModelNotificationSettings;Ljava/util/Map;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    return v1

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelNotificationSettings;->isMuted()Z

    move-result p3

    if-ne p3, v0, :cond_3

    return v1

    :cond_3
    invoke-interface {p4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p3

    instance-of p4, p3, Ljava/util/Collection;

    if-eqz p4, :cond_5

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result p4

    if-eqz p4, :cond_5

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_6
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_4

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/discord/utilities/streams/StreamContext;

    invoke-virtual {p4}, Lcom/discord/utilities/streams/StreamContext;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    cmp-long v4, v2, p1

    if-nez v4, :cond_7

    const-wide/16 v2, 0x400

    invoke-virtual {p4}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p4

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    invoke-interface {p5, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Long;

    invoke-static {v2, v3, p4}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p4

    if-eqz p4, :cond_7

    const/4 p4, 0x1

    goto :goto_2

    :cond_7
    const/4 p4, 0x0

    :goto_2
    if-eqz p4, :cond_6

    :goto_3
    return v0
.end method

.method private final sumMentionCountsForGuild(JLjava/util/Map;Ljava/util/Map;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    new-instance p3, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p3}, Lx/h/f;->sumOfInt(Ljava/lang/Iterable;)I

    move-result p1

    return p1

    :cond_2
    return p2
.end method

.method private final untargetCurrentTarget(Ljava/util/ArrayList;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/discord/widgets/guilds/list/GuildListItem;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->currentTargetOperation:Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;->component2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "editingList[toPosition]"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v4, v3, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v4, :cond_0

    move-object v5, v3

    check-cast v5, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x2ff

    const/16 v17, 0x0

    invoke-static/range {v5 .. v17}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;Lcom/discord/models/domain/ModelGuild;IZZZLjava/lang/Long;ZZZLjava/lang/Boolean;ILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    move-result-object v3

    goto :goto_0

    :cond_0
    instance-of v4, v3, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v4, :cond_1

    move-object v5, v3

    check-cast v5, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1ff

    const/16 v18, 0x0

    invoke-static/range {v5 .. v18}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;JLjava/lang/Integer;Ljava/lang/String;ZLjava/util/List;ZZIZZILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    move-result-object v3

    :goto_0
    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->currentTargetOperation:Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

    return-void
.end method


# virtual methods
.method public final listenForEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final moveAbove(II)V
    .locals 3

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v1, v0, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lcom/discord/widgets/guilds/list/GuildListItem$HelpItem;

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    instance-of v0, v0, Lcom/discord/widgets/guilds/list/GuildListItem$CreateItem;

    if-eqz v0, :cond_4

    :goto_0
    invoke-direct {p0, p1, p2, v2}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->move(IILjava/lang/Long;)V

    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "invalid target"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    return-void
.end method

.method public final moveBelow(II)V
    .locals 3

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v1, v0, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getFolderId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_2
    :goto_0
    add-int/lit8 p2, p2, 0x1

    invoke-direct {p0, p1, p2, v2}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->move(IILjava/lang/Long;)V

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "invalid target"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    return-void
.end method

.method public final onDrop()Z
    .locals 14

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->currentTargetOperation:Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

    if-eqz v4, :cond_1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v5}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->untargetCurrentTarget(Ljava/util/ArrayList;)V

    invoke-virtual {v4}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;->getFromPosition()I

    move-result v0

    invoke-virtual {v4}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;->getTargetPosition()I

    move-result v6

    invoke-direct {p0, v5, v0, v6}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->performTargetOperation(Ljava/util/ArrayList;II)V

    move-object v0, v5

    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v6, v5, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v6, :cond_3

    new-instance v6, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;

    check-cast v5, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getFolderId()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getGuilds()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getColor()Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->getName()Ljava/lang/String;

    move-result-object v12

    move-object v7, v6

    invoke-direct/range {v7 .. v12}, Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;-><init>(JLjava/util/List;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    instance-of v6, v5, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v6, :cond_2

    check-cast v5, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v6

    if-nez v6, :cond_2

    new-instance v6, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v5

    invoke-direct {v6, v5}, Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildsSorted()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/discord/stores/StoreGuildsSorted;->setPositions(Ljava/util/List;)V

    new-instance v0, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/stores/StoreGuildsSorted$Entry;

    invoke-virtual {v5}, Lcom/discord/stores/StoreGuildsSorted$Entry;->asModelGuildFolder()Lcom/discord/models/domain/ModelGuildFolder;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    sget-object v3, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v3}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v3

    sget-object v5, Lcom/discord/restapi/RestAPIParams$UserSettings;->Companion:Lcom/discord/restapi/RestAPIParams$UserSettings$Companion;

    invoke-virtual {v5, v0}, Lcom/discord/restapi/RestAPIParams$UserSettings$Companion;->createWithGuildFolders(Ljava/util/List;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/discord/utilities/rest/RestAPI;->updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    const-class v6, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$onDrop$2;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$onDrop$2;

    const/16 v12, 0x1e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    if-eqz v4, :cond_6

    const/4 v1, 0x1

    :cond_6
    return v1
.end method

.method public final onItemClicked(Lcom/discord/widgets/guilds/list/GuildListItem;)V
    .locals 9

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreNavigation$PanelAction;->CLOSE:Lcom/discord/stores/StoreNavigation$PanelAction;

    invoke-static {p1, v0, v2, v1, v2}, Lcom/discord/stores/StoreNavigation;->setNavigationPanelAction$default(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildSelected;->set(J)V

    goto/16 :goto_1

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v1

    const-wide/16 v2, 0x0

    check-cast p1, Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel$default(Lcom/discord/utilities/channel/ChannelSelector;JJIILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowUnavailableGuilds;

    check-cast p1, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->getUnavailableGuildCount()I

    move-result p1

    invoke-direct {v1, p1}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowUnavailableGuilds;-><init>(I)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$FriendsItem;->isSelected()Z

    move-result p1

    if-eqz p1, :cond_4

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreNavigation$PanelAction;->CLOSE:Lcom/discord/stores/StoreNavigation$PanelAction;

    invoke-static {p1, v0, v2, v1, v2}, Lcom/discord/stores/StoreNavigation;->setNavigationPanelAction$default(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;ILjava/lang/Object;)V

    goto :goto_1

    :cond_4
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreGuildSelected;->set(J)V

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/discord/widgets/guilds/list/GuildListItem$CreateItem;->INSTANCE:Lcom/discord/widgets/guilds/list/GuildListItem$CreateItem;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowCreateGuild;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowCreateGuild;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/discord/widgets/guilds/list/GuildListItem$HelpItem;->INSTANCE:Lcom/discord/widgets/guilds/list/GuildListItem$HelpItem;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowHelp;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowHelp;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    :cond_7
    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v0, :cond_9

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExpandedGuildFolders()Lcom/discord/stores/StoreExpandedGuildFolders;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem;->getItemId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreExpandedGuildFolders;->closeFolder(J)V

    goto :goto_0

    :cond_8
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExpandedGuildFolders()Lcom/discord/stores/StoreExpandedGuildFolders;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem;->getItemId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreExpandedGuildFolders;->openFolder(J)V

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;

    invoke-direct {v1, v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$AnnounceFolderToggleForAccessibility;-><init>(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_9
    :goto_1
    return-void
.end method

.method public final onItemLongPressed(Lcom/discord/widgets/guilds/list/GuildListItem;)V
    .locals 4

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowChannelActions;

    check-cast p1, Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$PrivateChannelItem;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Event$ShowChannelActions;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final target(II)V
    .locals 20

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    if-nez v3, :cond_0

    const/4 v2, 0x0

    :cond_0
    move-object v3, v2

    check-cast v3, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    if-eqz v3, :cond_3

    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v4}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->untargetCurrentTarget(Ljava/util/ArrayList;)V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/guilds/list/GuildListItem;

    instance-of v5, v2, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v5, :cond_1

    move-object v6, v2

    check-cast v6, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x2ff

    const/16 v18, 0x0

    invoke-static/range {v6 .. v18}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;Lcom/discord/models/domain/ModelGuild;IZZZLjava/lang/Long;ZZZLjava/lang/Boolean;ILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    move-result-object v2

    goto :goto_0

    :cond_1
    instance-of v5, v2, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    if-eqz v5, :cond_2

    move-object v6, v2

    check-cast v6, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x1ff

    const/16 v19, 0x0

    invoke-static/range {v6 .. v19}, Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;->copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;JLjava/lang/Integer;Ljava/lang/String;ZLjava/util/List;ZZIZZILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$FolderItem;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

    move/from16 v5, p1

    invoke-direct {v2, v5, v1}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;-><init>(II)V

    iput-object v2, v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;->currentTargetOperation:Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$TargetOperation;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;Ljava/util/List;ZZILjava/lang/Object;)Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$ViewState$Loaded;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid target item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "targeting with no items"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
