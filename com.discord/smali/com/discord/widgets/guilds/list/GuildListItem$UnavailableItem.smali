.class public final Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;
.super Lcom/discord/widgets/guilds/list/GuildListItem;
.source "GuildListItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/list/GuildListItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnavailableItem"
.end annotation


# instance fields
.field private final unavailableGuildCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/widgets/guilds/list/GuildListItem;-><init>(JLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;IILjava/lang/Object;)Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->copy(I)Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    return v0
.end method

.method public final copy(I)Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    invoke-direct {v0, p1}, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;

    iget v0, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    iget p1, p1, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getUnavailableGuildCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "UnavailableItem(unavailableGuildCount="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/guilds/list/GuildListItem$UnavailableItem;->unavailableGuildCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
