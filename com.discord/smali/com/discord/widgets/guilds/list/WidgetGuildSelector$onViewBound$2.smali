.class public final Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;
.super Lx/m/c/k;
.source "WidgetGuildSelector.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/text/Editable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/guilds/list/WidgetGuildSelector;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;->this$0:Lcom/discord/widgets/guilds/list/WidgetGuildSelector;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/text/Editable;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;->invoke(Landroid/text/Editable;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;->this$0:Lcom/discord/widgets/guilds/list/WidgetGuildSelector;

    invoke-static {p1}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->access$getSearchInputSubject$p(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;->this$0:Lcom/discord/widgets/guilds/list/WidgetGuildSelector;

    invoke-static {v0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->access$getSearch$p(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
