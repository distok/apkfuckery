.class public abstract Lcom/discord/widgets/guilds/list/GuildListViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "GuildListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;,
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$FriendsViewHolder;,
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$PrivateChannelViewHolder;,
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$FolderViewHolder;,
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$SpaceViewHolder;,
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$SimpleViewHolder;
    }
.end annotation


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/list/GuildListViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final configureMentionsCount(Landroid/widget/TextView;I)V
    .locals 1

    const-string/jumbo v0, "textView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public onDragEnded(Z)V
    .locals 0

    return-void
.end method

.method public onDragStarted()V
    .locals 0

    return-void
.end method
