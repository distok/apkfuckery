.class public final Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;
.super Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;
.source "WidgetGuildListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoveBelow"
.end annotation


# instance fields
.field private final fromPosition:I

.field private final targetPosition:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    iput p2, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;IIILjava/lang/Object;)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->copy(II)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    return v0
.end method

.method public final copy(II)Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    iget v1, p1, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    iget p1, p1, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFromPosition()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    return v0
.end method

.method public final getTargetPosition()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "MoveBelow(fromPosition="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->fromPosition:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", targetPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildListAdapter$Operation$MoveBelow;->targetPosition:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
