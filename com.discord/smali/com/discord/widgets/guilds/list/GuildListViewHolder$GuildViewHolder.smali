.class public final Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;
.super Lcom/discord/widgets/guilds/list/GuildListViewHolder;
.source "GuildListViewHolder.kt"

# interfaces
.implements Lcom/discord/widgets/guilds/list/GuildsDragAndDropCallback$DraggableViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/list/GuildListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GuildViewHolder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$Companion;

.field private static final DEFAULT_AVATAR_TEXT_SIZE_SP:F = 14.0f

.field private static final TARGETED_AVATAR_TEXT_SIZE_DP:F = 8.0f


# instance fields
.field private data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

.field private final defaultAvatarSize:I

.field private final imageRequestSize:I

.field private isDragging:Z

.field private final itemApplicationStream:Landroid/view/View;

.field private final itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

.field private final itemAvatarContainer:Landroid/view/ViewGroup;

.field private final itemAvatarText:Landroid/widget/TextView;

.field private final itemMentions:Landroid/widget/TextView;

.field private final itemSelected:Landroid/view/View;

.field private final itemUnread:Landroid/view/View;

.field private final itemVoice:Landroid/view/View;

.field private final onClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onLongPressed:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final overlayColor:I

.field private final overlayColorInFolder:I

.field private final targetedAvatarMargin:I

.field private final targetedAvatarSize:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->Companion:Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .param p2    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "II",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClicked"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLongPressed"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/guilds/list/GuildListViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p2, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->overlayColor:I

    iput p3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->overlayColorInFolder:I

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->onClicked:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->onLongPressed:Lkotlin/jvm/functions/Function1;

    const p2, 0x7f0a0552

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "itemView.findViewById(R.\u2026.guilds_item_avatar_wrap)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const p3, 0x7f0a0550

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string p4, "itemView.findViewById(R.id.guilds_item_avatar)"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p4, 0x7f0a0551

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string p5, "itemView.findViewById(R.\u2026.guilds_item_avatar_text)"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p4, Landroid/widget/TextView;

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    const p4, 0x7f0a0558

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string p5, "itemView.findViewById(R.id.guilds_item_mentions)"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p4, Landroid/widget/TextView;

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemMentions:Landroid/widget/TextView;

    const p4, 0x7f0a055e

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string p5, "itemView.findViewById(R.id.guilds_item_voice)"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemVoice:Landroid/view/View;

    const p4, 0x7f0a054f

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string p5, "itemView.findViewById(R.\u2026_item_application_stream)"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemApplicationStream:Landroid/view/View;

    const p4, 0x7f0a055d

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string p5, "itemView.findViewById(R.id.guilds_item_unread)"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemUnread:Landroid/view/View;

    const p4, 0x7f0a055c

    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string p5, "itemView.findViewById(R.id.guilds_item_selected)"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemSelected:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const p5, 0x7f070067

    invoke-virtual {p4, p5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p4

    iput p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->defaultAvatarSize:I

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const p5, 0x7f0700dd

    invoke-virtual {p4, p5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p4

    iput p4, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->targetedAvatarSize:I

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p4, 0x7f0700dc

    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->targetedAvatarMargin:I

    const/4 p1, 0x1

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->setClipToOutline(Z)V

    invoke-virtual {p3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iget p1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p1}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result p1

    iput p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->imageRequestSize:I

    return-void
.end method

.method public static final synthetic access$getOnClicked$p(Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->onClicked:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getOnLongPressed$p(Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->onLongPressed:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method private final configureDraggingAlpha()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->isDragging:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    :goto_0
    return-void
.end method

.method private final configureGuildIconBackground(ZZZ)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    xor-int/lit8 v2, p1, 0x1

    if-eqz p2, :cond_0

    iget v3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->overlayColorInFolder:I

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->overlayColor:I

    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/discord/utilities/images/MGImages;->setCornerRadius(Landroid/widget/ImageView;FZLjava/lang/Integer;)V

    const/4 v0, 0x0

    if-eqz p3, :cond_2

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const p2, 0x7f0801ca

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const p2, 0x7f08015f

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    :goto_1
    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_3

    :cond_2
    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const p2, 0x7f0801c7

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_3

    :cond_3
    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    const p3, 0x7f080151

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    if-eqz p2, :cond_4

    const p1, 0x7f04013b

    goto :goto_2

    :cond_4
    const p1, 0x7f04013a

    :goto_2
    iget-object p2, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    invoke-static {p2, p1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p1

    iget-object p2, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarContainer:Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    :goto_3
    return-void
.end method

.method private final configureGuildIconImage(Lcom/discord/models/domain/ModelGuild;Z)V
    .locals 19

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->canHaveAnimatedServerIcon()Z

    move-result v1

    const-string v2, "itemAvatar.hierarchy"

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v5, 0x2

    const-string v6, "a"

    invoke-static {v1, v6, v4, v5}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v1

    if-ne v1, v3, :cond_0

    iget-object v1, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->o(ILandroid/graphics/drawable/Drawable;)V

    iget-object v1, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    iput v4, v1, Lf/g/g/e/f;->n:I

    iget v2, v1, Lf/g/g/e/f;->m:I

    if-ne v2, v3, :cond_1

    iput v4, v1, Lf/g/g/e/f;->m:I

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    const v5, 0x7f0800a9

    invoke-virtual {v1, v5}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->p(I)V

    iget-object v1, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x32

    iget-object v1, v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    iput v2, v1, Lf/g/g/e/f;->n:I

    iget v2, v1, Lf/g/g/e/f;->m:I

    if-ne v2, v3, :cond_1

    iput v4, v1, Lf/g/g/e/f;->m:I

    :cond_1
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->hasIcon()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xa

    const/4 v15, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p1

    move/from16 v7, p2

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Lcom/discord/models/domain/ModelGuild;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "?size="

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->imageRequestSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lx/m/c/j;->stringPlus(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iget-object v10, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    iget v13, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->imageRequestSize:I

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x70

    const/16 v18, 0x0

    move v12, v13

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private final configureGuildIconPositioning(Z)V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-static {v2, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget v3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->targetedAvatarMargin:I

    invoke-virtual {v0, v3, v3, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget v3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->targetedAvatarSize:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget v3, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->defaultAvatarSize:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    const/4 v3, 0x1

    const/high16 v4, 0x41000000    # 8.0f

    invoke-virtual {p1, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->targetedAvatarMargin:I

    invoke-virtual {v2, p1, p1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->targetedAvatarSize:I

    iput p1, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput p1, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    const/4 v3, 0x2

    const/high16 v4, 0x41600000    # 14.0f

    invoke-virtual {p1, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    const/4 p1, -0x1

    iput p1, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput p1, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    :goto_1
    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatar:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public canDrag()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isLurkingGuild()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final configure(Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;)V
    .locals 11

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v5

    if-eq v2, v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 v0, 0x1

    :goto_4
    iget-object v5, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isTargetedForFolderCreation()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_5

    :cond_5
    move-object v5, v1

    :goto_5
    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isTargetedForFolderCreation()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isTargetedForFolderCreation()Z

    move-result v6

    if-eq v5, v6, :cond_7

    :cond_6
    const/4 v5, 0x1

    goto :goto_6

    :cond_7
    const/4 v5, 0x0

    :goto_6
    iput-object p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    iget-object v6, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v7, "itemView"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v8

    invoke-virtual {v6, v8}, Landroid/view/View;->setSelected(Z)V

    iget-object v6, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v8, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$configure$1;

    invoke-direct {v8, p0, p1}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$configure$1;-><init>(Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;)V

    invoke-virtual {v6, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v8, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$configure$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder$configure$2;-><init>(Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;)V

    invoke-virtual {v6, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-lez v6, :cond_8

    const/4 v6, 0x1

    goto :goto_7

    :cond_8
    const/4 v6, 0x0

    :goto_7
    if-ne v6, v3, :cond_9

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v6

    const-string v8, "ICON_UNSET"

    invoke-static {v6, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v3

    if-eqz v6, :cond_9

    const/4 v6, 0x1

    goto :goto_8

    :cond_9
    const/4 v6, 0x0

    :goto_8
    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v8

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v9

    if-nez v9, :cond_b

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isTargetedForFolderCreation()Z

    move-result v9

    if-eqz v9, :cond_a

    goto :goto_9

    :cond_a
    const/4 v9, 0x0

    goto :goto_a

    :cond_b
    :goto_9
    const/4 v9, 0x1

    :goto_a
    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v10

    invoke-virtual {v10}, Lcom/discord/models/domain/ModelGuild;->hasIcon()Z

    move-result v10

    invoke-direct {p0, v8, v9, v10}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->configureGuildIconBackground(ZZZ)V

    if-eqz v5, :cond_c

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isTargetedForFolderCreation()Z

    move-result v5

    invoke-direct {p0, v5}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->configureGuildIconPositioning(Z)V

    :cond_c
    if-nez v0, :cond_d

    if-eqz v2, :cond_e

    :cond_d
    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v5

    invoke-direct {p0, v0, v5}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->configureGuildIconImage(Lcom/discord/models/domain/ModelGuild;Z)V

    :cond_e
    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->configureDraggingAlpha()V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    if-eqz v6, :cond_f

    goto :goto_b

    :cond_f
    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v1

    :goto_b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_11

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060292

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    goto :goto_c

    :cond_10
    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040153

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    :goto_c
    iget-object v1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemAvatarText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_11
    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemMentions:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getMentionCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/discord/widgets/guilds/list/GuildListViewHolder;->configureMentionsCount(Landroid/widget/TextView;I)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getMentionCount()I

    move-result v0

    if-lez v0, :cond_12

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "itemView.context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f10009c

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-virtual {v1, v2, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_d

    :cond_12
    const-string v0, ""

    :goto_d
    const-string v1, "if (mentionCount > 0) it\u2026tionCount\n      ) else \"\""

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v5, 0x7f120cb8

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    aput-object v0, v6, v3

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemVoice:Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isConnectedToVoice()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x0

    goto :goto_e

    :cond_13
    const/16 v1, 0x8

    :goto_e
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemApplicationStream:Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isConnectedToVoice()Z

    move-result v1

    if-nez v1, :cond_14

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getHasOngoingApplicationStream()Z

    move-result v1

    if-eqz v1, :cond_14

    goto :goto_f

    :cond_14
    const/4 v3, 0x0

    :goto_f
    if-eqz v3, :cond_15

    const/4 v1, 0x0

    goto :goto_10

    :cond_15
    const/16 v1, 0x8

    :goto_10
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemUnread:Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isUnread()Z

    move-result v1

    if-eqz v1, :cond_16

    const/4 v1, 0x0

    goto :goto_11

    :cond_16
    const/16 v1, 0x8

    :goto_11
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->itemSelected:Landroid/view/View;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isSelected()Z

    move-result p1

    if-eqz p1, :cond_17

    goto :goto_12

    :cond_17
    const/16 v4, 0x8

    :goto_12
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final getFolderId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->getFolderId()Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final isLastGuildInFolder()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isLastGuildInFolder()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final isTargetedForFolderCreation()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->data:Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/GuildListItem$GuildItem;->isTargetedForFolderCreation()Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public onDragEnded(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/discord/widgets/guilds/list/GuildListViewHolder;->onDragEnded(Z)V

    if-eqz p1, :cond_0

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->isDragging:Z

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->configureDraggingAlpha()V

    return-void
.end method

.method public onDragStarted()V
    .locals 1

    invoke-super {p0}, Lcom/discord/widgets/guilds/list/GuildListViewHolder;->onDragStarted()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->isDragging:Z

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/GuildListViewHolder$GuildViewHolder;->configureDraggingAlpha()V

    return-void
.end method
