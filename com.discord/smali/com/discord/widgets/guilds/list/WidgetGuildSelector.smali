.class public final Lcom/discord/widgets/guilds/list/WidgetGuildSelector;
.super Lcom/discord/app/AppFragment;
.source "WidgetGuildSelector.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;

.field public static final EXTRA_GUILD_ID:Ljava/lang/String; = "EXTRA_GUILD_ID"

.field public static final EXTRA_IGNORED_GUILD_IDS:Ljava/lang/String; = "EXTRA_IGNORED_GUILD_IDS"

.field public static final REQUEST_CODE_GUILD_SEARCH:I = 0x1


# instance fields
.field private adapter:Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;

.field private final ignoredGuildIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final search$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchInputSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;

    const-string v3, "search"

    const-string v4, "getSearch()Lcom/google/android/material/textfield/TextInputLayout;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;

    const-string v6, "recyclerView"

    const-string v7, "getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->Companion:Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0525

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->search$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0528

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->ignoredGuildIds:Ljava/util/Set;

    const-string v0, ""

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->searchInputSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->configureUI(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$getIgnoredGuildIds$p(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->ignoredGuildIds:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic access$getSearch$p(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->getSearch()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSearchInputSubject$p(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->searchInputSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method private final configureUI(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter$GuildItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->adapter:Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$configureUI$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$configureUI$1;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)V

    invoke-virtual {v0, p1, v1}, Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;->configure(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_0
    const-string p1, "adapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getSearch()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->search$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0214

    return v0
.end method

.method public onResume()V
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->searchInputSubject:Lrx/subjects/BehaviorSubject;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3, v1}, Lrx/Observable;->o(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGuildsSorted()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildsSorted;->getFlat()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$1;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$2;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)V

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026)\n        )\n      }\n    }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$3;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$3;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;

    new-instance v9, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$4;

    invoke-direct {v9, p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onResume$4;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->ignoredGuildIds:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IGNORED_GUILD_IDS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type kotlin.collections.ArrayList<com.discord.models.domain.GuildId /* = kotlin.Long */> /* = java.util.ArrayList<com.discord.models.domain.GuildId /* = kotlin.Long */> */"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;

    iput-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->adapter:Lcom/discord/widgets/guilds/list/WidgetGuildSearchAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->getSearch()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$1;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)V

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setStartIconOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->getSearch()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$onViewBound$2;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildSelector;)V

    invoke-static {p1, p0, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    return-void
.end method
