.class public final Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetGuildsListViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$Chunk;
    }
.end annotation


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string v0, "clock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;->clock:Lcom/discord/utilities/time/Clock;

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;)Lcom/discord/utilities/time/Clock;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method private final observeStores()Lrx/Observable;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$1;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$1;->invoke()Lrx/Observable;

    move-result-object v1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getLurking()Lcom/discord/stores/StoreLurking;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreLurking;->getLurkingGuildIds()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$2;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$2;

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExpandedGuildFolders()Lcom/discord/stores/StoreExpandedGuildFolders;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreExpandedGuildFolders;->get()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v4}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v4

    new-instance v5, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$3;

    move-object/from16 v10, p0

    invoke-direct {v5, v10}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$3;-><init>(Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;)V

    invoke-virtual {v4, v5}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    new-instance v5, Lcom/discord/utilities/streams/StreamContextService;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0xff

    const/16 v21, 0x0

    move-object v11, v5

    invoke-direct/range {v11 .. v21}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v5}, Lcom/discord/utilities/streams/StreamContextService;->getForAllStreamingUsers()Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/stores/StorePermissions;->observePermissionsForAllChannels()Lrx/Observable;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/stores/StoreNavigation;->observeLeftPanelState()Lrx/Observable;

    move-result-object v7

    sget-object v8, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$4;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$4;

    invoke-virtual {v7, v8}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getTabsNavigation()Lcom/discord/stores/StoreTabsNavigation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreTabsNavigation;->observeSelectedTab()Lrx/Observable;

    move-result-object v0

    sget-object v8, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$5;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$5;

    invoke-virtual {v0, v8}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v8

    sget-object v9, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$6;->INSTANCE:Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory$observeStores$6;

    invoke-static/range {v1 .. v9}, Lrx/Observable;->d(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func8;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026HomeTab\n        )\n      }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/discord/widgets/guilds/list/WidgetGuildsListViewModel;-><init>(Lrx/Observable;)V

    return-object p1
.end method
