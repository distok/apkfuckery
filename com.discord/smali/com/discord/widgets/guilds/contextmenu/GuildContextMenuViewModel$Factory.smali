.class public final Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;
.super Ljava/lang/Object;
.source "GuildContextMenuViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J

.field private final guildStore:Lcom/discord/stores/StoreGuilds;

.field private final readStateStore:Lcom/discord/stores/StoreReadStates;

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final selectedGuildStore:Lcom/discord/stores/StoreGuildSelected;

.field private final userStore:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreReadStates;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "guildStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readStateStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedGuildStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->guildStore:Lcom/discord/stores/StoreGuilds;

    iput-object p4, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->userStore:Lcom/discord/stores/StoreUser;

    iput-object p5, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->readStateStore:Lcom/discord/stores/StoreReadStates;

    iput-object p6, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->selectedGuildStore:Lcom/discord/stores/StoreGuildSelected;

    iput-object p7, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-void
.end method

.method public synthetic constructor <init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreReadStates;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v0

    move-object v6, v0

    goto :goto_2

    :cond_2
    move-object v6, p5

    :goto_2
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    move-object v7, v0

    goto :goto_3

    :cond_3
    move-object v7, p6

    :goto_3
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_4

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    move-object v8, v0

    goto :goto_4

    :cond_4
    move-object/from16 v8, p7

    :goto_4
    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreReadStates;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method private final observeStoreState(J)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->guildStore:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->userStore:Lcom/discord/stores/StoreUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->readStateStore:Lcom/discord/stores/StoreReadStates;

    invoke-virtual {v2, p1, p2}, Lcom/discord/stores/StoreReadStates;->getIsUnread(J)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->selectedGuildStore:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {p2}, Lcom/discord/stores/StoreGuildSelected;->observeSelectedGuildId()Lrx/Observable;

    move-result-object p2

    sget-object v2, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;

    invoke-static {v0, v1, p1, p2, v2}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026      )\n        }\n      }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->guildId:J

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->observeStoreState(J)Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;-><init>(JLrx/Observable;Lcom/discord/utilities/rest/RestAPI;)V

    return-object p1
.end method
