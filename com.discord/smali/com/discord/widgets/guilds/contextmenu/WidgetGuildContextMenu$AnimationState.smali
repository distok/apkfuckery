.class public final enum Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;
.super Ljava/lang/Enum;
.source "WidgetGuildContextMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnimationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

.field public static final enum ANIMATING_IN:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

.field public static final enum ANIMATING_OUT:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    const-string v2, "ANIMATING_IN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->ANIMATING_IN:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    const-string v2, "ANIMATING_OUT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->ANIMATING_OUT:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->$VALUES:[Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;
    .locals 1

    const-class v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;
    .locals 1

    sget-object v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->$VALUES:[Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    invoke-virtual {v0}, [Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    return-object v0
.end method
