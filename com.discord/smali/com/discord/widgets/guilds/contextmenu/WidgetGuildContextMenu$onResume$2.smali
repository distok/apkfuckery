.class public final synthetic Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$onResume$2;
.super Lx/m/c/i;
.source "WidgetGuildContextMenu.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V
    .locals 7

    const-class v3, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const/4 v1, 0x1

    const-string v4, "handleEvent"

    const-string v5, "handleEvent(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$onResume$2;->invoke(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    invoke-static {v0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->access$handleEvent(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V

    return-void
.end method
