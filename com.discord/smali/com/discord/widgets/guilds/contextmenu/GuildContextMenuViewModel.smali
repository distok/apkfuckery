.class public final Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;
.super Lf/a/b/l0;
.source "GuildContextMenuViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;,
        Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;,
        Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;,
        Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final guildId:J

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLrx/Observable;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;",
            ">;",
            "Lcom/discord/utilities/rest/RestAPI;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->storeStateObservable:Lrx/Observable;

    iput-object p4, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 p1, 0x2

    invoke-static {p3, p0, v0, p1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    new-instance v7, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$1;-><init>(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getEventSubject$p(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->handleStoreState(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;)V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    instance-of v0, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

    check-cast p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->getMyUserId()J

    move-result-wide v5

    const/4 v7, 0x1

    const/4 v8, 0x0

    cmp-long v9, v3, v5

    if-eqz v9, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->getSelectedGuildId()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v9

    cmp-long p1, v4, v9

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    invoke-direct {v0, v1, v2, v3, v7}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;-><init>(Lcom/discord/models/domain/ModelGuild;ZZZ)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Invalid;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_3
    :goto_2
    return-void
.end method


# virtual methods
.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->guildId:J

    return-wide v0
.end method

.method public final getRestAPI()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final getStoreStateObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->storeStateObservable:Lrx/Observable;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onMarkAsReadClicked()V
    .locals 13

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->ackGuild(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v3, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    new-instance v10, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$onMarkAsReadClicked$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$onMarkAsReadClicked$1;-><init>(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
