.class public final Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;
.super Lcom/discord/app/AppFragment;
.source "WidgetGuildContextMenu.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;,
        Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;

.field private static final FRAGMENT_TAG:Ljava/lang/String; = "WidgetGuildContextMenu"

.field private static final SCREEN_BOTTOM_BUFFER:I

.field private static final VIEW_CONTAINER_TAG:Ljava/lang/String; = "WidgetGuildContextMenuViewContainer"

.field private static isShowingContextMenu:Z


# instance fields
.field private animationState:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

.field private animator:Landroid/animation/Animator;

.field private final guildContextMenuCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final header$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final leaveGuildOption$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final markAsReadOption$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final moreOptionsOption$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationsOption$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const-string v3, "guildContextMenuCard"

    const-string v4, "getGuildContextMenuCard()Landroidx/cardview/widget/CardView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const-string v6, "header"

    const-string v7, "getHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const-string v6, "markAsReadOption"

    const-string v7, "getMarkAsReadOption()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const-string v6, "notificationsOption"

    const-string v7, "getNotificationsOption()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const-string v6, "leaveGuildOption"

    const-string v7, "getLeaveGuildOption()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const-string v6, "moreOptionsOption"

    const-string v7, "getMoreOptionsOption()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->Companion:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;

    const/16 v0, 0x60

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    sput v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->SCREEN_BOTTOM_BUFFER:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a04c7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->guildContextMenuCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04c8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04cb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->markAsReadOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04cd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->notificationsOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04ca

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->leaveGuildOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04cc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->moreOptionsOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->configureUI(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$doCircularRemove(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->doCircularRemove()V

    return-void
.end method

.method public static final synthetic access$getAnimationState$p(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animationState:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    return-object p0
.end method

.method public static final synthetic access$getSCREEN_BOTTOM_BUFFER$cp()I
    .locals 1

    sget v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->SCREEN_BOTTOM_BUFFER:I

    return v0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->viewModel:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->handleEvent(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$isShowingContextMenu$cp()Z
    .locals 1

    sget-boolean v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->isShowingContextMenu:Z

    return v0
.end method

.method public static final synthetic access$setAnimationState$p(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animationState:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    return-void
.end method

.method public static final synthetic access$setShowingContextMenu$cp(Z)V
    .locals 0

    sput-boolean p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->isShowingContextMenu:Z

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->viewModel:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState;)V
    .locals 2

    instance-of v0, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->configureValidUI(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->Companion:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "requireActivity()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;->hide(Landroidx/fragment/app/FragmentActivity;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final configureValidUI(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getMarkAsReadOption()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$1;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getNotificationsOption()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$2;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getLeaveGuildOption()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$3;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$3;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getMoreOptionsOption()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getLeaveGuildOption()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;->getShowLeaveGuild()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getMarkAsReadOption()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;->getShowMarkAsRead()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_2

    const/4 v3, 0x1

    :cond_2
    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->doCircularReveal()V

    :cond_3
    return-void
.end method

.method private final doCircularRemove()V
    .locals 7

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animationState:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->ANIMATING_IN:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    :cond_0
    sget-object v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->Companion:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const-string v3, "requireActivity()"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$Companion;->hide(Landroidx/fragment/app/FragmentActivity;Z)V

    return-void

    :cond_1
    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->ANIMATING_OUT:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    if-ne v0, v1, :cond_2

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v5, v3

    int-to-double v3, v4

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->isAttachedToWindow()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v2, v0, v3, v5}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animator:Landroid/animation/Animator;

    iput-object v1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animationState:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    const-string v1, "animator"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$doCircularRemove$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$doCircularRemove$1;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final doCircularReveal()V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isAttachedToWindow()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-double v4, v2

    int-to-double v2, v3

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v1, v0, v4, v2}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animator:Landroid/animation/Animator;

    sget-object v2, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;->ANIMATING_IN:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    iput-object v2, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->animationState:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$AnimationState;

    const-string v2, "animator"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    new-instance v2, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$doCircularReveal$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$doCircularReveal$1;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->getGuildContextMenuCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final getGuildContextMenuCard()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->guildContextMenuCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->header$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getLeaveGuildOption()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->leaveGuildOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMarkAsReadOption()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->markAsReadOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMoreOptionsOption()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->moreOptionsOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNotificationsOption()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->notificationsOption$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event$Dismiss;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Event$Dismiss;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->doCircularRemove()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0207

    return v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->isShowingContextMenu:Z

    return-void
.end method

.method public onResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->viewModel:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$onResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$onResume$1;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->viewModel:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$onResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$onResume$2;-><init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p2, "com.discord.intent.extra.EXTRA_GUILD_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance p2, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v9}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreReadStates;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, p0, p2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p2, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    invoke-virtual {p1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProvider(this, \u2026ewModel::class.java\n    )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    iput-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->viewModel:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "missing guild id"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
