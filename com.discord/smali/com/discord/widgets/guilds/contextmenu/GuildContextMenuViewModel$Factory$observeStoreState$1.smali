.class public final Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "GuildContextMenuViewModel.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory;->observeStoreState(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;
    .locals 8

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Invalid;

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    const-string v0, "me"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    const-string p2, "isUnread"

    invoke-static {p3, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const-string p2, "selectedGuildId"

    invoke-static {p4, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;-><init>(Lcom/discord/models/domain/ModelGuild;JZJ)V

    move-object p1, v7

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Lcom/discord/models/domain/ModelUser;

    check-cast p3, Ljava/lang/Boolean;

    check-cast p4, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
