.class public final Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;
.super Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;
.source "GuildContextMenuViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Valid"
.end annotation


# instance fields
.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final isGuildUnread:Z

.field private final myUserId:J

.field private final selectedGuildId:J


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;JZJ)V
    .locals 1

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-wide p2, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    iput-boolean p4, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    iput-wide p5, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;Lcom/discord/models/domain/ModelGuild;JZJILjava/lang/Object;)Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-wide p2, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p4, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    :cond_2
    move p8, p4

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-wide p5, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    :cond_3
    move-wide v2, p5

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move p6, p8

    move-wide p7, v2

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->copy(Lcom/discord/models/domain/ModelGuild;JZJ)Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    return-wide v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    return v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    return-wide v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;JZJ)Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;
    .locals 8

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;-><init>(Lcom/discord/models/domain/ModelGuild;JZJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    iget-wide v2, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    iget-boolean v1, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    iget-wide v2, p1, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getMyUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    return-wide v0
.end method

.method public final getSelectedGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final isGuildUnread()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Valid(guild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->myUserId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", isGuildUnread="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->isGuildUnread:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", selectedGuildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$StoreState$Valid;->selectedGuildId:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
