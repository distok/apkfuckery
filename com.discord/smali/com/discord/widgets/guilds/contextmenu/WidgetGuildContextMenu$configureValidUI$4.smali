.class public final Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;
.super Ljava/lang/Object;
.source "WidgetGuildContextMenu.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->configureValidUI(Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

.field public final synthetic this$0:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;->this$0:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    iput-object p2, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;->$viewState:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    iget-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;->this$0:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    invoke-static {p1}, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;->access$doCircularRemove(Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;)V

    sget-object v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->Companion:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;

    iget-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;->this$0:Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string p1, "parentFragmentManager"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;->$viewState:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;->isGuildSelected()Z

    move-result v2

    iget-object p1, p0, Lcom/discord/widgets/guilds/contextmenu/WidgetGuildContextMenu$configureValidUI$4;->$viewState:Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/contextmenu/GuildContextMenuViewModel$ViewState$Valid;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;->show$default(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;Landroidx/fragment/app/FragmentManager;ZJJILjava/lang/Object;)V

    return-void
.end method
