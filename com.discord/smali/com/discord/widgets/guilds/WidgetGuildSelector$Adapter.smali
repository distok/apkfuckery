.class public final Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetGuildSelector.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/WidgetGuildSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;",
        ">;"
    }
.end annotation


# instance fields
.field private final dialog:Lcom/discord/widgets/guilds/WidgetGuildSelector;

.field private final noGuildStringId:I


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/guilds/WidgetGuildSelector;I)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialog"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object p2, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;->dialog:Lcom/discord/widgets/guilds/WidgetGuildSelector;

    iput p3, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;->noGuildStringId:I

    return-void
.end method

.method public static final synthetic access$getDialog$p(Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;)Lcom/discord/widgets/guilds/WidgetGuildSelector;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;->dialog:Lcom/discord/widgets/guilds/WidgetGuildSelector;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild;
    .locals 1

    const-string p2, "parent"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild;

    iget p2, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;->noGuildStringId:I

    const v0, 0x7f0d0217

    invoke-direct {p1, v0, p0, p2}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Adapter$ItemGuild;-><init>(ILcom/discord/widgets/guilds/WidgetGuildSelector$Adapter;I)V

    return-object p1
.end method
