.class public final Lcom/discord/widgets/guilds/WidgetGuildSelector$get$1;
.super Ljava/lang/Object;
.source "WidgetGuildSelector.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/WidgetGuildSelector;->get(ZLcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/LinkedHashMap<",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelGuild;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $filterFunction:Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;

.field public final synthetic $includeNoGuild:Z


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$get$1;->$filterFunction:Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;

    iput-boolean p2, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$get$1;->$includeNoGuild:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/LinkedHashMap;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/WidgetGuildSelector$get$1;->call(Ljava/util/LinkedHashMap;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/LinkedHashMap;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object p1

    const-string v0, "guilds.values"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$get$1;->$filterFunction:Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/models/domain/ModelGuild;

    invoke-interface {v0, v3}, Lcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;->includeGuild(Lcom/discord/models/domain/ModelGuild;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-boolean p1, p0, Lcom/discord/widgets/guilds/WidgetGuildSelector$get$1;->$includeNoGuild:Z

    if-eqz p1, :cond_2

    new-instance p1, Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    :cond_2
    sget-object p1, Lx/h/l;->d:Lx/h/l;

    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuild;

    new-instance v3, Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;

    invoke-direct {v3, v2}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Item;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {p1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
