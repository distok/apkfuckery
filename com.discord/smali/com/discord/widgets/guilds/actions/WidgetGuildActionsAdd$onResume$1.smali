.class public final Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd$onResume$1;
.super Ljava/lang/Object;
.source "WidgetGuildActionsAdd.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd$onResume$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd$onResume$1;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd$onResume$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd$onResume$1;->INSTANCE:Lcom/discord/widgets/guilds/actions/WidgetGuildActionsAdd$onResume$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8

    sget-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->Companion:Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;

    const-string v1, "it"

    const-string v2, "it.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "Create or Join Guild Modal"

    const/4 v5, 0x0

    const/16 v6, 0x16

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;->show$default(Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;Landroid/content/Context;Lcom/discord/widgets/guilds/create/StockGuildTemplate;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method
