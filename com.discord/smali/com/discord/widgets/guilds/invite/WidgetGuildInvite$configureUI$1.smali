.class public final Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;
.super Ljava/lang/Object;
.source "WidgetGuildInvite.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;->configureUI(Lcom/discord/stores/StoreInstantInvites$InviteState$Resolved;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $invite:Lcom/discord/models/domain/ModelInvite;

.field public final synthetic this$0:Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;Lcom/discord/models/domain/ModelInvite;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;->this$0:Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;->$invite:Lcom/discord/models/domain/ModelInvite;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    sget-object p1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;->$invite:Lcom/discord/models/domain/ModelInvite;

    const-string v1, "Accept Invite Page"

    invoke-virtual {p1, v0, v1}, Lcom/discord/utilities/rest/RestAPI;->postInviteCode(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;->this$0:Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;

    const/4 v1, 0x2

    invoke-static {p1, v0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/b/r;->a:Lf/a/b/r;

    iget-object v1, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;->this$0:Lcom/discord/widgets/guilds/invite/WidgetGuildInvite;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1$1;-><init>(Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;)V

    new-instance v3, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1$2;-><init>(Lcom/discord/widgets/guilds/invite/WidgetGuildInvite$configureUI$1;)V

    invoke-virtual {v0, v1, v2, v3}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
