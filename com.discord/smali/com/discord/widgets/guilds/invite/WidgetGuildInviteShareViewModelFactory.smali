.class public final Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;
.super Ljava/lang/Object;
.source "WidgetGuildInviteShareViewModelFactory.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# instance fields
.field private final guildId:J

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;J)V
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;->resources:Landroid/content/res/Resources;

    iput-wide p2, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;->guildId:J

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getInviteSettings()Lcom/discord/stores/StoreInviteSettings;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v5

    new-instance v6, Lcom/discord/widgets/guilds/invite/InviteGenerator;

    invoke-direct {v6}, Lcom/discord/widgets/guilds/invite/InviteGenerator;-><init>()V

    sget-object v0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->Companion:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;->create()Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;

    move-result-object v7

    iget-object v8, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;->resources:Landroid/content/res/Resources;

    iget-wide v10, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;->guildId:J

    const/4 v9, 0x1

    move-object v1, p1

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;-><init>(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreUser;Lcom/discord/widgets/guilds/invite/InviteGenerator;Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;Landroid/content/res/Resources;ZJ)V

    return-object p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;->guildId:J

    return-wide v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModelFactory;->resources:Landroid/content/res/Resources;

    return-object v0
.end method
