.class public final Lcom/discord/widgets/guilds/invite/WidgetInviteModel$Companion;
.super Ljava/lang/Object;
.source "WidgetInviteModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/invite/WidgetInviteModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/WidgetInviteModel$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Lcom/discord/models/domain/ModelInvite$Settings;Ljava/util/Map;Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/guilds/invite/WidgetInviteModel;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelInvite$Settings;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Lcom/discord/models/domain/ModelGuild;",
            ")",
            "Lcom/discord/widgets/guilds/invite/WidgetInviteModel;"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    move-object/from16 v2, p7

    const-string v3, "settings"

    move-object/from16 v6, p1

    invoke-static {v6, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "invitableChannels"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "inviteGenerationState"

    move-object/from16 v4, p3

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "me"

    move-object/from16 v11, p5

    invoke-static {v11, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "dms"

    move-object/from16 v12, p6

    invoke-static {v12, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->getLastGeneratedInvite()Lcom/discord/models/domain/ModelInvite;

    move-result-object v3

    new-instance v10, Ljava/util/ArrayList;

    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v10, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {}, Lcom/discord/models/domain/ModelChannel;->getSortByNameAndType()Ljava/util/Comparator;

    move-result-object v5

    invoke-static {v10, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-eqz v5, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelChannel;

    :goto_0
    move-object v5, v0

    goto :goto_1

    :cond_0
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelChannel;

    goto :goto_0

    :cond_1
    move-object v5, v7

    :goto_1
    const/4 v0, 0x1

    if-eqz v3, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelInvite;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelInvite;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    const-string v9, "generatedInvite.channel"

    invoke-static {v1, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v13

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v15

    cmp-long v1, v13, v15

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v2, :cond_3

    invoke-virtual/range {p7 .. p7}, Lcom/discord/models/domain/ModelGuild;->getVanityUrlCode()Ljava/lang/String;

    move-result-object v7

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->getState()Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    move-result-object v9

    sget-object v13, Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;->FAILURE:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    if-ne v9, v13, :cond_5

    :cond_4
    if-eqz v7, :cond_5

    invoke-static {v7, v2}, Lcom/discord/models/domain/ModelInvite;->createForVanityUrl(Ljava/lang/String;Lcom/discord/models/domain/ModelGuild;)Lcom/discord/models/domain/ModelInvite;

    move-result-object v1

    move-object v7, v1

    const/4 v9, 0x1

    goto :goto_3

    :cond_5
    move v9, v1

    move-object v7, v3

    :goto_3
    new-instance v1, Lcom/discord/widgets/guilds/invite/WidgetInviteModel;

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->getState()Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;->GENERATING:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    if-ne v2, v3, :cond_6

    const/4 v8, 0x1

    :cond_6
    move-object v4, v1

    move-object/from16 v6, p1

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    invoke-direct/range {v4 .. v12}, Lcom/discord/widgets/guilds/invite/WidgetInviteModel;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelInvite$Settings;Lcom/discord/models/domain/ModelInvite;ZZLjava/util/List;Lcom/discord/models/domain/ModelUser;Ljava/util/List;)V

    return-object v1
.end method
