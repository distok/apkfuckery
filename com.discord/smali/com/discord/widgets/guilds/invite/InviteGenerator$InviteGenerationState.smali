.class public final Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;
.super Ljava/lang/Object;
.source "InviteGenerator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/invite/InviteGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteGenerationState"
.end annotation


# instance fields
.field private final lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

.field private final state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;-><init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)V
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    sget-object p2, Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;->INIT:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;-><init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;ILjava/lang/Object;)Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->copy(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelInvite;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;-><init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    iget-object v1, p1, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    iget-object p1, p1, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLastGeneratedInvite()Lcom/discord/models/domain/ModelInvite;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    return-object v0
.end method

.method public final getState()Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelInvite;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "InviteGenerationState(lastGeneratedInvite="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->lastGeneratedInvite:Lcom/discord/models/domain/ModelInvite;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/invite/InviteGenerator$InviteGenerationState;->state:Lcom/discord/widgets/guilds/invite/InviteGenerator$GenerationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
