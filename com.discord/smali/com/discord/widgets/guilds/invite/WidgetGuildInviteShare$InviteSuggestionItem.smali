.class public abstract Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem;
.super Ljava/lang/Object;
.source "WidgetGuildInviteShare.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "InviteSuggestionItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$User;,
        Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$Channel;,
        Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;
    }
.end annotation


# static fields
.field public static final SearchNoResultsItem:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem;->SearchNoResultsItem:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem;-><init>()V

    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public abstract hasSentInvite()Z
.end method
