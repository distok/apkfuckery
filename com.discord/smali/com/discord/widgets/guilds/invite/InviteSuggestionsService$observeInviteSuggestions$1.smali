.class public final Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;
.super Ljava/lang/Object;
.source "InviteSuggestionsService.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->observeInviteSuggestions()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/guilds/invite/UserAffinityData;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;->this$0:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;->call(Ljava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/List;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/guilds/invite/UserAffinityData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;->this$0:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->getStoreUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    const-string v1, "affinityUserIds"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->observeUsers(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;->this$0:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->getStoreUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/discord/stores/StoreUserRelationships;->observe(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1$1;

    invoke-direct {v2, p1}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1$1;-><init>(Ljava/util/List;)V

    invoke-static {v0, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
