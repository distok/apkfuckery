.class public final Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;
.super Ljava/lang/Object;
.source "InviteSuggestionsService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;


# instance fields
.field private final storeChannels:Lcom/discord/stores/StoreChannels;

.field private final storeMessagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

.field private final storeUserAffinities:Lcom/discord/stores/StoreUserAffinities;

.field private final storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

.field private final storeUsers:Lcom/discord/stores/StoreUser;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->Companion:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreMessagesMostRecent;Lcom/discord/stores/StoreUserAffinities;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserRelationships;)V
    .locals 1

    const-string/jumbo v0, "storeChannels"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeMessagesMostRecent"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserAffinities"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUsers"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserRelationships"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeMessagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    iput-object p3, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUserAffinities:Lcom/discord/stores/StoreUserAffinities;

    iput-object p4, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUsers:Lcom/discord/stores/StoreUser;

    iput-object p5, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    return-void
.end method


# virtual methods
.method public final getStoreChannels()Lcom/discord/stores/StoreChannels;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-object v0
.end method

.method public final getStoreMessagesMostRecent()Lcom/discord/stores/StoreMessagesMostRecent;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeMessagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    return-object v0
.end method

.method public final getStoreUserAffinities()Lcom/discord/stores/StoreUserAffinities;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUserAffinities:Lcom/discord/stores/StoreUserAffinities;

    return-object v0
.end method

.method public final getStoreUserRelationships()Lcom/discord/stores/StoreUserRelationships;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    return-object v0
.end method

.method public final getStoreUsers()Lcom/discord/stores/StoreUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUsers:Lcom/discord/stores/StoreUser;

    return-object v0
.end method

.method public final observeInviteSuggestions()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/widgets/guilds/invite/InviteSuggestion;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeChannels:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannels;->observePrivateChannels()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeMessagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessagesMostRecent;->get()Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;->storeUserAffinities:Lcom/discord/stores/StoreUserAffinities;

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserAffinities;->getAffinityUserIds()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$1;-><init>(Lcom/discord/widgets/guilds/invite/InviteSuggestionsService;)V

    invoke-virtual {v2, v3}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$2;->INSTANCE:Lcom/discord/widgets/guilds/invite/InviteSuggestionsService$observeInviteSuggestions$2;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026 inviteSuggestionList\n  }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
