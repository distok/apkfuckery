.class public final Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;
.super Landroidx/core/widget/NestedScrollView;
.source "ViewInviteSettingsSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final channelSpinner$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final channelsSpinnerAdapter:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

.field private final expiresAfterRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final generateLinkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final maxUsesRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onGenerateLinkListener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

.field private final temporaryMembershipSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private updateSettings:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelInvite$Settings;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public viewModel:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;

    const-string v3, "channelSpinner"

    const-string v4, "getChannelSpinner()Landroid/widget/Spinner;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;

    const-string v6, "expiresAfterRadioGroup"

    const-string v7, "getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;

    const-string v6, "maxUsesRadioGroup"

    const-string v7, "getMaxUsesRadioGroup()Landroid/widget/RadioGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;

    const-string/jumbo v6, "temporaryMembershipSwitch"

    const-string v7, "getTemporaryMembershipSwitch()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;

    const-string v6, "generateLinkButton"

    const-string v7, "getGenerateLinkButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;)V

    const p1, 0x7f0a04d8

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelSpinner$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04de

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->expiresAfterRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04e2

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->maxUsesRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04eb

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->temporaryMembershipSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04df

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->generateLinkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance p1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "context"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0d0139

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;-><init>(Landroid/content/Context;I[Lcom/discord/models/domain/ModelChannel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelsSpinnerAdapter:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    sget-object v0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$updateSettings$1;->INSTANCE:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$updateSettings$1;

    iput-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->updateSettings:Lkotlin/jvm/functions/Function1;

    sget-object v0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$onGenerateLinkListener$1;->INSTANCE:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$onGenerateLinkListener$1;

    iput-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->onGenerateLinkListener:Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0136

    invoke-static {v0, v1, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    const v0, 0x7f120e5b

    invoke-static {p0, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getChannelSpinner()Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;

    move-result-object p1

    sget-object v0, Lcom/discord/models/domain/ModelInvite$Settings;->EXPIRES_AFTER_ARRAY:[I

    const-string v1, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$1;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesRadioGroup()Landroid/widget/RadioGroup;

    move-result-object p1

    sget-object v0, Lcom/discord/models/domain/ModelInvite$Settings;->MAX_USES_ARRAY:[I

    const-string v1, "ModelInvite.Settings.MAX_USES_ARRAY"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$2;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->setOnItemSelected()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrSet"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p1, 0x7f0a04d8

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelSpinner$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04de

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->expiresAfterRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04e2

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->maxUsesRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04eb

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->temporaryMembershipSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04df

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->generateLinkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance p1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string p2, "context"

    invoke-static {v1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0d0139

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;-><init>(Landroid/content/Context;I[Lcom/discord/models/domain/ModelChannel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelsSpinnerAdapter:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    sget-object p2, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$updateSettings$1;->INSTANCE:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$updateSettings$1;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->updateSettings:Lkotlin/jvm/functions/Function1;

    sget-object p2, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$onGenerateLinkListener$1;->INSTANCE:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$onGenerateLinkListener$1;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->onGenerateLinkListener:Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const v0, 0x7f0d0136

    invoke-static {p2, v0, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    const p2, 0x7f120e5b

    invoke-static {p0, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getChannelSpinner()Landroid/widget/Spinner;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;

    move-result-object p1

    sget-object p2, Lcom/discord/models/domain/ModelInvite$Settings;->EXPIRES_AFTER_ARRAY:[I

    const-string v0, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$1;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesRadioGroup()Landroid/widget/RadioGroup;

    move-result-object p1

    sget-object p2, Lcom/discord/models/domain/ModelInvite$Settings;->MAX_USES_ARRAY:[I

    const-string v0, "ModelInvite.Settings.MAX_USES_ARRAY"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$2;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->setOnItemSelected()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrSet"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p1, 0x7f0a04d8

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelSpinner$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04de

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->expiresAfterRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04e2

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->maxUsesRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04eb

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->temporaryMembershipSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a04df

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->generateLinkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance p1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string p2, "context"

    invoke-static {v1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0d0139

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;-><init>(Landroid/content/Context;I[Lcom/discord/models/domain/ModelChannel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelsSpinnerAdapter:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    sget-object p2, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$updateSettings$1;->INSTANCE:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$updateSettings$1;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->updateSettings:Lkotlin/jvm/functions/Function1;

    sget-object p2, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$onGenerateLinkListener$1;->INSTANCE:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$onGenerateLinkListener$1;

    iput-object p2, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->onGenerateLinkListener:Lkotlin/jvm/functions/Function0;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f0d0136

    invoke-static {p2, p3, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    const p2, 0x7f120e5b

    invoke-static {p0, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getChannelSpinner()Landroid/widget/Spinner;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;

    move-result-object p1

    sget-object p2, Lcom/discord/models/domain/ModelInvite$Settings;->EXPIRES_AFTER_ARRAY:[I

    const-string p3, "ModelInvite.Settings.EXPIRES_AFTER_ARRAY"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$1;

    invoke-direct {p3, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$1;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesRadioGroup()Landroid/widget/RadioGroup;

    move-result-object p1

    sget-object p2, Lcom/discord/models/domain/ModelInvite$Settings;->MAX_USES_ARRAY:[I

    const-string p3, "ModelInvite.Settings.MAX_USES_ARRAY"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$2;

    invoke-direct {p3, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$2;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->setOnItemSelected()V

    return-void
.end method

.method public static final synthetic access$getChannelsSpinnerAdapter$p(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelsSpinnerAdapter:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    return-object p0
.end method

.method public static final synthetic access$getExpireAfterString(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpireAfterString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getMaxUsesString(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPendingInviteSettings$p(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)Lcom/discord/models/domain/ModelInvite$Settings;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

    return-object p0
.end method

.method public static final synthetic access$getTemporaryMembershipSwitch$p(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getTemporaryMembershipSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setPendingInviteSettings$p(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;Lcom/discord/models/domain/ModelInvite$Settings;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

    return-void
.end method

.method private final createHorizontalCheckableButtons(Landroid/widget/RadioGroup;[ILkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/RadioGroup;",
            "[I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    return-void

    :cond_0
    array-length v0, p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget v4, p2, v2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f0d014d

    invoke-virtual {v5, v6, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    const-string v6, "null cannot be cast to non-null type android.widget.RadioButton"

    invoke-static {v5, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v5, Landroid/widget/RadioButton;

    invoke-virtual {v5, v4}, Landroid/widget/RadioButton;->setId(I)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p3, v4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    if-nez v3, :cond_1

    invoke-virtual {v5}, Landroid/widget/RadioButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type android.widget.RadioGroup.LayoutParams"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v3, Landroid/widget/RadioGroup$LayoutParams;

    const/16 v4, 0x10

    invoke-static {v4}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RadioGroup$LayoutParams;->leftMargin:I

    invoke-virtual {v5, v3}, Landroid/widget/RadioButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {p1, v5}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private final getChannelSpinner()Landroid/widget/Spinner;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelSpinner$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    return-object v0
.end method

.method private final getExpireAfterString(I)Ljava/lang/String;
    .locals 6

    if-eqz p1, :cond_5

    const/16 v0, 0x708

    const/4 v1, 0x0

    const-string v2, "context"

    const-string v3, "resources"

    const/4 v4, 0x1

    if-eq p1, v0, :cond_4

    const/16 v0, 0xe10

    const v5, 0x7f100031

    if-eq p1, v0, :cond_3

    const/16 v0, 0x5460

    if-eq p1, v0, :cond_2

    const v0, 0xa8c0

    if-eq p1, v0, :cond_1

    const v0, 0x15180

    if-eq p1, v0, :cond_0

    const-string p1, ""

    goto/16 :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f100030

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-static {p1, v0, v2, v4, v3}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Ljava/lang/Object;

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {p1, v0, v5, v3, v2}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {p1, v0, v5, v3, v2}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {p1, v0, v5, v4, v2}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f100033

    new-array v3, v4, [Ljava/lang/Object;

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-static {p1, v0, v2, v4, v3}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_5
    const p1, 0x7f12111f

    invoke-static {p0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->expiresAfterRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    return-object v0
.end method

.method private final getGenerateLinkButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->generateLinkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getMaxUsesRadioGroup()Landroid/widget/RadioGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->maxUsesRadioGroup$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    return-object v0
.end method

.method private final getMaxUsesString(I)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string/jumbo p1, "\u221e"

    :goto_0
    return-object p1
.end method

.method private final getTemporaryMembershipSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->temporaryMembershipSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final setOnItemSelected()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getChannelSpinner()Landroid/widget/Spinner;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$setOnItemSelected$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$setOnItemSelected$1;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method


# virtual methods
.method public final configureUi(Lcom/discord/widgets/guilds/invite/WidgetInviteModel;)V
    .locals 9

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->channelsSpinnerAdapter:Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/invite/WidgetInviteModel;->getInvitableChannels()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v2, [Lcom/discord/models/domain/ModelChannel;

    invoke-interface {v1, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    const-string v3, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-static {v1, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, [Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$ChannelsSpinnerAdapter;->setData([Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/invite/WidgetInviteModel;->getInvitableChannels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/invite/WidgetInviteModel;->getTargetChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    cmp-long v3, v5, v7

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    :goto_2
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getChannelSpinner()Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/widget/Spinner;->setSelection(IZ)V

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/invite/WidgetInviteModel;->getSettings()Lcom/discord/models/domain/ModelInvite$Settings;

    move-result-object v0

    if-eqz v0, :cond_e

    iput-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    invoke-static {v2, v0}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const-string v6, "null cannot be cast to non-null type android.widget.RadioButton"

    if-eqz v5, :cond_3

    move-object v5, v0

    check-cast v5, Lx/h/o;

    invoke-virtual {v5}, Lx/h/o;->nextInt()I

    move-result v5

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v5, Landroid/widget/RadioButton;

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v5, 0x0

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/widget/RadioButton;

    invoke-virtual {v7}, Landroid/widget/RadioButton;->getId()I

    move-result v7

    iget-object v8, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

    if-eqz v8, :cond_5

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelInvite$Settings;->getMaxAge()I

    move-result v8

    if-ne v7, v8, :cond_5

    const/4 v7, 0x1

    goto :goto_4

    :cond_5
    const/4 v7, 0x0

    :goto_4
    if-eqz v7, :cond_4

    goto :goto_5

    :cond_6
    move-object v1, v5

    :goto_5
    check-cast v1, Landroid/widget/RadioButton;

    if-eqz v1, :cond_7

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_7
    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getExpiresAfterRadioGroup()Landroid/widget/RadioGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$4;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$4;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesRadioGroup()Landroid/widget/RadioGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    invoke-static {v2, v0}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object v3, v0

    check-cast v3, Lx/h/o;

    invoke-virtual {v3}, Lx/h/o;->nextInt()I

    move-result v3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesRadioGroup()Landroid/widget/RadioGroup;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v3, Landroid/widget/RadioButton;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_8
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/widget/RadioButton;

    invoke-virtual {v3}, Landroid/widget/RadioButton;->getId()I

    move-result v3

    iget-object v6, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

    if-eqz v6, :cond_a

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelInvite$Settings;->getMaxUses()I

    move-result v6

    if-ne v3, v6, :cond_a

    const/4 v3, 0x1

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    :goto_7
    if-eqz v3, :cond_9

    move-object v5, v1

    :cond_b
    check-cast v5, Landroid/widget/RadioButton;

    if-eqz v5, :cond_c

    invoke-virtual {v5, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_c
    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getMaxUsesRadioGroup()Landroid/widget/RadioGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$8;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$8;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getTemporaryMembershipSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->pendingInviteSettings:Lcom/discord/models/domain/ModelInvite$Settings;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelInvite$Settings;->isTemporary()Z

    move-result v2

    :cond_d
    invoke-virtual {v0, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getTemporaryMembershipSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$9;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$9;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->getGenerateLinkButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$10;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet$configureUi$10;-><init>(Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;Lcom/discord/widgets/guilds/invite/WidgetInviteModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_e
    return-void
.end method

.method public final getOnGenerateLinkListener()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->onGenerateLinkListener:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getUpdateSettings()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelInvite$Settings;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->updateSettings:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getViewModel()Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->viewModel:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final setOnGenerateLinkListener(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->onGenerateLinkListener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setUpdateSettings(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelInvite$Settings;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->updateSettings:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setViewModel(Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/invite/ViewInviteSettingsSheet;->viewModel:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShareViewModel;

    return-void
.end method
