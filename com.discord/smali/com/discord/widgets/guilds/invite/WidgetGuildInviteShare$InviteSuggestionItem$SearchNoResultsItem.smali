.class public final Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;
.super Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem;
.source "WidgetGuildInviteShare.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchNoResultsItem"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$InviteSuggestionItem$SearchNoResultsItem;-><init>()V

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    const-string v0, "SEARCH_NO_RESULTS"

    return-object v0
.end method

.method public hasSentInvite()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
