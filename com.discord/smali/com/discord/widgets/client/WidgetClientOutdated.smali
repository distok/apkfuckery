.class public Lcom/discord/widgets/client/WidgetClientOutdated;
.super Lcom/discord/app/AppFragment;
.source "WidgetClientOutdated.java"


# static fields
.field public static final synthetic d:I


# instance fields
.field private outdatedUpdate:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method

.method public static launch(Landroid/content/Context;)V
    .locals 3

    const-class v0, Lcom/discord/widgets/client/WidgetClientOutdated;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {p0, v0, v1, v2}, Lf/a/b/m;->e(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01db

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const p2, 0x7f0a02a8

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/client/WidgetClientOutdated;->outdatedUpdate:Landroid/view/View;

    sget-object p2, Lf/a/o/c/a;->d:Lf/a/o/c/a;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object p1, Lf/a/o/c/b;->d:Lf/a/o/c/b;

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setOnBackPressed(Lrx/functions/Func0;)V

    return-void
.end method
