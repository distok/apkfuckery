.class public final Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;
.super Ljava/lang/Object;
.source "WidgetChannelFollowSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->configureChannelSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $selectedGuild:Lcom/discord/models/domain/ModelGuild;

.field public final synthetic $viewState:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

.field public final synthetic this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/models/domain/ModelGuild;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    iput-object p2, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->$selectedGuild:Lcom/discord/models/domain/ModelGuild;

    iput-object p3, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->$viewState:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8

    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    iget-object v1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    iget-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->$selectedGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    new-instance v7, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$ChannelFollowChannelFilterFunction;

    iget-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->$selectedGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v4

    iget-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;->$viewState:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getAvailableChannels()Ljava/util/Set;

    move-result-object p1

    invoke-direct {v7, v4, v5, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$ChannelFollowChannelFilterFunction;-><init>(JLjava/util/Set;)V

    const v4, 0x186a1

    const/4 v5, 0x0

    const v6, 0x7f12112e

    invoke-virtual/range {v0 .. v7}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->launch(Landroidx/fragment/app/Fragment;JIZILcom/discord/widgets/channels/WidgetChannelSelector$FilterFunction;)V

    return-void
.end method
