.class public final Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;
.super Ljava/lang/Object;
.source "WidgetChannelFollowSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->configureGuildSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

.field public final synthetic this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    iput-object p2, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;->$viewState:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/guilds/WidgetGuildSelector;->Companion:Lcom/discord/widgets/guilds/WidgetGuildSelector$Companion;

    iget-object v1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    new-instance v5, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$ChannelFollowGuildFilterFunction;

    iget-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;->$viewState:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getAvailableGuilds()Ljava/util/Set;

    move-result-object p1

    invoke-direct {v5, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$ChannelFollowGuildFilterFunction;-><init>(Ljava/util/Set;)V

    const v2, 0x186a2

    const/4 v3, 0x0

    const v4, 0x7f12112e

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Companion;->launch(Landroidx/fragment/app/Fragment;IZILcom/discord/widgets/guilds/WidgetGuildSelector$FilterFunction;)V

    return-void
.end method
