.class public final Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;
.super Ljava/lang/Object;
.source "WidgetChannelFollowSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->configureChannelSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $selectedChannel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    iput-object p2, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;->$selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    invoke-static {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->access$getViewModel$p(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;->$selectedChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->followChannel(J)V

    return-void
.end method
