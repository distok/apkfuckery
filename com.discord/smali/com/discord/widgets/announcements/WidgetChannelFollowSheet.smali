.class public final Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetChannelFollowSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$ChannelFollowChannelFilterFunction;,
        Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$ChannelFollowGuildFilterFunction;,
        Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;

.field private static final REQUEST_CODE_CHANNEL_FOLLOW_CHANNEL:I = 0x186a1

.field private static final REQUEST_CODE_CHANNEL_FOLLOW_GUILD:I = 0x186a2

.field private static final VIEW_INDEX_FOLLOW:I = 0x0

.field private static final VIEW_INDEX_NO_AVAILABLE_GUILDS:I = 0x1


# instance fields
.field private final channelDropdown$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final createChannelFollowerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final errorText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildDropdown$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final selectedChannelName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final selectedGuildName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final sourceChannelName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final sourceGuildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v3, "createChannelFollowerButton"

    const-string v4, "getCreateChannelFollowerButton()Landroid/widget/Button;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "sourceGuildIcon"

    const-string v7, "getSourceGuildIcon()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "sourceChannelName"

    const-string v7, "getSourceChannelName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "channelDropdown"

    const-string v7, "getChannelDropdown()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "selectedChannelName"

    const-string v7, "getSelectedChannelName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "guildDropdown"

    const-string v7, "getGuildDropdown()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "selectedGuildName"

    const-string v7, "getSelectedGuildName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "errorText"

    const-string v7, "getErrorText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const-string v6, "flipper"

    const-string v7, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->Companion:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0309

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->createChannelFollowerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0171

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->sourceGuildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a016e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->sourceChannelName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0172

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->channelDropdown$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0174

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->selectedChannelName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0173

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->guildDropdown$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0175

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->selectedGuildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a016f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->errorText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0465

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->configureUI(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->viewModel:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->viewModel:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    return-void
.end method

.method private final configureChannelSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
    .locals 10

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getSelectedGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getSelectedChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getChannelDropdown()Landroidx/cardview/widget/CardView;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;

    invoke-direct {v3, p0, v0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/models/domain/ModelGuild;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    if-eqz v1, :cond_1

    const p1, 0x7f040155

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedChannelName()Landroid/widget/TextView;

    move-result-object v0

    sget-object v2, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v2, "requireContext()"

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v6

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils;Landroid/content/Context;Ljava/lang/String;IZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getCreateChannelFollowerButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getCreateChannelFollowerButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;

    invoke-direct {v2, p0, v1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureChannelSelector$2;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    const p1, 0x7f040158

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedChannelName()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f12163b

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getCreateChannelFollowerButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedChannelName()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedGuildName()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private final configureGuildSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getGuildDropdown()Landroidx/cardview/widget/CardView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$configureGuildSelector$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getSelectedGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedGuildName()Landroid/widget/TextView;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const v1, 0x7f12163b

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    const p1, 0x7f040155

    goto :goto_1

    :cond_1
    const p1, 0x7f040158

    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedGuildName()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSelectedGuildName()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V
    .locals 9

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSourceGuildIcon()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getSourceGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Lcom/discord/models/domain/ModelGuild;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x7c

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getSourceChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getSourceChannelName()Landroid/widget/TextView;

    move-result-object v2

    sget-object v3, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "requireContext()"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    invoke-virtual {v3, v4, v5, v0, v1}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName(Landroid/content/Context;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getAvailableGuilds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->configureGuildSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->configureChannelSelector(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getErrorTextRes()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getErrorText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->getErrorTextRes()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getErrorText()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->getErrorText()Landroid/widget/TextView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private final getChannelDropdown()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->channelDropdown$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getCreateChannelFollowerButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->createChannelFollowerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getErrorText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->errorText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getGuildDropdown()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->guildDropdown$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getSelectedChannelName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->selectedChannelName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSelectedGuildName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->selectedGuildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSourceChannelName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->sourceChannelName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSourceGuildIcon()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->sourceGuildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method public static final show(Landroidx/fragment/app/FragmentManager;JJ)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->Companion:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;JJ)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0187

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p2, 0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/discord/widgets/guilds/WidgetGuildSelector;->Companion:Lcom/discord/widgets/guilds/WidgetGuildSelector$Companion;

    new-instance v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onActivityResult$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onActivityResult$2;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)V

    invoke-virtual {v0, p1, p3, v1, p2}, Lcom/discord/widgets/guilds/WidgetGuildSelector$Companion;->handleResult(ILandroid/content/Intent;Lkotlin/jvm/functions/Function3;Z)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    new-instance v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onActivityResult$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onActivityResult$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)V

    invoke-virtual {v0, p1, p3, v1, p2}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->handleResult(ILandroid/content/Intent;Lkotlin/jvm/functions/Function3;Z)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x186a1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const-string v0, "inflater"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "com.discord.intent.extra.EXTRA_GUILD_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    new-instance v4, Landroidx/lifecycle/ViewModelProvider;

    new-instance v5, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Factory;

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Factory;-><init>(JJ)V

    invoke-direct {v4, p0, v5}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    invoke-virtual {v4, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    iput-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->viewModel:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppBottomSheet;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->viewModel:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    const/4 v1, 0x0

    const-string/jumbo v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string/jumbo v3, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$$inlined$filterIs$1;->INSTANCE:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$$inlined$filterIs$1;

    invoke-virtual {v0, v3}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$$inlined$filterIs$2;->INSTANCE:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$$inlined$filterIs$2;

    invoke-virtual {v0, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v3, "filter { it is T }.map { it as T }"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->viewModel:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
