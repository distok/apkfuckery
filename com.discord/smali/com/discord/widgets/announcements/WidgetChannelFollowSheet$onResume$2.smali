.class public final Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;
.super Lx/m/c/k;
.source "WidgetChannelFollowSheet.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;->invoke(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    invoke-virtual {p1}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    sget-object p1, Lcom/discord/widgets/announcements/WidgetChannelFollowSuccessDialog;->Companion:Lcom/discord/widgets/announcements/WidgetChannelFollowSuccessDialog$Companion;

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheet$onResume$2;->this$0:Lcom/discord/widgets/announcements/WidgetChannelFollowSheet;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSuccessDialog$Companion;->show(Landroidx/fragment/app/FragmentManager;)V

    return-void
.end method
