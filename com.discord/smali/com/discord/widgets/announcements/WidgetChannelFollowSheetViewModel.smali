.class public final Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;
.super Lf/a/b/l0;
.source "WidgetChannelFollowSheetViewModel.kt"

# interfaces
.implements Lcom/discord/app/AppComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;,
        Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;,
        Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;",
        ">;",
        "Lcom/discord/app/AppComponent;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedChannelSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedGuildSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final sourceChannelId:J

.field private final sourceGuildId:J

.field private final storeChannels:Lcom/discord/stores/StoreChannels;

.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storePermissions:Lcom/discord/stores/StorePermissions;


# direct methods
.method public constructor <init>(JJLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)V
    .locals 13

    move-object v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    const-string/jumbo v4, "storeGuilds"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "storeChannels"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "storePermissions"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v4}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    move-wide v4, p1

    iput-wide v4, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->sourceGuildId:J

    move-wide/from16 v4, p3

    iput-wide v4, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->sourceChannelId:J

    iput-object v1, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object v2, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object v3, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storePermissions:Lcom/discord/stores/StorePermissions;

    const/4 v1, 0x0

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->selectedGuildSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->selectedChannelSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->observeViewStateFromStores()Lrx/Observable;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, p0, v1, v3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    new-instance v10, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getEventSubject$p(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$handleChannelFollowError(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;Lcom/discord/utilities/error/Error;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->handleChannelFollowError(Lcom/discord/utilities/error/Error;)V

    return-void
.end method

.method public static final synthetic access$updateViewState(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;)V
    .locals 0

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final calculateChannelsWithPermissions()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storePermissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0}, Lcom/discord/stores/StorePermissions;->observePermissionsForAllChannels()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$calculateChannelsWithPermissions$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$calculateChannelsWithPermissions$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final handleChannelFollowError(Lcom/discord/utilities/error/Error;)V
    .locals 12

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p1

    const-string v0, "error.response"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error$Response;->getCode()I

    move-result p1

    const/16 v0, 0x7537

    if-eq p1, v0, :cond_0

    const p1, 0x7f12075b

    goto :goto_0

    :cond_0
    const p1, 0x7f12075f

    :goto_0
    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel.ViewState.Loaded"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/16 v10, 0x3f

    const/4 v11, 0x0

    invoke-static/range {v2 .. v11}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Set;Ljava/util/Set;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final observeViewStateFromStores()Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iget-wide v1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->sourceGuildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    iget-wide v1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->sourceChannelId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v4

    invoke-direct {p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->calculateChannelsWithPermissions()Lrx/Observable;

    move-result-object v5

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->selectedGuildSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$observeViewStateFromStores$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$observeViewStateFromStores$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v6

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->selectedChannelSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$observeViewStateFromStores$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$observeViewStateFromStores$2;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v7

    sget-object v8, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$observeViewStateFromStores$3;->INSTANCE:Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$observeViewStateFromStores$3;

    invoke-static/range {v3 .. v8}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026  )\n\n      expected\n    }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final followChannel(J)V
    .locals 11

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->sourceChannelId:J

    new-instance v3, Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;

    invoke-direct {v3, p1, p2}, Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;-><init>(J)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/utilities/rest/RestAPI;->createChannelFollower(JLcom/discord/restapi/RestAPIParams$ChannelFollowerPost;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;

    new-instance v6, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$followChannel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$followChannel$1;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V

    new-instance v8, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$followChannel$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$followChannel$2;-><init>(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getStoreChannels()Lcom/discord/stores/StoreChannels;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-object v0
.end method

.method public final getStoreGuilds()Lcom/discord/stores/StoreGuilds;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    return-object v0
.end method

.method public final getStorePermissions()Lcom/discord/stores/StorePermissions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->storePermissions:Lcom/discord/stores/StorePermissions;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final selectChannel(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->selectedChannelSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final selectGuild(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;->selectedGuildSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
