.class public final synthetic Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator$enqueueNoticeWhenEnabled$1;
.super Lx/m/c/i;
.source "AccessibilityDetectionNavigator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;->enqueueNoticeWhenEnabled(Lcom/discord/app/AppComponent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/accessibility/AccessibilityState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;)V
    .locals 7

    const-class v3, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;

    const/4 v1, 0x1

    const-string v4, "showAccessibilityDetectionDialog"

    const-string v5, "showAccessibilityDetectionDialog(Lcom/discord/utilities/accessibility/AccessibilityState;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/accessibility/AccessibilityState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator$enqueueNoticeWhenEnabled$1;->invoke(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;

    invoke-static {v0, p1}, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;->access$showAccessibilityDetectionDialog(Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;Lcom/discord/utilities/accessibility/AccessibilityState;)V

    return-void
.end method
