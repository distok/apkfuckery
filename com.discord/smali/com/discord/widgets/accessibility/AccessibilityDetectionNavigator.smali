.class public final Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;
.super Ljava/lang/Object;
.source "AccessibilityDetectionNavigator.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;

    invoke-direct {v0}, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;-><init>()V

    sput-object v0, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;->INSTANCE:Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$showAccessibilityDetectionDialog(Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;->showAccessibilityDetectionDialog(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    return-void
.end method

.method private final showAccessibilityDetectionDialog(Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 17

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/accessibility/AccessibilityState;->getFeatures()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->SCREENREADER:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    sget-object v13, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator$showAccessibilityDetectionDialog$1;->INSTANCE:Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator$showAccessibilityDetectionDialog$1;

    const/16 v14, 0xe6

    const/16 v16, 0x0

    const-string v2, "ALLOW_ACCESSIBILITY_DETECTION_DIALOG"

    move-object v1, v15

    move-object/from16 p1, v0

    move-object v0, v15

    move-object/from16 v15, v16

    invoke-direct/range {v1 .. v15}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v1, p1

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final enqueueNoticeWhenEnabled(Lcom/discord/app/AppComponent;)V
    .locals 12

    const-string v0, "appComponent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->Companion:Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;->getINSTANCE()Lcom/discord/utilities/accessibility/AccessibilityMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->observeAccessibilityState()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;

    new-instance v9, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator$enqueueNoticeWhenEnabled$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator$enqueueNoticeWhenEnabled$1;-><init>(Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
