.class public final Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;
.super Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;
.source "WidgetFriendsListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/WidgetFriendsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemPendingUser"
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final itemAcceptButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final itemActivity$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final itemAvatar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final itemDeclineButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final itemName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final itemStatus$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;

    const/4 v1, 0x6

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string v3, "itemAvatar"

    const-string v4, "getItemAvatar()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/s;

    const-string v6, "itemName"

    const-string v7, "getItemName()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Lx/m/c/s;

    const-string v6, "itemActivity"

    const-string v7, "getItemActivity()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x3

    new-instance v4, Lx/m/c/s;

    const-string v6, "itemStatus"

    const-string v7, "getItemStatus()Lcom/discord/views/StatusView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x4

    new-instance v4, Lx/m/c/s;

    const-string v6, "itemAcceptButton"

    const-string v7, "getItemAcceptButton()Landroidx/appcompat/widget/AppCompatImageView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x5

    new-instance v4, Lx/m/c/s;

    const-string v6, "itemDeclineButton"

    const-string v7, "getItemDeclineButton()Landroidx/appcompat/widget/AppCompatImageView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d01f6

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;-><init>(ILcom/discord/widgets/friends/WidgetFriendsListAdapter;)V

    const v0, 0x7f0a0471

    invoke-static {p0, v0}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemAvatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a047a

    invoke-static {p0, v0}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0470

    invoke-static {p0, v0}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemActivity$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a047b

    invoke-static {p0, v0}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemStatus$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a046f

    invoke-static {p0, v0}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemAcceptButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0476

    invoke-static {p0, v0}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemDeclineButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V

    const/4 p1, 0x0

    new-array p1, p1, [Landroid/view/View;

    invoke-virtual {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;)Lcom/discord/widgets/friends/WidgetFriendsListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    return-object p0
.end method

.method private final getItemAcceptButton()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemAcceptButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    return-object v0
.end method

.method private final getItemActivity()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemActivity$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    return-object v0
.end method

.method private final getItemAvatar()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemAvatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getItemDeclineButton()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemDeclineButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    return-object v0
.end method

.method private final getItemName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getItemStatus()Lcom/discord/views/StatusView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->itemStatus$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/StatusView;

    return-object v0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V
    .locals 8

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    check-cast p2, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemName()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemStatus()Lcom/discord/views/StatusView;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/views/StatusView;->setPresence(Lcom/discord/models/domain/ModelPresence;)V

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getRelationshipType()I

    move-result p1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemActivity()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemActivity()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12120d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemAcceptButton()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemActivity()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemActivity()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120d79

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemAcceptButton()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemAvatar()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    const v3, 0x7f07006b

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelUser;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemAcceptButton()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$onConfigure$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->getItemDeclineButton()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$onConfigure$2;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$onConfigure$2;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/friends/FriendsListViewModel$Item;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;->onConfigure(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V

    return-void
.end method
