.class public final Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;
.super Lcom/discord/widgets/friends/FriendsListViewModel$Item;
.source "FriendsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/FriendsListViewModel$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Header"
.end annotation


# instance fields
.field private final count:I

.field private final titleStringResId:I


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    iput p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;IIILjava/lang/Object;)Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->copy(II)Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    return v0
.end method

.method public final copy(II)Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    iget v1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    iget p1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/discord/widgets/friends/FriendsListViewModel$Item;->get_type()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTitleStringResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Header(titleStringResId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->titleStringResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;->count:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
