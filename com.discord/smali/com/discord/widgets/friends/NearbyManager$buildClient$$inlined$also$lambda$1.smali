.class public final Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;
.super Lf/h/a/f/k/b/c;
.source "NearbyManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/NearbyManager;->buildClient(Landroidx/fragment/app/FragmentActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/friends/NearbyManager;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/NearbyManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;->this$0:Lcom/discord/widgets/friends/NearbyManager;

    invoke-direct {p0}, Lf/h/a/f/k/b/c;-><init>()V

    return-void
.end method


# virtual methods
.method public onPermissionChanged(Z)V
    .locals 3

    invoke-super {p0, p1}, Lf/h/a/f/k/b/c;->onPermissionChanged(Z)V

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;->this$0:Lcom/discord/widgets/friends/NearbyManager;

    invoke-static {p1}, Lcom/discord/widgets/friends/NearbyManager;->access$getNearbyStateSubject$p(Lcom/discord/widgets/friends/NearbyManager;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;

    const/16 v1, 0x62

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;-><init>(I)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/discord/utilities/analytics/AnalyticsTracker;->nearbyConnected()V

    iget-object p1, p0, Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;->this$0:Lcom/discord/widgets/friends/NearbyManager;

    invoke-static {p1}, Lcom/discord/widgets/friends/NearbyManager;->access$getNearbyStateSubject$p(Lcom/discord/widgets/friends/NearbyManager;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;

    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;->this$0:Lcom/discord/widgets/friends/NearbyManager;

    invoke-static {v2}, Lcom/discord/widgets/friends/NearbyManager;->access$getNearbyUserIds$p(Lcom/discord/widgets/friends/NearbyManager;)Ljava/util/HashSet;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;-><init>(Ljava/util/Set;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
