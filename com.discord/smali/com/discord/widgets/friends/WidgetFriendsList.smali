.class public final Lcom/discord/widgets/friends/WidgetFriendsList;
.super Lcom/discord/app/AppFragment;
.source "WidgetFriendsList.kt"

# interfaces
.implements Lcom/discord/widgets/tabs/OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/WidgetFriendsList$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ANALYTICS_SOURCE:Ljava/lang/String; = "Friends"

.field public static final Companion:Lcom/discord/widgets/friends/WidgetFriendsList$Companion;

.field private static final VIEW_INDEX_EMPTY:I = 0x1

.field private static final VIEW_INDEX_LOADING:I = 0x2

.field private static final VIEW_INDEX_RECYCLER:I


# instance fields
.field private adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

.field private final emptyFriendsStateView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private loadingAdapter:Lcom/discord/widgets/friends/WidgetFriendsListLoadingAdapter;

.field private final loadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

.field private final recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final toolbarTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/friends/FriendsListViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/friends/WidgetFriendsList;

    const-string v3, "flipper"

    const-string v4, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsList;

    const-string v6, "recyclerView"

    const-string v7, "getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsList;

    const-string v6, "emptyFriendsStateView"

    const-string v7, "getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsList;

    const-string v6, "loadingView"

    const-string v7, "getLoadingView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsList;

    const-string/jumbo v6, "toolbarTitle"

    const-string v7, "getToolbarTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/friends/WidgetFriendsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/WidgetFriendsList$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/friends/WidgetFriendsList;->Companion:Lcom/discord/widgets/friends/WidgetFriendsList$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a046e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0480

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a046d

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->emptyFriendsStateView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a047d

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->loadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0abd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->toolbarTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/friends/WidgetFriendsList;Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsList;->configureUI(Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/friends/WidgetFriendsList;)Lcom/discord/widgets/friends/FriendsListViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->viewModel:Lcom/discord/widgets/friends/FriendsListViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/friends/WidgetFriendsList;Lcom/discord/widgets/friends/FriendsListViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsList;->handleEvent(Lcom/discord/widgets/friends/FriendsListViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/friends/WidgetFriendsList;Lcom/discord/widgets/friends/FriendsListViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->viewModel:Lcom/discord/widgets/friends/FriendsListViewModel;

    return-void
.end method

.method private final configureToolbar()V
    .locals 7

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->bindToolbar()Lkotlin/Unit;

    const v0, 0x7f12085a

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarTitleLayoutMinimumTappableArea()Lkotlin/Unit;

    new-instance v3, Lcom/discord/widgets/friends/WidgetFriendsList$configureToolbar$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$configureToolbar$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    const v2, 0x7f0e000f

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu$default(Lcom/discord/app/AppFragment;ILrx/functions/Action2;Lrx/functions/Action1;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->showLoadingView()V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->showEmptyView()V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsList;->updateView(Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;)V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final displayFlipperChild(I)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method

.method private final getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->emptyFriendsStateView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/friends/EmptyFriendsStateView;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getLoadingView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->loadingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getToolbarTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->toolbarTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsList;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/friends/FriendsListViewModel$Event;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsList;->handleShowToast(Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsList;->handleShowFriendRequestErrorToast(Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;)V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsList;->handleLaunchVoiceCall(Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;)V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleLaunchVoiceCall(Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVoiceCall(J)V

    return-void

    :cond_0
    const-string p1, "privateCallLauncher"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final handleShowFriendRequestErrorToast(Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;)V
    .locals 3

    sget-object v0, Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;->INSTANCE:Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;->getAbortCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;->getUsername()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;->getRelationshipResponse(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->l(Landroidx/fragment/app/Fragment;Ljava/lang/CharSequence;II)V

    return-void
.end method

.method private final handleShowToast(Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;)V
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;->getStringRes()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    return-void
.end method

.method private final showEmptyView()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/widgets/friends/WidgetFriendsList;->displayFlipperChild(I)V

    return-void
.end method

.method private final showLoadingView()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/discord/widgets/friends/WidgetFriendsList;->displayFlipperChild(I)V

    return-void
.end method

.method private final updateView(Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/friends/WidgetFriendsList;->displayFlipperChild(I)V

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01f2

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$Factory;

    invoke-direct {v0}, Lcom/discord/widgets/friends/FriendsListViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/friends/FriendsListViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026istViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->viewModel:Lcom/discord/widgets/friends/FriendsListViewModel;

    new-instance p1, Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p0, p0, v0, v1}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;-><init>(Lcom/discord/app/AppPermissions$Requests;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    return-void
.end method

.method public onTabSelected()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->configureToolbar()V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->friendsListViewed()V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->getToolbarTitle()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/discord/widgets/tabs/WidgetTabsHost;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/discord/widgets/tabs/NavigationTab;->FRIENDS:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-virtual {p1, v0, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->registerTabSelectionListener(Lcom/discord/widgets/tabs/NavigationTab;Lcom/discord/widgets/tabs/OnTabSelectedListener;)V

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->getEmptyFriendsStateView()Lcom/discord/widgets/friends/EmptyFriendsStateView;

    move-result-object p1

    const-string v0, "Friends"

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/EmptyFriendsStateView;->updateView(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v2, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    invoke-direct {v2, p1}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz p1, :cond_2

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$2;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->setOnClickUserProfile(Lkotlin/jvm/functions/Function2;)V

    :cond_2
    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz p1, :cond_3

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$3;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->setOnClickPendingHeaderExpand(Lkotlin/jvm/functions/Function0;)V

    :cond_3
    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz p1, :cond_4

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$4;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->setOnClickCall(Lkotlin/jvm/functions/Function1;)V

    :cond_4
    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz p1, :cond_5

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$5;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$5;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->setOnClickChat(Lkotlin/jvm/functions/Function1;)V

    :cond_5
    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz p1, :cond_6

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$6;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$6;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->setOnClickAcceptFriend(Lkotlin/jvm/functions/Function1;)V

    :cond_6
    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    if-eqz p1, :cond_7

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$7;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBound$7;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->setOnClickDeclineFriend(Lkotlin/jvm/functions/Function2;)V

    :cond_7
    new-instance p1, Lcom/discord/widgets/friends/WidgetFriendsListLoadingAdapter;

    invoke-direct {p1}, Lcom/discord/widgets/friends/WidgetFriendsListLoadingAdapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->loadingAdapter:Lcom/discord/widgets/friends/WidgetFriendsListLoadingAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsList;->getLoadingView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->loadingAdapter:Lcom/discord/widgets/friends/WidgetFriendsListLoadingAdapter;

    if-eqz v0, :cond_8

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_8
    const-string p1, "loadingAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->viewModel:Lcom/discord/widgets/friends/FriendsListViewModel;

    const-string/jumbo v1, "viewModel"

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/friends/WidgetFriendsList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBoundOrOnResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsList;->viewModel:Lcom/discord/widgets/friends/FriendsListViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/friends/FriendsListViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/friends/WidgetFriendsList;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBoundOrOnResume$2;

    invoke-direct {v10, p0}, Lcom/discord/widgets/friends/WidgetFriendsList$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/friends/WidgetFriendsList;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
