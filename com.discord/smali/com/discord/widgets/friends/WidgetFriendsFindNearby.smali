.class public final Lcom/discord/widgets/friends/WidgetFriendsFindNearby;
.super Lcom/discord/app/AppFragment;
.source "WidgetFriendsFindNearby.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;,
        Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final enableButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final learnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private meUserId:Ljava/lang/Long;

.field private final nearbyManager:Lcom/discord/widgets/friends/NearbyManager;

.field private final recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

.field private final searchingBody$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchingLottie$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchingTitle$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const-string v3, "recycler"

    const-string v4, "getRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const-string v6, "enableButton"

    const-string v7, "getEnableButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const-string v6, "searchingTitle"

    const-string v7, "getSearchingTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const-string v6, "searchingBody"

    const-string v7, "getSearchingBody()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const-string v6, "searchingLottie"

    const-string v7, "getSearchingLottie()Lcom/discord/rlottie/RLottieImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const-string v6, "learnMore"

    const-string v7, "getLearnMore()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a06e5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06e3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->enableButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06e8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->searchingTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06e6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->searchingBody$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06e7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->searchingLottie$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06e4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->learnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/friends/NearbyManager;

    invoke-direct {v0}, Lcom/discord/widgets/friends/NearbyManager;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->nearbyManager:Lcom/discord/widgets/friends/NearbyManager;

    return-void
.end method

.method private final acceptFriendRequest(J)V
    .locals 11

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    const-string v2, "Nearby - Accept Friend Request"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    move-wide v3, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/rest/RestAPI;->addRelationship$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    new-instance v8, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$acceptFriendRequest$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$acceptFriendRequest$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$acceptFriendRequest(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->acceptFriendRequest(J)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->configureUI(Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;)V

    return-void
.end method

.method public static final synthetic access$declineFriendRequest(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->declineFriendRequest(JZ)V

    return-void
.end method

.method public static final synthetic access$enableScanning(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->enableScanning()V

    return-void
.end method

.method public static final synthetic access$getResultsAdapter$p(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "resultsAdapter"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$sendFriendRequest(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->sendFriendRequest(Ljava/lang/String;I)V

    return-void
.end method

.method public static final synthetic access$setResultsAdapter$p(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;)V
    .locals 6

    instance-of v0, p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Uninitialized;

    const v1, 0x7f1200a9

    const v2, 0x7f040154

    const v3, 0x7f1200b6

    const/16 v4, 0x8

    const/4 v5, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingTitle()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getEnableButton()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingLottie()Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    iget-object v0, p1, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    iput-boolean v5, p1, Lcom/discord/rlottie/RLottieImageView;->f:Z

    iget-boolean p1, p1, Lcom/discord/rlottie/RLottieImageView;->e:Z

    if-eqz p1, :cond_6

    iput-boolean v5, v0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    goto/16 :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v0

    check-cast p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;

    invoke-virtual {p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;->getErrorCode()Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getErrorMessage(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f060238

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getEnableButton()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingLottie()Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    iget-object v0, p1, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-nez v0, :cond_2

    goto/16 :goto_0

    :cond_2
    iput-boolean v5, p1, Lcom/discord/rlottie/RLottieImageView;->f:Z

    iget-boolean p1, p1, Lcom/discord/rlottie/RLottieImageView;->e:Z

    if-eqz p1, :cond_6

    iput-boolean v5, v0, Lcom/discord/rlottie/RLottieDrawable;->D:Z

    goto/16 :goto_0

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Empty;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingTitle()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getEnableButton()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingLottie()Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/rlottie/RLottieImageView;->a()V

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingTitle()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1200b0

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f1200af

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingBody()Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getEnableButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;

    invoke-virtual {p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;->getItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    const-string p1, "resultsAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_6
    :goto_0
    return-void
.end method

.method private final declineFriendRequest(JZ)V
    .locals 11

    if-eqz p3, :cond_0

    const p3, 0x7f120852

    goto :goto_0

    :cond_0
    const p3, 0x7f12084f

    :goto_0
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    const-string v1, "Nearby - Remove Friend Request"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->removeRelationship(Ljava/lang/String;J)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$declineFriendRequest$1;

    invoke-direct {v8, p0, p3}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$declineFriendRequest$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;I)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final enableScanning()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->meUserId:Ljava/lang/Long;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->updateMeUserIdAndInitNearbyManager()V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->meUserId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->nearbyManager:Lcom/discord/widgets/friends/NearbyManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type androidx.fragment.app.FragmentActivity"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/friends/NearbyManager;->buildClientAndPublish(Landroidx/fragment/app/FragmentActivity;)V

    :cond_1
    return-void
.end method

.method private final getEnableButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->enableButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getErrorMessage(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x63

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_4

    const v0, 0x7f1200aa

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_1
    :goto_0
    const/16 v1, 0x62

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_4

    const v0, 0x7f1200b5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_4

    const v0, 0x7f1200b1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    :goto_2
    return-object v0
.end method

.method private final getLearnMore()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->learnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->recycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getSearchingBody()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->searchingBody$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSearchingLottie()Lcom/discord/rlottie/RLottieImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->searchingLottie$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/rlottie/RLottieImageView;

    return-object v0
.end method

.method private final getSearchingTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->searchingTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final sendFriendRequest(Ljava/lang/String;I)V
    .locals 5

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    const-string v1, "Nearby - Add Friend Suggestion"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->sendRelationshipRequest(Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/b/r;->a:Lf/a/b/r;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$sendFriendRequest$1;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$sendFriendRequest$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;Ljava/lang/String;)V

    new-instance v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$sendFriendRequest$2;

    invoke-direct {v4, p0, p1, p2}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$sendFriendRequest$2;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;Ljava/lang/String;I)V

    invoke-virtual {v1, v2, v3, v4}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method private final updateMeUserIdAndInitNearbyManager()V
    .locals 4

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->meUserId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->nearbyManager:Lcom/discord/widgets/friends/NearbyManager;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/widgets/friends/NearbyManager;->initialize(J)V

    :cond_1
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01f8

    return v0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->nearbyManager:Lcom/discord/widgets/friends/NearbyManager;

    invoke-virtual {v0}, Lcom/discord/widgets/friends/NearbyManager;->disableNearby()V

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->enableScanning()V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v1, "Nearby"

    invoke-virtual {v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->friendAddViewed(Ljava/lang/String;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 12

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->updateMeUserIdAndInitNearbyManager()V

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    const/4 v0, 0x0

    const-string v1, "resultsAdapter"

    if-eqz p1, :cond_4

    new-instance v2, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    invoke-virtual {p1, v2}, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;->setSendHandler(Lkotlin/jvm/functions/Function2;)V

    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    if-eqz p1, :cond_3

    new-instance v2, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$2;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    invoke-virtual {p1, v2}, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;->setAcceptHandler(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    if-eqz p1, :cond_2

    new-instance v2, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$3;

    invoke-direct {v2, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$3;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    invoke-virtual {p1, v2}, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;->setDeclineHandler(Lkotlin/jvm/functions/Function2;)V

    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$4;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;->setOnItemClick(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getEnableButton()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$5;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$5;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getLearnMore()Landroid/widget/TextView;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$6;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBound$6;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->getSearchingLottie()Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    const/high16 v0, 0x7f110000

    const/16 v1, 0xc8

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v6

    invoke-static {v1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v7

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v10, 0x0

    sget-object v1, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->LOOP:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    const-string v2, "playbackMode"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v11, Lcom/discord/rlottie/RLottieDrawable;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v2, "context"

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/display/DisplayUtils;->getScreenRefreshRate(Landroid/content/Context;)F

    move-result v8

    const/4 v9, 0x0

    const/high16 v4, 0x7f110000

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/discord/rlottie/RLottieDrawable;-><init>(Landroid/content/Context;ILjava/lang/String;IIFZ[I)V

    iput-object v11, p1, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    invoke-virtual {v11, v1}, Lcom/discord/rlottie/RLottieDrawable;->f(Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;)V

    iget-object v0, p1, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/rlottie/RLottieDrawable;->e(Z)V

    :cond_0
    iget-object v0, p1, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUserRelationships;->observeForType(I)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lk;->e:Lk;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreUserRelationships;->observeForType(I)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lk;->f:Lk;

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lm;->d:Lm;

    if-eqz v2, :cond_0

    new-instance v3, Ln;

    invoke-direct {v3, v2}, Ln;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lrx/functions/Func2;

    invoke-static {v1, v0, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .comb\u2026erRequestsModel\n        )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .comb\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/b/r;->a:Lf/a/b/r;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBoundOrOnResume$1;

    invoke-direct {v4, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    invoke-virtual {v1, v3, v4, v2}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    sget-object v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;

    iget-object v1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->nearbyManager:Lcom/discord/widgets/friends/NearbyManager;

    invoke-virtual {v1}, Lcom/discord/widgets/friends/NearbyManager;->getState()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;->get(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;->resultsAdapter:Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;

    if-eqz v1, :cond_1

    invoke-static {v0, p0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/friends/WidgetFriendsFindNearby;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBoundOrOnResume$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/friends/WidgetFriendsFindNearby;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_1
    const-string v0, "resultsAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
