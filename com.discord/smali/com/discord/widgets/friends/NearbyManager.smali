.class public final Lcom/discord/widgets/friends/NearbyManager;
.super Ljava/lang/Object;
.source "NearbyManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/NearbyManager$NearbyState;,
        Lcom/discord/widgets/friends/NearbyManager$Companion;
    }
.end annotation


# static fields
.field public static final CONNECTION_ERROR:I = 0x63

.field public static final Companion:Lcom/discord/widgets/friends/NearbyManager$Companion;

.field public static final PERMISSION_DENIED:I = 0x62


# instance fields
.field private meUserId:Ljava/lang/Long;

.field private messageListener:Lcom/google/android/gms/nearby/messages/MessageListener;

.field private messagePublishOptions:Lcom/google/android/gms/nearby/messages/PublishOptions;

.field private messagesClient:Lcom/google/android/gms/nearby/messages/MessagesClient;

.field private final nearbyStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/friends/NearbyManager$NearbyState;",
            ">;"
        }
    .end annotation
.end field

.field private final nearbyUserIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private outboundMessage:Lcom/google/android/gms/nearby/messages/Message;

.field private subscribeOptions:Lcom/google/android/gms/nearby/messages/SubscribeOptions;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/friends/NearbyManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/NearbyManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/friends/NearbyManager;->Companion:Lcom/discord/widgets/friends/NearbyManager$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    sget-object v0, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;->INSTANCE:Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$foundUserId(Lcom/discord/widgets/friends/NearbyManager;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/NearbyManager;->foundUserId(J)V

    return-void
.end method

.method public static final synthetic access$getNearbyStateSubject$p(Lcom/discord/widgets/friends/NearbyManager;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getNearbyUserIds$p(Lcom/discord/widgets/friends/NearbyManager;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    return-object p0
.end method

.method public static final synthetic access$lostUserId(Lcom/discord/widgets/friends/NearbyManager;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/NearbyManager;->lostUserId(J)V

    return-void
.end method

.method public static final synthetic access$parseUserId(Lcom/discord/widgets/friends/NearbyManager;Lcom/google/android/gms/nearby/messages/Message;)Ljava/lang/Long;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/NearbyManager;->parseUserId(Lcom/google/android/gms/nearby/messages/Message;)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method private final buildClient(Landroidx/fragment/app/FragmentActivity;)V
    .locals 3

    new-instance v0, Lf/h/a/f/k/b/a$a;

    invoke-direct {v0}, Lf/h/a/f/k/b/a$a;-><init>()V

    new-instance v1, Lf/h/a/f/k/b/a;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lf/h/a/f/k/b/a;-><init>(Lf/h/a/f/k/b/a$a;Lf/h/a/f/k/b/h;)V

    const-string v0, "Activity must not be null"

    invoke-static {p1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Options must not be null"

    invoke-static {v1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/k/b/e/i;

    invoke-direct {v0, p1, v1}, Lf/h/a/f/k/b/e/i;-><init>(Landroid/app/Activity;Lf/h/a/f/k/b/a;)V

    new-instance p1, Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/friends/NearbyManager$buildClient$$inlined$also$lambda$1;-><init>(Lcom/discord/widgets/friends/NearbyManager;)V

    invoke-virtual {v0, p1}, Lf/h/a/f/k/b/e/i;->m(Ljava/lang/Object;)Lf/h/a/f/f/h/i/k;

    move-result-object p1

    new-instance v1, Lf/h/a/f/k/b/e/n;

    invoke-direct {v1, p1}, Lf/h/a/f/k/b/e/n;-><init>(Lf/h/a/f/f/h/i/k;)V

    new-instance v2, Lf/h/a/f/k/b/e/o;

    invoke-direct {v2, p1}, Lf/h/a/f/k/b/e/o;-><init>(Lf/h/a/f/f/h/i/k;)V

    invoke-virtual {v0, p1, v1, v2}, Lf/h/a/f/k/b/e/i;->k(Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/a0;Lf/h/a/f/k/b/e/a0;)Lcom/google/android/gms/tasks/Task;

    iput-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->messagesClient:Lcom/google/android/gms/nearby/messages/MessagesClient;

    return-void
.end method

.method private final declared-synchronized foundUserId(J)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->meUserId:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-eqz v2, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    new-instance p2, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {p2, v0}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;-><init>(Ljava/util/Set;)V

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized lostUserId(J)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    new-instance p2, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {p2, v0}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;-><init>(Ljava/util/Set;)V

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final parseUserId(Lcom/google/android/gms/nearby/messages/Message;)Ljava/lang/Long;
    .locals 2

    iget-object p1, p1, Lcom/google/android/gms/nearby/messages/Message;->e:[B

    const-string v0, "message.content"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/String;

    sget-object v1, Lx/s/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 v1, 0x75

    if-ne p1, v1, :cond_0

    const/4 p1, 0x2

    :try_start_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).substring(startIndex)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final setupBroadcaster(J)V
    .locals 12

    sget-object v0, Lcom/google/android/gms/nearby/messages/Strategy;->l:Lcom/google/android/gms/nearby/messages/Strategy;

    new-instance v1, Lcom/discord/widgets/friends/NearbyManager$setupBroadcaster$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/friends/NearbyManager$setupBroadcaster$1;-><init>(Lcom/discord/widgets/friends/NearbyManager;)V

    new-instance v2, Lcom/google/android/gms/nearby/messages/PublishOptions;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/gms/nearby/messages/PublishOptions;-><init>(Lcom/google/android/gms/nearby/messages/Strategy;Lf/h/a/f/k/b/b;Lf/h/a/f/k/b/i;)V

    iput-object v2, p0, Lcom/discord/widgets/friends/NearbyManager;->messagePublishOptions:Lcom/google/android/gms/nearby/messages/PublishOptions;

    new-instance v0, Lcom/google/android/gms/nearby/messages/Message;

    const-string/jumbo v1, "u:"

    invoke-static {v1, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lx/s/a;->a:Ljava/nio/charset/Charset;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-static {p1, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    const-string p1, "(this as java.lang.String).getBytes(charset)"

    invoke-static {v6, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/gms/nearby/messages/Message;->j:[Lcom/google/android/gms/internal/nearby/zzgs;

    const/4 v5, 0x2

    const-string v7, ""

    const-string v8, ""

    const-wide/16 v10, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/nearby/messages/Message;-><init>(I[BLjava/lang/String;Ljava/lang/String;[Lcom/google/android/gms/internal/nearby/zzgs;J)V

    iput-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->outboundMessage:Lcom/google/android/gms/nearby/messages/Message;

    return-void
.end method

.method private final setupListener()V
    .locals 4

    new-instance v0, Lcom/discord/widgets/friends/NearbyManager$setupListener$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/friends/NearbyManager$setupListener$1;-><init>(Lcom/discord/widgets/friends/NearbyManager;)V

    iput-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->messageListener:Lcom/google/android/gms/nearby/messages/MessageListener;

    sget-object v0, Lcom/google/android/gms/nearby/messages/Strategy;->l:Lcom/google/android/gms/nearby/messages/Strategy;

    sget-object v0, Lcom/google/android/gms/nearby/messages/MessageFilter;->j:Lcom/google/android/gms/nearby/messages/MessageFilter;

    sget-object v1, Lcom/google/android/gms/nearby/messages/Strategy;->m:Lcom/google/android/gms/nearby/messages/Strategy;

    new-instance v2, Lcom/google/android/gms/nearby/messages/SubscribeOptions;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, Lcom/google/android/gms/nearby/messages/SubscribeOptions;-><init>(Lcom/google/android/gms/nearby/messages/Strategy;Lcom/google/android/gms/nearby/messages/MessageFilter;Lf/h/a/f/k/b/d;)V

    iput-object v2, p0, Lcom/discord/widgets/friends/NearbyManager;->subscribeOptions:Lcom/google/android/gms/nearby/messages/SubscribeOptions;

    return-void
.end method


# virtual methods
.method public final activateNearby()V
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->messagesClient:Lcom/google/android/gms/nearby/messages/MessagesClient;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/friends/NearbyManager;->outboundMessage:Lcom/google/android/gms/nearby/messages/Message;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/discord/widgets/friends/NearbyManager;->messagePublishOptions:Lcom/google/android/gms/nearby/messages/PublishOptions;

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/discord/widgets/friends/NearbyManager;->messageListener:Lcom/google/android/gms/nearby/messages/MessageListener;

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/discord/widgets/friends/NearbyManager;->subscribeOptions:Lcom/google/android/gms/nearby/messages/SubscribeOptions;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v6, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;

    new-instance v7, Ljava/util/HashSet;

    iget-object v8, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    invoke-direct {v7, v8}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v6, v7}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;-><init>(Ljava/util/Set;)V

    invoke-virtual {v5, v6}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/nearby/messages/MessagesClient;->f(Lcom/google/android/gms/nearby/messages/Message;Lcom/google/android/gms/nearby/messages/PublishOptions;)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/nearby/messages/MessagesClient;->g(Lcom/google/android/gms/nearby/messages/MessageListener;Lcom/google/android/gms/nearby/messages/SubscribeOptions;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final buildClientAndPublish(Landroidx/fragment/app/FragmentActivity;)V
    .locals 1

    const-string v0, "fragmentActivity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->messagesClient:Lcom/google/android/gms/nearby/messages/MessagesClient;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/NearbyManager;->buildClient(Landroidx/fragment/app/FragmentActivity;)V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/friends/NearbyManager;->activateNearby()V

    return-void
.end method

.method public final disableNearby()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->messagesClient:Lcom/google/android/gms/nearby/messages/MessagesClient;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/friends/NearbyManager;->outboundMessage:Lcom/google/android/gms/nearby/messages/Message;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/discord/widgets/friends/NearbyManager;->messageListener:Lcom/google/android/gms/nearby/messages/MessageListener;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/nearby/messages/MessagesClient;->h(Lcom/google/android/gms/nearby/messages/Message;)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/nearby/messages/MessagesClient;->i(Lcom/google/android/gms/nearby/messages/MessageListener;)Lcom/google/android/gms/tasks/Task;

    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyUserIds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;->INSTANCE:Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final getState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/friends/NearbyManager$NearbyState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->nearbyStateSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "nearbyStateSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final initialize(J)V
    .locals 1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/NearbyManager;->meUserId:Ljava/lang/Long;

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/NearbyManager;->setupBroadcaster(J)V

    invoke-direct {p0}, Lcom/discord/widgets/friends/NearbyManager;->setupListener()V

    return-void
.end method
