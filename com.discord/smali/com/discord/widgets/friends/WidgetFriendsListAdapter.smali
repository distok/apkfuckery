.class public final Lcom/discord/widgets/friends/WidgetFriendsListAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetFriendsListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;,
        Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingHeader;,
        Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemHeader;,
        Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;,
        Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
        ">;"
    }
.end annotation


# instance fields
.field private onClickAcceptFriend:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClickCall:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClickChat:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClickDeclineFriend:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClickPendingHeaderExpand:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClickUserProfile:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/view/View;",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickPendingHeaderExpand$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickPendingHeaderExpand$1;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickPendingHeaderExpand:Lkotlin/jvm/functions/Function0;

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickUserProfile$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickUserProfile$1;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickUserProfile:Lkotlin/jvm/functions/Function2;

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickCall$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickCall$1;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickCall:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickChat$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickChat$1;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickChat:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickAcceptFriend$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickAcceptFriend$1;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickAcceptFriend:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickDeclineFriend$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$onClickDeclineFriend$1;

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickDeclineFriend:Lkotlin/jvm/functions/Function2;

    return-void
.end method


# virtual methods
.method public final getOnClickAcceptFriend()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickAcceptFriend:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClickCall()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickCall:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClickChat()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickChat:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClickDeclineFriend()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickDeclineFriend:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getOnClickPendingHeaderExpand()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickPendingHeaderExpand:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnClickUserProfile()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Landroid/view/View;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickUserProfile:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_3

    const/4 p1, 0x1

    if-eq p2, p1, :cond_2

    const/4 p1, 0x2

    if-eq p2, p1, :cond_1

    const/4 p1, 0x3

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemHeader;

    invoke-direct {p1, p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemHeader;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingHeader;

    invoke-direct {p1, p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingHeader;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;

    invoke-direct {p1, p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V

    goto :goto_0

    :cond_3
    new-instance p1, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;

    invoke-direct {p1, p0}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V

    :goto_0
    return-object p1
.end method

.method public final setOnClickAcceptFriend(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickAcceptFriend:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnClickCall(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickCall:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnClickChat(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickChat:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnClickDeclineFriend(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickDeclineFriend:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setOnClickPendingHeaderExpand(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickPendingHeaderExpand:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setOnClickUserProfile(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/view/View;",
            "-",
            "Lcom/discord/models/domain/ModelUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->onClickUserProfile:Lkotlin/jvm/functions/Function2;

    return-void
.end method
