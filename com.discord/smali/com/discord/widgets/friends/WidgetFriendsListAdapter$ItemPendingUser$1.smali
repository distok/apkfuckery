.class public final Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$1;
.super Ljava/lang/Object;
.source "WidgetFriendsListAdapter.kt"

# interfaces
.implements Lrx/functions/Action3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;-><init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action3<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$1;->$adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/friends/FriendsListViewModel$Item;)V
    .locals 1

    iget-object p2, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$1;->$adapter:Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    invoke-virtual {p2}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->getOnClickUserProfile()Lkotlin/jvm/functions/Function2;

    move-result-object p2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.friends.FriendsListViewModel.Item.PendingFriendRequest"

    invoke-static {p3, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p3, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;

    invoke-virtual {p3}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p3

    invoke-interface {p2, p1, p3}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/widgets/friends/FriendsListViewModel$Item;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser$1;->call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/friends/FriendsListViewModel$Item;)V

    return-void
.end method
