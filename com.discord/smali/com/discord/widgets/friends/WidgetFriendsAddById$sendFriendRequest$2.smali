.class public final Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;
.super Ljava/lang/Object;
.source "WidgetFriendsAddById.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/WidgetFriendsAddById;->sendFriendRequest(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/utilities/error/Error;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $discriminator:I

.field public final synthetic $username:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/WidgetFriendsAddById;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById;

    iput-object p2, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->$username:Ljava/lang/String;

    iput p3, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->$discriminator:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/utilities/error/Error;)V
    .locals 6

    sget-object v0, Lcom/discord/utilities/rest/RestAPIAbortMessages;->INSTANCE:Lcom/discord/utilities/rest/RestAPIAbortMessages;

    const-string v1, "error"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;-><init>(Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;Lcom/discord/utilities/error/Error;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/rest/RestAPIAbortMessages;->handleAbortCodeOrDefault$default(Lcom/discord/utilities/rest/RestAPIAbortMessages;Lcom/discord/utilities/error/Error;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/utilities/error/Error;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->call(Lcom/discord/utilities/error/Error;)V

    return-void
.end method
