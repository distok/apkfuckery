.class public abstract Lcom/discord/widgets/friends/FriendsListViewModel$Item;
.super Ljava/lang/Object;
.source "FriendsListViewModel.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/FriendsListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Item$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/friends/FriendsListViewModel$Item$Companion;

.field public static final TYPE_FRIEND:I = 0x0

.field public static final TYPE_HEADER:I = 0x3

.field public static final TYPE_PENDING_FRIEND:I = 0x1

.field public static final TYPE_PENDING_HEADER:I = 0x2


# instance fields
.field private final _type:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/friends/FriendsListViewModel$Item;->Companion:Lcom/discord/widgets/friends/FriendsListViewModel$Item$Companion;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item;->_type:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item;->_type:I

    return v0
.end method

.method public final get_type()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item;->_type:I

    return v0
.end method
