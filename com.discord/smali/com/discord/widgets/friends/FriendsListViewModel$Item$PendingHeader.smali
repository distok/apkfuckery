.class public final Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;
.super Lcom/discord/widgets/friends/FriendsListViewModel$Item;
.source "FriendsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/FriendsListViewModel$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PendingHeader"
.end annotation


# instance fields
.field private final count:I

.field private final isPendingSectionExpanded:Z

.field private final showExpandButton:Z

.field private final titleStringResId:I


# direct methods
.method public constructor <init>(IIZZ)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    iput p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    iput-boolean p3, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    iput-boolean p4, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;IIZZILjava/lang/Object;)Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->copy(IIZZ)Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    return v0
.end method

.method public final copy(IIZZ)Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;-><init>(IIZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    iget v1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    iget v1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    iget-boolean v1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    iget-boolean p1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/friends/FriendsListViewModel$Item;->get_type()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShowExpandButton()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    return v0
.end method

.method public final getTitleStringResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    add-int/2addr v0, v2

    return v0
.end method

.method public final isPendingSectionExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "PendingHeader(titleStringResId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->titleStringResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isPendingSectionExpanded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->isPendingSectionExpanded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showExpandButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->showExpandButton:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
