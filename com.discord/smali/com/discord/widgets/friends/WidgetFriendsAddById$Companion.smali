.class public final Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion;
.super Ljava/lang/Object;
.source "WidgetFriendsAddById.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/WidgetFriendsAddById;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$extractUsernameAndDiscriminator(Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion;Ljava/lang/CharSequence;)Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion;->extractUsernameAndDiscriminator(Ljava/lang/CharSequence;)Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;

    move-result-object p0

    return-object p0
.end method

.method private final extractUsernameAndDiscriminator(Ljava/lang/CharSequence;)Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;
    .locals 3

    invoke-static {}, Lcom/discord/widgets/friends/WidgetFriendsAddById;->access$getPATTERN_USERNAME$cp()Lkotlin/text/Regex;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/text/Regex;->matchEntire(Ljava/lang/CharSequence;)Lkotlin/text/MatchResult;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lx/s/e;

    invoke-virtual {v0}, Lx/s/e;->getGroupValues()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    new-instance p1, Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;

    invoke-virtual {v0}, Lx/s/e;->getGroupValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lx/s/e;->getGroupValues()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
