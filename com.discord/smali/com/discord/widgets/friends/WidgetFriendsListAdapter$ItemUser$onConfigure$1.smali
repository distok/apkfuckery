.class public final Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetFriendsListAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;->onConfigure(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $item:Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;

.field public final synthetic this$0:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser$onConfigure$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;

    iput-object p2, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser$onConfigure$1;->$item:Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser$onConfigure$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;

    invoke-static {p1}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;->access$getAdapter$p(Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;)Lcom/discord/widgets/friends/WidgetFriendsListAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/friends/WidgetFriendsListAdapter;->getOnClickCall()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser$onConfigure$1;->$item:Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;

    invoke-virtual {v0}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
