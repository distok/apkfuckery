.class public final Lcom/discord/widgets/friends/FriendsListViewModel$1;
.super Lx/m/c/k;
.source "FriendsListViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/FriendsListViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/utilities/rest/RestAPI;ZLcom/discord/utilities/channel/ChannelUtils;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/friends/FriendsListViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$1;->this$0:Lcom/discord/widgets/friends/FriendsListViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel$1;->invoke(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V
    .locals 1

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$1;->this$0:Lcom/discord/widgets/friends/FriendsListViewModel;

    invoke-static {v0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel;->access$handleStoreState(Lcom/discord/widgets/friends/FriendsListViewModel;Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V

    return-void
.end method
