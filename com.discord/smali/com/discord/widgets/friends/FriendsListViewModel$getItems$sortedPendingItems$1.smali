.class public final Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;
.super Ljava/lang/Object;
.source "FriendsListViewModel.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/FriendsListViewModel;->getItems(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;

    invoke-direct {v0}, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;)I
    .locals 3

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getRelationshipType()I

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getRelationshipType()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getRelationshipType()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getRelationshipType()I

    move-result v0

    if-ne v0, v2, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/discord/models/domain/ModelUser;->compareUserNames(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;)I

    move-result p1

    :goto_0
    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;

    check-cast p2, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;->compare(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;)I

    move-result p1

    return p1
.end method
