.class public final Lcom/discord/widgets/friends/FriendsListViewModel;
.super Lf/a/b/l0;
.source "FriendsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Event;,
        Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;,
        Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Item;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Factory;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final COLLAPSED_PENDING_ITEM_COUNT:I = 0x2

.field public static final Companion:Lcom/discord/widgets/friends/FriendsListViewModel$Companion;

.field private static final LOCATION:Ljava/lang/String; = "Friends List"


# instance fields
.field private final channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

.field private computeItemJob:Lrx/functions/Cancellable;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final initRequestSectionExpanded:Z

.field private isPendingSectionExpanded:Z

.field private listSections:Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeChannels:Lcom/discord/stores/StoreChannels;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/friends/FriendsListViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/friends/FriendsListViewModel;->Companion:Lcom/discord/widgets/friends/FriendsListViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/utilities/rest/RestAPI;ZLcom/discord/utilities/channel/ChannelUtils;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Z",
            "Lcom/discord/utilities/channel/ChannelUtils;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannels"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelUtils"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->storeObservable:Lrx/Observable;

    iput-object p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object p3, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-boolean p4, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->initRequestSectionExpanded:Z

    iput-object p5, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

    new-instance p2, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    sget-object p3, Lx/h/l;->d:Lx/h/l;

    const/4 p5, 0x0

    invoke-direct {p2, p5, p3, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;)V

    iput-object p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->listSections:Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    iput-boolean p4, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->isPendingSectionExpanded:Z

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 p2, 0x2

    invoke-static {p1, p0, p5, p2, p5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/friends/FriendsListViewModel;

    new-instance v6, Lcom/discord/widgets/friends/FriendsListViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/friends/FriendsListViewModel$1;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/utilities/rest/RestAPI;ZLcom/discord/utilities/channel/ChannelUtils;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x8

    if-eqz p7, :cond_0

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p4

    :goto_0
    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    sget-object p4, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {p4}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object p5

    :cond_1
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/friends/FriendsListViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/utilities/rest/RestAPI;ZLcom/discord/utilities/channel/ChannelUtils;)V

    return-void
.end method

.method public static final synthetic access$emitLaunchVoiceCallEvent(Lcom/discord/widgets/friends/FriendsListViewModel;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel;->emitLaunchVoiceCallEvent(J)V

    return-void
.end method

.method public static final synthetic access$emitShowFriendRequestAbortToast(Lcom/discord/widgets/friends/FriendsListViewModel;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel;->emitShowFriendRequestAbortToast(ILjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$emitShowToastEvent(Lcom/discord/widgets/friends/FriendsListViewModel;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel;->emitShowToastEvent(I)V

    return-void
.end method

.method public static final synthetic access$getItems(Lcom/discord/widgets/friends/FriendsListViewModel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/friends/FriendsListViewModel;->getItems(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleComputedItems(Lcom/discord/widgets/friends/FriendsListViewModel;Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel;->handleComputedItems(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/friends/FriendsListViewModel;Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel;->handleStoreState(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V

    return-void
.end method

.method private final asyncComputeAndHandleOnUiThread(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/functions/Cancellable;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/error/Error;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/functions/Cancellable;"
        }
    .end annotation

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v2, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$1;

    invoke-direct {v2, p1}, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    sget-object p1, Lrx/Emitter$BackpressureMode;->d:Lrx/Emitter$BackpressureMode;

    invoke-static {v2, p1}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v2

    invoke-virtual {p1, v2}, Lrx/Observable;->S(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    const-string v2, "Observable\n        .crea\u2026Schedulers.computation())"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {p1, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/friends/FriendsListViewModel;

    new-instance v9, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$2;

    invoke-direct {v9, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    new-instance v7, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$3;

    invoke-direct {v7, p2}, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$3;-><init>(Lkotlin/jvm/functions/Function1;)V

    new-instance v6, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$4;

    invoke-direct {v6, v0}, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$4;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;)V

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x12

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    new-instance p1, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$5;

    invoke-direct {p1, v0}, Lcom/discord/widgets/friends/FriendsListViewModel$asyncComputeAndHandleOnUiThread$5;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;)V

    return-object p1
.end method

.method public static synthetic asyncComputeAndHandleOnUiThread$default(Lcom/discord/widgets/friends/FriendsListViewModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/functions/Cancellable;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/friends/FriendsListViewModel;->asyncComputeAndHandleOnUiThread(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/functions/Cancellable;

    move-result-object p0

    return-object p0
.end method

.method private final emitLaunchVoiceCallEvent(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowFriendRequestAbortToast(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;-><init>(ILjava/lang/String;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowToastEvent(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;

    invoke-direct {v1, p1}, Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;-><init>(I)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final generateLoadedItems()V
    .locals 8
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->listSections:Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    invoke-virtual {v0}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->getPendingHeaderItem()Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->isPendingSectionExpanded:Z

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->copy$default(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;IIZZILjava/lang/Object;)Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    iget-object v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->listSections:Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->copy$default(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->listSections:Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    invoke-direct {p0, v0}, Lcom/discord/widgets/friends/FriendsListViewModel;->getVisibleItems(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;

    invoke-direct {v1, v0}, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;-><init>(Ljava/util/List;)V

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;

    :goto_1
    invoke-virtual {p0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final getItems(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;)",
            "Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$1;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$1;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lx/h/f;->asSequence(Ljava/util/Map;)Lkotlin/sequences/Sequence;

    move-result-object p1

    check-cast p1, Lx/h/i;

    invoke-virtual {p1}, Lx/h/i;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {p2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelUser;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/models/domain/ModelUserRelationship;->getType(Ljava/lang/Integer;)I

    move-result v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/models/domain/ModelPresence;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p4, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v6, :cond_0

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$1;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$1;

    invoke-virtual {v3, v2}, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$1;->invoke(I)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;

    invoke-direct {v3, v6, v7, v2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;I)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;

    invoke-direct {v2, v6, v7, v4}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$sortedPendingItems$1;

    invoke-static {v0, p1}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    const/4 p2, 0x0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    const/4 p4, 0x1

    xor-int/2addr p3, p4

    if-eqz p3, :cond_4

    new-instance p2, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    const p3, 0x7f12086d

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v4, 0x0

    if-le v2, v3, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-direct {p2, p3, v0, v4, v2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;-><init>(IIZZ)V

    :cond_4
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;

    invoke-virtual {v4}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;->isOnline()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    sget-object v2, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$onlineFriendItems$2;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$onlineFriendItems$2;

    invoke-static {v0, v2}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, p4

    if-eqz v2, :cond_7

    new-instance v2, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;

    const v3, 0x7f120869

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;-><init>(II)V

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;

    invoke-virtual {v3}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;->isOnline()Z

    move-result v3

    xor-int/2addr v3, p4

    if-eqz v3, :cond_8

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_9
    sget-object v1, Lcom/discord/widgets/friends/FriendsListViewModel$getItems$offlineFriendItems$2;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$getItems$offlineFriendItems$2;

    invoke-static {v0, v1}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr p4, v1

    if-eqz p4, :cond_a

    new-instance p4, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;

    const v1, 0x7f120868

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {p4, v1, v2}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;-><init>(II)V

    invoke-virtual {p3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_a
    new-instance p4, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    invoke-direct {p4, p2, p1, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;)V

    return-object p4
.end method

.method private final getVisibleItems(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->component1()Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->component2()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->component3()Ljava/util/List;

    move-result-object p1

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean v2, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->isPendingSectionExpanded:Z

    if-eqz v2, :cond_1

    const v2, 0x7fffffff

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    :goto_0
    invoke-static {v1, v2}, Lx/h/f;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final handleComputedItems(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;)V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->listSections:Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    invoke-direct {p0}, Lcom/discord/widgets/friends/FriendsListViewModel;->generateLoadedItems()V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->computeItemJob:Lrx/functions/Cancellable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/functions/Cancellable;->cancel()V

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;->getRelationshipsState()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;->getRelationshipsState()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;->getRelationships()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object p1, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_1
    new-instance v1, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;Ljava/util/Map;Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V

    const/4 v2, 0x0

    new-instance v3, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$2;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/friends/FriendsListViewModel;->asyncComputeAndHandleOnUiThread$default(Lcom/discord/widgets/friends/FriendsListViewModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/functions/Cancellable;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->computeItemJob:Lrx/functions/Cancellable;

    return-void

    :cond_2
    sget-object p1, Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final acceptFriendRequest(JLjava/lang/String;)V
    .locals 11

    const-string/jumbo v0, "username"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    const-string v2, "Friends List"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    move-wide v3, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/rest/RestAPI;->addRelationship$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/friends/FriendsListViewModel;

    new-instance v8, Lcom/discord/widgets/friends/FriendsListViewModel$acceptFriendRequest$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/friends/FriendsListViewModel$acceptFriendRequest$1;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V

    new-instance v6, Lcom/discord/widgets/friends/FriendsListViewModel$acceptFriendRequest$2;

    invoke-direct {v6, p0, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$acceptFriendRequest$2;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getInitRequestSectionExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->initRequestSectionExpanded:Z

    return v0
.end method

.method public final getRestAPI()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final getStoreChannels()Lcom/discord/stores/StoreChannels;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-object v0
.end method

.method public final getStoreObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->storeObservable:Lrx/Observable;

    return-object v0
.end method

.method public final handleClickPendingHeader()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->isPendingSectionExpanded:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->isPendingSectionExpanded:Z

    invoke-direct {p0}, Lcom/discord/widgets/friends/FriendsListViewModel;->generateLoadedItems()V

    return-void
.end method

.method public final launchVoiceCall(J)V
    .locals 11

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/channel/ChannelUtils;->createPrivateChannel(J)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/friends/FriendsListViewModel;

    new-instance v8, Lcom/discord/widgets/friends/FriendsListViewModel$launchVoiceCall$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/friends/FriendsListViewModel$launchVoiceCall$1;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V

    new-instance v6, Lcom/discord/widgets/friends/FriendsListViewModel$launchVoiceCall$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/friends/FriendsListViewModel$launchVoiceCall$2;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final removeFriendRequest(JI)V
    .locals 11

    const/4 v0, 0x3

    if-ne p3, v0, :cond_0

    const p3, 0x7f120852

    goto :goto_0

    :cond_0
    const p3, 0x7f12084f

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    const-string v1, "Friends List"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->removeRelationship(Ljava/lang/String;J)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/friends/FriendsListViewModel;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v8, Lcom/discord/widgets/friends/FriendsListViewModel$removeFriendRequest$1;

    invoke-direct {v8, p0, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$removeFriendRequest$1;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;I)V

    const/4 v7, 0x0

    new-instance v6, Lcom/discord/widgets/friends/FriendsListViewModel$removeFriendRequest$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/friends/FriendsListViewModel$removeFriendRequest$2;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel;)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
