.class public final Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;
.super Lx/m/c/k;
.source "WidgetFriendsAddById.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->call(Lcom/discord/utilities/error/Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $error:Lcom/discord/utilities/error/Error;

.field public final synthetic this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;Lcom/discord/utilities/error/Error;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;

    iput-object p2, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->$error:Lcom/discord/utilities/error/Error;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;

    iget-object v0, v0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById;

    invoke-static {v0}, Lcom/discord/widgets/friends/WidgetFriendsAddById;->access$getInputEditText$p(Lcom/discord/widgets/friends/WidgetFriendsAddById;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    const v1, 0x7f130249

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setErrorTextAppearance(I)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;->INSTANCE:Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;

    iget-object v1, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;

    iget-object v1, v1, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->$error:Lcom/discord/utilities/error/Error;

    const-string v3, "error"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v2

    const-string v3, "error.response"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/utilities/error/Error$Response;->getCode()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;

    iget-object v4, v4, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->$username:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;

    iget v4, v4, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->$discriminator:I

    invoke-static {v4}, Lcom/discord/models/domain/ModelUser;->getDiscriminatorWithPadding(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;->getRelationshipResponse(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2$1;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;

    iget-object v1, v1, Lcom/discord/widgets/friends/WidgetFriendsAddById$sendFriendRequest$2;->this$0:Lcom/discord/widgets/friends/WidgetFriendsAddById;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/discord/widgets/friends/WidgetFriendsAddById;->access$setInputEditError(Lcom/discord/widgets/friends/WidgetFriendsAddById;Ljava/lang/String;)V

    return-void
.end method
