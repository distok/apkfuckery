.class public final Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;
.super Ljava/lang/Object;
.source "WidgetFriendsFindNearby.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;->get(Lrx/Observable;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/friends/NearbyManager$NearbyState;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/friends/NearbyManager$NearbyState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider$get$1;->call(Lcom/discord/widgets/friends/NearbyManager$NearbyState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/widgets/friends/NearbyManager$NearbyState;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/friends/NearbyManager$NearbyState;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;",
            ">;"
        }
    .end annotation

    instance-of v0, p1, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;

    check-cast p1, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;

    invoke-virtual {p1}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;->getCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;-><init>(Ljava/lang/Integer;)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Uninitialized;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Uninitialized;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_0
    move-object p1, v0

    goto :goto_1

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;

    invoke-virtual {p1}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;->getNearbyUserIds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;

    invoke-virtual {p1}, Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;->getNearbyUserIds()Ljava/util/Set;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;->access$getUserModels(Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;Ljava/util/Collection;)Lrx/Observable;

    move-result-object p1

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Empty;->INSTANCE:Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Empty;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
