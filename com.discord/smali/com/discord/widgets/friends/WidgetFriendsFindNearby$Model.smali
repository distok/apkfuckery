.class public abstract Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;
.super Ljava/lang/Object;
.source "WidgetFriendsFindNearby.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/WidgetFriendsFindNearby;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;,
        Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Empty;,
        Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;,
        Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Uninitialized;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;-><init>()V

    return-void
.end method
