.class public final Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
.super Ljava/lang/Object;
.source "FriendsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/FriendsListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListSections"
.end annotation


# instance fields
.field private final friendsItemsWithHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

.field private final pendingItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pendingItems"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "friendsItemsWithHeaders"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    iput-object p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->copy(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;)",
            "Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;"
        }
    .end annotation

    const-string v0, "pendingItems"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "friendsItemsWithHeaders"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;-><init>(Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    iget-object v1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFriendsItemsWithHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    return-object v0
.end method

.method public final getPendingHeaderItem()Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    return-object v0
.end method

.method public final getPendingItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/friends/FriendsListViewModel$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ListSections(pendingHeaderItem="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingHeaderItem:Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pendingItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->pendingItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", friendsItemsWithHeaders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;->friendsItemsWithHeaders:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
