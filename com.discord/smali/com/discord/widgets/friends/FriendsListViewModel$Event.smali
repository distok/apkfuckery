.class public abstract Lcom/discord/widgets/friends/FriendsListViewModel$Event;
.super Ljava/lang/Object;
.source "FriendsListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/friends/FriendsListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;,
        Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/friends/FriendsListViewModel$Event;-><init>()V

    return-void
.end method
