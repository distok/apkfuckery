.class public final Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;
.super Lx/m/c/k;
.source "FriendsListViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/friends/FriendsListViewModel;->handleStoreState(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $relationships:Ljava/util/Map;

.field public final synthetic $storeState:Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;

.field public final synthetic this$0:Lcom/discord/widgets/friends/FriendsListViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/friends/FriendsListViewModel;Ljava/util/Map;Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->this$0:Lcom/discord/widgets/friends/FriendsListViewModel;

    iput-object p2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->$relationships:Ljava/util/Map;

    iput-object p3, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->$storeState:Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->this$0:Lcom/discord/widgets/friends/FriendsListViewModel;

    iget-object v1, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->$relationships:Ljava/util/Map;

    iget-object v2, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->$storeState:Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;

    invoke-virtual {v2}, Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;->getUsers()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->$storeState:Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;

    invoke-virtual {v3}, Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;->getPresences()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->$storeState:Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;

    invoke-virtual {v4}, Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;->getApplicationStreams()Ljava/util/Map;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/discord/widgets/friends/FriendsListViewModel;->access$getItems(Lcom/discord/widgets/friends/FriendsListViewModel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/friends/FriendsListViewModel$handleStoreState$1;->invoke()Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;

    move-result-object v0

    return-object v0
.end method
