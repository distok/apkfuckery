.class public final Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;
.super Ljava/lang/Object;
.source "FeedbackSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final feedbackIssues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation
.end field

.field private final issuesHeaderTextResId:I

.field private final promptTextResId:I

.field private final selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field private final titleTextResId:I


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)V
    .locals 1
    .param p3    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;III)V"
        }
    .end annotation

    const-string v0, "selectedFeedbackRating"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedbackIssues"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    iput-object p2, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    iput p3, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    iput p4, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    iput p5, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;IIIILjava/lang/Object;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->copy(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    return v0
.end method

.method public final copy(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;
    .locals 7
    .param p3    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;III)",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;"
        }
    .end annotation

    const-string v0, "selectedFeedbackRating"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedbackIssues"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    iget-object v1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    iget v1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    iget v1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    iget p1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFeedbackIssues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    return-object v0
.end method

.method public final getIssuesHeaderTextResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    return v0
.end method

.method public final getPromptTextResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    return v0
.end method

.method public final getSelectedFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method

.method public final getTitleTextResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(selectedFeedbackRating="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->selectedFeedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", feedbackIssues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->feedbackIssues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", titleTextResId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->titleTextResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", promptTextResId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->promptTextResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", issuesHeaderTextResId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->issuesHeaderTextResId:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
