.class public final Lcom/discord/widgets/feedback/WidgetFeedbackSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetFeedbackSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;,
        Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final ARG_CALL_FEEDBACK_CONFIG:Ljava/lang/String; = "ARG_CONFIG"

.field public static final ARG_FEEDBACK_TYPE:Ljava/lang/String; = "ARG_FEEDBACK_TYPE"

.field public static final ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID:Ljava/lang/String; = "ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID"

.field public static final ARG_STREAM_FEEDBACK_STREAM_KEY:Ljava/lang/String; = "ARG_STREAM_FEEDBACK_STREAM_KEY "

.field public static final Companion:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;

.field private static final SHEET_DISMISS_DELAY_MS:J = 0x258L

.field private static final SHEET_EXPAND_DELAY_MS:J = 0x64L


# instance fields
.field private final closeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final feedbackView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private isShowingFeedbackIssues:Z

.field private onDismissed:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final titleTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/feedback/FeedbackSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const-string v6, "closeButton"

    const-string v7, "getCloseButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const-string v6, "feedbackView"

    const-string v7, "getFeedbackView()Lcom/discord/widgets/voice/feedback/FeedbackView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const-string/jumbo v6, "titleTextView"

    const-string v7, "getTitleTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->Companion:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    sget-object v0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$onDismissed$1;->INSTANCE:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$onDismissed$1;

    iput-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->onDismissed:Lkotlin/jvm/functions/Function0;

    const v0, 0x7f0a0420

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a041f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->closeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0421

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->feedbackView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0422

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->titleTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->viewModel:Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->handleEvent(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$isShowingFeedbackIssues$p(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->isShowingFeedbackIssues:Z

    return p0
.end method

.method public static final synthetic access$setShowingFeedbackIssues$p(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->isShowingFeedbackIssues:Z

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;Lcom/discord/widgets/feedback/FeedbackSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->viewModel:Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->updateView(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;)V

    return-void
.end method

.method private final getCloseButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->closeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFeedbackView()Lcom/discord/widgets/voice/feedback/FeedbackView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->feedbackView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/feedback/FeedbackView;

    return-object v0
.end method

.method private final getTitleTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->titleTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;)V
    .locals 11

    instance-of v0, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;->getShowConfirmation()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireDialog()Landroid/app/Dialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->cancel()V

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x258

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n             \u2026S, TimeUnit.MILLISECONDS)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, p0, v1, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$handleEvent$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$handleEvent$1;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->Companion:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->getPendingFeedback()Lcom/discord/widgets/voice/feedback/PendingFeedback;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->getShowCxLinkForIssueDetails()Z

    move-result p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;->launch(Landroid/content/Context;Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireDialog()Landroid/app/Dialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->cancel()V

    :cond_2
    :goto_0
    return-void
.end method

.method private final updateView(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;)V
    .locals 12

    iget-boolean v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->isShowingFeedbackIssues:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getFeedbackIssues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->getContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppBottomSheet;->updatePeekHeightPx(I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/discord/app/AppBottomSheet;->setBottomSheetState(I)V

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getTitleTextResId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->getFeedbackView()Lcom/discord/widgets/voice/feedback/FeedbackView;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getPromptTextResId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v0, "getString(viewState.promptTextResId)"

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getSelectedFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object v5

    new-instance v6, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$1;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    new-instance v7, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$2;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    new-instance v8, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$3;

    invoke-direct {v8, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$3;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getIssuesHeaderTextResId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v0, "getString(viewState.issuesHeaderTextResId)"

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getFeedbackIssues()Ljava/util/List;

    move-result-object v10

    new-instance v11, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$4;

    invoke-direct {v11, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$4;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    invoke-virtual/range {v3 .. v11}, Lcom/discord/widgets/voice/feedback/FeedbackView;->updateView(Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    if-eqz v1, :cond_2

    const-wide/16 v0, 0x64

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n          .ti\u2026S, TimeUnit.MILLISECONDS)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, p0, v1, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$5;

    invoke-direct {v8, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$5;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
    .locals 11

    const-string v0, "compositeSubscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppBottomSheet;->bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V

    iget-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->viewModel:Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    const/4 v0, 0x0

    const-string/jumbo v1, "viewModel"

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel;->observeViewState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$bindSubscriptions$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$bindSubscriptions$1;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->viewModel:Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel;->observeEvents()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$bindSubscriptions$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$bindSubscriptions$2;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01ef

    return v0
.end method

.method public final getOnDismissed()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->onDismissed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    iget-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->onDismissed:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->getCloseButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$onResume$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$onResume$1;-><init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "ARG_FEEDBACK_TYPE"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type com.discord.widgets.feedback.WidgetFeedbackSheet.FeedbackType"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const-string p2, "requireArguments()"

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance p1, Lcom/discord/widgets/feedback/StreamFeedbackSheetViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/discord/widgets/feedback/StreamFeedbackSheetViewModelProvider;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/feedback/CallFeedbackSheetViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/discord/widgets/feedback/CallFeedbackSheetViewModelProvider;-><init>(Landroid/os/Bundle;)V

    :goto_0
    :try_start_0
    invoke-interface {p1, p0}, Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider;->get(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->viewModel:Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
    :try_end_0
    .catch Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider$ErrorProvidingViewModelExpection; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    move-object v2, p1

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "error providing view model"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->requireDialog()Landroid/app/Dialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->cancel()V

    :goto_1
    return-void
.end method

.method public final setOnDismissed(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->onDismissed:Lkotlin/jvm/functions/Function0;

    return-void
.end method
