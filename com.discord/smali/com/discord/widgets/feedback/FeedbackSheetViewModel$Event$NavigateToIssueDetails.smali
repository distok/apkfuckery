.class public final Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;
.super Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;
.source "FeedbackSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NavigateToIssueDetails"
.end annotation


# instance fields
.field private final pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

.field private final showCxLinkForIssueDetails:Z


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)V
    .locals 1

    const-string v0, "pendingFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    iput-boolean p2, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;Lcom/discord/widgets/voice/feedback/PendingFeedback;ZILjava/lang/Object;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->copy(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/voice/feedback/PendingFeedback;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    return v0
.end method

.method public final copy(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;
    .locals 1

    const-string v0, "pendingFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    iget-object v1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    iget-boolean p1, p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPendingFeedback()Lcom/discord/widgets/voice/feedback/PendingFeedback;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    return-object v0
.end method

.method public final getShowCxLinkForIssueDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "NavigateToIssueDetails(pendingFeedback="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showCxLinkForIssueDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;->showCxLinkForIssueDetails:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
