.class public final Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;
.super Ljava/lang/Object;
.source "WidgetFeedbackSheet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/feedback/WidgetFeedbackSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final newInstance(Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;)Lcom/discord/widgets/feedback/WidgetFeedbackSheet;
    .locals 3

    const-string v0, "feedbackType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    invoke-direct {v0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ARG_FEEDBACK_TYPE"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method
