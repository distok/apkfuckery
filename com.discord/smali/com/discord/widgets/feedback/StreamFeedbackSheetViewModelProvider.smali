.class public final Lcom/discord/widgets/feedback/StreamFeedbackSheetViewModelProvider;
.super Ljava/lang/Object;
.source "FeedbackSheetViewModelProviders.kt"

# interfaces
.implements Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider;


# instance fields
.field private final args:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "args"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/feedback/StreamFeedbackSheetViewModelProvider;->args:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public get(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
    .locals 8

    const-string v0, "sheet"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/feedback/StreamFeedbackSheetViewModelProvider;->args:Landroid/os/Bundle;

    const-string v1, "ARG_STREAM_FEEDBACK_STREAM_KEY "

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v0, "args.getString(WidgetFee\u2026AM_FEEDBACK_STREAM_KEY)!!"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/feedback/StreamFeedbackSheetViewModelProvider;->args:Landroid/os/Bundle;

    const-string v1, "ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/stores/StoreExperiments;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, p1, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    return-object p1
.end method
