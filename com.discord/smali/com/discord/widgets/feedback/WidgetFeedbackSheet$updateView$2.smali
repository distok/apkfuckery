.class public final Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$2;
.super Lx/m/c/k;
.source "WidgetFeedbackSheet.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->updateView(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/feedback/WidgetFeedbackSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$2;->this$0:Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$updateView$2;->this$0:Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    invoke-static {v0}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->access$getViewModel$p(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NEUTRAL:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-interface {v0, v1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel;->selectRating(Lcom/discord/widgets/voice/feedback/FeedbackRating;)V

    return-void
.end method
