.class public interface abstract Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
.super Ljava/lang/Object;
.source "FeedbackSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;,
        Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;
    }
.end annotation


# virtual methods
.method public abstract observeEvents()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end method

.method public abstract observeViewState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract selectIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V
.end method

.method public abstract selectRating(Lcom/discord/widgets/voice/feedback/FeedbackRating;)V
.end method

.method public abstract submitForm()V
.end method
