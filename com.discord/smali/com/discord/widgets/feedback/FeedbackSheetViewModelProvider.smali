.class public interface abstract Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider;
.super Ljava/lang/Object;
.source "FeedbackSheetViewModelProviders.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider$ErrorProvidingViewModelExpection;
    }
.end annotation


# virtual methods
.method public abstract get(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
.end method
