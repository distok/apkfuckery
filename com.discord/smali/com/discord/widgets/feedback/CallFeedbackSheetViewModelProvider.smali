.class public final Lcom/discord/widgets/feedback/CallFeedbackSheetViewModelProvider;
.super Ljava/lang/Object;
.source "FeedbackSheetViewModelProviders.kt"

# interfaces
.implements Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider;


# instance fields
.field private final args:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "args"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/feedback/CallFeedbackSheetViewModelProvider;->args:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public get(Lcom/discord/widgets/feedback/WidgetFeedbackSheet;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel;
    .locals 5

    const-string v0, "sheet"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/feedback/CallFeedbackSheetViewModelProvider;->args:Landroid/os/Bundle;

    const-string v1, "ARG_CONFIG"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    if-eqz v0, :cond_0

    const-string v1, "args.getParcelable<CallF\u2026ss.simpleName}\"\n        )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v2, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v2, v0, v4, v3, v4}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;-><init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;Lcom/discord/stores/StoreExperiments;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v1, p1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;

    invoke-virtual {v1, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel;

    return-object p1

    :cond_0
    new-instance p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider$ErrorProvidingViewModelExpection;

    const-string v0, "failed to get parcelable Config for "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/feedback/CallFeedbackSheetViewModelProvider;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/discord/widgets/feedback/FeedbackSheetViewModelProvider$ErrorProvidingViewModelExpection;-><init>(Ljava/lang/String;)V

    throw p1
.end method
