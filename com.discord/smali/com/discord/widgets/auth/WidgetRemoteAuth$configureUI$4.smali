.class public final Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$4;
.super Ljava/lang/Object;
.source "WidgetRemoteAuth.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetRemoteAuth;->configureUI(LWidgetRemoteAuthViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetRemoteAuth;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$4;->this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 12

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$4;->this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;

    invoke-static {p1}, Lcom/discord/widgets/auth/WidgetRemoteAuth;->access$getViewModel$p(Lcom/discord/widgets/auth/WidgetRemoteAuth;)LWidgetRemoteAuthViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LWidgetRemoteAuthViewModel$ViewState$b;

    if-eqz v0, :cond_0

    iget-object v0, p1, LWidgetRemoteAuthViewModel;->e:Lcom/discord/utilities/rest/RestAPI;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object p1

    const-string v2, "null cannot be cast to non-null type WidgetRemoteAuthViewModel.ViewState.Loaded"

    invoke-static {p1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, LWidgetRemoteAuthViewModel$ViewState$b;

    iget-object p1, p1, LWidgetRemoteAuthViewModel$ViewState$b;->a:Ljava/lang/String;

    invoke-direct {v1, p1}, Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI;->postRemoteAuthCancel(Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, LWidgetRemoteAuthViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lp;->d:Lp;

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$4;->this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method
