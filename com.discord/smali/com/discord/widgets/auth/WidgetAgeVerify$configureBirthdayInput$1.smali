.class public final Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;
.super Lx/m/c/k;
.source "WidgetAgeVerify.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAgeVerify;->configureBirthdayInput(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $timeOfBirth:Ljava/lang/Long;

.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAgeVerify;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;->$timeOfBirth:Ljava/lang/Long;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 8

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lf/a/a/j;->k:Lf/a/a/j$a;

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string p1, "parentFragmentManager"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    const v0, 0x7f1200d6

    invoke-virtual {p1, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string p1, "getString(R.string.age_gate_date_of_birth)"

    invoke-static {v3, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;->$timeOfBirth:Ljava/lang/Long;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/utilities/birthday/BirthdayHelper;->INSTANCE:Lcom/discord/utilities/birthday/BirthdayHelper;

    invoke-virtual {p1}, Lcom/discord/utilities/birthday/BirthdayHelper;->defaultInputAge()J

    move-result-wide v4

    :goto_0
    sget-object p1, Lcom/discord/utilities/birthday/BirthdayHelper;->INSTANCE:Lcom/discord/utilities/birthday/BirthdayHelper;

    invoke-virtual {p1}, Lcom/discord/utilities/birthday/BirthdayHelper;->getMaxDateOfBirth()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v7}, Lf/a/a/j$a;->a(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;JJ)Lf/a/a/j;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1$$special$$inlined$apply$lambda$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1$$special$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1;)V

    iput-object v0, p1, Lf/a/a/j;->d:Lkotlin/jvm/functions/Function1;

    return-void
.end method
