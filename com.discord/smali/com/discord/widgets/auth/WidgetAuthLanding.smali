.class public final Lcom/discord/widgets/auth/WidgetAuthLanding;
.super Lcom/discord/app/AppFragment;
.source "WidgetAuthLanding.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/auth/WidgetAuthLanding$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/auth/WidgetAuthLanding$Companion;

.field private static final VIEW_INDEX_HAS_INVITE:I = 0x1

.field private static final VIEW_INDEX_NO_INVITE:I


# instance fields
.field private final buttonLogin$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buttonRegister$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final inviteInfoView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthLanding;

    const-string v3, "buttonLogin"

    const-string v4, "getButtonLogin()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/auth/WidgetAuthLanding;

    const-string v6, "buttonRegister"

    const-string v7, "getButtonRegister()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/auth/WidgetAuthLanding;

    const-string v6, "flipper"

    const-string v7, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/auth/WidgetAuthLanding;

    const-string v6, "inviteInfoView"

    const-string v7, "getInviteInfoView()Lcom/discord/widgets/auth/AuthInviteInfoView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/auth/WidgetAuthLanding;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLanding$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/auth/WidgetAuthLanding$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/auth/WidgetAuthLanding;->Companion:Lcom/discord/widgets/auth/WidgetAuthLanding$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a00c3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->buttonLogin$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00c4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->buttonRegister$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00c5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00c2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->inviteInfoView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/auth/WidgetAuthLanding;Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthLanding;->configureUI(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;)V

    return-void
.end method

.method private final configureRegisterButton(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_HOME_CONFIG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/home/HomeConfig;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/home/HomeConfig;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/home/HomeConfig;->getAgeGated()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/discord/widgets/auth/WidgetAuthAgeGated;->Companion:Lcom/discord/widgets/auth/WidgetAuthAgeGated$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/discord/widgets/auth/WidgetAuthAgeGated$Companion;->start(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getButtonRegister()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/auth/WidgetAuthLanding$configureRegisterButton$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/auth/WidgetAuthLanding$configureRegisterButton$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getButtonRegister()Landroid/view/View;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/auth/WidgetAuthLanding$configureRegisterButton$2;->INSTANCE:Lcom/discord/widgets/auth/WidgetAuthLanding$configureRegisterButton$2;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;)V
    .locals 2

    instance-of v0, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    instance-of v0, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getInviteInfoView()Lcom/discord/widgets/auth/AuthInviteInfoView;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;

    invoke-virtual {v1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;->getInvite()Lcom/discord/models/domain/ModelInvite;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/auth/AuthInviteInfoView;->configureInvite(Lcom/discord/models/domain/ModelInvite;)V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getInviteInfoView()Lcom/discord/widgets/auth/AuthInviteInfoView;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;

    invoke-virtual {v1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;->getGuildTemplate()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/auth/AuthInviteInfoView;->configureGuildTemplate(Lcom/discord/models/domain/ModelGuildTemplate;)V

    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;->getAgeGateError()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthLanding;->configureRegisterButton(Ljava/lang/String;)V

    return-void
.end method

.method private final getButtonLogin()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->buttonLogin$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/auth/WidgetAuthLanding;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getButtonRegister()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->buttonRegister$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/auth/WidgetAuthLanding;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/auth/WidgetAuthLanding;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getInviteInfoView()Lcom/discord/widgets/auth/AuthInviteInfoView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->inviteInfoView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/auth/WidgetAuthLanding;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/auth/AuthInviteInfoView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d017b

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 9

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;

    invoke-direct {v0}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(this, \u2026ingViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLanding;->viewModel:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    invoke-virtual {p1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/auth/WidgetAuthLanding;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/auth/WidgetAuthLanding$onViewBound$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/auth/WidgetAuthLanding$onViewBound$1;-><init>(Lcom/discord/widgets/auth/WidgetAuthLanding;)V

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    const p1, 0x7f040048

    invoke-static {p0, p1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result p1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarColor(Landroidx/fragment/app/Fragment;IZ)V

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getButtonLogin()Landroid/view/View;

    move-result-object p1

    sget-object v1, Lcom/discord/widgets/auth/WidgetAuthLanding$onViewBound$2;->INSTANCE:Lcom/discord/widgets/auth/WidgetAuthLanding$onViewBound$2;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAuthLanding;->getButtonRegister()Landroid/view/View;

    move-result-object p1

    sget-object v1, Lcom/discord/widgets/auth/WidgetAuthLanding$onViewBound$3;->INSTANCE:Lcom/discord/widgets/auth/WidgetAuthLanding$onViewBound$3;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt p1, v1, :cond_0

    sget-object p1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-static {p1, v1, v0}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    sget-object p1, Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink;->Companion:Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink$Companion;->disable()V

    :cond_1
    return-void
.end method
