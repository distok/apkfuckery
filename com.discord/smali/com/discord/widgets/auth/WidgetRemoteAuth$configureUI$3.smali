.class public final Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$3;
.super Ljava/lang/Object;
.source "WidgetRemoteAuth.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetRemoteAuth;->configureUI(LWidgetRemoteAuthViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetRemoteAuth;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$3;->this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 13

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetRemoteAuth$configureUI$3;->this$0:Lcom/discord/widgets/auth/WidgetRemoteAuth;

    invoke-static {p1}, Lcom/discord/widgets/auth/WidgetRemoteAuth;->access$getViewModel$p(Lcom/discord/widgets/auth/WidgetRemoteAuth;)LWidgetRemoteAuthViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LWidgetRemoteAuthViewModel$ViewState$b;

    if-eqz v0, :cond_0

    iget-object v0, p1, LWidgetRemoteAuthViewModel;->e:Lcom/discord/utilities/rest/RestAPI;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type WidgetRemoteAuthViewModel.ViewState.Loaded"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v2, LWidgetRemoteAuthViewModel$ViewState$b;

    iget-object v2, v2, LWidgetRemoteAuthViewModel$ViewState$b;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI;->postRemoteAuthFinish(Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object v4

    const-class v5, LWidgetRemoteAuthViewModel;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lq;

    invoke-direct {v10, p1}, Lq;-><init>(LWidgetRemoteAuthViewModel;)V

    const/4 v9, 0x0

    new-instance v8, Lr;

    invoke-direct {v8, p1}, Lr;-><init>(LWidgetRemoteAuthViewModel;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method
