.class public final Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$1;
.super Lx/m/c/k;
.source "WidgetAuthLandingViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreAnalytics;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$1;->invoke(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;->handleStoreState(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;)V

    return-void
.end method
