.class public final Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;
.super Ljava/lang/Object;
.source "WidgetAuthLandingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final ageGateError:Ljava/lang/String;

.field private final guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

.field private final invite:Lcom/discord/models/domain/ModelInvite;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)V
    .locals 1

    const-string v0, "guildTemplateState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    iput-object p3, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->copy(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelInvite;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;
    .locals 1

    const-string v0, "guildTemplateState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    iget-object v1, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    iget-object v1, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAgeGateError()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildTemplateState()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    return-object v0
.end method

.method public final getInvite()Lcom/discord/models/domain/ModelInvite;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelInvite;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StoreState(invite="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->invite:Lcom/discord/models/domain/ModelInvite;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildTemplateState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->guildTemplateState:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ageGateError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->ageGateError:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
