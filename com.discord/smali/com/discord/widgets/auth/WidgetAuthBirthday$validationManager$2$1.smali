.class public final Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2$1;
.super Ljava/lang/Object;
.source "WidgetAuthBirthday.kt"

# interfaces
.implements Lcom/discord/utilities/view/validators/InputValidator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;->invoke()Lcom/discord/utilities/view/validators/ValidationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/discord/utilities/view/validators/InputValidator<",
        "Landroid/widget/CheckBox;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getErrorMessage(Landroid/widget/CheckBox;)Ljava/lang/CharSequence;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;

    iget-object v0, v0, Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthBirthday;

    invoke-static {v0}, Lcom/discord/widgets/auth/WidgetAuthBirthday;->access$isConsentRequired$p(Lcom/discord/widgets/auth/WidgetAuthBirthday;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;

    iget-object v0, v0, Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthBirthday;

    invoke-static {v0}, Lcom/discord/widgets/auth/WidgetAuthBirthday;->access$isConsentRequired$p(Lcom/discord/widgets/auth/WidgetAuthBirthday;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f121817

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic getErrorMessage(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthBirthday$validationManager$2$1;->getErrorMessage(Landroid/widget/CheckBox;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
