.class public final Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;
.super Ljava/lang/Object;
.source "WidgetAuthMfa.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthMfa;->showBackupCodesDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $dialog:Landroidx/appcompat/app/AlertDialog;

.field public final synthetic $editText:Lcom/google/android/material/textfield/TextInputLayout;

.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAuthMfa;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAuthMfa;Landroidx/appcompat/app/AlertDialog;Lcom/google/android/material/textfield/TextInputLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthMfa;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;->$dialog:Landroidx/appcompat/app/AlertDialog;

    iput-object p3, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;->$editText:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthMfa;

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;->$dialog:Landroidx/appcompat/app/AlertDialog;

    const-string v1, "dialog"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$showBackupCodesDialog$1;->$editText:Lcom/google/android/material/textfield/TextInputLayout;

    const-string v2, "editText"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/auth/WidgetAuthMfa;->access$evaluateBackupCode(Lcom/discord/widgets/auth/WidgetAuthMfa;Landroidx/appcompat/app/AlertDialog;Ljava/lang/String;)V

    return-void
.end method
