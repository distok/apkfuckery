.class public final Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;
.super Ljava/lang/Object;
.source "WidgetAgeVerifyViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private dateOfBirth:Ljava/lang/Long;

.field private final displayedChild:I

.field private final errorStringId:Ljava/lang/Integer;

.field private final isSubmitting:Z

.field private final safeChannel:Lcom/discord/models/domain/ModelChannel;

.field private final shouldClose:Z

.field private underageMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    iput p3, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    iput-object p4, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    iput-boolean p7, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move p3, p1

    move-object p4, p9

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy(ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;Z)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    return v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    return v0
.end method

.method public final component4()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    return v0
.end method

.method public final copy(ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;Z)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;
    .locals 9

    new-instance v8, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-object v0, v8

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;-><init>(ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;Z)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    iget-boolean v1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    iget v1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    iget-boolean p1, p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDateOfBirth()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    return-object v0
.end method

.method public final getDisplayedChild()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    return v0
.end method

.method public final getErrorStringId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSafeChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getShouldClose()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    return v0
.end method

.method public final getUnderageMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v3

    :cond_4
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    if-eqz v2, :cond_5

    goto :goto_3

    :cond_5
    move v1, v2

    :goto_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isSubmitting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    return v0
.end method

.method public final setDateOfBirth(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    return-void
.end method

.method public final setUnderageMessage(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(isSubmitting="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->isSubmitting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", errorStringId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->errorStringId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displayedChild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->displayedChild:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", dateOfBirth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->dateOfBirth:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", underageMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->underageMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", safeChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->safeChannel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldClose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->shouldClose:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
