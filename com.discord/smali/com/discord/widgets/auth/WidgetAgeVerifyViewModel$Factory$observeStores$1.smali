.class public final Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;
.super Ljava/lang/Object;
.source "WidgetAgeVerifyViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;

    invoke-direct {v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    if-nez p1, :cond_1

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/channel/ChannelUtils;->getDefaultChannel(J)Lrx/Observable;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1$1;->INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    :goto_1
    return-object p1
.end method
