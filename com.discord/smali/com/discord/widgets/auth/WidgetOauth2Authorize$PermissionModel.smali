.class public final Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;
.super Ljava/lang/Object;
.source "WidgetOauth2Authorize.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetOauth2Authorize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PermissionModel"
.end annotation


# instance fields
.field private final fakeText:Ljava/lang/Integer;

.field private final scope:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;->scope:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;->fakeText:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public final getFakeText()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;->fakeText:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getScope()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;->scope:Ljava/lang/String;

    return-object v0
.end method
