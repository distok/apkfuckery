.class public final Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;
.super Lf/a/b/l0;
.source "WidgetAuthLandingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;,
        Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;,
        Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeAuthentication:Lcom/discord/stores/StoreAuthentication;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreAnalytics;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreAuthentication;",
            "Lcom/discord/stores/StoreAnalytics;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAuthentication"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAnalytics"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;->storeObservable:Lrx/Observable;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;->storeAuthentication:Lcom/discord/stores/StoreAuthentication;

    iput-object p3, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    const/4 v0, 0x2

    invoke-static {p1, p0, v1, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    new-instance v8, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$1;-><init>(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreAuthentication;->requestConsentRequired()V

    invoke-virtual {p3}, Lcom/discord/stores/StoreAnalytics;->appLandingViewed()V

    return-void
.end method


# virtual methods
.method public final handleStoreState(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;)V
    .locals 2
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->getInvite()Lcom/discord/models/domain/ModelInvite;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->getGuildTemplateState()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;->getAgeGateError()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;

    invoke-direct {v1, v0, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;-><init>(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    instance-of v0, v1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;

    check-cast v1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;->getGuildTemplate()Lcom/discord/models/domain/ModelGuildTemplate;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;-><init>(Lcom/discord/models/domain/ModelGuildTemplate;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;

    invoke-direct {v0, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
