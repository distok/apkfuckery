.class public final Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetAgeVerifyViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final storeAuth:Lcom/discord/stores/StoreAuthentication;

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;-><init>(Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreChannelsSelected;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreChannelsSelected;)V
    .locals 1

    const-string/jumbo v0, "storeAuth"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->storeAuth:Lcom/discord/stores/StoreAuthentication;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreChannelsSelected;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object p2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;-><init>(Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreChannelsSelected;)V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v2

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->storeAuth:Lcom/discord/stores/StoreAuthentication;

    iget-object v5, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {p0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v6

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;-><init>(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreChannelsSelected;Lrx/Observable;)V

    return-object p1
.end method

.method public final observeStores()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreAuthentication;->getIsAuthed$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$1;

    invoke-virtual {v0, v3}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;->INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;

    invoke-static {v1, v2, v0, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026hed\n          )\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
