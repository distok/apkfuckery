.class public final Lcom/discord/widgets/auth/WidgetAuthMfa$evaluateCode$2;
.super Ljava/lang/Object;
.source "WidgetAuthMfa.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthMfa;->evaluateCode(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/utilities/error/Error;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAuthMfa;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAuthMfa;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$evaluateCode$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthMfa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/utilities/error/Error;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthMfa$evaluateCode$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthMfa;

    invoke-static {p1}, Lcom/discord/widgets/auth/WidgetAuthMfa;->access$getCodeVerificationView$p(Lcom/discord/widgets/auth/WidgetAuthMfa;)Lcom/discord/views/CodeVerificationView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/views/CodeVerificationView;->b()V

    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->loginAttempt(Z)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/utilities/error/Error;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthMfa$evaluateCode$2;->call(Lcom/discord/utilities/error/Error;)V

    return-void
.end method
