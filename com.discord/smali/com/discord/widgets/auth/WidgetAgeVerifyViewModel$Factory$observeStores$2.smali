.class public final Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;
.super Ljava/lang/Object;
.source "WidgetAgeVerifyViewModel.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;

    invoke-direct {v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;->INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;ZLcom/discord/models/domain/ModelChannel;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;
    .locals 2

    new-instance v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getNsfwAllowed()Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->UNKNOWN:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    :goto_0
    const-string v1, "me?.nsfwAllowed ?: ModelUser.NsfwAllowance.UNKNOWN"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p3, p1, p2}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser$NsfwAllowance;Z)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    check-cast p3, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory$observeStores$2;->call(Lcom/discord/models/domain/ModelUser;ZLcom/discord/models/domain/ModelChannel;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
