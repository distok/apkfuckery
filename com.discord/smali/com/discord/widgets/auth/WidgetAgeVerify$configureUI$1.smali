.class public final Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;
.super Ljava/lang/Object;
.source "WidgetAgeVerify.kt"

# interfaces
.implements Lrx/functions/Func0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAgeVerify;->configureUI(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAgeVerify;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-static {v0}, Lcom/discord/widgets/auth/WidgetAgeVerify;->access$getViewFlipper$p(Lcom/discord/widgets/auth/WidgetAgeVerify;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    const-string v2, "requireContext()"

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-static {v0}, Lcom/discord/widgets/auth/WidgetAgeVerify;->access$getViewModel$p(Lcom/discord/widgets/auth/WidgetAgeVerify;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->backToSafety()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-virtual {v0}, Lcom/discord/app/AppFragment;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/app/AppActivity;->l(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-virtual {v0}, Lcom/discord/app/AppFragment;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/app/AppActivity;->l(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->this$0:Lcom/discord/widgets/auth/WidgetAgeVerify;

    invoke-static {v0}, Lcom/discord/widgets/auth/WidgetAgeVerify;->access$getViewModel$p(Lcom/discord/widgets/auth/WidgetAgeVerify;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->onConfirmBackClicked()V

    :goto_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/auth/WidgetAgeVerify$configureUI$1;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
