.class public final Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;
.super Lcom/discord/widgets/auth/WidgetOauth2Authorize;
.source "WidgetOauth2AuthorizeSamsung.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

.field private static final REQ_CODE_SAMSUNG:I = 0x1553

.field private static final REQ_CODE_SAMSUNG_DISCLAIMER:I = 0x1552


# instance fields
.field private final grantBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private samsungAuthCode:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    const-string v3, "grantBtn"

    const-string v4, "getGrantBtn()Landroid/widget/Button;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;-><init>()V

    const v0, 0x7f0a0726

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->grantBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$authorizeForSamsung(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->authorizeForSamsung(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$checkDisclaimer(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->checkDisclaimer()V

    return-void
.end method

.method public static final synthetic access$getSamsungAuthCode$p(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->samsungAuthCode:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$setSamsungAuthCode$p(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->samsungAuthCode:Ljava/lang/String;

    return-void
.end method

.method private final authorizeForSamsung(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    invoke-virtual {p0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;->getOauth2ViewModel()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->getOauthAuthorize()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->post(Ljava/lang/String;)Lrx/Observable;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p2, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p2

    const/4 v0, 0x2

    invoke-static {p2, p0, v2, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    new-instance v7, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$authorizeForSamsung$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$authorizeForSamsung$1;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;)V

    new-instance v9, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$authorizeForSamsung$2;

    invoke-direct {v9, p0, p1}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$authorizeForSamsung$2;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final checkDisclaimer()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.REQUEST_NEW_THIRD_PARTY_INTEGRATION_WITH_SAMSUNG_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    const-string v2, "97t47j218f"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "progress_theme"

    const-string v2, "dark"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x1552

    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private final getGrantBtn()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->grantBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final startSamsungAccountLink(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    const-string v0, "authServerUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lx/s/u;->take(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xc6b

    if-eq v1, v2, :cond_2

    const/16 v2, 0xcb0

    if-eq v1, v2, :cond_1

    const/16 v2, 0xe9e

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string/jumbo v1, "us"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "https://us.account.samsung.com"

    goto :goto_1

    :cond_1
    const-string v1, "eu"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v1, "cn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "https://account.samsung.cn"

    goto :goto_1

    :cond_3
    :goto_0
    const-string p1, "https://account.samsung.com"

    :goto_1
    sget-object v1, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GET /authorize "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$logI(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;->getOauth2ViewModel()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->getOauthAuthorize()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->getForSamsung(Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0, v3, v0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$startSamsungAccountLink$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$startSamsungAccountLink$1;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;)V

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$startSamsungAccountLink$2;

    invoke-direct {v10, p0, p2}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$startSamsungAccountLink$2;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;Ljava/lang/String;)V

    const/16 v11, 0x14

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public configureUI(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;->configureUI(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->getGrantBtn()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$configureUI$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$configureUI$1;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public createOauthAuthorize(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;
    .locals 15

    const-string v0, "requestUrl"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;->createOauthAuthorize(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->getState()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    invoke-static {v0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$createSAStateId(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v4, v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3f5

    const/4 v14, 0x0

    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    const-string v6, "https://discord.com/api/v6/oauth2/samsung/authorize/callback"

    invoke-static/range {v1 .. v14}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->copy$default(Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object v0

    return-object v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0246

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Oauth onActivityResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$logI(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;)V

    const/16 v1, 0x1552

    const/16 v2, 0x1553

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eq p1, v1, :cond_7

    if-eq p1, v2, :cond_0

    sget-object v0, Lcom/discord/widgets/auth/WidgetAuthCaptcha;->Companion:Lcom/discord/widgets/auth/WidgetAuthCaptcha$Companion;

    new-instance v1, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$onActivityResult$isCaptchaHandled$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$onActivityResult$isCaptchaHandled$1;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;)V

    invoke-virtual {v0, p2, p3, v1}, Lcom/discord/widgets/auth/WidgetAuthCaptcha$Companion;->handleResult(ILandroid/content/Intent;Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-super {p0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_4

    :cond_0
    const/16 p1, 0x1f4

    if-ne p2, p1, :cond_3

    const-string p1, "Connection requires SA service, but something went wrong."

    invoke-static {v0, p1, v5, v4, v5}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->logW$default(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    if-eqz p3, :cond_1

    const-string p1, "com.discord.samsung.intent.extra.ATTEMPT_COUNT"

    invoke-virtual {p3, p1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    :cond_1
    if-ge v3, v4, :cond_2

    const-string p1, "Retrying SA connection.\nBecause sometimes it just doesn\'t bind the first time."

    invoke-static {v0, p1, v5, v4, v5}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->logW$default(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object p1, Lcom/discord/samsung/SamsungConnectActivity;->g:Lcom/discord/samsung/SamsungConnectActivity$a;

    invoke-virtual {p1, p0, v2, v3}, Lcom/discord/samsung/SamsungConnectActivity$a;->a(Landroidx/fragment/app/Fragment;II)V

    :cond_2
    return-void

    :cond_3
    if-eqz p3, :cond_6

    new-instance p1, Lkotlin/Pair;

    const-string p2, "SAMSUNG_REQ_AUTH_PARAM_AUTHCODE"

    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "SAMSUNG_REQ_AUTH_PARAM_AUTH_SERVER_URL"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->samsungAuthCode:Ljava/lang/String;

    const-string p3, ""

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_4
    move-object p1, p3

    :goto_0
    const-string v0, "saUrl ?: \"\""

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_5

    goto :goto_1

    :cond_5
    move-object p2, p3

    :goto_1
    const-string p3, "authCode ?: \"\""

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->startSamsungAccountLink(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    const-string p1, "auth code not sent"

    invoke-static {v0, p1, v5, v4, v5}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->logW$default(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_7
    const/4 p1, -0x1

    if-ne p2, p1, :cond_8

    const-string p1, "disclaimer accepted"

    invoke-static {v0, p1}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$logI(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/samsung/SamsungConnectActivity;->g:Lcom/discord/samsung/SamsungConnectActivity$a;

    invoke-virtual {p1, p0, v2, v3}, Lcom/discord/samsung/SamsungConnectActivity$a;->a(Landroidx/fragment/app/Fragment;II)V

    goto :goto_4

    :cond_8
    if-eqz p3, :cond_9

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_9

    const-string p2, "error_message"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_2

    :cond_9
    move-object p1, v5

    :goto_2
    if-eqz p3, :cond_a

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    if-eqz p2, :cond_a

    const-string p3, "error_code"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_3

    :cond_a
    move-object p2, v5

    :goto_3
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection requires disclaimer acceptance. ["

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "] "

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1, v5, v4, v5}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->logW$default(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    const p1, 0x7f12073c

    const/4 p2, 0x4

    invoke-static {p0, p1, v3, p2}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_b
    :goto_4
    return-void
.end method

.method public final samsungCallbackHandshake(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    instance-of v3, v2, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;

    iget v4, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->label:I

    const/high16 v5, -0x80000000

    and-int v6, v4, v5

    if-eqz v6, :cond_0

    sub-int/2addr v4, v5

    iput v4, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;

    invoke-direct {v3, v0, v2}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v2, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->result:Ljava/lang/Object;

    sget-object v4, Lx/j/g/a;->d:Lx/j/g/a;

    iget v5, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->label:I

    const/4 v6, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-eqz v5, :cond_4

    if-eq v5, v9, :cond_3

    if-eq v5, v8, :cond_2

    if-ne v5, v7, :cond_1

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$6:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$5:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$4:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$3:Ljava/lang/Object;

    check-cast v1, Lb0/y;

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$2:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$1:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$0:Ljava/lang/Object;

    check-cast v1, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    invoke-static {v2}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$5:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v5, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$4:Ljava/lang/Object;

    check-cast v5, Landroid/net/Uri;

    iget-object v8, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$3:Ljava/lang/Object;

    check-cast v8, Lb0/y;

    iget-object v9, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$2:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    iget-object v10, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$1:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    iget-object v11, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$0:Ljava/lang/Object;

    check-cast v11, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    invoke-static {v2}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_3
    iget-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$3:Ljava/lang/Object;

    check-cast v1, Lb0/y;

    iget-object v5, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$2:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    iget-object v9, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$1:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    iget-object v10, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$0:Ljava/lang/Object;

    check-cast v10, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    invoke-static {v2}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    move-object v15, v10

    move-object/from16 v19, v5

    move-object v5, v1

    move-object v1, v9

    move-object v9, v2

    move-object/from16 v2, v19

    goto :goto_1

    :cond_4
    invoke-static {v2}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    new-instance v2, Lb0/y$a;

    invoke-direct {v2}, Lb0/y$a;-><init>()V

    const/4 v5, 0x0

    iput-boolean v5, v2, Lb0/y$a;->h:Z

    new-instance v5, Lb0/y;

    invoke-direct {v5, v2}, Lb0/y;-><init>(Lb0/y$a;)V

    sget-object v2, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    const-string v10, "GET /callback"

    invoke-static {v2, v10}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$logI(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;)V

    iput-object v0, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$0:Ljava/lang/Object;

    iput-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$1:Ljava/lang/Object;

    move-object/from16 v2, p2

    iput-object v2, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$2:Ljava/lang/Object;

    iput-object v5, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$3:Ljava/lang/Object;

    iput v9, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->label:I

    sget-object v9, Ly/a/h0;->b:Ly/a/v;

    new-instance v10, Lf/a/i/a;

    invoke-direct {v10, v1, v5, v6}, Lf/a/i/a;-><init>(Ljava/lang/String;Lb0/y;Lkotlin/coroutines/Continuation;)V

    invoke-static {v9, v10, v3}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v9

    if-ne v9, v4, :cond_5

    return-object v4

    :cond_5
    move-object v15, v0

    :goto_1
    move-object v14, v9

    check-cast v14, Landroid/net/Uri;

    if-eqz v14, :cond_8

    const-string v9, "redirect_uri"

    invoke-virtual {v14, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_8

    const-string v9, "getCallbackUri?.getQuery\u2026m GET/callback\"\n        )"

    invoke-static {v13, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v9, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "POST /callback "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$logI(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/discord/widgets/auth/WidgetOauth2Authorize;->getOauth2ViewModel()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->getOauthAuthorize()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->getState()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iput-object v15, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$0:Ljava/lang/Object;

    iput-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$1:Ljava/lang/Object;

    iput-object v2, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$2:Ljava/lang/Object;

    iput-object v5, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$3:Ljava/lang/Object;

    iput-object v14, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$4:Ljava/lang/Object;

    iput-object v13, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$5:Ljava/lang/Object;

    iput v8, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->label:I

    sget-object v8, Ly/a/h0;->b:Ly/a/v;

    new-instance v12, Lf/a/i/b;

    const/16 v16, 0x0

    move-object v9, v12

    move-object v11, v2

    move-object v7, v12

    move-object v12, v13

    move-object/from16 v17, v13

    move-object v13, v5

    move-object/from16 v18, v14

    move-object/from16 v14, v16

    invoke-direct/range {v9 .. v14}, Lf/a/i/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lb0/y;Lkotlin/coroutines/Continuation;)V

    invoke-static {v8, v7, v3}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v7

    if-ne v7, v4, :cond_6

    return-object v4

    :cond_6
    move-object v10, v1

    move-object v9, v2

    move-object v8, v5

    move-object v2, v7

    move-object v11, v15

    move-object/from16 v1, v17

    move-object/from16 v5, v18

    :goto_2
    check-cast v2, Landroid/net/Uri;

    sget-object v7, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;->Companion:Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "POST /callback success "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;->access$logI(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;Ljava/lang/String;)V

    sget-object v7, Ly/a/h0;->a:Ly/a/v;

    sget-object v7, Ly/a/s1/j;->b:Ly/a/e1;

    new-instance v12, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$2;

    invoke-direct {v12, v11, v6}, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$2;-><init>(Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;Lkotlin/coroutines/Continuation;)V

    iput-object v11, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$0:Ljava/lang/Object;

    iput-object v10, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$1:Ljava/lang/Object;

    iput-object v9, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$2:Ljava/lang/Object;

    iput-object v8, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$3:Ljava/lang/Object;

    iput-object v5, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$4:Ljava/lang/Object;

    iput-object v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$5:Ljava/lang/Object;

    iput-object v2, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->L$6:Ljava/lang/Object;

    const/4 v1, 0x3

    iput v1, v3, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1;->label:I

    invoke-static {v7, v12, v3}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v4, :cond_7

    return-object v4

    :cond_7
    :goto_3
    sget-object v1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v1

    :cond_8
    new-instance v1, Lcom/discord/samsung/SamsungConnect$SamsungCallbackException;

    const-string v2, "no_redirect_uri"

    const-string v3, "no redirect uri returned from GET/callback"

    invoke-direct {v1, v2, v3}, Lcom/discord/samsung/SamsungConnect$SamsungCallbackException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1
.end method
