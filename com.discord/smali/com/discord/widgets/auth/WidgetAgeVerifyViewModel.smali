.class public final Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;
.super Lf/a/b/l0;
.source "WidgetAgeVerifyViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Event;,
        Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;,
        Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Views;,
        Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;,
        Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final eventsSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeAuth:Lcom/discord/stores/StoreAuthentication;

.field private final storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreChannelsSelected;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/utilities/time/Clock;",
            "Lcom/discord/stores/StoreAuthentication;",
            "Lcom/discord/stores/StoreChannelsSelected;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "restAPI"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAuth"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannelsSelected"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeObservable"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;-><init>(ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;Z)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object p2, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p3, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->storeAuth:Lcom/discord/stores/StoreAuthentication;

    iput-object p4, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->storeChannelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p5, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->storeObservable:Lrx/Observable;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->eventsSubject:Lrx/subjects/PublishSubject;

    const/4 p1, 0x0

    const/4 p2, 0x2

    invoke-static {p5, p0, p1, p2, p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;

    new-instance v6, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$1;-><init>(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->handleStoreState(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;)V

    return-void
.end method

.method public static final synthetic access$handleUserUpdateFailure(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;Lcom/discord/utilities/error/Error;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->handleUserUpdateFailure(Lcom/discord/utilities/error/Error;)V

    return-void
.end method

.method public static final synthetic access$handleUserUpdateSuccess(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->handleUserUpdateSuccess()V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;)V
    .locals 11

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;->getAuthed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;->getNsfwAllowed()Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    move-result-object v0

    sget-object v2, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->UNKNOWN:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v8, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    const/4 v8, 0x1

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;->getSafeChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v7

    const/16 v9, 0x1f

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private final handleUserUpdateFailure(Lcom/discord/utilities/error/Error;)V
    .locals 11
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v0

    const-string v2, "error.response"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/utilities/error/Error$Response;->getMessages()Ljava/util/Map;

    move-result-object v0

    const-string v3, "error.response.messages"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "date_of_birth"

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error$Response;->getMessages()Ljava/util/Map;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lx/h/l;->d:Lx/h/l;

    :goto_0
    const-string v0, "error.response.messages\n\u2026IRTH_KEY) { emptyList() }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Ljava/lang/Iterable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const-string v3, "\n"

    invoke-static/range {v2 .. v9}, Lx/h/f;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v8, 0x0

    const/16 v9, 0x6a

    const/4 v10, 0x0

    move-object v6, p1

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->storeAuth:Lcom/discord/stores/StoreAuthentication;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAuthentication;->setAgeGateError(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    const p1, 0x7f1200dc

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7c

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private final handleUserUpdateSuccess()V
    .locals 11
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7e

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->eventsSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Event$Verified;->INSTANCE:Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Event$Verified;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final backToSafety()Z
    .locals 9
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->getSafeChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel$default(Lcom/discord/utilities/channel/ChannelSelector;JJIILjava/lang/Object;)V

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(Lcom/discord/models/domain/ModelChannel;)V

    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final getClock()Lcom/discord/utilities/time/Clock;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->clock:Lcom/discord/utilities/time/Clock;

    return-object v0
.end method

.method public final getStoreObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->storeObservable:Lrx/Observable;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->eventsSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventsSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onConfirmBackClicked()V
    .locals 11
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7b

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final setDateOfBirth(J)V
    .locals 11
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x77

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final submit(Z)V
    .locals 29
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->getDateOfBirth()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sget-object v1, Lcom/discord/utilities/birthday/BirthdayHelper;->INSTANCE:Lcom/discord/utilities/birthday/BirthdayHelper;

    invoke-virtual {v1, v12, v13}, Lcom/discord/utilities/birthday/BirthdayHelper;->getAge(J)I

    move-result v1

    const/16 v3, 0x12

    const/4 v14, 0x1

    const/4 v15, 0x0

    if-ge v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->getDisplayedChild()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7b

    const/4 v11, 0x0

    invoke-static/range {v2 .. v11}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_1
    new-instance v1, Lcom/discord/restapi/RestAPIParams$UserInfo;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string/jumbo v4, "yyyy-MM-dd"

    invoke-static {v3, v4}, Lcom/discord/utilities/time/TimeUtils;->toUTCDateTime(Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x1ff

    const/16 v28, 0x0

    move-object/from16 v16, v1

    invoke-direct/range {v16 .. v28}, Lcom/discord/restapi/RestAPIParams$UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7c

    const/4 v11, 0x0

    invoke-static/range {v2 .. v11}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;->copy$default(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;ZLjava/lang/Integer;ILjava/lang/Long;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;

    move-result-object v2

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    sget-object v2, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    if-eqz p1, :cond_2

    const-string v3, "NSFW Channel"

    goto :goto_1

    :cond_2
    const-string v3, "Public Server"

    :goto_1
    invoke-virtual {v2, v12, v13, v3}, Lcom/discord/utilities/analytics/AnalyticsTracker;->ageGateSubmitted(JLjava/lang/String;)V

    iget-object v2, v0, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v2, v1}, Lcom/discord/utilities/rest/RestAPI;->patchUser(Lcom/discord/restapi/RestAPIParams$UserInfo;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v15, v14, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const/4 v3, 0x2

    invoke-static {v1, v0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$submit$1;

    invoke-direct {v10, v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$submit$1;-><init>(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;)V

    const/4 v9, 0x0

    new-instance v8, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$submit$2;

    invoke-direct {v8, v0}, Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$submit$2;-><init>(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_3
    return-void
.end method
