.class public final Lcom/discord/widgets/auth/WidgetAuthLogin$validationManager$2;
.super Lx/m/c/k;
.source "WidgetAuthLogin.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthLogin;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/utilities/view/validators/ValidationManager;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAuthLogin;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAuthLogin;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLogin$validationManager$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthLogin;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/utilities/view/validators/ValidationManager;
    .locals 9

    new-instance v0, Lcom/discord/utilities/view/validators/ValidationManager;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/discord/utilities/view/validators/Input;

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    iget-object v3, p0, Lcom/discord/widgets/auth/WidgetAuthLogin$validationManager$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthLogin;

    invoke-static {v3}, Lcom/discord/widgets/auth/WidgetAuthLogin;->access$getLoginWrap$p(Lcom/discord/widgets/auth/WidgetAuthLogin;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    sget-object v6, Lcom/discord/utilities/view/validators/BasicTextInputValidator;->Companion:Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;

    const v7, 0x7f120f69

    invoke-virtual {v6, v7}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v5, v8

    const-string v7, "login"

    invoke-direct {v2, v7, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    iget-object v3, p0, Lcom/discord/widgets/auth/WidgetAuthLogin$validationManager$2;->this$0:Lcom/discord/widgets/auth/WidgetAuthLogin;

    invoke-static {v3}, Lcom/discord/widgets/auth/WidgetAuthLogin;->access$getPasswordWrap$p(Lcom/discord/widgets/auth/WidgetAuthLogin;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    const v7, 0x7f121256

    invoke-virtual {v6, v7}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v6

    aput-object v6, v5, v8

    const-string v6, "password"

    invoke-direct {v2, v6, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/discord/utilities/view/validators/ValidationManager;-><init>([Lcom/discord/utilities/view/validators/Input;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/auth/WidgetAuthLogin$validationManager$2;->invoke()Lcom/discord/utilities/view/validators/ValidationManager;

    move-result-object v0

    return-object v0
.end method
