.class public final Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "WidgetOauth2Authorize.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetOauth2Authorize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OAuth2ViewModel"
.end annotation


# instance fields
.field public oauthAuthorize:Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

.field private oauthGetResponse:Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final getOauthAuthorize()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->oauthAuthorize:Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "oauthAuthorize"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getOauthGetResponse()Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->oauthGetResponse:Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;

    return-object v0
.end method

.method public final setOauthAuthorize(Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->oauthAuthorize:Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    return-void
.end method

.method public final setOauthGetResponse(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;->oauthGetResponse:Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;

    return-void
.end method
