.class public final Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "WidgetAuthLandingViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;->observeStoreState(Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreGuildTemplates;Lcom/discord/stores/StoreAuthentication;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/String;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $storeGuildTemplates:Lcom/discord/stores/StoreGuildTemplates;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildTemplates;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$1;->$storeGuildTemplates:Lcom/discord/stores/StoreGuildTemplates;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$1;->call(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$1;->$storeGuildTemplates:Lcom/discord/stores/StoreGuildTemplates;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildTemplates;->observeGuildTemplate(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
