.class public final Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;
.super Ljava/lang/Object;
.source "WidgetAuthLandingViewModel.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;->observeStoreState(Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreGuildTemplates;Lcom/discord/stores/StoreAuthentication;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelInvite;",
        "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
        "Ljava/lang/String;",
        "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;

    invoke-direct {v0}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;
    .locals 2

    new-instance v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    const-string v1, "guildTemplate"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelInvite;

    check-cast p2, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;->call(Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
