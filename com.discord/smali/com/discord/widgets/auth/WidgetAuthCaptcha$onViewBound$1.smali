.class public final Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1;
.super Ljava/lang/Object;
.source "WidgetAuthCaptcha.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/auth/WidgetAuthCaptcha;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/auth/WidgetAuthCaptcha;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/auth/WidgetAuthCaptcha;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthCaptcha;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 10

    sget-object p1, Lcom/discord/utilities/captcha/CaptchaHelper;->INSTANCE:Lcom/discord/utilities/captcha/CaptchaHelper;

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthCaptcha;

    invoke-virtual {v0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/captcha/CaptchaHelper;->tryShowCaptcha(Landroid/app/Activity;)Lrx/Observable;

    move-result-object v1

    iget-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1;->this$0:Lcom/discord/widgets/auth/WidgetAuthCaptcha;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v7, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1$1;-><init>(Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1;)V

    new-instance v5, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1$2;

    invoke-direct {v5, p0}, Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1$2;-><init>(Lcom/discord/widgets/auth/WidgetAuthCaptcha$onViewBound$1;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x16

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
