.class public final Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;
.super Ljava/lang/Object;
.source "WidgetOauth2Authorize.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetOauth2Authorize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$createOauthAuthorize(Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;->createOauthAuthorize(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleError(Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;->handleError(Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final createLaunchIntent(Landroid/net/Uri;Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "REQ_URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->getInternalReferrer()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->isUnsupported()Z

    move-result p1

    if-nez p1, :cond_1

    const p1, 0x10008000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_1
    return-object v0
.end method

.method private final createOauthAuthorize(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;
    .locals 17

    move-object/from16 v0, p1

    sget-object v1, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion$createOauthAuthorize$1;->INSTANCE:Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion$createOauthAuthorize$1;

    new-instance v16, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    const-string v2, "client_id"

    invoke-virtual {v1, v0, v2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion$createOauthAuthorize$1;->invoke(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v2, "redirect_uri"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "response_type"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v2, "state"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "scope"

    invoke-virtual {v1, v0, v2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion$createOauthAuthorize$1;->invoke(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "permissions"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v1, "code_challenge"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "code_challenge_method"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v1, "internal_referrer"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const/4 v8, 0x0

    const/16 v14, 0x10

    const/4 v15, 0x0

    move-object/from16 v2, v16

    invoke-direct/range {v2 .. v15}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v16
.end method

.method private final handleError(Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Exception;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f1211e8

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026missing_param, e.message)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-virtual {v2, v1, p2}, Lcom/discord/app/AppLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p2, 0x0

    const/16 v2, 0x8

    invoke-static {p1, v1, v0, p2, v2}, Lf/a/b/p;->j(Landroid/content/Context;Ljava/lang/CharSequence;ILcom/discord/utilities/view/ToastManager;I)V

    invoke-interface {p3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method public static synthetic handleError$default(Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion$handleError$1;->INSTANCE:Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion$handleError$1;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;->handleError(Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public final getNoticeName(J)Ljava/lang/String;
    .locals 1

    const-string v0, "OAUTH_REQUEST:"

    invoke-static {v0, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final launch(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestUri"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0, p2}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;->createOauthAuthorize(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;->handleError$default(Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;->getClientId()J

    move-result-wide v1

    sget-object v3, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v3, v1, v2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->oauth2AuthorizedViewed(J)V

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "591317049637339146"

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1c

    if-lt v1, v4, :cond_0

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v4, "samsung"

    invoke-static {v1, v4, v2}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_1

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_2

    const-class v1, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    goto :goto_3

    :cond_2
    const-class v1, Lcom/discord/widgets/auth/WidgetOauth2Authorize;

    :goto_3
    invoke-direct {p0, p2, v0}, Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;->createLaunchIntent(Landroid/net/Uri;Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;)Landroid/content/Intent;

    move-result-object p2

    invoke-static {p1, v1, p2}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    :cond_3
    return-void
.end method
