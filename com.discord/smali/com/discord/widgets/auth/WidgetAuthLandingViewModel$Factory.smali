.class public final Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetAuthLandingViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final observeStoreState(Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreGuildTemplates;Lcom/discord/stores/StoreAuthentication;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreInviteSettings;",
            "Lcom/discord/stores/StoreGuildTemplates;",
            "Lcom/discord/stores/StoreAuthentication;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/stores/StoreInviteSettings;->getInvite()Lrx/Observable;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/stores/StoreGuildTemplates;->getDynamicLinkGuildTemplateCode()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$1;

    invoke-direct {v1, p2}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/stores/StoreGuildTemplates;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    invoke-virtual {p3}, Lcom/discord/stores/StoreAuthentication;->getAgeGateError()Lrx/Observable;

    move-result-object p3

    sget-object v0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory$observeStoreState$2;

    invoke-static {p1, p2, p3, v0}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026teError\n        )\n      }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getInviteSettings()Lcom/discord/stores/StoreInviteSettings;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildTemplates()Lcom/discord/stores/StoreGuildTemplates;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Factory;->observeStoreState(Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreGuildTemplates;Lcom/discord/stores/StoreAuthentication;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    invoke-direct {p1, v1, v2, v0}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreAnalytics;)V

    return-object p1
.end method
