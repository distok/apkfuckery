.class public abstract Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;
.super Ljava/lang/Object;
.source "WidgetAuthLandingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;,
        Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;,
        Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;
    }
.end annotation


# instance fields
.field private final ageGateError:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;->ageGateError:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getAgeGateError()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;->ageGateError:Ljava/lang/String;

    return-object v0
.end method
