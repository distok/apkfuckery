.class public final Lcom/discord/restapi/RestAPIParams;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/RestAPIParams$AuthRegister;,
        Lcom/discord/restapi/RestAPIParams$AuthLogin;,
        Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;,
        Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;,
        Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;,
        Lcom/discord/restapi/RestAPIParams$Channel;,
        Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;,
        Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;,
        Lcom/discord/restapi/RestAPIParams$ChannelPosition;,
        Lcom/discord/restapi/RestAPIParams$Consents;,
        Lcom/discord/restapi/RestAPIParams$CreateChannel;,
        Lcom/discord/restapi/RestAPIParams$CreateGuild;,
        Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;,
        Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;,
        Lcom/discord/restapi/RestAPIParams$DeleteGuild;,
        Lcom/discord/restapi/RestAPIParams$EmptyBody;,
        Lcom/discord/restapi/RestAPIParams$EnableIntegration;,
        Lcom/discord/restapi/RestAPIParams$ForgotPassword;,
        Lcom/discord/restapi/RestAPIParams$GuildIntegration;,
        Lcom/discord/restapi/RestAPIParams$GuildMember;,
        Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;,
        Lcom/discord/restapi/RestAPIParams$GuildMFA;,
        Lcom/discord/restapi/RestAPIParams$GroupDM;,
        Lcom/discord/restapi/RestAPIParams$Invite;,
        Lcom/discord/restapi/RestAPIParams$MFALogin;,
        Lcom/discord/restapi/RestAPIParams$Message;,
        Lcom/discord/restapi/RestAPIParams$Nick;,
        Lcom/discord/restapi/RestAPIParams$Phone;,
        Lcom/discord/restapi/RestAPIParams$Ring;,
        Lcom/discord/restapi/RestAPIParams$Thumbnail;,
        Lcom/discord/restapi/RestAPIParams$Role;,
        Lcom/discord/restapi/RestAPIParams$UpdateGuild;,
        Lcom/discord/restapi/RestAPIParams$CommunityGating;,
        Lcom/discord/restapi/RestAPIParams$BanGuildMember;,
        Lcom/discord/restapi/RestAPIParams$PruneGuild;,
        Lcom/discord/restapi/RestAPIParams$UserAgreements;,
        Lcom/discord/restapi/RestAPIParams$UserDevices;,
        Lcom/discord/restapi/RestAPIParams$UserGuildSettings;,
        Lcom/discord/restapi/RestAPIParams$UserInfo;,
        Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;,
        Lcom/discord/restapi/RestAPIParams$UserSettings;,
        Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;,
        Lcom/discord/restapi/RestAPIParams$UserRelationship;,
        Lcom/discord/restapi/RestAPIParams$Science;,
        Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;,
        Lcom/discord/restapi/RestAPIParams$VanityUrl;,
        Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;,
        Lcom/discord/restapi/RestAPIParams$VerificationCode;,
        Lcom/discord/restapi/RestAPIParams$DeletePhone;,
        Lcom/discord/restapi/RestAPIParams$CaptchaCode;,
        Lcom/discord/restapi/RestAPIParams$ConnectedAccount;,
        Lcom/discord/restapi/RestAPIParams$ConnectionState;,
        Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;,
        Lcom/discord/restapi/RestAPIParams$AuthCode;,
        Lcom/discord/restapi/RestAPIParams$PostGuildEmoji;,
        Lcom/discord/restapi/RestAPIParams$PatchGuildEmoji;,
        Lcom/discord/restapi/RestAPIParams$EnableMFA;,
        Lcom/discord/restapi/RestAPIParams$UpdateSubscription;,
        Lcom/discord/restapi/RestAPIParams$DisableAccount;,
        Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;,
        Lcom/discord/restapi/RestAPIParams$OAuth2Authorize;,
        Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;,
        Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;,
        Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;,
        Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;,
        Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;,
        Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;,
        Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;,
        Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
