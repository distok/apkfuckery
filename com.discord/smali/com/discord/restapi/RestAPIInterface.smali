.class public interface abstract Lcom/discord/restapi/RestAPIInterface;
.super Ljava/lang/Object;
.source "RestAPIInterface.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/RestAPIInterface$Dynamic;,
        Lcom/discord/restapi/RestAPIInterface$RtcLatency;,
        Lcom/discord/restapi/RestAPIInterface$Files;,
        Lcom/discord/restapi/RestAPIInterface$DefaultImpls;,
        Lcom/discord/restapi/RestAPIInterface$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/restapi/RestAPIInterface$Companion;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/discord/restapi/RestAPIInterface$Companion;->$$INSTANCE:Lcom/discord/restapi/RestAPIInterface$Companion;

    sput-object v0, Lcom/discord/restapi/RestAPIInterface;->Companion:Lcom/discord/restapi/RestAPIInterface$Companion;

    return-void
.end method


# virtual methods
.method public abstract acceptGift(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "entitlements/gift-codes/{code}/redeem"
    .end annotation
.end method

.method public abstract ackGuild(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/ack"
    .end annotation
.end method

.method public abstract addChannelPin(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/pins/{messageId}"
    .end annotation
.end method

.method public abstract addChannelRecipient(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "recipientId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/recipients/{recipientId}"
    .end annotation
.end method

.method public abstract addReaction(JJLjava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "reaction"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/messages/{messageId}/reactions/{reaction}/@me"
    .end annotation
.end method

.method public abstract addRelationship(JLcom/discord/restapi/RestAPIParams$UserRelationship;Ljava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UserRelationship;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UserRelationship;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "users/@me/relationships/{userId}"
    .end annotation
.end method

.method public abstract authorizeConnection(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUrl;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "connections/{connection}/authorize"
    .end annotation
.end method

.method public abstract banGuildMember(JJLcom/discord/restapi/RestAPIParams$BanGuildMember;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$BanGuildMember;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$BanGuildMember;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/bans/{userId}"
    .end annotation
.end method

.method public abstract batchUpdateRole(JLjava/util/List;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$Role;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/roles"
    .end annotation
.end method

.method public abstract call(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelCall$Ringable;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/call"
    .end annotation
.end method

.method public abstract cancelSubscriptionSlot(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "subscriptionSlotId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/guilds/premium/subscription-slots/{subscriptionSlotId}/cancel"
    .end annotation
.end method

.method public abstract changeGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMember;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$GuildMember;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$GuildMember;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/members/{userId}"
    .end annotation
.end method

.method public abstract changeGuildNickname(JLcom/discord/restapi/RestAPIParams$Nick;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Nick;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Nick;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/members/@me/nick"
    .end annotation
.end method

.method public abstract claimSku(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "skuId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "store/skus/{skuId}/purchase"
    .end annotation
.end method

.method public abstract confirmCommunityGating(JLcom/discord/restapi/RestAPIParams$CommunityGating;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$CommunityGating;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$CommunityGating;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/requests/@me"
    .end annotation
.end method

.method public abstract convertDMToGroup(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "recipientId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/recipients/{recipientId}"
    .end annotation
.end method

.method public abstract createChannelFollower(JLcom/discord/restapi/RestAPIParams$ChannelFollowerPost;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/followers"
    .end annotation
.end method

.method public abstract createGuild(Lcom/discord/restapi/RestAPIParams$CreateGuild;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$CreateGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$CreateGuild;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds"
    .end annotation
.end method

.method public abstract createGuildChannel(JLcom/discord/restapi/RestAPIParams$CreateGuildChannel;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/channels"
    .end annotation
.end method

.method public abstract createGuildFromTemplate(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "guildTemplateCode"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/templates/{guildTemplateCode}"
    .end annotation
.end method

.method public abstract createPurchaseMetadata(Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "google-play/purchase-metadata"
    .end annotation
.end method

.method public abstract createRole(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/roles"
    .end annotation
.end method

.method public abstract crosspostMessage(JLjava/lang/Long;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages/{messageId}/crosspost"
    .end annotation
.end method

.method public abstract deleteAccount(Lcom/discord/restapi/RestAPIParams$DisableAccount;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$DisableAccount;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DisableAccount;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/delete"
    .end annotation
.end method

.method public abstract deleteChannel(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}"
    .end annotation
.end method

.method public abstract deleteChannelPin(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/pins/{messageId}"
    .end annotation
.end method

.method public abstract deleteConnection(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connectionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/connections/{connection}/{connectionId}"
    .end annotation
.end method

.method public abstract deleteGuild(JLcom/discord/restapi/RestAPIParams$DeleteGuild;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$DeleteGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$DeleteGuild;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/delete"
    .end annotation
.end method

.method public abstract deleteGuildEmoji(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "emojiId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/emojis/{emojiId}"
    .end annotation
.end method

.method public abstract deleteGuildIntegration(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/integrations/{integrationId}"
    .end annotation
.end method

.method public abstract deleteMessage(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channel_id"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "message_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channel_id}/messages/{message_id}"
    .end annotation
.end method

.method public abstract deleteOAuthToken(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "oauthId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "oauth2/tokens/{oauthId}"
    .end annotation
.end method

.method public abstract deletePaymentSource(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "paymentSourceId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/billing/payment-sources/{paymentSourceId}"
    .end annotation
.end method

.method public abstract deletePermissionOverwrites(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "targetId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/permissions/{targetId}"
    .end annotation
.end method

.method public abstract deleteRole(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "roleId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/roles/{roleId}"
    .end annotation
.end method

.method public abstract deleteSubscription(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "subscriptionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/billing/subscriptions/{subscriptionId}"
    .end annotation
.end method

.method public abstract disableAccount(Lcom/discord/restapi/RestAPIParams$DisableAccount;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$DisableAccount;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DisableAccount;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/disable"
    .end annotation
.end method

.method public abstract disableMFA(Lcom/discord/restapi/RestAPIParams$AuthCode;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$AuthCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$AuthCode;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/totp/disable"
    .end annotation
.end method

.method public abstract disableMfaSMS(Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/sms/disable"
    .end annotation
.end method

.method public abstract disconnectGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/members/{userId}"
    .end annotation
.end method

.method public abstract downgradeSubscription(Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "google-play/downgrade-subscription"
    .end annotation
.end method

.method public abstract editChannel(JLcom/discord/restapi/RestAPIParams$Channel;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Channel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Channel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "channels/{channelId}"
    .end annotation
.end method

.method public abstract editGroupDM(JLcom/discord/restapi/RestAPIParams$GroupDM;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$GroupDM;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$GroupDM;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "channels/{channelId}"
    .end annotation
.end method

.method public abstract editMessage(JJLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channel_id"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "message_id"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$Message;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "channels/{channel_id}/messages/{message_id}"
    .end annotation
.end method

.method public abstract enableIntegration(JLcom/discord/restapi/RestAPIParams$EnableIntegration;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$EnableIntegration;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$EnableIntegration;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/integrations"
    .end annotation
.end method

.method public abstract enableMFA(Lcom/discord/restapi/RestAPIParams$EnableMFA;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$EnableMFA;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$EnableMFA;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/totp/enable"
    .end annotation
.end method

.method public abstract enableMfaSMS(Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/sms/enable"
    .end annotation
.end method

.method public abstract forgotPassword(Lcom/discord/restapi/RestAPIParams$ForgotPassword;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$ForgotPassword;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$ForgotPassword;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/forgot"
    .end annotation
.end method

.method public abstract generateGiftCode(Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/entitlements/gift-codes"
    .end annotation
.end method

.method public abstract getActivityMetadata(JLjava/lang/String;J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "sessionId"
        .end annotation
    .end param
    .param p4    # J
        .annotation runtime Lf0/i0/s;
            value = "applicationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelActivityMetaData;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/sessions/{sessionId}/activities/{applicationId}/metadata"
    .end annotation
.end method

.method public abstract getApplications(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/t;
            value = "application_ids"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelApplication;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "applications/public"
    .end annotation
.end method

.method public abstract getAuditLogs(JILjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "before"
        .end annotation
    .end param
    .param p5    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "user_id"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "action_type"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelAuditLog;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/audit-logs"
    .end annotation
.end method

.method public abstract getBackupCodes(Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$BackupCodesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelBackupCodes;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/mfa/codes"
    .end annotation
.end method

.method public abstract getBans(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelBan;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/bans"
    .end annotation
.end method

.method public abstract getChannelFollowerStats(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannelFollowerStatsDto;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/follower-stats"
    .end annotation
.end method

.method public abstract getChannelMessages(JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "before"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "after"
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages"
    .end annotation
.end method

.method public abstract getChannelMessagesAround(JIJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .param p4    # J
        .annotation runtime Lf0/i0/t;
            value = "around"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJ)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages"
    .end annotation
.end method

.method public abstract getChannelPins(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/pins"
    .end annotation
.end method

.method public abstract getConnectionAccessToken(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "platformType"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "accountId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelConnectionAccessToken;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/connections/{platformType}/{accountId}/access-token"
    .end annotation
.end method

.method public abstract getConnectionState(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "pinNumber"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelConnectionState;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "connections/{connection}/callback-continuation/{pinNumber}"
    .end annotation
.end method

.method public abstract getConnections()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelConnectedAccount;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/connections"
    .end annotation
.end method

.method public abstract getConsentRequired()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ConsentRequired;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "auth/consent-required"
    .end annotation
.end method

.method public abstract getConsents()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Consents;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/consent"
    .end annotation
.end method

.method public abstract getExperiments()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "experiments"
    .end annotation
.end method

.method public abstract getGifSearchResults(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "q"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "media_format"
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/GifDto;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/search"
    .end annotation
.end method

.method public abstract getGifSuggestedSearchTerms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "q"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/suggest"
    .end annotation
.end method

.method public abstract getGifTrendingSearchTerms(Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/trending-search"
    .end annotation
.end method

.method public abstract getGifts()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/entitlements/gifts"
    .end annotation
.end method

.method public abstract getGuildEmojis(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/emoji/ModelEmojiGuild;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/emojis"
    .end annotation
.end method

.method public abstract getGuildIntegrations(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildIntegration;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/integrations"
    .end annotation
.end method

.method public abstract getGuildInvites(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/invites"
    .end annotation
.end method

.method public abstract getGuildMemberVerificationForm(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/member-verification"
    .end annotation
.end method

.method public abstract getGuildPreview(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildPreview;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/preview"
    .end annotation
.end method

.method public abstract getGuildTemplateCode(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "guildTemplateCode"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildTemplate;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/templates/{guildTemplateCode}"
    .end annotation
.end method

.method public abstract getGuildVoiceRegions()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelVoiceRegion;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "voice/regions"
    .end annotation
.end method

.method public abstract getGuildVoiceRegions(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelVoiceRegion;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/regions"
    .end annotation
.end method

.method public abstract getGuildWelcomeScreen(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuildWelcomeScreen;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/welcome-screen"
    .end annotation
.end method

.method public abstract getHarvestStatus()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Harvest;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/harvest"
    .end annotation
.end method

.method public abstract getInviteCode(Ljava/lang/String;Z)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_counts"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "invites/{code}"
    .end annotation
.end method

.method public abstract getInvoicePreview(Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/billing/invoices/preview"
    .end annotation
.end method

.method public abstract getLibrary()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelLibraryApplication;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/library"
    .end annotation
.end method

.method public abstract getMentions(IZZLjava/lang/Long;Ljava/lang/Long;)Lrx/Observable;
    .param p1    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lf0/i0/t;
            value = "roles"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "everyone"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "guild_id"
        .end annotation
    .end param
    .param p5    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "before"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZ",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/mentions"
    .end annotation
.end method

.method public abstract getMyEntitlements(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "applicationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/applications/{applicationId}/entitlements"
    .end annotation
.end method

.method public abstract getMyStickerPacks()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelUserStickerPack;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/sticker-packs"
    .end annotation
.end method

.method public abstract getOAuthTokens()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelOAuth2Token;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "oauth2/tokens"
    .end annotation
.end method

.method public abstract getOauth2Authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "client_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "state"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "response_type"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "redirect_uri"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "prompt"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "scope"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "permissions"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "oauth2/authorize"
    .end annotation
.end method

.method public abstract getOauth2SamsungAuthorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "client_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "state"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "response_type"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "redirect_uri"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "prompt"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "scope"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "oauth2/samsung/authorize"
    .end annotation
.end method

.method public abstract getPaymentSources()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/PaymentSourceRaw;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/billing/payment-sources"
    .end annotation
.end method

.method public abstract getPruneCount(JI)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lf0/i0/t;
            value = "days"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild$PruneCountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/prune"
    .end annotation
.end method

.method public abstract getReactionUsers(JJLjava/lang/String;Ljava/lang/Integer;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "emoji"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages/{messageId}/reactions/{emoji}"
    .end annotation
.end method

.method public abstract getRelationships()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/relationships"
    .end annotation
.end method

.method public abstract getRelationships(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/relationships"
    .end annotation
.end method

.method public abstract getSpotifyTrack(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/spotify/ModelSpotifyTrack;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "tracks/{id}"
    .end annotation
.end method

.method public abstract getStickerPack(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "packId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "sticker-packs/{packId}"
    .end annotation
.end method

.method public abstract getStickerPacks(II)Lrx/Observable;
    .param p1    # I
        .annotation runtime Lf0/i0/t;
            value = "offset"
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "sticker-packs"
    .end annotation
.end method

.method public abstract getStickerStoreDirectoryLayoutV2(JZLjava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "storeDirectoryLayoutId"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_store_listings"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "sticker-packs/directory-v2/{storeDirectoryLayoutId}"
    .end annotation
.end method

.method public abstract getStreamPreview(Ljava/lang/String;J)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "streamKey"
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lf0/i0/t;
            value = "version"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelApplicationStreamPreview;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "streams/{streamKey}/preview"
    .end annotation
.end method

.method public abstract getSubscriptionSlots()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/guilds/premium/subscription-slots"
    .end annotation
.end method

.method public abstract getSubscriptions()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/billing/subscriptions"
    .end annotation
.end method

.method public abstract getTrendingGifCategories(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "media_format"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/trending"
    .end annotation
.end method

.method public abstract getTrendingGifCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "provider"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "locale"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "media_format"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lf0/i0/t;
            value = "limit"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/GifDto;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "gifs/trending-gifs"
    .end annotation
.end method

.method public abstract getUserAffinities()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserAffinities;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/affinities/users"
    .end annotation
.end method

.method public abstract getUserNote(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserNote;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/notes/{userId}"
    .end annotation
.end method

.method public abstract joinGuild(JZLjava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "lurker"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "session_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/members/@me"
    .end annotation
.end method

.method public abstract joinGuildFromIntegration(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "integrations/{integrationId}/join"
    .end annotation
.end method

.method public abstract kickGuildMember(JJLjava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "reason"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/members/{userId}"
    .end annotation
.end method

.method public abstract leaveGuild(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/guilds/{guildId}"
    .end annotation
.end method

.method public abstract logout(Lcom/discord/restapi/RestAPIParams$UserDevices;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserDevices;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserDevices;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/logout"
    .end annotation
.end method

.method public abstract patchGuildEmoji(JJLcom/discord/restapi/RestAPIParams$PatchGuildEmoji;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "emojiId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$PatchGuildEmoji;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$PatchGuildEmoji;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/emoji/ModelEmojiGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/emojis/{emojiId}"
    .end annotation
.end method

.method public abstract patchUser(Lcom/discord/restapi/RestAPIParams$UserInfo;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserInfo;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserInfo;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me"
    .end annotation
.end method

.method public abstract phoneVerificationsVerify(Lcom/discord/restapi/RestAPIParams$VerificationCode;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$VerificationCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$VerificationCode;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPhoneVerificationToken;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "phone-verifications/verify"
    .end annotation
.end method

.method public abstract postAuthFingerprint(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Fingerprint;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/fingerprint"
    .end annotation
.end method

.method public abstract postAuthHandoff(Ljava/util/Map;)Lrx/Observable;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$TokenHandoff;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/handoff"
    .end annotation
.end method

.method public abstract postAuthLogin(Lcom/discord/restapi/RestAPIParams$AuthLogin;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$AuthLogin;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$AuthLogin;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/auth/ModelLoginResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/login"
    .end annotation
.end method

.method public abstract postAuthRegister(Lcom/discord/restapi/RestAPIParams$AuthRegister;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$AuthRegister;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$AuthRegister;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/register"
    .end annotation
.end method

.method public abstract postAuthVerifyResend(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/verify/resend"
    .end annotation
.end method

.method public abstract postChannelInvite(JLcom/discord/restapi/RestAPIParams$Invite;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Invite;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Invite;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/invites"
    .end annotation
.end method

.method public abstract postChannelMessagesAck(JLjava/lang/Long;Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p4    # Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages/{messageId}/ack"
    .end annotation
.end method

.method public abstract postGuildEmoji(JLcom/discord/restapi/RestAPIParams$PostGuildEmoji;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$PostGuildEmoji;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$PostGuildEmoji;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/emoji/ModelEmojiGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/emojis"
    .end annotation
.end method

.method public abstract postInviteCode(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$EmptyBody;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "invites/{code}"
    .end annotation
.end method

.method public abstract postMFACode(Lcom/discord/restapi/RestAPIParams$MFALogin;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$MFALogin;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$MFALogin;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/auth/ModelLoginResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "auth/mfa/totp"
    .end annotation
.end method

.method public abstract postOauth2Authorize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "client_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "state"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "response_type"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "redirect_uri"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "prompt"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "scope"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "permissions"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "code_challenge"
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "code_challenge_method"
        .end annotation
    .end param
    .param p10    # Ljava/util/Map;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponsePost;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "oauth2/authorize"
    .end annotation
.end method

.method public abstract postRemoteAuthCancel(Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/remote-auth/cancel"
    .end annotation
.end method

.method public abstract postRemoteAuthFinish(Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/remote-auth/finish"
    .end annotation
.end method

.method public abstract postRemoteAuthInitialize(Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelRemoteAuthHandshake;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/remote-auth"
    .end annotation
.end method

.method public abstract postStreamPreview(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$Thumbnail;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "streamKey"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$Thumbnail;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$Thumbnail;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "streams/{streamKey}/preview"
    .end annotation
.end method

.method public abstract pruneMembers(JLcom/discord/restapi/RestAPIParams$PruneGuild;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$PruneGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$PruneGuild;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/prune"
    .end annotation
.end method

.method public abstract removeAllReactions(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/messages/{messageId}/reactions"
    .end annotation
.end method

.method public abstract removeChannelRecipient(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "recipientId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/recipients/{recipientId}"
    .end annotation
.end method

.method public abstract removeReaction(JJLjava/lang/String;J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "reaction"
        .end annotation
    .end param
    .param p6    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "J)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/messages/{messageId}/reactions/{reaction}/{userId}"
    .end annotation
.end method

.method public abstract removeRelationship(JLjava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/relationships/{userId}"
    .end annotation
.end method

.method public abstract removeSelfReaction(JJLjava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "messageId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            encoded = true
            value = "reaction"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "channels/{channelId}/messages/{messageId}/reactions/{reaction}/@me"
    .end annotation
.end method

.method public abstract reorderChannels(JLjava/util/List;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/restapi/RestAPIParams$ChannelPosition;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/channels"
    .end annotation
.end method

.method public abstract requestHarvest()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Harvest;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/harvest"
    .end annotation
.end method

.method public abstract resolveGiftCode(Ljava/lang/String;ZZ)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_application"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lf0/i0/t;
            value = "with_subscription_plan"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "entitlements/gift-codes/{code}"
    .end annotation
.end method

.method public abstract resolveSkuIdGift(JLjava/lang/Long;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/t;
            value = "sku_id"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "subscription_plan_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/@me/entitlements/gift-codes"
    .end annotation
.end method

.method public abstract revokeGiftCode(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "users/@me/entitlements/gift-codes/{code}"
    .end annotation
.end method

.method public abstract revokeInvite(Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "inviteCode"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelInvite;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "invites/{inviteCode}"
    .end annotation
.end method

.method public abstract ring(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Ring;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Ring;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/call/ring"
    .end annotation
.end method

.method public abstract science(Lcom/discord/restapi/RestAPIParams$Science;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$Science;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$Science;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "science"
    .end annotation
.end method

.method public abstract searchChannelMessages(JLjava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "max_id"
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "author_id"
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "mentions"
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "has"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "context_size"
        .end annotation
    .end param
    .param p8    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "content"
        .end annotation
    .end param
    .param p9    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "attempts"
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Lf0/i0/t;
            value = "include_nsfw"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "channels/{channelId}/messages/search"
    .end annotation
.end method

.method public abstract searchGuildMessages(JLjava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "max_id"
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "author_id"
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "mentions"
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "channel_id"
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "has"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lf0/i0/t;
            value = "context_size"
        .end annotation
    .end param
    .param p9    # Ljava/util/List;
        .annotation runtime Lf0/i0/t;
            value = "content"
        .end annotation
    .end param
    .param p10    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/t;
            value = "attempts"
        .end annotation
    .end param
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lf0/i0/t;
            value = "include_nsfw"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelSearchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "guilds/{guildId}/messages/search"
    .end annotation
.end method

.method public abstract sendMessage(JLcom/discord/restapi/PayloadJSON;[Lokhttp3/MultipartBody$Part;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/PayloadJSON;
        .annotation runtime Lf0/i0/q;
            value = "payload_json"
        .end annotation
    .end param
    .param p4    # [Lokhttp3/MultipartBody$Part;
        .annotation runtime Lf0/i0/q;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/PayloadJSON<",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            ">;[",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/l;
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages"
    .end annotation
.end method

.method public abstract sendMessage(JLcom/discord/restapi/RestAPIParams$Message;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Message;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Message;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/messages"
    .end annotation
.end method

.method public abstract sendRelationshipRequest(Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/relationships"
    .end annotation
.end method

.method public abstract setConsents(Lcom/discord/restapi/RestAPIParams$Consents;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$Consents;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$Consents;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/consent"
    .end annotation
.end method

.method public abstract setMfaLevel(JLcom/discord/restapi/RestAPIParams$GuildMFA;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$GuildMFA;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$GuildMFA;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/mfa"
    .end annotation
.end method

.method public abstract setUserTyping(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$EmptyBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$EmptyBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelTypingResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/typing"
    .end annotation
.end method

.method public abstract stopRinging(JLcom/discord/restapi/RestAPIParams$Ring;Ljava/lang/String;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$Ring;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lf0/i0/i;
            value = "X-Context-Properties"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$Ring;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "channels/{channelId}/call/stop-ringing"
    .end annotation
.end method

.method public abstract submitConnectionState(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$ConnectionState;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$ConnectionState;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$ConnectionState;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "connections/{connection}/callback"
    .end annotation
.end method

.method public abstract subscribeToGuild(JLcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "guilds/{guildId}/premium/subscriptions"
    .end annotation
.end method

.method public abstract syncIntegration(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "guilds/{guildId}/integrations/{integrationId}/sync"
    .end annotation
.end method

.method public abstract transferGuildOwnership(JLcom/discord/restapi/RestAPIParams$TransferGuildOwnership;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}"
    .end annotation
.end method

.method public abstract unbanUser(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/bans/{userId}"
    .end annotation
.end method

.method public abstract uncancelSubscriptionSlot(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "subscriptionSlotId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/guilds/premium/subscription-slots/{subscriptionSlotId}/uncancel"
    .end annotation
.end method

.method public abstract unsubscribeToGuild(JJ)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "subscriptionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/b;
        value = "guilds/{guildId}/premium/subscriptions/{subscriptionId}"
    .end annotation
.end method

.method public abstract updateConnection(Ljava/lang/String;Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$ConnectedAccount;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connection"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "connectionId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$ConnectedAccount;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$ConnectedAccount;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelConnectedAccount;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/connections/{connection}/{connectionId}"
    .end annotation
.end method

.method public abstract updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UpdateGuild;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UpdateGuild;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}"
    .end annotation
.end method

.method public abstract updateGuildIntegration(JJLcom/discord/restapi/RestAPIParams$GuildIntegration;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "integrationId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$GuildIntegration;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$GuildIntegration;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/integrations/{integrationId}"
    .end annotation
.end method

.method public abstract updatePaymentSource(Ljava/lang/String;Lcom/discord/models/domain/PatchPaymentSourceRaw;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "paymentSourceId"
        .end annotation
    .end param
    .param p2    # Lcom/discord/models/domain/PatchPaymentSourceRaw;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/PatchPaymentSourceRaw;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/billing/payment-sources/{paymentSourceId}"
    .end annotation
.end method

.method public abstract updatePermissionOverwrites(JJLcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "channelId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "targetId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "channels/{channelId}/permissions/{targetId}"
    .end annotation
.end method

.method public abstract updatePrivateChannelSettings(Lcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserGuildSettings;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserGuildSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/guilds/@me/settings"
    .end annotation
.end method

.method public abstract updateRole(JJLcom/discord/restapi/RestAPIParams$Role;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "roleId"
        .end annotation
    .end param
    .param p5    # Lcom/discord/restapi/RestAPIParams$Role;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/discord/restapi/RestAPIParams$Role;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/roles/{roleId}"
    .end annotation
.end method

.method public abstract updateSubscription(Ljava/lang/String;Lcom/discord/restapi/RestAPIParams$UpdateSubscription;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "subscriptionId"
        .end annotation
    .end param
    .param p2    # Lcom/discord/restapi/RestAPIParams$UpdateSubscription;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/restapi/RestAPIParams$UpdateSubscription;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/billing/subscriptions/{subscriptionId}"
    .end annotation
.end method

.method public abstract updateUserGuildSettings(JLcom/discord/restapi/RestAPIParams$UserGuildSettings;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UserGuildSettings;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UserGuildSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/guilds/{guildId}/settings"
    .end annotation
.end method

.method public abstract updateUserNotes(JLcom/discord/restapi/RestAPIParams$UserNoteUpdate;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/p;
        value = "users/@me/notes/{userId}"
    .end annotation
.end method

.method public abstract updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserSettings;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/settings"
    .end annotation
.end method

.method public abstract updateUserSettingsCustomStatus(Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/settings"
    .end annotation
.end method

.method public abstract updateVanityUrl(JLcom/discord/restapi/RestAPIParams$VanityUrl;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "guildId"
        .end annotation
    .end param
    .param p3    # Lcom/discord/restapi/RestAPIParams$VanityUrl;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/restapi/RestAPIParams$VanityUrl;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelGuild$VanityUrlResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "guilds/{guildId}/vanity-url"
    .end annotation
.end method

.method public abstract uploadLog(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "filename"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "debug-logs/4/{filename}"
    .end annotation
.end method

.method public abstract uploadLogs([Lokhttp3/MultipartBody$Part;)Lrx/Observable;
    .param p1    # [Lokhttp3/MultipartBody$Part;
        .annotation runtime Lf0/i0/q;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/l;
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "debug-logs/multi/4"
    .end annotation
.end method

.method public abstract userActivityAction(JJLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .param p3    # J
        .annotation runtime Lf0/i0/s;
            value = "applicationId"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lf0/i0/s;
            value = "sessionId"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lf0/i0/s;
            value = "actionType"
        .end annotation
    .end param
    .param p7    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "channel_id"
        .end annotation
    .end param
    .param p8    # Ljava/lang/Long;
        .annotation runtime Lf0/i0/t;
            value = "message_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/sessions/{sessionId}/activities/{applicationId}/{actionType}"
    .end annotation
.end method

.method public abstract userAddPhone(Lcom/discord/restapi/RestAPIParams$Phone;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$Phone;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$Phone;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/phone"
    .end annotation
.end method

.method public abstract userAgreements(Lcom/discord/restapi/RestAPIParams$UserAgreements;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserAgreements;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserAgreements;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/n;
        value = "users/@me/agreements"
    .end annotation
.end method

.method public abstract userCaptchaVerify(Lcom/discord/restapi/RestAPIParams$CaptchaCode;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$CaptchaCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$CaptchaCode;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/captcha/verify"
    .end annotation
.end method

.method public abstract userCreateChannel(Lcom/discord/restapi/RestAPIParams$CreateChannel;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$CreateChannel;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$CreateChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/channels"
    .end annotation
.end method

.method public abstract userCreateDevice(Lcom/discord/restapi/RestAPIParams$UserDevices;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$UserDevices;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$UserDevices;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/devices"
    .end annotation
.end method

.method public abstract userGet(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}"
    .end annotation
.end method

.method public abstract userPhoneDelete(Lcom/discord/restapi/RestAPIParams$DeletePhone;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$DeletePhone;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$DeletePhone;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/h;
        hasBody = true
        method = "DELETE"
        path = "users/@me/phone"
    .end annotation
.end method

.method public abstract userPhoneWithToken(Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "users/@me/phone"
    .end annotation
.end method

.method public abstract userProfileGet(J)Lrx/Observable;
    .param p1    # J
        .annotation runtime Lf0/i0/s;
            value = "userId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserProfile;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/f;
        value = "users/{userId}/profile"
    .end annotation
.end method

.method public abstract verifyPurchaseToken(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;)Lrx/Observable;
    .param p1    # Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;
        .annotation runtime Lf0/i0/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lf0/i0/o;
        value = "google-play/verify-purchase-token"
    .end annotation
.end method
