.class public final Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VerifyPurchaseTokenBody"
.end annotation


# instance fields
.field private final oneTimePurchaseSkuId:Ljava/lang/String;

.field private final packageName:Ljava/lang/String;

.field private final purchaseToken:Ljava/lang/String;

.field private final subscriptionSkuId:Ljava/lang/String;

.field private final userId:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "purchaseToken"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageName"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    iput-wide p2, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    iput-object p4, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-wide p2, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p4, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    :cond_2
    move-object p8, p4

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    :cond_3
    move-object v2, p5

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p6, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    :cond_4
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move-object p6, p8

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->copy(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    return-wide v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;
    .locals 8

    const-string v0, "purchaseToken"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageName"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    iget-wide v2, p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOneTimePurchaseSkuId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPurchaseToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubscriptionSkuId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "VerifyPurchaseTokenBody(purchaseToken="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->purchaseToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->userId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", packageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionSkuId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->subscriptionSkuId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", oneTimePurchaseSkuId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;->oneTimePurchaseSkuId:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
