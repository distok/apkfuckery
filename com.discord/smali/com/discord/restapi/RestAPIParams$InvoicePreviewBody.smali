.class public final Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InvoicePreviewBody"
.end annotation


# instance fields
.field private final applyEntitlements:Z

.field private final renewal:Z

.field private final subscriptionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1

    const-string/jumbo v0, "subscriptionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    iput-boolean p3, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->copy(Ljava/lang/String;ZZ)Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;ZZ)Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;
    .locals 1

    const-string/jumbo v0, "subscriptionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;-><init>(Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    iget-boolean v1, p1, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    iget-boolean p1, p1, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplyEntitlements()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    return v0
.end method

.method public final getRenewal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    return v0
.end method

.method public final getSubscriptionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "InvoicePreviewBody(subscriptionId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->subscriptionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", renewal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->renewal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", applyEntitlements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;->applyEntitlements:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
