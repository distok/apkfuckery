.class public final Lcom/discord/restapi/RequiredHeadersInterceptor;
.super Ljava/lang/Object;
.source "RestInterceptors.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;
    }
.end annotation


# instance fields
.field private final headersProvider:Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;


# direct methods
.method public constructor <init>(Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;)V
    .locals 1

    const-string v0, "headersProvider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RequiredHeadersInterceptor;->headersProvider:Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 12

    const-string v0, "chain"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/restapi/RequiredHeadersInterceptor;->headersProvider:Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;

    invoke-interface {v0}, Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/restapi/RequiredHeadersInterceptor;->headersProvider:Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;

    invoke-interface {v1}, Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;->getFingerprint()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/restapi/RequiredHeadersInterceptor;->headersProvider:Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;

    invoke-interface {v2}, Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;->getLocale()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/restapi/RequiredHeadersInterceptor;->headersProvider:Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;

    invoke-interface {v3}, Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->c()Lb0/a0;

    move-result-object v4

    const-string v5, "request"

    invoke-static {v4, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    iget-object v7, v4, Lb0/a0;->b:Lb0/x;

    iget-object v8, v4, Lb0/a0;->c:Ljava/lang/String;

    iget-object v10, v4, Lb0/a0;->e:Lokhttp3/RequestBody;

    iget-object v5, v4, Lb0/a0;->f:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v5, v4, Lb0/a0;->f:Ljava/util/Map;

    invoke-static {v5}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    :goto_0
    iget-object v4, v4, Lb0/a0;->d:Lokhttp3/Headers;

    invoke-virtual {v4}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v4

    const-string v6, "User-Agent"

    const-string v9, "name"

    invoke-static {v6, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v11, "value"

    invoke-static {v3, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v6, v3}, Lokhttp3/Headers$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    if-eqz v0, :cond_1

    const-string v3, "Authorization"

    invoke-static {v3, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v3, v0}, Lokhttp3/Headers$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    :cond_1
    if-eqz v1, :cond_2

    const-string v0, "X-Fingerprint"

    invoke-static {v0, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Lokhttp3/Headers$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    :cond_2
    if-eqz v2, :cond_3

    const-string v0, "Accept-Language"

    invoke-static {v0, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0, v2}, Lokhttp3/Headers$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    :cond_3
    if-eqz v7, :cond_5

    invoke-virtual {v4}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v9

    sget-object v0, Lb0/g0/c;->a:[B

    const-string v0, "$this$toImmutableMap"

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lx/h/m;->d:Lx/h/m;

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v5}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "Collections.unmodifiableMap(LinkedHashMap(this))"

    invoke-static {v0, v1}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    move-object v11, v0

    new-instance v0, Lb0/a0;

    move-object v6, v0

    invoke-direct/range {v6 .. v11}, Lb0/a0;-><init>(Lb0/x;Ljava/lang/String;Lokhttp3/Headers;Lokhttp3/RequestBody;Ljava/util/Map;)V

    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->a(Lb0/a0;)Lokhttp3/Response;

    move-result-object p1

    return-object p1

    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string/jumbo v0, "url == null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
