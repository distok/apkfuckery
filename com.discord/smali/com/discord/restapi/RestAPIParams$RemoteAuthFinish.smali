.class public final Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RemoteAuthFinish"
.end annotation


# instance fields
.field private final handshakeToken:Ljava/lang/String;

.field private final temporary:Z


# direct methods
.method public constructor <init>(ZLjava/lang/String;)V
    .locals 1

    const-string v0, "handshakeToken"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;->temporary:Z

    iput-object p2, p0, Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;->handshakeToken:Ljava/lang/String;

    return-void
.end method
