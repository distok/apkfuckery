.class public final Lcom/discord/restapi/PayloadJSON;
.super Ljava/lang/Object;
.source "PayloadJSON.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/PayloadJSON$ConverterFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final data:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    return-void
.end method

.method public static final synthetic access$getData$p(Lcom/discord/restapi/PayloadJSON;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    return-object p0
.end method

.method private final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/restapi/PayloadJSON;Ljava/lang/Object;ILjava/lang/Object;)Lcom/discord/restapi/PayloadJSON;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/restapi/PayloadJSON;->copy(Ljava/lang/Object;)Lcom/discord/restapi/PayloadJSON;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Ljava/lang/Object;)Lcom/discord/restapi/PayloadJSON;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/discord/restapi/PayloadJSON<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/discord/restapi/PayloadJSON;

    invoke-direct {v0, p1}, Lcom/discord/restapi/PayloadJSON;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/restapi/PayloadJSON;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/restapi/PayloadJSON;

    iget-object v0, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    iget-object p1, p1, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "PayloadJSON(data="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/restapi/PayloadJSON;->data:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
