.class public final Lcom/discord/restapi/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BOTTOM_END:I = 0x7f0a0001

.field public static final BOTTOM_START:I = 0x7f0a0002

.field public static final NO_DEBUG:I = 0x7f0a0006

.field public static final SHOW_ALL:I = 0x7f0a0008

.field public static final SHOW_PATH:I = 0x7f0a0009

.field public static final SHOW_PROGRESS:I = 0x7f0a000a

.field public static final TOP_END:I = 0x7f0a000c

.field public static final TOP_START:I = 0x7f0a000d

.field public static final accelerate:I = 0x7f0a0010

.field public static final accessibility_action_clickable_span:I = 0x7f0a001a

.field public static final accessibility_custom_action_0:I = 0x7f0a001b

.field public static final accessibility_custom_action_1:I = 0x7f0a001c

.field public static final accessibility_custom_action_10:I = 0x7f0a001d

.field public static final accessibility_custom_action_11:I = 0x7f0a001e

.field public static final accessibility_custom_action_12:I = 0x7f0a001f

.field public static final accessibility_custom_action_13:I = 0x7f0a0020

.field public static final accessibility_custom_action_14:I = 0x7f0a0021

.field public static final accessibility_custom_action_15:I = 0x7f0a0022

.field public static final accessibility_custom_action_16:I = 0x7f0a0023

.field public static final accessibility_custom_action_17:I = 0x7f0a0024

.field public static final accessibility_custom_action_18:I = 0x7f0a0025

.field public static final accessibility_custom_action_19:I = 0x7f0a0026

.field public static final accessibility_custom_action_2:I = 0x7f0a0027

.field public static final accessibility_custom_action_20:I = 0x7f0a0028

.field public static final accessibility_custom_action_21:I = 0x7f0a0029

.field public static final accessibility_custom_action_22:I = 0x7f0a002a

.field public static final accessibility_custom_action_23:I = 0x7f0a002b

.field public static final accessibility_custom_action_24:I = 0x7f0a002c

.field public static final accessibility_custom_action_25:I = 0x7f0a002d

.field public static final accessibility_custom_action_26:I = 0x7f0a002e

.field public static final accessibility_custom_action_27:I = 0x7f0a002f

.field public static final accessibility_custom_action_28:I = 0x7f0a0030

.field public static final accessibility_custom_action_29:I = 0x7f0a0031

.field public static final accessibility_custom_action_3:I = 0x7f0a0032

.field public static final accessibility_custom_action_30:I = 0x7f0a0033

.field public static final accessibility_custom_action_31:I = 0x7f0a0034

.field public static final accessibility_custom_action_4:I = 0x7f0a0035

.field public static final accessibility_custom_action_5:I = 0x7f0a0036

.field public static final accessibility_custom_action_6:I = 0x7f0a0037

.field public static final accessibility_custom_action_7:I = 0x7f0a0038

.field public static final accessibility_custom_action_8:I = 0x7f0a0039

.field public static final accessibility_custom_action_9:I = 0x7f0a003a

.field public static final action_bar:I = 0x7f0a003f

.field public static final action_bar_activity_content:I = 0x7f0a0040

.field public static final action_bar_container:I = 0x7f0a0041

.field public static final action_bar_root:I = 0x7f0a0042

.field public static final action_bar_spinner:I = 0x7f0a0043

.field public static final action_bar_subtitle:I = 0x7f0a0044

.field public static final action_bar_title:I = 0x7f0a0046

.field public static final action_container:I = 0x7f0a004a

.field public static final action_context_bar:I = 0x7f0a004b

.field public static final action_divider:I = 0x7f0a004c

.field public static final action_image:I = 0x7f0a004d

.field public static final action_menu_divider:I = 0x7f0a004e

.field public static final action_menu_presenter:I = 0x7f0a004f

.field public static final action_mode_bar:I = 0x7f0a0050

.field public static final action_mode_bar_stub:I = 0x7f0a0051

.field public static final action_mode_close_button:I = 0x7f0a0052

.field public static final action_text:I = 0x7f0a0053

.field public static final actions:I = 0x7f0a0054

.field public static final activity_chooser_view_content:I = 0x7f0a005e

.field public static final add:I = 0x7f0a0060

.field public static final adjust_height:I = 0x7f0a006f

.field public static final adjust_width:I = 0x7f0a0070

.field public static final alertTitle:I = 0x7f0a007e

.field public static final aligned:I = 0x7f0a0090

.field public static final animateToEnd:I = 0x7f0a0094

.field public static final animateToStart:I = 0x7f0a0095

.field public static final asConfigured:I = 0x7f0a00a1

.field public static final async:I = 0x7f0a00a2

.field public static final auto:I = 0x7f0a00df

.field public static final autoComplete:I = 0x7f0a00e0

.field public static final autoCompleteToEnd:I = 0x7f0a00e1

.field public static final autoCompleteToStart:I = 0x7f0a00e2

.field public static final barrier:I = 0x7f0a00f2

.field public static final baseline:I = 0x7f0a00f3

.field public static final bidirectional:I = 0x7f0a00f6

.field public static final blocking:I = 0x7f0a00ff

.field public static final bottom:I = 0x7f0a0127

.field public static final bounce:I = 0x7f0a0129

.field public static final buttonPanel:I = 0x7f0a012f

.field public static final cancel_button:I = 0x7f0a014b

.field public static final center:I = 0x7f0a014f

.field public static final centerCrop:I = 0x7f0a0150

.field public static final centerInside:I = 0x7f0a0151

.field public static final chain:I = 0x7f0a0154

.field public static final checkbox:I = 0x7f0a0295

.field public static final checked:I = 0x7f0a0297

.field public static final chip:I = 0x7f0a0298

.field public static final chip1:I = 0x7f0a0299

.field public static final chip2:I = 0x7f0a029a

.field public static final chip3:I = 0x7f0a029b

.field public static final chip_group:I = 0x7f0a029c

.field public static final chip_image:I = 0x7f0a029d

.field public static final chip_text:I = 0x7f0a029e

.field public static final chronometer:I = 0x7f0a02a2

.field public static final circle_center:I = 0x7f0a02a4

.field public static final circular:I = 0x7f0a02a5

.field public static final clear_text:I = 0x7f0a02a6

.field public static final column:I = 0x7f0a02af

.field public static final column_reverse:I = 0x7f0a02b0

.field public static final confirm_button:I = 0x7f0a02dc

.field public static final container:I = 0x7f0a02f2

.field public static final content:I = 0x7f0a02f3

.field public static final contentPanel:I = 0x7f0a02f4

.field public static final coordinator:I = 0x7f0a02f9

.field public static final cos:I = 0x7f0a02fa

.field public static final custom:I = 0x7f0a0320

.field public static final customPanel:I = 0x7f0a0321

.field public static final cut:I = 0x7f0a0322

.field public static final dark:I = 0x7f0a0323

.field public static final date_picker_actions:I = 0x7f0a0325

.field public static final decelerate:I = 0x7f0a0329

.field public static final decelerateAndComplete:I = 0x7f0a032a

.field public static final decor_content_parent:I = 0x7f0a032b

.field public static final default_activity_button:I = 0x7f0a032c

.field public static final deltaRelative:I = 0x7f0a032e

.field public static final design_bottom_sheet:I = 0x7f0a0331

.field public static final design_menu_item_action_area:I = 0x7f0a0332

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0333

.field public static final design_menu_item_text:I = 0x7f0a0334

.field public static final design_navigation_view:I = 0x7f0a0335

.field public static final dialog_button:I = 0x7f0a033c

.field public static final dragDown:I = 0x7f0a0369

.field public static final dragEnd:I = 0x7f0a036a

.field public static final dragLeft:I = 0x7f0a036b

.field public static final dragRight:I = 0x7f0a036c

.field public static final dragStart:I = 0x7f0a036d

.field public static final dragUp:I = 0x7f0a036e

.field public static final dropdown_menu:I = 0x7f0a0372

.field public static final easeIn:I = 0x7f0a0374

.field public static final easeInOut:I = 0x7f0a0375

.field public static final easeOut:I = 0x7f0a0376

.field public static final edit_query:I = 0x7f0a039a

.field public static final end:I = 0x7f0a03c7

.field public static final expand_activities_button:I = 0x7f0a03e4

.field public static final expanded_menu:I = 0x7f0a03e5

.field public static final fade:I = 0x7f0a040f

.field public static final fill:I = 0x7f0a0426

.field public static final filled:I = 0x7f0a0429

.field public static final fitBottomStart:I = 0x7f0a042d

.field public static final fitCenter:I = 0x7f0a042e

.field public static final fitEnd:I = 0x7f0a042f

.field public static final fitStart:I = 0x7f0a0430

.field public static final fitXY:I = 0x7f0a0432

.field public static final fixed:I = 0x7f0a0433

.field public static final flex_end:I = 0x7f0a0439

.field public static final flex_start:I = 0x7f0a044b

.field public static final flip:I = 0x7f0a044c

.field public static final floating:I = 0x7f0a044d

.field public static final focusCrop:I = 0x7f0a0461

.field public static final forever:I = 0x7f0a0466

.field public static final fragment_container_view_tag:I = 0x7f0a0467

.field public static final ghost_view:I = 0x7f0a0483

.field public static final ghost_view_holder:I = 0x7f0a0484

.field public static final gone:I = 0x7f0a04b4

.field public static final group_divider:I = 0x7f0a04b8

.field public static final guideline:I = 0x7f0a04c2

.field public static final header_title:I = 0x7f0a0564

.field public static final home:I = 0x7f0a0566

.field public static final honorRequest:I = 0x7f0a0578

.field public static final icon:I = 0x7f0a057a

.field public static final icon_frame:I = 0x7f0a057b

.field public static final icon_group:I = 0x7f0a057c

.field public static final icon_only:I = 0x7f0a057d

.field public static final ignore:I = 0x7f0a0586

.field public static final ignoreRequest:I = 0x7f0a0587

.field public static final image:I = 0x7f0a0588

.field public static final incoming:I = 0x7f0a0599

.field public static final info:I = 0x7f0a05a9

.field public static final invisible:I = 0x7f0a05c3

.field public static final italic:I = 0x7f0a05e6

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0624

.field public static final jumpToEnd:I = 0x7f0a0629

.field public static final jumpToStart:I = 0x7f0a062a

.field public static final labeled:I = 0x7f0a0631

.field public static final largeLabel:I = 0x7f0a0633

.field public static final layout:I = 0x7f0a0637

.field public static final left:I = 0x7f0a063f

.field public static final light:I = 0x7f0a0640

.field public static final line1:I = 0x7f0a0641

.field public static final line3:I = 0x7f0a0642

.field public static final linear:I = 0x7f0a0643

.field public static final listMode:I = 0x7f0a0645

.field public static final list_item:I = 0x7f0a0646

.field public static final masked:I = 0x7f0a0658

.field public static final material_clock_display:I = 0x7f0a065e

.field public static final material_clock_face:I = 0x7f0a065f

.field public static final material_clock_hand:I = 0x7f0a0660

.field public static final material_clock_period_am_button:I = 0x7f0a0661

.field public static final material_clock_period_pm_button:I = 0x7f0a0662

.field public static final material_clock_period_toggle:I = 0x7f0a0663

.field public static final material_hour_text_input:I = 0x7f0a0664

.field public static final material_hour_tv:I = 0x7f0a0665

.field public static final material_label:I = 0x7f0a0666

.field public static final material_minute_text_input:I = 0x7f0a0667

.field public static final material_minute_tv:I = 0x7f0a0668

.field public static final material_textinput_timepicker:I = 0x7f0a0669

.field public static final material_timepicker_cancel_button:I = 0x7f0a066a

.field public static final material_timepicker_container:I = 0x7f0a066b

.field public static final material_timepicker_edit_text:I = 0x7f0a066c

.field public static final material_timepicker_mode_button:I = 0x7f0a066d

.field public static final material_timepicker_ok_button:I = 0x7f0a066e

.field public static final material_timepicker_view:I = 0x7f0a066f

.field public static final material_value_index:I = 0x7f0a0670

.field public static final message:I = 0x7f0a06a9

.field public static final middle:I = 0x7f0a06ad

.field public static final mini:I = 0x7f0a06ae

.field public static final month_grid:I = 0x7f0a06b0

.field public static final month_navigation_bar:I = 0x7f0a06b1

.field public static final month_navigation_fragment_toggle:I = 0x7f0a06b2

.field public static final month_navigation_next:I = 0x7f0a06b3

.field public static final month_navigation_previous:I = 0x7f0a06b4

.field public static final month_title:I = 0x7f0a06b5

.field public static final motion_base:I = 0x7f0a06b6

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a06b7

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a06b8

.field public static final mtrl_calendar_frame:I = 0x7f0a06b9

.field public static final mtrl_calendar_main_pane:I = 0x7f0a06ba

.field public static final mtrl_calendar_months:I = 0x7f0a06bb

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a06bc

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a06bd

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a06be

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a06bf

.field public static final mtrl_child_content_container:I = 0x7f0a06c0

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a06c1

.field public static final mtrl_motion_snapshot_view:I = 0x7f0a06c2

.field public static final mtrl_picker_fullscreen:I = 0x7f0a06c3

.field public static final mtrl_picker_header:I = 0x7f0a06c4

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a06c5

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a06c6

.field public static final mtrl_picker_header_toggle:I = 0x7f0a06c7

.field public static final mtrl_picker_text_input_date:I = 0x7f0a06c8

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a06c9

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a06ca

.field public static final mtrl_picker_title_text:I = 0x7f0a06cb

.field public static final multiply:I = 0x7f0a06cc

.field public static final navigation_header_container:I = 0x7f0a06de

.field public static final none:I = 0x7f0a06f3

.field public static final normal:I = 0x7f0a06f4

.field public static final notification_background:I = 0x7f0a06fe

.field public static final notification_main_column:I = 0x7f0a06ff

.field public static final notification_main_column_container:I = 0x7f0a0700

.field public static final nowrap:I = 0x7f0a0704

.field public static final off:I = 0x7f0a0734

.field public static final on:I = 0x7f0a0738

.field public static final outgoing:I = 0x7f0a073a

.field public static final outline:I = 0x7f0a073b

.field public static final packed:I = 0x7f0a0749

.field public static final parallax:I = 0x7f0a074b

.field public static final parent:I = 0x7f0a074c

.field public static final parentPanel:I = 0x7f0a074d

.field public static final parentRelative:I = 0x7f0a074e

.field public static final parent_matrix:I = 0x7f0a074f

.field public static final password_toggle:I = 0x7f0a0760

.field public static final path:I = 0x7f0a0761

.field public static final pathRelative:I = 0x7f0a0762

.field public static final percent:I = 0x7f0a0779

.field public static final pin:I = 0x7f0a0788

.field public static final position:I = 0x7f0a0797

.field public static final postLayout:I = 0x7f0a0798

.field public static final progress_circular:I = 0x7f0a0801

.field public static final progress_horizontal:I = 0x7f0a0804

.field public static final radio:I = 0x7f0a0814

.field public static final rectangles:I = 0x7f0a081b

.field public static final recycler_view:I = 0x7f0a081c

.field public static final reverseSawtooth:I = 0x7f0a0829

.field public static final right:I = 0x7f0a083b

.field public static final right_icon:I = 0x7f0a083c

.field public static final right_side:I = 0x7f0a083d

.field public static final rounded:I = 0x7f0a0866

.field public static final row:I = 0x7f0a0867

.field public static final row_index_key:I = 0x7f0a0868

.field public static final row_reverse:I = 0x7f0a0869

.field public static final save_non_transition_alpha:I = 0x7f0a086a

.field public static final save_overlay_view:I = 0x7f0a086b

.field public static final sawtooth:I = 0x7f0a086c

.field public static final scale:I = 0x7f0a086d

.field public static final screen:I = 0x7f0a086f

.field public static final scrollIndicatorDown:I = 0x7f0a0875

.field public static final scrollIndicatorUp:I = 0x7f0a0876

.field public static final scrollView:I = 0x7f0a0877

.field public static final scrollable:I = 0x7f0a0879

.field public static final search_badge:I = 0x7f0a087a

.field public static final search_bar:I = 0x7f0a087b

.field public static final search_button:I = 0x7f0a087c

.field public static final search_close_btn:I = 0x7f0a087d

.field public static final search_edit_frame:I = 0x7f0a087e

.field public static final search_go_btn:I = 0x7f0a0880

.field public static final search_mag_icon:I = 0x7f0a0881

.field public static final search_plate:I = 0x7f0a0882

.field public static final search_src_text:I = 0x7f0a0885

.field public static final search_voice_btn:I = 0x7f0a0891

.field public static final seekbar:I = 0x7f0a0892

.field public static final seekbar_value:I = 0x7f0a0893

.field public static final select_dialog_listview:I = 0x7f0a0894

.field public static final selected:I = 0x7f0a0896

.field public static final selection_type:I = 0x7f0a0898

.field public static final shortcut:I = 0x7f0a0a02

.field public static final sin:I = 0x7f0a0a06

.field public static final slide:I = 0x7f0a0a08

.field public static final smallLabel:I = 0x7f0a0a09

.field public static final snackbar_action:I = 0x7f0a0a0a

.field public static final snackbar_text:I = 0x7f0a0a0b

.field public static final space_around:I = 0x7f0a0a0f

.field public static final space_between:I = 0x7f0a0a10

.field public static final space_evenly:I = 0x7f0a0a11

.field public static final spacer:I = 0x7f0a0a12

.field public static final spinner:I = 0x7f0a0a15

.field public static final spline:I = 0x7f0a0a16

.field public static final split_action_bar:I = 0x7f0a0a17

.field public static final spread:I = 0x7f0a0a18

.field public static final spread_inside:I = 0x7f0a0a19

.field public static final square:I = 0x7f0a0a1a

.field public static final src_atop:I = 0x7f0a0a1b

.field public static final src_in:I = 0x7f0a0a1c

.field public static final src_over:I = 0x7f0a0a1d

.field public static final standard:I = 0x7f0a0a1f

.field public static final start:I = 0x7f0a0a20

.field public static final startHorizontal:I = 0x7f0a0a21

.field public static final startVertical:I = 0x7f0a0a22

.field public static final staticLayout:I = 0x7f0a0a28

.field public static final staticPostLayout:I = 0x7f0a0a29

.field public static final stop:I = 0x7f0a0a67

.field public static final stretch:I = 0x7f0a0a6c

.field public static final submenuarrow:I = 0x7f0a0a6d

.field public static final submit_area:I = 0x7f0a0a6e

.field public static final switchWidget:I = 0x7f0a0a7c

.field public static final tabMode:I = 0x7f0a0a7e

.field public static final tag_accessibility_actions:I = 0x7f0a0a8c

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a0a8d

.field public static final tag_accessibility_heading:I = 0x7f0a0a8e

.field public static final tag_accessibility_pane_title:I = 0x7f0a0a8f

.field public static final tag_screen_reader_focusable:I = 0x7f0a0a90

.field public static final tag_transition_group:I = 0x7f0a0a91

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0a92

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0a93

.field public static final test_checkbox_android_button_tint:I = 0x7f0a0a9a

.field public static final test_checkbox_app_button_tint:I = 0x7f0a0a9b

.field public static final test_radiobutton_android_button_tint:I = 0x7f0a0a9c

.field public static final test_radiobutton_app_button_tint:I = 0x7f0a0a9d

.field public static final text:I = 0x7f0a0a9e

.field public static final text2:I = 0x7f0a0aa0

.field public static final textSpacerNoButtons:I = 0x7f0a0aa2

.field public static final textSpacerNoTitle:I = 0x7f0a0aa3

.field public static final text_input_end_icon:I = 0x7f0a0aa6

.field public static final text_input_start_icon:I = 0x7f0a0aa7

.field public static final textinput_counter:I = 0x7f0a0aaa

.field public static final textinput_error:I = 0x7f0a0aab

.field public static final textinput_helper_text:I = 0x7f0a0aac

.field public static final textinput_placeholder:I = 0x7f0a0aad

.field public static final textinput_prefix_text:I = 0x7f0a0aae

.field public static final textinput_suffix_text:I = 0x7f0a0aaf

.field public static final time:I = 0x7f0a0ab5

.field public static final title:I = 0x7f0a0ab6

.field public static final titleDividerNoCustom:I = 0x7f0a0ab7

.field public static final title_template:I = 0x7f0a0ab8

.field public static final top:I = 0x7f0a0abf

.field public static final topPanel:I = 0x7f0a0ac0

.field public static final touch_outside:I = 0x7f0a0ac2

.field public static final transition_current_scene:I = 0x7f0a0ac5

.field public static final transition_layout_save:I = 0x7f0a0ac6

.field public static final transition_position:I = 0x7f0a0ac7

.field public static final transition_scene_layoutid_cache:I = 0x7f0a0ac8

.field public static final transition_transform:I = 0x7f0a0ac9

.field public static final triangle:I = 0x7f0a0ad1

.field public static final unchecked:I = 0x7f0a0adb

.field public static final uniform:I = 0x7f0a0ade

.field public static final unlabeled:I = 0x7f0a0adf

.field public static final up:I = 0x7f0a0ae0

.field public static final view_offset_helper:I = 0x7f0a0b63

.field public static final visible:I = 0x7f0a0b77

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a0b78

.field public static final wide:I = 0x7f0a0bac

.field public static final withinBounds:I = 0x7f0a0be8

.field public static final wrap:I = 0x7f0a0be9

.field public static final wrap_content:I = 0x7f0a0bea

.field public static final wrap_reverse:I = 0x7f0a0beb

.field public static final zero_corner_chip:I = 0x7f0a0bf0


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
