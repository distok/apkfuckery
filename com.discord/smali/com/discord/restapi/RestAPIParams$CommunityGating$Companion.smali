.class public final Lcom/discord/restapi/RestAPIParams$CommunityGating$Companion;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams$CommunityGating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/restapi/RestAPIParams$CommunityGating$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromMemberVerificationForm(Ljava/lang/String;Ljava/util/List;)Lcom/discord/restapi/RestAPIParams$CommunityGating;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;)",
            "Lcom/discord/restapi/RestAPIParams$CommunityGating;"
        }
    .end annotation

    new-instance v0, Lcom/discord/restapi/RestAPIParams$CommunityGating;

    new-instance v1, Lf/h/d/k;

    invoke-direct {v1}, Lf/h/d/k;-><init>()V

    sget-object v2, Lf/h/d/d;->g:Lf/h/d/d;

    iput-object v2, v1, Lf/h/d/k;->c:Lf/h/d/e;

    invoke-virtual {v1}, Lf/h/d/k;->a()Lcom/google/gson/Gson;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/gson/Gson;->k(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p2, p1}, Lcom/discord/restapi/RestAPIParams$CommunityGating;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
