.class public final Lcom/discord/restapi/RestAPIParams$AuthRegister;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AuthRegister"
.end annotation


# instance fields
.field private final captchaKey:Ljava/lang/String;

.field private final consent:Z

.field private final dateOfBirth:Ljava/lang/String;

.field private final email:Ljava/lang/String;

.field private final fingerprint:Ljava/lang/String;

.field private final guildTemplateCode:Ljava/lang/String;

.field private final invite:Ljava/lang/String;

.field private final password:Ljava/lang/String;

.field private final username:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->fingerprint:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->username:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->email:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->password:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->captchaKey:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->invite:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->guildTemplateCode:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->consent:Z

    iput-object p9, p0, Lcom/discord/restapi/RestAPIParams$AuthRegister;->dateOfBirth:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 11

    move/from16 v0, p10

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v10, v0

    goto :goto_0

    :cond_0
    move-object/from16 v10, p9

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/discord/restapi/RestAPIParams$AuthRegister;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method
