.class public final Lcom/discord/restapi/RestAPIParams$Message$MessageReference;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams$Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MessageReference"
.end annotation


# instance fields
.field private final channelId:J

.field private final guildId:Ljava/lang/Long;

.field private final messageId:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/Long;JLjava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;->guildId:Ljava/lang/Long;

    iput-wide p2, p0, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;->channelId:J

    iput-object p4, p0, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;->messageId:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;->channelId:J

    return-wide v0
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getMessageId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/RestAPIParams$Message$MessageReference;->messageId:Ljava/lang/Long;

    return-object v0
.end method
