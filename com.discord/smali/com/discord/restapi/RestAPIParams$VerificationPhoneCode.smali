.class public final Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VerificationPhoneCode"
.end annotation


# instance fields
.field private final password:Ljava/lang/String;

.field private final phoneToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "phoneToken"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "password"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;->phoneToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;->password:Ljava/lang/String;

    return-void
.end method
