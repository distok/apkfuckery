.class public final Lcom/discord/restapi/RestAPIBuilder;
.super Ljava/lang/Object;
.source "RestAPIBuilder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/RestAPIBuilder$Companion;
    }
.end annotation


# static fields
.field private static final API_VERSION:I = 0x8

.field public static final CONTENT_TYPE_JSON:Ljava/lang/String; = "application/json"

.field public static final CONTENT_TYPE_TEXT:Ljava/lang/String; = "text/plain"

.field public static final Companion:Lcom/discord/restapi/RestAPIBuilder$Companion;

.field private static final DEFAULT_TIMEOUT_MILLIS:J = 0x2710L

.field private static clientCallback:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/String;",
            "-",
            "Lb0/y;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final baseApiUrl:Ljava/lang/String;

.field private final cookieJar:Lb0/p;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/restapi/RestAPIBuilder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/restapi/RestAPIBuilder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/restapi/RestAPIBuilder;->Companion:Lcom/discord/restapi/RestAPIBuilder$Companion;

    sget-object v0, Lcom/discord/restapi/RestAPIBuilder$Companion$clientCallback$1;->INSTANCE:Lcom/discord/restapi/RestAPIBuilder$Companion$clientCallback$1;

    sput-object v0, Lcom/discord/restapi/RestAPIBuilder;->clientCallback:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lb0/p;)V
    .locals 1

    const-string v0, "baseApiUrl"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cookieJar"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/RestAPIBuilder;->baseApiUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/restapi/RestAPIBuilder;->cookieJar:Lb0/p;

    return-void
.end method

.method public static final synthetic access$getClientCallback$cp()Lkotlin/jvm/functions/Function2;
    .locals 1

    sget-object v0, Lcom/discord/restapi/RestAPIBuilder;->clientCallback:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public static final synthetic access$setClientCallback$cp(Lkotlin/jvm/functions/Function2;)V
    .locals 0

    sput-object p0, Lcom/discord/restapi/RestAPIBuilder;->clientCallback:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static synthetic build$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 7

    and-int/lit8 v0, p9, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move v0, p2

    :goto_0
    and-int/lit8 v1, p9, 0x4

    if-eqz v1, :cond_1

    const-wide/16 v1, 0x2710

    goto :goto_1

    :cond_1
    move-wide v1, p3

    :goto_1
    and-int/lit8 v3, p9, 0x8

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    move-object v3, v4

    goto :goto_2

    :cond_2
    move-object v3, p5

    :goto_2
    and-int/lit8 v5, p9, 0x10

    if-eqz v5, :cond_3

    goto :goto_3

    :cond_3
    move-object v4, p6

    :goto_3
    and-int/lit8 v5, p9, 0x20

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    goto :goto_4

    :cond_4
    move v5, p7

    :goto_4
    and-int/lit8 v6, p9, 0x40

    if-eqz v6, :cond_5

    const-string v6, "application/json"

    goto :goto_5

    :cond_5
    move-object v6, p8

    :goto_5
    move-object p2, p0

    move-object p3, p1

    move p4, v0

    move-wide p5, v1

    move-object p7, v3

    move-object p8, v4

    move/from16 p9, v5

    move-object/from16 p10, v6

    invoke-virtual/range {p2 .. p10}, Lcom/discord/restapi/RestAPIBuilder;->build(Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private final buildApi(Lb0/y;Ljava/lang/Class;Ljava/lang/String;ZZLjava/lang/String;)Ljava/lang/Object;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb0/y;",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    move-object/from16 v0, p2

    new-instance v1, Lf/h/d/k;

    invoke-direct {v1}, Lf/h/d/k;-><init>()V

    sget-object v2, Lf/h/d/d;->g:Lf/h/d/d;

    iput-object v2, v1, Lf/h/d/k;->c:Lf/h/d/e;

    new-instance v2, Lcom/discord/models/domain/Model$TypeAdapterFactory;

    invoke-direct {v2}, Lcom/discord/models/domain/Model$TypeAdapterFactory;-><init>()V

    iget-object v3, v1, Lf/h/d/k;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;->INSTANCE:Lcom/discord/models/experiments/dto/UserExperimentDto$TypeAdapterFactory;

    iget-object v3, v1, Lf/h/d/k;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v2, Lcom/discord/restapi/RestAPIParams$ChannelPosition;

    new-instance v3, Lcom/discord/restapi/RestAPIParams$ChannelPosition$Serializer;

    invoke-direct {v3}, Lcom/discord/restapi/RestAPIParams$ChannelPosition$Serializer;-><init>()V

    const/4 v4, 0x1

    invoke-static {v4}, Lf/h/a/f/f/n/g;->h(Z)V

    instance-of v5, v3, Lf/h/d/l;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lf/h/d/k;->d:Ljava/util/Map;

    move-object v6, v3

    check-cast v6, Lf/h/d/l;

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {v2}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v5

    iget-object v6, v1, Lf/h/d/k;->e:Ljava/util/List;

    invoke-virtual {v5}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v8

    const/4 v9, 0x0

    if-ne v7, v8, :cond_1

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    :goto_0
    new-instance v8, Lf/h/d/w/y/m$c;

    const/4 v10, 0x0

    invoke-direct {v8, v3, v5, v7, v10}, Lf/h/d/w/y/m$c;-><init>(Ljava/lang/Object;Lcom/google/gson/reflect/TypeToken;ZLjava/lang/Class;)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    instance-of v5, v3, Lcom/google/gson/TypeAdapter;

    if-eqz v5, :cond_2

    iget-object v5, v1, Lf/h/d/k;->e:Ljava/util/List;

    invoke-static {v2}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v2

    check-cast v3, Lcom/google/gson/TypeAdapter;

    sget-object v6, Lf/h/d/w/y/o;->a:Lcom/google/gson/TypeAdapter;

    new-instance v6, Lf/h/d/w/y/p;

    invoke-direct {v6, v2, v3}, Lf/h/d/w/y/p;-><init>(Lcom/google/gson/reflect/TypeToken;Lcom/google/gson/TypeAdapter;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p4, :cond_3

    iput-boolean v4, v1, Lf/h/d/k;->g:Z

    :cond_3
    invoke-virtual {v1}, Lf/h/d/k;->a()Lcom/google/gson/Gson;

    move-result-object v1

    const-string v2, ""

    if-eqz p5, :cond_4

    const-string/jumbo v3, "v8/"

    move-object/from16 v5, p3

    goto :goto_1

    :cond_4
    move-object/from16 v5, p3

    move-object v3, v2

    :goto_1
    invoke-static {v5, v3}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lf0/w;->c:Lf0/w;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const-string v8, "client == null"

    move-object/from16 v12, p1

    invoke-static {v12, v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v8, Lf0/g0/a/g;

    invoke-direct {v8, v10, v9}, Lf0/g0/a/g;-><init>(Lrx/Scheduler;Z)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lf0/h0/b/k;

    invoke-direct {v8}, Lf0/h0/b/k;-><init>()V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/discord/restapi/PayloadJSON$ConverterFactory;

    const-string v11, "gson"

    invoke-static {v1, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v8, v1}, Lcom/discord/restapi/PayloadJSON$ConverterFactory;-><init>(Lcom/google/gson/Gson;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v8, "application/json"

    move-object/from16 v11, p6

    invoke-static {v11, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    new-instance v8, Lf0/h0/a/a;

    invoke-direct {v8, v1}, Lf0/h0/a/a;-><init>(Lcom/google/gson/Gson;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    const-string v1, "baseUrl == null"

    invoke-static {v3, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v1, "$this$toHttpUrl"

    invoke-static {v3, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lb0/x$a;

    invoke-direct {v1}, Lb0/x$a;-><init>()V

    invoke-virtual {v1, v10, v3}, Lb0/x$a;->e(Lb0/x;Ljava/lang/String;)Lb0/x$a;

    invoke-virtual {v1}, Lb0/x$a;->b()Lb0/x;

    move-result-object v13

    iget-object v1, v13, Lb0/x;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v5}, Lf0/w;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v3, Lf0/i;

    invoke-direct {v3, v1}, Lf0/i;-><init>(Ljava/util/concurrent/Executor;)V

    iget-boolean v7, v5, Lf0/w;->a:Z

    if-eqz v7, :cond_6

    const/4 v7, 0x2

    new-array v7, v7, [Lf0/e$a;

    sget-object v8, Lf0/g;->a:Lf0/e$a;

    aput-object v8, v7, v9

    aput-object v3, v7, v4

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto :goto_2

    :cond_6
    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    :goto_2
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/2addr v7, v4

    iget-boolean v8, v5, Lf0/w;->a:Z

    add-int/2addr v7, v8

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v7, Lf0/c;

    invoke-direct {v7}, Lf0/c;-><init>()V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-boolean v5, v5, Lf0/w;->a:Z

    if-eqz v5, :cond_7

    sget-object v5, Lf0/s;->a:Lf0/h$a;

    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    goto :goto_3

    :cond_7
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    :goto_3
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v5, Lf0/b0;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v14

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v15

    const/16 v17, 0x0

    move-object v11, v5

    move-object/from16 v12, p1

    move-object/from16 v16, v1

    invoke-direct/range {v11 .. v17}, Lf0/b0;-><init>(Lb0/e$a;Lb0/x;Ljava/util/List;Ljava/util/List;Ljava/util/concurrent/Executor;Z)V

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1, v4}, Ljava/util/ArrayDeque;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v3

    array-length v3, v3

    if-eqz v3, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Type parameters are unsupported on "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eq v2, v0, :cond_8

    const-string v2, " which is an interface of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    invoke-virtual {v2}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_4

    :cond_a
    iget-boolean v1, v5, Lf0/b0;->f:Z

    if-eqz v1, :cond_d

    sget-object v1, Lf0/w;->c:Lf0/w;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v3, :cond_d

    aget-object v7, v2, v6

    iget-boolean v8, v1, Lf0/w;->a:Z

    if-eqz v8, :cond_b

    invoke-virtual {v7}, Ljava/lang/reflect/Method;->isDefault()Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v8, 0x1

    goto :goto_6

    :cond_b
    const/4 v8, 0x0

    :goto_6
    if-nez v8, :cond_c

    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v8

    invoke-static {v8}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v8

    if-nez v8, :cond_c

    invoke-virtual {v5, v7}, Lf0/b0;->b(Ljava/lang/reflect/Method;)Lf0/c0;

    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_d
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Class;

    aput-object v0, v2, v9

    new-instance v3, Lf0/a0;

    invoke-direct {v3, v5, v0}, Lf0/a0;-><init>(Lf0/b0;Ljava/lang/Class;)V

    invoke-static {v1, v2, v3}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "API declarations must be interfaces."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "baseUrl must end in /: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic buildApi$default(Lcom/discord/restapi/RestAPIBuilder;Lb0/y;Ljava/lang/Class;Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 7

    and-int/lit8 p7, p7, 0x8

    if-eqz p7, :cond_0

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p4

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/restapi/RestAPIBuilder;->buildApi(Lb0/y;Ljava/lang/Class;Ljava/lang/String;ZZLjava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private final buildOkHttpClient(Ljava/lang/Long;Ljava/util/List;)Lb0/y;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "+",
            "Lokhttp3/Interceptor;",
            ">;)",
            "Lb0/y;"
        }
    .end annotation

    new-instance v0, Lb0/y$a;

    invoke-direct {v0}, Lb0/y$a;-><init>()V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/Interceptor;

    const-string v2, "interceptor"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lb0/y$a;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, p2}, Lb0/y$a;->a(JLjava/util/concurrent/TimeUnit;)Lb0/y$a;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string/jumbo v3, "unit"

    invoke-static {p2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "timeout"

    invoke-static {v4, v1, v2, p2}, Lb0/g0/c;->b(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result v1

    iput v1, v0, Lb0/y$a;->z:I

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v1, v2, p2}, Lb0/g0/c;->b(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, v0, Lb0/y$a;->x:I

    :cond_1
    iget-object p1, p0, Lcom/discord/restapi/RestAPIBuilder;->cookieJar:Lb0/p;

    const-string p2, "cookieJar"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, v0, Lb0/y$a;->j:Lb0/p;

    new-instance p1, Lb0/y;

    invoke-direct {p1, v0}, Lb0/y;-><init>(Lb0/y$a;)V

    return-object p1
.end method

.method public static synthetic buildOkHttpClient$default(Lcom/discord/restapi/RestAPIBuilder;Ljava/lang/Long;Ljava/util/List;ILjava/lang/Object;)Lb0/y;
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    move-object p2, v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/restapi/RestAPIBuilder;->buildOkHttpClient(Ljava/lang/Long;Ljava/util/List;)Lb0/y;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final build(Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;ZJ",
            "Ljava/util/List<",
            "+",
            "Lokhttp3/Interceptor;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    const-string v0, "apiDefinition"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentType"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-direct {p0, p3, p5}, Lcom/discord/restapi/RestAPIBuilder;->buildOkHttpClient(Ljava/lang/Long;Ljava/util/List;)Lb0/y;

    move-result-object v1

    if-eqz p6, :cond_0

    sget-object p3, Lcom/discord/restapi/RestAPIBuilder;->clientCallback:Lkotlin/jvm/functions/Function2;

    invoke-interface {p3, p6, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v3, p0, Lcom/discord/restapi/RestAPIBuilder;->baseApiUrl:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    move v4, p2

    move v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/discord/restapi/RestAPIBuilder;->buildApi(Lb0/y;Ljava/lang/Class;Ljava/lang/String;ZZLjava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
