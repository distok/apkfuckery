.class public final Lcom/discord/restapi/utils/CountingRequestBody;
.super Lokhttp3/RequestBody;
.source "CountingRequestBody.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/utils/CountingRequestBody$CountingSink;
    }
.end annotation


# instance fields
.field private final bytesWrittenSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final delegate:Lokhttp3/RequestBody;

.field private final estimatedContentLength:J

.field private final ignoreWriteToCount:I

.field private numWriteToCalls:I


# direct methods
.method public constructor <init>(Lokhttp3/RequestBody;I)V
    .locals 2

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/utils/CountingRequestBody;->delegate:Lokhttp3/RequestBody;

    iput p2, p0, Lcom/discord/restapi/utils/CountingRequestBody;->ignoreWriteToCount:I

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/restapi/utils/CountingRequestBody;->bytesWrittenSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p1}, Lokhttp3/RequestBody;->contentLength()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/discord/restapi/utils/CountingRequestBody;->estimatedContentLength:J

    return-void
.end method

.method public synthetic constructor <init>(Lokhttp3/RequestBody;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/restapi/utils/CountingRequestBody;-><init>(Lokhttp3/RequestBody;I)V

    return-void
.end method

.method public static final synthetic access$updateProgress(Lcom/discord/restapi/utils/CountingRequestBody;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/restapi/utils/CountingRequestBody;->updateProgress(J)V

    return-void
.end method

.method private final updateProgress(J)V
    .locals 2

    iget v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->numWriteToCalls:I

    iget v1, p0, Lcom/discord/restapi/utils/CountingRequestBody;->ignoreWriteToCount:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->bytesWrittenSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public contentLength()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public contentType()Lokhttp3/MediaType;
    .locals 1

    iget-object v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->delegate:Lokhttp3/RequestBody;

    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentType()Lokhttp3/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public final getBytesWrittenObservable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->bytesWrittenSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->H()Lrx/Observable;

    move-result-object v0

    const-string v1, "bytesWrittenSubject.dist\u2026().onBackpressureLatest()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getEstimatedContentLength()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->estimatedContentLength:J

    return-wide v0
.end method

.method public writeTo(Lokio/BufferedSink;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->numWriteToCalls:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->numWriteToCalls:I

    new-instance v0, Lcom/discord/restapi/utils/CountingRequestBody$CountingSink;

    new-instance v1, Lcom/discord/restapi/utils/CountingRequestBody$writeTo$countingSink$1;

    invoke-direct {v1, p0}, Lcom/discord/restapi/utils/CountingRequestBody$writeTo$countingSink$1;-><init>(Lcom/discord/restapi/utils/CountingRequestBody;)V

    invoke-direct {v0, p1, v1}, Lcom/discord/restapi/utils/CountingRequestBody$CountingSink;-><init>(Lc0/v;Lkotlin/jvm/functions/Function1;)V

    const-string p1, "$this$buffer"

    invoke-static {v0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lc0/q;

    invoke-direct {p1, v0}, Lc0/q;-><init>(Lc0/v;)V

    iget-object v0, p0, Lcom/discord/restapi/utils/CountingRequestBody;->delegate:Lokhttp3/RequestBody;

    invoke-virtual {v0, p1}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    invoke-virtual {p1}, Lc0/q;->flush()V

    return-void
.end method
