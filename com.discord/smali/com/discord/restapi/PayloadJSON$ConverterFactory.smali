.class public final Lcom/discord/restapi/PayloadJSON$ConverterFactory;
.super Lf0/h$a;
.source "PayloadJSON.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/PayloadJSON;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConverterFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;
    }
.end annotation


# instance fields
.field private final gsonConverterFactory:Lf0/h0/a/a;


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1

    const-string v0, "gson"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lf0/h$a;-><init>()V

    new-instance v0, Lf0/h0/a/a;

    invoke-direct {v0, p1}, Lf0/h0/a/a;-><init>(Lcom/google/gson/Gson;)V

    iput-object v0, p0, Lcom/discord/restapi/PayloadJSON$ConverterFactory;->gsonConverterFactory:Lf0/h0/a/a;

    return-void
.end method


# virtual methods
.method public requestBodyConverter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lf0/b0;)Lf0/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lf0/b0;",
            ")",
            "Lf0/h<",
            "*",
            "Lokhttp3/RequestBody;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parameterAnnotations"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "methodAnnotations"

    invoke-static {p3, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "retrofit"

    invoke-static {p4, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of p2, p1, Ljava/lang/reflect/ParameterizedType;

    const/4 p3, 0x0

    if-nez p2, :cond_0

    move-object p1, p3

    :cond_0
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object p2

    const-class p4, Lcom/discord/restapi/PayloadJSON;

    invoke-static {p2, p4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_1

    return-object p3

    :cond_1
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object p1

    const/4 p2, 0x0

    aget-object p1, p1, p2

    iget-object p2, p0, Lcom/discord/restapi/PayloadJSON$ConverterFactory;->gsonConverterFactory:Lf0/h0/a/a;

    iget-object p3, p2, Lf0/h0/a/a;->a:Lcom/google/gson/Gson;

    invoke-static {p1}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/google/gson/Gson;->g(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object p1

    new-instance p3, Lf0/h0/a/b;

    iget-object p2, p2, Lf0/h0/a/a;->a:Lcom/google/gson/Gson;

    invoke-direct {p3, p2, p1}, Lf0/h0/a/b;-><init>(Lcom/google/gson/Gson;Lcom/google/gson/TypeAdapter;)V

    new-instance p1, Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;

    invoke-direct {p1, p3}, Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;-><init>(Lf0/h;)V

    return-object p1

    :cond_2
    return-object p3
.end method
