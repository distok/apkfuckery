.class public final Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;
.super Ljava/lang/Object;
.source "PayloadJSON.kt"

# interfaces
.implements Lf0/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/PayloadJSON$ConverterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestBodyConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf0/h<",
        "Lcom/discord/restapi/PayloadJSON<",
        "TT;>;",
        "Lokhttp3/RequestBody;",
        ">;"
    }
.end annotation


# instance fields
.field private final gsonRequestBodyConverter:Lf0/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf0/h<",
            "TT;",
            "Lokhttp3/RequestBody;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf0/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/h<",
            "TT;",
            "Lokhttp3/RequestBody;",
            ">;)V"
        }
    .end annotation

    const-string v0, "gsonRequestBodyConverter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;->gsonRequestBodyConverter:Lf0/h;

    return-void
.end method


# virtual methods
.method public bridge synthetic convert(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/restapi/PayloadJSON;

    invoke-virtual {p0, p1}, Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;->convert(Lcom/discord/restapi/PayloadJSON;)Lokhttp3/RequestBody;

    move-result-object p1

    return-object p1
.end method

.method public convert(Lcom/discord/restapi/PayloadJSON;)Lokhttp3/RequestBody;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/restapi/PayloadJSON<",
            "TT;>;)",
            "Lokhttp3/RequestBody;"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/restapi/PayloadJSON$ConverterFactory$RequestBodyConverter;->gsonRequestBodyConverter:Lf0/h;

    invoke-static {p1}, Lcom/discord/restapi/PayloadJSON;->access$getData$p(Lcom/discord/restapi/PayloadJSON;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Lf0/h;->convert(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lokhttp3/RequestBody;

    return-object p1
.end method
