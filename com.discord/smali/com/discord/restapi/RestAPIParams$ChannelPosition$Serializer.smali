.class public final Lcom/discord/restapi/RestAPIParams$ChannelPosition$Serializer;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"

# interfaces
.implements Lf/h/d/s;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams$ChannelPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Serializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/d/s<",
        "Lcom/discord/restapi/RestAPIParams$ChannelPosition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serialize(Lcom/discord/restapi/RestAPIParams$ChannelPosition;Ljava/lang/reflect/Type;Lf/h/d/r;)Lcom/google/gson/JsonElement;
    .locals 4

    if-eqz p1, :cond_3

    const-class p3, Lcom/discord/restapi/RestAPIParams$ChannelPosition;

    invoke-static {p2, p3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    goto :goto_1

    :cond_0
    new-instance p2, Lcom/google/gson/JsonObject;

    invoke-direct {p2}, Lcom/google/gson/JsonObject;-><init>()V

    invoke-virtual {p1}, Lcom/discord/restapi/RestAPIParams$ChannelPosition;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    const-string v0, "id"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/JsonObject;->e(Ljava/lang/String;Ljava/lang/Number;)V

    invoke-virtual {p1}, Lcom/discord/restapi/RestAPIParams$ChannelPosition;->getPosition()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const-string v0, "position"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/JsonObject;->e(Ljava/lang/String;Ljava/lang/Number;)V

    invoke-virtual {p1}, Lcom/discord/restapi/RestAPIParams$ChannelPosition;->getParentId()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    const-string p1, "parent_id"

    cmp-long p3, v0, v2

    if-nez p3, :cond_1

    iget-object p3, p2, Lcom/google/gson/JsonObject;->a:Lcom/google/gson/internal/LinkedTreeMap;

    sget-object v0, Lf/h/d/p;->a:Lf/h/d/p;

    invoke-virtual {p3, p1, v0}, Lcom/google/gson/internal/LinkedTreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lcom/google/gson/JsonObject;->e(Ljava/lang/String;Ljava/lang/Number;)V

    :cond_2
    :goto_0
    return-object p2

    :cond_3
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lf/h/d/r;)Lcom/google/gson/JsonElement;
    .locals 0

    check-cast p1, Lcom/discord/restapi/RestAPIParams$ChannelPosition;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/restapi/RestAPIParams$ChannelPosition$Serializer;->serialize(Lcom/discord/restapi/RestAPIParams$ChannelPosition;Ljava/lang/reflect/Type;Lf/h/d/r;)Lcom/google/gson/JsonElement;

    move-result-object p1

    return-object p1
.end method
