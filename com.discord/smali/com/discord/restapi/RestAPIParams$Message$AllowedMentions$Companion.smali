.class public final Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions$Companion;
.super Ljava/lang/Object;
.source "RestAPIParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Lcom/discord/models/domain/ModelAllowedMentions;)Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;
    .locals 4

    const-string v0, "model"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelAllowedMentions;->getParse()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelAllowedMentions$Types;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelAllowedMentions$Types;->serialize()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelAllowedMentions;->getUsers()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelAllowedMentions;->getRoles()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelAllowedMentions;->getRepliedUser()Ljava/lang/Boolean;

    move-result-object p1

    new-instance v3, Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;

    invoke-direct {v3, v1, v0, v2, p1}, Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;)V

    return-object v3
.end method
