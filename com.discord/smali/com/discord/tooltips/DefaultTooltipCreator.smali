.class public final Lcom/discord/tooltips/DefaultTooltipCreator;
.super Ljava/lang/Object;
.source "DefaultTooltipCreator.kt"


# instance fields
.field public final a:Lcom/discord/tooltips/TooltipManager;


# direct methods
.method public constructor <init>(Lcom/discord/tooltips/TooltipManager;)V
    .locals 1

    const-string/jumbo v0, "tooltipManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/tooltips/DefaultTooltipCreator;->a:Lcom/discord/tooltips/TooltipManager;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/lang/String;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Lcom/discord/tooltips/TooltipManager$Tooltip;",
            "Lcom/discord/floating_view_manager/FloatingViewGravity;",
            "IIZ",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v4, p3

    move-object/from16 v5, p4

    const-string v2, "anchorView"

    move-object v3, p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tooltipText"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tooltip"

    invoke-static {p3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tooltipGravity"

    invoke-static {v5, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "componentPausedObservable"

    move-object/from16 v9, p8

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/tooltips/DefaultTooltipCreator;->a:Lcom/discord/tooltips/TooltipManager;

    move/from16 v8, p7

    invoke-virtual {v2, p3, v8}, Lcom/discord/tooltips/TooltipManager;->b(Lcom/discord/tooltips/TooltipManager$Tooltip;Z)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget-object v6, Lcom/discord/floating_view_manager/FloatingViewGravity;->TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;

    if-ne v5, v6, :cond_1

    const v6, 0x7f0d0037

    goto :goto_0

    :cond_1
    const v6, 0x7f0d0036

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v7

    const-string v10, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-static {v7, v10}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v7, Landroid/view/ViewGroup;

    const/4 v11, 0x0

    invoke-virtual {v2, v6, v7, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, v10}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v6, v2

    check-cast v6, Landroid/view/ViewGroup;

    const v2, 0x7f0a032d

    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const-string/jumbo v7, "tooltipTextView"

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/discord/tooltips/DefaultTooltipCreator$a;

    invoke-direct {v1, p0, p3}, Lcom/discord/tooltips/DefaultTooltipCreator$a;-><init>(Lcom/discord/tooltips/DefaultTooltipCreator;Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/discord/tooltips/DefaultTooltipCreator;->a:Lcom/discord/tooltips/TooltipManager;

    move-object v2, p1

    move-object v3, v6

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/discord/tooltips/TooltipManager;->d(Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V

    return-void
.end method
