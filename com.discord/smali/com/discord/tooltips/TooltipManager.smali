.class public Lcom/discord/tooltips/TooltipManager;
.super Ljava/lang/Object;
.source "TooltipManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/tooltips/TooltipManager$Tooltip;,
        Lcom/discord/tooltips/TooltipManager$a;
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/a/l/a;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:Lcom/discord/floating_view_manager/FloatingViewManager;


# direct methods
.method public constructor <init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x1

    :cond_0
    const-string p5, "acknowledgedTooltipsCache"

    invoke-static {p1, p5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p5, "shownTooltipNames"

    invoke-static {p2, p5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p5, "floatingViewManager"

    invoke-static {p4, p5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/tooltips/TooltipManager;->b:Lf/a/l/a;

    iput-object p2, p0, Lcom/discord/tooltips/TooltipManager;->c:Ljava/util/Set;

    iput p3, p0, Lcom/discord/tooltips/TooltipManager;->d:I

    iput-object p4, p0, Lcom/discord/tooltips/TooltipManager;->e:Lcom/discord/floating_view_manager/FloatingViewManager;

    new-instance p1, Lf/a/l/c;

    invoke-direct {p1, p0}, Lf/a/l/c;-><init>(Lcom/discord/tooltips/TooltipManager;)V

    iput-object p1, p4, Lcom/discord/floating_view_manager/FloatingViewManager;->a:Lkotlin/jvm/functions/Function1;

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/tooltips/TooltipManager;->a:Ljava/util/Map;

    return-void
.end method

.method public static synthetic e(Lcom/discord/tooltips/TooltipManager;Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;ILjava/lang/Object;)V
    .locals 10

    and-int/lit8 v0, p9, 0x8

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/floating_view_manager/FloatingViewGravity;->TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p4

    :goto_0
    and-int/lit8 v0, p9, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    move v6, p5

    :goto_1
    and-int/lit8 v0, p9, 0x20

    if-eqz v0, :cond_2

    const/4 v7, 0x0

    goto :goto_2

    :cond_2
    move/from16 v7, p6

    :goto_2
    and-int/lit8 v0, p9, 0x40

    if-eqz v0, :cond_3

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    move/from16 v8, p7

    :goto_3
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/discord/tooltips/TooltipManager;->d(Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/discord/tooltips/TooltipManager$Tooltip;)V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    invoke-virtual {p1}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getCacheKey()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/tooltips/TooltipManager;->b:Lf/a/l/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "tooltipCacheKey"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lf/a/l/a;->a:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/tooltips/TooltipManager;->b:Lf/a/l/a;

    const/4 v2, 0x1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lf/a/l/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public final b(Lcom/discord/tooltips/TooltipManager$Tooltip;Z)Z
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getCacheKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/discord/tooltips/TooltipManager;->b:Lf/a/l/a;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v3, "tooltipCacheKey"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v2, Lf/a/l/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v2, p0, Lcom/discord/tooltips/TooltipManager;->c:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getTooltipName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    iget-object v2, p0, Lcom/discord/tooltips/TooltipManager;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v0, :cond_2

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    iget p1, p0, Lcom/discord/tooltips/TooltipManager;->d:I

    if-ge v2, p1, :cond_2

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public final c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/tooltips/TooltipManager;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getTooltipName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/discord/tooltips/TooltipManager;->e:Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-virtual {v0, p1}, Lcom/discord/floating_view_manager/FloatingViewManager;->b(I)V

    :cond_0
    return-void
.end method

.method public final d(Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V
    .locals 10
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Lcom/discord/tooltips/TooltipManager$Tooltip;",
            "Lcom/discord/floating_view_manager/FloatingViewGravity;",
            "IIZ",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p3

    const-string v2, "anchorView"

    move-object v4, p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tooltipView"

    move-object v5, p2

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tooltip"

    invoke-static {p3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tooltipGravity"

    move-object v6, p4

    invoke-static {p4, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "componentPausedObservable"

    move-object/from16 v9, p8

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v2, p7

    invoke-virtual {p0, p3, v2}, Lcom/discord/tooltips/TooltipManager;->b(Lcom/discord/tooltips/TooltipManager$Tooltip;Z)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p3}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    iget-object v2, v0, Lcom/discord/tooltips/TooltipManager;->c:Ljava/util/Set;

    invoke-virtual {p3}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getTooltipName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lcom/discord/tooltips/TooltipManager;->a:Ljava/util/Map;

    invoke-virtual {p3}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getTooltipName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v0, Lcom/discord/tooltips/TooltipManager;->e:Lcom/discord/floating_view_manager/FloatingViewManager;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    move v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p8

    invoke-virtual/range {v3 .. v9}, Lcom/discord/floating_view_manager/FloatingViewManager;->c(Landroid/view/View;Landroid/view/View;Lcom/discord/floating_view_manager/FloatingViewGravity;IILrx/Observable;)V

    return-void
.end method
