.class public final Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;
.super Lx/m/c/k;
.source "StoreApplication.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;->invoke(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $results:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;->this$0:Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;

    iput-object p2, p0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;->$results:Ljava/util/List;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;->$results:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/models/domain/ModelApplication;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;->this$0:Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;

    iget-object v4, v4, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;->this$0:Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1;

    iget-wide v4, v4, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1;->$appId:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    check-cast v1, Lcom/discord/models/domain/ModelApplication;

    iget-object v0, p0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1$1;->this$0:Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1$1;->this$0:Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1;

    iget-object v2, v0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1;->this$0:Lcom/discord/stores/StoreApplication;

    iget-wide v3, v0, Lcom/discord/stores/StoreApplication$fetchIfNonexisting$1;->$appId:J

    invoke-static {v2, v3, v4, v1}, Lcom/discord/stores/StoreApplication;->access$handleFetchResult(Lcom/discord/stores/StoreApplication;JLcom/discord/models/domain/ModelApplication;)V

    return-void
.end method
