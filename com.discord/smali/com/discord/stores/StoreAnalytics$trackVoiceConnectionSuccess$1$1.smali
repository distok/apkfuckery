.class public final Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1$1;
.super Lx/m/c/k;
.source "StoreAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic this$0:Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1$1;->this$0:Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;

    iput-object p2, p0, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1$1;->invoke(Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;)V
    .locals 4

    const-string v0, "audioInfo"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v1, p0, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1$1;->this$0:Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;

    iget-object v2, v1, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;->$properties:Ljava/util/Map;

    iget-object v3, p0, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, v1, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v1}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getRtcRegion$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcRegion;->getPreferredRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, p1, v3, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->voiceConnectionSuccess(Ljava/util/Map;Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;Lcom/discord/models/domain/ModelChannel;Ljava/lang/String;)V

    return-void
.end method
