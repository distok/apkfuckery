.class public final Lcom/discord/stores/StoreVideoStreams$UserStreams;
.super Ljava/lang/Object;
.source "StoreVideoStreams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreVideoStreams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserStreams"
.end annotation


# instance fields
.field private final applicationStreamId:Ljava/lang/Integer;

.field private final callStreamId:Ljava/lang/Integer;

.field private final isEmpty:Z


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->isEmpty:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreVideoStreams$UserStreams;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/discord/stores/StoreVideoStreams$UserStreams;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->copy(Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/discord/stores/StoreVideoStreams$UserStreams;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/discord/stores/StoreVideoStreams$UserStreams;
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StoreVideoStreams$UserStreams;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplicationStreamId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getCallStreamId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->isEmpty:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "UserStreams(callStreamId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->callStreamId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", applicationStreamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVideoStreams$UserStreams;->applicationStreamId:Ljava/lang/Integer;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->w(Ljava/lang/StringBuilder;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
