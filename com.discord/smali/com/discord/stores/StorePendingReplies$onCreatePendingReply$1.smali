.class public final Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;
.super Lx/m/c/k;
.source "StorePendingReplies.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePendingReplies;->onCreatePendingReply(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelMessage;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $message:Lcom/discord/models/domain/ModelMessage;

.field public final synthetic $shouldMention:Z

.field public final synthetic $showMentionToggle:Z

.field public final synthetic this$0:Lcom/discord/stores/StorePendingReplies;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePendingReplies;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelMessage;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->this$0:Lcom/discord/stores/StorePendingReplies;

    iput-object p2, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p3, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$message:Lcom/discord/models/domain/ModelMessage;

    iput-boolean p4, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$shouldMention:Z

    iput-boolean p5, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$showMentionToggle:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->this$0:Lcom/discord/stores/StorePendingReplies;

    invoke-static {v1}, Lcom/discord/stores/StorePendingReplies;->access$getPendingReplies$p(Lcom/discord/stores/StorePendingReplies;)Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Lcom/discord/stores/StorePendingReplies$PendingReply;

    new-instance v4, Lcom/discord/models/domain/ModelMessage$MessageReference;

    iget-object v5, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$message:Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v4, v0, v5, v6}, Lcom/discord/models/domain/ModelMessage$MessageReference;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$message:Lcom/discord/models/domain/ModelMessage;

    iget-boolean v5, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$shouldMention:Z

    iget-boolean v6, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->$showMentionToggle:Z

    invoke-direct {v3, v4, v0, v5, v6}, Lcom/discord/stores/StorePendingReplies$PendingReply;-><init>(Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelMessage;ZZ)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;->this$0:Lcom/discord/stores/StorePendingReplies;

    invoke-virtual {v0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method
