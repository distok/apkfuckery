.class public final Lcom/discord/stores/StoreLurking$startLurkingInternal$2;
.super Lx/m/c/k;
.source "StoreLurking.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreLurking;->startLurkingInternal(JLjava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic $jumpToDestination:Lkotlin/jvm/functions/Function0;

.field public final synthetic this$0:Lcom/discord/stores/StoreLurking;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreLurking;Lkotlin/jvm/functions/Function0;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->this$0:Lcom/discord/stores/StoreLurking;

    iput-object p2, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->$jumpToDestination:Lkotlin/jvm/functions/Function0;

    iput-wide p3, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->$guildId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->invoke(Lcom/discord/models/domain/ModelGuild;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelGuild;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->$jumpToDestination:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreLurking;->isLurking$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelGuild;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0}, Lcom/discord/stores/StoreLurking;->getLurkingGuildIds$app_productionDiscordExternalRelease()Ljava/util/ArrayList;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->$guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-static {v0}, Lcom/discord/stores/StoreLurking;->access$getGuildIdSubject$p(Lcom/discord/stores/StoreLurking;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v1}, Lcom/discord/stores/StoreLurking;->getLurkingGuildIds$app_productionDiscordExternalRelease()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-static {v0}, Lcom/discord/stores/StoreLurking;->access$getStream$p(Lcom/discord/stores/StoreLurking;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->$guildId:J

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getWelcomeScreen()Lcom/discord/models/domain/ModelGuildWelcomeScreen;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/stores/StoreStream;->handleGuildJoined(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p1

    const-string v0, "LURK:"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$2;->$guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreLurking$startLurkingInternal$2$1;->INSTANCE:Lcom/discord/stores/StoreLurking$startLurkingInternal$2$1;

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreNavigation;->launchNotice(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
