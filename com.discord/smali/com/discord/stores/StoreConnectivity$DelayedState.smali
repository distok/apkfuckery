.class public final Lcom/discord/stores/StoreConnectivity$DelayedState;
.super Ljava/lang/Object;
.source "StoreConnectivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreConnectivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DelayedState"
.end annotation


# instance fields
.field private final delay:J

.field private final state:Lcom/discord/stores/StoreConnectivity$State;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreConnectivity$State;J)V
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    iput-wide p2, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreConnectivity$State;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    invoke-static {}, Lcom/discord/stores/StoreConnectivity;->access$Companion()Lcom/discord/stores/StoreConnectivity$Companion;

    const-wide/16 p2, 0x3e8

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreConnectivity$DelayedState;-><init>(Lcom/discord/stores/StoreConnectivity$State;J)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/stores/StoreConnectivity$State;JILjava/lang/Object;)Lcom/discord/stores/StoreConnectivity$DelayedState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-wide p2, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreConnectivity$DelayedState;->copy(Lcom/discord/stores/StoreConnectivity$State;J)Lcom/discord/stores/StoreConnectivity$DelayedState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreConnectivity$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    return-wide v0
.end method

.method public final copy(Lcom/discord/stores/StoreConnectivity$State;J)Lcom/discord/stores/StoreConnectivity$DelayedState;
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreConnectivity$DelayedState;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/stores/StoreConnectivity$DelayedState;-><init>(Lcom/discord/stores/StoreConnectivity$State;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreConnectivity$DelayedState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreConnectivity$DelayedState;

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    iget-object v1, p1, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    iget-wide v2, p1, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDelay()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    return-wide v0
.end method

.method public final getState()Lcom/discord/stores/StoreConnectivity$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "DelayedState(state="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->state:Lcom/discord/stores/StoreConnectivity$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/stores/StoreConnectivity$DelayedState;->delay:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
