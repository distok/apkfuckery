.class public final Lcom/discord/stores/StoreChangeLog;
.super Lcom/discord/stores/Store;
.source "StoreChangeLog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreChangeLog$StorePayload;
    }
.end annotation


# instance fields
.field public app:Landroid/app/Application;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final notices:Lcom/discord/stores/StoreNotices;

.field private final users:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreNotices;Lcom/discord/stores/StoreUser;)V
    .locals 1

    const-string v0, "clock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notices"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "users"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChangeLog;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p2, p0, Lcom/discord/stores/StoreChangeLog;->notices:Lcom/discord/stores/StoreNotices;

    iput-object p3, p0, Lcom/discord/stores/StoreChangeLog;->users:Lcom/discord/stores/StoreUser;

    return-void
.end method

.method public static final synthetic access$createChangeLogNotice(Lcom/discord/stores/StoreChangeLog;)Lcom/discord/stores/StoreNotices$Notice;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreChangeLog;->createChangeLogNotice()Lcom/discord/stores/StoreNotices$Notice;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getNotices$p(Lcom/discord/stores/StoreChangeLog;)Lcom/discord/stores/StoreNotices;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChangeLog;->notices:Lcom/discord/stores/StoreNotices;

    return-object p0
.end method

.method public static final synthetic access$shouldShowChangelog(Lcom/discord/stores/StoreChangeLog;Landroid/content/Context;JLjava/lang/String;Lcom/discord/models/experiments/domain/Experiment;)Z
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/stores/StoreChangeLog;->shouldShowChangelog(Landroid/content/Context;JLjava/lang/String;Lcom/discord/models/experiments/domain/Experiment;)Z

    move-result p0

    return p0
.end method

.method private final createChangeLogNotice()Lcom/discord/stores/StoreNotices$Notice;
    .locals 17

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    new-instance v12, Lcom/discord/stores/StoreChangeLog$createChangeLogNotice$1;

    move-object/from16 v14, p0

    invoke-direct {v12, v14}, Lcom/discord/stores/StoreChangeLog$createChangeLogNotice$1;-><init>(Lcom/discord/stores/StoreChangeLog;)V

    const-string v1, "CHANGE_LOG"

    const/4 v2, 0x0

    const-wide/16 v3, 0x539

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/32 v10, 0x240c8400

    const/16 v13, 0x62

    const/16 v16, 0x0

    move-object v0, v15

    move-object/from16 v14, v16

    invoke-direct/range {v0 .. v14}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v15
.end method

.method private final getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "string"

    invoke-virtual {v0, p2, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    if-lez p2, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string p1, "context.getString(id)"

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-object p3
.end method

.method private final getLastSeenChangeLogVersion()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CACHE_KEY_VIEWED_CHANGE_LOG_VERSION"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final isTooYoung(J)Z
    .locals 5

    const/16 v0, 0x16

    ushr-long/2addr p1, v0

    const-wide v0, 0x14aa2cab000L

    add-long/2addr p1, v0

    const-wide/32 v0, 0x19bfcc00

    add-long/2addr p1, v0

    iget-object v2, p0, Lcom/discord/stores/StoreChangeLog;->notices:Lcom/discord/stores/StoreNotices;

    invoke-virtual {v2}, Lcom/discord/stores/StoreNotices;->getFirstUseTimestamp()J

    move-result-wide v2

    add-long/2addr v2, v0

    iget-object v0, p0, Lcom/discord/stores/StoreChangeLog;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v4, v0, p1

    if-ltz v4, :cond_1

    cmp-long p1, v0, v2

    if-gez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public static synthetic openChangeLog$default(Lcom/discord/stores/StoreChangeLog;Landroid/content/Context;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreChangeLog;->openChangeLog(Landroid/content/Context;Z)V

    return-void
.end method

.method private final setLastSeenChangeLogVersion(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreChangeLog$lastSeenChangeLogVersion$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreChangeLog$lastSeenChangeLogVersion$1;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final shouldShowChangelog(Landroid/content/Context;JLjava/lang/String;Lcom/discord/models/experiments/domain/Experiment;)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "change_log_md_experiment_body"

    const-string/jumbo v4, "string"

    invoke-virtual {v1, v3, v4, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_0
    const v1, 0x7f12041b

    :goto_0
    const-string v2, "en"

    invoke-static {p1, v1, v2}, Lcom/discord/utilities/string/StringUtilsKt;->getStringByLocale(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v1, p4}, Lcom/discord/utilities/string/StringUtilsKt;->getStringByLocale(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p4, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p4

    xor-int/2addr p4, v0

    const/4 v2, 0x0

    if-eqz p4, :cond_1

    invoke-static {v3, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_1

    return v2

    :cond_1
    const-string p4, "context.getString(R.string.change_log_md_date)"

    const v1, 0x7f12041c

    if-eqz p5, :cond_2

    invoke-virtual {p5}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result p5

    if-ne p5, v0, :cond_2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p5

    invoke-static {p5, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "change_log_md_experiment_date"

    invoke-direct {p0, p1, p4, p5}, Lcom/discord/stores/StoreChangeLog;->getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0}, Lcom/discord/stores/StoreChangeLog;->getLastSeenChangeLogVersion()Ljava/lang/String;

    move-result-object p4

    if-eqz p4, :cond_4

    invoke-static {p4}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_3

    goto :goto_2

    :cond_3
    const/4 p4, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 p4, 0x1

    :goto_3
    if-nez p4, :cond_6

    invoke-direct {p0, p2, p3}, Lcom/discord/stores/StoreChangeLog;->isTooYoung(J)Z

    move-result p2

    if-eqz p2, :cond_5

    goto :goto_4

    :cond_5
    invoke-direct {p0}, Lcom/discord/stores/StoreChangeLog;->getLastSeenChangeLogVersion()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v0

    return p1

    :cond_6
    :goto_4
    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreChangeLog;->markSeen(Ljava/lang/String;)V

    return v2
.end method


# virtual methods
.method public final getApp()Landroid/app/Application;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreChangeLog;->app:Landroid/app/Application;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "app"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 11

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getUserSettings()Lcom/discord/models/domain/ModelUserSettings;

    move-result-object p1

    const-string v0, "payload.userSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getLocale()Ljava/lang/String;

    move-result-object p1

    const-string v0, "payload.userSettings.locale"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "-"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-static {p1, v0, v1, v1, v2}, Lx/s/r;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZII)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/discord/stores/StoreChangeLog;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;->INSTANCE:Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    const-string/jumbo v0, "users\n        .observeMe\u2026serId = userId)\n        }"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreChangeLog;

    new-instance v8, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;-><init>(Lcom/discord/stores/StoreChangeLog;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final init(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "app"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/discord/stores/StoreChangeLog;->app:Landroid/app/Application;

    return-void
.end method

.method public final markSeen(Ljava/lang/String;)V
    .locals 7

    const-string v0, "currentVersion"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreChangeLog;->setLastSeenChangeLogVersion(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/stores/StoreChangeLog;->notices:Lcom/discord/stores/StoreNotices;

    const-string v2, "CHANGE_LOG"

    const-wide/16 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/stores/StoreNotices;->markSeen$default(Lcom/discord/stores/StoreNotices;Ljava/lang/String;JILjava/lang/Object;)V

    return-void
.end method

.method public final openChangeLog(Landroid/content/Context;Z)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    const-string v1, "context"

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    const-string v3, "2020-10_stickers_user_android"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v1

    const v3, 0x7f12041c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "context.getString(R.string.change_log_md_date)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "change_log_md_experiment_date"

    invoke-direct {v0, v2, v7, v5}, Lcom/discord/stores/StoreChangeLog;->getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const v7, 0x7f120425

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "context.getString(R.string.change_log_md_revision)"

    invoke-static {v8, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "change_log_md_revision"

    invoke-direct {v0, v2, v10, v8}, Lcom/discord/stores/StoreChangeLog;->getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f120426

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "context.getString(R.string.change_log_md_video)"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "change_log_md_experiment_video"

    invoke-direct {v0, v2, v13, v11}, Lcom/discord/stores/StoreChangeLog;->getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const v13, 0x7f12041b

    invoke-virtual {v2, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "context.getString(R.string.change_log_md_body)"

    invoke-static {v14, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "change_log_md_experiment_body"

    invoke-direct {v0, v2, v13, v14}, Lcom/discord/stores/StoreChangeLog;->getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const v14, 0x7f120297

    invoke-virtual {v2, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v10, "context.getString(R.string.back)"

    invoke-static {v14, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "change_log_md_experiment_template"

    invoke-direct {v0, v2, v10, v14}, Lcom/discord/stores/StoreChangeLog;->getChangelogExperimentString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz p2, :cond_0

    sget-object v14, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion$ExitStyle;->BACK:Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion$ExitStyle;

    goto :goto_0

    :cond_0
    sget-object v14, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion$ExitStyle;->CLOSE:Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion$ExitStyle;

    :goto_0
    const-string/jumbo v7, "special"

    invoke-static {v10, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v1

    if-ne v1, v4, :cond_1

    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLogSpecial;->Companion:Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion;

    const/4 v9, 0x1

    move-object/from16 v2, p1

    move-object v3, v5

    move-object v4, v8

    move-object v5, v11

    move-object v6, v13

    move-object v7, v14

    move v8, v9

    invoke-virtual/range {v1 .. v8}, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion;->launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/widgets/settings/WidgetChangeLogSpecial$Companion$ExitStyle;Z)V

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLog;->Companion:Lcom/discord/widgets/settings/WidgetChangeLog$Companion;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f120425

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f120426

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f12041b

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/widgets/settings/WidgetChangeLog$Companion;->launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public final setApp(Landroid/app/Application;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreChangeLog;->app:Landroid/app/Application;

    return-void
.end method
