.class public final Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;
.super Lx/m/c/k;
.source "StoreMessagesLoader.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages(JZZZLjava/lang/Long;Ljava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/domain/ModelMessage;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId$inlined:J

.field public final synthetic $channelLoadedState$inlined:Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

.field public final synthetic $loadPagedMessages$4$inlined:Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;

.field public final synthetic $shouldRequestNewerMessages:Z

.field public final synthetic $shouldRequestOlderMessages:Z

.field public final synthetic this$0:Lcom/discord/stores/StoreMessagesLoader;


# direct methods
.method public constructor <init>(ZZLcom/discord/stores/StoreMessagesLoader;Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;JLcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$shouldRequestOlderMessages:Z

    iput-boolean p2, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$shouldRequestNewerMessages:Z

    iput-object p3, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->this$0:Lcom/discord/stores/StoreMessagesLoader;

    iput-object p4, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$channelLoadedState$inlined:Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

    iput-wide p5, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$channelId$inlined:J

    iput-object p7, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$loadPagedMessages$4$inlined:Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$loadPagedMessages$4$inlined:Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;

    iget-wide v1, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$channelId$inlined:J

    const-string v3, "messages"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$shouldRequestOlderMessages:Z

    iget-boolean v5, p0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;->$shouldRequestNewerMessages:Z

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;->invoke(JLjava/util/List;ZZ)V

    return-void
.end method
