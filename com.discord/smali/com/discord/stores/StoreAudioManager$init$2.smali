.class public final Lcom/discord/stores/StoreAudioManager$init$2;
.super Lx/m/c/k;
.source "StoreAudioManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAudioManager;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $audioManager:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAudioManager$init$2;->$audioManager:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreAudioManager$init$2;->invoke(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAudioManager$init$2;->$audioManager:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;

    const-string v1, "it"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->configure(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;)V

    return-void
.end method
