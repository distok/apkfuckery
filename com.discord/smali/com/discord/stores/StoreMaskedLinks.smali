.class public final Lcom/discord/stores/StoreMaskedLinks;
.super Lcom/discord/stores/Store;
.source "StoreMaskedLinks.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreMaskedLinks$Companion;
    }
.end annotation


# static fields
.field private static final Companion:Lcom/discord/stores/StoreMaskedLinks$Companion;

.field private static final HOST_SPOTIFY:Ljava/lang/String; = "https://spotify.com"

.field private static final HOST_SPOTIFY_OPEN:Ljava/lang/String; = "https://open.spotify.com"

.field private static final TRUSTED_DOMAINS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final USER_TRUSTED_DOMAINS_CACHE_KEY:Ljava/lang/String; = "USER_TRUSTED_DOMAINS_CACHE_KEY"


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private final storeStream:Lcom/discord/stores/StoreStream;

.field private userTrustedDomains:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private userTrustedDomainsCopy:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/discord/stores/StoreMaskedLinks$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMaskedLinks$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMaskedLinks;->Companion:Lcom/discord/stores/StoreMaskedLinks$Companion;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "https://discord.com"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "https://discordapp.com"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "https://cdn.discordapp.com"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "https://discord.gift"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string v2, "https://discord.gg"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-string v2, "https://discord.new"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const-string v2, "https://spotify.com"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v1, v3

    const-string v2, "https://open.spotify.com"

    invoke-static {v0, v2}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x7

    aput-object v0, v1, v2

    invoke-static {v1}, Lx/h/f;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/stores/StoreMaskedLinks;->TRUSTED_DOMAINS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreStream;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStream"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreMaskedLinks;->storeStream:Lcom/discord/stores/StoreStream;

    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomains:Ljava/util/Set;

    sget-object p1, Lx/h/n;->d:Lx/h/n;

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomainsCopy:Ljava/util/Set;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/discord/stores/StoreMaskedLinks$Companion;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreMaskedLinks;->Companion:Lcom/discord/stores/StoreMaskedLinks$Companion;

    return-object v0
.end method

.method public static final synthetic access$getStoreStream$p(Lcom/discord/stores/StoreMaskedLinks;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMaskedLinks;->storeStream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$getUserTrustedDomains$p(Lcom/discord/stores/StoreMaskedLinks;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomains:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic access$getUserTrustedDomainsCopy$p(Lcom/discord/stores/StoreMaskedLinks;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomainsCopy:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic access$isDirty$p(Lcom/discord/stores/StoreMaskedLinks;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreMaskedLinks;->isDirty:Z

    return p0
.end method

.method public static final synthetic access$setDirty$p(Lcom/discord/stores/StoreMaskedLinks;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreMaskedLinks;->isDirty:Z

    return-void
.end method

.method public static final synthetic access$setUserTrustedDomains$p(Lcom/discord/stores/StoreMaskedLinks;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomains:Ljava/util/Set;

    return-void
.end method

.method public static final synthetic access$setUserTrustedDomainsCopy$p(Lcom/discord/stores/StoreMaskedLinks;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomainsCopy:Ljava/util/Set;

    return-void
.end method

.method private final isTrustedDomain(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreMaskedLinks;->Companion:Lcom/discord/stores/StoreMaskedLinks$Companion;

    invoke-static {v0, p1}, Lcom/discord/stores/StoreMaskedLinks$Companion;->access$getDomainName(Lcom/discord/stores/StoreMaskedLinks$Companion;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreMaskedLinks;->TRUSTED_DOMAINS:Ljava/util/List;

    invoke-static {v0, p1}, Lx/h/f;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomainsCopy:Ljava/util/Set;

    invoke-static {v0, p1}, Lx/h/f;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    new-instance p1, Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lx/h/n;->d:Lx/h/n;

    const-string v2, "USER_TRUSTED_DOMAINS_CACHE_KEY"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    :goto_0
    invoke-direct {p1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomains:Ljava/util/Set;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreMaskedLinks;->isDirty:Z

    return-void
.end method

.method public final observeIsTrustedDomain(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "url"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMaskedLinks;->isTrustedDomain(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    new-instance p2, Lg0/l/e/j;

    invoke-direct {p2, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(true)"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p2

    :cond_0
    if-eqz p2, :cond_2

    invoke-static {p2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iget-object p2, p0, Lcom/discord/stores/StoreMaskedLinks;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream;->getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;-><init>(Lcom/discord/stores/StoreMaskedLinks;Z)V

    invoke-virtual {p2, v0}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo p2, "storeStream\n          .c\u2026            }\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onDispatchEnded()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/stores/StoreMaskedLinks;->isDirty:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreMaskedLinks;->isDirty:Z

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomains:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreMaskedLinks;->userTrustedDomainsCopy:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreMaskedLinks$onDispatchEnded$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreMaskedLinks$onDispatchEnded$1;-><init>(Lcom/discord/stores/StoreMaskedLinks;)V

    invoke-static {v0, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    :cond_0
    return-void
.end method

.method public final trustDomain(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "url"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMaskedLinks;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreMaskedLinks$trustDomain$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreMaskedLinks$trustDomain$1;-><init>(Lcom/discord/stores/StoreMaskedLinks;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
