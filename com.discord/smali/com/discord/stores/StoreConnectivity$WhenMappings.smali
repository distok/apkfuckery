.class public final synthetic Lcom/discord/stores/StoreConnectivity$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/discord/stores/StoreConnectivity$State;->values()[Lcom/discord/stores/StoreConnectivity$State;

    const/4 v0, 0x4

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/stores/StoreConnectivity$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/stores/StoreConnectivity$State;->ONLINE:Lcom/discord/stores/StoreConnectivity$State;

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v2, Lcom/discord/stores/StoreConnectivity$State;->OFFLINE:Lcom/discord/stores/StoreConnectivity$State;

    const/4 v2, 0x2

    aput v2, v1, v3

    sget-object v3, Lcom/discord/stores/StoreConnectivity$State;->OFFLINE_AIRPLANE_MODE:Lcom/discord/stores/StoreConnectivity$State;

    const/4 v3, 0x3

    aput v3, v1, v2

    sget-object v2, Lcom/discord/stores/StoreConnectivity$State;->CONNECTING:Lcom/discord/stores/StoreConnectivity$State;

    aput v0, v1, v3

    return-void
.end method
