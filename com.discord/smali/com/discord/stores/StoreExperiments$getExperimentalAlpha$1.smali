.class public final Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;
.super Lx/m/c/k;
.source "StoreExperiments.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreExperiments;->getExperimentalAlpha()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreExperiments;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExperiments;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;->this$0:Lcom/discord/stores/StoreExperiments;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;->invoke()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Z
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;->this$0:Lcom/discord/stores/StoreExperiments;

    invoke-static {v0}, Lcom/discord/stores/StoreExperiments;->access$getStoreUser$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;->this$0:Lcom/discord/stores/StoreExperiments;

    invoke-static {v1}, Lcom/discord/stores/StoreExperiments;->access$getStoreGuilds$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuilds;->getGuilds()Ljava/util/Map;

    move-result-object v1

    const-wide v2, 0x2bc056ab0800006L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuild;

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isStaff()Z

    move-result v0

    if-eq v0, v2, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_0
    return v2
.end method
