.class public final Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;
.super Lx/m/c/k;
.source "StoreApplicationStreamPreviews.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreApplicationStreamPreviews;->handleFetchFailed(Ljava/lang/String;Lcom/discord/utilities/error/Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lrx/Subscription;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $streamKey:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreApplicationStreamPreviews;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;->this$0:Lcom/discord/stores/StoreApplicationStreamPreviews;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;->$streamKey:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lrx/Subscription;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;->invoke(Lrx/Subscription;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lrx/Subscription;)V
    .locals 2

    const-string/jumbo v0, "subscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;->this$0:Lcom/discord/stores/StoreApplicationStreamPreviews;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationStreamPreviews;->access$getFetchStreamPreviewSubscriptions$p(Lcom/discord/stores/StoreApplicationStreamPreviews;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;->$streamKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
