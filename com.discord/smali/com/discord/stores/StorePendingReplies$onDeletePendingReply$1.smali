.class public final Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;
.super Lx/m/c/k;
.source "StorePendingReplies.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePendingReplies;->onDeletePendingReply(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic this$0:Lcom/discord/stores/StorePendingReplies;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePendingReplies;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;->this$0:Lcom/discord/stores/StorePendingReplies;

    iput-wide p2, p0, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;->$channelId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;->this$0:Lcom/discord/stores/StorePendingReplies;

    invoke-static {v0}, Lcom/discord/stores/StorePendingReplies;->access$getPendingReplies$p(Lcom/discord/stores/StorePendingReplies;)Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;->$channelId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;->this$0:Lcom/discord/stores/StorePendingReplies;

    invoke-virtual {v0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method
