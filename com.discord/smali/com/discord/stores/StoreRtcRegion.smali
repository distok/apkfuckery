.class public final Lcom/discord/stores/StoreRtcRegion;
.super Ljava/lang/Object;
.source "StoreRtcRegion.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreRtcRegion$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreRtcRegion$Companion;

.field private static final LATENCY_TEST_CACHE_TTL_MS:I = 0x5265c00

.field private static final MAX_LATENCY_TEST_CONN_OPEN_JITTER_MS:I = 0x7530

.field private static final MIN_LATENCY_TEST_CONN_OPEN_JITTER_MS:I = 0x3e8


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

.field private final lastTestResultCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Lcom/discord/stores/RtcLatencyTestResult;",
            ">;"
        }
    .end annotation
.end field

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeExperiments:Lcom/discord/stores/StoreExperiments;

.field private final storeMediaEngine:Lcom/discord/stores/StoreMediaEngine;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreRtcRegion$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreRtcRegion$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreRtcRegion;->Companion:Lcom/discord/stores/StoreRtcRegion$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 7

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeMediaEngine"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeExperiments"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcRegion;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreRtcRegion;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p3, p0, Lcom/discord/stores/StoreRtcRegion;->storeMediaEngine:Lcom/discord/stores/StoreMediaEngine;

    iput-object p4, p0, Lcom/discord/stores/StoreRtcRegion;->storeExperiments:Lcom/discord/stores/StoreExperiments;

    iput-object p5, p0, Lcom/discord/stores/StoreRtcRegion;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    new-instance p1, Lcom/discord/utilities/persister/Persister;

    new-instance p2, Lcom/discord/stores/RtcLatencyTestResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/RtcLatencyTestResult;-><init>(Ljava/util/List;Ljava/util/List;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const-string p3, "CACHE_KEY_LATENCY_TEST_RESULT"

    invoke-direct {p1, p3, p2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResultCache:Lcom/discord/utilities/persister/Persister;

    new-instance p1, Lcom/discord/stores/RtcLatencyTestResult;

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/RtcLatencyTestResult;-><init>(Ljava/util/List;Ljava/util/List;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    sget-object p5, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p5}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p5

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreRtcRegion;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method public static final synthetic access$fetchRtcLatencyTestRegionsIps(Lcom/discord/stores/StoreRtcRegion;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcRegion;->fetchRtcLatencyTestRegionsIps()V

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/stores/StoreRtcRegion;)Lcom/discord/utilities/time/Clock;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreRtcRegion;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$getRestAPI$p(Lcom/discord/stores/StoreRtcRegion;)Lcom/discord/utilities/rest/RestAPI;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreRtcRegion;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object p0
.end method

.method public static final synthetic access$getStoreMediaEngine$p(Lcom/discord/stores/StoreRtcRegion;)Lcom/discord/stores/StoreMediaEngine;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreRtcRegion;->storeMediaEngine:Lcom/discord/stores/StoreMediaEngine;

    return-object p0
.end method

.method public static final synthetic access$maybePerformLatencyTest(Lcom/discord/stores/StoreRtcRegion;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcRegion;->maybePerformLatencyTest(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$shouldPerformLatencyTest(Lcom/discord/stores/StoreRtcRegion;Ljava/util/List;J)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreRtcRegion;->shouldPerformLatencyTest(Ljava/util/List;J)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$updateLastTestResult(Lcom/discord/stores/StoreRtcRegion;Lcom/discord/stores/RtcLatencyTestResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcRegion;->updateLastTestResult(Lcom/discord/stores/RtcLatencyTestResult;)V

    return-void
.end method

.method private final areStringListsEqual(Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_1

    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v3
.end method

.method private final fetchRtcLatencyTestRegionsIps()V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->storeMediaEngine:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->storeExperiments:Lcom/discord/stores/StoreExperiments;

    const-string v1, "2020-08_rtc_latency_test_experiment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "filter { it != null }.map { it!! }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$1;->INSTANCE:Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$2;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$2;-><init>(Lcom/discord/stores/StoreRtcRegion;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    const-string/jumbo v0, "storeExperiments.observe\u2026            }\n          }"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreRtcRegion;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$3;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$3;-><init>(Lcom/discord/stores/StoreRtcRegion;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final maybePerformLatencyTest(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelRtcLatencyRegion;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;-><init>(Lcom/discord/stores/StoreRtcRegion;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final shouldPerformLatencyTest(Ljava/util/List;J)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;J)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    invoke-virtual {v0}, Lcom/discord/stores/RtcLatencyTestResult;->getLatencyRankedRegions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    invoke-virtual {v0}, Lcom/discord/stores/RtcLatencyTestResult;->getGeoRankedRegions()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/discord/stores/StoreRtcRegion;->areStringListsEqual(Ljava/util/List;Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    invoke-virtual {p1}, Lcom/discord/stores/RtcLatencyTestResult;->getLastTestTimestampMs()J

    move-result-wide v0

    sub-long/2addr p2, v0

    const p1, 0x5265c00

    int-to-long v0, p1

    cmp-long p1, p2, v0

    if-ltz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final updateLastTestResult(Lcom/discord/stores/RtcLatencyTestResult;)V
    .locals 2

    iput-object p1, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResultCache:Lcom/discord/utilities/persister/Persister;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getPreferredRegion()Ljava/lang/String;
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    invoke-virtual {v0}, Lcom/discord/stores/RtcLatencyTestResult;->getLatencyRankedRegions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    invoke-virtual {v0}, Lcom/discord/stores/RtcLatencyTestResult;->getLatencyRankedRegions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final handleConnectionOpen$app_productionDiscordExternalRelease()V
    .locals 12
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0x7148

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v3

    const-string v0, "Observable.timer(fetchJi\u2026), TimeUnit.MILLISECONDS)"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v4, Lcom/discord/stores/StoreRtcRegion;

    new-instance v9, Lcom/discord/stores/StoreRtcRegion$handleConnectionOpen$1;

    invoke-direct {v9, p0}, Lcom/discord/stores/StoreRtcRegion$handleConnectionOpen$1;-><init>(Lcom/discord/stores/StoreRtcRegion;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final init()V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResultCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/RtcLatencyTestResult;

    iput-object v0, p0, Lcom/discord/stores/StoreRtcRegion;->lastTestResult:Lcom/discord/stores/RtcLatencyTestResult;

    return-void
.end method
