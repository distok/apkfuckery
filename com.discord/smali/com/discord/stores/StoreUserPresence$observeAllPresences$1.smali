.class public final Lcom/discord/stores/StoreUserPresence$observeAllPresences$1;
.super Lx/m/c/k;
.source "StoreUserPresence.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUserPresence;->observeAllPresences()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelPresence;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreUserPresence;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUserPresence;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence$observeAllPresences$1;->this$0:Lcom/discord/stores/StoreUserPresence;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserPresence$observeAllPresences$1;->invoke()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence$observeAllPresences$1;->this$0:Lcom/discord/stores/StoreUserPresence;

    invoke-static {v0}, Lcom/discord/stores/StoreUserPresence;->access$getPresencesSnapshot$p(Lcom/discord/stores/StoreUserPresence;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
