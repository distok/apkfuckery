.class public final Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$1;
.super Lx/m/c/k;
.source "StoreEmoji.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreEmoji;->buildUsableEmojiSet(Ljava/util/Map;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/List;ZZZZ)Lcom/discord/models/domain/emoji/EmojiSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreEmoji$EmojiContext;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$1;->$emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$1;->invoke(J)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(J)Z
    .locals 5

    iget-object v0, p0, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$1;->$emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    instance-of v1, v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;

    invoke-virtual {v0}, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;->getGuildId()J

    move-result-wide v0

    cmp-long v4, v0, p1

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Global;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    instance-of v1, v0, Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;

    invoke-virtual {v0}, Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;->getGuildId()J

    move-result-wide v0

    cmp-long v4, v0, p1

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    return v2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
