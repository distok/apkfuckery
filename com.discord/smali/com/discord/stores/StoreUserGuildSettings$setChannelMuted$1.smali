.class public final Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;
.super Lx/m/c/k;
.source "StoreUserGuildSettings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUserGuildSettings;->setChannelMuted(Landroid/content/Context;JZLcom/discord/models/domain/ModelMuteConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $muteConfig:Lcom/discord/models/domain/ModelMuteConfig;

.field public final synthetic $muted:Z

.field public final synthetic this$0:Lcom/discord/stores/StoreUserGuildSettings;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUserGuildSettings;JLandroid/content/Context;ZLcom/discord/models/domain/ModelMuteConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->this$0:Lcom/discord/stores/StoreUserGuildSettings;

    iput-wide p2, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$channelId:J

    iput-object p4, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$context:Landroid/content/Context;

    iput-boolean p5, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$muted:Z

    iput-object p6, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$muteConfig:Lcom/discord/models/domain/ModelMuteConfig;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 15

    iget-object v0, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->this$0:Lcom/discord/stores/StoreUserGuildSettings;

    invoke-static {v0}, Lcom/discord/stores/StoreUserGuildSettings;->access$getStoreChannels$p(Lcom/discord/stores/StoreUserGuildSettings;)Lcom/discord/stores/StoreChannels;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$channelId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f120468

    const v7, 0x7f120468

    goto :goto_0

    :cond_0
    const v1, 0x7f12040b

    const v7, 0x7f12040b

    :goto_0
    iget-object v2, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->this$0:Lcom/discord/stores/StoreUserGuildSettings;

    iget-object v3, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$context:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v4, "channel.guildId"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v6, Lcom/discord/restapi/RestAPIParams$UserGuildSettings;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    new-instance v14, Lcom/discord/restapi/RestAPIParams$UserGuildSettings$ChannelOverride;

    iget-boolean v8, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$muted:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iget-object v10, p0, Lcom/discord/stores/StoreUserGuildSettings$setChannelMuted$1;->$muteConfig:Lcom/discord/models/domain/ModelMuteConfig;

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v8, v14

    invoke-direct/range {v8 .. v13}, Lcom/discord/restapi/RestAPIParams$UserGuildSettings$ChannelOverride;-><init>(Ljava/lang/Boolean;Lcom/discord/models/domain/ModelMuteConfig;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v6, v0, v1, v14}, Lcom/discord/restapi/RestAPIParams$UserGuildSettings;-><init>(JLcom/discord/restapi/RestAPIParams$UserGuildSettings$ChannelOverride;)V

    invoke-static/range {v2 .. v7}, Lcom/discord/stores/StoreUserGuildSettings;->access$updateUserGuildSettings(Lcom/discord/stores/StoreUserGuildSettings;Landroid/content/Context;JLcom/discord/restapi/RestAPIParams$UserGuildSettings;I)V

    :cond_1
    return-void
.end method
