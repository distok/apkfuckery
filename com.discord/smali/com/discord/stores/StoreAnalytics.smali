.class public final Lcom/discord/stores/StoreAnalytics;
.super Lcom/discord/stores/Store;
.source "StoreAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreAnalytics$ScreenViewed;
    }
.end annotation


# instance fields
.field private analyticsToken:Ljava/lang/String;

.field private authToken:Ljava/lang/String;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private fingerprint:Ljava/lang/String;

.field private hasTrackedAppUiShown:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private inputMode:Ljava/lang/String;

.field private final screenViewedSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/stores/StoreAnalytics$ScreenViewed;",
            ">;"
        }
    .end annotation
.end field

.field private selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

.field private final stores:Lcom/discord/stores/StoreStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string/jumbo v0, "stores"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreAnalytics;->clock:Lcom/discord/utilities/time/Clock;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->hasTrackedAppUiShown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->screenViewedSubject:Lrx/subjects/PublishSubject;

    return-void
.end method

.method public static final synthetic access$getChannelProperties(Lcom/discord/stores/StoreAnalytics;Lcom/discord/models/domain/ModelChannel;Z)Ljava/util/Map;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreAnalytics;->getChannelProperties(Lcom/discord/models/domain/ModelChannel;Z)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGuildProperties(Lcom/discord/stores/StoreAnalytics;J)Ljava/util/Map;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreAnalytics;->getGuildProperties(J)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getInputMode$p(Lcom/discord/stores/StoreAnalytics;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreAnalytics;->inputMode:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getSelectedVoiceChannel$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/models/domain/ModelChannel;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreAnalytics;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    return-object p0
.end method

.method public static final synthetic access$getSnapshotProperties(Lcom/discord/stores/StoreAnalytics;J)Ljava/util/Map;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreAnalytics;->getSnapshotProperties(J)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$onScreenViewed(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAnalytics;->onScreenViewed(Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V

    return-void
.end method

.method public static final synthetic access$setInputMode$p(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->inputMode:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setSelectedVoiceChannel$p(Lcom/discord/stores/StoreAnalytics;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    return-void
.end method

.method private final getChannelProperties(Lcom/discord/models/domain/ModelChannel;Z)Ljava/util/Map;
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Z)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getPermissions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StorePermissions;->getPermissionsByChannel()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getPermissionOverwrites()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelPermissionOverwrite;

    const-wide/16 v2, 0x400

    invoke-static {v1, v2, v3}, Lcom/discord/models/domain/ModelPermissionOverwrite;->denies(Lcom/discord/models/domain/ModelPermissionOverwrite;J)Z

    move-result v1

    sget-object v2, Lcom/discord/utilities/analytics/AnalyticsUtils;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsUtils;

    invoke-virtual {v2, p1}, Lcom/discord/utilities/analytics/AnalyticsUtils;->getProperties$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelChannel;)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lkotlin/Pair;

    new-instance v4, Lkotlin/Pair;

    const-string v5, "channel_member_perms"

    invoke-direct {v4, v5, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x0

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v4, Lkotlin/Pair;

    const-string v5, "channel_hidden"

    invoke-direct {v4, v5, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v3, v0

    invoke-static {v3}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v0}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    new-instance p2, Lkotlin/Pair;

    const-string v1, "channel_is_nsfw"

    invoke-direct {p2, v1, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p2}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lx/h/m;->d:Lx/h/m;

    :goto_0
    invoke-static {v0, p1}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic getChannelProperties$default(Lcom/discord/stores/StoreAnalytics;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Ljava/util/Map;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreAnalytics;->getChannelProperties(Lcom/discord/models/domain/ModelChannel;Z)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final getGuildProperties(J)Ljava/util/Map;
    .locals 11
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuilds;->getGuildsInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuild;

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGuildMemberCounts$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildMemberCounts;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreGuildMemberCounts;->getApproximateMemberCount(J)I

    move-result v1

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/discord/stores/StoreChannels;->getChannelsForGuild$app_productionDiscordExternalRelease(J)Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    check-cast v6, Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v3, Lx/h/m;->d:Lx/h/m;

    :cond_2
    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreGuilds;->getGuildRolesInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v5}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    const-wide/16 v6, 0x0

    if-eqz v5, :cond_6

    iget-object v8, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v8}, Lcom/discord/stores/StoreStream;->getGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuilds;

    move-result-object v8

    invoke-virtual {v8}, Lcom/discord/stores/StoreGuilds;->getGuildMembersComputedInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v8

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map;

    if-eqz v8, :cond_5

    invoke-interface {v8, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    :goto_3
    iget-object v8, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v8}, Lcom/discord/stores/StoreStream;->getPermissions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePermissions;

    move-result-object v8

    invoke-virtual {v8}, Lcom/discord/stores/StorePermissions;->getGuildPermissions()Ljava/util/Map;

    move-result-object v8

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto :goto_4

    :cond_6
    const/4 v5, 0x0

    :cond_7
    :goto_4
    const/16 v8, 0x9

    new-array v8, v8, [Lkotlin/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lkotlin/Pair;

    const-string v9, "guild_id"

    invoke-direct {p2, v9, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p2, v8, v4

    const/4 p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance v1, Lkotlin/Pair;

    const-string v9, "guild_size_total"

    invoke-direct {v1, v9, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v8, p1

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    new-instance p2, Lkotlin/Pair;

    const-string v1, "guild_num_channels"

    invoke-direct {p2, v1, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x2

    aput-object p2, v8, p1

    const/4 p2, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_5

    :cond_8
    const/4 v1, 0x0

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v9, Lkotlin/Pair;

    const-string v10, "guild_num_text_channels"

    invoke-direct {v9, v10, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v9, v8, p2

    const/4 p2, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_9

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    :cond_9
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    new-instance v1, Lkotlin/Pair;

    const-string v3, "guild_num_voice_channels"

    invoke-direct {v1, v3, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v8, p2

    const/4 p1, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance v1, Lkotlin/Pair;

    const-string v2, "guild_num_roles"

    invoke-direct {v1, v2, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v8, p1

    const/4 p1, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance v1, Lkotlin/Pair;

    const-string v2, "guild_member_num_roles"

    invoke-direct {v1, v2, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v8, p1

    const/4 p1, 0x7

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    new-instance v1, Lkotlin/Pair;

    const-string v2, "guild_member_perms"

    invoke-direct {v1, v2, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v8, p1

    const/16 p1, 0x8

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->isVip()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    new-instance v0, Lkotlin/Pair;

    const-string v1, "guild_is_vip"

    invoke-direct {v0, v1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v0, v8, p1

    invoke-static {v8}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    :cond_a
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1
.end method

.method private final getSnapshotProperties(J)Ljava/util/Map;
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    const-string v1, "channel.guildId"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreAnalytics;->getGuildProperties(J)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p0, p1, v1, v2, p2}, Lcom/discord/stores/StoreAnalytics;->getChannelProperties$default(Lcom/discord/stores/StoreAnalytics;Lcom/discord/models/domain/ModelChannel;ZILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    invoke-static {v0, p1}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private final getStreamFeedbackReasonFromIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_1

    packed-switch p1, :pswitch_data_0

    :goto_0
    const-string p1, "NO_ISSUE"

    goto :goto_1

    :pswitch_0
    const-string p1, "STREAM_STOPPED_UNEXPECTEDLY"

    goto :goto_1

    :pswitch_1
    const-string p1, "AUDIO_POOR"

    goto :goto_1

    :pswitch_2
    const-string p1, "AUDIO_MISSING"

    goto :goto_1

    :pswitch_3
    const-string p1, "OUT_OF_SYNC"

    goto :goto_1

    :pswitch_4
    const-string p1, "LAGGING"

    goto :goto_1

    :pswitch_5
    const-string p1, "BLURRY"

    goto :goto_1

    :pswitch_6
    const-string p1, "BLACK_SCREEN"

    goto :goto_1

    :cond_1
    const-string p1, "OTHER"

    :goto_1
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final handleVideoInputUpdate(Lco/discord/media_engine/VideoInputDeviceDescription;Z)V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v3, p0, Lcom/discord/stores/StoreAnalytics;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    const-string v1, "selectedVoiceChannel.guildId"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getVoiceStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreVoiceStates;->getMediaStatesBlocking()Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    move-object v4, v0

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0x0

    :goto_1
    move-wide v1, v0

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v5, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v5}, Lcom/discord/stores/StoreStream;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/stores/StoreRtcConnection;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getMediaSessionId()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    move-object v7, v5

    move-object v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->videoInputsUpdate(JLcom/discord/models/domain/ModelChannel;Ljava/util/Map;Lco/discord/media_engine/VideoInputDeviceDescription;ZLjava/lang/String;)V

    :cond_3
    return-void
.end method

.method private final onScreenViewed(Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->hasTrackedAppUiShown:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$onScreenViewed$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$onScreenViewed$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic onUserSettingsPaneViewed$default(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreAnalytics;->onUserSettingsPaneViewed(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final updateTrackingData()V
    .locals 5

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->authToken:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    xor-int/2addr v0, v2

    if-nez v0, :cond_4

    iget-object v3, p0, Lcom/discord/stores/StoreAnalytics;->fingerprint:Ljava/lang/String;

    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-nez v3, :cond_4

    const/4 v3, 0x1

    goto :goto_4

    :cond_4
    const/4 v3, 0x0

    :goto_4
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->analyticsToken:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    goto :goto_6

    :cond_6
    :goto_5
    const/4 v0, 0x1

    :goto_6
    if-nez v0, :cond_7

    const/4 v0, 0x1

    goto :goto_7

    :cond_7
    const/4 v0, 0x0

    :goto_7
    const/4 v4, 0x0

    if-eqz v3, :cond_8

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->getTracker()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    move-result-object v0

    invoke-virtual {v0, v4, v2}, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;->setTrackingData(Ljava/lang/String;Z)V

    goto :goto_8

    :cond_8
    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->getTracker()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics;->analyticsToken:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-static {v0, v2, v1, v3, v4}, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;->setTrackingData$default(Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_8

    :cond_9
    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->getTracker()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;->setTrackingData(Ljava/lang/String;Z)V

    :goto_8
    return-void
.end method


# virtual methods
.method public final ackMessage(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$ackMessage$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreAnalytics$ackMessage$1;-><init>(Lcom/discord/stores/StoreAnalytics;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final appLandingViewed()V
    .locals 4

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefsSessionDurable()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CACHE_KEY_LOGOUT_TS"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->appLandingViewed(J)V

    return-void
.end method

.method public final appUiViewed(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->screenViewedSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$ScreenViewed;

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p1, v2, v3}, Lcom/discord/stores/StoreAnalytics$ScreenViewed;-><init>(Ljava/lang/Class;J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final deepLinkReceived(Landroid/content/Intent;Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;)V
    .locals 2

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "metadata"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$deepLinkReceived$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/discord/stores/StoreAnalytics$deepLinkReceived$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final handleAuthToken(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->authToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/StoreAnalytics;->updateTrackingData()V

    return-void
.end method

.method public final handleConnected(Z)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->analyticsToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/StoreAnalytics;->updateTrackingData()V

    :cond_0
    return-void
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getAnalyticsToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->analyticsToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/StoreAnalytics;->updateTrackingData()V

    return-void
.end method

.method public final handleFingerprint(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics;->fingerprint:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/StoreAnalytics;->updateTrackingData()V

    return-void
.end method

.method public final handleIsScreenSharingChanged(Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getMediaEngine$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaEngine;->getSelectedVideoInputDeviceBlocking()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleVideoInputUpdate(Lco/discord/media_engine/VideoInputDeviceDescription;Z)V

    return-void
.end method

.method public final handlePreLogout()V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefsSessionDurable()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "editor"

    invoke-static {v0, v1}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/stores/StoreAnalytics;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "CACHE_KEY_LOGOUT_TS"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final handleUserSpeaking(Ljava/util/Set;)V
    .locals 11
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "speakingUsers"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->inputMode:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v7, p0, Lcom/discord/stores/StoreAnalytics;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v7, :cond_1

    iget-object v1, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    move-wide v8, v1

    sget-object v10, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    move-object v1, v10

    move-wide v2, v8

    move-object v4, p1

    move-object v5, v0

    move-object v6, v7

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/analytics/AnalyticsTracker;->userSpeaking(JLjava/util/Set;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/analytics/AnalyticsTracker;->userListening(JLjava/util/Set;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;)V

    :cond_1
    return-void
.end method

.method public final handleVideoInputDeviceSelected(Lco/discord/media_engine/VideoInputDeviceDescription;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getApplicationStreaming$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->isScreenSharing()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/discord/stores/StoreAnalytics;->handleVideoInputUpdate(Lco/discord/media_engine/VideoInputDeviceDescription;Z)V

    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 11

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMediaSettings;->getInputMode()Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreAnalytics;

    new-instance v6, Lcom/discord/stores/StoreAnalytics$init$1;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreAnalytics$init$1;-><init>(Lcom/discord/stores/StoreAnalytics;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreAnalytics;

    new-instance v6, Lcom/discord/stores/StoreAnalytics$init$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreAnalytics$init$2;-><init>(Lcom/discord/stores/StoreAnalytics;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreAnalytics;->screenViewedSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/stores/StoreAnalytics$init$3;->INSTANCE:Lcom/discord/stores/StoreAnalytics$init$3;

    invoke-virtual {p1, v0}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lg0/l/a/u1$a;->a:Lg0/l/a/u1;

    new-instance v1, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v1, p1, v0}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v2

    const-string p1, "screenViewedSubject\n    \u2026       }\n        .first()"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreAnalytics;

    new-instance v8, Lcom/discord/stores/StoreAnalytics$init$4;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreAnalytics$init$4;-><init>(Lcom/discord/stores/StoreAnalytics;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final inviteSent(Lcom/discord/models/domain/ModelInvite;Lcom/discord/models/domain/ModelMessage;Ljava/lang/String;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$inviteSent$1;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/discord/stores/StoreAnalytics$inviteSent$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelInvite;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onGuildSettingsPaneViewed(Ljava/lang/String;J)V
    .locals 2

    const-string v0, "pane"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$onGuildSettingsPaneViewed$1;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/discord/stores/StoreAnalytics$onGuildSettingsPaneViewed$1;-><init>(Lcom/discord/stores/StoreAnalytics;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onNotificationSettingsUpdated(Lcom/discord/models/domain/ModelNotificationSettings;Ljava/lang/Long;)V
    .locals 2

    const-string v0, "notifSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$onNotificationSettingsUpdated$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/discord/stores/StoreAnalytics$onNotificationSettingsUpdated$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/lang/Long;Lcom/discord/models/domain/ModelNotificationSettings;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onOverlayVoiceEvent(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;-><init>(Lcom/discord/stores/StoreAnalytics;Z)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onUserSettingsPaneViewed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "pane"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$onUserSettingsPaneViewed$1;

    invoke-direct {v1, p2, p1}, Lcom/discord/stores/StoreAnalytics$onUserSettingsPaneViewed$1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackCallReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V
    .locals 2

    const-string v0, "pendingCallFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackChannelOpened(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    new-instance v2, Lcom/discord/stores/StoreAnalytics$trackChannelOpened$$inlined$let$lambda$1;

    invoke-direct {v2, v0, p0, p1, p2}, Lcom/discord/stores/StoreAnalytics$trackChannelOpened$$inlined$let$lambda$1;-><init>(Lcom/discord/models/domain/ModelChannel;Lcom/discord/stores/StoreAnalytics;J)V

    invoke-virtual {v1, p1, p2, v2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->channelOpened(JLkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method public final trackChatInputComponentViewed(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->chatInputComponentViewed(Ljava/lang/String;)V

    return-void
.end method

.method public final trackFailedMessageResolved(IIIZZLcom/discord/stores/FailedMessageResolutionType;JIJ)V
    .locals 16

    const-string v0, "resolutionType"

    move-object/from16 v10, p6

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v15, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;

    move-object v1, v15

    move-object/from16 v2, p0

    move-wide/from16 v3, p10

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p4

    move/from16 v9, p5

    move-wide/from16 v11, p7

    move/from16 v13, p9

    invoke-direct/range {v1 .. v13}, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;-><init>(Lcom/discord/stores/StoreAnalytics;JIIIZZLcom/discord/stores/FailedMessageResolutionType;JI)V

    invoke-virtual {v14, v15}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackFileUploadAlertViewed(Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZ)V
    .locals 12

    const-string v0, "alertType"

    move-object v3, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v10, v0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v11, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;

    move-object v1, v11

    move-object v2, p0

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZ)V

    invoke-virtual {v10, v11}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackGuildProfileOpened(J)V
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->openGuildProfileSheet(J)V

    return-void
.end method

.method public final trackGuildViewed(J)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackGuildViewed$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreAnalytics$trackGuildViewed$1;-><init>(Lcom/discord/stores/StoreAnalytics;J)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->guildViewed(JLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackMediaSessionJoined(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackMediaSessionJoined$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$trackMediaSessionJoined$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackOpenGiftAcceptModal(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8

    const-string v0, "giftCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/stores/StoreAnalytics$trackOpenGiftAcceptModal$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p3

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreAnalytics$trackOpenGiftAcceptModal$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackSearchResultSelected(Lcom/discord/utilities/analytics/SearchType;ILcom/discord/utilities/analytics/SourceObject;)V
    .locals 2

    const-string v0, "searchType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceObject"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackSearchResultSelected$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/discord/stores/StoreAnalytics$trackSearchResultSelected$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/analytics/SearchType;ILcom/discord/utilities/analytics/SourceObject;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackSearchResultViewed(Lcom/discord/utilities/analytics/SearchType;I)V
    .locals 2

    const-string v0, "searchType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/analytics/SearchType;I)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackSearchStarted(Lcom/discord/utilities/analytics/SearchType;)V
    .locals 2

    const-string v0, "searchType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackSearchStarted$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$trackSearchStarted$1;-><init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/analytics/SearchType;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackShowCallFeedbackSheet(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackShowCallFeedbackSheet$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreAnalytics$trackShowCallFeedbackSheet$1;-><init>(Lcom/discord/stores/StoreAnalytics;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackStreamReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;)V
    .locals 7

    const-string v0, "pendingStreamFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->getIssue()Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->getFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreAnalytics;->getStreamFeedbackReasonFromIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->getMediaSessionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->getIssueDetails()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/analytics/AnalyticsTracker;->reportStreamProblem(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final trackVideoLayoutToggled(Ljava/lang/String;JLcom/discord/models/domain/ModelChannel;)V
    .locals 1

    const-string/jumbo v0, "videoLayout"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/utilities/analytics/AnalyticsTracker;->videoLayoutToggled(Ljava/lang/String;JLcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public final trackVideoStreamEnded(Ljava/util/Map;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sender_user_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Long;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v0, "channel_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_1

    move-object v0, v2

    :cond_1
    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-string v0, "guild_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    move-object v2, v0

    :goto_0
    move-object v8, v2

    check-cast v8, Ljava/lang/Long;

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->stores:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getApplicationStreaming$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v3

    invoke-virtual/range {v3 .. v8}, Lcom/discord/stores/StoreApplicationStreaming;->getMaxViewersForStream(JJLjava/lang/Long;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "max_viewers"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->videoStreamEnded(Ljava/util/Map;)V

    :cond_4
    return-void
.end method

.method public final trackVoiceConnectionFailure(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionFailure$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionFailure$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackVoiceConnectionSuccess(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$trackVoiceConnectionSuccess$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackVoiceDisconnect(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAnalytics$trackVoiceDisconnect$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAnalytics$trackVoiceDisconnect$1;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
