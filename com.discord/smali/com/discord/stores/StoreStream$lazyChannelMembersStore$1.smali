.class public final synthetic Lcom/discord/stores/StoreStream$lazyChannelMembersStore$1;
.super Lx/m/c/i;
.source "StoreStream.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStream;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChannels;)V
    .locals 7

    const-class v3, Lcom/discord/stores/StoreChannels;

    const/4 v1, 0x1

    const-string v4, "observeChannel"

    const-string v5, "observeChannel(J)Lrx/Observable;"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/stores/StoreStream$lazyChannelMembersStore$1;->invoke(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(J)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
