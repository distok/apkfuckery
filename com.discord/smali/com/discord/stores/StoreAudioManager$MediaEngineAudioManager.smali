.class public final Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;
.super Ljava/lang/Object;
.source "StoreAudioManager.kt"


# annotations
.annotation build Landroidx/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreAudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MediaEngineAudioManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;,
        Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;
    }
.end annotation


# instance fields
.field private final audioManager:Landroid/media/AudioManager;

.field private initialAudioManagerSettings:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

.field private isBluetoothScoStarted:Z


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->isBluetoothScoStarted:Z

    return-void
.end method

.method private final extractSettings(Landroid/media/AudioManager;)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;
    .locals 3

    new-instance v0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    invoke-virtual {p1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    invoke-virtual {p1}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    invoke-virtual {p1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;-><init>(ZIZ)V

    return-object v0
.end method

.method private final startBluetoothSCO(Landroid/media/AudioManager;)V
    .locals 1

    invoke-virtual {p1}, Landroid/media/AudioManager;->startBluetoothSco()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    iput-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->isBluetoothScoStarted:Z

    return-void
.end method

.method private final stopBluetoothSCO(Landroid/media/AudioManager;)V
    .locals 1

    invoke-virtual {p1}, Landroid/media/AudioManager;->stopBluetoothSco()V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    iput-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->isBluetoothScoStarted:Z

    return-void
.end method

.method private final trySetMode(Landroid/media/AudioManager;I)V
    .locals 0

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/media/AudioManager;->setMode(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private final useSettings(Landroid/media/AudioManager;Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;)V
    .locals 1

    invoke-virtual {p2}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->getSettingBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->isBluetoothScoStarted:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->startBluetoothSCO(Landroid/media/AudioManager;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->isBluetoothScoStarted:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->stopBluetoothSCO(Landroid/media/AudioManager;)V

    :cond_1
    :goto_0
    invoke-virtual {p2}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->getSettingSpeakerPhoneOn()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;)V
    .locals 6

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;->component1()Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;->component2()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    sget-object v2, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/discord/rtcconnection/RtcConnection$State$g;->a:Lcom/discord/rtcconnection/RtcConnection$State$g;

    invoke-static {v0, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/discord/rtcconnection/RtcConnection$State$c;->a:Lcom/discord/rtcconnection/RtcConnection$State$c;

    invoke-static {v0, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_0
    const/4 v4, 0x3

    goto :goto_1

    :cond_3
    instance-of v3, v0, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-eqz v3, :cond_4

    move-object v3, v0

    check-cast v3, Lcom/discord/rtcconnection/RtcConnection$State$d;

    iget-boolean v3, v3, Lcom/discord/rtcconnection/RtcConnection$State$d;->a:Z

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_4
    :goto_1
    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->setMode(I)V

    if-ne v0, v2, :cond_8

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->initialAudioManagerSettings:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    if-eqz v1, :cond_5

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0, v1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->extractSettings(Landroid/media/AudioManager;)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    move-result-object v1

    :goto_2
    iput-object v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->initialAudioManagerSettings:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    iget-object v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    instance-of v2, p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    iget-boolean v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->isBluetoothScoStarted:Z

    if-eqz v1, :cond_7

    instance-of p1, p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz p1, :cond_6

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected()Z

    move-result p1

    if-nez p1, :cond_a

    :cond_6
    iget-object p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->stopBluetoothSCO(Landroid/media/AudioManager;)V

    goto :goto_3

    :cond_7
    instance-of p1, p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz p1, :cond_a

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->getCanBluetoothScoStart()Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->startBluetoothSCO(Landroid/media/AudioManager;)V

    goto :goto_3

    :cond_8
    iget-object p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->initialAudioManagerSettings:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    if-eqz p1, :cond_9

    iget-object v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0, v0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->useSettings(Landroid/media/AudioManager;Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;)V

    :cond_9
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;->initialAudioManagerSettings:Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    :cond_a
    :goto_3
    return-void
.end method
