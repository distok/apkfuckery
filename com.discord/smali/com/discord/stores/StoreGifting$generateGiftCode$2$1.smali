.class public final Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;
.super Lx/m/c/k;
.source "StoreGifting.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGifting$generateGiftCode$2;->invoke(Lcom/discord/utilities/error/Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $error:Lcom/discord/utilities/error/Error;

.field public final synthetic this$0:Lcom/discord/stores/StoreGifting$generateGiftCode$2;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGifting$generateGiftCode$2;Lcom/discord/utilities/error/Error;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->this$0:Lcom/discord/stores/StoreGifting$generateGiftCode$2;

    iput-object p2, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->$error:Lcom/discord/utilities/error/Error;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->$error:Lcom/discord/utilities/error/Error;

    invoke-virtual {v0}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v4, Ljava/lang/Exception;

    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->$error:Lcom/discord/utilities/error/Error;

    invoke-virtual {v0}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v3, "Generate Gift Code Error"

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->this$0:Lcom/discord/stores/StoreGifting$generateGiftCode$2;

    iget-object v1, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->this$0:Lcom/discord/stores/StoreGifting;

    iget-object v0, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->$comboId:Ljava/lang/String;

    sget-object v2, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;->INSTANCE:Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    invoke-static {v1, v0, v2}, Lcom/discord/stores/StoreGifting;->access$setGifts(Lcom/discord/stores/StoreGifting;Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->this$0:Lcom/discord/stores/StoreGifting$generateGiftCode$2;

    iget-object v1, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->this$0:Lcom/discord/stores/StoreGifting;

    iget-object v0, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->$comboId:Ljava/lang/String;

    sget-object v2, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;->INSTANCE:Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    invoke-static {v1, v0, v2}, Lcom/discord/stores/StoreGifting;->access$setGifts(Lcom/discord/stores/StoreGifting;Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->$error:Lcom/discord/utilities/error/Error;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->this$0:Lcom/discord/stores/StoreGifting$generateGiftCode$2;

    iget-object v1, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->this$0:Lcom/discord/stores/StoreGifting;

    iget-object v0, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->$comboId:Ljava/lang/String;

    sget-object v2, Lcom/discord/stores/StoreGifting$GiftState$Invalid;->INSTANCE:Lcom/discord/stores/StoreGifting$GiftState$Invalid;

    invoke-static {v1, v0, v2}, Lcom/discord/stores/StoreGifting;->access$setGifts(Lcom/discord/stores/StoreGifting;Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    :goto_0
    iget-object v0, p0, Lcom/discord/stores/StoreGifting$generateGiftCode$2$1;->this$0:Lcom/discord/stores/StoreGifting$generateGiftCode$2;

    iget-object v1, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->this$0:Lcom/discord/stores/StoreGifting;

    iget-object v0, v0, Lcom/discord/stores/StoreGifting$generateGiftCode$2;->$comboId:Ljava/lang/String;

    sget-object v2, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;->INSTANCE:Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    invoke-static {v1, v0, v2}, Lcom/discord/stores/StoreGifting;->access$setGifts(Lcom/discord/stores/StoreGifting;Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    return-void
.end method
