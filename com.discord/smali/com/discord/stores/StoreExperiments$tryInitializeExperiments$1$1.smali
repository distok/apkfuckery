.class public final Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;
.super Lx/m/c/k;
.source "StoreExperiments.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;->invoke(Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $it:Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;

.field public final synthetic this$0:Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->this$0:Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;

    iput-object p2, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->$it:Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->this$0:Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;

    iget-object v0, v0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;->this$0:Lcom/discord/stores/StoreExperiments;

    invoke-static {v0}, Lcom/discord/stores/StoreExperiments;->access$getStoreAuthentication$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/StoreAuthentication;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->$it:Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;->getFingerprint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreAuthentication;->setFingerprint(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->this$0:Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;

    iget-object v0, v0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->$it:Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;

    invoke-virtual {v1}, Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;->getAssignments()Ljava/util/List;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v2

    const/16 v3, 0x10

    if-ge v2, v3, :cond_0

    const/16 v2, 0x10

    :cond_0
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/discord/models/experiments/dto/UserExperimentDto;

    invoke-virtual {v4}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getNameHash()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    invoke-static {v0, v3, v1}, Lcom/discord/stores/StoreExperiments;->access$handleLoadedUserExperiments(Lcom/discord/stores/StoreExperiments;Ljava/util/Map;Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1$1;->this$0:Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;

    iget-object v0, v0, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;->this$0:Lcom/discord/stores/StoreExperiments;

    invoke-static {v0}, Lcom/discord/stores/StoreExperiments;->access$setInitialized(Lcom/discord/stores/StoreExperiments;)V

    return-void
.end method
