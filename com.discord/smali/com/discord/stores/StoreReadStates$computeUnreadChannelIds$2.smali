.class public final Lcom/discord/stores/StoreReadStates$computeUnreadChannelIds$2;
.super Lx/m/c/k;
.source "StoreReadStates.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreReadStates;->computeUnreadChannelIds()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;+",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreReadStates;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreReadStates;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreReadStates$computeUnreadChannelIds$2;->this$0:Lcom/discord/stores/StoreReadStates;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreReadStates$computeUnreadChannelIds$2;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;+",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    iget-object v1, p0, Lcom/discord/stores/StoreReadStates$computeUnreadChannelIds$2;->this$0:Lcom/discord/stores/StoreReadStates;

    invoke-static {v1}, Lcom/discord/stores/StoreReadStates;->access$getUnreadChannelIds$p(Lcom/discord/stores/StoreReadStates;)Lrx/subjects/SerializedSubject;

    move-result-object v1

    iget-object v1, v1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v1, v0}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreReadStates$computeUnreadChannelIds$2;->this$0:Lcom/discord/stores/StoreReadStates;

    invoke-static {v0}, Lcom/discord/stores/StoreReadStates;->access$getUnreadGuildIds$p(Lcom/discord/stores/StoreReadStates;)Lrx/subjects/SerializedSubject;

    move-result-object v0

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method
