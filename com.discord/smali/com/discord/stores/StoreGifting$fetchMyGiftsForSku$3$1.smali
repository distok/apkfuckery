.class public final Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;
.super Lx/m/c/k;
.source "StoreGifting.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;->invoke(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $gifts:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;->this$0:Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;

    iput-object p2, p0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;->$gifts:Ljava/util/List;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    iget-object v0, p0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;->this$0:Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;

    iget-object v1, v0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;->this$0:Lcom/discord/stores/StoreGifting;

    iget-object v0, v0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;->$comboId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/discord/stores/StoreGifting;->access$removeGiftCode(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;->$gifts:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGift;

    iget-object v2, p0, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3$1;->this$0:Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;

    iget-object v2, v2, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;->this$0:Lcom/discord/stores/StoreGifting;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->getCode()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    invoke-direct {v4, v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;-><init>(Lcom/discord/models/domain/ModelGift;)V

    invoke-static {v2, v3, v4}, Lcom/discord/stores/StoreGifting;->access$setGifts(Lcom/discord/stores/StoreGifting;Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    goto :goto_0

    :cond_0
    return-void
.end method
