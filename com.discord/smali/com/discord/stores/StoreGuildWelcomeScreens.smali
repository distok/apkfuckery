.class public final Lcom/discord/stores/StoreGuildWelcomeScreens;
.super Lcom/discord/stores/StoreV2;
.source "StoreGuildWelcomeScreens.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final guildWelcomeScreensSeen:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private guildWelcomeScreensSeenSnapshot:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private guildWelcomeScreensSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;",
            ">;"
        }
    .end annotation
.end field

.field private final guildWelcomeScreensState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;",
            ">;"
        }
    .end annotation
.end field

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSnapshot:Ljava/util/Map;

    sget-object p1, Lx/h/n;->d:Lx/h/n;

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSeenSnapshot:Ljava/util/Set;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensState:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSeen:Ljava/util/HashSet;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreGuildWelcomeScreens;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getGuildWelcomeScreensSeen$p(Lcom/discord/stores/StoreGuildWelcomeScreens;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSeen:Ljava/util/HashSet;

    return-object p0
.end method

.method public static final synthetic access$getGuildWelcomeScreensState$p(Lcom/discord/stores/StoreGuildWelcomeScreens;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensState:Ljava/util/HashMap;

    return-object p0
.end method

.method public static final synthetic access$handleGuildWelcomeScreen(Lcom/discord/stores/StoreGuildWelcomeScreens;JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreGuildWelcomeScreens;->handleGuildWelcomeScreen(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    return-void
.end method

.method public static final synthetic access$handleGuildWelcomeScreenFetchFailed(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens;->handleGuildWelcomeScreenFetchFailed(J)V

    return-void
.end method

.method public static final synthetic access$handleGuildWelcomeScreenFetchStart(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens;->handleGuildWelcomeScreenFetchStart(J)V

    return-void
.end method

.method private final handleGuildWelcomeScreen(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensState:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;

    invoke-direct {p2, p3}, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;-><init>(Lcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleGuildWelcomeScreenFetchFailed(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensState:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Failure;->INSTANCE:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Failure;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleGuildWelcomeScreenFetchStart(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensState:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Fetching;->INSTANCE:Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Fetching;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method


# virtual methods
.method public final fetchIfNonexisting(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;-><init>(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getGuildWelcomeScreen(J)Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSnapshot:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    return-object p1
.end method

.method public final handleGuildJoined(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreGuildWelcomeScreens;->handleGuildWelcomeScreen(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    return-void
.end method

.method public final hasWelcomeScreenBeenSeen(J)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSeenSnapshot:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final markWelcomeScreenShown(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildWelcomeScreens$markWelcomeScreenShown$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens$markWelcomeScreenShown$1;-><init>(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final observeGuildWelcomeScreen(J)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;

    invoke-direct {v5, p0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;-><init>(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observationDeck.connectR\u2026 }.distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public snapshotData()V
    .locals 2

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensState:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSnapshot:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSeen:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens;->guildWelcomeScreensSeenSnapshot:Ljava/util/Set;

    return-void
.end method
