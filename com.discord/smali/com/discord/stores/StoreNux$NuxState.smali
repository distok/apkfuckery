.class public final Lcom/discord/stores/StoreNux$NuxState;
.super Ljava/lang/Object;
.source "StoreNux.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreNux;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NuxState"
.end annotation


# instance fields
.field private final addGuildHint:Z

.field private final firstOpen:Z

.field private final postRegister:Z

.field private final premiumGuildHintGuildId:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/StoreNux$NuxState;-><init>(ZZZLjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZZLjava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    iput-boolean p2, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    iput-boolean p3, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    iput-object p4, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    return-void
.end method

.method public synthetic constructor <init>(ZZZLjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreNux$NuxState;-><init>(ZZZLjava/lang/Long;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreNux$NuxState;ZZZLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/stores/StoreNux$NuxState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-boolean p1, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-boolean p2, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreNux$NuxState;->copy(ZZZLjava/lang/Long;)Lcom/discord/stores/StoreNux$NuxState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    return v0
.end method

.method public final component4()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(ZZZLjava/lang/Long;)Lcom/discord/stores/StoreNux$NuxState;
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreNux$NuxState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/StoreNux$NuxState;-><init>(ZZZLjava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreNux$NuxState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreNux$NuxState;

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddGuildHint()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    return v0
.end method

.method public final getFirstOpen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    return v0
.end method

.method public final getPostRegister()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    return v0
.end method

.method public final getPremiumGuildHintGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "NuxState(postRegister="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/stores/StoreNux$NuxState;->postRegister:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", firstOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreNux$NuxState;->firstOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", addGuildHint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreNux$NuxState;->addGuildHint:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", premiumGuildHintGuildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreNux$NuxState;->premiumGuildHintGuildId:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
