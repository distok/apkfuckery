.class public final Lcom/discord/stores/StoreExpressionPickerNavigation;
.super Ljava/lang/Object;
.source "StoreExpressionPickerNavigation.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;
    }
.end annotation


# instance fields
.field private final expressionPickerEventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedTabPersister:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    sget-object v1, Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;->EMOJI:Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;

    const-string v2, "CACHE_KEY_SELECTED_EXPRESSION_TRAY_TAB"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreExpressionPickerNavigation;->selectedTabPersister:Lcom/discord/utilities/persister/Persister;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreExpressionPickerNavigation;->expressionPickerEventSubject:Lrx/subjects/PublishSubject;

    return-void
.end method


# virtual methods
.method public final createExpressionPickerEvent(Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;)V
    .locals 1

    const-string v0, "expressionPickerEvent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreExpressionPickerNavigation;->expressionPickerEventSubject:Lrx/subjects/PublishSubject;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final observeExpressionPickerEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExpressionPickerNavigation;->expressionPickerEventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "expressionPickerEventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeSelectedTab()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExpressionPickerNavigation;->selectedTabPersister:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final onSelectTab(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V
    .locals 2

    const-string v0, "expressionTrayTab"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreExpressionPickerNavigation;->selectedTabPersister:Lcom/discord/utilities/persister/Persister;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    return-void
.end method
