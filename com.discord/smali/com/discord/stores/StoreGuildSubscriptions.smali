.class public final Lcom/discord/stores/StoreGuildSubscriptions;
.super Lcom/discord/stores/Store;
.source "StoreGuildSubscriptions.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final storeStream:Lcom/discord/stores/StoreStream;

.field private final subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string/jumbo v0, "storeStream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildSubscriptions;->storeStream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreGuildSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    new-instance p2, Lcom/discord/stores/StoreGuildSubscriptions$subscriptionsManager$1;

    invoke-direct {p2, p0}, Lcom/discord/stores/StoreGuildSubscriptions$subscriptionsManager$1;-><init>(Lcom/discord/stores/StoreGuildSubscriptions;)V

    invoke-direct {p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;-><init>(Lkotlin/jvm/functions/Function2;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    return-void
.end method

.method public static final synthetic access$getStoreStream$p(Lcom/discord/stores/StoreGuildSubscriptions;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->storeStream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$getSubscriptionsManager$p(Lcom/discord/stores/StoreGuildSubscriptions;)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    return-object p0
.end method


# virtual methods
.method public final handleConnectionReady(Z)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGuildSubscriptions;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreGuildSelected;->getSelectedGuildId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/discord/stores/StoreGuildSubscriptions;->handleGuildSelect(J)V

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {p1}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->queueExistingSubscriptions()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/discord/stores/StoreGuildSubscriptions;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreGuildSelected;->getSelectedGuildId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/discord/stores/StoreGuildSubscriptions;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreRtcConnection;->getConnectedGuildId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->retainAll(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public final handleGuildRemove(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->remove(J)V

    return-void
.end method

.method public final handleGuildSelect(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribeTyping(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribeActivities(J)V

    return-void
.end method

.method public final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->reset()V

    return-void
.end method

.method public final handleSubscribeMember(JJ)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribeMember(JJ)V

    return-void
.end method

.method public final handleUnsubscribeMember(JJ)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->unsubscribeMember(JJ)V

    return-void
.end method

.method public onDispatchEnded()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->subscriptionsManager:Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    invoke-virtual {v0}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->flush()V

    return-void
.end method

.method public final subscribeChannelRange(JJLkotlin/ranges/IntRange;)V
    .locals 9

    const-string v0, "range"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_1

    cmp-long v2, p3, v0

    if-gtz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v8, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p5

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;-><init>(Lcom/discord/stores/StoreGuildSubscriptions;Lkotlin/ranges/IntRange;JJ)V

    invoke-virtual {v0, v8}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final subscribeUser(JJ)V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/stores/StoreGuildSubscriptions$subscribeUser$1;

    move-object v1, v7

    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreGuildSubscriptions$subscribeUser$1;-><init>(Lcom/discord/stores/StoreGuildSubscriptions;JJ)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final unsubscribeUser(JJ)V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/stores/StoreGuildSubscriptions$unsubscribeUser$1;

    move-object v1, v7

    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreGuildSubscriptions$unsubscribeUser$1;-><init>(Lcom/discord/stores/StoreGuildSubscriptions;JJ)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
