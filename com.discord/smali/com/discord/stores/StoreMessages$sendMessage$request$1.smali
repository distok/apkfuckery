.class public final Lcom/discord/stores/StoreMessages$sendMessage$request$1;
.super Lx/m/c/k;
.source "StoreMessages.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessages;->sendMessage(JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lrx/Emitter<",
        "Lcom/discord/utilities/messagesend/MessageResult;",
        ">;",
        "Lcom/discord/utilities/messagesend/MessageRequest$Send;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $activity:Lcom/discord/models/domain/activity/ModelActivity;

.field public final synthetic $attemptTimestamp:J

.field public final synthetic $localMessage:Lcom/discord/models/domain/ModelMessage;

.field public final synthetic $validAttachments:Lkotlin/jvm/internal/Ref$ObjectRef;

.field public final synthetic this$0:Lcom/discord/stores/StoreMessages;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/activity/ModelActivity;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iput-object p2, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    iput-object p3, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$validAttachments:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p4, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$activity:Lcom/discord/models/domain/activity/ModelActivity;

    iput-wide p5, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$attemptTimestamp:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lrx/Emitter;)Lcom/discord/utilities/messagesend/MessageRequest$Send;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            ">;)",
            "Lcom/discord/utilities/messagesend/MessageRequest$Send;"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/utilities/messagesend/MessageRequest$Send;

    iget-object v2, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    const-string v1, "localMessage"

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$validAttachments:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v4, v1

    check-cast v4, Ljava/util/List;

    iget-object v3, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$activity:Lcom/discord/models/domain/activity/ModelActivity;

    iget-wide v8, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$attemptTimestamp:J

    new-instance v5, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    invoke-direct {v5, p0, p1}, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;-><init>(Lcom/discord/stores/StoreMessages$sendMessage$request$1;Lrx/Emitter;)V

    new-instance v6, Lcom/discord/stores/StoreMessages$sendMessage$request$1$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreMessages$sendMessage$request$1$2;-><init>(Lcom/discord/stores/StoreMessages$sendMessage$request$1;)V

    new-instance v7, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;-><init>(Lcom/discord/stores/StoreMessages$sendMessage$request$1;)V

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/discord/utilities/messagesend/MessageRequest$Send;-><init>(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;J)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lrx/Emitter;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->invoke(Lrx/Emitter;)Lcom/discord/utilities/messagesend/MessageRequest$Send;

    move-result-object p1

    return-object p1
.end method
