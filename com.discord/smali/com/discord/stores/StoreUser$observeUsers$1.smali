.class public final Lcom/discord/stores/StoreUser$observeUsers$1;
.super Ljava/lang/Object;
.source "StoreUser.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUser;->observeUsers(Ljava/util/Collection;Z)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $fetchUserIfMissing:Z

.field public final synthetic $userIds:Ljava/util/Collection;

.field public final synthetic this$0:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUser;ZLjava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUser$observeUsers$1;->this$0:Lcom/discord/stores/StoreUser;

    iput-boolean p2, p0, Lcom/discord/stores/StoreUser$observeUsers$1;->$fetchUserIfMissing:Z

    iput-object p3, p0, Lcom/discord/stores/StoreUser$observeUsers$1;->$userIds:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreUser$observeUsers$1;->call(Ljava/util/Map;)V

    return-void
.end method

.method public final call(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreUser$observeUsers$1;->$fetchUserIfMissing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreUser$observeUsers$1;->this$0:Lcom/discord/stores/StoreUser;

    iget-object v1, p0, Lcom/discord/stores/StoreUser$observeUsers$1;->$userIds:Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/discord/stores/StoreUser;->access$fetchMissing(Lcom/discord/stores/StoreUser;Ljava/util/Collection;Ljava/util/Set;)V

    :cond_0
    return-void
.end method
