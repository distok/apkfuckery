.class public final Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1$1;
.super Lx/m/c/k;
.source "StoreGooglePlayPurchases.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1$1;->invoke(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;)V
    .locals 1

    instance-of p1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/utilities/billing/BillingUtils;->INSTANCE:Lcom/discord/utilities/billing/BillingUtils;

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;

    iget-object v0, v0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->$purchases:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/billing/BillingUtils;->verifyPurchases(Ljava/util/List;)V

    :cond_0
    return-void
.end method
