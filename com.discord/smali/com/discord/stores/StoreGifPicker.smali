.class public final Lcom/discord/stores/StoreGifPicker;
.super Lcom/discord/stores/StoreV2;
.source "StoreGifPicker.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGifPicker$CacheHistory;,
        Lcom/discord/stores/StoreGifPicker$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreGifPicker$Companion;

.field private static final searchResultsLoadingList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;"
        }
    .end annotation
.end field

.field private static final searchTermsLoadingList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final gifCategories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;"
        }
    .end annotation
.end field

.field private gifCategoriesSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final gifSearchHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/stores/StoreGifPicker$CacheHistory<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;>;"
        }
    .end annotation
.end field

.field private gifSearchHistorySnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;>;"
        }
    .end annotation
.end field

.field private final gifSuggestedSearchTermsHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/stores/StoreGifPicker$CacheHistory<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final gifTrendingSearchTerms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private gifTrendingSearchTermsSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isFetchingGifCategories:Z

.field private isFetchingTrendingCategoryGifs:Z

.field private isFetchingTrendingSearchTerms:Z

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;

.field private suggestedSearchTermsSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final trendingCategoryGifs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;"
        }
    .end annotation
.end field

.field private trendingCategoryGifsSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;"
        }
    .end annotation
.end field

.field private trendingGifCategoryPreviewUrl:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreGifPicker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreGifPicker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreGifPicker;->Companion:Lcom/discord/stores/StoreGifPicker$Companion;

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    sput-object v0, Lcom/discord/stores/StoreGifPicker;->searchResultsLoadingList:Ljava/util/List;

    sput-object v0, Lcom/discord/stores/StoreGifPicker;->searchTermsLoadingList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreGifPicker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object p3, p0, Lcom/discord/stores/StoreGifPicker;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object p4, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->gifCategoriesSnapshot:Ljava/util/List;

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTermsSnapshot:Ljava/util/List;

    sget-object p2, Lx/h/m;->d:Lx/h/m;

    iput-object p2, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistorySnapshot:Ljava/util/Map;

    iput-object p2, p0, Lcom/discord/stores/StoreGifPicker;->suggestedSearchTermsSnapshot:Ljava/util/Map;

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifsSnapshot:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->gifCategories:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTerms:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifs:Ljava/util/List;

    const-string p1, ""

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->trendingGifCategoryPreviewUrl:Ljava/lang/String;

    new-instance p1, Lcom/discord/stores/StoreGifPicker$CacheHistory;

    invoke-direct {p1}, Lcom/discord/stores/StoreGifPicker$CacheHistory;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    new-instance p1, Lcom/discord/stores/StoreGifPicker$CacheHistory;

    invoke-direct {p1}, Lcom/discord/stores/StoreGifPicker$CacheHistory;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->gifSuggestedSearchTermsHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    sget-object p3, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p3}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p3

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p4

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreGifPicker;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$fetchGifTrendingSearchTerms(Lcom/discord/stores/StoreGifPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->fetchGifTrendingSearchTerms()V

    return-void
.end method

.method public static final synthetic access$fetchGifsForSearchQuery(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->fetchGifsForSearchQuery(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$fetchSuggestedSearchTerms(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->fetchSuggestedSearchTerms(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$fetchTrendingCategoryGifs(Lcom/discord/stores/StoreGifPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->fetchTrendingCategoryGifs()V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreGifPicker;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getGifCategories(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->getGifCategories()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGifCategories$p(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGifPicker;->gifCategories:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getGifSearchHistory$p(Lcom/discord/stores/StoreGifPicker;)Lcom/discord/stores/StoreGifPicker$CacheHistory;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    return-object p0
.end method

.method public static final synthetic access$getGifSuggestedSearchTermsHistory$p(Lcom/discord/stores/StoreGifPicker;)Lcom/discord/stores/StoreGifPicker$CacheHistory;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGifPicker;->gifSuggestedSearchTermsHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    return-object p0
.end method

.method public static final synthetic access$getGifTrendingSearchTerms(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->getGifTrendingSearchTerms()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGifTrendingSearchTerms$p(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTerms:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getSearchHistory(Lcom/discord/stores/StoreGifPicker;)Ljava/util/Map;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->getSearchHistory()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSearchResultsLoadingList$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreGifPicker;->searchResultsLoadingList:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getSearchTermsLoadingList$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreGifPicker;->searchTermsLoadingList:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getSuggestedSearchTermsHistory(Lcom/discord/stores/StoreGifPicker;)Ljava/util/Map;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->getSuggestedSearchTermsHistory()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTrendingCategoryGifs(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->getTrendingCategoryGifs()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTrendingCategoryGifs$p(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifs:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getTrendingGifCategoryPreviewUrl(Lcom/discord/stores/StoreGifPicker;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->getTrendingGifCategoryPreviewUrl()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleFetchGifCategoriesError(Lcom/discord/stores/StoreGifPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->handleFetchGifCategoriesError()V

    return-void
.end method

.method public static final synthetic access$handleFetchGifCategoriesOnNext(Lcom/discord/stores/StoreGifPicker;Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->handleFetchGifCategoriesOnNext(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V

    return-void
.end method

.method public static final synthetic access$handleFetchTrendingGifsError(Lcom/discord/stores/StoreGifPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->handleFetchTrendingGifsError()V

    return-void
.end method

.method public static final synthetic access$handleFetchTrendingGifsOnNext(Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->handleFetchTrendingGifsOnNext(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$handleFetchTrendingSearchTermsError(Lcom/discord/stores/StoreGifPicker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->handleFetchTrendingSearchTermsError()V

    return-void
.end method

.method public static final synthetic access$handleFetchTrendingSearchTermsOnNext(Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->handleFetchTrendingSearchTermsOnNext(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$handleGifSearchResults(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGifPicker;->handleGifSearchResults(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$handleSuggestedSearchTerms(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGifPicker;->handleSuggestedSearchTerms(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$handleTrendingCategoriesResponse(Lcom/discord/stores/StoreGifPicker;Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->handleTrendingCategoriesResponse(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V

    return-void
.end method

.method public static final synthetic access$isFetchingGifCategories$p(Lcom/discord/stores/StoreGifPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingGifCategories:Z

    return p0
.end method

.method public static final synthetic access$isFetchingTrendingCategoryGifs$p(Lcom/discord/stores/StoreGifPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingTrendingCategoryGifs:Z

    return p0
.end method

.method public static final synthetic access$isFetchingTrendingSearchTerms$p(Lcom/discord/stores/StoreGifPicker;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingTrendingSearchTerms:Z

    return p0
.end method

.method public static final synthetic access$setFetchingGifCategories$p(Lcom/discord/stores/StoreGifPicker;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingGifCategories:Z

    return-void
.end method

.method public static final synthetic access$setFetchingTrendingCategoryGifs$p(Lcom/discord/stores/StoreGifPicker;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingTrendingCategoryGifs:Z

    return-void
.end method

.method public static final synthetic access$setFetchingTrendingSearchTerms$p(Lcom/discord/stores/StoreGifPicker;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingTrendingSearchTerms:Z

    return-void
.end method

.method public static final synthetic access$updateTrendingCategoryGifs(Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->updateTrendingCategoryGifs(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$updateTrendingSearchTerms(Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifPicker;->updateTrendingSearchTerms(Ljava/util/List;)V

    return-void
.end method

.method private final fetchGifTrendingSearchTerms()V
    .locals 13

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingTrendingSearchTerms:Z

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-object v2, p0, Lcom/discord/stores/StoreGifPicker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "storeUserSettings.locale"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "tenor"

    const/4 v4, 0x5

    invoke-virtual {v1, v3, v2, v4}, Lcom/discord/utilities/rest/RestAPI;->getGifTrendingSearchTerms(Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreGifPicker;

    new-instance v8, Lcom/discord/stores/StoreGifPicker$fetchGifTrendingSearchTerms$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreGifPicker$fetchGifTrendingSearchTerms$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    new-instance v10, Lcom/discord/stores/StoreGifPicker$fetchGifTrendingSearchTerms$2;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreGifPicker$fetchGifTrendingSearchTerms$2;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final fetchGifsForSearchQuery(Ljava/lang/String;)V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v1, "storeUserSettings.locale"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tenor"

    const-string/jumbo v4, "tinygif"

    const/16 v5, 0x32

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/rest/RestAPI;->getGifSearchResults(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$1;->INSTANCE:Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "restAPI.getGifSearchResu\u2026to)\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$2;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->r(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v2

    const-string v0, "restAPI.getGifSearchResu\u2026y, emptyList())\n        }"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreGifPicker;

    new-instance v8, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;

    invoke-direct {v8, p0, p1}, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final fetchSuggestedSearchTerms(Ljava/lang/String;)V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "storeUserSettings.locale"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "tenor"

    const/4 v3, 0x5

    invoke-virtual {v0, v2, p1, v1, v3}, Lcom/discord/utilities/rest/RestAPI;->getGifSuggestedSearchTerms(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGifPicker$fetchSuggestedSearchTerms$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$fetchSuggestedSearchTerms$1;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->r(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v2

    const-string v0, "restAPI.getGifSuggestedS\u2026y, emptyList())\n        }"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreGifPicker;

    new-instance v8, Lcom/discord/stores/StoreGifPicker$fetchSuggestedSearchTerms$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/stores/StoreGifPicker$fetchSuggestedSearchTerms$2;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final fetchTrendingCategoryGifs()V
    .locals 13

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingTrendingCategoryGifs:Z

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-object v2, p0, Lcom/discord/stores/StoreGifPicker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "storeUserSettings.locale"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "tenor"

    const-string/jumbo v4, "tinygif"

    const/16 v5, 0x32

    invoke-virtual {v1, v3, v2, v4, v5}, Lcom/discord/utilities/rest/RestAPI;->getTrendingGifCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreGifPicker$fetchTrendingCategoryGifs$1;->INSTANCE:Lcom/discord/stores/StoreGifPicker$fetchTrendingCategoryGifs$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const-string v2, "restAPI.getTrendingGifCa\u2026to)\n          }\n        }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreGifPicker;

    new-instance v8, Lcom/discord/stores/StoreGifPicker$fetchTrendingCategoryGifs$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreGifPicker$fetchTrendingCategoryGifs$2;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    new-instance v10, Lcom/discord/stores/StoreGifPicker$fetchTrendingCategoryGifs$3;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreGifPicker$fetchTrendingCategoryGifs$3;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final getGifCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifCategoriesSnapshot:Ljava/util/List;

    return-object v0
.end method

.method private final getGifTrendingSearchTerms()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTermsSnapshot:Ljava/util/List;

    return-object v0
.end method

.method private final getSearchHistory()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistorySnapshot:Ljava/util/Map;

    return-object v0
.end method

.method private final getSuggestedSearchTermsHistory()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifSuggestedSearchTermsHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    return-object v0
.end method

.method private final getTrendingCategoryGifs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifsSnapshot:Ljava/util/List;

    return-object v0
.end method

.method private final getTrendingGifCategoryPreviewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->trendingGifCategoryPreviewUrl:Ljava/lang/String;

    return-object v0
.end method

.method private final handleFetchGifCategoriesError()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$handleFetchGifCategoriesError$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGifPicker$handleFetchGifCategoriesError$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleFetchGifCategoriesOnNext(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$handleFetchGifCategoriesOnNext$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$handleFetchGifCategoriesOnNext$1;-><init>(Lcom/discord/stores/StoreGifPicker;Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleFetchTrendingGifsError()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingGifsError$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingGifsError$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleFetchTrendingGifsOnNext(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingGifsOnNext$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingGifsOnNext$1;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleFetchTrendingSearchTermsError()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingSearchTermsError$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingSearchTermsError$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleFetchTrendingSearchTermsOnNext(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingSearchTermsOnNext$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$handleFetchTrendingSearchTermsOnNext$1;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleGifSearchResults(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleSuggestedSearchTerms(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifSuggestedSearchTermsHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleTrendingCategoriesResponse(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;->getCategories()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/gifpicker/dto/GifCategoryDto;

    new-instance v3, Lcom/discord/models/gifpicker/domain/ModelGifCategory;

    invoke-virtual {v2}, Lcom/discord/models/gifpicker/dto/GifCategoryDto;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/discord/models/gifpicker/dto/GifCategoryDto;->getSrc()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/discord/models/gifpicker/domain/ModelGifCategory;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/discord/stores/StoreGifPicker;->updateGifCategories(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;->getGifs()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/gifpicker/dto/TrendingGifPreviewDto;

    invoke-virtual {p1}, Lcom/discord/models/gifpicker/dto/TrendingGifPreviewDto;->getSrc()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string p1, ""

    :goto_1
    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker;->trendingGifCategoryPreviewUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final observeSearchHistory()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreGifPicker$observeSearchHistory$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreGifPicker$observeSearchHistory$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final observeSuggestedSearchTerms()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreGifPicker$observeSuggestedSearchTerms$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreGifPicker$observeSuggestedSearchTerms$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final updateGifCategories(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifCategories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifCategories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final updateTrendingCategoryGifs(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final updateTrendingSearchTerms(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTerms:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTerms:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method


# virtual methods
.method public final fetchGifCategories()V
    .locals 13

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreGifPicker;->isFetchingGifCategories:Z

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-object v2, p0, Lcom/discord/stores/StoreGifPicker;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "storeUserSettings.locale"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "tenor"

    const-string/jumbo v4, "tinygif"

    invoke-virtual {v1, v3, v2, v4}, Lcom/discord/utilities/rest/RestAPI;->getTrendingGifCategories(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreGifPicker;

    new-instance v8, Lcom/discord/stores/StoreGifPicker$fetchGifCategories$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreGifPicker$fetchGifCategories$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    new-instance v10, Lcom/discord/stores/StoreGifPicker$fetchGifCategories$2;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreGifPicker$fetchGifCategories$2;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final observeGifCategories()Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeGifCategories$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGifPicker$observeGifCategories$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v2, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v3, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    new-instance v7, Lcom/discord/stores/StoreGifPicker$observeGifCategories$2;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreGifPicker$observeGifCategories$2;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeGifTrendingSearchTerms()Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeGifTrendingSearchTerms$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGifPicker$observeGifTrendingSearchTerms$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v2, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v3, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    new-instance v7, Lcom/discord/stores/StoreGifPicker$observeGifTrendingSearchTerms$2;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreGifPicker$observeGifTrendingSearchTerms$2;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeGifsForSearchQuery(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;>;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeGifsForSearchQuery$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$observeGifsForSearchQuery$1;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->observeSearchHistory()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeGifsForSearchQuery$2;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreGifPicker$observeGifsForSearchQuery$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "observeSearchHistory().m\u2026gifSearchHistory[query] }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "filter { it != null }.map { it!! }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeSuggestedSearchTerms(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeSuggestedSearchTerms$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$observeSuggestedSearchTerms$2;-><init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker;->observeSuggestedSearchTerms()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeSuggestedSearchTerms$3;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreGifPicker$observeSuggestedSearchTerms$3;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "observeSuggestedSearchTe\u2026archTermsHistory[query] }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "filter { it != null }.map { it!! }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeTrendingCategoryGifs()Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifPicker$observeTrendingCategoryGifs$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGifPicker$observeTrendingCategoryGifs$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v2, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v3, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    new-instance v7, Lcom/discord/stores/StoreGifPicker$observeTrendingCategoryGifs$2;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreGifPicker$observeTrendingCategoryGifs$2;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeTrendingGifCategoryPreviewUrl()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreGifPicker$observeTrendingGifCategoryPreviewUrl$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreGifPicker$observeTrendingGifCategoryPreviewUrl$1;-><init>(Lcom/discord/stores/StoreGifPicker;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public snapshotData()V
    .locals 2

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->gifCategories:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifCategoriesSnapshot:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTerms:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifTrendingSearchTermsSnapshot:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGifPicker;->gifSearchHistorySnapshot:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->gifSuggestedSearchTermsHistory:Lcom/discord/stores/StoreGifPicker$CacheHistory;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGifPicker;->suggestedSearchTermsSnapshot:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifs:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGifPicker;->trendingCategoryGifsSnapshot:Ljava/util/List;

    return-void
.end method
