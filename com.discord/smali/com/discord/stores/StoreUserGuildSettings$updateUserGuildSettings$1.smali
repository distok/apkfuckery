.class public final Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;
.super Lx/m/c/k;
.source "StoreUserGuildSettings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUserGuildSettings;->updateUserGuildSettings(Landroid/content/Context;JLcom/discord/restapi/RestAPIParams$UserGuildSettings;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelNotificationSettings;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:Ljava/lang/Long;

.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $successString:I

.field public final synthetic this$0:Lcom/discord/stores/StoreUserGuildSettings;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUserGuildSettings;Ljava/lang/Long;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->this$0:Lcom/discord/stores/StoreUserGuildSettings;

    iput-object p2, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->$channelId:Ljava/lang/Long;

    iput-object p3, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->$context:Landroid/content/Context;

    iput p4, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->$successString:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelNotificationSettings;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->invoke(Lcom/discord/models/domain/ModelNotificationSettings;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelNotificationSettings;)V
    .locals 4

    const-string v0, "notifSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->this$0:Lcom/discord/stores/StoreUserGuildSettings;

    invoke-static {v0}, Lcom/discord/stores/StoreUserGuildSettings;->access$getAnalytics$p(Lcom/discord/stores/StoreUserGuildSettings;)Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->$channelId:Ljava/lang/Long;

    invoke-virtual {v0, p1, v1}, Lcom/discord/stores/StoreAnalytics;->onNotificationSettingsUpdated(Lcom/discord/models/domain/ModelNotificationSettings;Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->$context:Landroid/content/Context;

    iget v0, p0, Lcom/discord/stores/StoreUserGuildSettings$updateUserGuildSettings$1;->$successString:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {p1, v0, v1, v2, v3}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    return-void
.end method
