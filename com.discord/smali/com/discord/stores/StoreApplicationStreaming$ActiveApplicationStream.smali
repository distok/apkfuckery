.class public final Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
.super Ljava/lang/Object;
.source "StoreApplicationStreaming.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreApplicationStreaming;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActiveApplicationStream"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;
    }
.end annotation


# instance fields
.field private final state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

.field private final stream:Lcom/discord/models/domain/ModelApplicationStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stream"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;ILjava/lang/Object;)Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->copy(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stream"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;-><init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    iget-object v1, p1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    iget-object p1, p1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    return-object v0
.end method

.method public final getStream()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ActiveApplicationStream(state="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->state:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
