.class public final Lcom/discord/stores/StoreClientDataState$ClientDataState;
.super Ljava/lang/Object;
.source "StoreClientDataState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreClientDataState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientDataState"
.end annotation


# instance fields
.field private final guildHashes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildHash;",
            ">;"
        }
    .end annotation
.end field

.field private final highestLastMessageId:J

.field private final readStateVersion:I

.field private final userGuildSettingsVersion:I


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/discord/stores/StoreClientDataState$ClientDataState;-><init>(Ljava/util/Map;JIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;JII)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildHash;",
            ">;JII)V"
        }
    .end annotation

    const-string v0, "guildHashes"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    iput-wide p2, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    iput p4, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    iput p5, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Map;JIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    const-wide/16 p2, 0x0

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p6, 0x4

    const/4 p3, -0x1

    if-eqz p2, :cond_2

    const/4 p7, -0x1

    goto :goto_0

    :cond_2
    move p7, p4

    :goto_0
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    const/4 v2, -0x1

    goto :goto_1

    :cond_3
    move v2, p5

    :goto_1
    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move p6, p7

    move p7, v2

    invoke-direct/range {p2 .. p7}, Lcom/discord/stores/StoreClientDataState$ClientDataState;-><init>(Ljava/util/Map;JII)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreClientDataState$ClientDataState;Ljava/util/Map;JIIILjava/lang/Object;)Lcom/discord/stores/StoreClientDataState$ClientDataState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-wide p2, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p4, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    :cond_2
    move p7, p4

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p5, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    :cond_3
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move p6, p7

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/stores/StoreClientDataState$ClientDataState;->copy(Ljava/util/Map;JII)Lcom/discord/stores/StoreClientDataState$ClientDataState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildHash;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    return-wide v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    return v0
.end method

.method public final copy(Ljava/util/Map;JII)Lcom/discord/stores/StoreClientDataState$ClientDataState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildHash;",
            ">;JII)",
            "Lcom/discord/stores/StoreClientDataState$ClientDataState;"
        }
    .end annotation

    const-string v0, "guildHashes"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreClientDataState$ClientDataState;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreClientDataState$ClientDataState;-><init>(Ljava/util/Map;JII)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    iget-wide v2, p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    iget v1, p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    iget p1, p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildHashes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildHash;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    return-object v0
.end method

.method public final getHighestLastMessageId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    return-wide v0
.end method

.method public final getReadStateVersion()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    return v0
.end method

.method public final getUserGuildSettingsVersion()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toIdentifyData()Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;
    .locals 10

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuildHash;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildHash;->toArray()[Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    check-cast v1, [Ljava/lang/String;

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    iget-wide v6, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    iget v8, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    iget v9, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    new-instance v0, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;-><init>(Ljava/util/Map;JII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ClientDataState(guildHashes="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", highestLastMessageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->highestLastMessageId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", readStateVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->readStateVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", userGuildSettingsVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/StoreClientDataState$ClientDataState;->userGuildSettingsVersion:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
