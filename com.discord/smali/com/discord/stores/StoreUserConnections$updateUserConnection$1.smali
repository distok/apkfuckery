.class public final Lcom/discord/stores/StoreUserConnections$updateUserConnection$1;
.super Lx/m/c/k;
.source "StoreUserConnections.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUserConnections;->updateUserConnection(Lcom/discord/models/domain/ModelConnectedAccount;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelConnectedAccount;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreUserConnections;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUserConnections;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserConnections$updateUserConnection$1;->this$0:Lcom/discord/stores/StoreUserConnections;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelConnectedAccount;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreUserConnections$updateUserConnection$1;->invoke(Lcom/discord/models/domain/ModelConnectedAccount;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelConnectedAccount;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/stores/StoreUserConnections$updateUserConnection$1;->this$0:Lcom/discord/stores/StoreUserConnections;

    invoke-virtual {p1}, Lcom/discord/stores/StoreUserConnections;->fetchConnectedAccounts()V

    return-void
.end method
