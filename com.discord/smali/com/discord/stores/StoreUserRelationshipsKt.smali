.class public final Lcom/discord/stores/StoreUserRelationshipsKt;
.super Ljava/lang/Object;
.source "StoreUserRelationships.kt"


# static fields
.field private static final UNLOADED_RELATIONSHIPS_SENTINEL:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/Pair;

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    aput-object v4, v1, v2

    const-string v2, "pairs"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/util/HashMap;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v2, v1}, Lx/h/f;->putAll(Ljava/util/Map;[Lkotlin/Pair;)V

    sput-object v2, Lcom/discord/stores/StoreUserRelationshipsKt;->UNLOADED_RELATIONSHIPS_SENTINEL:Ljava/util/HashMap;

    return-void
.end method

.method public static final synthetic access$getUNLOADED_RELATIONSHIPS_SENTINEL$p()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreUserRelationshipsKt;->UNLOADED_RELATIONSHIPS_SENTINEL:Ljava/util/HashMap;

    return-object v0
.end method
