.class public final Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;
.super Ljava/lang/Object;
.source "StoreChangeLog.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreChangeLog;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Long;",
        "Lcom/discord/stores/StoreChangeLog$StorePayload;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;->INSTANCE:Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Long;)Lcom/discord/stores/StoreChangeLog$StorePayload;
    .locals 3

    new-instance v0, Lcom/discord/stores/StoreChangeLog$StorePayload;

    const-string/jumbo v1, "userId"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/discord/stores/StoreChangeLog$StorePayload;-><init>(J)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$1;->call(Ljava/lang/Long;)Lcom/discord/stores/StoreChangeLog$StorePayload;

    move-result-object p1

    return-object p1
.end method
