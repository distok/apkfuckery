.class public final Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;
.super Lx/m/c/k;
.source "StoreExperiments.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/models/experiments/domain/Experiment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $name:Ljava/lang/String;

.field public final synthetic $trigger:Z

.field public final synthetic this$0:Lcom/discord/stores/StoreExperiments;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iput-object p2, p0, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->$name:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->$trigger:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/models/experiments/domain/Experiment;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->$name:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->$trigger:Z

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;->invoke()Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    return-object v0
.end method
