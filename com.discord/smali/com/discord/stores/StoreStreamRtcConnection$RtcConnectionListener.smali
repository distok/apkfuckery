.class public final Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;
.super Ljava/lang/Object;
.source "StoreStreamRtcConnection.kt"

# interfaces
.implements Lcom/discord/rtcconnection/RtcConnection$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreStreamRtcConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "RtcConnectionListener"
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreStreamRtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStreamRtcConnection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnalyticsEvent(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "properties"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {p1}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getAnalyticsStore$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/StoreAnalytics;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreAnalytics;->trackMediaSessionJoined(Ljava/util/Map;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {p1}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getDispatcher$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/Dispatcher;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onAnalyticsEvent$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onAnalyticsEvent$1;-><init>(Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public onFatalClose()V
    .locals 0

    return-void
.end method

.method public onMediaSessionIdReceived()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getDispatcher$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onMediaSessionIdReceived$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onMediaSessionIdReceived$1;-><init>(Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onQualityUpdate(Lcom/discord/rtcconnection/RtcConnection$Quality;)V
    .locals 2

    const-string v0, "quality"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getDispatcher$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onQualityUpdate$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onQualityUpdate$1;-><init>(Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;Lcom/discord/rtcconnection/RtcConnection$Quality;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onSpeaking(JZ)V
    .locals 0

    return-void
.end method

.method public onStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 2

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getDispatcher$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;-><init>(Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;Lcom/discord/rtcconnection/RtcConnection$State;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onVideoStream(JLjava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getDispatcher$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onVideoStream$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onVideoStream$1;-><init>(Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;JLjava/lang/Integer;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
