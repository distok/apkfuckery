.class public abstract Lcom/discord/stores/StoreV2;
.super Lcom/discord/stores/Store;
.source "StoreV2.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;
.implements Lcom/discord/stores/updates/ObservationDeck$UpdateSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreV2$MarkChangedDelegate;
    }
.end annotation


# instance fields
.field private final updateSources:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/discord/stores/StoreV2;->updateSources:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final getUpdateSources()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreV2;->updateSources:Ljava/util/Set;

    return-object v0
.end method

.method public final markChanged()V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreV2;->updateSources:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final varargs markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V
    .locals 1

    const-string/jumbo v0, "updates"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    iget-object v0, p0, Lcom/discord/stores/StoreV2;->updateSources:Ljava/util/Set;

    invoke-static {v0, p1}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-void
.end method

.method public final markUnchanged(Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V
    .locals 1

    const-string/jumbo v0, "updateSource"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreV2;->updateSources:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onDispatchEnded()V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreV2;->updateSources:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public snapshotData()V
    .locals 0

    return-void
.end method
