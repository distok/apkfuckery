.class public final Lcom/discord/stores/StoreNotifications$configureContextSetter$1;
.super Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;
.source "StoreNotifications.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreNotifications;->configureContextSetter(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreNotifications;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreNotifications;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreNotifications$configureContextSetter$1;->this$0:Lcom/discord/stores/StoreNotifications;

    invoke-direct {p0}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreatedOrResumed(Lcom/discord/app/AppActivity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;->onActivityCreatedOrResumed(Lcom/discord/app/AppActivity;)V

    iget-object v0, p0, Lcom/discord/stores/StoreNotifications$configureContextSetter$1;->this$0:Lcom/discord/stores/StoreNotifications;

    invoke-static {v0, p1}, Lcom/discord/stores/StoreNotifications;->access$setContext$p(Lcom/discord/stores/StoreNotifications;Landroid/content/Context;)V

    return-void
.end method

.method public onActivityDestroyed(Lcom/discord/app/AppActivity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;->onActivityDestroyed(Lcom/discord/app/AppActivity;)V

    iget-object p1, p0, Lcom/discord/stores/StoreNotifications$configureContextSetter$1;->this$0:Lcom/discord/stores/StoreNotifications;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/stores/StoreNotifications;->access$setContext$p(Lcom/discord/stores/StoreNotifications;Landroid/content/Context;)V

    return-void
.end method
