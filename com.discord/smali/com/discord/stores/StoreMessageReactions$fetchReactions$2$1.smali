.class public final Lcom/discord/stores/StoreMessageReactions$fetchReactions$2$1;
.super Lx/m/c/k;
.source "StoreMessageReactions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;->invoke(Lcom/discord/utilities/error/Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2$1;->this$0:Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2$1;->this$0:Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;

    iget-object v1, v0, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;->this$0:Lcom/discord/stores/StoreMessageReactions;

    iget-wide v2, v0, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;->$channelId:J

    iget-wide v4, v0, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;->$messageId:J

    iget-object v6, v0, Lcom/discord/stores/StoreMessageReactions$fetchReactions$2;->$emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-static/range {v1 .. v6}, Lcom/discord/stores/StoreMessageReactions;->access$handleLoadReactionUsersFailure(Lcom/discord/stores/StoreMessageReactions;JJLcom/discord/models/domain/ModelMessageReaction$Emoji;)V

    return-void
.end method
