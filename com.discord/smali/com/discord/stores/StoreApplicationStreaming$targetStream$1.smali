.class public final Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;
.super Lx/m/c/k;
.source "StoreApplicationStreaming.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreApplicationStreaming;->targetStream(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $streamKey:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreApplicationStreaming;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplicationStreaming;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->$streamKey:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getActiveApplicationStream$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->$streamKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    return-void

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/discord/stores/StoreApplicationStreaming;->access$stopStreamInternal(Lcom/discord/stores/StoreApplicationStreaming;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationStreaming;->access$getStoreStream$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;->$streamKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->handleStreamTargeted(Ljava/lang/String;)V

    return-void
.end method
