.class public final Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;
.super Lx/m/c/k;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePremiumGuildSubscription;->updateUserPremiumGuildSubscriptionSlot(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $newSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

.field public final synthetic this$0:Lcom/discord/stores/StorePremiumGuildSubscription;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->this$0:Lcom/discord/stores/StorePremiumGuildSubscription;

    iput-object p2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->$newSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->this$0:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-static {v0}, Lcom/discord/stores/StorePremiumGuildSubscription;->access$getState$p(Lcom/discord/stores/StorePremiumGuildSubscription;)Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->$newSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->$newSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v3}, Lx/h/f;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->this$0:Lcom/discord/stores/StorePremiumGuildSubscription;

    new-instance v2, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-direct {v2, v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/Map;)V

    invoke-static {v1, v2}, Lcom/discord/stores/StorePremiumGuildSubscription;->access$setState$p(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StorePremiumGuildSubscription$State;)V

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;->this$0:Lcom/discord/stores/StorePremiumGuildSubscription;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/discord/stores/StorePremiumGuildSubscription;->access$setDirty$p(Lcom/discord/stores/StorePremiumGuildSubscription;Z)V

    :cond_0
    return-void
.end method
