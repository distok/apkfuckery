.class public final Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;
.super Lx/m/c/k;
.source "StoreGooglePlayPurchases.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGooglePlayPurchases;->onVerificationSuccess(Lcom/android/billingclient/api/Purchase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $purchase:Lcom/android/billingclient/api/Purchase;

.field public final synthetic this$0:Lcom/discord/stores/StoreGooglePlayPurchases;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/android/billingclient/api/Purchase;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    iput-object p2, p0, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;->$purchase:Lcom/android/billingclient/api/Purchase;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-static {v0}, Lcom/discord/stores/StoreGooglePlayPurchases;->access$getEventSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/PublishSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;

    iget-object v2, p0, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;->$purchase:Lcom/android/billingclient/api/Purchase;

    invoke-virtual {v2}, Lcom/android/billingclient/api/Purchase;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "purchase.sku"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-static {v0}, Lcom/discord/stores/StoreGooglePlayPurchases;->access$getQueryStateSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
