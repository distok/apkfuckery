.class public final Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;
.super Lx/m/c/k;
.source "StoreGatewayConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGatewayConnection;->requestApplicationCommands(JLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/gateway/GatewaySocket;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $applications:Z

.field public final synthetic $commandLimit:I

.field public final synthetic $guildId:J

.field public final synthetic $nonce:Ljava/lang/String;

.field public final synthetic $offset:Ljava/lang/Integer;

.field public final synthetic $query:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;ZLjava/lang/Integer;Ljava/lang/String;I)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$guildId:J

    iput-object p3, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$nonce:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$applications:Z

    iput-object p5, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$offset:Ljava/lang/Integer;

    iput-object p6, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$query:Ljava/lang/String;

    iput p7, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$commandLimit:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/gateway/GatewaySocket;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->invoke(Lcom/discord/gateway/GatewaySocket;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/gateway/GatewaySocket;)V
    .locals 9

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;

    iget-wide v2, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$guildId:J

    iget-object v8, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$nonce:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$applications:Z

    iget-object v5, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$offset:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$query:Ljava/lang/String;

    iget v1, p0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;->$commandLimit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;-><init>(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZLjava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/gateway/GatewaySocket;->requestApplicationCommands(Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;)V

    return-void
.end method
