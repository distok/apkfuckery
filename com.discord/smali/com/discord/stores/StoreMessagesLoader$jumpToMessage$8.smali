.class public final synthetic Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$8;
.super Lx/m/c/i;
.source "StoreMessagesLoader.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessagesLoader;->jumpToMessage(JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $handleTargetChannelResolved$2:Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;)V
    .locals 6

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$8;->$handleTargetChannelResolved$2:Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "handleTargetChannelResolved"

    const-string v4, "invoke(Lcom/discord/models/domain/ModelChannel;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$8;->invoke(Lcom/discord/models/domain/ModelChannel;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$8;->$handleTargetChannelResolved$2:Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;->invoke(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method
