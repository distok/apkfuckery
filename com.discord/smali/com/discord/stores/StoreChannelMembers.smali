.class public final Lcom/discord/stores/StoreChannelMembers;
.super Lcom/discord/stores/Store;
.source "StoreChannelMembers.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreChannelMembers$MemberListIdCalculator;,
        Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;,
        Lcom/discord/stores/StoreChannelMembers$MemberListUpdateException;
    }
.end annotation


# instance fields
.field private final channelsProvider:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Long;",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final flushTrigger:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final guildMemberCountsProvider:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Long;",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private isDirty:Z

.field private final memberListPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final memberLists:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;",
            ">;>;"
        }
    .end annotation
.end field

.field private final storeStream:Lcom/discord/stores/StoreStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreStream;",
            "Lcom/discord/stores/Dispatcher;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "+",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "+",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string/jumbo v0, "storeStream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelsProvider"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildMemberCountsProvider"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelMembers;->storeStream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreChannelMembers;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreChannelMembers;->channelsProvider:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/discord/stores/StoreChannelMembers;->guildMemberCountsProvider:Lkotlin/jvm/functions/Function1;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreChannelMembers;->memberListPublisher:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreChannelMembers;->flushTrigger:Lrx/subjects/PublishSubject;

    const-string p2, "flushTrigger"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p3, 0x1

    invoke-static {p1, p3, p4, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreChannelMembers;

    new-instance v6, Lcom/discord/stores/StoreChannelMembers$1;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreChannelMembers$1;-><init>(Lcom/discord/stores/StoreChannelMembers;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$doFlush(Lcom/discord/stores/StoreChannelMembers;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreChannelMembers;->doFlush()V

    return-void
.end method

.method public static final synthetic access$getGuildMemberCountsProvider$p(Lcom/discord/stores/StoreChannelMembers;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelMembers;->guildMemberCountsProvider:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getMemberListObservable(Lcom/discord/stores/StoreChannelMembers;JLjava/lang/String;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreChannelMembers;->getMemberListObservable(JLjava/lang/String;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getMemberListPublisher$p(Lcom/discord/stores/StoreChannelMembers;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelMembers;->memberListPublisher:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getMemberLists$p(Lcom/discord/stores/StoreChannelMembers;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    return-object p0
.end method

.method public static final synthetic access$isDirty$p(Lcom/discord/stores/StoreChannelMembers;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreChannelMembers;->isDirty:Z

    return p0
.end method

.method public static final synthetic access$makeGroup(Lcom/discord/stores/StoreChannelMembers;JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreChannelMembers;->makeGroup(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$makeMember(Lcom/discord/stores/StoreChannelMembers;JJZ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/stores/StoreChannelMembers;->makeMember(JJZ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setDirty$p(Lcom/discord/stores/StoreChannelMembers;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreChannelMembers;->isDirty:Z

    return-void
.end method

.method private final allowOwnerIndicator(J)Z
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuilds;->getGuildRolesInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildRole;->isHoist()Z

    move-result v2

    if-eqz v2, :cond_2

    const-wide/16 v2, 0x8

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_3
    :goto_1
    xor-int/lit8 p1, v0, 0x1

    return p1
.end method

.method private final doFlush()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreChannelMembers$doFlush$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreChannelMembers$doFlush$1;-><init>(Lcom/discord/stores/StoreChannelMembers;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final getMemberList(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_0

    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getMemberListObservable(JLjava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers;->memberListPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreChannelMembers$getMemberListObservable$1;

    invoke-direct {v1, p1, p2, p3}, Lcom/discord/stores/StoreChannelMembers$getMemberListObservable$1;-><init>(JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreChannelMembers$getMemberListObservable$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/discord/stores/StoreChannelMembers$getMemberListObservable$2;-><init>(Lcom/discord/stores/StoreChannelMembers;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "memberListPublisher\n    \u2026            }\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final handleDelete(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;J)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0, p3, p4, p1}, Lcom/discord/stores/StoreChannelMembers;->getMemberList(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;->getIndex()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->delete(I)V

    :cond_0
    return-void
.end method

.method private final handleInsert(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;->getIndex()I

    move-result v0

    invoke-direct {p0, p3, p4, p1}, Lcom/discord/stores/StoreChannelMembers;->getMemberList(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;->getItem()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;

    move-result-object p2

    invoke-direct {p0, p3, p4, p2}, Lcom/discord/stores/StoreChannelMembers;->makeRow(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->insert(ILcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)V

    :cond_0
    return-void
.end method

.method private final handleInvalidate(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;J)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;->getRange()Lkotlin/ranges/IntRange;

    move-result-object p2

    invoke-direct {p0, p3, p4, p1}, Lcom/discord/stores/StoreChannelMembers;->getMemberList(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->invalidate(Lkotlin/ranges/IntRange;)V

    :cond_0
    return-void
.end method

.method private final handleSync(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;J)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string v1, "memberLists[guildId]\n   \u2026mberLists[guildId] = it }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    const/4 v4, 0x0

    sget-object v8, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, v1

    move-object v3, p1

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;-><init>(Ljava/lang/String;ILcom/discord/utilities/logging/Logger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->getListId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " INSTANTIATE"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ChannelMemberList"

    invoke-virtual {v8, p1, v0}, Lcom/discord/app/AppLog;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, p3, p4}, Lcom/discord/stores/StoreChannelMembers;->allowOwnerIndicator(J)Z

    move-result p1

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;->getItems()Ljava/util/List;

    move-result-object v0

    new-instance v8, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;

    instance-of v3, v2, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;

    if-eqz v3, :cond_2

    check-cast v2, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v3, "item.member.user!!"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    move-object v2, p0

    move-wide v3, p3

    move v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/discord/stores/StoreChannelMembers;->makeMember(JJZ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object v2

    goto :goto_3

    :cond_2
    instance-of v3, v2, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;

    if-eqz v3, :cond_3

    check-cast v2, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;->getGroup()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;

    move-result-object v2

    invoke-direct {p0, p3, p4, v2}, Lcom/discord/stores/StoreChannelMembers;->makeGroup(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object v2

    :goto_3
    invoke-interface {v8, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;->getRange()Lkotlin/ranges/IntRange;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {v1, p1, v8}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->sync(ILjava/util/List;)V

    return-void
.end method

.method private final handleUpdate(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;J)V
    .locals 1

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;->getIndex()I

    move-result v0

    invoke-direct {p0, p3, p4, p1}, Lcom/discord/stores/StoreChannelMembers;->getMemberList(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;->getItem()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;

    move-result-object p2

    invoke-direct {p0, p3, p4, p2}, Lcom/discord/stores/StoreChannelMembers;->makeRow(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->update(ILcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;)V

    :cond_0
    return-void
.end method

.method private final makeGroup(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getType()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group$Type;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_2

    const/4 p1, 0x1

    if-eq v1, p1, :cond_1

    const/4 p1, 0x2

    if-ne v1, p1, :cond_0

    new-instance p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    sget-object p2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;->ONLINE:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getCount()I

    move-result p3

    invoke-direct {p1, v0, p2, p3}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;-><init>(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;I)V

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;

    sget-object p2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;->OFFLINE:Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getCount()I

    move-result p3

    invoke-direct {p1, v0, p2, p3}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader;-><init>(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$StatusHeader$Type;I)V

    return-object p1

    :cond_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreGuilds;->getGuildRolesInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildRole;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole;->getName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const-string p1, ""

    :goto_0
    new-instance p2, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;->getCount()I

    move-result p3

    invoke-direct {p2, v0, v1, p1, p3}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$RoleHeader;-><init>(JLjava/lang/String;I)V

    return-object p2
.end method

.method private final makeMember(JJZ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 24
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    move-wide/from16 v1, p3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreGuilds;->getGuildsInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v3

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreGuilds;->getGuildMembersComputedInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v4

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-object/from16 v13, p0

    goto :goto_0

    :cond_0
    move-object/from16 v13, p0

    move-object v4, v5

    :goto_0
    iget-object v6, v13, Lcom/discord/stores/StoreChannelMembers;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v6}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/stores/StoreUser;->getUsersInternal$app_productionDiscordExternalRelease()Ljava/util/Map;

    move-result-object v6

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelUser;

    if-eqz v6, :cond_5

    if-nez v4, :cond_1

    goto/16 :goto_3

    :cond_1
    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getColor()I

    move-result v7

    const/high16 v8, -0x1000000

    if-eq v7, v8, :cond_2

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getColor()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v8, v7

    goto :goto_1

    :cond_2
    move-object v8, v5

    :goto_1
    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v7

    if-eqz v7, :cond_3

    const v7, 0x7f1217ce

    goto :goto_2

    :cond_3
    const v7, 0x7f120384

    :goto_2
    new-instance v14, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;

    invoke-virtual {v6, v4}, Lcom/discord/models/domain/ModelUser;->getNickOrUsername(Lcom/discord/models/domain/ModelGuildMember$Computed;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "user.getNickOrUsername(member)"

    invoke-static {v9, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->isBot()Z

    move-result v10

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->isVerifiedBot()Z

    move-result v11

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v12

    invoke-virtual {v12}, Lcom/discord/stores/StoreUserPresence;->getPresences()Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    move-result-object v12

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v12, v15}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/discord/models/domain/ModelPresence;

    const/4 v15, 0x6

    const/4 v13, 0x0

    invoke-static {v6, v13, v5, v15, v5}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    if-eqz p5, :cond_4

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v5

    cmp-long v3, v5, v1

    if-nez v3, :cond_4

    const/4 v3, 0x1

    const/4 v13, 0x1

    :cond_4
    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getPremiumSince()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreApplicationStreaming;->isUserStreaming(J)Z

    move-result v17

    move-object v0, v14

    move-wide/from16 v1, p3

    move-object v3, v9

    move v4, v10

    move-object v5, v7

    move v6, v11

    move-object v7, v12

    move-object v9, v15

    move v10, v13

    move-object/from16 v11, v16

    move/from16 v12, v17

    invoke-direct/range {v0 .. v12}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row$Member;-><init>(JLjava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/domain/ModelPresence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;Z)V

    return-object v14

    :cond_5
    :goto_3
    sget-object v18, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v0, "Unable to make member. GuildID: "

    const-string v3, " -- UserID: "

    move-wide/from16 v6, p1

    invoke-static {v0, v6, v7, v3}, Lf/e/c/a/a;->K(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x6

    const/16 v23, 0x0

    invoke-static/range {v18 .. v23}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return-object v5
.end method

.method private final makeRow(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    instance-of v0, p3, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;

    if-eqz v0, :cond_0

    check-cast p3, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object p3

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p3

    invoke-static {p3}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v0, "item.member.user!!"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelMembers;->allowOwnerIndicator(J)Z

    move-result v6

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreChannelMembers;->makeMember(JJZ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p1

    goto :goto_0

    :cond_0
    instance-of v0, p3, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;

    if-eqz v0, :cond_1

    check-cast p3, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;->getGroup()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreChannelMembers;->makeGroup(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public final get(JJ)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreChannelMembers$MemberListIdCalculator;->INSTANCE:Lcom/discord/stores/StoreChannelMembers$MemberListIdCalculator;

    iget-object v1, p0, Lcom/discord/stores/StoreChannelMembers;->channelsProvider:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, p3, p4}, Lcom/discord/stores/StoreChannelMembers$MemberListIdCalculator;->computeMemberListId(Lkotlin/jvm/functions/Function1;J)Lrx/Observable;

    move-result-object p3

    invoke-virtual {p3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p3

    new-instance p4, Lcom/discord/stores/StoreChannelMembers$get$1;

    invoke-direct {p4, p0, p1, p2}, Lcom/discord/stores/StoreChannelMembers$get$1;-><init>(Lcom/discord/stores/StoreChannelMembers;J)V

    invoke-virtual {p3, p4}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "MemberListIdCalculator.c\u2026ldId, listId)\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final handleGuildMemberListUpdate(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "update"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getGuildId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getId()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;->INSTANCE:Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;

    invoke-virtual {v3, p1}, Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;->logUpdate(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V

    :try_start_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getOperations()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;

    instance-of v5, v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;

    if-eqz v5, :cond_1

    check-cast v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;

    invoke-direct {p0, v2, v4, v0, v1}, Lcom/discord/stores/StoreChannelMembers;->handleSync(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;J)V

    goto :goto_0

    :cond_1
    instance-of v5, v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;

    if-eqz v5, :cond_2

    check-cast v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;

    invoke-direct {p0, v2, v4, v0, v1}, Lcom/discord/stores/StoreChannelMembers;->handleUpdate(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;J)V

    goto :goto_0

    :cond_2
    instance-of v5, v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;

    if-eqz v5, :cond_3

    check-cast v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;

    invoke-direct {p0, v2, v4, v0, v1}, Lcom/discord/stores/StoreChannelMembers;->handleInsert(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;J)V

    goto :goto_0

    :cond_3
    instance-of v5, v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;

    if-eqz v5, :cond_4

    check-cast v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;

    invoke-direct {p0, v2, v4, v0, v1}, Lcom/discord/stores/StoreChannelMembers;->handleDelete(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;J)V

    goto :goto_0

    :cond_4
    instance-of v5, v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;

    if-eqz v5, :cond_0

    check-cast v4, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;

    invoke-direct {p0, v2, v4, v0, v1}, Lcom/discord/stores/StoreChannelMembers;->handleInvalidate(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;J)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getGuildId()J

    move-result-wide v3

    invoke-direct {p0, v3, v4, v2}, Lcom/discord/stores/StoreChannelMembers;->getMemberList(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getGroups()Ljava/util/List;

    move-result-object p1

    new-instance v4, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;

    invoke-direct {v4, p0, v0, v1}, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;-><init>(Lcom/discord/stores/StoreChannelMembers;J)V

    invoke-virtual {v3, p1, v4}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->setGroups(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    sget-object v3, Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;->INSTANCE:Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;

    invoke-virtual {v3, v0, v1, v2, p1}, Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;->dumpLogs(JLjava/lang/String;Ljava/lang/Exception;)V

    :cond_6
    :goto_1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreChannelMembers;->isDirty:Z

    return-void
.end method

.method public final handleGuildRemove(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreChannelMembers;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final handleGuildRoleUpdate(Lcom/discord/models/domain/ModelGuildRole$Payload;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "roleUpdate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole$Payload;->getGuildId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreChannelMembers;->allowOwnerIndicator(J)Z

    move-result p1

    iget-object v2, p0, Lcom/discord/stores/StoreChannelMembers;->memberLists:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;

    new-instance v4, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;

    invoke-direct {v4, p0, v0, v1, p1}, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;-><init>(Lcom/discord/stores/StoreChannelMembers;JZ)V

    invoke-virtual {v3, v4}, Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;->rebuildMembers(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreChannelMembers;->isDirty:Z

    return-void
.end method

.method public onDispatchEnded()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/stores/StoreChannelMembers;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers;->flushTrigger:Lrx/subjects/PublishSubject;

    sget-object v1, Lkotlin/Unit;->a:Lkotlin/Unit;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
